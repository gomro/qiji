//
//  QJMallShopHomeRequset.m
//  QJBox
//
//  Created by rui on 2022/8/24.
//

#import "QJMallShopHomeRequset.h"

@implementation QJMallShopHomeRequset

/**
 * @author: zjr
 * @date: 2022-7-25
 * @desc:获取商城商品列表数据
 * Post请求
 */
- (void)getMallShopListRequest{
    self.urlStr = [NSString stringWithFormat:@"/shop/qjcoin/product/list"];
    self.requestType = YTKRequestMethodPOST;
    self.serializerType = YTKRequestSerializerTypeJSON;
    [self start];
}

/**
 * @author: zjr
 * @date: 2022-9-21
 * @desc:兑换记录
 * Get请求
 */
- (void)getMallShopExchangesListRequest{
    self.urlStr = [NSString stringWithFormat:@"/shop/exchanges"];
    self.requestType = YTKRequestMethodGET;
    [self start];
}

/**
 * @author: zjr
 * @date: 2022-9-21
 * @desc:查看卡券、电子卡
 * Get请求
 */
- (void)getMallShopCardDetailRequest:(NSString *)cardID{
    self.urlStr = [NSString stringWithFormat:@"/shop/order/cardDetail/%@",cardID];
    self.requestType = YTKRequestMethodGET;
    [self start];
}

/**
 * @author: zjr
 * @date: 2022-9-21
 * @desc:发表商品评价
 * Post请求
 */
- (void)getMallShopPublishCommentRequest{
    self.urlStr = [NSString stringWithFormat:@"/comment/prod"];
    self.requestType = YTKRequestMethodPOST;
    self.serializerType = YTKRequestSerializerTypeJSON;
    [self start];
}

@end
