//
//  QJMallShopHomeRequset.h
//  QJBox
//
//  Created by rui on 2022/8/24.
//

#import "QJBaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJMallShopHomeRequset : QJBaseRequest

/**
 * @author: zjr
 * @date: 2022-7-25
 * @desc:获取商城商品列表数据
 * GET请求
 */
- (void)getMallShopListRequest;

/**
 * @author: zjr
 * @date: 2022-9-21
 * @desc:兑换记录
 * Get请求
 */
- (void)getMallShopExchangesListRequest;

/**
 * @author: zjr
 * @date: 2022-9-21
 * @desc:查看卡券、电子卡
 * Get请求
 */
- (void)getMallShopCardDetailRequest:(NSString *)cardID;

/**
 * @author: zjr
 * @date: 2022-9-21
 * @desc:发表商品评价
 * Post请求
 */
- (void)getMallShopPublishCommentRequest;

@end

NS_ASSUME_NONNULL_END
