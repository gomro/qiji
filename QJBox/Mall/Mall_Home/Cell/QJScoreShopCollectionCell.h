//
//  QJScoreShopCollectionCell.h
//  QJBox
//
//  Created by rui on 2022/6/28.
//

#import <UIKit/UIKit.h>
#import "QJMallShopHomeListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJScoreShopCollectionCell : UICollectionViewCell

@property (nonatomic, strong) QJMallShopHomeDetailModel *model;

@end

NS_ASSUME_NONNULL_END
