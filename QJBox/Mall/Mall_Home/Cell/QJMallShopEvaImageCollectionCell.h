//
//  QJMallShopEvaImageCollectionCell.h
//  QJBox
//
//  Created by rui on 2022/8/25.
//

#import <UIKit/UIKit.h>
#import "TZAssetModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJMallShopEvaImageCollectionCell : UICollectionViewCell

/* 点击回调 */
@property (nonatomic, copy) void(^closeBtnClick)(void);

@property (nonatomic, strong) TZAssetModel *model;

@property (nonatomic, strong) id assetOrUrl;

@property (nonatomic, strong) UIImage *image;

@property (nonatomic, assign) BOOL isChooseVideo;

@end

NS_ASSUME_NONNULL_END
