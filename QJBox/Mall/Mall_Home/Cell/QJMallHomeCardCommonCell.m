//
//  QJMallHomeCardCommonCell.m
//  QJBox
//
//  Created by rui on 2022/9/21.
//

#import "QJMallHomeCardCommonCell.h"

@interface QJMallHomeCardCommonCell ()

//白色底图
@property (nonatomic, strong) UIView *whiteBgView;
/* 卡号名称 */
@property (nonatomic, strong) UILabel *cardTitleLabel;
@property (nonatomic, strong) UILabel *cardNameLabel;
@property (nonatomic, strong) UIButton *pasteCardButton;

@end

@implementation QJMallHomeCardCommonCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = UIColorHex(F5F5F5);
        self.contentView.backgroundColor = UIColorHex(F5F5F5);

        /* 初始化UI */
        [self initUI];
        /* 添加约束 */
        [self makeConstraints];
    }
    return self;
}

/**
 * @author: zjr
 * @date: 2022-6-27
 * @desc: 初始化UI
 */
- (void)initUI{
    [self.contentView addSubview:self.whiteBgView];
    
    [self.whiteBgView addSubview:self.cardTitleLabel];
    [self.whiteBgView addSubview:self.cardNameLabel];
    [self.whiteBgView addSubview:self.pasteCardButton];
}

/**
 * @author: zjr
 * @date: 2022-7-1
 * @desc: 添加约束
 */
- (void)makeConstraints{
    [self.whiteBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(16);
        make.right.equalTo(self.contentView).offset(-16);
        make.top.bottom.equalTo(self.contentView);
    }];
    
    [self.cardTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.whiteBgView).offset(16);
        make.left.equalTo(self.whiteBgView).offset(16);
    }];
    
    [self.pasteCardButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.cardTitleLabel);
        make.right.equalTo(self.whiteBgView).offset(-16);
    }];
    
    [self.cardNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.whiteBgView).offset(16);
        make.right.equalTo(self.pasteCardButton.mas_left).offset(-10);
        make.bottom.equalTo(self.whiteBgView);
    }];
}

#pragma mark - Lazy Loading
- (UIView *)whiteBgView {
    if (!_whiteBgView) {
        _whiteBgView = [UIView new];
        _whiteBgView.backgroundColor = [UIColor whiteColor];
//        _whiteBgView.layer.cornerRadius = 8;
    }
    return _whiteBgView;
}

- (UILabel *)cardTitleLabel {
    if (!_cardTitleLabel) {
        _cardTitleLabel = [UILabel new];
        _cardTitleLabel.font = kFont(14);
        _cardTitleLabel.textColor = UIColorFromRGB(0x16191C);
        _cardTitleLabel.text = @"卡号:";
    }
    return _cardTitleLabel;
}

- (UILabel *)cardNameLabel {
    if (!_cardNameLabel) {
        _cardNameLabel = [UILabel new];
        _cardNameLabel.font = kFont(14);
        _cardNameLabel.textColor = UIColorFromRGB(0x16191C);
        _cardNameLabel.text = @"XYU78183066953";
    }
    return _cardNameLabel;
}

- (UIButton *)pasteCardButton{
    if (!_pasteCardButton) {
        _pasteCardButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_pasteCardButton setImage:[UIImage imageNamed:@"mall_home_pasteicon"] forState:UIControlStateNormal];
        [_pasteCardButton addTarget:self action:@selector(pasteCard) forControlEvents:UIControlEventTouchUpInside];
    }
    return _pasteCardButton;
}

/**
 * @author: zjr
 * @date: 2022-6-30
 * @desc: 数据赋值
 */
- (void)setModel:(QJMallShopCardItemDetailModel *)model{
    _model = model;
    _cardTitleLabel.text = _model.name;
    _cardNameLabel.text = _model.value;
    if (_model.copyable == YES){
        _pasteCardButton.hidden = NO;
        [self.pasteCardButton mas_updateConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.cardTitleLabel);
            make.right.equalTo(self.whiteBgView).offset(-16);
        }];
        
        [self.cardNameLabel mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.whiteBgView).offset(16);
            make.right.equalTo(self.pasteCardButton.mas_left).offset(-10);
            make.bottom.equalTo(self.whiteBgView);
        }];
    }else{
        _pasteCardButton.hidden = YES;
        [self.cardNameLabel mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.whiteBgView).offset(16);
            make.right.equalTo(self.whiteBgView).offset(-16);
            make.bottom.equalTo(self.whiteBgView);
        }];
    }
}

/**
 * @author: zjr
 * @date: 2022-8-26
 * @desc: button点击事件
 */
- (void)pasteCard{
    UIPasteboard *pab = [UIPasteboard generalPasteboard];
    pab.string = _model.value;
    if (pab == nil) {
        [QJAppTool showToast:@"复制失败"];
    } else {
        [QJAppTool showToast:@"复制成功"];
    }
}

@end
