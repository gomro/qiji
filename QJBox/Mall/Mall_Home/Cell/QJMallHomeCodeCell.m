//
//  QJMallHomeCodeCell.m
//  QJBox
//
//  Created by rui on 2022/8/24.
//

#import "QJMallHomeCodeCell.h"

@interface QJMallHomeCodeCell ()

//白色底图
@property (nonatomic, strong) UIView *whiteBgView;
/* 游戏banner 名称 */
@property (nonatomic, strong) UIImageView *bannerImageView;
@property (nonatomic, strong) UILabel *gameNameTitleLabel;
@property (nonatomic, strong) UILabel *gameNameLabel;
/* 兑换码名称 */
@property (nonatomic, strong) UILabel *codeTitleLabel;
@property (nonatomic, strong) UILabel *codeNameLabel;
@property (nonatomic, strong) UIButton *pasteCodeButton;
/* 使用说明 注意事项 */
@property (nonatomic, strong) UIView *lineView;
@property (nonatomic, strong) UILabel *explainTitleLabel;
@property (nonatomic, strong) UILabel *explainNameLabel;
@property (nonatomic, strong) UILabel *attentionTitleLabel;
@property (nonatomic, strong) UILabel *attentionNameLabel;

@end

@implementation QJMallHomeCodeCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor colorWithHexString:@"0xFFFFFF"];
        self.contentView.backgroundColor = [UIColor colorWithHexString:@"0xFFFFFF"];

        /* 初始化UI */
        [self initUI];
        /* 添加约束 */
        [self makeConstraints];
    }
    return self;
}

/**
 * @author: zjr
 * @date: 2022-6-27
 * @desc: 初始化UI
 */
- (void)initUI{
    [self.contentView addSubview:self.whiteBgView];
    [self.whiteBgView addSubview:self.bannerImageView];
    [self.whiteBgView addSubview:self.gameNameTitleLabel];
    [self.whiteBgView addSubview:self.gameNameLabel];
    
    [self.whiteBgView addSubview:self.codeTitleLabel];
    [self.whiteBgView addSubview:self.codeNameLabel];
    [self.whiteBgView addSubview:self.pasteCodeButton];
    
    [self.whiteBgView addSubview:self.lineView];
    [self.whiteBgView addSubview:self.explainTitleLabel];
    [self.whiteBgView addSubview:self.explainNameLabel];
    [self.whiteBgView addSubview:self.attentionTitleLabel];
    [self.whiteBgView addSubview:self.attentionNameLabel];
}

/**
 * @author: zjr
 * @date: 2022-7-1
 * @desc: 添加约束
 */
- (void)makeConstraints{
    [self.whiteBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(16);
        make.right.equalTo(self.contentView).offset(-16);
        make.top.bottom.equalTo(self.contentView);
    }];
    
    [self.bannerImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.whiteBgView).offset(16);
        make.left.equalTo(self.whiteBgView).offset(16);
        make.right.equalTo(self.whiteBgView).offset(-16);
        make.height.equalTo(@90);
    }];
    
    [self.gameNameTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.bannerImageView.mas_bottom).offset(16);
        make.left.equalTo(self.whiteBgView).offset(16);
    }];
    
    [self.gameNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.gameNameTitleLabel);
        make.right.equalTo(self.whiteBgView).offset(-16);
    }];
    
    [self.codeTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.gameNameTitleLabel.mas_bottom).offset(16);
        make.left.equalTo(self.whiteBgView).offset(16);
    }];
    
    [self.pasteCodeButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.codeTitleLabel);
        make.right.equalTo(self.whiteBgView).offset(-16);
    }];
    
    [self.codeNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.codeTitleLabel);
        make.right.equalTo(self.pasteCodeButton.mas_left).offset(-10);
    }];
    
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.codeTitleLabel.mas_bottom).offset(16);
        make.left.equalTo(self.whiteBgView).offset(16);
        make.right.equalTo(self.whiteBgView).offset(-16);
        make.height.equalTo(@1);
    }];
    
    [self.explainTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.whiteBgView).offset(16);
        make.right.equalTo(self.whiteBgView).offset(-16);
        make.top.equalTo(self.lineView.mas_bottom).offset(16);
    }];
    
    [self.explainNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.whiteBgView).offset(16);
        make.right.equalTo(self.whiteBgView).offset(-16);
        make.top.equalTo(self.explainTitleLabel.mas_bottom).offset(8);
    }];
    
    [self.attentionTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.whiteBgView).offset(16);
        make.right.equalTo(self.whiteBgView).offset(-16);
        make.top.equalTo(self.explainNameLabel.mas_bottom).offset(16);
    }];
    
    [self.attentionNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.whiteBgView).offset(16);
        make.right.equalTo(self.whiteBgView).offset(-16);
        make.top.equalTo(self.attentionTitleLabel.mas_bottom).offset(8);
        make.bottom.equalTo(self.whiteBgView).offset(-16);
    }];
}

#pragma mark - Lazy Loading
- (UIView *)whiteBgView {
    if (!_whiteBgView) {
        _whiteBgView = [UIView new];
        _whiteBgView.backgroundColor = [UIColor whiteColor];
        _whiteBgView.layer.cornerRadius = 8;
    }
    return _whiteBgView;
}

- (UIImageView *)bannerImageView{
    if (!_bannerImageView) {
        _bannerImageView = [UIImageView new];
        _bannerImageView.layer.cornerRadius = 4;
        _bannerImageView.backgroundColor = [UIColor redColor];
    }
    return _bannerImageView;
}

- (UILabel *)gameNameTitleLabel {
    if (!_gameNameTitleLabel) {
        _gameNameTitleLabel = [UILabel new];
        _gameNameTitleLabel.font = kFont(14);
        _gameNameTitleLabel.textColor = UIColorFromRGB(0x16191C);
        _gameNameTitleLabel.text = @"游戏名称:";
    }
    return _gameNameTitleLabel;
}

- (UILabel *)gameNameLabel {
    if (!_gameNameLabel) {
        _gameNameLabel = [UILabel new];
        _gameNameLabel.font = kFont(14);
        _gameNameLabel.textColor = UIColorFromRGB(0x16191C);
        _gameNameLabel.text = @"天使之战";
    }
    return _gameNameLabel;
}


- (UILabel *)codeTitleLabel {
    if (!_codeTitleLabel) {
        _codeTitleLabel = [UILabel new];
        _codeTitleLabel.font = kFont(14);
        _codeTitleLabel.textColor = UIColorFromRGB(0x16191C);
        _codeTitleLabel.text = @"兑换码:";
    }
    return _codeTitleLabel;
}

- (UILabel *)codeNameLabel {
    if (!_codeNameLabel) {
        _codeNameLabel = [UILabel new];
        _codeNameLabel.font = kFont(14);
        _codeNameLabel.textColor = UIColorFromRGB(0x16191C);
        _codeNameLabel.text = @"06693HD53YQ";
    }
    return _codeNameLabel;
}

- (UIButton *)pasteCodeButton{
    if (!_pasteCodeButton) {
        _pasteCodeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_pasteCodeButton setImage:[UIImage imageNamed:@"mall_home_pasteicon"] forState:UIControlStateNormal];
        [_pasteCodeButton addTarget:self action:@selector(pasteCode) forControlEvents:UIControlEventTouchUpInside];
    }
    return _pasteCodeButton;
}

- (UIView *)lineView{
    if (!_lineView) {
        _lineView = [UIView new];
        _lineView.backgroundColor = UIColorFromRGB(0xEBEDF0);
    }
    return _lineView;
}

- (UILabel *)explainTitleLabel {
    if (!_explainTitleLabel) {
        _explainTitleLabel = [UILabel new];
        _explainTitleLabel.font = kFont(12);
        _explainTitleLabel.textColor = UIColorFromRGB(0x16191C);
        _explainTitleLabel.text = @"使用说明:";
    }
    return _explainTitleLabel;
}

- (UILabel *)explainNameLabel {
    if (!_explainNameLabel) {
        _explainNameLabel = [UILabel new];
        _explainNameLabel.font = kFont(12);
        _explainNameLabel.textColor = UIColorFromRGB(0x919599);
        _explainNameLabel.text = @"进入游戏内，输入卡号和兑换码进行兑换";
    }
    return _explainNameLabel;
}

- (UILabel *)attentionTitleLabel {
    if (!_attentionTitleLabel) {
        _attentionTitleLabel = [UILabel new];
        _attentionTitleLabel.font = kFont(12);
        _attentionTitleLabel.textColor = UIColorFromRGB(0x16191C);
        _attentionTitleLabel.text = @"注意:";
    }
    return _attentionTitleLabel;
}

- (UILabel *)attentionNameLabel {
    if (!_attentionNameLabel) {
        _attentionNameLabel = [UILabel new];
        _attentionNameLabel.font = kFont(12);
        _attentionNameLabel.textColor = UIColorFromRGB(0x919599);
        _attentionNameLabel.text = @"本卡最终解释权归盛旭奇迹盒子所有";
    }
    return _attentionNameLabel;
}

/**
 * @author: zjr
 * @date: 2022-6-30
 * @desc: 数据赋值
 */
//- (void)setModel:(QJTaskCenterCommonDetailModel *)model{
    
//}

/**
 * @author: zjr
 * @date: 2022-8-26
 * @desc: button点击事件
 */
- (void)pasteCode{
    UIPasteboard *pab = [UIPasteboard generalPasteboard];
    pab.string = @"222222222";
    if (pab == nil) {
        [QJAppTool showToast:@"复制失败"];
    } else {
        [QJAppTool showToast:@"复制成功"];
    }
}

@end
