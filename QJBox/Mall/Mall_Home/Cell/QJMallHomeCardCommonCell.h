//
//  QJMallHomeCardCommonCell.h
//  QJBox
//
//  Created by rui on 2022/9/21.
//

#import <UIKit/UIKit.h>
#import "QJMallShopCardDetailModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJMallHomeCardCommonCell : UITableViewCell

@property (nonatomic, strong) QJMallShopCardItemDetailModel *model;

@end

NS_ASSUME_NONNULL_END
