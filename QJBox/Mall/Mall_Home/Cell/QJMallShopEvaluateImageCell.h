//
//  QJMallShopEvaluateImageCell.h
//  QJBox
//
//  Created by rui on 2022/8/25.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJMallShopEvaluateImageCell : UITableViewCell

@property (nonatomic, copy) void(^btnClick)(NSMutableArray *dataArray);

@property (nonatomic, copy) void(^reloadHeightClick)(void);

@end

NS_ASSUME_NONNULL_END
