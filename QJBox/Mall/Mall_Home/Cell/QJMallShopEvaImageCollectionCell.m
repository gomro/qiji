//
//  QJMallShopEvaImageCollectionCell.m
//  QJBox
//
//  Created by rui on 2022/8/25.
//

#import "QJMallShopEvaImageCollectionCell.h"

@interface QJMallShopEvaImageCollectionCell ()

/* 白色底图 */
@property (nonatomic, strong) UIView *whiteView;
/* 图片 */
@property (nonatomic, strong) UIImageView *photoImageView;
/* icon */
@property (nonatomic, strong) UIImageView *cameraImageView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIButton *closeButton;
/* 播放icon */
@property (nonatomic, strong) UIImageView *playImageView;

@end

@implementation QJMallShopEvaImageCollectionCell

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        /* 初始化UI */
        [self initUI];
        /* 添加约束 */
        [self makeConstraints];
    }
    return self;
}

/**
 * @author: zjr
 * @date: 2022-6-28
 * @desc: 初始化UI
 */
- (void)initUI{
    [self.contentView addSubview:self.whiteView];
    [self.whiteView addSubview:self.photoImageView];
    [self.whiteView addSubview:self.cameraImageView];
    [self.whiteView addSubview:self.titleLabel];
    [self.whiteView addSubview:self.playImageView];
    [self.whiteView addSubview:self.closeButton];
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    UIView *view = [super hitTest:point withEvent:event];
//    if (view == self) {
//        return nil;
//    }
    return view;
}

- (void)setIsChooseVideo:(BOOL)isChooseVideo{
    _isChooseVideo = isChooseVideo;
    if (_isChooseVideo) {
        _titleLabel.text = @"添加图片";
    }else{
        _titleLabel.text = @"添加图片/视频";
    }
}

- (void)setModel:(TZAssetModel *)model{
    _model = model;
    self.closeButton.hidden = YES;
    self.cameraImageView.hidden = NO;
    self.titleLabel.hidden = NO;
    self.photoImageView.hidden = YES;
    self.playImageView.hidden = YES;

    [self.whiteView layoutIfNeeded];
    [self.whiteView.layer.sublayers enumerateObjectsUsingBlock:^(__kindof CALayer * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:[CAShapeLayer class]]) {
            [obj removeFromSuperlayer];
        }
    }];
    [self.whiteView.layer addSublayer:[self newLayer:UIColorFromRGB(0x919599) fillColor:[UIColor clearColor] rect:self.whiteView.bounds width:1.0 pattern:@[@(5)] radius:8]];
    
    if (_model.coverImage) {
        self.closeButton.hidden = NO;

        [self.whiteView.layer.sublayers enumerateObjectsUsingBlock:^(__kindof CALayer * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([obj isKindOfClass:[CAShapeLayer class]]) {
                [obj removeFromSuperlayer];
            }
        }];
        self.cameraImageView.hidden = YES;
        self.titleLabel.hidden = YES;
        
        self.photoImageView.hidden = NO;
        self.photoImageView.image = _model.coverImage;
    }
    
    if (_model.isVideo) {
        self.playImageView.hidden = NO;
    }
    
    [_closeButton layoutIfNeeded];
    [_closeButton setEnlargeEdge:10];
}

/**
 * @author: zjr
 * @date: 2022-7-1
 * @desc: 添加约束
 */
- (void)makeConstraints{
    [self.whiteView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.contentView);
    }];
    
    [self.photoImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.bottom.equalTo(self.whiteView);
    }];
    
    [self.cameraImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.whiteView);
        make.centerY.equalTo(self.whiteView).offset(-10);
    }];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.whiteView);
        make.top.equalTo(self.cameraImageView.mas_bottom).offset(10);
    }];
    
    [self.closeButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.whiteView).offset(-12);
        make.right.equalTo(self.whiteView).offset(12);
        make.height.mas_equalTo(24);
        make.width.mas_equalTo(24);
    }];
    
    [self.playImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self.whiteView);
    }];
}

#pragma mark - Lazy Loading
- (UIImageView *)playImageView{
    if (!_playImageView) {
        _playImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"qj_play_normal_24"]];
    }
    return _playImageView;
}

- (UIView *)whiteView {
    if (!_whiteView) {
        _whiteView = [UIView new];
        _whiteView.backgroundColor = [UIColor whiteColor];
    }
    return _whiteView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.font = kFont(14);
        _titleLabel.text = @"添加图片/视频";
        _titleLabel.textColor = UIColorFromRGB(0x919599);
    }
    return _titleLabel;
}

- (UIImageView *)photoImageView {
    if (!_photoImageView) {
        _photoImageView = [UIImageView new];
        _photoImageView.contentMode = UIViewContentModeScaleAspectFill;
        _photoImageView.layer.cornerRadius = 8;
        _photoImageView.layer.masksToBounds = YES;
    }
    return _photoImageView;
}

- (UIImageView *)cameraImageView {
    if (!_cameraImageView) {
        _cameraImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mall_home_cameraicon"]];
    }
    return _cameraImageView;
}

- (UIButton *)closeButton {
    if (!_closeButton) {
        _closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _closeButton.frame = CGRectMake(0, 0, 24, 24);
        [_closeButton setBackgroundImage:[UIImage imageNamed:@"mall_home_closeicon"] forState:UIControlStateNormal];
        [_closeButton addTarget:self action:@selector(closeButtonClick) forControlEvents:UIControlEventTouchUpInside];
        [_closeButton setEnlargeEdge:10];
    }
    return _closeButton;
}

- (UICollectionViewLayoutAttributes *)preferredLayoutAttributesFittingAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes {
    // 这两个不能少
    [self setNeedsLayout];
    [self layoutIfNeeded];
    UICollectionViewLayoutAttributes * attributes = [super preferredLayoutAttributesFittingAttributes:layoutAttributes];
    CGRect cellFrame = attributes.frame;
    cellFrame.size.width = (QJScreenWidth - 32*2 - 30)/3;
    cellFrame.size.height = (QJScreenWidth - 32*2 - 30)/3;
    attributes.frame = cellFrame;
    return attributes;
}

- (void)closeButtonClick{
    DLog(@"删除按钮点击");
    if (self.closeBtnClick) {
        self.closeBtnClick();
    }
}

@end
