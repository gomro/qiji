//
//  QJMallHomeGiftCell.m
//  QJBox
//
//  Created by rui on 2022/8/24.
// mall_home_giftcardbg

#import "QJMallHomeGiftCell.h"

@interface QJMallHomeGiftCell ()

//白色底图
@property (nonatomic, strong) UIView *whiteBgView;
/* 卡券说明 */
@property (nonatomic, strong) UIImageView *bannerImageView;
@property (nonatomic, strong) UILabel *moneyTitleLabel;
@property (nonatomic, strong) UILabel *moneyNameLabel;
@property (nonatomic, strong) UILabel *moneyTypeLabel;
@property (nonatomic, strong) UILabel *timeLabel;
/* 使用说明 注意事项 */
@property (nonatomic, strong) UIView *lineView;
@property (nonatomic, strong) UILabel *explainTitleLabel;
@property (nonatomic, strong) UILabel *explainNameLabel;
@property (nonatomic, strong) UILabel *attentionTitleLabel;
@property (nonatomic, strong) UILabel *attentionNameLabel;
@property (nonatomic, strong) UIButton *receiveButton;

@end

@implementation QJMallHomeGiftCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor colorWithHexString:@"0xFFFFFF"];
        self.contentView.backgroundColor = [UIColor colorWithHexString:@"0xFFFFFF"];

        /* 初始化UI */
        [self initUI];
        /* 添加约束 */
        [self makeConstraints];
    }
    return self;
}

/**
 * @author: zjr
 * @date: 2022-6-27
 * @desc: 初始化UI
 */
- (void)initUI{
    [self.contentView addSubview:self.whiteBgView];
    [self.whiteBgView addSubview:self.bannerImageView];
    
    [self.bannerImageView addSubview:self.moneyTitleLabel];
    [self.bannerImageView addSubview:self.moneyNameLabel];
    [self.bannerImageView addSubview:self.moneyTypeLabel];
    [self.bannerImageView addSubview:self.timeLabel];
    
    [self.whiteBgView addSubview:self.lineView];
    [self.whiteBgView addSubview:self.explainTitleLabel];
    [self.whiteBgView addSubview:self.explainNameLabel];
    [self.whiteBgView addSubview:self.attentionTitleLabel];
    [self.whiteBgView addSubview:self.attentionNameLabel];
    [self.whiteBgView addSubview:self.receiveButton];
}

/**
 * @author: zjr
 * @date: 2022-7-1
 * @desc: 添加约束
 */
- (void)makeConstraints{
    [self.whiteBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(16);
        make.right.equalTo(self.contentView).offset(-16);
        make.top.bottom.equalTo(self.contentView);
    }];
    
    [self.bannerImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.whiteBgView).offset(16);
        make.left.equalTo(self.whiteBgView).offset(16);
        make.right.equalTo(self.whiteBgView).offset(-16);
        make.height.equalTo(@90);
    }];
    
    [self.moneyTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.bannerImageView).offset(10);
        make.left.equalTo(self.bannerImageView).offset(16);
    }];
    
    [self.moneyNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.moneyTitleLabel).offset(-2);
        make.left.equalTo(self.moneyTitleLabel.mas_right).offset(9);
    }];
    
    [self.moneyTypeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.bannerImageView).offset(14);
        make.right.equalTo(self.bannerImageView).offset(-16);
        make.width.equalTo(@50);
        make.height.equalTo(@20);
    }];
    
    [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.bannerImageView).offset(-14);
        make.right.equalTo(self.bannerImageView).offset(-16);
    }];
    
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.bannerImageView.mas_bottom).offset(16);
        make.left.equalTo(self.whiteBgView).offset(16);
        make.right.equalTo(self.whiteBgView).offset(-16);
        make.height.equalTo(@1);
    }];
    
    [self.explainTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.whiteBgView).offset(16);
        make.right.equalTo(self.whiteBgView).offset(-16);
        make.top.equalTo(self.lineView.mas_bottom).offset(16);
    }];
    
    [self.explainNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.whiteBgView).offset(16);
        make.right.equalTo(self.whiteBgView).offset(-16);
        make.top.equalTo(self.explainTitleLabel.mas_bottom).offset(8);
    }];
    
    [self.attentionTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.whiteBgView).offset(16);
        make.right.equalTo(self.whiteBgView).offset(-16);
        make.top.equalTo(self.explainNameLabel.mas_bottom).offset(16);
    }];
    
    [self.attentionNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.whiteBgView).offset(16);
        make.right.equalTo(self.whiteBgView).offset(-16);
        make.top.equalTo(self.attentionTitleLabel.mas_bottom).offset(8);
    }];
    
    [self.receiveButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.whiteBgView).offset(16);
        make.right.equalTo(self.whiteBgView).offset(-16);
        make.height.equalTo(@37);
        make.top.equalTo(self.attentionNameLabel.mas_bottom).offset(16);
        make.bottom.equalTo(self.whiteBgView).offset(-16);
    }];
    
    [self.moneyTitleLabel layoutIfNeeded];
    [self.moneyTitleLabel setSingleStr:@"¥ 100" range:[@"¥ 100" rangeOfString:@"100"] font:kFont(24)];
}

#pragma mark - Lazy Loading
- (UIView *)whiteBgView {
    if (!_whiteBgView) {
        _whiteBgView = [UIView new];
        _whiteBgView.backgroundColor = [UIColor whiteColor];
        _whiteBgView.layer.cornerRadius = 8;
    }
    return _whiteBgView;
}

- (UIImageView *)bannerImageView{
    if (!_bannerImageView) {
        _bannerImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mall_home_giftcardbg"]];
    }
    return _bannerImageView;
}

- (UILabel *)moneyTitleLabel {
    if (!_moneyTitleLabel) {
        _moneyTitleLabel = [UILabel new];
        _moneyTitleLabel.font = kFont(14);
        _moneyTitleLabel.textColor = UIColorFromRGB(0x7B4C16);
        _moneyTitleLabel.text = @"¥ 100";
    }
    return _moneyTitleLabel;
}

- (UILabel *)moneyNameLabel {
    if (!_moneyNameLabel) {
        _moneyNameLabel = [UILabel new];
        _moneyNameLabel.font = kFont(12);
        _moneyNameLabel.textColor = UIColorFromRGB(0x7B4C16);
        _moneyNameLabel.text = @"现金券";
    }
    return _moneyNameLabel;
}

- (UILabel *)moneyTypeLabel {
    if (!_moneyTypeLabel) {
        _moneyTypeLabel = [UILabel new];
        _moneyTypeLabel.font = kFont(10);
        _moneyTypeLabel.textColor = UIColorFromRGB(0xFFFFFF);
        _moneyTypeLabel.backgroundColor = UIColorFromRGB(0xFF9500);
        _moneyTypeLabel.text = @"抵扣券";
        _moneyTypeLabel.textAlignment = NSTextAlignmentCenter;
        _moneyTypeLabel.layer.cornerRadius = 8;
        _moneyTypeLabel.layer.masksToBounds = YES;
    }
    return _moneyTypeLabel;
}

- (UILabel *)timeLabel {
    if (!_timeLabel) {
        _timeLabel = [UILabel new];
        _timeLabel.font = kFont(10);
        _timeLabel.textColor = UIColorFromRGB(0x7B4C16);
        _timeLabel.text = @"有效期至 2021.12.31";
    }
    return _timeLabel;
}

- (UIView *)lineView{
    if (!_lineView) {
        _lineView = [UIView new];
        _lineView.backgroundColor = UIColorFromRGB(0xEBEDF0);
    }
    return _lineView;
}

- (UILabel *)explainTitleLabel {
    if (!_explainTitleLabel) {
        _explainTitleLabel = [UILabel new];
        _explainTitleLabel.font = kFont(12);
        _explainTitleLabel.textColor = UIColorFromRGB(0x16191C);
        _explainTitleLabel.text = @"使用说明:";
    }
    return _explainTitleLabel;
}

- (UILabel *)explainNameLabel {
    if (!_explainNameLabel) {
        _explainNameLabel = [UILabel new];
        _explainNameLabel.font = kFont(12);
        _explainNameLabel.textColor = UIColorFromRGB(0x919599);
        _explainNameLabel.text = @"进入游戏内，输入卡号和兑换码进行兑换";
    }
    return _explainNameLabel;
}

- (UILabel *)attentionTitleLabel {
    if (!_attentionTitleLabel) {
        _attentionTitleLabel = [UILabel new];
        _attentionTitleLabel.font = kFont(12);
        _attentionTitleLabel.textColor = UIColorFromRGB(0x16191C);
        _attentionTitleLabel.text = @"注意:";
    }
    return _attentionTitleLabel;
}

- (UILabel *)attentionNameLabel {
    if (!_attentionNameLabel) {
        _attentionNameLabel = [UILabel new];
        _attentionNameLabel.font = kFont(12);
        _attentionNameLabel.textColor = UIColorFromRGB(0x919599);
        _attentionNameLabel.text = @"本卡最终解释权归盛旭奇迹盒子所有";
    }
    return _attentionNameLabel;
}

- (UIButton *)receiveButton{
    if (!_receiveButton) {
        _receiveButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_receiveButton setTitle:@"领取" forState:UIControlStateNormal];
        [_receiveButton setTitleColor:UIColorFromRGB(0xFEFEFE) forState:UIControlStateNormal];
        _receiveButton.backgroundColor = UIColorFromRGB(0xFF9500);
        _receiveButton.titleLabel.font = kFont(14);
        _receiveButton.layer.cornerRadius = 17;
        [_receiveButton addTarget:self action:@selector(receiveButtonClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _receiveButton;
}

/**
 * @author: zjr
 * @date: 2022-6-30
 * @desc: 数据赋值
 */
//- (void)setModel:(QJTaskCenterCommonDetailModel *)model{
    
//}

/**
 * @author: zjr
 * @date: 2022-8-26
 * @desc: button点击事件
 */
- (void)receiveButtonClick{
    DLog(@"去完成");
}

@end
