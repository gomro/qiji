//
//  QJMallShopEvaluateContentCell.m
//  QJBox
//
//  Created by rui on 2022/8/25.
//

#import "QJMallShopEvaluateContentCell.h"

@interface QJMallShopEvaluateContentCell ()<UITextViewDelegate>

//白色底图
@property (nonatomic, strong) UIView *whiteBgView;
/* 输入内容textView */
@property (nonatomic, strong) UITextView *textView;
/* 数量提示 */
@property (nonatomic, strong) UILabel *numLabel;
/* 默认文案 */
@property (nonatomic, strong) UILabel *placeholderLabel;

@end

@implementation QJMallShopEvaluateContentCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor colorWithHexString:@"0xFFFFFF"];
        self.contentView.backgroundColor = [UIColor colorWithHexString:@"0xFFFFFF"];

        /* 初始化UI */
        [self initUI];
        /* 添加约束 */
        [self makeConstraints];
    }
    return self;
}

/**
 * @author: zjr
 * @date: 2022-6-27
 * @desc: 初始化UI
 */
- (void)initUI{
    [self.contentView addSubview:self.whiteBgView];
    [self.whiteBgView addSubview:self.textView];
    [self.whiteBgView addSubview:self.numLabel];
    [self.textView addSubview:self.placeholderLabel];
}

/**
 * @author: zjr
 * @date: 2022-7-1
 * @desc: 添加约束
 */
- (void)makeConstraints{
    [self.whiteBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(16);
        make.right.equalTo(self.contentView).offset(-16);
        make.top.bottom.equalTo(self.contentView);
    }];
    
    [self.textView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.whiteBgView);//.offset(5);
        make.right.equalTo(self.whiteBgView);//.offset(-5);
        make.height.mas_equalTo(300*kWScale);
        make.top.bottom.equalTo(self.whiteBgView);
    }];
    
    [self.numLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.textView.mas_right).mas_offset(-10);
        make.bottom.mas_equalTo(self.textView.mas_bottom).mas_offset(-10);
    }];
    
    [self.placeholderLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(10);
        make.left.mas_equalTo(15);
    }];
}

#pragma mark - Lazy Loading
- (UIView *)whiteBgView {
    if (!_whiteBgView) {
        _whiteBgView = [UIView new];
        _whiteBgView.backgroundColor = [UIColor whiteColor];
    }
    return _whiteBgView;
}

- (UITextView *)textView {
    if (!_textView) {
        _textView = [[UITextView alloc] initWithFrame:CGRectMake(16, 100, kScreenWidth - 32, 200*kWScale)];
        _textView.backgroundColor = [UIColor whiteColor];
        _textView.textContainerInset = UIEdgeInsetsMake(10, 10, 10, 10);
        _textView.textColor = UIColorFromRGB(0x16191c);
        _textView.font = FontRegular(16);
        _textView.delegate = self;
    }
    return _textView;
}

- (UILabel *)placeholderLabel {
    if (!_placeholderLabel) {
        _placeholderLabel = [[UILabel alloc] init];
        _placeholderLabel.text = @"分享您对商品的评价";
        _placeholderLabel.textColor = UIColorFromRGB(0x919599);
        _placeholderLabel.font = FontRegular(14);
    }
    return _placeholderLabel;
}

- (UILabel *)numLabel {
    if (!_numLabel) {
        _numLabel = [[UILabel alloc] init];
        _numLabel.textColor = UIColorFromRGB(0xC8CACC);
        _numLabel.font = FontRegular(14);
        _numLabel.text = @"0/500";
    }
    return _numLabel;
}

#pragma mark - UITextView
- (void)textViewDidChangeSelection:(UITextView *)textView {
    if ([textView.text isEqualToString:@""]){
        [self.placeholderLabel setHidden:NO];
    }else{
        [self.placeholderLabel setHidden:YES];
    }
}

-(void)textViewDidChange:(UITextView *)textView {
    UITextRange *selectedRange = [textView markedTextRange];
    //获取高亮部分
    UITextPosition *pos = [textView positionFromPosition:selectedRange.start offset:0];
   //如果在变化中是高亮部分在变，就不要计算字符了
    if (selectedRange && pos) {
        return;
    }
    if (textView.text.length > 500) {
        textView.text = [textView.text substringToIndex:500];
    }
    
    if (self.btnClick) {
        self.btnClick(textView.text);
    }

    self.numLabel.text = [NSString stringWithFormat:@"%ld/500",textView.text.length];
}

@end
