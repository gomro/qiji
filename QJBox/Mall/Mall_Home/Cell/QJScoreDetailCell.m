//
//  QJScoreDetailCell.m
//  QJBox
//
//  Created by rui on 2022/6/28.
//

#import "QJScoreDetailCell.h"

@interface QJScoreDetailCell ()

//标题
@property (nonatomic, strong) UILabel *titleLabel;
/* icon */
@property (nonatomic, strong) UIImageView *iconImageView;
/* 任务 */
@property (nonatomic, strong) UILabel *subTitleLabel;
//任务分数
@property (nonatomic, strong) UILabel *scoreLabel;
//获取时间
@property (nonatomic, strong) UILabel *timeLabel;
@property (nonatomic, strong) UIView *lineView;

@end

@implementation QJScoreDetailCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        /* 初始化UI */
        [self initUI];
        /* 添加约束 */
        [self makeConstraints];
    }
    return self;
}

/**
 * @author: zjr
 * @date: 2022-6-27
 * @desc: 初始化UI
 */
- (void)initUI{
    self.contentView.backgroundColor = [UIColor colorWithHexString:@"0xFFFFFF"];
    [self.contentView addSubview:self.titleLabel];
    [self.contentView addSubview:self.iconImageView];
    [self.contentView addSubview:self.subTitleLabel];
    [self.contentView addSubview:self.scoreLabel];
    [self.contentView addSubview:self.timeLabel];
    [self.contentView addSubview:self.lineView];
}

/**
 * @author: zjr
 * @date: 2022-7-1
 * @desc: 添加约束
 */
- (void)makeConstraints{
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(16);
        make.left.equalTo(self.contentView).offset(16);
    }];

    [self.iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).offset(8);
        make.left.equalTo(self.contentView).offset(16);
        make.height.equalTo(@(36*kWScale));
        make.width.equalTo(@(36*kWScale));
    }];
    
    [self.subTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.iconImageView);
        make.left.equalTo(self.iconImageView.mas_right).offset(16);
    }];
    
    [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.iconImageView.mas_bottom);
        make.left.equalTo(self.iconImageView.mas_right).offset(16);
    }];
    
    [self.scoreLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.iconImageView);
        make.right.equalTo(self.contentView).offset(-16);
    }];
    
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(16);
        make.right.equalTo(self.contentView).offset(-16);
        make.bottom.equalTo(self.contentView);
        make.height.equalTo(@1);
    }];
}

#pragma mark - Lazy Loading
- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.font = kFont(12);
        _titleLabel.textColor = UIColorFromRGB(0x919599);
        _titleLabel.text = @"SXXN2022072276876";
    }
    return _titleLabel;
}

- (UIImageView *)iconImageView{
    if (!_iconImageView) {
        _iconImageView = [UIImageView new];
        _iconImageView.layer.cornerRadius = 10;
        _iconImageView.layer.masksToBounds = YES;
        _iconImageView.contentMode = UIViewContentModeScaleAspectFill;
        _iconImageView.backgroundColor = [UIColor redColor];
    }
    return _iconImageView;
}

- (UILabel *)subTitleLabel {
    if (!_subTitleLabel) {
        _subTitleLabel = [UILabel new];
        _subTitleLabel.font = kFont(14);
        _subTitleLabel.textColor = UIColorFromRGB(0x919599);
        _subTitleLabel.text = @"魔法光药水";
    }
    return _subTitleLabel;
}

- (UILabel *)scoreLabel {
    if (!_scoreLabel) {
        _scoreLabel = [UILabel new];
        _scoreLabel.font = kFont(14);
        _scoreLabel.textColor = UIColorFromRGB(0xFF9500);
        _scoreLabel.text = @"+10";
    }
    return _scoreLabel;
}

- (UILabel *)timeLabel {
    if (!_timeLabel) {
        _timeLabel = [UILabel new];
        _timeLabel.font = kFont(12);
        _timeLabel.textColor = UIColorFromRGB(0x474849);
        _timeLabel.text = @"2022-07-01 19:54";
    }
    return _timeLabel;
}

- (UIView *)lineView{
    if (!_lineView) {
        _lineView = [UIView new];
        _lineView.backgroundColor = UIColorFromRGB(0xEBEDF0);
    }
    return _lineView;
}

/**
 * @author: zjr
 * @date: 2022-6-30
 * @desc: 数据赋值
 */
- (void)setModel:(QJMallShopHomeDetailModel *)model{
    _model = model;
    QJMallShopHomeDetailModel *scoreModel = [_model.items safeObjectAtIndex:0];
    [self.iconImageView setImageWithURL:[NSURL URLWithString:[scoreModel.picture checkImageUrlString]] placeholder:[UIImage imageNamed:@"mine_head"]];
    _titleLabel.text = kCheckStringNil(_model.sn);
    _subTitleLabel.text = kCheckStringNil(scoreModel.name);
    _timeLabel.text = kCheckStringNil(_model.createTime);
    _scoreLabel.text = [NSString stringWithFormat:@"- %@",kCheckStringNil(scoreModel.qjcoinPartPrice)];
}

@end
