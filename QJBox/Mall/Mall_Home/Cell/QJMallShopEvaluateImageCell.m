//
//  QJMallShopEvaluateImageCell.m
//  QJBox
//
//  Created by rui on 2022/8/25.
//

#import "QJMallShopEvaluateImageCell.h"
/* 自定义collectioncell */
#import "QJMallShopEvaNormalCollectionCell.h"
#import "QJMallShopEvaImageCollectionCell.h"

/* 图片选择器 */
#import "TZImagePickerController.h"
#import "HZPhotoBrowser.h"

@interface QJMallShopEvaluateImageCell ()<UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource,HZPhotoBrowserDelegate>

//白色底图
@property (nonatomic, strong) UIView *whiteBgView;
/* 是否选择视频 */
@property (nonatomic, assign) BOOL isChooseVideo;
/* 内容填充collection */
@property (nonatomic, strong) UICollectionView *collectionView;
/* 选中的数据源 */
@property (nonatomic, strong) NSMutableArray *dataArray;
/* 选中的asset数据 */
@property (nonatomic, strong) NSMutableArray *assetArray;

@property (nonatomic, strong) NSMutableArray *modelArray;

@end

@implementation QJMallShopEvaluateImageCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor colorWithHexString:@"0xFFFFFF"];
        self.contentView.backgroundColor = [UIColor colorWithHexString:@"0xFFFFFF"];

        self.isChooseVideo = NO;
        self.dataArray = [NSMutableArray array];
        self.assetArray = [NSMutableArray array];
        self.modelArray = [NSMutableArray array];
        /* 初始化UI */
        [self initUI];
        /* 添加约束 */
        [self makeConstraints];
    }
    return self;
}

/**
 * @author: zjr
 * @date: 2022-6-27
 * @desc: 初始化UI
 */
- (void)initUI{
    [self.contentView addSubview:self.whiteBgView];
    [self.whiteBgView addSubview:self.collectionView];
}

-(void)layoutSubviews {
    [super layoutSubviews];
    [self.whiteBgView layoutIfNeeded];
    [self.whiteBgView showCorner:8 rectCorner:UIRectCornerBottomLeft | UIRectCornerBottomRight];
}

/**
 * @author: zjr
 * @date: 2022-7-1
 * @desc: 添加约束
 */
- (void)makeConstraints{
    [self.whiteBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(16);
        make.right.equalTo(self.contentView).offset(-16);
        make.top.bottom.equalTo(self.contentView);
    }];
    
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.whiteBgView);//.offset(16);
        make.right.equalTo(self.whiteBgView);//.offset(-16);
        make.top.equalTo(self.whiteBgView);
        make.bottom.equalTo(self.whiteBgView).offset(-24);
    }];
}

#pragma mark - Lazy Loading
- (UIView *)whiteBgView {
    if (!_whiteBgView) {
        _whiteBgView = [UIView new];
        _whiteBgView.backgroundColor = [UIColor whiteColor];
    }
    return _whiteBgView;
}

- (UICollectionView *)collectionView{
    if (!_collectionView) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        layout.minimumLineSpacing = 10;
        layout.minimumInteritemSpacing = 10;
        layout.sectionInset = UIEdgeInsetsMake(10, 16, 10, 16);
        layout.estimatedItemSize = CGSizeMake((QJScreenWidth - 32*2 - 30)/3, (QJScreenWidth - 32*2 - 30)/3);
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, NavigationBar_Bottom_Y, QJScreenWidth, QJScreenHeight-NavigationBar_Bottom_Y) collectionViewLayout:layout];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.scrollEnabled = NO;
        _collectionView.backgroundColor = UIColorFromRGB(0xFFFFFF);
        _collectionView.showsVerticalScrollIndicator = NO;
        [_collectionView registerClass:[QJMallShopEvaNormalCollectionCell class] forCellWithReuseIdentifier:@"QJMallShopEvaNormalCollectionCell"];
        [_collectionView registerClass:[QJMallShopEvaImageCollectionCell class] forCellWithReuseIdentifier:@"QJMallShopEvaImageCollectionCell"];
    }
    return _collectionView;
}

- (BOOL)checkChooseVideo{
    self.isChooseVideo = NO;
    [self.modelArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        TZAssetModel *model = (TZAssetModel *)obj;
        if (model.isVideo) {
            self.isChooseVideo = YES;
        }
    }];
    return self.isChooseVideo;
}

- (void)openImageBrower{
    TZImagePickerController *vc = [[TZImagePickerController alloc]initWithMaxImagesCount:6 delegate:nil];
    vc.showSelectedIndex = YES;
    vc.showPhotoCannotSelectLayer = YES;
    vc.maxImagesCount = 6 - self.modelArray.count;
    vc.allowEditVideo = YES;
    vc.allowPickingVideo = ![self checkChooseVideo];
    vc.videoMaximumDuration = 15;//最大拍摄时间
    vc.maxCropVideoDuration = 15;//最大裁剪视频时长
    vc.maxChooseVideoDuration = 15;//最大视频时长限制
    
    vc.didFinishPickingAndEditingVideoHandle = ^(UIImage *coverImage, NSString *outputPath, NSString *errorMsg) {
        TZAssetModel *model = [TZAssetModel new];
        model.isVideo = YES;
        model.coverImage = coverImage;
        model.outputPath = outputPath;
        model.objectKey = [NSString stringWithFormat:@"video/%@.mp4", [NSString getRandomFileName]];
        model.coverObjectKey = [NSString stringWithFormat:@"image/%@.jpg", [NSString getRandomFileName]];
        [self.modelArray addObject:model];
        
        [self reloadCollectionData];
    };
    
    vc.didFinishPickingPhotosHandle = ^(NSArray<UIImage *> *photos, NSArray *assets, BOOL isSelectOriginalPhoto) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [photos enumerateObjectsUsingBlock:^(UIImage * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                TZAssetModel *model = [TZAssetModel new];
                model.isVideo = NO;
                model.coverImage = obj;
                model.objectKey = [NSString stringWithFormat:@"image/%@.jpg", [NSString getRandomFileName]];
                [self.modelArray addObject:model];
            }];
            
            [self reloadCollectionData];
        });
    };
    
    vc.didFinishVideoAndVideoTimeLenthMaxHandle = ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [[UIApplication sharedApplication].keyWindow makeToast:@"视频时长超过15s"];
        });
    };
    
    vc.modalPresentationStyle = UIModalPresentationCustom;
    [[QJAppTool getCurrentViewController] presentViewController:vc animated:YES completion:nil];
}

- (void)reloadCollectionData{
    [self.collectionView reloadData];
    if (self.reloadHeightClick) {
        self.reloadHeightClick();
    }
    if (self.btnClick) {
        self.btnClick(self.modelArray);
    }
}

#pragma mark - UICollectionView
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    NSInteger num = self.modelArray.count;
    NSInteger otherNum = 1;
    return num < 6 ? num + otherNum: num;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (self.modelArray.count == 0) {
        QJMallShopEvaNormalCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"QJMallShopEvaNormalCollectionCell" forIndexPath:indexPath];
        return cell;
    }else{
        QJMallShopEvaImageCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"QJMallShopEvaImageCollectionCell" forIndexPath:indexPath];
        cell.isChooseVideo = [self checkChooseVideo];
        cell.model = [self.modelArray safeObjectAtIndex:indexPath.item];
        WS(weakSelf)
        cell.closeBtnClick = ^{
            [weakSelf.modelArray removeObjectAtIndex:indexPath.item];
            [weakSelf reloadCollectionData];
        };
        return cell;
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (self.modelArray.count == 0) {
        [self openImageBrower];
        return;
    }
    if (indexPath.item == self.modelArray.count) {
        [self openImageBrower];
    }else{
        [self showBrower:indexPath.item];
    }
}

- (CGSize)systemLayoutSizeFittingSize:(CGSize)targetSize withHorizontalFittingPriority:(UILayoutPriority)horizontalFittingPriority verticalFittingPriority:(UILayoutPriority)verticalFittingPriority {
    CGSize size = [super systemLayoutSizeFittingSize:targetSize withHorizontalFittingPriority:horizontalFittingPriority verticalFittingPriority:verticalFittingPriority];
    // 这个不能少，否则 self.coll.collectionViewLayout.collectionViewContentSize.height = 0
    [self.collectionView layoutIfNeeded];
    // 这里得到 UICollectionView 内容的宽高
    CGSize collSize = self.collectionView.collectionViewLayout.collectionViewContentSize;
    return CGSizeMake(size.width, size.height+collSize.height);
}

- (void)showBrower:(NSInteger)index{
//    NSMutableArray *urlArray = [NSMutableArray array];
//    [self.assetArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//        PHAsset *asset = (PHAsset *)obj;
//        [urlArray addObject:asset.localIdentifier];
//    }];
//    HZPhotoBrowser *browser = [[HZPhotoBrowser alloc] init];
//    browser.isSavePhoto = NO;
//    browser.isFullWidthForLandScape = NO;
//    browser.isNeedLandscape = NO;
//    browser.imageUrlHeader = @{};
//    PHAsset *asset = [self.assetArray safeObjectAtIndex:index];
//    DLog(@"phAsset.localIdentifier = %@",asset.localIdentifier);
//    browser.sourceImagesContainerView = self;
//    browser.imageArray = urlArray;
//    browser.imageCount = urlArray.count;
//    browser.currentImageIndex = (int)index;
//    browser.delegate = self;
//    [browser show];
}

//- (UIImage *)photoBrowser:(HZPhotoBrowser *)browser placeholderImageForIndex:(NSInteger)index{
//    return [self.dataArray safeObjectAtIndex:index];
//}

//// 返回临时占位图片（即原来的小图）
//- (UIImage *)photoBrowser:(HZPhotoBrowser *)browser placeholderImageForIndex:(NSInteger)index {
//    return [UIImage new];
//}
//
//// 返回高质量图片的url
//- (NSURL *)photoBrowser:(HZPhotoBrowser *)browser highQualityImageURLForIndex:(NSInteger)index {
//    return [self.dataArray safeObjectAtIndex:index];
//}

@end
