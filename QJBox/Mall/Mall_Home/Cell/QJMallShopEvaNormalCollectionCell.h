//
//  QJMallShopEvaNormalCollectionCell.h
//  QJBox
//
//  Created by rui on 2022/8/25.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJMallShopEvaNormalCollectionCell : UICollectionViewCell

/* 点击回调 */
@property (nonatomic, copy) void(^btnClick)(void);
- (void)updateFrame;

@end

NS_ASSUME_NONNULL_END
