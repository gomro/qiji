//
//  QJMallShopEvaNormalCollectionCell.m
//  QJBox
//
//  Created by rui on 2022/8/25.
//

#import "QJMallShopEvaNormalCollectionCell.h"

@interface QJMallShopEvaNormalCollectionCell ()

/* 白色底图 */
@property (nonatomic, strong) UIView *whiteView;
/* 默认cell */
@property (nonatomic, strong) UIImageView *cameraImageView;
@property (nonatomic, strong) UILabel *titleLabel;

@end

@implementation QJMallShopEvaNormalCollectionCell

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        /* 初始化UI */
        [self initUI];
        /* 添加约束 */
        [self makeConstraints];
    }
    return self;
}

/**
 * @author: zjr
 * @date: 2022-6-28
 * @desc: 初始化UI
 */
- (void)initUI{
    [self.contentView addSubview:self.whiteView];
    [self.whiteView addSubview:self.cameraImageView];
    [self.whiteView addSubview:self.titleLabel];
}

-(void)layoutSubviews {
    [super layoutSubviews];
    [self updateFrame];
}

- (void)updateFrame{
    [self.whiteView layoutIfNeeded];
    [self.whiteView.layer.sublayers enumerateObjectsUsingBlock:^(__kindof CALayer * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:[CAShapeLayer class]]) {
            [obj removeFromSuperlayer];
        }
    }];
    [self.whiteView.layer addSublayer:[self newLayer:UIColorFromRGB(0x919599) fillColor:[UIColor clearColor] rect:self.whiteView.bounds width:1.0 pattern:@[@(5)] radius:8]];
}

/**
 * @author: zjr
 * @date: 2022-7-1
 * @desc: 添加约束
 */
- (void)makeConstraints{
    [self.whiteView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.contentView);
    }];
    [self.cameraImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.whiteView);
        make.centerY.equalTo(self.whiteView).offset(-10);
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.whiteView);
        make.top.equalTo(self.cameraImageView.mas_bottom).offset(10);
    }];
}

#pragma mark - Lazy Loading
- (UIView *)whiteView {
    if (!_whiteView) {
        _whiteView = [UIView new];
        _whiteView.backgroundColor = [UIColor whiteColor];
    }
    return _whiteView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.font = kFont(14);
        _titleLabel.text = @"添加图片/视频";
        _titleLabel.textColor = UIColorFromRGB(0x919599);
    }
    return _titleLabel;
}

- (UIImageView *)cameraImageView {
    if (!_cameraImageView) {
        _cameraImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mall_home_cameraicon"]];
    }
    return _cameraImageView;
}

- (UICollectionViewLayoutAttributes *)preferredLayoutAttributesFittingAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes {
    // 这两个不能少
    [self setNeedsLayout];
    [self layoutIfNeeded];
    UICollectionViewLayoutAttributes * attributes = [super preferredLayoutAttributesFittingAttributes:layoutAttributes];
    CGRect cellFrame = attributes.frame;
    cellFrame.size.width = QJScreenWidth - 16*4;
    cellFrame.size.height = (QJScreenWidth - 32*2 - 30)/3;
    attributes.frame = cellFrame;
    return attributes;
}

@end

