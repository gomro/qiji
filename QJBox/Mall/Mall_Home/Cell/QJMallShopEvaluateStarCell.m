//
//  QJMallShopEvaluateStarCell.m
//  QJBox
//
//  Created by rui on 2022/8/25.
//

#import "QJMallShopEvaluateStarCell.h"
/* 自定义星星 */
#import "QJEvaluateStarView.h"

@interface QJMallShopEvaluateStarCell ()

//白色底图
@property (nonatomic, strong) UIView *whiteBgView;
/* 标题 */
@property (nonatomic, strong) UILabel *titleLabel;
/* 评分 */
@property (nonatomic, strong) QJEvaluateStarView *starView;
@property (nonatomic, strong) UIView *lineView;

@end

@implementation QJMallShopEvaluateStarCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor colorWithHexString:@"0xFFFFFF"];
        self.contentView.backgroundColor = [UIColor colorWithHexString:@"0xFFFFFF"];

        /* 初始化UI */
        [self initUI];
        /* 添加约束 */
        [self makeConstraints];
    }
    return self;
}

/**
 * @author: zjr
 * @date: 2022-6-27
 * @desc: 初始化UI
 */
- (void)initUI{
    [self.contentView addSubview:self.whiteBgView];
    [self.whiteBgView addSubview:self.titleLabel];
    [self.whiteBgView addSubview:self.starView];
    [self.whiteBgView addSubview:self.lineView];
}

/**
 * @author: zjr
 * @date: 2022-7-1
 * @desc: 添加约束
 */
- (void)makeConstraints{
    [self.whiteBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(16);
        make.right.equalTo(self.contentView).offset(-16);
        make.top.bottom.equalTo(self.contentView);
    }];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.whiteBgView).offset(16);
        make.height.equalTo(@(64*kWScale));
        make.top.bottom.equalTo(self.whiteBgView);
    }];
    
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.whiteBgView);
        make.bottom.equalTo(self.whiteBgView);
        make.height.equalTo(@1);
    }];
    
    [self.starView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.titleLabel);
        make.left.equalTo(self.titleLabel.mas_right).offset(5);
        make.size.mas_equalTo(CGSizeMake(168*kWScale,20*kWScale));
    }];
    [self.starView updateWithIndex:5];
    WS(weakSelf)
    self.starView.starClick = ^(NSInteger index) {
        DLog(@"选中了几个星星 = %ld",index);
        if (weakSelf.btnClick) {
            weakSelf.btnClick([NSString stringWithFormat:@"%ld",index+1]);
        }
    };
}

-(void)layoutSubviews {
    [super layoutSubviews];
    [self.whiteBgView layoutIfNeeded];
    [self.whiteBgView showCorner:8 rectCorner:UIRectCornerTopLeft | UIRectCornerTopRight];
}

#pragma mark - Lazy Loading
- (UIView *)whiteBgView {
    if (!_whiteBgView) {
        _whiteBgView = [UIView new];
        _whiteBgView.backgroundColor = [UIColor whiteColor];
    }
    return _whiteBgView;
}

- (UIView *)lineView{
    if (!_lineView) {
        _lineView = [UIView new];
        _lineView.backgroundColor = UIColorFromRGB(0xEBEDF0);
    }
    return _lineView;
}

- (QJEvaluateStarView *)starView{
    if (!_starView) {
        _starView = [[QJEvaluateStarView alloc] initWithFrame:CGRectMake(0, 0, 148*kWScale, 20*kWScale)];
    }
    return _starView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.font = kFont(14);
        _titleLabel.textColor = UIColorFromRGB(0x000000);
        _titleLabel.text = @"商品评分";
    }
    return _titleLabel;
}

@end
