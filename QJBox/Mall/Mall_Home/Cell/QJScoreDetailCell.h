//
//  QJScoreDetailCell.h
//  QJBox
//
//  Created by rui on 2022/6/28.
//

#import "QJBaseTableViewCell.h"
/* model */
#import "QJMallShopHomeListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJScoreDetailCell : QJBaseTableViewCell

@property (nonatomic, strong) QJMallShopHomeDetailModel *model;

@end

NS_ASSUME_NONNULL_END
