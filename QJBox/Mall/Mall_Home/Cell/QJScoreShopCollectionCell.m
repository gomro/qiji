//
//  QJScoreShopCollectionCell.m
//  QJBox
//
//  Created by rui on 2022/6/28.
//

#import "QJScoreShopCollectionCell.h"
#import "QJLabelAttributeModel.h"

@interface QJScoreShopCollectionCell ()

/* 白色视图 */
@property (nonatomic, strong) UIView *whiteView;
/* 图片 */
@property (nonatomic, strong) UIImageView *imageView;
/* 标题 */
@property (nonatomic, strong) UILabel *titleLabel;
/* 兑换积分 */
@property (nonatomic, strong) UILabel *scoreLabel;
/* 剩余数量 */
@property (nonatomic, strong) UILabel *countLabel;

@end

@implementation QJScoreShopCollectionCell

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        /* 初始化UI */
        [self initUI];
        /* 添加约束 */
        [self makeConstraints];
    }
    return self;
}

/**
 * @author: zjr
 * @date: 2022-6-28
 * @desc: 初始化UI
 */
- (void)initUI{
    [self.contentView addSubview:self.whiteView];
    [self.whiteView addSubview:self.imageView];
    [self.whiteView addSubview:self.countLabel];
    [self.whiteView addSubview:self.titleLabel];
    [self.whiteView addSubview:self.scoreLabel];
}

/**
 * @author: zjr
 * @date: 2022-7-1
 * @desc: 添加约束
 */
- (void)makeConstraints{
    [self.whiteView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.contentView);
    }];
    
    [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.whiteView);
        make.left.equalTo(self.whiteView);
        make.right.equalTo(self.whiteView);
        make.size.mas_equalTo(CGSizeMake(self.contentView.width, self.contentView.width));
    }];

    [self.countLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.whiteView);
        make.right.equalTo(self.whiteView);
        make.size.mas_equalTo(CGSizeMake([@"已兑29/100" getWidthWithFont:kFont(8) withHeight:20] + 20, 20));
    }];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.imageView.mas_bottom).offset(8);
        make.left.equalTo(self.whiteView).offset(8);
        make.right.equalTo(self.whiteView).offset(-8);
        make.height.equalTo(@(15*kWScale));
    }];
    
    [self.scoreLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.whiteView).offset(-10);
        make.left.equalTo(self.whiteView).offset(8);
        make.right.equalTo(self.whiteView).offset(-8);
        make.height.equalTo(@(20*kWScale));
    }];
    
    [self.whiteView layoutIfNeeded];
    [self.whiteView showCorner:8 rectCorner:UIRectCornerAllCorners];
}

#pragma mark - Lazy Loading
- (UIView *)whiteView {
    if (!_whiteView) {
        _whiteView = [UIView new];
        _whiteView.backgroundColor = [UIColor whiteColor];
    }
    return _whiteView;
}

- (UIImageView *)imageView {
    if (!_imageView) {
        _imageView = [UIImageView new];
        _imageView.backgroundColor = [UIColor whiteColor];
        _imageView.contentMode = UIViewContentModeScaleAspectFill;
        _imageView.clipsToBounds = YES;
    }
    return _imageView;
}

- (UILabel *)countLabel {
    if (!_countLabel) {
        _countLabel = [UILabel new];
        _countLabel.font = kFont(8);
        _countLabel.textColor = [UIColor colorWithHexString:@"0xFF9500"];
        _countLabel.backgroundColor = [UIColor colorWithHexString:@"0xFFF4E5"];
        _countLabel.textAlignment = NSTextAlignmentCenter;
        _countLabel.text = @"已兑29/100";
    }
    return _countLabel;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.font = kFont(14);
        _titleLabel.textColor = [UIColor colorWithHexString:@"0x000000"];
        _titleLabel.textAlignment = NSTextAlignmentLeft;
        _titleLabel.text = @"高级技能材料-魔法光剑";
    }
    return _titleLabel;
}

- (UILabel *)scoreLabel {
    if (!_scoreLabel) {
        _scoreLabel = [UILabel new];
        _scoreLabel.font = kFont(10);
        _scoreLabel.textColor = [UIColor colorWithHexString:@"0xFF9500"];
        _scoreLabel.textAlignment = NSTextAlignmentLeft;
        _scoreLabel.text = @"";
    }
    return _scoreLabel;
}

- (void)setModel:(QJMallShopHomeDetailModel *)model{
    _model = model;
    [self.imageView setImageWithURL:[NSURL URLWithString:[_model.mainPicture checkImageUrlString]] placeholder:[UIImage imageNamed:@"mine_head"]];
    _titleLabel.text = kCheckStringNil(_model.name);
    _countLabel.text = [NSString stringWithFormat:@"已兑%@/%@",_model.totalSoldNum,_model.totalCount];
    [self.countLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.whiteView);
        make.size.mas_equalTo(CGSizeMake([_countLabel.text getWidthWithFont:kFont(8) withHeight:20] + 20, 20));
    }];
    [self.countLabel layoutIfNeeded];
    [self.countLabel showCorner:8 rectCorner:UIRectCornerBottomLeft];

    NSMutableArray *modelArray = [NSMutableArray array];
    NSString *priceString = [NSString stringWithFormat:@"%@ 奇迹币",_model.qjcoinPartPrice];
    QJLabelAttributeModel *labelModle = [QJLabelAttributeModel new];
    labelModle.font = kFont(18);
    labelModle.textColor = [UIColor colorWithHexString:@"0xFF9500"];
    labelModle.range = [priceString rangeOfString:_model.qjcoinPartPrice];
    [modelArray addObject:labelModle];
    if (![kCheckStringNil(_model.moneyPartPrice) isEqualToString:@"0"]){
        priceString = [NSString stringWithFormat:@"%@ 奇迹币+¥ %@",_model.qjcoinPartPrice,_model.moneyPartPrice];
        QJLabelAttributeModel *moneyModel = [QJLabelAttributeModel new];
        moneyModel.font = kFont(18);
        moneyModel.textColor = [UIColor colorWithHexString:@"0xFF9500"];
        NSRange moneyRange = NSMakeRange(priceString.length - _model.moneyPartPrice.length, _model.moneyPartPrice.length);
        moneyModel.range = moneyRange;
        [modelArray addObject:moneyModel];
    }
    [_scoreLabel setSingleStr:priceString andArrayModel:modelArray maxWidth:(QJScreenWidth - 45)/2 - 16];
}

@end
