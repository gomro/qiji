//
//  QJMallShopHomeListModel.h
//  QJBox
//
//  Created by rui on 2022/8/24.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@class QJMallShopHomeDetailModel;

@interface QJMallShopHomeListModel : NSObject

/* 数据源数组 */
@property (nonatomic, strong) NSMutableArray<QJMallShopHomeDetailModel *> *records;

/* 总条数 */
@property (nonatomic, strong) NSNumber *total;
/* 分页 */
@property (nonatomic, strong) NSNumber *page;
/* 当前页 */
@property (nonatomic, strong) NSNumber *current;
/* 是否能分页 */
@property (nonatomic, assign) BOOL hasNext;
/* 是否需要加载更多 */
@property (assign, nonatomic) BOOL willLoadMore;
/* 分页 */
- (NSDictionary *)toParams;
/* 解析model */
-(void)configObj:(QJMallShopHomeListModel *)model;

@end

@interface QJMallShopHomeDetailModel : NSObject

/* 商品主图 */
@property (nonatomic, strong) NSString *mainPicture;
/* 游戏id */
@property (nonatomic, strong) NSString *ID;
@property (nonatomic, strong) NSString *moneyPartPrice;//金额部分售价
@property (nonatomic, strong) NSString *name;//商品名称
@property (nonatomic, strong) NSString *qjcoinPartPrice;//奇迹币部分售价
@property (nonatomic, strong) NSString *stock;//库存
@property (nonatomic, strong) NSString *totalCount;//总数量
@property (nonatomic, strong) NSString *totalSoldNum;//总销量

/**兑换记录**/
@property (nonatomic, strong) NSMutableArray<QJMallShopHomeDetailModel *> *items;
@property (nonatomic, strong) NSString *createTime;//创建时间
@property (nonatomic, strong) NSString *sn;//订单编号
@property (nonatomic, strong) NSString *picture;//产品图片

@end

NS_ASSUME_NONNULL_END

