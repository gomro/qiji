//
//  QJMallShopCardDetailModel.m
//  QJBox
//
//  Created by rui on 2022/9/21.
//

#import "QJMallShopCardDetailModel.h"

@implementation QJMallShopCardDetailModel

+ (NSDictionary *)modelCustomPropertyMapper{
    return @{@"ID" : @[@"id"]};
}

@end

@implementation QJMallShopCardItemDetailModel

+ (NSDictionary *)modelCustomPropertyMapper{
    return @{@"ID" : @[@"id"]};
}

+ (NSDictionary *)modelContainerPropertyGenericClass{
    return @{@"contentItems" : [QJMallShopCardItemDetailModel class],
             };
}

@end
