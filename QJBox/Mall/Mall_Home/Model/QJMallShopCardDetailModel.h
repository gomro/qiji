//
//  QJMallShopCardDetailModel.h
//  QJBox
//
//  Created by rui on 2022/9/21.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@class QJMallShopCardItemDetailModel;

@interface QJMallShopCardDetailModel : NSObject

/* 数据源 */
@property (nonatomic, strong) QJMallShopCardItemDetailModel *ecardDetailVo;
@property (nonatomic, strong) NSString *ID;
@property (nonatomic, strong) NSString *sn;
@property (nonatomic, strong) NSString *createTime;

@end

@interface QJMallShopCardItemDetailModel : NSObject

/* 数据源数组 */
@property (nonatomic, strong) NSMutableArray<QJMallShopCardItemDetailModel *> *contentItems;

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *value;
@property (nonatomic, assign) BOOL copyable;

@property (nonatomic, strong) NSDictionary *targetPage;
//封面图
@property (nonatomic, strong) NSString *coverImg;
//使用说明
@property (nonatomic, strong) NSString *desc;
//注意事项
@property (nonatomic, strong) NSString *notes;
//类型
@property (nonatomic, strong) NSString *typeName;

@end

NS_ASSUME_NONNULL_END
