//
//  QJMallCardConvertViewController.m
//  QJBox
//
//  Created by rui on 2022/8/24.
//

#import "QJMallCardConvertViewController.h"
/* 通用卡券cell */
#import "QJMallHomeCardCommonCell.h"
#import "QJMallShopCardDetailModel.h"
/* 请求 */
#import "QJMallShopHomeRequset.h"

/* 兑换卡 */
#import "QJMallHomeCardCell.h"
/* 礼包兑换码 */
#import "QJMallHomeCodeCell.h"
/* 代金券 */
#import "QJMallHomeGiftCell.h"

@interface QJMallCardConvertViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) QJMallShopCardDetailModel *cardModel;

@end

@implementation QJMallCardConvertViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"兑换卡";
    self.view.backgroundColor = UIColorHex(F5F5F5);
    self.navBar.backgroundColor = UIColorHex(F5F5F5);
    
    /* 初始化UI */
    [self initUI];
    /* 添加约束 */
    [self makeConstraints];
    /* 请求 */
    [self sendRequest];
}

/**
 * @author: zjr
 * @date: 2022-8-24
 * @desc: 初始化UI
 */
- (void)initUI{
    self.cardModel = [[QJMallShopCardDetailModel alloc] init];
    [self.view addSubview:self.tableView];
}

/**
 * @author: zjr
 * @date: 2022-8-24
 * @desc: 添加约束
 */
- (void)makeConstraints{
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.navBar.mas_bottom);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view);
//        make.left.right.bottom.equalTo(self.view);
//        make.top.equalTo(self.view).offset(NavigationBar_Bottom_Y);
    }];
}

#pragma mark - Lazy Loading
-(UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.width , self.view.height) style:UITableViewStyleGrouped];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = UIColorHex(F5F5F5);
        [_tableView registerClass:[QJMallHomeCardCommonCell class] forCellReuseIdentifier:@"QJMallHomeCardCommonCell"];
//        [_tableView registerClass:[QJMallHomeCardCell class] forCellReuseIdentifier:@"QJMallHomeCardCell"];
//        [_tableView registerClass:[QJMallHomeCodeCell class] forCellReuseIdentifier:@"QJMallHomeCodeCell"];
//        [_tableView registerClass:[QJMallHomeGiftCell class] forCellReuseIdentifier:@"QJMallHomeGiftCell"];
        _tableView.estimatedRowHeight = 40;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        if (@available(iOS 11.0, *)) {
            _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
        if (@available(iOS 15.0, *)) {
            _tableView.sectionHeaderTopPadding = 0;
        }
    }
    return _tableView;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.cardModel.ecardDetailVo.contentItems.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 106*kWScale;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return (122+16)*kWScale;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, QJScreenWidth, 106*kWScale)];
    UIView *whiteBgView = [UIView new];
    whiteBgView.backgroundColor = [UIColor whiteColor];
    [headerView addSubview:whiteBgView];
    
    [whiteBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(headerView).offset(16);
        make.right.equalTo(headerView).offset(-16);
        make.top.bottom.equalTo(headerView);
    }];
    
    UIImageView *bannerImageView = [UIImageView new];
    bannerImageView.layer.cornerRadius = 4;
    [whiteBgView addSubview:bannerImageView];
    
    [bannerImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(whiteBgView).offset(16);
        make.left.equalTo(whiteBgView).offset(16);
        make.right.equalTo(whiteBgView).offset(-16);
        make.height.equalTo(@(90*kWScale));
    }];
    
    [whiteBgView layoutIfNeeded];
    [whiteBgView showCorner:8 rectCorner:UIRectCornerTopLeft | UIRectCornerTopRight];

    [bannerImageView setImageWithURL:[NSURL URLWithString:[self.cardModel.ecardDetailVo.coverImg checkImageUrlString]] placeholder:[UIImage imageNamed:@"mine_head"]];

    return headerView;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, QJScreenWidth, (122+16)*kWScale)];
    UIView *whiteBgView = [UIView new];
    whiteBgView.backgroundColor = [UIColor whiteColor];
    [footerView addSubview:whiteBgView];
        
    [whiteBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(footerView).offset(16);
        make.right.equalTo(footerView).offset(-16);
        make.top.bottom.equalTo(footerView);
    }];

    UIView *lineView = [UIView new];
    lineView.backgroundColor = UIColorFromRGB(0xEBEDF0);
    [footerView addSubview:lineView];
    
    UILabel *explainTitleLabel = [UILabel new];
    explainTitleLabel.font = kFont(12);
    explainTitleLabel.textColor = UIColorFromRGB(0x16191C);
    explainTitleLabel.text = @"使用说明:";
    [footerView addSubview:explainTitleLabel];
    
    UILabel *explainNameLabel = [UILabel new];
    explainNameLabel.font = kFont(12);
    explainNameLabel.textColor = UIColorFromRGB(0x919599);
    explainNameLabel.text = @"进入游戏内，输入卡号和兑换码进行兑换";
    [footerView addSubview:explainNameLabel];
    
    UILabel *attentionTitleLabel = [UILabel new];
    attentionTitleLabel.font = kFont(12);
    attentionTitleLabel.textColor = UIColorFromRGB(0x16191C);
    attentionTitleLabel.text = @"注意:";
    [footerView addSubview:attentionTitleLabel];
    
    UILabel *attentionNameLabel = [UILabel new];
    attentionNameLabel.font = kFont(12);
    attentionNameLabel.textColor = UIColorFromRGB(0x919599);
    attentionNameLabel.text = @"本卡最终解释权归盛旭奇迹盒子所有";
    [footerView addSubview:attentionNameLabel];
    
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(whiteBgView).offset(16);
        make.left.equalTo(whiteBgView).offset(16);
        make.right.equalTo(whiteBgView).offset(-16);
        make.height.equalTo(@1);
    }];
    
    [explainTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(whiteBgView).offset(16);
        make.right.equalTo(whiteBgView).offset(-16);
        make.top.equalTo(lineView.mas_bottom).offset(16);
    }];
    
    [explainNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(whiteBgView).offset(16);
        make.right.equalTo(whiteBgView).offset(-16);
        make.top.equalTo(explainTitleLabel.mas_bottom).offset(8);
    }];
    
    [attentionTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(whiteBgView).offset(16);
        make.right.equalTo(whiteBgView).offset(-16);
        make.top.equalTo(explainNameLabel.mas_bottom).offset(16);
    }];
    
    [attentionNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(whiteBgView).offset(16);
        make.right.equalTo(whiteBgView).offset(-16);
        make.top.equalTo(attentionTitleLabel.mas_bottom).offset(8);
    }];
    
    [whiteBgView layoutIfNeeded];
    [whiteBgView showCorner:8 rectCorner:UIRectCornerBottomLeft | UIRectCornerBottomRight];

    explainNameLabel.text = self.cardModel.ecardDetailVo.desc;
    attentionNameLabel.text = self.cardModel.ecardDetailVo.notes;
    return footerView;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    QJMallHomeCardCommonCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QJMallHomeCardCommonCell" forIndexPath:indexPath];
    QJMallShopCardItemDetailModel *model = [self.cardModel.ecardDetailVo.contentItems safeObjectAtIndex:indexPath.row];
    cell.model = model;
    return cell;

//    if (indexPath.row == 0) {
//        QJMallHomeCardCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QJMallHomeCardCell" forIndexPath:indexPath];
//        return cell;
//    }else if (indexPath.row == 1) {
//        QJMallHomeCodeCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QJMallHomeCodeCell" forIndexPath:indexPath];
//        return cell;
//    }else{
//        QJMallHomeGiftCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QJMallHomeGiftCell" forIndexPath:indexPath];
//        return cell;
//    }
}

#pragma mark - 请求方法
/**
 * @author: zjr
 * @date: 2022-7-7
 * @desc: 卡券详情
 */
- (void)sendRequest{
    QJMallShopHomeRequset *productReq = [[QJMallShopHomeRequset alloc] init];
    if (![kCheckStringNil(self.idStr) isEqualToString:@""]){
        [productReq getMallShopCardDetailRequest:self.idStr];
    }

    WS(weakSelf)
    productReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            weakSelf.cardModel = [QJMallShopCardDetailModel modelWithDictionary:EncodeDicFromDic(request.responseObject, @"data")];
        }else{
            NSString *msg = EncodeStringFromDic(request.responseObject, @"message");
            [weakSelf.view makeToast:msg];
        }
        [weakSelf.tableView reloadData];
    };
    productReq.failureCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        [weakSelf.tableView reloadData];
        [weakSelf.view makeToast:@"数据异常 请稍后再试"];
    };
}

@end
