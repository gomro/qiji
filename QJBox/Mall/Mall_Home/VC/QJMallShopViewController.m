//
//  QJMallShopViewController.m
//  QJBox
//
//  Created by rui on 2022/6/28.
//

#import "QJMallShopViewController.h"
#import "QJMallScoreDetailViewController.h"
#import "QJMallShopEvaluateViewController.h"
/* 自定义商品View */
#import "QJMallShopHomeView.h"
#import "QJSegmentLineView.h"
#import "QJMallShopHomeHeaderView.h"
#import "QJSearchSortView.h"
#import "QJScoreRequest.h"

/* 三方View */
#import "GKPageScrollView.h"
#import "QJMallExchangeSucceededVC.h"
#import "QJMallOrderListViewController.h"

@interface QJMallShopViewController ()<GKPageScrollViewDelegate,GKPageTableViewGestureDelegate,UIScrollViewDelegate>

/* 自定义导航View */
@property (nonatomic, strong) UIView *navView;
/* 返回按钮 */
@property (nonatomic, strong) UIButton *backButton;
@property (nonatomic, strong) UIButton *recordButton;
/* 标题 */
@property (nonatomic, strong) UILabel *titleLabel;

/* View */
@property (nonatomic, strong) GKPageScrollView  *pageScrollView;
@property (nonatomic, strong) NSMutableArray    *childVCs;
@property (nonatomic, strong) UIScrollView      *scrollView;
@property (nonatomic, strong) UIView            *pageView;
@property (nonatomic, strong) UIView            *segmentView;
@property (nonatomic, assign) NSInteger chooseViewInt;
/* 搜索右侧选择按钮 */
@property (nonatomic, strong) UIButton *sortButton;

@property (nonatomic, strong) QJSegmentLineView *segment;
/* 自定义headerView */
@property (nonatomic, strong) QJMallShopHomeHeaderView *headerView;
/* 筛选view */
@property (nonatomic, strong) QJSearchSortView *sortView;
@property (nonatomic, strong) NSMutableArray *tagModels;

/* GKScrollView回调 */
@property (nonatomic,   copy) void(^listScrollViewDidScroll)(UIScrollView *scrollView);

@end

@implementation QJMallShopViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    /* 调整自定义导航视图层次 */
    [self.view bringSubviewToFront:self.navView];
    
    /* 请求数据 */
    [self taskInfoReq];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navBar.hidden = YES;
    self.chooseViewInt = 0;
    /* 加载自定义导航 */
    [self initNav];
    /* 初始化UI */
    [self initUI];
    
    // 兑换商品成功
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pushSuccessVC:) name:QJMallConfirmOrderSuccess object:nil];
    
}

- (void)pushSuccessVC:(NSNotification *)notification {
    NSString *type = notification.object;
    QJMallExchangeSucceededVC *vc = [QJMallExchangeSucceededVC new];
    vc.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:vc animated:YES completion:nil];
    @weakify(self);
    vc.QJMallConfirmOrderBlock = ^{
        @strongify(self);
        QJMallOrderListViewController *vc = [[QJMallOrderListViewController alloc] init];
        vc.status = type.integerValue == 1 ? @"1" : @"2";
        [self.navigationController pushViewController:vc animated:YES];
    };
    
}

/**
 * @author: zjr
 * @date: 2022-8-15
 * @desc: 自定义导航
 */
- (void)initNav{
    self.navView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, QJScreenWidth, NavigationBar_Bottom_Y)];
    self.navView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.navView];
    
    self.backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.backButton setImage:[UIImage imageNamed:@"backImage_white"] forState:UIControlStateNormal];
    self.backButton.frame = CGRectMake(9, [QJDeviceConstant top_iPhoneX_SPACE]+27, 30, 30);
    [self.backButton addTarget:self action:@selector(backBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [self.navView addSubview:self.backButton];
    
    self.recordButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.recordButton setTitle:@"兑换记录" forState:UIControlStateNormal];
    self.recordButton.titleLabel.font = kFont(12);
    self.recordButton.frame = CGRectMake(QJScreenWidth - 75, [QJDeviceConstant top_iPhoneX_SPACE]+27, 60, 30);
    [self.recordButton addTarget:self action:@selector(recordBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [self.navView addSubview:self.recordButton];
    
    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 30)];
    self.titleLabel.centerY = self.backButton.centerY;
    self.titleLabel.centerX = QJScreenWidth/2;
    self.titleLabel.text = @"奇迹币商城";
    self.titleLabel.font = kboldFont(17);
    self.titleLabel.textColor = UIColorFromRGB(0xFFFFFF);
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.navView addSubview:self.titleLabel];
}

/**
 * @author: zjr
 * @date: 2022-8-24
 * @desc: 初始化UI
 */
- (void)initUI{
    [self.view addSubview:self.pageScrollView];
    [self.pageScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.bottom.equalTo(self.view);
    }];
    
    for (int i = 0; i < 3 ; i ++) {
        QJMallShopHomeView *view = [[QJMallShopHomeView alloc] initWithFrame:CGRectMake(0, 0, QJScreenWidth, QJScreenHeight - 44*kWScale - NavigationBar_Bottom_Y)];
        view.searchKey = @"AVERAGE";
        if (i == 0) {
            view.type = @"";
        }else if (i == 1) {
            view.type = @"1";
        }else {
            view.type = @"2";
        }
        [view refreshFirst];
        [self.childVCs addObject:view];
    }
    [self.childVCs enumerateObjectsUsingBlock:^(UIView *vc, NSUInteger idx, BOOL * _Nonnull stop) {
        [self.scrollView addSubview:vc];
        vc.frame = CGRectMake(idx * QJScreenWidth, 0, QJScreenWidth, QJScreenHeight - 44*kWScale - NavigationBar_Bottom_Y);
    }];
    _scrollView.contentSize = CGSizeMake(self.childVCs.count * QJScreenWidth, 0);
    
    [self.pageScrollView reloadData];
    
    self.tagModels = [NSMutableArray array];
    QJSearchTagDetailModel *model = [QJSearchTagDetailModel new];
    model.name = @"综合排序";
    model.keyword = @"AVERAGE";
    model.isSelect = YES;
    
    QJSearchTagDetailModel *model1 = [QJSearchTagDetailModel new];
    model1.name = @"奇迹币从高到低";
    model1.keyword = @"QJCOIN_DESC";
    model1.isSelect = NO;
    
    QJSearchTagDetailModel *model2 = [QJSearchTagDetailModel new];
    model2.name = @"奇迹币从低到高";
    model2.keyword = @"QJCOIN_ASC";
    model2.isSelect = NO;
    [self.tagModels addObject:model];
    [self.tagModels addObject:model1];
    [self.tagModels addObject:model2];
    
    [self.view addSubview:self.sortView];
}

/**
 * @author: zjr
 * @date: 2022-6-28
 * @desc: button点击事件
 */
- (void)backBtnAction{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)recordBtnAction{
//    QJMallShopEvaluateViewController *testVC = [[QJMallShopEvaluateViewController alloc] init];
//    [self.navigationController pushViewController:testVC animated:YES];
    QJMallScoreDetailViewController *scoreDetailVC = [[QJMallScoreDetailViewController alloc] init];
    [self.navigationController pushViewController:scoreDetailVC animated:YES];
}

- (void)timeChoose:(UIButton *)btn {
    if (self.sortView.hidden == YES) {
        self.sortView.hidden = NO;
        [self.view bringSubviewToFront:self.sortView];
    }else{
        self.sortView.hidden = YES;
    }
    [self.sortView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.segmentView.mas_bottom);
        make.bottom.equalTo(self.view);
        make.left.right.equalTo(self.view);
    }];
}

#pragma mark - Lazy Loading
- (GKPageScrollView *)pageScrollView {
    if (!_pageScrollView) {
        _pageScrollView = [[GKPageScrollView alloc] initWithDelegate:self];
        _pageScrollView.ceilPointHeight = NavigationBar_Bottom_Y;
        _pageScrollView.mainTableView.gestureDelegate = self;
        _pageScrollView.isAllowListRefresh = YES;
    }
    return _pageScrollView;
}

- (QJMallShopHomeHeaderView *)headerView {
    if (!_headerView) {
        _headerView = [[QJMallShopHomeHeaderView alloc] initWithFrame:CGRectMake(0, 0, self.view.width, 216*kWScale)];
    }
    return _headerView;
}

- (UIView *)pageView {
    if (!_pageView) {
        _pageView = [UIView new];
        _pageView.backgroundColor = [UIColor whiteColor];
        [_pageView addSubview:self.segmentView];
        [_pageView addSubview:self.scrollView];
    }
    return _pageView;
}

- (UIView *)segmentView {
    if (!_segmentView) {
        _segmentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, QJScreenWidth, 44*kWScale)];
        _segmentView.backgroundColor = [UIColor whiteColor];
        [_segmentView addSubview:self.segment];
        [_segmentView addSubview:self.sortButton];
        [self.sortButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.segmentView).offset(-16);
//            make.width.mas_equalTo(84*kWScale);
            make.height.mas_equalTo(40*kWScale);
            make.centerY.equalTo(self.segmentView);
        }];
//        [self.sortButton layoutIfNeeded];
        [self.sortButton layoutWithStatus:QJLayoutStatusImageRight andMargin:5];
    }
    return _segmentView;
}

- (QJSegmentLineView *)segment {
    if (!_segment) {
        _segment = [[QJSegmentLineView alloc] initWithFrame:CGRectMake(0, 0, QJScreenWidth, 44*kWScale)];
        _segment.backgroundColor = [UIColor whiteColor];
        _segment.segmentWidthStyle = QJSegmentViewWidthStyleAuto;
        _segment.font = kFont(16);
        _segment.selectedFont = kboldFont(18);
        _segment.textColor = UIColorFromRGB(0x16191C);
        _segment.selectedTextColor = UIColorFromRGB(0x16191C);
        _segment.leftOffset = 16;
        _segment.itemSpace = 16;
        _segment.showBottomLine = NO;
        _segment.tag = 10000;
        [_segment setTitleArray:@[@"全部",@"实物",@"卡券",]];
        WS(weakSelf)
        [_segment setSegmentedItemSelectedBlock:^(QJSegmentLineView * _Nonnull segment, NSInteger selectedIndex) {
            weakSelf.chooseViewInt = selectedIndex;
            [weakSelf.scrollView setContentOffset:CGPointMake(QJScreenWidth * selectedIndex, 0) animated:NO];
        }];
    }
    return _segment;
}

- (UIScrollView *)scrollView {
    if (!_scrollView) {
        CGFloat scrollW = self.view.width;
        CGFloat scrollH = QJScreenHeight - 44*kWScale - NavigationBar_Bottom_Y;
        
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 44, scrollW, scrollH)];
        _scrollView.backgroundColor = [UIColor whiteColor];
        _scrollView.pagingEnabled = YES;
        _scrollView.directionalLockEnabled = YES;
//        _scrollView.scrollEnabled = NO;
        _scrollView.bounces = NO;
        _scrollView.delegate = self;
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.showsHorizontalScrollIndicator = NO;
    }
    return _scrollView;
}

- (NSMutableArray *)childVCs {
    if (!_childVCs) {
        _childVCs = [NSMutableArray array];
    }
    return _childVCs;
}

- (QJSearchSortView *)sortView{
    if (!_sortView) {
        _sortView = [[QJSearchSortView alloc] initWithFrame:CGRectMake(0, 0, QJScreenWidth, QJScreenHeight-44*kWScale-NavigationBar_Bottom_Y)];
        _sortView.hidden = YES;
        
        _sortView.sortTags = self.tagModels;
        WS(weakSelf)
        _sortView.itemSelectBlock = ^(QJSearchTagDetailModel * _Nonnull model) {
            [weakSelf.tagModels enumerateObjectsUsingBlock:^(QJSearchTagDetailModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                obj.isSelect = NO;
            }];
            model.isSelect = YES;
            QJMallShopHomeView *view = [weakSelf.childVCs safeObjectAtIndex:weakSelf.chooseViewInt];
            view.searchKey = model.keyword;
            [view refreshFirst];
            
            [weakSelf.sortButton setTitle:model.name forState:UIControlStateNormal];
//            [weakSelf.sortButton layoutIfNeeded];
            [weakSelf.sortButton layoutWithStatus:QJLayoutStatusImageRight andMargin:5];
            weakSelf.sortView.sortTags = weakSelf.tagModels;
        };
    }
    return _sortView;
}

- (UIButton *)sortButton {
    if (!_sortButton) {
        _sortButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_sortButton setTitle:@"综合排序" forState:UIControlStateNormal];
        _sortButton.titleLabel.font = MYFONTALL(FONT_REGULAR, 12);
        [_sortButton setTitleColor:UIColorFromRGB(0x474849) forState:UIControlStateNormal];
        [_sortButton setImage:[UIImage imageNamed:@"iconDownOne"] forState:UIControlStateNormal];
        [_sortButton addTarget:self action:@selector(timeChoose:) forControlEvents:UIControlEventTouchUpInside];
        [_sortButton layoutWithStatus:QJLayoutStatusImageRight andMargin:5];
    }
    return _sortButton;
}

#pragma mark - GKPageScrollView
- (UIView *)headerViewInPageScrollView:(GKPageScrollView *)pageScrollView {
    return self.headerView;
}

- (NSArray<id<GKPageListViewDelegate>> *)listViewsInPageScrollView:(GKPageScrollView *)pageScrollView {
    return self.childVCs;
}

- (UIView *)pageViewInPageScrollView:(GKPageScrollView *)pageScrollView {
    return self.pageView;
}

- (void)listViewDidScrollCallback:(void (^)(UIScrollView * _Nonnull))callback {
    self.listScrollViewDidScroll = callback;
}

- (void)mainTableViewDidScroll:(UIScrollView *)scrollView isMainCanScroll:(BOOL)isMainCanScroll {
    
    //计算导航栏的透明度
    CGFloat minAlphaOffset = 0;
    CGFloat maxAlphaOffset = 216*kWScale - NavigationBar_Bottom_Y;
    CGFloat offset = scrollView.contentOffset.y;
    CGFloat alpha = (offset - minAlphaOffset) / (maxAlphaOffset - minAlphaOffset);
    
    UIColor *navColor = UIColorFromRGB(0xFFB736);
    self.navView.backgroundColor = [navColor colorWithAlphaComponent:alpha];
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [self.pageScrollView horizonScrollViewWillBeginScroll];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    int page = (_scrollView.contentOffset.x+self.view.width/2.0) / self.view.width;
    self.chooseViewInt = page;
    [_segment setSegmentSelectedIndex:page];

    [self.pageScrollView horizonScrollViewDidEndedScroll];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    [self.pageScrollView horizonScrollViewDidEndedScroll];
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    !self.listScrollViewDidScroll ? : self.listScrollViewDidScroll(scrollView);
}

/**
 * @author: zjr
 * @date: 2022-8-24
 * @desc: header赋值
 */
- (void)taskInfoReq{
    QJScoreRequest *scoreReq = [[QJScoreRequest alloc] init];
    [scoreReq getUserScoreInfoRequest];
    WS(weakSelf)
    scoreReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            if ([EncodeDicFromDic(request.responseObject, @"data") isKindOfClass:[NSDictionary class]]) {
                NSDictionary *dataDic = EncodeDicFromDic(request.responseObject, @"data");
                NSString *qicoinStr = EncodeStringFromDicDefEmtryValue(dataDic, @"qicoin");;
                weakSelf.headerView.qiCoin = [NSString stringWithFormat:@"%@", qicoinStr];
                [self.childVCs enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    QJMallShopHomeView *view = (QJMallShopHomeView *)obj;
                    view.qjIcon = [NSString stringWithFormat:@"%@", qicoinStr];
                }];
            }
        }else{
            NSString *msg = EncodeStringFromDic(request.responseObject, @"message");
            [weakSelf.view makeToast:msg];
        }
    };
    scoreReq.failureCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        [weakSelf.view makeToast:@"数据异常 请稍后再试"];
    };
}

@end
