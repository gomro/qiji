//
//  QJMallScoreDetailViewController.m
//  QJBox
//
//  Created by rui on 2022/6/28.
//

#import "QJMallScoreDetailViewController.h"

/* 积分明细View */
#import "QJMallScoreDetailView.h"

@interface QJMallScoreDetailViewController ()<UIScrollViewDelegate>

/* 兑换明细 */
@property (nonatomic, strong) QJMallScoreDetailView *conversionView;

@end

@implementation QJMallScoreDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"兑换记录";
    /* 初始化UI */
    [self initUI];
}

/**
 * @author: zjr
 * @date: 2022-6-28
 * @desc: 初始化UI
 */
- (void)initUI{
    self.conversionView = [[QJMallScoreDetailView alloc] initWithFrame:CGRectMake(0, 0, self.view.width, self.view.height-NavigationBar_Bottom_Y)];
    [self.view addSubview:self.conversionView];
    [self.conversionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(NavigationBar_Bottom_Y);;
//        make.bottom.equalTo(self.view);
        make.left.right.bottom.equalTo(self.view);
    }];
    [self.conversionView refreshFirst];
}

@end
