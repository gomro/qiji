//
//  QJMallCardConvertViewController.h
//  QJBox
//
//  Created by rui on 2022/8/24.
//

#import "QJBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJMallCardConvertViewController : QJBaseViewController

@property (nonatomic, strong) NSString *idStr;

@end

NS_ASSUME_NONNULL_END
