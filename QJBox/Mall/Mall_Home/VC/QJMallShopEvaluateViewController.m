//
//  QJMallShopEvaluateViewController.m
//  QJBox
//
//  Created by rui on 2022/8/25.
//

#import "QJMallShopEvaluateViewController.h"
#import "QJMallShopEvaluateStarCell.h"
#import "QJMallShopEvaluateContentCell.h"
#import "QJMallShopEvaluateImageCell.h"
#import "QJDetailAlertView.h"
#import "TYAlertController.h"

#import "QJCampsiteRequest.h"
#import "QJMallShopHomeRequset.h"
/* 图片选择器 */
#import "TZImagePickerController.h"

static  int enterCount = 0;
static  int leaveCount = 0;

@interface QJMallShopEvaluateViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) UIView *buttonView;
@property (nonatomic, strong) UIButton *publishButton;

/* 发布参数 */
@property (nonatomic, strong) NSString *starConutString;
@property (nonatomic, strong) NSString *contentString;
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, strong) NSMutableArray *imageUrlPath;
@property (nonatomic, strong) NSString *videoAddress;
@property (nonatomic, strong) NSString *videoCoverAddress;
@property (nonatomic, strong) dispatch_group_t group;

@end

@implementation QJMallShopEvaluateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"评价";
    self.view.backgroundColor = UIColorHex(F5F5F5);
    self.navBar.backgroundColor = UIColorHex(F5F5F5);
    self.starConutString = @"5";
    /* 初始化UI */
    [self initUI];
    /* 添加约束 */
    [self makeConstraints];
    [self resetPublishButton];
}

/**
 * @author: zjr
 * @date: 2022-8-24
 * @desc: 初始化UI
 */
- (void)initUI{
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.buttonView];
    [self.buttonView addSubview:self.publishButton];
}

/**
 * @author: zjr
 * @date: 2022-8-24
 * @desc: 添加约束
 */
- (void)makeConstraints{
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.top.equalTo(self.view).offset(NavigationBar_Bottom_Y);
        make.bottom.equalTo(self.view).offset(-(56+Bottom_iPhoneX_SPACE)*kWScale);
    }];
    
    [self.buttonView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.height.equalTo(@((56+Bottom_iPhoneX_SPACE)*kWScale));
    }];
    
    [self.publishButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.buttonView).offset(8);
        make.left.equalTo(self.buttonView).offset(38);
        make.right.equalTo(self.buttonView).offset(-38);
        make.height.equalTo(@(40*kWScale));
    }];
    
    [self.buttonView layoutIfNeeded];
    [_buttonView viewShadowPathWithColor:[UIColor hx_colorWithHexStr:@"000000" alpha:0.08] shadowOpacity:1 shadowRadius:6 shadowPathType:QJShadowPathTop shadowPathWidth:6];
}

#pragma mark - Lazy Loading
-(UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.width , self.view.height) style:UITableViewStylePlain];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = UIColorHex(F5F5F5);
        [_tableView registerClass:[QJMallShopEvaluateStarCell class] forCellReuseIdentifier:@"QJMallShopEvaluateStarCell"];
        [_tableView registerClass:[QJMallShopEvaluateContentCell class] forCellReuseIdentifier:@"QJMallShopEvaluateContentCell"];
        [_tableView registerClass:[QJMallShopEvaluateImageCell class] forCellReuseIdentifier:@"QJMallShopEvaluateImageCell"];
        _tableView.estimatedRowHeight = 40;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        if (@available(iOS 11.0, *)) {
            _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
        if (@available(iOS 15.0, *)) {
            _tableView.sectionHeaderTopPadding = 0;
        }
    }
    return _tableView;
}

- (UIView *)buttonView{
    if (!_buttonView) {
        _buttonView = [UIView new];
        _buttonView.backgroundColor = [UIColor whiteColor];
    }
    return _buttonView;
}
    
- (UIButton *)publishButton{
    if (!_publishButton) {
        _publishButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_publishButton setTitle:@"提交" forState:UIControlStateNormal];
        [_publishButton setTitleColor:UIColorFromRGB(0xFFFFFF) forState:UIControlStateNormal];
        _publishButton.titleLabel.font = kFont(14);
        _publishButton.backgroundColor = UIColorFromRGB(0xFF9500);//C8CACC
        _publishButton.layer.cornerRadius = 20;
        [_publishButton addTarget:self action:@selector(publishClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _publishButton;
}

#pragma mark - UITableView
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 3;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    WS(weakSelf)
    if (indexPath.row == 0) {
        QJMallShopEvaluateStarCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QJMallShopEvaluateStarCell" forIndexPath:indexPath];
        cell.btnClick = ^(NSString * _Nonnull starCount) {
            weakSelf.starConutString = starCount;
            [weakSelf resetPublishButton];
        };
        return cell;
    }else if (indexPath.row == 1) {
        QJMallShopEvaluateContentCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QJMallShopEvaluateContentCell" forIndexPath:indexPath];
        cell.btnClick = ^(NSString * _Nonnull contentString) {
            weakSelf.contentString = contentString;
            [weakSelf resetPublishButton];
        };
        return cell;
    }else{
        QJMallShopEvaluateImageCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QJMallShopEvaluateImageCell" forIndexPath:indexPath];
        cell.reloadHeightClick = ^{
            [weakSelf.tableView reloadData];
        };
        cell.btnClick = ^(NSMutableArray * _Nonnull dataArray) {
            weakSelf.dataArray = dataArray;
            [weakSelf resetPublishButton];
        };
        return cell;
    }
}

/**
 * @author: zjr
 * @date: 2022-8-25
 * @desc: button点击事件
 */
- (void)publishClick{
    DLog(@"发布");
    if (self.contentString.length == 0) {
        [self.view makeToast:@"内容不能为空"];
        return;
    }
    [QJAppTool showHUDLoading];
    if (!self.group) {
        self.group = dispatch_group_create();
    }
    self.imageUrlPath = [NSMutableArray array];
    for (int i = 0; i < self.dataArray.count; i++) {
        TZAssetModel *model = [self.dataArray safeObjectAtIndex:i];
        if (model.isVideo) {
            NSData *videoData = [NSData dataWithContentsOfURL:[NSURL fileURLWithPath:model.outputPath]];
//            [self.imageUrlPath safeAddObject:model.objectKey];
            [self pushData:videoData objectKey:model.objectKey];
            self.videoAddress = [NSString stringWithFormat:@"/%@",model.objectKey];

            NSData *data = UIImageJPEGRepresentation(model.coverImage, 0.5);
            [self pushData:data objectKey:model.coverObjectKey];
            self.videoCoverAddress = [NSString stringWithFormat:@"/%@",model.coverObjectKey];
        }else{
            NSData *data = UIImageJPEGRepresentation(model.coverImage, 0.5);
            [self.imageUrlPath safeAddObject:model.objectKey];
            [self pushData:data objectKey:model.objectKey];
        }
    }
    [self commite];
}

- (void)getVideoImageFromPHAsset:(PHAsset *)asset Complete:(void (^)(UIImage *image))resultBack{
    PHImageRequestOptions *option = [[PHImageRequestOptions alloc] init];
    option.resizeMode = PHImageRequestOptionsResizeModeFast;
    [[PHImageManager defaultManager] requestImageForAsset:asset targetSize:CGSizeMake(200, 200) contentMode:PHImageContentModeDefault options:option resultHandler:^(UIImage * _Nullable result, NSDictionary * _Nullable info) {
        UIImage *iamge = result;
        resultBack(iamge);
    }];
}

- (void)commite {
    WS(weakSelf)
    QJMallShopHomeRequset *commitReq = [QJMallShopHomeRequset new];
    dispatch_group_notify(self.group, dispatch_get_main_queue(), ^{
        DLog(@"提交");
        NSMutableArray *imageUrl = [NSMutableArray array];
        for (NSString *str in self.imageUrlPath) {
            NSString *url = [NSString stringWithFormat:@"/%@",str];
            [imageUrl safeAddObject:url];
        }

        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        if (self.videoAddress){
            [dic setValue:self.videoAddress forKey:@"videoAddress"];
        }
        if (self.videoCoverAddress) {
            [dic setValue:self.videoCoverAddress forKey:@"videoPicture"];
        }
        [dic setValue:imageUrl forKey:@"pictures"];
        [dic setValue:weakSelf.contentString forKey:@"content"];
        [dic setValue:weakSelf.starConutString forKey:@"levelComment"];
        if (self.idStr){
            [dic setValue:self.idStr forKey:@"orderItemId"];
        }
//        [dic setValue:@"1572164938655793154" forKey:@"orderItemId"];
        commitReq.dic = dic.copy;

        [QJAppTool hideHUDLoading];
        [commitReq getMallShopPublishCommentRequest];
        commitReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
            if (ResponseSuccess) {
                [weakSelf.view makeToast:@"提交成功" duration:3 position:CSToastPositionCenter image:[UIImage imageNamed:@"success_login"]];
                [weakSelf.navigationController popViewControllerAnimated:YES];
                if (self.clickShopEvaluate) {
                    self.clickShopEvaluate();
                }
            }else{
                [weakSelf.view makeToast:ResponseMsg];
            }
        };
        commitReq.failureCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
            [weakSelf.view makeToast:ResponseFailToastMsg];
        };
    });
}

- (void)onBack:(id)sender{
    BOOL isContent = self.contentString.length > 0?YES:NO;
    BOOL isData = self.dataArray.count > 0?YES:NO;
    if (isContent || isData) {
        QJDetailAlertView *alertView = [[QJDetailAlertView alloc] initWithFrame:CGRectZero titleType:QJDetailAlertViewTitleTypeDefault buttonType:QJDetailAlertViewButtonTypeDefault];
        [alertView showTitleText:@"" describeText:@"您还没提交评价确定要退出吗？"];
        [alertView.leftButton setTitle:@"取消" forState:UIControlStateNormal];
        [alertView.rightButton setTitle:@"确定" forState:UIControlStateNormal];
        [alertView.rightButton setTitleColor:UIColorFromRGB(0x007AFF) forState:UIControlStateNormal];
        WS(weakSelf)
        [alertView returnClick:^(QJDetailAlertViewButtonClickType type) {
            [[QJAppTool getCurrentViewController] dismissViewControllerAnimated:YES completion:nil];
            if (type == QJDetailAlertViewButtonClickTypeRight) {
                [weakSelf.navigationController popViewControllerAnimated:YES];
            }
        }];
        TYAlertController *alertController = [TYAlertController alertControllerWithAlertView:alertView preferredStyle:TYAlertControllerStyleAlert];
        [self.navigationController presentViewController:alertController animated:YES completion:nil];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)resetPublishButton{
    BOOL isContent = self.contentString.length > 0?YES:NO;
    BOOL isData = self.dataArray.count > 0?YES:NO;
    if (isContent && isData) {
        _publishButton.backgroundColor = UIColorFromRGB(0xFF9500);
        _publishButton.userInteractionEnabled = YES;
    }else{
        _publishButton.backgroundColor = UIColorFromRGB(0xC8CACC);
        _publishButton.userInteractionEnabled = NO;
    }
}

//上传图片和视频
- (void)pushData:(NSData *)data objectKey:(NSString *)objectKey {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        id<OSSCredentialProvider> credential = [[OSSCustomSignerCredentialProvider alloc] initWithImplementedSigner:^NSString * _Nullable(NSString * _Nonnull contentToSign, NSError *__autoreleasing  _Nullable * _Nullable error) {
            OSSTaskCompletionSource * tcs = [OSSTaskCompletionSource taskCompletionSource];
            __block NSString *signToken= @"";
            QJCampsiteRequest *startRequest = [[QJCampsiteRequest alloc] init];
            [startRequest netWorkGetSign:contentToSign];
            [startRequest setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
                if (ResponseSuccess) {
                    NSString *data = request.responseJSONObject[@"data"][@"contentSign"];
                    signToken = data;
                    [tcs setResult:kCheckNil(data)];
                } else {
                    [tcs setError:request.error];
                }
            }];
            
            [tcs.task waitUntilFinished];
            if (tcs.task.error) {
                return nil;
            } else {
                return signToken;
            }
        }];
        OSSClient *client = [[OSSClient alloc] initWithEndpoint: @"https://oss-cn-hangzhou.aliyuncs.com" credentialProvider:credential];
        OSSPutObjectRequest * put = [OSSPutObjectRequest new];
        put.bucketName = @"sxqj";
        put.objectKey = objectKey;
        put.uploadingData = data;
        // （可选）设置上传进度。
        put.uploadProgress = ^(int64_t bytesSent, int64_t totalByteSent, int64_t totalBytesExpectedToSend) {
            // 指定当前上传长度、当前已经上传总长度、待上传的总长度。
            NSLog(@"%lld, %lld, %lld", bytesSent, totalByteSent, totalBytesExpectedToSend);
        };
        dispatch_group_enter(self.group);
        enterCount++;
        OSSTask * putTask = [client putObject:put];
        
        [putTask waitUntilFinished];
        [putTask continueWithBlock:^id(OSSTask *task) {
            if (!task.error) {
                NSLog(@"upload object success!");
                leaveCount++;
                if (leaveCount <= enterCount) {
                    if (self) {
                        dispatch_group_leave(self.group);
                    }
                }
            } else {
                leaveCount++;
                if (leaveCount <= enterCount) {
                    if (self) {
                        dispatch_group_leave(self.group);
                    }
                }
                NSLog(@"upload object failed, error: %@" , task.error);
            }
            return nil;
        }];
    });
}

@end
