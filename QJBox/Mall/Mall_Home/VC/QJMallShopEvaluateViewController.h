//
//  QJMallShopEvaluateViewController.h
//  QJBox
//
//  Created by rui on 2022/8/25.
//

#import "QJBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJMallShopEvaluateViewController : QJBaseViewController

@property (nonatomic, strong) NSString *idStr;
@property (nonatomic, copy) void(^clickShopEvaluate)(void);

@end

NS_ASSUME_NONNULL_END
