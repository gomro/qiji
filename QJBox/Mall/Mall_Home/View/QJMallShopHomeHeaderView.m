//
//  QJMallShopHomeHeaderView.m
//  QJBox
//
//  Created by rui on 2022/8/24.
//

#import "QJMallShopHomeHeaderView.h"
#import "QJMallOrderListViewController.h"
//#import "QJMallShopEvaluateViewController.h"
//#import "QJMallCardConvertViewController.h"

@interface QJMallShopHomeHeaderView ()

@property (nonatomic, strong) UIImageView *bgImageView;
@property (nonatomic, strong) UIImageView *bigImageView;
@property (nonatomic, strong) UIImageView *smallImageView;
@property (nonatomic, strong) UIView *contentView;
@property (nonatomic, strong) UIImageView *contentImageView;
@property (nonatomic, strong) UIView *lineView;
@property (nonatomic, strong) UIView *lineImageView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *scoreLabel;
@property (nonatomic, strong) UIButton *orderButton;

//@property (nonatomic, strong) UIButton *cardButton;
//@property (nonatomic, strong) UIButton *evaButton;

@end

@implementation QJMallShopHomeHeaderView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        /* 初始化UI */
        [self initUI];
        /* 添加约束 */
        [self makeConstraints];
    }
    return self;
}

/**
 * @author: zjr
 * @date: 2022-8-24
 * @desc: 加载view/约束
 */
- (void)initUI {
    [self addSubview:self.bgImageView];
    [self addSubview:self.bigImageView];
    [self addSubview:self.contentView];
    [self.contentView addSubview:self.contentImageView];
    [self.contentView addSubview:self.titleLabel];
    [self.contentView addSubview:self.scoreLabel];
    [self.contentView addSubview:self.orderButton];
//    [self.contentView addSubview:self.cardButton];
//    [self.contentView addSubview:self.evaButton];
    
    [self addSubview:self.smallImageView];
    [self addSubview:self.lineView];
    [self.lineView addSubview:self.lineImageView];
}


/**
 * @author: zjr
 * @date: 2022-7-1
 * @desc: 添加约束
 */
- (void)makeConstraints{
    [self.bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    
    [self.bigImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(68);
        make.right.equalTo(self).offset(-86);
        make.width.equalTo(@(68*kWScale));
        make.height.equalTo(@(68*kWScale));
    }];
    
    [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(112);
        make.left.equalTo(self).offset(16);
        make.right.equalTo(self).offset(-16);
        make.height.equalTo(@(70*kWScale));
    }];
    
    [self.contentView layoutIfNeeded];
    [self.contentView showCorner:8 rectCorner:UIRectCornerTopLeft | UIRectCornerTopRight];
    
    [self.contentImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.contentView);
        make.left.equalTo(self.contentView);
        make.right.equalTo(self.contentView);
    }];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(10);
        make.left.equalTo(self.contentView).offset(16);
    }];
    
    [self.scoreLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).offset(10);
        make.left.equalTo(self.contentView).offset(16);
    }];
    
    [self.orderButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.right.equalTo(self.contentView);
        make.width.equalTo(@(96*kWScale));
        make.height.equalTo(@(30*kWScale));
    }];
    
//    [self.cardButton mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerX.equalTo(self.contentView);
//        make.top.equalTo(self.titleLabel.mas_bottom).offset(10);
//        make.width.equalTo(@96);
//        make.height.equalTo(@30);
//    }];
    
//    [self.evaButton mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerX.equalTo(self.contentView);
//        make.top.equalTo(self.titleLabel);
//        make.width.equalTo(@96);
//        make.height.equalTo(@30);
//    }];
    
    [self.smallImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.contentView.mas_top).offset(12);
        make.centerX.equalTo(self.contentView);
        make.width.equalTo(@(24*kWScale));
        make.height.equalTo(@(24*kWScale));
    }];
    
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_bottom).offset(-7);
        make.left.equalTo(self).offset(7);
        make.right.equalTo(self).offset(-7);
        make.height.equalTo(@(14*kWScale));
    }];
    
    [self.lineImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.lineView).offset(4);
        make.left.equalTo(self.lineView).offset(6);
        make.right.equalTo(self.lineView).offset(-6);
        make.height.equalTo(@(6*kWScale));
    }];
    
    [self bringSubviewToFront:self.contentView];
    [self bringSubviewToFront:self.smallImageView];
    
    [self.orderButton layoutIfNeeded];
    [self.orderButton showCorner:15 rectCorner:UIRectCornerTopLeft | UIRectCornerBottomLeft];
    [self.orderButton layoutWithStatus:QJLayoutStatusNormal andMargin:5];
}

- (UIImageView *)bgImageView{
    if (!_bgImageView) {
        _bgImageView = [UIImageView new];
        _bgImageView.image = [UIImage imageNamed:@"mall_home_bg"];
    }
    return _bgImageView;
}

- (UIImageView *)bigImageView{
    if (!_bigImageView) {
        _bigImageView = [UIImageView new];
        _bigImageView.image = [UIImage imageNamed:@"mall_home_bigIcoin"];
    }
    return _bigImageView;
}

- (UIImageView *)smallImageView{
    if (!_smallImageView) {
        _smallImageView = [UIImageView new];
        _smallImageView.image = [UIImage imageNamed:@"mall_home_smallIcoin"];
    }
    return _smallImageView;
}

- (UIImageView *)contentImageView{
    if (!_contentImageView) {
        _contentImageView = [UIImageView new];
        _contentImageView.image = [UIImage imageNamed:@"mall_home_grayline"];
    }
    return _contentImageView;
}

- (UIView *)contentView{
    if (!_contentView) {
        _contentView = [UIView new];
        _contentView.backgroundColor = UIColorFromRGB(0xFFFFFF);
    }
    return _contentView;
}

- (UIView *)lineView{
    if (!_lineView) {
        _lineView = [UIView new];
        _lineView.backgroundColor = UIColorFromRGB(0xFF9B2E);
        _lineView.layer.cornerRadius = 7;
    }
    return _lineView;
}

- (UIView *)lineImageView{
    if (!_lineImageView) {
        _lineImageView = [UIView new];
        _lineImageView.backgroundColor = UIColorFromRGB(0xE57915);
        _lineImageView.layer.cornerRadius = 3;
    }
    return _lineImageView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.font = kFont(12);
        _titleLabel.textColor = [UIColor blackColor];
        _titleLabel.text = @"我的奇迹币";
    }
    return _titleLabel;
}

- (UILabel *)scoreLabel {
    if (!_scoreLabel) {
        _scoreLabel = [UILabel new];
        _scoreLabel.font = kboldFont(20);
        _scoreLabel.textColor = [UIColor blackColor];
        _scoreLabel.text = @"";
    }
    return _scoreLabel;
}

- (UIButton *)orderButton{
    if (!_orderButton) {
        _orderButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_orderButton setImage:[UIImage imageNamed:@"mall_home_ordericon"] forState:UIControlStateNormal];
        _orderButton.titleLabel.font = kFont(14);
        _orderButton.backgroundColor = UIColorFromRGB(0xFF8125);
        [_orderButton setTitle:@"我的订单" forState:UIControlStateNormal];
        [_orderButton setTitleColor:UIColorFromRGB(0xFFFFFF) forState:UIControlStateNormal];
        _orderButton.frame = CGRectMake(0, 0, 96*kWScale, 30*kWScale);
        [_orderButton addTarget:self action:@selector(orderBtnAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _orderButton;
}

//- (UIButton *)cardButton{
//    if (!_cardButton) {
//        _cardButton = [UIButton buttonWithType:UIButtonTypeCustom];
//        _cardButton.titleLabel.font = kFont(14);
//        _cardButton.backgroundColor = UIColorFromRGB(0xFF8125);
//        [_cardButton setTitle:@"卡券兑换" forState:UIControlStateNormal];
//        [_cardButton setTitleColor:UIColorFromRGB(0xFFFFFF) forState:UIControlStateNormal];
//        _cardButton.frame = CGRectMake(0, 0, 96, 30);
//        [_cardButton addTarget:self action:@selector(cardBtnAction) forControlEvents:UIControlEventTouchUpInside];
//    }
//    return _cardButton;
//}

//- (UIButton *)evaButton{
//    if (!_evaButton) {
//        _evaButton = [UIButton buttonWithType:UIButtonTypeCustom];
//        _evaButton.titleLabel.font = kFont(14);
//        _evaButton.backgroundColor = UIColorFromRGB(0xFF8125);
//        [_evaButton setTitle:@"评价" forState:UIControlStateNormal];
//        [_evaButton setTitleColor:UIColorFromRGB(0xFFFFFF) forState:UIControlStateNormal];
//        _evaButton.frame = CGRectMake(0, 0, 96, 30);
//        [_evaButton addTarget:self action:@selector(evaBtnAction) forControlEvents:UIControlEventTouchUpInside];
//    }
//    return _evaButton;
//}

//- (void)cardBtnAction{
//    QJMallCardConvertViewController *cardVC = [[QJMallCardConvertViewController alloc] init];
//    [[QJAppTool getCurrentViewController].navigationController pushViewController:cardVC animated:YES];
//}

- (void)orderBtnAction{
    QJMallOrderListViewController *vc = [[QJMallOrderListViewController alloc] init];
    [[QJAppTool getCurrentViewController].navigationController pushViewController:vc animated:YES];
}

//- (void)evaBtnAction{
//    QJMallShopEvaluateViewController *vc = [[QJMallShopEvaluateViewController alloc] init];
//    [[QJAppTool getCurrentViewController].navigationController pushViewController:vc animated:YES];
//}

- (void)setQiCoin:(NSString *)qiCoin{
    _qiCoin = qiCoin;
    _scoreLabel.text = _qiCoin;
}

@end
