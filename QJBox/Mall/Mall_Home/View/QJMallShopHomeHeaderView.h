//
//  QJMallShopHomeHeaderView.h
//  QJBox
//
//  Created by rui on 2022/8/24.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJMallShopHomeHeaderView : UIView

@property (nonatomic, strong) NSString *qiCoin;

@end

NS_ASSUME_NONNULL_END
