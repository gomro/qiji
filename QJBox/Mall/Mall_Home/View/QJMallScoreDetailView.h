//
//  QJMallScoreDetailView.h
//  QJBox
//
//  Created by rui on 2022/6/28.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJMallScoreDetailView : UIView

- (void)refreshFirst;

@end

NS_ASSUME_NONNULL_END
