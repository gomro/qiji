//
//  QJMallScoreDetailView.m
//  QJBox
//
//  Created by rui on 2022/6/28.
//

#import "QJMallScoreDetailView.h"

#import "QJScoreDetailCell.h"
/* model */
#import "QJMallShopHomeListModel.h"
#import "QJMallShopHomeRequset.h"

@interface QJMallScoreDetailView ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) QJMallShopHomeListModel *pageModel;

@end

@implementation QJMallScoreDetailView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self initChildView];
    }
    return self;
}

/**
 * @author: zjr
 * @date: 2022-8-15
 * @desc: 加载view/约束
 */
- (void)initChildView {
    self.pageModel = [QJMallShopHomeListModel new];
    [self addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
}

#pragma mark --Lazy Loading
-(UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        self.tableView.backgroundColor = UIColorFromRGB(0xFFFFFF);
        [_tableView registerClass:[QJScoreDetailCell class] forCellReuseIdentifier:@"QJScoreDetailCell"];
        // 自动计算行高模式
        _tableView.estimatedRowHeight = 88*kWScale;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        if (@available(iOS 11.0, *)) {
            _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
        if (@available(iOS 15.0, *)) {
            _tableView.sectionHeaderTopPadding = 0;
        }
        _tableView.mj_header = [QJRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(refreshFirst)];
        _tableView.mj_footer = [QJRefreshFooter footerWithRefreshingTarget:self refreshingAction:@selector(refreshMore)];
    }
    return _tableView;
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.pageModel.records.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    QJScoreDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QJScoreDetailCell" forIndexPath:indexPath];
    QJMallShopHomeDetailModel *model = [self.pageModel.records safeObjectAtIndex:indexPath.row];
    cell.model = model;
    return cell;
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 88*kWScale;
}

#pragma mark - 请求方法
/**
 * @author: zjr
 * @date: 2022-8-2
 * @desc: 请求方法
 */
- (void)refreshFirst{
    _pageModel.willLoadMore = NO;
    [self sendRequest];
}

- (void)refreshMore{
    if(_pageModel.hasNext){
        _pageModel.willLoadMore=YES;
        [self sendRequest];
    }else{
        [self.tableView.mj_footer endRefreshing];
        [self.tableView.mj_footer endRefreshingWithNoMoreData];
    }
}

- (void)sendRequest {
    QJMallShopHomeRequset *productReq = [[QJMallShopHomeRequset alloc] init];
    NSMutableDictionary *muDic = [NSMutableDictionary dictionaryWithDictionary:_pageModel.toParams];
    productReq.dic = muDic.copy;
    [productReq getMallShopExchangesListRequest];

    WS(weakSelf)
    productReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            [weakSelf.pageModel configObj:[QJMallShopHomeListModel modelWithDictionary:EncodeDicFromDic(request.responseObject, @"data")]];
        }else{
            NSString *msg = EncodeStringFromDic(request.responseObject, @"message");
            [weakSelf makeToast:msg];
        }
        [weakSelf.tableView reloadData];
        [weakSelf.tableView.mj_header endRefreshing];
        [weakSelf.tableView.mj_footer endRefreshing];
        QJEmptyCommonView *empty = [QJEmptyCommonView new];
        empty.string = @"还没有任何兑换记记录哦～";
        [weakSelf.tableView tableViewDisplayWhenHaveNoDataWithView:empty ifNecessaryForRowCount:weakSelf.pageModel.records.count];
    };
    productReq.failureCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        [weakSelf.tableView reloadData];
        [weakSelf.tableView.mj_header endRefreshing];
        [weakSelf.tableView.mj_footer endRefreshing];
        [weakSelf makeToast:@"数据异常 请稍后再试"];
        QJEmptyCommonView *empty = [QJEmptyCommonView new];
        empty.string = @"还没有任何兑换记记录哦～";
        [weakSelf.tableView tableViewDisplayWhenHaveNoDataWithView:empty ifNecessaryForRowCount:weakSelf.pageModel.records.count];
    };
}

@end
