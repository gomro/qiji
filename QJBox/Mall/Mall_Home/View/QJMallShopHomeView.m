//
//  QJMallShopHomeView.m
//  QJBox
//
//  Created by rui on 2022/8/24.
//

#import "QJMallShopHomeView.h"
/* 自定义商品Cell */
#import "QJScoreShopCollectionCell.h"
/* 商品详情页面 */
#import "QJMallShopEvaluateViewController.h"
#import "QJMallDetailViewController.h"
/* model */
#import "QJMallShopHomeListModel.h"
#import "QJMallShopHomeRequset.h"
/* 底部滚动view */
#import "GKPageScrollView.h"

@interface QJMallShopHomeView ()<UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource>

@property (nonatomic, strong) UICollectionView *collectionView;
/* GKScroll回调 */
@property (nonatomic, copy) void(^listScrollViewScrollBlock)(UIScrollView *scrollView);
@property (nonatomic, strong) QJMallShopHomeListModel *pageModel;
@end

@implementation QJMallShopHomeView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self initChildView];
    }
    return self;
}

/**
 * @author: zjr
 * @date: 2022-8-15
 * @desc: 加载view/约束
 */
- (void)initChildView {
    self.pageModel = [QJMallShopHomeListModel new];
    [self addSubview:self.collectionView];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
}

#pragma mark - Lazy Loading
- (UICollectionView *)collectionView{
    if (!_collectionView) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        layout.minimumLineSpacing = 15;
        layout.minimumInteritemSpacing = 15;
        layout.sectionInset = UIEdgeInsetsMake(15, 15, 15, 15);
        layout.itemSize = CGSizeMake((QJScreenWidth - 45)/2, (QJScreenWidth - 45)/2+66*kWScale);
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, NavigationBar_Bottom_Y, QJScreenWidth, QJScreenHeight-NavigationBar_Bottom_Y) collectionViewLayout:layout];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.backgroundColor = UIColorFromRGB(0xF7F7F7);
        _collectionView.showsVerticalScrollIndicator = NO;
        [_collectionView registerClass:[QJScoreShopCollectionCell class] forCellWithReuseIdentifier:@"QJScoreShopCollectionCell"];
        _collectionView.mj_header = [QJRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(refreshFirst)];
        _collectionView.mj_footer = [QJRefreshFooter footerWithRefreshingTarget:self refreshingAction:@selector(refreshMore)];
    }
    return _collectionView;
}

#pragma mark - UICollectionView
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    QJEmptyCommonView *emptyView = [QJEmptyCommonView new];
    emptyView.backgroundColor = UIColorFromRGB(0xF7F7F7);
    emptyView.string = @"暂无商品";
    emptyView.topSpace = 100*kWScale;
    [collectionView collectionViewDisplayWhenHaveNoDataWithView:emptyView ifNecessaryForRowCount:self.pageModel.records.count];

//    [collectionView collectionViewDisplayWhenHaveNoDataWithMsg:@"暂无数据～" centerImage:[UIImage imageNamed:@"icon_empty_no_data"] ifNecessaryForRowCount:self.pageModel.records.count];
    return self.pageModel.records.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    QJScoreShopCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"QJScoreShopCollectionCell" forIndexPath:indexPath];
    QJMallShopHomeDetailModel *model = [self.pageModel.records safeObjectAtIndex:indexPath.item];
    cell.model = model;
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    QJMallShopHomeDetailModel *model = [self.pageModel.records safeObjectAtIndex:indexPath.item];
    QJMallDetailViewController *shopDetailVC = [[QJMallDetailViewController alloc] init];
    if (self.qjIcon.integerValue < model.qjcoinPartPrice.integerValue) {
        shopDetailVC.isInsufficient = YES;
    }else{
        shopDetailVC.isInsufficient = NO;
    }
    shopDetailVC.mallID = model.ID;
    [[QJAppTool getCurrentViewController].navigationController pushViewController:shopDetailVC animated:YES];
}

#pragma mark - GKPageListViewDelegate
- (UIScrollView *)listScrollView {
    return self.collectionView;
}

- (void)listViewDidScrollCallback:(void (^)(UIScrollView * _Nonnull))callback {
    self.listScrollViewScrollBlock = callback;
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    !self.listScrollViewScrollBlock ? : self.listScrollViewScrollBlock(scrollView);
}

#pragma mark - 请求方法
/**
 * @author: zjr
 * @date: 2022-8-2
 * @desc: 请求方法
 */
- (void)refreshFirst{
    _pageModel.willLoadMore = NO;
    [self sendRequest];
}

- (void)refreshMore{
    if(_pageModel.hasNext){
        _pageModel.willLoadMore=YES;
        [self sendRequest];
    }else{
        [self.collectionView.mj_footer endRefreshing];
        [self.collectionView.mj_footer endRefreshingWithNoMoreData];
    }
}

- (void)sendRequest {
    DLog(@"searchKey = %@",self.searchKey);
    QJMallShopHomeRequset *productReq = [[QJMallShopHomeRequset alloc] init];
    NSMutableDictionary *muDic = [NSMutableDictionary dictionaryWithDictionary:_pageModel.toParams];
    [muDic setValue:@"20" forKey:@"size"];
    if (self.searchKey) {
        [muDic setValue:self.searchKey forKey:@"sort"];
    }
    if (self.type && self.type.length > 0) {
        [muDic setValue:self.type forKey:@"type"];
    }
    productReq.dic = muDic.copy;
    [productReq getMallShopListRequest];

    WS(weakSelf)
    productReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            [weakSelf.pageModel configObj:[QJMallShopHomeListModel modelWithDictionary:EncodeDicFromDic(request.responseObject, @"data")]];
        }else{
            NSString *msg = EncodeStringFromDic(request.responseObject, @"message");
            [weakSelf makeToast:msg];
        }
        [weakSelf.collectionView reloadData];
        [weakSelf.collectionView.mj_header endRefreshing];
        [weakSelf.collectionView.mj_footer endRefreshing];
    };
    productReq.failureCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        [weakSelf.collectionView reloadData];
        [weakSelf.collectionView.mj_header endRefreshing];
        [weakSelf.collectionView.mj_footer endRefreshing];
        [weakSelf makeToast:@"数据异常 请稍后再试"];
    };
}

@end
