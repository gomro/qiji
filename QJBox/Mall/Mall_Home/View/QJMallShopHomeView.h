//
//  QJMallShopHomeView.h
//  QJBox
//
//  Created by rui on 2022/8/24.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJMallShopHomeView : UIView

/* 类型区分 all-全部 realObject-实物 coupon-卡券 */
@property (nonatomic, copy) NSString *type;

@property (nonatomic, copy) NSString *searchKey;

@property (nonatomic, copy) NSString *qjIcon;

- (void)refreshFirst;

@end

NS_ASSUME_NONNULL_END
