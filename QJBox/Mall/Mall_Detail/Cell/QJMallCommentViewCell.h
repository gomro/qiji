//
//  QJMallCommentViewCell.h
//  QJBox
//
//  Created by Sun on 2022/8/29.
//

#import <UIKit/UIKit.h>
#import "QJMallCommentModel.h"

NS_ASSUME_NONNULL_BEGIN
@protocol QJMallCommentViewCellDelegate <NSObject>

/**点击视频**/
- (void)didVideoImageUrl:(NSString *)videoStr;

/**文本展开收起**/
- (void)didShowReloadModel:(QJMallCommentModel *)model;

@end


@interface QJMallCommentViewCell : UITableViewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier cellStyle:(QJMallCommentStyle)cellStyle;
- (void)configureCellWithData:(QJMallCommentModel *)model;
@property (nonatomic, weak) id <QJMallCommentViewCellDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
