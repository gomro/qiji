//
//  QJMallDetailViewCell.m
//  QJBox
//
//  Created by Sun on 2022/8/29.
//

#import "QJMallDetailViewCell.h"
@interface QJMallDetailViewCell ()
/**描述*/
@property (nonatomic, strong) UILabel *titleLabel;
/**图片*/
@property (nonatomic, strong) UIImageView *detailImage;

@property (nonatomic, assign) QJMallDetailViewCellStyle cellStyle;
@end

@implementation QJMallDetailViewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier cellStyle:(QJMallDetailViewCellStyle)cellStyle {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor = [UIColor whiteColor];
        self.cellStyle = cellStyle;
        [self loadUI];
    }
    return self;
}

#pragma mark - Load UI
- (void)loadUI {
    if (self.cellStyle == QJMallDetailViewCellStyleImage) {
        [self.contentView addSubview:self.detailImage];
    } else {
        [self.contentView addSubview:self.titleLabel];
    }
    [self makeConstraints];
}

- (void)makeConstraints {
    if (self.cellStyle == QJMallDetailViewCellStyleImage) {
        [self.detailImage mas_makeConstraints:^(MASConstraintMaker *make) {                    
            make.top.equalTo(self.contentView.mas_top).offset(8);
            make.height.mas_equalTo(Get375Width(193));
            make.bottom.equalTo(self.contentView.mas_bottom);
            make.left.equalTo(self.contentView.mas_left).offset(16);
            make.right.equalTo(self.contentView.mas_right).offset(-16);
        }];
    } else {
        [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.contentView.mas_top).offset(2);
            make.bottom.equalTo(self.contentView.mas_bottom);
            make.left.equalTo(self.contentView.mas_left).offset(16);
            make.right.equalTo(self.contentView.mas_right).offset(-16);
        }];
    }
}

- (UIImageView *)detailImage {
    if (!_detailImage) {
        _detailImage = [UIImageView new];
        _detailImage.layer.masksToBounds = YES;
        _detailImage.layer.cornerRadius = 4;
        _detailImage.userInteractionEnabled = YES;
    }
    return _detailImage;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.font = MYFONTALL(FONT_REGULAR, 15);
        _titleLabel.numberOfLines = 0;
        _titleLabel.textColor = UIColorHex(333333);
    }
    return _titleLabel;
}

- (void)configureCellWithDataConent:(NSString *)conent {
    self.titleLabel.text = conent;
}

- (void)configureCellWithDataImageStr:(NSString *)imageStr {
    NSString *imageString = [kCheckStringNil(imageStr) checkImageUrlString];
    NSURL *imageURL = [NSURL URLWithString:imageString];
    [self.detailImage setImageURL:imageURL];
    
}
@end
