//
//  QJMallDetailViewCell.h
//  QJBox
//
//  Created by Sun on 2022/8/29.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef NS_ENUM(NSInteger, QJMallDetailViewCellStyle){
    QJMallDetailViewCellStyleDefault,    // 文字
    QJMallDetailViewCellStyleImage,      // 图片
};
@interface QJMallDetailViewCell : UITableViewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier cellStyle:(QJMallDetailViewCellStyle)cellStyle;

- (void)configureCellWithDataConent:(NSString *)conent;

- (void)configureCellWithDataImageStr:(NSString *)imageStr;

@end

NS_ASSUME_NONNULL_END
