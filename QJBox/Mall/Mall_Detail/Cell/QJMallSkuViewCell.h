//
//  QJMallSkuViewCell.h
//  QJBox
//
//  Created by Sun on 2022/9/5.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJMallSkuViewCell : UICollectionViewCell
@property (nonatomic, strong) UILabel *label;

+ (CGSize) getSizeWithText:(NSString*)text;
- (void)name:(NSString *)text;
@end

NS_ASSUME_NONNULL_END
