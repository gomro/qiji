//
//  QJMallSkuViewCell.m
//  QJBox
//
//  Created by Sun on 2022/9/5.
//

#import "QJMallSkuViewCell.h"

@implementation QJMallSkuViewCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self.contentView addSubview:self.label];
    }
    return self;
}

- (UILabel *)label {
    if (!_label) {
        self.label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 10, 30)];
        self.label.font = MYFONTALL(FONT_MEDIUM, 14);
        self.label.textColor = RGB(172, 172, 172);
        self.label.text = @"测试测试";
        self.label.textAlignment = NSTextAlignmentCenter;
        self.label.backgroundColor = [UIColor whiteColor];
        self.label.layer.borderColor = RGB(236, 236, 236).CGColor;
        self.label.layer.borderWidth = 1;
        self.label.layer.cornerRadius = 4;
    }
    return _label;
}

+ (CGSize)getSizeWithText:(NSString*)text; {
    NSMutableParagraphStyle* style = [[NSMutableParagraphStyle alloc] init];
    style.lineBreakMode = NSLineBreakByCharWrapping;
    CGSize size = [text boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, 30) options: NSStringDrawingUsesLineFragmentOrigin   attributes:@{NSFontAttributeName:MYFONTALL(FONT_MEDIUM, 14), NSParagraphStyleAttributeName:style} context:nil].size;
    return CGSizeMake((int)size.width + 40, 30);
}

- (void)name:(NSString *)text {
    NSMutableParagraphStyle* style = [[NSMutableParagraphStyle alloc] init];
    style.lineBreakMode = NSLineBreakByCharWrapping;
    CGSize size = [text boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, 30) options: NSStringDrawingUsesLineFragmentOrigin   attributes:@{NSFontAttributeName:MYFONTALL(FONT_MEDIUM, 14),NSParagraphStyleAttributeName:style} context:nil].size;
    self.label.frame = CGRectMake(0, 0, (int)size.width + 30, 30);
    self.label.text = text;
}

@end
