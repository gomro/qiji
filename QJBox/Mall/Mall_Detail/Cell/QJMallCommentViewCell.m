//
//  QJMallCommentViewCell.m
//  QJBox
//
//  Created by Sun on 2022/8/29.
//

#import "QJMallCommentViewCell.h"
#import "QJMallCommentUserView.h"
#import "QJMallOrderDetailCommentView.h"
#import "HZPhotoBrowser.h"


@interface QJMallCommentViewCell ()<HZPhotoBrowserDelegate>
/**用户*/
@property (nonatomic, strong) QJMallCommentUserView *userView;
/**订单详情的用户区域*/
@property (nonatomic, strong) QJMallOrderDetailCommentView *mineCommentView;
/**时间*/
@property (nonatomic, strong) UILabel *timeLb;
/**描述*/
@property (nonatomic, strong) YYLabel *titleLabel;
/**图片1*/
@property (nonatomic, strong) UIImageView *oneImage;
/**图片2*/
@property (nonatomic, strong) UIImageView *twoImage;
/**图片3*/
@property (nonatomic, strong) UIImageView *threeImage;
/**图片更多*/
@property (nonatomic, strong) UILabel *numLb;
/**视频图标*/
@property (nonatomic, strong) UIImageView *videoPlayer;

@property (strong, nonatomic) MASConstraint *topConstraint;

@property (nonatomic, strong) QJMallCommentModel *dataModel;

@property (nonatomic, assign) QJMallCommentStyle cellStyle;
@end
@implementation QJMallCommentViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier cellStyle:(QJMallCommentStyle)cellStyle {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor = [UIColor whiteColor];
        self.cellStyle = cellStyle;
        [self loadUI];
    }
    return self;
}

#pragma mark - Load UI
- (void)loadUI {
    [self.contentView addSubview:self.userView];
    [self.contentView addSubview:self.mineCommentView];
    [self.contentView addSubview:self.timeLb];
    [self.contentView addSubview:self.titleLabel];
    [self.contentView addSubview:self.oneImage];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(playNow)];

    if (self.cellStyle == QJMallCommentStyleTwoImage) {
        [self.contentView addSubview:self.twoImage];
    } else if (self.cellStyle == QJMallCommentStyleMoreImage) {
        [self.contentView addSubview:self.twoImage];
        [self.contentView addSubview:self.threeImage];
        [self.threeImage addSubview:self.numLb];
    } else if (self.cellStyle == QJMallCommentStyleOnlyVideo) {
        [self.oneImage addSubview:self.videoPlayer];
        [self.oneImage addGestureRecognizer:tap];
    } else if (self.cellStyle == QJMallCommentStyleImageVideo) {
        [self.contentView addSubview:self.twoImage];
        [self.oneImage addSubview:self.videoPlayer];
        [self.oneImage addGestureRecognizer:tap];
    } else if (self.cellStyle == QJMallCommentStyleImagesAndVideo) {
        [self.contentView addSubview:self.twoImage];
        [self.contentView addSubview:self.threeImage];
        [self.oneImage addSubview:self.videoPlayer];
        [self.threeImage addSubview:self.numLb];
        [self.oneImage addGestureRecognizer:tap];
    }
    [self makeConstraints];
    
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(playNow)];
    [self.videoPlayer addGestureRecognizer:tap1];
}

- (void)makeConstraints {
    [self.userView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top);
        make.left.equalTo(self.contentView.mas_left).offset(16);
        make.height.mas_equalTo(36*kWScale);
    }];
    
    [self.mineCommentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.contentView);
        make.height.mas_equalTo(71*kWScale);
    }];
    
    [self.timeLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top);
        make.right.equalTo(self.contentView.mas_right).offset(-16);
    }];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        self.topConstraint = make.top.equalTo(self.userView.mas_bottom).offset(4);
        make.left.equalTo(self.contentView.mas_left).offset(16);
        make.right.equalTo(self.contentView.mas_right).offset(-16);
    }];
        
    if (self.cellStyle == QJMallCommentStyleOneImage) {
        [self.oneImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.titleLabel.mas_bottom).offset(4);
            make.left.equalTo(self.contentView.mas_left).offset(16);
            make.width.height.mas_equalTo(Get375Width(220));
            make.bottom.equalTo(self.contentView.mas_bottom).offset(-20);
        }];
    } else if (self.cellStyle == QJMallCommentStyleTwoImage) {
        CGFloat width = (kScreenWidth - 40) / 2.0;
        [self.oneImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.titleLabel.mas_bottom).offset(4);
            make.left.equalTo(self.contentView.mas_left).offset(16);
            make.width.height.mas_equalTo(width);
            make.bottom.equalTo(self.contentView.mas_bottom).offset(-20);
        }];
        
        [self.twoImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.contentView.mas_right).offset(-16);
            make.width.height.mas_equalTo(width);
            make.centerY.equalTo(self.oneImage);
        }];
    } else if (self.cellStyle == QJMallCommentStyleMoreImage) {
        CGFloat width = kScreenWidth / 3.0 - 15;
        [self.oneImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.titleLabel.mas_bottom).offset(4);
            make.left.equalTo(self.contentView.mas_left).offset(16);
            make.width.height.mas_equalTo(width);
            make.bottom.equalTo(self.contentView.mas_bottom).offset(-20);
        }];
        
        [self.twoImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.height.mas_equalTo(width);
            make.centerX.equalTo(self.contentView);
            make.centerY.equalTo(self.oneImage);
        }];
        
        [self.threeImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.contentView.mas_right).offset(-16);
            make.width.height.mas_equalTo(width);
            make.centerY.equalTo(self.oneImage);
        }];
        
        [self.numLb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.threeImage.mas_right).offset(-4);
            make.bottom.equalTo(self.threeImage.mas_bottom).offset(-5);
        }];
    }  else if (self.cellStyle == QJMallCommentStyleOnlyVideo) {
        CGFloat width = kScreenWidth - 32;
        CGFloat height = (172.0 * width) / 343.0;
        [self.oneImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.titleLabel.mas_bottom).offset(4);
            make.left.equalTo(self.contentView.mas_left).offset(16);
            make.width.mas_equalTo(width);
            make.height.mas_equalTo(height*kWScale);
            make.bottom.equalTo(self.contentView.mas_bottom).offset(-20);
        }];
        [self.videoPlayer mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(self.oneImage);
            make.width.height.mas_equalTo(40*kWScale);
        }];
    }  else if (self.cellStyle == QJMallCommentStyleImageVideo) {
        CGFloat width = (kScreenWidth - 40) / 2.0;
        [self.oneImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.titleLabel.mas_bottom).offset(4);
            make.left.equalTo(self.contentView.mas_left).offset(16);
            make.width.height.mas_equalTo(width);
            make.bottom.equalTo(self.contentView.mas_bottom).offset(-20);
        }];
        
        [self.twoImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.contentView.mas_right).offset(-16);
            make.width.height.mas_equalTo(width);
            make.centerY.equalTo(self.oneImage);
        }];
        [self.videoPlayer mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(self.oneImage);
            make.width.height.mas_equalTo(40);
        }];
    }  else if (self.cellStyle == QJMallCommentStyleImagesAndVideo) {
        CGFloat widthAll = kScreenWidth - 32;
        CGFloat widthR = (widthAll - 13) / 3.0;
        CGFloat widthL = widthR * 2 +  5;
        
        [self.oneImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.titleLabel.mas_bottom).offset(4);
            make.left.equalTo(self.contentView.mas_left).offset(16);
            make.width.height.mas_equalTo(widthL);
            make.bottom.equalTo(self.contentView.mas_bottom).offset(-20);
        }];
        
        [self.twoImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.contentView.mas_right).offset(-16);
            make.top.equalTo(self.oneImage.mas_top);
            make.width.height.mas_equalTo(widthR);
        }];
        
        [self.threeImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.contentView.mas_right).offset(-16);
            make.bottom.equalTo(self.oneImage.mas_bottom);
            make.width.height.mas_equalTo(widthR);
        }];
        
        [self.numLb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.threeImage.mas_right).offset(-4);
            make.bottom.equalTo(self.threeImage.mas_bottom).offset(-5);
        }];
        [self.videoPlayer mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(self.oneImage);
            make.width.height.mas_equalTo(40);
        }];
    }
}

- (QJMallCommentUserView *)userView {
    if (!_userView) {
        _userView = [QJMallCommentUserView new];
    }
    return _userView;
}

- (QJMallOrderDetailCommentView *)mineCommentView {
    if (!_mineCommentView) {
        _mineCommentView = [QJMallOrderDetailCommentView new];
        [_mineCommentView setHidden:YES];
    }
    return _mineCommentView;
}

- (UILabel *)timeLb {
    if (!_timeLb) {
        _timeLb = [UILabel new];
        _timeLb.font = MYFONTALL(FONT_REGULAR, 10);
        _timeLb.textColor = UIColorHex(919599);
    }
    return _timeLb;
}

- (YYLabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [YYLabel new];
        _titleLabel.font = MYFONTALL(FONT_REGULAR, 14);
        _titleLabel.numberOfLines = 2;
        _titleLabel.textColor = UIColorHex(474849);
        _titleLabel.preferredMaxLayoutWidth = kScreenWidth - 32;
    }
    return _titleLabel;
}

- (UIImageView *)oneImage {
    if (!_oneImage) {
        _oneImage = [UIImageView new];
        _oneImage.layer.masksToBounds = YES;
        _oneImage.layer.cornerRadius = 4;
        _oneImage.userInteractionEnabled = YES;
    }
    return _oneImage;
}

- (UIImageView *)twoImage {
    if (!_twoImage) {
        _twoImage = [UIImageView new];
        _twoImage.layer.masksToBounds = YES;
        _twoImage.layer.cornerRadius = 4;
        _twoImage.userInteractionEnabled = YES;
    }
    return _twoImage;
}

- (UIImageView *)threeImage {
    if (!_threeImage) {
        _threeImage = [UIImageView new];
        _threeImage.layer.masksToBounds = YES;
        _threeImage.layer.cornerRadius = 4;
        _threeImage.userInteractionEnabled = YES;
    }
    return _threeImage;
}

- (UIImageView *)videoPlayer {
    if (!_videoPlayer) {
        _videoPlayer = [UIImageView new];
        _videoPlayer.image = [UIImage imageNamed:@"qj_home_play"];
        _videoPlayer.layer.masksToBounds = YES;
        _videoPlayer.userInteractionEnabled = YES;
    }
    return _videoPlayer;
}

- (UILabel *)numLb {
    if (!_numLb) {
        _numLb = [UILabel new];
        _numLb.font = MYFONTALL(FONT_BOLD, 16);
        _numLb.textColor = UIColorHex(FFFFFF);
    }
    return _numLb;
}

- (void)configureCellWithData:(QJMallCommentModel *)model {
    self.dataModel = model;
    
    if (model.isMineComment) {
        [self.mineCommentView setHidden:NO];
        [self.userView setHidden:YES];
        [self.timeLb setHidden:YES];

        [self.topConstraint uninstall];
        [self.titleLabel mas_updateConstraints:^(MASConstraintMaker *make) {
            self.topConstraint = make.top.equalTo(self.mineCommentView.mas_bottom).offset(4);
        }];

        [self.mineCommentView configureUserViewWithData:model];
    } else {
        [self.userView configureUserViewWithData:model];
    }
    
    NSMutableAttributedString *detailString = [[NSMutableAttributedString alloc] initWithString:kCheckStringNil(model.content)];
    detailString.font = MYFONTALL(FONT_REGULAR, 14);
    detailString.color = UIColorHex(474849);
    self.titleLabel.attributedText = detailString;

    if (model.isShow) {
        [self packUpString];
    } else {
        [self addLableMore];
    }
    self.timeLb.text = kCheckStringNil(model.createTime);
    
    NSString *videoAddress = kCheckStringNil(model.videoAddress);
    NSArray *pictures = model.pictures;
    self.numLb.text = @"";
    if([videoAddress isEqualToString:@""]) {
        NSString *onePic = @"";
        if(pictures.count) {
            onePic = pictures[0];
        }
        self.oneImage.tag = 30;
        UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickImage:)];
        [self.oneImage addGestureRecognizer:tap1];

        self.twoImage.tag = 31;
        UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickImage:)];
        [self.twoImage addGestureRecognizer:tap2];
        
        self.threeImage.tag = 32;
        UITapGestureRecognizer *tap3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickImage:)];
        [self.threeImage addGestureRecognizer:tap3];
            
        [self configImage:kCheckStringNil(onePic) imageView:self.oneImage];
        if(pictures.count == 2) {
            NSString *twoPic = pictures[1];
            [self configImage:kCheckStringNil(twoPic) imageView:self.twoImage];
        } else if(pictures.count >= 3) {
            NSString *twoPic = pictures[1];
            [self configImage:kCheckStringNil(twoPic) imageView:self.twoImage];
            NSString *threePic = pictures[2];
            [self configImage:kCheckStringNil(threePic) imageView:self.threeImage];
            if(pictures.count != 3) {
                self.numLb.text = [NSString stringWithFormat:@"+%ld", pictures.count];
            }
        }
    } else {
        [self configImage:kCheckStringNil(model.videoPicture) imageView:self.oneImage];
        if(pictures.count == 1) {
            NSString *onePic = pictures[0];
            [self configImage:kCheckStringNil(onePic) imageView:self.twoImage];
        } else if(pictures.count >= 2) {
            NSString *onePic = pictures[0];
            [self configImage:kCheckStringNil(onePic) imageView:self.twoImage];
            NSString *twoPic = pictures[1];
            [self configImage:kCheckStringNil(twoPic) imageView:self.threeImage];
            if(pictures.count != 2) {
                self.numLb.text = [NSString stringWithFormat:@"+%ld", pictures.count];
            }
        }

        self.twoImage.tag = 30;
        UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickImage:)];
        [self.twoImage addGestureRecognizer:tap2];
        
        self.threeImage.tag = 31;
        UITapGestureRecognizer *tap3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickImage:)];
        [self.threeImage addGestureRecognizer:tap3];
    }
}

- (void)configImage:(NSString *)imageString imageView:(UIImageView *)image {
    NSString *imageS = [kCheckStringNil(imageString) checkImageUrlString];
    NSURL *imageURL = [NSURL URLWithString:imageS];
    [image setImageWithURL:imageURL placeholder:[UIImage imageNamed:@"Rectangle5891"] options:YYWebImageOptionAllowBackgroundTask completion:nil];
}

- (void)addLableMore {
    self.titleLabel.numberOfLines = 2;
    NSString *moreString = @"... 全部";
    NSMutableAttributedString *mutableAttributedString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@", moreString]];
    NSRange toUserRange = [moreString rangeOfString:@"... "];
    [mutableAttributedString setColor:UIColorHex(474849) range:toUserRange];
    [mutableAttributedString setFont:MYFONTALL(FONT_REGULAR, 14) range:toUserRange];
    NSRange expandRange = [moreString rangeOfString:@"全部"];
    [mutableAttributedString setColor:UIColorHex(007AFF) range:expandRange];
    [mutableAttributedString setFont:MYFONTALL(FONT_REGULAR, 12) range:expandRange];

    //添加点击事件
    YYTextHighlight *toHighlight = [YYTextHighlight new];
    [mutableAttributedString setTextHighlight:toHighlight range:expandRange];
    __weak typeof(self) weakSelf = self;
    toHighlight.tapAction = ^(UIView *containerView, NSAttributedString *text, NSRange range, CGRect rect) {
        weakSelf.dataModel.isShow = YES;
        if (weakSelf.delegate && [weakSelf.delegate respondsToSelector:@selector(didShowReloadModel:)]) {
            [self.delegate didShowReloadModel:weakSelf.dataModel];
        }
    };
    YYLabel *seeMore = [YYLabel new];
    seeMore.attributedText = mutableAttributedString;
    [seeMore sizeToFit];
    
    NSAttributedString *truncationToken = [NSAttributedString attachmentStringWithContent:seeMore contentMode:UIViewContentModeCenter attachmentSize:seeMore.frame.size alignToFont:MYFONTALL(FONT_REGULAR, 14) alignment:YYTextVerticalAlignmentTop];
    self.titleLabel.truncationToken = truncationToken;
}

- (void)playNow {
    if (self.delegate && [self.delegate respondsToSelector:@selector(didVideoImageUrl:)]) {
//        [self.delegate didVideoImageUrl:@"https://xy2.v.netease.com/r/video/20190110/bea8e70d-ffc0-4433-b250-0393cff10b75.mp4"];
        [self.delegate didVideoImageUrl:self.dataModel.videoAddress];

    }
}


- (void)packUpString {
    self.titleLabel.numberOfLines = 0;
    
    NSMutableAttributedString *mutableAttributedString = [self.titleLabel.attributedText mutableCopy];

    NSMutableAttributedString *moreString = [[NSMutableAttributedString alloc] initWithString:@"收起"];
    [moreString setColor:UIColorHex(007AFF)];
    [moreString setFont:MYFONTALL(FONT_REGULAR, 12)];
    //添加点击事件
    YYTextHighlight *toHighlight = [YYTextHighlight new];
    [moreString setTextHighlight:toHighlight range:moreString.rangeOfAll];
    __weak typeof(self) weakSelf = self;
    toHighlight.tapAction = ^(UIView *containerView, NSAttributedString *text, NSRange range, CGRect rect) {
        weakSelf.dataModel.isShow = NO;
        if (weakSelf.delegate && [weakSelf.delegate respondsToSelector:@selector(didShowReloadModel:)]) {
            [self.delegate didShowReloadModel:weakSelf.dataModel];
        }
    };
    [mutableAttributedString appendAttributedString:moreString];
    self.titleLabel.attributedText = mutableAttributedString;
}
#pragma mark - 图片点击
- (void)clickImage:(UITapGestureRecognizer*)gesture {
    NSInteger tag = gesture.view.tag - 30;
    [self touchPictureIndex:tag];
    
}

- (void)touchPictureIndex:(NSInteger)index {
    NSMutableArray *imageArr = [NSMutableArray arrayWithCapacity:0];
    for (NSString *picStr in self.dataModel.pictures) {
        NSString *imageString = [picStr checkImageUrlString];
        [imageArr addObject:imageString];
    }
    HZPhotoBrowser *browser = [[HZPhotoBrowser alloc] init];

    browser.isSavePhoto = YES;
    browser.isFullWidthForLandScape = NO;
    browser.isNeedLandscape = NO;
    browser.imageUrlHeader = @{};
    browser.imageArray = [imageArr copy];
    browser.imageCount = imageArr.count;
    browser.currentImageIndex = (int)index;
    browser.delegate = self;
    [browser show];
}

//// 返回临时占位图片（即原来的小图）
- (UIImage *)photoBrowser:(HZPhotoBrowser *)browser placeholderImageForIndex:(NSInteger)index {

    return [UIImage new];
}

// 返回高质量图片的url
- (NSURL *)photoBrowser:(HZPhotoBrowser *)browser highQualityImageURLForIndex:(NSInteger)index {
    
    NSMutableArray *imageArr = [NSMutableArray arrayWithCapacity:0];
    for (NSString *picStr in self.dataModel.pictures) {
        NSString *imageString = [picStr checkImageUrlString];
        [imageArr addObject:[NSURL URLWithString:imageString]];

    }
    return imageArr[index];
}

@end
