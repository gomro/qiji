//
//  QJMallCommentViewController.h
//  QJBox
//
//  Created by Sun on 2022/8/31.
//

#import "QJBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJMallCommentViewController : QJBaseViewController
@property (nonatomic, strong) NSString *mallID;

@end

NS_ASSUME_NONNULL_END
