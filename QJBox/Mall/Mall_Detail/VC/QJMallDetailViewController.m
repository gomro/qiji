//
//  QJMallDetailViewController.m
//  QJBox
//
//  Created by Sun on 2022/8/26.
//

#import "QJMallDetailViewController.h"
#import "QJMallDetailHeaderView.h"
#import "QJMallDetailViewCell.h"
#import "QJMallCommentViewCell.h"
#import "QJMallCommentHeaderView.h"
#import "QJMallCommentFooterView.h"
#import "QJMallCommentViewController.h"
#import "QJMallSkuView.h"
#import "TYAlertController.h"
#import "QJMallDetailRequest.h"
#import "QJMallDetailModel.h"
#import "QJMallCommentModel.h"
#import "QJMallCommentVideoVC.h"
#import "QJMallConfirmOrderVC.h"
#import "QJConsultDetailWebViewCell.h"

@interface QJMallDetailViewController ()<UITableViewDelegate, UITableViewDataSource, QJMallCommentViewCellDelegate, QJConsultDetailWebViewCellDelegate>
/**导航*/
@property (nonatomic, strong) UIView *navBackView;
/**返回按钮*/
@property (nonatomic, strong) UIButton *navBack;
/**tableView*/
@property (nonatomic, strong) UITableView *tableView;
/**底部*/
@property (nonatomic, strong) UIView *bottomView;
/**兑换*/
@property (nonatomic, strong) UIButton *convertButton;
/**数据*/
@property (nonatomic, strong) QJMallDetailModel *dataModel;

/**comment数据*/
@property (nonatomic, strong) NSMutableArray *dataSource;
/**comment总数*/
@property (nonatomic, copy) NSString *totalComment;
/**comment好评*/
@property (nonatomic, copy) NSString *commentPraise;

@property (nonatomic, assign) CGFloat webHeight;

@end

@implementation QJMallDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavBarHidden:YES];
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.bottomView];
    [self initNavAndTable];
    [self initBottomView];
    self.webHeight = 0;
    [self getMallList];
    [self getMallComment];
    self.totalComment = @"0";
    self.commentPraise = @"100";
}

- (void)initBottomView {
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.left.right.equalTo(self.view);
        make.height.mas_equalTo((Bottom_iPhoneX_SPACE + 56)*kWScale);
    }];
    [self.bottomView addSubview:self.convertButton];
    [self.convertButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.bottomView.mas_top).offset(16);
        make.left.equalTo(self.bottomView.mas_left).offset(37);
        make.right.equalTo(self.bottomView.mas_right).offset(-37);
        make.height.mas_equalTo(40*kWScale);
    }];
    
    if (self.isInsufficient == YES) {
        [self.convertButton setTitle:@"奇迹币不足" forState:UIControlStateNormal];
        self.convertButton.backgroundColor = UIColorHex(C8CACC);
        self.convertButton.userInteractionEnabled = NO;
    }
}

- (void)initNavAndTable {
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.view);
        make.bottom.equalTo(self.bottomView.mas_top);
    }];
    
    [self.view addSubview:self.navBackView];
    [self.navBackView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.view);
        make.height.mas_equalTo(NavigationBar_Bottom_Y);
    }];

    UIView *backView = [[UIView alloc] initWithFrame:CGRectMake(0, [QJDeviceConstant sysStatusBarHeight], kScreenWidth, 44)];
    [self.navBackView addSubview:backView];

    [backView addSubview:self.navBack];
    [self.navBack mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(30);
        make.height.mas_equalTo(30);
        make.centerY.equalTo(backView);
        make.left.equalTo(self.view).offset(16);
    }];
}

#pragma mark - Lazy Loading
- (UITableView *)tableView {
    if (!_tableView) {
        self.tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        self.tableView.backgroundColor = [UIColor whiteColor];
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.tableView.showsVerticalScrollIndicator = NO;
        self.tableView.showsHorizontalScrollIndicator = NO;
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        self.tableView.estimatedRowHeight = 500;
        if (@available(iOS 11.0, *)) {
            self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
    }
    return _tableView;
}
- (UIView *)navBackView {
    if (!_navBackView) {
        _navBackView = [UIView new];
    }
    return _navBackView;
}

- (UIButton *)navBack {
    if (!_navBack) {
        _navBack = [UIButton new];
        [_navBack setImage:[UIImage imageNamed:@"backImage_white"] forState:UIControlStateNormal];
        [_navBack addTarget:self action:@selector(popView) forControlEvents:UIControlEventTouchUpInside];
    }
    return _navBack;
}

- (UIView *)bottomView {
    if (!_bottomView) {
        _bottomView = [UIView new];
        _bottomView.backgroundColor = [UIColor whiteColor];
        _bottomView.layer.shadowColor = UIColorHex(CECECE40).CGColor;
        _bottomView.layer.shadowOpacity = 1;
        _bottomView.layer.shadowRadius = 6;
        _bottomView.layer.shadowOffset = CGSizeMake(0, -6);
    }
    return _bottomView;
}

- (UIButton *)convertButton {
    if (!_convertButton) {
        _convertButton = [UIButton new];
        [_convertButton setTitle:@"立即兑换" forState:UIControlStateNormal];
        [_convertButton setTitleColor:UIColorHex(FFFFFF) forState:UIControlStateNormal];
        _convertButton.titleLabel.font = MYFONTALL(FONT_BOLD, 12);
        _convertButton.layer.cornerRadius = 4;
        _convertButton.backgroundColor = UIColorHex(1F2A4D);
        [_convertButton addTarget:self action:@selector(convertMall) forControlEvents:UIControlEventTouchUpInside];
    }
    return _convertButton;
}

- (NSMutableArray *)dataSource {
    if(!_dataSource) {
        _dataSource = [NSMutableArray arrayWithCapacity:0];
    }
    return _dataSource;
}
#pragma mark - 跳转
- (void)convertMall {
    if(self.dataModel && self.dataModel.propertyMetadata.count) {
        QJMallSkuView *skuView = [[QJMallSkuView alloc] initWithFrame:CGRectZero];
        [skuView configureTopViewWithData:self.dataModel];
        TYAlertController *alertController = [TYAlertController alertControllerWithAlertView:skuView preferredStyle:TYAlertControllerStyleActionSheet];
        
        skuView.closeClick = ^{
            [[QJAppTool getCurrentViewController] dismissViewControllerAnimated:YES completion:nil];
        };
        MJWeakSelf
        skuView.skuFinish = ^(NSString * _Nonnull skuId) {
            [[QJAppTool getCurrentViewController] dismissViewControllerAnimated:YES completion:nil];
            [weakSelf pushOrderView:kCheckStringNil(skuId) display:YES];
        };
        
        alertController.backgoundTapDismissEnable = YES;
        [[QJAppTool getCurrentViewController] presentViewController:alertController animated:YES completion:nil];
    } else {
        if (self.dataModel.skus.count) {
            QJMallSkuDetailModel *skuModel = self.dataModel.skus[0];
            if(!skuModel.stock) {
                [[QJAppTool getCurrentViewController].view makeToast:@"暂无库存"];
                return;
            }
            [self pushOrderView:kCheckStringNil(skuModel.skuId) display:NO];
        }
    }
}

- (void)pushOrderView:(NSString *)skuId display:(BOOL)display {
    QJMallConfirmOrderVC *orderVC = [[QJMallConfirmOrderVC alloc] init];
    orderVC.cartNo = skuId;
    if(display) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.navigationController pushViewController:orderVC animated:YES];
        });
    } else {
        [self.navigationController pushViewController:orderVC animated:YES];
    }
}

- (void)popView {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableViewDelegate,UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == 0) {
        return 1;
    }
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        NSString *cellMineList = @"cellDetailWebId";
        QJConsultDetailWebViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellMineList];
        if (!cell) {
            cell = [[QJConsultDetailWebViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellMineList];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cell.delegate = self;
        NSString *htmlContent = self.dataModel.content;
        [cell configureCellWithDataModel:htmlContent];
        return cell;
    } else {
        NSString *cellMallComment = @"cellMallCommentId";
        QJMallCommentStyle style = QJMallCommentStyleOneImage;
        QJMallCommentModel *model = self.dataSource[indexPath.row];
        style = model.commentStyle;
        cellMallComment = [NSString stringWithFormat:@"cellMallCommentId%ld", style];
        QJMallCommentViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellMallComment];
        if (!cell) {
            cell = [[QJMallCommentViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellMallComment cellStyle:style];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cell.delegate = self;
        [cell configureCellWithData:model];
        return cell;
    }

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    if (indexPath.section == 0 && indexPath.row != 0) {
//        return Get375Width(193) + 8;
//    }
    if (indexPath.section == 0) {
        return self.webHeight;
    }
    return UITableViewAutomaticDimension;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        QJMallDetailHeaderView *headerView = [[QJMallDetailHeaderView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 208*kWScale + kScreenWidth)];
        [headerView configureTopViewWithData:self.dataModel];
        return headerView;
    } else {
        if(self.dataModel.showComment) {
            QJMallCommentHeaderView *headerView = [[QJMallCommentHeaderView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 80*kWScale)];
            [headerView configureHeaderViewWithTotal:self.totalComment praise:self.commentPraise];
            MJWeakSelf
            headerView.clickButton = ^{
                [weakSelf pushMoreComment];
            };
            return headerView;
        } else {
            UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 0.001)];

            return headerView;
        }
  
    }

}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return 208*kWScale + kScreenWidth;
    } else {
        if(self.dataModel.showComment) {
            return 80*kWScale;
        } else {
            return 0.01;
        }
    }
    
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    if (section == 1 && self.dataModel.showComment) {
        CGFloat height = 120 *kWScale;
        if(!self.dataSource.count) {
            height = 320 *kWScale;
        }
        QJMallCommentFooterView *footerView = [[QJMallCommentFooterView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, height)];
        if(self.dataSource.count) {
            footerView.moreButton.hidden = NO;
            footerView.emptyView.hidden = YES;
        } else {
            footerView.moreButton.hidden = YES;
            footerView.emptyView.hidden = NO;
        }
        MJWeakSelf
        footerView.clickButton = ^{
            [weakSelf pushMoreComment];
        };
        return footerView;
    }
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 0.001)];

    return footerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (section == 1 && self.dataModel.showComment) {
        CGFloat height = 120 *kWScale;
        if(!self.dataSource.count) {
            height = 320 *kWScale;
        }
        return height;
    }
    return 0.001;
}

- (void)pushMoreComment {
    QJMallCommentViewController *commentVC = [[QJMallCommentViewController alloc] init];
    commentVC.mallID = self.mallID;
    [self.navigationController pushViewController:commentVC animated:YES];
}
#pragma mark - QJConsultDetailWebViewCellDelegate
- (void)changeWebViewHeight:(CGFloat)height {
    self.webHeight = height;
    [self.tableView reloadData];
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (scrollView == self.tableView) {
        UIColor *navColor = [UIColor whiteColor];
        CGFloat alpha = MIN(0.8, 1 - (NavigationBar_Bottom_Y - self.tableView.contentOffset.y)/NavigationBar_Bottom_Y);
        self.navBackView.backgroundColor = [navColor colorWithAlphaComponent:alpha];
        if (alpha > 0.5) {
            [_navBack setImage:[UIImage imageNamed:@"backimage_black"] forState:UIControlStateNormal];
        } else {
            [_navBack setImage:[UIImage imageNamed:@"backImage_white"] forState:UIControlStateNormal];
        }
    }
}

#pragma mark - data

- (void)getMallList {
    QJMallDetailRequest *startRequest = [[QJMallDetailRequest alloc] init];
    [startRequest netWorkGetMallDetailWithId:kCheckStringNil(self.mallID)];
    [startRequest setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            NSDictionary *dataDic = request.responseJSONObject[@"data"];
            QJMallDetailModel *model = [QJMallDetailModel modelWithDictionary:dataDic];
            self.dataModel = model;
            self.commentPraise = self.dataModel.goodRatio;
            if (self.dataModel.totalStock == 0) {
                [self.convertButton setTitle:@"已兑完" forState:UIControlStateNormal];
                self.convertButton.backgroundColor = UIColorHex(C8CACC);
                self.convertButton.userInteractionEnabled = NO;
            }
            self.commentPraise = kCheckStringNil(self.dataModel.goodRatio);
            [self.tableView reloadData];
        }
    }];
    [startRequest setFailureCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
     
    }];
}

- (void)getMallComment {
    QJMallDetailRequest *startRequest = [[QJMallDetailRequest alloc] init];
    [startRequest netWorkGetMallCommentWithId:kCheckStringNil(self.mallID) page:1 size:3];
    [startRequest setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            [self.dataSource removeAllObjects];
            NSArray *dataArr = request.responseJSONObject[@"data"][@"records"];
            for (NSDictionary *dic in dataArr) {
                QJMallCommentModel *model = [QJMallCommentModel QJMallCommentWithDictionary:dic];
                [self.dataSource addObject:model];
            }
            NSString *totalString = request.responseJSONObject[@"data"][@"total"];
            self.totalComment = kCheckStringNil(totalString);
            
            [self.tableView reloadData];
        }
    }];
    [startRequest setFailureCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
     
    }];
}

#pragma mark - QJMallCommentViewCellDelegate
- (void)didShowReloadModel:(QJMallCommentModel *)model {    
    NSMutableArray *arr = [self.dataSource mutableCopy];
    NSInteger row = 0;
    for (NSInteger i = 0; i < self.dataSource.count; i++) {
        QJMallCommentModel *modelComment = self.dataSource[i];
        if ([modelComment.mallCommentID isEqualToString:model.mallCommentID]) {
            arr[i] = modelComment;
            row = i;
        }
    }
    self.dataSource = arr;
    [self.tableView reloadRow:row inSection:1 withRowAnimation:UITableViewRowAnimationFade];
}

- (void)didVideoImageUrl:(NSString *)videoStr {
    NSURL *videoUrl = [NSURL URLWithString:videoStr];
    
    QJMallCommentVideoVC *videoPlayer = [[QJMallCommentVideoVC alloc] init];
    videoPlayer.videoUrl = videoUrl;
    [self.navigationController pushViewController:videoPlayer animated:YES];
}
@end

