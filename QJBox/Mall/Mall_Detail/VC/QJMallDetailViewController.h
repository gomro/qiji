//
//  QJMallDetailViewController.h
//  QJBox
//
//  Created by Sun on 2022/8/26.
//

#import "QJBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJMallDetailViewController : QJBaseViewController

@property (nonatomic, strong) NSString *mallID;
@property (nonatomic, assign) BOOL isInsufficient;

@end

NS_ASSUME_NONNULL_END
