//
//  QJMallCommentViewController.m
//  QJBox
//
//  Created by Sun on 2022/8/31.
//

#import "QJMallCommentViewController.h"
#import "QJMallCommentViewCell.h"
#import <AVKit/AVKit.h>
#import "QJMallCommentVideoVC.h"
#import "QJMallCommentModel.h"
#import "QJMallDetailRequest.h"

@interface QJMallCommentViewController ()<UITableViewDelegate, UITableViewDataSource, QJMallCommentViewCellDelegate>

/**tableView*/
@property (nonatomic, strong) UITableView *tableView;

/**comment数据*/
@property (nonatomic, strong) NSMutableArray *dataSource;
@property (nonatomic, assign) NSInteger page;

@end

@implementation QJMallCommentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"全部评论";
    self.view.backgroundColor = [UIColor whiteColor];
    [self initNavAndTable];
    [self getMallComment];
    self.tableView.mj_header = [QJRefreshHeader headerWithRefreshingBlock:^{
        self.page = 1;
        [self getMallComment];
    }];
    self.tableView.mj_footer = [QJRefreshFooter footerWithRefreshingBlock:^{
        [self getMallComment];
    }];
}

- (void)initNavAndTable {
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.top.equalTo(self.view.mas_top).offset(NavigationBar_Bottom_Y);
    }];

}

#pragma mark - Lazy Loading
- (UITableView *)tableView {
    if (!_tableView) {
        self.tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        self.tableView.backgroundColor = [UIColor whiteColor];
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.tableView.showsVerticalScrollIndicator = NO;
        self.tableView.showsHorizontalScrollIndicator = NO;
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        self.tableView.estimatedRowHeight = 100;
        if (@available(iOS 11.0, *)) {
            self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
    }
    return _tableView;
}

- (NSMutableArray *)dataSource {
    if(!_dataSource) {
        _dataSource = [NSMutableArray arrayWithCapacity:0];
    }
    return _dataSource;
}

#pragma mark - UITableViewDelegate,UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    QJEmptyCommonView *emptyView = [QJEmptyCommonView new];
    emptyView.string = @"还没有收到任何评论哦~";
    emptyView.emptyImage = @"empty_no_data";
    emptyView.topSpace = 100;
    [tableView tableViewDisplayWhenHaveNoDataWithView:emptyView ifNecessaryForRowCount:self.dataSource.count];
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *cellMallComment = @"cellMallCommentId";
    QJMallCommentStyle style = QJMallCommentStyleOneImage;
    QJMallCommentModel *model = self.dataSource[indexPath.row];
    style = model.commentStyle;
    cellMallComment = [NSString stringWithFormat:@"cellMallCommentId%ld", style];
    QJMallCommentViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellMallComment];
    if (!cell) {
        cell = [[QJMallCommentViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellMallComment cellStyle:style];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.delegate = self;
    [cell configureCellWithData:model];
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {

    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 0.001)];

    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {

    return 0.001;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {

    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 0.001)];

    return footerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {

    return 0.001;
}

#pragma mark - data

- (void)getMallComment {
    QJMallDetailRequest *startRequest = [[QJMallDetailRequest alloc] init];
    [startRequest netWorkGetMallCommentWithId:kCheckStringNil(self.mallID) page:self.page size:10];
    [startRequest setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {

            if (self.page == 1) {
                [self.dataSource removeAllObjects];
                [self.tableView.mj_footer resetNoMoreData];
                [self.tableView.mj_header endRefreshing];
            } else {
                [self.tableView.mj_footer endRefreshing];
            }
            BOOL hasNext = [request.responseJSONObject[@"data"][@"hasNext"] boolValue];
            if (hasNext) {
                self.page += 1;
            } else {
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
            }

            NSArray *dataArr = request.responseJSONObject[@"data"][@"records"];
            for (NSDictionary *dic in dataArr) {
                QJMallCommentModel *model = [QJMallCommentModel QJMallCommentWithDictionary:dic];
                [self.dataSource addObject:model];
            }
            
            [self.tableView reloadData];
        }
    }];
    [startRequest setFailureCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        [self.tableView reloadData];
        if (self.page == 1) {
            [self.tableView.mj_footer resetNoMoreData];
            [self.tableView.mj_header endRefreshing];
        } else {
            [self.tableView.mj_footer endRefreshing];
        }
    }];
}

#pragma mark - QJMallCommentViewCellDelegate
- (void)didShowReloadModel:(QJMallCommentModel *)model {
    NSMutableArray *arr = [self.dataSource mutableCopy];
    NSInteger row = 0;
    for (NSInteger i = 0; i < self.dataSource.count; i++) {
        QJMallCommentModel *modelComment = self.dataSource[i];
        if ([modelComment.mallCommentID isEqualToString:model.mallCommentID]) {
            arr[i] = modelComment;
            row = i;
        }
    }
    self.dataSource = arr;
    [self.tableView reloadRow:row inSection:1 withRowAnimation:UITableViewRowAnimationFade];
}

- (void)didVideoImageUrl:(NSString *)videoStr {
    NSURL *videoUrl = [NSURL URLWithString:videoStr];
    
    QJMallCommentVideoVC *videoPlayer = [[QJMallCommentVideoVC alloc] init];
    videoPlayer.videoUrl = videoUrl;
    [self.navigationController pushViewController:videoPlayer animated:YES];
}

@end

