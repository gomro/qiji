//
//  QJMallCommentVideoVC.m
//  QJBox
//
//  Created by Sun on 2022/9/6.
//

#import "QJMallCommentVideoVC.h"
#import <SJVideoPlayer/SJVideoPlayer.h>

@interface QJMallCommentVideoVC ()
@property (nonatomic, strong) SJVideoPlayer *player;

@end

@implementation QJMallCommentVideoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self _setupViews];
    
    // Do any additional setup after loading the view from its nib.
}

- (void)_setupViews {
    _player = [SJVideoPlayer player];
    [self _removeExtraItems];
    
    _player.URLAsset = [SJVideoPlayerURLAsset.alloc initWithURL:self.videoUrl];
    [self.view addSubview:_player.view];
    [_player.view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.offset(0);
    }];
}

- (BOOL)shouldAutorotate {
    return NO;
}

- (BOOL)prefersHomeIndicatorAutoHidden {
    return YES;
}

///
/// 删除当前demo不需要的item
///
- (void)_removeExtraItems {
    [_player.defaultEdgeControlLayer.bottomAdapter removeItemForTag:SJEdgeControlLayerBottomItem_Full];
    [_player.defaultEdgeControlLayer.bottomAdapter removeItemForTag:SJEdgeControlLayerBottomItem_Separator];
    [_player.defaultEdgeControlLayer.bottomAdapter exchangeItemForTag:SJEdgeControlLayerBottomItem_DurationTime withItemForTag:SJEdgeControlLayerBottomItem_Progress];
    SJEdgeControlButtonItem *durationItem = [_player.defaultEdgeControlLayer.bottomAdapter itemForTag:SJEdgeControlLayerBottomItem_DurationTime];
    durationItem.insets = SJEdgeInsetsMake(8, 16);
    _player.defaultEdgeControlLayer.bottomContainerView.backgroundColor = [UIColor.blackColor colorWithAlphaComponent:0.6];
    _player.defaultEdgeControlLayer.topContainerView.backgroundColor = [UIColor.blackColor colorWithAlphaComponent:0.6];
    [_player.defaultEdgeControlLayer.bottomAdapter reload];
    if (@available(iOS 14.0, *)) {
        _player.defaultEdgeControlLayer.automaticallyShowsPictureInPictureItem = NO;
    }
    
    SJVideoPlayer.update(^(SJVideoPlayerConfigurations * _Nonnull commonSettings) {
        // 注意, 该block将在子线程执行
        commonSettings.resources.progressTrackColor = UIColorHex(FFFFFF4D);
        commonSettings.resources.progressTraceColor = [UIColor whiteColor];
        commonSettings.resources.progressTrackHeight = 1;
        commonSettings.resources.progressThumbSize = 11;
        commonSettings.resources.progressThumbImage = [UIImage imageNamed:@"EllipseProgress"];
    });
    
    
}


@end
