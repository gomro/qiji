//
//  QJMallCommentVideoVC.h
//  QJBox
//
//  Created by Sun on 2022/9/6.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJMallCommentVideoVC : UIViewController
@property (nonatomic, strong) NSURL *videoUrl;
@end

NS_ASSUME_NONNULL_END
