//
//  QJMallSkuView.m
//  QJBox
//
//  Created by Sun on 2022/9/1.
//

#import "QJMallSkuView.h"
#import "QJMallSkuLayout.h"
#import "QJMallSkuHeaderView.h"
#import "QJMallSkuViewCell.h"

@interface QJMallSkuView ()<UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>
/**顶部图片*/
@property (nonatomic, strong) UIImageView *imageView;
/**货币*/
@property (nonatomic, strong) YYLabel *coinsLb;
/**金额*/
@property (nonatomic, strong) YYLabel *priceLb;
/**关闭*/
@property (nonatomic, strong) UIButton *closeButton;
/**底部*/
@property (nonatomic, strong) UIView *bottomView;
/**兑换*/
@property (nonatomic, strong) UIButton *convertButton;
/**UICollectionView*/
@property (nonatomic, strong) UICollectionView *collectionView;
/**flowLayout*/
@property (nonatomic, strong) QJMallSkuLayout *flowLayout;

/**已经选中的规格数组*/
@property (nonatomic ,strong) NSMutableArray *seletedIndexPaths;
/**不可选indexPath数组*/
@property (nonatomic ,strong) NSMutableArray *seletedEnable;

/**已经选中数据*/
@property (nonatomic ,strong) NSMutableDictionary *seletedDic;

/**数据*/
@property (nonatomic, strong) NSMutableArray *dataSource;
/**数据*/
@property (nonatomic, strong) QJMallDetailModel *dataModel;

/**总库存*/
@property (nonatomic, assign) NSInteger stock;

@property (nonatomic ,copy) NSString *skuId;

@end
@implementation QJMallSkuView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight - 162);
        self.backgroundColor = [UIColor whiteColor];
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(16, 16)];
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = self.bounds;
        maskLayer.path = maskPath.CGPath;
        self.layer.mask = maskLayer;
        self.skuId = @"";
        self.stock = 0;
        [self addSubview:self.imageView];
        [self addSubview:self.coinsLb];
        [self addSubview:self.priceLb];
        [self addSubview:self.closeButton];
        [self addSubview:self.bottomView];
        [self addSubview:self.collectionView];
        [self.bottomView addSubview:self.convertButton];
        [self makeConstraints];
        [self initBottomView];
        [self initCollectionView];
    }
    return self;
}
- (void)makeConstraints {
    [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(16);
        make.top.equalTo(self.mas_top).offset(24);
        make.width.height.mas_equalTo(64);
    }];
    
    [self.coinsLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).offset(37);
        make.left.equalTo(self.imageView.mas_right).offset(16);
        make.height.mas_equalTo(20);
    }];
    
    [self.priceLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).offset(37);
        make.left.equalTo(self.coinsLb.mas_right).offset(4);
        make.height.mas_equalTo(20);
    }];
    
    [self.closeButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).offset(20);
        make.right.equalTo(self.mas_right).offset(-16);
        make.height.mas_equalTo(24);
    }];
}

- (void)initBottomView {
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.left.right.equalTo(self);
        make.height.mas_equalTo(Bottom_iPhoneX_SPACE + 56);
    }];
    [self.convertButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.bottomView.mas_top).offset(16);
        make.left.equalTo(self.bottomView.mas_left).offset(37);
        make.right.equalTo(self.bottomView.mas_right).offset(-37);
        make.height.mas_equalTo(40);
    }];
}

- (void)initCollectionView {
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.imageView.mas_bottom).offset(8);
        make.left.equalTo(self.mas_left);
        make.right.equalTo(self.mas_right);
        make.bottom.equalTo(self.bottomView.mas_top);
    }];
    [self.collectionView registerClass:[QJMallSkuViewCell class] forCellWithReuseIdentifier:@"StandardCellID"];
    [self.collectionView registerClass:[QJMallSkuHeaderView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderID"];
    [self.collectionView registerClass:[QJMallSkuFooterView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"FooterID"];
}

#pragma mark - Lazy Loading

- (UIImageView *)imageView {
    if (!_imageView) {
        _imageView = [UIImageView new];
        _imageView.image = [UIImage imageNamed:@"Rectangle5891"];
        _imageView.contentMode = UIViewContentModeScaleAspectFill;
        _imageView.layer.cornerRadius = 4;
        _imageView.clipsToBounds = YES;
        _imageView.userInteractionEnabled = YES;
    }
    return _imageView;
}


- (YYLabel *)coinsLb {
    if (!_coinsLb) {
        _coinsLb = [YYLabel new];
        _coinsLb.textColor = UIColorHex(FF9500);
        _coinsLb.font = MYFONTALL(FONT_MEDIUM, 20);
    }
    return _coinsLb;
}

- (YYLabel *)priceLb {
    if (!_priceLb) {
        _priceLb = [YYLabel new];
        _priceLb.textColor = UIColorHex(FF9500);
        _priceLb.font = MYFONTALL(FONT_MEDIUM, 20);
    }
    return _priceLb;
}

- (UIButton *)closeButton {
    if (!_closeButton) {
        _closeButton = [UIButton new];
        [_closeButton setBackgroundImage:[UIImage imageNamed:@"qj_close_24"] forState:UIControlStateNormal];
        [_closeButton addTarget:self action:@selector(closeView) forControlEvents:UIControlEventTouchUpInside];
    }
    return _closeButton;
}

- (UIView *)bottomView {
    if (!_bottomView) {
        _bottomView = [UIView new];
        _bottomView.backgroundColor = [UIColor whiteColor];
    }
    return _bottomView;
}

- (UIButton *)convertButton {
    if (!_convertButton) {
        _convertButton = [UIButton new];
        [_convertButton setTitle:@"立即兑换" forState:UIControlStateNormal];
        [_convertButton setTitleColor:UIColorHex(FFFFFF) forState:UIControlStateNormal];
        _convertButton.titleLabel.font = MYFONTALL(FONT_BOLD, 12);
        _convertButton.layer.cornerRadius = 4;
        _convertButton.backgroundColor = UIColorHex(1F2A4D);
        [_convertButton addTarget:self action:@selector(convertMall) forControlEvents:UIControlEventTouchUpInside];
    }
    return _convertButton;
}

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:self.flowLayout];
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        _collectionView.alwaysBounceVertical = YES;
        _collectionView.allowsMultipleSelection = YES;
    }
    return _collectionView;
}

- (QJMallSkuLayout *)flowLayout {
    if (!_flowLayout) {
        _flowLayout = [[QJMallSkuLayout alloc] init];
        _flowLayout.minimumLineSpacing = 10.0;
        _flowLayout.minimumInteritemSpacing = 10.0;
        _flowLayout.sectionInset = UIEdgeInsetsMake(0, 16, 0, 10);
    }
    return _flowLayout;
}

- (NSMutableArray *)seletedIndexPaths {
    if(!_seletedIndexPaths) {
        _seletedIndexPaths = [NSMutableArray arrayWithCapacity:0];
    }
    return _seletedIndexPaths;
}

-(NSMutableArray *)seletedEnable {
    if(!_seletedEnable) {
        _seletedEnable = [NSMutableArray arrayWithCapacity:0];
    }
    return _seletedEnable;
}

-(NSMutableArray *)dataSource {
    if(!_dataSource) {
        _dataSource = [NSMutableArray arrayWithCapacity:0];
    }
    return _dataSource;
}

- (NSMutableDictionary *)seletedDic {
    if(!_seletedDic) {
        _seletedDic = [NSMutableDictionary dictionaryWithCapacity:0];
    }
    return _seletedDic;
}

#pragma mark - 跳转
- (void)convertMall {
    if(!self.stock) {
        [[QJAppTool getCurrentViewController].view makeToast:@"暂无库存"];
        return;
    }
    if(self.skuFinish && ![self.skuId isEqualToString:@""]) {
        self.skuFinish(self.skuId);
    }
}

- (void)closeView {
    if (self.closeClick) {
        self.closeClick();
    }
}

#pragma mark - UICollectionViewDataSource, UICollectionDelegate
-(NSInteger) numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return self.dataSource.count;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    QJMallSkuTitleModel *dataModel = self.dataSource[section];
    return dataModel.value.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    QJMallSkuViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"StandardCellID" forIndexPath:indexPath];
    QJMallSkuTitleModel *dataModel = self.dataSource[indexPath.section];
    NSArray *arr = dataModel.value;

    [cell name:arr[indexPath.row]];
    cell.label = cell.label;

    ///不可选
    if ([self.seletedEnable containsObject:indexPath]) {

        cell.userInteractionEnabled = NO;
        cell.label.textColor = RGB(172, 172, 172);
        cell.label.backgroundColor = RGB(236, 236, 236);
        cell.label.layer.borderColor = [UIColor whiteColor].CGColor;
        cell.label.layer.borderWidth = 0.0001;
        cell.label.layer.cornerRadius = 4;
        cell.label.layer.masksToBounds = YES;
    }
    //可选
    else
    {
        cell.label.textColor = UIColorHex(919599);
        cell.label.backgroundColor = UIColorHex(F5F5F5);
        cell.label.layer.borderColor = UIColorHex(F5F5F5).CGColor;
        cell.label.layer.borderWidth = 1;
        cell.label.layer.cornerRadius = 4;
        cell.label.layer.masksToBounds = YES;
        cell.userInteractionEnabled = YES;
    }
    
    //选中
    if ([self.seletedIndexPaths containsObject:indexPath]) {
        cell.userInteractionEnabled = YES;
        cell.label.textColor = UIColorHex(FF9500);
        cell.label.backgroundColor = UIColorHex(FF95001A);
        cell.label.layer.borderColor = UIColorHex(FF9500).CGColor;
        cell.label.layer.borderWidth = 1;
        cell.label.layer.cornerRadius = 4;
        cell.label.layer.masksToBounds = YES;
    }
    

    return cell;
}
//返回每一个cell的尺寸
- (CGSize) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    QJMallSkuTitleModel *dataModel = self.dataSource[indexPath.section];
    NSArray *arr = dataModel.value;
    NSString *text = arr[indexPath.row];
    return [QJMallSkuViewCell getSizeWithText:text];
}


- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    UICollectionReusableView *reusableView = nil;

    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        QJMallSkuHeaderView *header = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderID" forIndexPath:indexPath];
        QJMallSkuTitleModel *dataModel = self.dataSource[indexPath.section];
        [header configureWithTitle:kCheckStringNil(dataModel.name)];
        return header;
    } else if ([kind isEqualToString:UICollectionElementKindSectionFooter]) {
        QJMallSkuFooterView *footer = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"FooterID" forIndexPath:indexPath];
        return footer;
    }
    return reusableView;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    //取出所有选中状态的按钮标题
    //如果已经被选中则取消选中
    
    QJMallSkuTitleModel *dataModel = self.dataSource[indexPath.section];
    NSString *title = kCheckStringNil(dataModel.name);
    NSArray *arr = dataModel.value;
    NSString *name = kCheckStringNil(arr[indexPath.row]);
    if ([self.seletedIndexPaths containsObject:indexPath]) {
//        [self.seletedIndexPaths removeObjectAtIndex:indexPath.section];
//        [self.seletedIndexPaths insertObject:@"0" atIndex:indexPath.section];
//        name = @"";
    } else {
        [self.seletedIndexPaths removeObjectAtIndex:indexPath.section];
        [self.seletedIndexPaths insertObject:indexPath atIndex:indexPath.section];
    }
    self.seletedDic[title] = name;
    [self selectItemNow];
    [self.seletedEnable removeAllObjects];
    [self.collectionView reloadData];
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    return CGSizeMake(kScreenWidth, 50);
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section {

    return CGSizeMake(kScreenWidth, 20);

}

#pragma mark - 数据
- (void)configureTopViewWithData:(QJMallDetailModel *)model {
    self.dataModel = model;
    [self changeHeaderImage:model.mainPicture coinPartPrice:model.qjcoinPartPrice moneyPartPrice:model.moneyPartPrice];
    
    [self.dataSource removeAllObjects];
    [self.dataSource addObjectsFromArray:model.propertyMetadata];
    
    for (NSInteger i = 0; i < self.dataSource.count; i++) {
        NSIndexPath * indexPath = [NSIndexPath indexPathForItem:0 inSection:i];
        [self.seletedIndexPaths addObject:indexPath];
    }
    [self.collectionView reloadData];
}

- (void)changeHeaderImage:(NSString *)imageStr coinPartPrice:(NSString *)coinPartPrice  moneyPartPrice:(NSString *)moneyPartPrice {
    
    NSString *imageString = [kCheckStringNil(imageStr) checkImageUrlString];
    [self.imageView setImageWithURL:[NSURL URLWithString:imageString] placeholder:nil];
    
    NSMutableAttributedString *attributedStr = [[NSMutableAttributedString alloc]initWithString:@""];
    NSMutableAttributedString *spaceString = [[NSMutableAttributedString alloc] initWithString:kCheckStringNil(coinPartPrice)];
    spaceString.font = MYFONTALL(FONT_MEDIUM, 20);
    spaceString.color = UIColorHex(FF9500);
    
    NSMutableAttributedString *detailString = [[NSMutableAttributedString alloc] initWithString:@"奇迹币"];
    detailString.font = MYFONTALL(FONT_REGULAR, 12);
    detailString.color = UIColorHex(FF9500);
    
    [attributedStr appendAttributedString:spaceString];
    [attributedStr appendAttributedString:detailString];
    self.coinsLb.attributedText = attributedStr;
    
    if([moneyPartPrice floatValue]) {
        NSMutableAttributedString *attributedStr = [[NSMutableAttributedString alloc]initWithString:@""];
        NSMutableAttributedString *spaceString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"+%@", moneyPartPrice]];
        spaceString.font = MYFONTALL(FONT_MEDIUM, 20);
        spaceString.color = UIColorHex(FF9500);
        
        NSMutableAttributedString *detailString = [[NSMutableAttributedString alloc] initWithString:@" 元"];
        detailString.font = MYFONTALL(FONT_REGULAR, 12);
        detailString.color = UIColorHex(FF9500);
        
        [attributedStr appendAttributedString:spaceString];
        [attributedStr appendAttributedString:detailString];
        self.priceLb.attributedText = attributedStr;
    }
}

- (void)selectItemNow {
    self.skuId = @"";
    self.stock = 0;
    NSArray *skuArr = self.dataModel.skus;
    for (QJMallSkuDetailModel *model in skuArr) {
        NSArray *properties = model.properties;
        NSInteger selectInt = 0;
        for (QJMallSkuSelectModel *selectModel in properties) {
            NSString *name = kCheckStringNil(selectModel.name);
            NSString *value = kCheckStringNil(selectModel.value);
            NSString *selectValue = self.seletedDic[name];
            if(![kCheckStringNil(selectValue) isEqualToString:@""] && [selectValue isEqualToString:value]) {
                selectInt = selectInt + 1;
            }
        }
        if(selectInt == properties.count) {
            self.skuId = kCheckStringNil(model.skuId);
            self.stock = model.stock;
            [self changeHeaderImage:model.picture coinPartPrice:model.qjcoinPartPrice moneyPartPrice:model.moneyPartPrice];
            break;
        }
    }
}
@end
