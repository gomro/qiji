//
//  QJMallCommentFooterView.m
//  QJBox
//
//  Created by Sun on 2022/8/31.
//

#import "QJMallCommentFooterView.h"
@interface QJMallCommentFooterView ()
@property (nonatomic, strong) UILabel *commentTitle;
@property (nonatomic, strong) UIImageView *moreImage;

@end
@implementation QJMallCommentFooterView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        
        [self addSubview:self.emptyView];

        [self addSubview:self.moreButton];
        [self.moreButton addSubview:self.commentTitle];
        [self.moreButton addSubview:self.moreImage];
        [self makeConstraints];
    }
    return self;
}

- (UIButton *)moreButton {
    if (!_moreButton) {
        _moreButton = [UIButton new];
        _moreButton.backgroundColor = UIColorHex(F9F9F9);
        [_moreButton addTarget:self action:@selector(moreComment) forControlEvents:UIControlEventTouchUpInside];
    }
    return _moreButton;
}

- (UILabel *)commentTitle {
    if (!_commentTitle) {
        _commentTitle = [UILabel new];
        _commentTitle.textColor = UIColorHex(919599);
        _commentTitle.font = MYFONTALL(FONT_MEDIUM, 12);
        _commentTitle.text = @"查看全部评论";
    }
    return _commentTitle;
}

- (QJEmptyCommonView *)emptyView {
    if(!_emptyView) {
        _emptyView = [QJEmptyCommonView new];
        _emptyView.string = @"还没有收到任何评论哦~";
        _emptyView.emptyImage = @"empty_no_data";
        _emptyView.topSpace = 0;

    }
    return _emptyView;
}

- (UIImageView *)moreImage {
    if (!_moreImage) {
        _moreImage = [UIImageView new];
        _moreImage.image = [UIImage imageNamed:@"mall_comment_doubleRight"];
    }
    return _moreImage;
}


- (void)makeConstraints {

    
    [self.emptyView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).offset(10*kWScale);
        make.left.right.bottom.equalTo(self);
    }];
    
    [self.moreButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.mas_bottom);
        make.left.right.equalTo(self);
        make.height.mas_equalTo(46*kWScale);
    }];
    [self.commentTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.moreButton);
        make.centerX.equalTo(self.moreButton.mas_centerX).offset(-10);
    }];
    
    [self.moreImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.moreButton);
        make.left.equalTo(self.commentTitle.mas_right).offset(2);
        make.width.height.mas_equalTo(16*kWScale);
    }];
    
}
- (void)moreComment {
    if (self.clickButton) {
        self.clickButton();
    }
}


@end
