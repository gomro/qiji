//
//  QJMallOrderDetailCommentView.m
//  QJBox
//
//  Created by macm on 2022/10/13.
//

#import "QJMallOrderDetailCommentView.h"
@interface QJMallOrderDetailCommentView ()
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *leftLabel;
@property (nonatomic, strong) UILabel *timeLb;

@end
@implementation QJMallOrderDetailCommentView
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:self.titleLabel];
        [self addSubview:self.leftLabel];
        [self addSubview:self.timeLb];
        [self makeConstraints];
    }
    return self;
}

- (void)makeConstraints {
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(15);
        make.centerX.mas_equalTo(self);
    }];
    [self.leftLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self).mas_offset(16);
        make.top.equalTo(self.titleLabel.mas_bottom).offset(10);
    }];
    [self.timeLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self).mas_offset(-16);
        make.centerY.equalTo(self.leftLabel);
    }];
    
    for (int i = 0; i < 5; i++) {
        UIImageView *starImage = [UIImageView new];
        starImage.tag = 300 + i;
        starImage.image = [UIImage imageNamed:@"mall_comment_startSelected"];
        [self addSubview:starImage];
        [starImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.leftLabel);
            make.left.equalTo(self.leftLabel.mas_right).offset(7 + 16 * i);
            make.width.height.mas_equalTo(12);
        }];
    }
    
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.text = @"我的评价";
        _titleLabel.textColor = UIColorHex(000000);
        _titleLabel.font = MYFONTALL(FONT_MEDIUM, 14);
    }
    return _titleLabel;
}

- (UILabel *)leftLabel {
    if (!_leftLabel) {
        _leftLabel = [UILabel new];
        _leftLabel.text = @"评分";
        _leftLabel.textColor = UIColorHex(717273);
        _leftLabel.font = MYFONTALL(FONT_REGULAR, 14);
    }
    return _leftLabel;
}

- (UILabel *)timeLb {
    if (!_timeLb) {
        _timeLb = [UILabel new];
        _timeLb.font = MYFONTALL(FONT_REGULAR, 10);
        _timeLb.textColor = UIColorHex(919599);
    }
    return _timeLb;
}

- (void)configureUserViewWithData:(QJMallCommentModel *)model {
    NSInteger levelComment = [model.levelComment integerValue];
    self.timeLb.text = kCheckStringNil(model.createTime);

    for (int i = 0; i < 5; i++) {
        UIImageView *startImage = [self viewWithTag:300 + i];
        if(i + 1 <= levelComment) {
            startImage.image = [UIImage imageNamed:@"mall_comment_startSelected"];
        } else {
            startImage.image = [UIImage imageNamed:@"mall_comment_start"];
        }
    }

}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
