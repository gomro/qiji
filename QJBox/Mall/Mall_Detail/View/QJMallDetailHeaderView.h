//
//  QJMallDetailHeaderView.h
//  QJBox
//
//  Created by Sun on 2022/8/26.
//

#import <UIKit/UIKit.h>
#import "QJMallDetailModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJMallDetailHeaderView : UIView
- (void)configureTopViewWithData:(QJMallDetailModel *)model;

@end

NS_ASSUME_NONNULL_END
