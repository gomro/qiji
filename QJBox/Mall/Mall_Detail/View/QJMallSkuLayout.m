//
//  QJMallSkuLayout.m
//  QJBox
//
//  Created by Sun on 2022/9/5.
//

#import "QJMallSkuLayout.h"

@implementation QJMallSkuLayout
// 调整布局左对齐
- (nullable NSArray<__kindof UICollectionViewLayoutAttributes *> *)layoutAttributesForElementsInRect:(CGRect)rect {
    NSArray *array = [super layoutAttributesForElementsInRect:rect];
    NSInteger count = array.count;
    if (count == 0) {
        return array;
    }
    NSMutableArray *attributes = [NSMutableArray array];
    [attributes addObject:[array firstObject]];
    
    for (int i = 0; i < count; i++) {
        if (i == 0) {
            continue;
        }        
        UICollectionViewLayoutAttributes *attrLast = [array objectAtIndex:i - 1];
        UICollectionViewLayoutAttributes *attr = [array objectAtIndex:i];
        CGRect frameLast = attrLast.frame;
        CGRect frame = attr.frame;
        if (frameLast.origin.y == frame.origin.y) {
            frame.origin.x = CGRectGetMaxX(frameLast) + self.minimumInteritemSpacing;
            attr.frame = frame;
        }
        [attributes addObject:attr];
    }
    return attributes;
}
@end
