//
//  QJMallOrderDetailCommentView.h
//  QJBox
//
//  Created by macm on 2022/10/13.
//

#import <UIKit/UIKit.h>
#import "QJMallCommentModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJMallOrderDetailCommentView : UIView
- (void)configureUserViewWithData:(QJMallCommentModel *)model;

@end

NS_ASSUME_NONNULL_END
