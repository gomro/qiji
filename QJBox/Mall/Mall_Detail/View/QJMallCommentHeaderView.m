//
//  QJMallCommentHeaderView.m
//  QJBox
//
//  Created by Sun on 2022/8/30.
//

#import "QJMallCommentHeaderView.h"
@interface QJMallCommentHeaderView ()
/**标题*/
@property (nonatomic, strong) UILabel *commentTitle;
/**分割线*/
@property (nonatomic, strong) UIView *lineView;
/**好评度*/
@property (nonatomic, strong) UIButton *praiseButton;

@end
@implementation QJMallCommentHeaderView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:self.commentTitle];
        [self addSubview:self.lineView];
        [self addSubview:self.praiseButton];
        [self makeConstraints];
    }
    return self;
}

- (UIView *)lineView {
    if (!_lineView) {
        _lineView = [UIView new];
        _lineView.backgroundColor = UIColorHex(F9F9F9);
    }
    return _lineView;
}

- (UILabel *)commentTitle {
    if (!_commentTitle) {
        _commentTitle = [UILabel new];
        _commentTitle.textColor = UIColorHex(000000);
        _commentTitle.font = MYFONTALL(FONT_REGULAR, 16);
    }
    return _commentTitle;
}

- (UIButton *)praiseButton {
    if (!_praiseButton) {
        _praiseButton = [UIButton new];
        [_praiseButton setTitleColor:UIColorHex(474849) forState:UIControlStateNormal];
        _praiseButton.titleLabel.font = MYFONTALL(FONT_REGULAR, 12);
        [_praiseButton addTarget:self action:@selector(moreComment) forControlEvents:UIControlEventTouchUpInside];
    }
    return _praiseButton;
}

- (void)moreComment {
    if (self.clickButton) {
        self.clickButton();
    }
}


- (void)makeConstraints {
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).offset(5 *kWScale);
        make.left.right.equalTo(self);
        make.height.mas_equalTo(8*kWScale);
    }];
    [self.commentTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.lineView.mas_bottom).offset(25*kWScale);
        make.left.equalTo(self.mas_left).offset(16);
        make.height.mas_equalTo(16*kWScale);
    }];
    
    [self.praiseButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.commentTitle);
        make.right.equalTo(self.mas_right).offset(-20);
    }];
    
}

- (void)configureHeaderViewWithTotal:(NSString *)total praise:(NSString *)praise {
    self.commentTitle.text = [NSString stringWithFormat:@"全部评价(%@条)", total];
    [self.praiseButton setTitle:[NSString stringWithFormat:@"好评度(%@%%)", praise] forState:UIControlStateNormal];
}
@end
