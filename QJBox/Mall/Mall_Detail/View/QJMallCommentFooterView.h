//
//  QJMallCommentFooterView.h
//  QJBox
//
//  Created by Sun on 2022/8/31.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void(^ButtonClickBlock)(void);

@interface QJMallCommentFooterView : UIView
@property (nonatomic, copy) ButtonClickBlock clickButton;

@property (nonatomic, strong) UIButton *moreButton;

@property (nonatomic, strong) QJEmptyCommonView *emptyView;
@end

NS_ASSUME_NONNULL_END
