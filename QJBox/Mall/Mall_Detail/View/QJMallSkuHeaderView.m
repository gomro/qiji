//
//  QJMallSkuHeaderView.m
//  QJBox
//
//  Created by Sun on 2022/9/5.
//

#import "QJMallSkuHeaderView.h"

@interface QJMallSkuHeaderView ()
@property (nonatomic, strong) UILabel *titleLb;
@end

@implementation QJMallSkuHeaderView
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self addSubview:self.titleLb];
        [self.titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.mas_top).offset(16);
            make.left.equalTo(self.mas_left).offset(16);
        }];
        
        self.titleLb.text = @"尺寸规格";
    }
    return self;
}

- (UILabel *)titleLb {
    if (!_titleLb) {
        _titleLb = [UILabel new];
        _titleLb.textColor = UIColorHex(16191C);
        _titleLb.font = MYFONTALL(FONT_BOLD, 14);
    }
    return _titleLb;
}

- (void)configureWithTitle:(NSString *)title {
    self.titleLb.text = title;

}
@end


@interface QJMallSkuFooterView ()
@property (nonatomic, strong) UIView *lineView;
@end
@implementation QJMallSkuFooterView
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self addSubview:self.lineView];
        [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.mas_bottom);
            make.left.equalTo(self.mas_left).offset(16);
            make.right.equalTo(self.mas_right).offset(-16);
            make.height.mas_equalTo(1);
        }];
    }
    return self;
}

- (UIView *)lineView {
    if (!_lineView) {
        _lineView = [UIView new];
        _lineView.backgroundColor = UIColorHex(EBEDF0);
    }
    return _lineView;
}
@end
