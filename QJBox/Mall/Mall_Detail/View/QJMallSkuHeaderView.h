//
//  QJMallSkuHeaderView.h
//  QJBox
//
//  Created by Sun on 2022/9/5.
//

#import <UIKit/UIKit.h>


@interface QJMallSkuHeaderView : UICollectionReusableView
- (void)configureWithTitle:(NSString *)title;

@end

@interface QJMallSkuFooterView : UICollectionReusableView

@end
