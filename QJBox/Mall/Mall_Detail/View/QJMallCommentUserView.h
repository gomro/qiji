//
//  QJMallCommentUserView.h
//  QJBox
//
//  Created by Sun on 2022/8/29.
//

#import <UIKit/UIKit.h>
#import "QJMallCommentModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJMallCommentUserView : UIView
- (void)configureUserViewWithData:(QJMallCommentModel *)model;

@end

NS_ASSUME_NONNULL_END
