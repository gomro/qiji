//
//  QJMallCommentHeaderView.h
//  QJBox
//
//  Created by Sun on 2022/8/30.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void(^ButtonClickBlock)(void);


@interface QJMallCommentHeaderView : UIView
- (void)configureHeaderViewWithTotal:(NSString *)total praise:(NSString *)praise;
@property (nonatomic, copy) ButtonClickBlock clickButton;

@end

NS_ASSUME_NONNULL_END
