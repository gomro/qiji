//
//  QJMallCommentUserView.m
//  QJBox
//
//  Created by Sun on 2022/8/29.
//

#import "QJMallCommentUserView.h"

@interface QJMallCommentUserView ()
/**头像*/
@property (nonatomic, strong) UIImageView *imageView;
/**昵称*/
@property (nonatomic, strong) UILabel *nickName;

@end
@implementation QJMallCommentUserView
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:self.imageView];
        [self addSubview:self.nickName];
        [self makeConstraints];
    }
    return self;
}

- (void)makeConstraints {
    [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.equalTo(self);
        make.width.height.mas_equalTo(36);
    }];
    [self.nickName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).offset(3);
        make.left.equalTo(self.imageView.mas_right).offset(7);
        make.right.equalTo(self.mas_right);
        make.height.mas_equalTo(14);
        make.width.mas_lessThanOrEqualTo(88);
    }];
    
    for (int i = 0; i < 5; i++) {
        UIImageView *starImage = [UIImageView new];
        starImage.tag = 300 + i;
        starImage.image = [UIImage imageNamed:@"mall_comment_startSelected"];
        [self addSubview:starImage];
        [starImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.nickName.mas_bottom).offset(4);
            make.left.equalTo(self.imageView.mas_right).offset(7 + 16 * i);
            make.width.height.mas_equalTo(12);
        }];
    }
    
}

- (UIImageView *)imageView {
    if (!_imageView) {
        _imageView = [UIImageView new];
        _imageView.layer.masksToBounds = YES;
        _imageView.layer.cornerRadius = 18;
        _imageView.userInteractionEnabled = YES;
    }
    return _imageView;
}
- (UILabel *)nickName {
    if (!_nickName) {
        _nickName = [UILabel new];
        _nickName.textColor = UIColorHex(474849);
        _nickName.font = MYFONTALL(FONT_REGULAR, 14);
    }
    return _nickName;
}

- (void)configureUserViewWithData:(QJMallCommentModel *)model {
    self.nickName.text = kCheckStringNil(model.userName);
    NSString *imageString = [kCheckStringNil(model.coverImage) checkImageUrlString];
    NSURL *imageURL = [NSURL URLWithString:imageString];
    [self.imageView setImageWithURL:imageURL options:YYWebImageOptionAllowBackgroundTask];
    
    NSInteger levelComment = [model.levelComment integerValue];
    
    for (int i = 0; i < 5; i++) {
        UIImageView *startImage = [self viewWithTag:300 + i];
        if(i + 1 <= levelComment) {
            startImage.image = [UIImage imageNamed:@"mall_comment_startSelected"];
        } else {
            startImage.image = [UIImage imageNamed:@"mall_comment_start"];
        }
    }

}
@end
