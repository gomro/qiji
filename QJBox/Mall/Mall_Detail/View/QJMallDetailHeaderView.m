//
//  QJMallDetailHeaderView.m
//  QJBox
//
//  Created by Sun on 2022/8/26.
//

#import "QJMallDetailHeaderView.h"
#import "SPCycleScrollView.h"
@interface QJMallDetailHeaderView ()<SPCycleScrollViewDelegate>
/**顶部轮播图片*/
@property (nonatomic, strong) SPCycleScrollView *cycleImageView;
/**轮播图地址**/
@property (nonatomic, copy) NSArray *topImageArr;
/**背景图*/
@property (nonatomic, strong) UIView *backView;
/**货币*/
@property (nonatomic, strong) YYLabel *coinsLb;
/**金额*/
@property (nonatomic, strong) YYLabel *priceLb;
/**标题*/
@property (nonatomic, strong) UILabel *titleLb;
/**数量*/
@property (nonatomic, strong) UILabel *numLb;
/**分割线1*/
@property (nonatomic, strong) UIView *lineView1;
/**说明标题*/
@property (nonatomic, strong) UILabel *directionsTitle;
/**说明*/
@property (nonatomic, strong) UILabel *directionsLb;
/**分割线2*/
@property (nonatomic, strong) UIView *lineView2;
/**详情标题  */
@property (nonatomic, strong) UILabel *detailTitle;
/**图片数量**/
@property (nonatomic, strong) UILabel *imageLb;

@end
@implementation QJMallDetailHeaderView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:self.cycleImageView];
        [self addSubview:self.imageLb];
        [self addSubview:self.backView];
        [self.backView addSubview:self.coinsLb];
        [self.backView addSubview:self.priceLb];
        [self.backView addSubview:self.titleLb];
        [self.backView addSubview:self.numLb];
        [self.backView addSubview:self.lineView1];
        [self.backView addSubview:self.directionsTitle];
        [self.backView addSubview:self.directionsLb];
        [self.backView addSubview:self.lineView2];
        [self.backView addSubview:self.detailTitle];
        [self makeConstraints];
    }
    return self;
}

- (void)makeConstraints {
    [self.cycleImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.equalTo(self);
        make.width.height.mas_equalTo(kScreenWidth);
    }];
    [self.imageLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.cycleImageView.mas_right).offset(-24*kWScale);
        make.bottom.equalTo(self.cycleImageView.mas_bottom).offset(-27*kWScale);
        make.height.mas_equalTo(20 * kWScale);
        make.width.mas_equalTo(39 * kWScale);
    }];
    
    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self);
        make.top.equalTo(self.cycleImageView.mas_bottom).offset(-11*kWScale);
    }];
    
    [self.coinsLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.backView.mas_top).offset(16);
        make.left.equalTo(self.backView.mas_left).offset(16);
        make.height.mas_equalTo(Get375Width(20));
    }];
    
    [self.priceLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.backView.mas_top).offset(16*kWScale);
        make.left.equalTo(self.coinsLb.mas_right).offset(4);
        make.height.mas_equalTo(Get375Width(20));
    }];
    
    [self.titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.coinsLb.mas_bottom).offset(8*kWScale);
        make.left.equalTo(self.backView.mas_left).offset(16);
        make.height.mas_equalTo(Get375Width(14));
    }];
    
    [self.numLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLb.mas_bottom).offset(8*kWScale);
        make.left.equalTo(self.backView.mas_left).offset(16);
        make.right.equalTo(self.backView.mas_right).offset(-16);
        make.height.mas_equalTo(Get375Width(14));
    }];
    
    [self.lineView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.numLb.mas_bottom).offset(16*kWScale);
        make.left.right.equalTo(self.backView);
        make.height.mas_equalTo(8*kWScale);
    }];
    
    [self.directionsTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.lineView1.mas_bottom).offset(16*kWScale);
        make.left.equalTo(self.backView.mas_left).offset(16);
        make.height.mas_equalTo(Get375Width(14));
    }];
    
    [self.directionsLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.directionsTitle.mas_bottom).offset(8*kWScale);
        make.left.equalTo(self.backView.mas_left).offset(16);
        make.right.equalTo(self.backView.mas_right).offset(-16);
        make.height.mas_equalTo(Get375Width(21));
    }];
    
    [self.lineView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.directionsLb.mas_bottom).offset(16*kWScale);
        make.left.right.equalTo(self.backView);
        make.height.mas_equalTo(8*kWScale);
    }];
    
    [self.detailTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.lineView2.mas_bottom).offset(16*kWScale);
        make.left.equalTo(self.backView.mas_left).offset(16);
        make.height.mas_equalTo(Get375Width(14));
    }];
    
    [self changeBackRounded];
}

#pragma mark - Lazy Loading
- (SPCycleScrollView *)cycleImageView {
    if (!_cycleImageView) {
        _cycleImageView = [[SPCycleScrollView alloc] init];
        _cycleImageView.duration = 3.0;
        _cycleImageView.autoScroll = NO;
        _cycleImageView.delegate = self;
        _cycleImageView.pageControl.hidden = YES;
    }
    return _cycleImageView;
}

- (UILabel *)imageLb {
    if (!_imageLb) {
        _imageLb = [UILabel new];
        _imageLb.backgroundColor = UIColorHex(00000099);
        _imageLb.textColor = UIColorHex(#FFFFFF);
        _imageLb.font = MYFONTALL(FONT_MEDIUM, 12);
        _imageLb.layer.cornerRadius = 9 * kWScale;
        _imageLb.layer.masksToBounds = YES;
        _imageLb.textAlignment = NSTextAlignmentCenter;
    }
    return _imageLb;
}

- (UIView *)backView {
    if (!_backView) {
        _backView = [UIView new];
        _backView.backgroundColor = [UIColor whiteColor];
    }
    return _backView;
}

- (YYLabel *)coinsLb {
    if (!_coinsLb) {
        _coinsLb = [YYLabel new];
        _coinsLb.textColor = UIColorHex(FF9500);
        _coinsLb.font = MYFONTALL(FONT_MEDIUM, 20);
    }
    return _coinsLb;
}

- (YYLabel *)priceLb {
    if (!_priceLb) {
        _priceLb = [YYLabel new];
        _priceLb.textColor = UIColorHex(FF9500);
        _priceLb.font = MYFONTALL(FONT_MEDIUM, 20);
    }
    return _priceLb;
}

- (UILabel *)titleLb {
    if (!_titleLb) {
        _titleLb = [UILabel new];
        _titleLb.textColor = UIColorHex(16191C);
        _titleLb.font = MYFONTALL(FONT_MEDIUM, 16);
    }
    return _titleLb;
}

- (UILabel *)numLb {
    if (!_numLb) {
        _numLb = [UILabel new];
        _numLb.textColor = UIColorHex(919599);
        _numLb.font = MYFONTALL(FONT_REGULAR, 12);
    }
    return _numLb;
}

- (UIView *)lineView1 {
    if (!_lineView1) {
        _lineView1 = [UIView new];
        _lineView1.backgroundColor = UIColorHex(F9F9F9);
    }
    return _lineView1;
}

- (UILabel *)directionsTitle {
    if (!_directionsTitle) {
        _directionsTitle = [UILabel new];
        _directionsTitle.textColor = UIColorHex(16191C);
        _directionsTitle.font = MYFONTALL(FONT_BOLD, 14);
        _directionsTitle.text = @"兑换说明:";
    }
    return _directionsTitle;
}

- (UILabel *)directionsLb {
    if (!_directionsLb) {
        _directionsLb = [UILabel new];
        _directionsLb.textColor = UIColorHex(474849);
        _directionsLb.font = MYFONTALL(FONT_REGULAR, 14);
        _directionsLb.text = @"兑换须知  商店兑换后不可进行退换";
    }
    return _directionsLb;
}

- (UIView *)lineView2 {
    if (!_lineView2) {
        _lineView2 = [UIView new];
        _lineView2.backgroundColor = UIColorHex(F9F9F9);
    }
    return _lineView2;
}

- (UILabel *)detailTitle {
    if (!_detailTitle) {
        _detailTitle = [UILabel new];
        _detailTitle.textColor = UIColorHex(16191C);
        _detailTitle.font = MYFONTALL(FONT_BOLD, 14);
        _detailTitle.text = @"商品介绍:";
    }
    return _detailTitle;
}

- (void)changeBackRounded {
    [self.backView layoutIfNeeded];
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.backView.bounds byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(8, 8)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.backView.bounds;
    maskLayer.path = maskPath.CGPath;
    self.backView.layer.mask = maskLayer;
}

- (void)configureTopViewWithData:(QJMallDetailModel *)model {
    NSMutableArray *imageArr = [NSMutableArray arrayWithCapacity:0];
    for (NSString *imageStr in model.pictures) {
        NSString *imageString = [kCheckStringNil(imageStr) checkImageUrlString];
        [imageArr addObject:imageString];
    }
    self.topImageArr = [imageArr copy];
    self.cycleImageView.urlImages = [imageArr copy];
    self.imageLb.text = [NSString stringWithFormat:@"1/%ld", self.topImageArr.count];
    
    self.titleLb.text = kCheckStringNil(model.name);
    self.numLb.text = [NSString stringWithFormat:@"已兑%ld/%ld", model.totalSoldNum, model.totalCount];

    NSMutableAttributedString *attributedStr = [[NSMutableAttributedString alloc]initWithString:@""];
    NSMutableAttributedString *spaceString = [[NSMutableAttributedString alloc] initWithString:kCheckStringNil(model.qjcoinPartPrice)];
    spaceString.font = MYFONTALL(FONT_MEDIUM, 20);
    spaceString.color = UIColorHex(FF9500);
    
    NSMutableAttributedString *detailString = [[NSMutableAttributedString alloc] initWithString:@" 奇迹币"];
    detailString.font = MYFONTALL(FONT_REGULAR, 12);
    detailString.color = UIColorHex(FF9500);

    [attributedStr appendAttributedString:spaceString];
    [attributedStr appendAttributedString:detailString];
    self.coinsLb.attributedText = attributedStr;
    if([model.moneyPartPrice floatValue]) {
     
        NSMutableAttributedString *attributedStr = [[NSMutableAttributedString alloc]initWithString:@""];
        NSMutableAttributedString *spaceString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"+%@", model.moneyPartPrice]];
        spaceString.font = MYFONTALL(FONT_MEDIUM, 20);
        spaceString.color = UIColorHex(FF9500);
        
        NSMutableAttributedString *detailString = [[NSMutableAttributedString alloc] initWithString:@" 元"];
        detailString.font = MYFONTALL(FONT_REGULAR, 12);
        detailString.color = UIColorHex(FF9500);

        [attributedStr appendAttributedString:spaceString];
        [attributedStr appendAttributedString:detailString];
        self.priceLb.attributedText = attributedStr;
        
    }
    
    
    _directionsLb.text = kCheckStringNil(model.exchangeExplain);

}

#pragma mark - SPCycleScrollViewDelegate
// 轮播图的图片被点击时触发的代理方法,index为点击的图片下标
- (void)cycleScrollView:(SPCycleScrollView *)cycleScrollView clickedImageAtIndex:(NSUInteger)index {
    
}

/** 图片滚动回调 */
- (void)cycleScrollView:(SPCycleScrollView *)cycleScrollView didScrollToIndex:(NSInteger)index {
    self.imageLb.text = [NSString stringWithFormat:@"%ld/%ld",index + 1, self.topImageArr.count];

}
@end
