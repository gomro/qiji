//
//  QJMallSkuView.h
//  QJBox
//
//  Created by Sun on 2022/9/1.
//

#import <UIKit/UIKit.h>
#import "QJMallDetailModel.h"

NS_ASSUME_NONNULL_BEGIN

typedef void(^ButtonCloseBlock)(void);
typedef void(^selectSkuFinish)(NSString *skuId);

@interface QJMallSkuView : UIView
/**关闭回调*/
@property (nonatomic, copy) ButtonCloseBlock closeClick;

/**立即兑换*/
@property (nonatomic, copy) selectSkuFinish skuFinish;
- (void)configureTopViewWithData:(QJMallDetailModel *)model;

@end

NS_ASSUME_NONNULL_END
