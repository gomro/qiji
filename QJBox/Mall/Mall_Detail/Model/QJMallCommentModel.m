//
//  QJMallCommentModel.m
//  QJBox
//
//  Created by Sun on 2022/8/29.
//

#import "QJMallCommentModel.h"

@implementation QJMallCommentModel
- (id)initWithDictionary:(NSDictionary *)dic {
    self = [super init];
    if (self) {
        [self setValuesForKeysWithDictionary:dic];
        NSString *time = kCheckStringNil(dic[@"createTime"]);
        if (![time isEqualToString:@""]) {
            NSDate *currentDate = [NSString getDateForString:time format:@"yyyy-MM-dd HH:mm:ss"];
            NSString *dateStr = [NSString getStringForDate:currentDate format:@"yyyy-MM-dd"];
            self.createTime = dateStr;
        }
        
        NSString *videoAddress = kCheckStringNil(dic[@"videoAddress"]);
        NSArray *pictures = dic[@"pictures"];
        if([videoAddress isEqualToString:@""]) {
            if(pictures.count == 2) {
                self.commentStyle = QJMallCommentStyleTwoImage;
            } else if(pictures.count >= 3) {
                self.commentStyle = QJMallCommentStyleMoreImage;
            } else {
                self.commentStyle = QJMallCommentStyleOneImage;
            }            
        } else {
            if(pictures.count == 1) {
                self.commentStyle = QJMallCommentStyleImageVideo;
            } else if(pictures.count >= 2) {
                self.commentStyle = QJMallCommentStyleImagesAndVideo;
            } else {
                self.commentStyle = QJMallCommentStyleOnlyVideo;
            }
        }
        
    }
    return self;
}

+ (id)QJMallCommentWithDictionary:(NSDictionary *)dic {
    return [[self alloc] initWithDictionary:dic];
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key {
    if ([key isEqualToString:@"id"]) {
        self.mallCommentID = value;
    }
}
@end
