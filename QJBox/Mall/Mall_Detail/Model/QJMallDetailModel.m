//
//  QJMallDetailModel.m
//  QJBox
//
//  Created by Sun on 2022/9/16.
//

#import "QJMallDetailModel.h"

@implementation QJMallDetailModel

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"detailId" : @"id"};
}

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{
             @"pictures" : [NSString class],
             @"propertyMetadata" : [QJMallSkuTitleModel class],
             @"skus" : [QJMallSkuDetailModel class],
    };
}

@end

@implementation QJMallSkuTitleModel

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{
             @"value" : [NSString class],
    };
}

@end

@implementation QJMallSkuDetailModel

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{
             @"properties" : [QJMallSkuSelectModel class],
    };
}

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"skuId" : @"id"};
}

@end

@implementation QJMallSkuSelectModel


@end
