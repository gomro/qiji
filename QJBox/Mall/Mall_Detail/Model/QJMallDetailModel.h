//
//  QJMallDetailModel.h
//  QJBox
//
//  Created by Sun on 2022/9/16.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJMallDetailModel : NSObject

/**id*/
@property (nonatomic, copy) NSString *detailId;

/**详细描述*/
@property (nonatomic, copy) NSString *content;

/**兑换说明*/
@property (nonatomic, copy) NSString *exchangeExplain;

/**是否展示商品评价*/
@property (nonatomic, assign) BOOL showComment;

@property (nonatomic, copy) NSString *goodRatio;//好评率

/**商品类型: 实物商品=1, 电子卡=2,可用值:ECARD,PHYSICAL_PRODUCT  */
@property (nonatomic, copy) NSString *libProductType;

/**商品主图*/
@property (nonatomic, copy) NSString *mainPicture;

/**金额原价/划线价*/
@property (nonatomic, copy) NSString *moneyOriginPrice;

/**金额部分售价*/
@property (nonatomic, copy) NSString *moneyPartPrice;

/**奇迹币部分售价*/
@property (nonatomic, copy) NSString *qjcoinPartPrice;

/**商品名称*/
@property (nonatomic, copy) NSString *name;

/**总数量*/
@property (nonatomic, assign) NSInteger totalCount;

/**总销量*/
@property (nonatomic, assign) NSInteger totalSoldNum;

/**总库存*/
@property (nonatomic, assign) NSInteger totalStock;

/**商品图列表*/
@property (nonatomic, copy) NSArray *pictures;

/**规格属性元信息*/
@property (nonatomic, copy) NSArray *propertyMetadata;

/**单品SKU列表*/
@property (nonatomic, copy) NSArray *skus;

@end

@interface QJMallSkuTitleModel : NSObject

/**属性名称*/
@property (nonatomic, copy) NSString *name;

/**属性值*/
@property (nonatomic, copy) NSArray *value;

@end

@interface QJMallSkuDetailModel : NSObject

/**属性Id*/
@property (nonatomic, copy) NSString *skuId;

/**金额部分售价*/
@property (nonatomic, copy) NSString *moneyPartPrice;

/**图片*/
@property (nonatomic, copy) NSString *picture;

/**奇迹币部分售价*/
@property (nonatomic, copy) NSString *qjcoinPartPrice;

/**总库存*/
@property (nonatomic, assign) NSInteger stock;

/**属性值*/
@property (nonatomic, copy) NSArray *properties;

@end


@interface QJMallSkuSelectModel : NSObject

/**属性名称*/
@property (nonatomic, copy) NSString *name;

/**属性值*/
@property (nonatomic, copy) NSString *value;

@end

NS_ASSUME_NONNULL_END
