//
//  QJMallCommentModel.h
//  QJBox
//
//  Created by Sun on 2022/8/29.
//

#import <Foundation/Foundation.h>
typedef NS_ENUM(NSInteger, QJMallCommentStyle){
    QJMallCommentStyleOneImage,       // 仅有一张图片
    QJMallCommentStyleTwoImage,       // 两张图
    QJMallCommentStyleMoreImage,      //超过三张图片
    QJMallCommentStyleOnlyVideo,      // 仅有一个视频
    QJMallCommentStyleImageVideo,     // 一个视频一张图
    QJMallCommentStyleImagesAndVideo,  // 一个视频超过两张图
};

NS_ASSUME_NONNULL_BEGIN

@interface QJMallCommentModel : NSObject
/**是否展开*/
@property (nonatomic, assign) BOOL isShow;
/**类型*/
@property (nonatomic, assign) QJMallCommentStyle commentStyle;

/**评论内容*/
@property (nonatomic, copy) NSString *content;
/**评论用户-用户头像    */
@property (nonatomic, copy) NSString *coverImage;
/**评论-展示时间*/
@property (nonatomic, copy) NSString *createTime;
/**id*/
@property (nonatomic, copy) NSString *mallCommentID;
/**评论-星级*/
@property (nonatomic, copy) NSString *levelComment;
/**评论用户-用户名*/
@property (nonatomic, copy) NSString *userName;
/**视频地址*/
@property (nonatomic, copy) NSString *videoAddress;
/**全部图片*/
@property (nonatomic, copy) NSArray *pictures;
/**视频封面图*/
@property (nonatomic, copy) NSString *videoPicture;
/**是否来自已完成订单页的我的评价*/
@property (nonatomic, assign) BOOL isMineComment;

- (id)initWithDictionary:(NSDictionary *)dic;
+ (id)QJMallCommentWithDictionary:(NSDictionary *)dic;

@end

NS_ASSUME_NONNULL_END
