//
//  QJMallDetailRequest.h
//  QJBox
//
//  Created by Sun on 2022/9/16.
//

#import "QJBaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJMallDetailRequest : QJBaseRequest
/**商品详情*/
- (void)netWorkGetMallDetailWithId:(NSString *)detailId;

/**商品评论*/
- (void)netWorkGetMallCommentWithId:(NSString *)detailId page:(NSInteger)page size:(NSInteger)size;
@end

NS_ASSUME_NONNULL_END
