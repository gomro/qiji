//
//  QJMallDetailRequest.m
//  QJBox
//
//  Created by Sun on 2022/9/16.
//

#import "QJMallDetailRequest.h"

@implementation QJMallDetailRequest
- (void)netWorkGetMallDetailWithId:(NSString *)detailId {
    self.dic = @{};
    self.urlStr = [NSString stringWithFormat:@"/shop/qjcoin/product/%@", detailId];
    [self start];
}

- (void)netWorkGetMallCommentWithId:(NSString *)detailId page:(NSInteger)page size:(NSInteger)size {
    self.dic = @{
        @"page":@(page),
        @"size":@(size)
    };
    self.urlStr = [NSString stringWithFormat:@"/comment/prod/%@/root", detailId];
    [self start];
}

@end
