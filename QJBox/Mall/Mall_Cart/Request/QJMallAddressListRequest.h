//
//  QJMallAddressListRequest.h
//  QJBox
//
//  Created by wxy on 2022/8/25.
//

#import "QJBaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

/// 查询地址列表
@interface QJMallAddressListRequest : QJBaseRequest



@end

NS_ASSUME_NONNULL_END
