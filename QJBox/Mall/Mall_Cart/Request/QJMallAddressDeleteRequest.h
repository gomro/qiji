//
//  QJMallAddressDeleteRequest.h
//  QJBox
//
//  Created by wxy on 2022/8/25.
//

#import "QJBaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

/// 删除地址
@interface QJMallAddressDeleteRequest : QJBaseRequest

- (void)deleteAddressWithID:(NSString *)addressID;

@end

NS_ASSUME_NONNULL_END
