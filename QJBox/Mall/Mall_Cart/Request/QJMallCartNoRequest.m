//
//  QJMallCartNoRequest.m
//  QJBox
//
//  Created by wxy on 2022/8/24.
//

#import "QJMallCartNoRequest.h"

@implementation QJMallCartNoRequest
//- (void)orderUnPayDetailID:(NSString *)ID {
//    self.urlStr = [NSString stringWithFormat:@"/shop/order/unPayDetail/%@",ID];
//    [self start];
//}

- (NSString *)requestUrl {
    return @"/shop/order/submit";
}

 

- (YTKRequestMethod)requestMethod {
    
    return YTKRequestMethodPOST;
}


- (YTKRequestSerializerType)requestSerializerType {
    
    return YTKRequestSerializerTypeJSON;
}

@end
