//
//  QJMallAddressAddOrEditRequest.h
//  QJBox
//
//  Created by wxy on 2022/8/25.
//

#import "QJBaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

/// 新建地址
@interface QJMallAddressAddOrEditRequest : QJBaseRequest



@end

NS_ASSUME_NONNULL_END
