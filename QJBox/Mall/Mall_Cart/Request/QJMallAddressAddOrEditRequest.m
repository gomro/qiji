//
//  QJMallAddressAddOrEditRequest.m
//  QJBox
//
//  Created by wxy on 2022/8/25.
//

#import "QJMallAddressAddOrEditRequest.h"

@implementation QJMallAddressAddOrEditRequest


- (NSString *)requestUrl {
    return @"/recAddress/post";
}

- (YTKRequestMethod)requestMethod {
    
    return YTKRequestMethodPOST;
}

- (YTKRequestSerializerType)requestSerializerType {
    
    return YTKRequestSerializerTypeJSON;
}

@end
