//
//  QJMallAddressListRequest.m
//  QJBox
//
//  Created by wxy on 2022/8/25.
//

#import "QJMallAddressListRequest.h"

@implementation QJMallAddressListRequest

- (NSString *)requestUrl {
    return @"/recAddress/list";
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

- (YTKRequestSerializerType)requestSerializerType {
    return YTKRequestSerializerTypeJSON;
}


@end
