//
//  QJMallUnPayRequest.h
//  QJBox
//
//  Created by macm on 2022/9/27.
//

#import "QJBaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJMallUnPayRequest : QJBaseRequest
- (void)orderUnPayDetailID:(NSString *)ID;

@end

NS_ASSUME_NONNULL_END
