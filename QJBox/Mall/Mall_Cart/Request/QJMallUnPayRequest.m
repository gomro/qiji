//
//  QJMallUnPayRequest.m
//  QJBox
//
//  Created by macm on 2022/9/27.
//

#import "QJMallUnPayRequest.h"

@implementation QJMallUnPayRequest
- (void)orderUnPayDetailID:(NSString *)ID {
    self.urlStr = [NSString stringWithFormat:@"/shop/order/unPayDetail/%@",ID];
    [self start];
}

 

- (YTKRequestMethod)requestMethod {
    
    return YTKRequestMethodGET;
}


- (YTKRequestSerializerType)requestSerializerType {
    
    return YTKRequestSerializerTypeJSON;
}

@end
