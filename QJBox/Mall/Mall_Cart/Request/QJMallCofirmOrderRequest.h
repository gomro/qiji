//
//  QJMallCofirmOrderRequest.h
//  QJBox
//
//  Created by wxy on 2022/8/24.
//

#import "QJBaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

/// 提交订单
@interface QJMallCofirmOrderRequest : QJBaseRequest

@end

NS_ASSUME_NONNULL_END
