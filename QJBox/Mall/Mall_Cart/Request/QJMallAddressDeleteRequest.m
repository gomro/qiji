//
//  QJMallAddressDeleteRequest.m
//  QJBox
//
//  Created by wxy on 2022/8/25.
//

#import "QJMallAddressDeleteRequest.h"

@implementation QJMallAddressDeleteRequest

- (void)deleteAddressWithID:(NSString *)addressID{
    self.urlStr = [NSString stringWithFormat:@"/recAddress/%@",addressID];
    [self start];
}

- (YTKRequestMethod)requestMethod {
    
    return YTKRequestMethodDELETE;
}

- (YTKRequestSerializerType)requestSerializerType {
    
    return YTKRequestSerializerTypeJSON;
}

@end
