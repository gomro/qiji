//
//  QJMallCartNoRequest.h
//  QJBox
//
//  Created by wxy on 2022/8/24.
//

#import "QJBaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

/// 请求确认订单
@interface QJMallCartNoRequest : QJBaseRequest


@end

NS_ASSUME_NONNULL_END
