//
//  QJMallConfirmCartModel.h
//  QJBox
//
//  Created by wxy on 2022/8/24.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

/// 车2模型
@interface QJMallConfirmCartModel : NSObject
@property (nonatomic, copy) NSString *idStr;

@end

NS_ASSUME_NONNULL_END
