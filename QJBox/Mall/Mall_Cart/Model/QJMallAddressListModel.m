//
//  QJMallAddressListModel.m
//  QJBox
//
//  Created by rui on 2022/9/5.
//

#import "QJMallAddressListModel.h"

@implementation QJMallAddressListModel

- (instancetype)init{
    self = [super init];
    if (self) {
        _page = [NSNumber numberWithInteger:1];
    }
    return self;
}

+ (NSDictionary *)modelContainerPropertyGenericClass{
    return @{@"records" : [QJMallAddressModel class],
             };
}

- (NSDictionary *)toParams{
    NSDictionary *dict;
    if(_willLoadMore){
        _page=[NSNumber numberWithInteger:_page.integerValue+1];
    }else{
        _page=[NSNumber numberWithInteger:1];
    }
    dict = @{@"page" : _page,
             @"size" : @"20",
    };
    return dict;
}

-(void)configObj:(QJMallAddressListModel *)model{
    self.total=model.total;
    self.current=model.current;
    self.hasNext=model.hasNext;
    
    if (_willLoadMore) {
        [self.records addObjectsFromArray:model.records];
    }else{
        self.records = [NSMutableArray arrayWithArray:model.records];
    }
}

@end
