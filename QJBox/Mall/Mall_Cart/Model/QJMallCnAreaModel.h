//
//  QJMallCnAreaModel.h
//  QJBox
//
//  Created by macm on 2022/9/6.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJMallCnAreaModel : NSObject
@property (nonatomic, copy) NSString *areaCode; //    行政区域代码    string
@property (nonatomic, assign) NSInteger level; //    层级:从0开始    integer(int32)
@property (nonatomic, copy) NSString *name; //    名称    string
@property (nonatomic, copy) NSString *parentCode; //    父级行政区域代码    string

@end

NS_ASSUME_NONNULL_END
