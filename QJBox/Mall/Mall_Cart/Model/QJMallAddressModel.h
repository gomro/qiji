//
//  QJMallAddressModel.h
//  QJBox
//
//  Created by wxy on 2022/8/25.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

/// 地址列表模型
@interface QJMallAddressModel : NSObject

@property (nonatomic, copy) NSString *linkman;

@property (nonatomic, copy) NSString *mobile;

@property (nonatomic, copy) NSString *province;

@property (nonatomic, copy) NSString *provinceAreacode;

@property (nonatomic, copy) NSString *city;

@property (nonatomic, copy) NSString *cityeAreacode;

@property (nonatomic, copy) NSString *area;

@property (nonatomic, copy) NSString *areaAreacode;

@property (nonatomic, copy) NSString *street;

@property (nonatomic, copy) NSString *streetAreacode;

@property (nonatomic, copy) NSString *detail;//门牌号等

@property (nonatomic, assign) BOOL isDefault;//是否默认地址

@property (nonatomic, copy) NSString *ID;

@end

NS_ASSUME_NONNULL_END
