//
//  QJMallAddressListModel.h
//  QJBox
//
//  Created by rui on 2022/9/5.
//

#import <Foundation/Foundation.h>
#import "QJMallAddressModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJMallAddressListModel : NSObject

/* 数据源数组 */
@property (nonatomic, strong) NSMutableArray<QJMallAddressModel *> *records;
/* 总条数 */
@property (nonatomic, strong) NSNumber *total;
/* 分页 */
@property (nonatomic, strong) NSNumber *page;
/* 当前页 */
@property (nonatomic, strong) NSNumber *current;
/* 是否能分页 */
@property (nonatomic, assign) BOOL hasNext;
/* 是否需要加载更多 */
@property (assign, nonatomic) BOOL willLoadMore;
/* 分页 */
- (NSDictionary *)toParams;
/* 解析model */
-(void)configObj:(QJMallAddressListModel *)model;

@end

NS_ASSUME_NONNULL_END
