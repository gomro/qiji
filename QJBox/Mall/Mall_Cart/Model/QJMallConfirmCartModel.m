//
//  QJMallConfirmCartModel.m
//  QJBox
//
//  Created by wxy on 2022/8/24.
//

#import "QJMallConfirmCartModel.h"

@implementation QJMallConfirmCartModel
+ (NSDictionary *)modelCustomPropertyMapper{
    return @{@"idStr" : @[@"id"]};
}
@end
