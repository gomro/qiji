//
//  QJMallEmptyAddressListView.m
//  QJBox
//
//  Created by wxy on 2022/8/25.
//

#import "QJMallEmptyAddressListView.h"

@interface QJMallEmptyAddressListView ()

@property (nonatomic, strong) UIImageView *iconImageView;

@property (nonatomic, strong) UILabel *textLabel;


@end


@implementation QJMallEmptyAddressListView


- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self addSubview:self.iconImageView];
        [self addSubview:self.textLabel];
        
        self.backgroundColor = RGB(244, 244, 244);
        [self.iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.centerX.equalTo(self);
                    make.top.equalTo(self).offset(94);
                    make.width.equalTo(@91);
                    make.height.equalTo(@72);
        }];
        
        [self.textLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.centerX.equalTo(self);
                    make.height.equalTo(@18);
                    make.top.equalTo(self.iconImageView.mas_bottom).offset(16);
                
        }];
        
        
        
    }
    return self;
}



#pragma  mark  -------getter

- (UIImageView *)iconImageView {
    if (!_iconImageView) {
        _iconImageView = [UIImageView new];
        _iconImageView.backgroundColor = [UIColor grayColor];
    }
    return _iconImageView;
}

- (UILabel *)textLabel {
    if (!_textLabel) {
        _textLabel = [UILabel new];
        _textLabel.font = FontRegular(12);
        _textLabel.textColor = kColorWithHexString(@"000000");
        _textLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _textLabel;
}


@end
