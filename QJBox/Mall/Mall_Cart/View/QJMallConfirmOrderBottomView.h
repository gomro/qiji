//
//  QJMallConfirmOrderBottomView.h
//  QJBox
//
//  Created by wxy on 2022/8/24.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJMallConfirmOrderBottomView : UIView

@property (nonatomic, copy) void(^confirmOrderBlock)(void);

@property (nonatomic, strong) UIButton *okBtn;

@property (nonatomic, copy) NSString *contentStr;
@end

NS_ASSUME_NONNULL_END
