//
//  QJLocationAddressListView.h
//  QJBox
//
//  Created by rui on 2022/9/5.
//

#import <UIKit/UIKit.h>
#import <AMapSearchKit/AMapSearchKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJLocationAddressListView : UIView

@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, copy) void(^localAddress)(NSString *city,NSString *address,AMapPOI *model);

@end

NS_ASSUME_NONNULL_END
