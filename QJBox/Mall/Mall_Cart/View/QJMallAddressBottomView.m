//
//  QJMallAddressBottomView.m
//  QJBox
//
//  Created by wxy on 2022/8/25.
//

#import "QJMallAddressBottomView.h"

@interface QJMallAddressBottomView ()



@property (nonatomic, strong) UIView *bgView;


@end



@implementation QJMallAddressBottomView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor whiteColor];
        [self addSubview:self.bgView];
        [self addSubview:self.btn];
        [self.btn mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.equalTo(self).offset(37.5);
                    make.right.equalTo(self).offset(-37.5);
                    make.height.equalTo(@(kWScale* 40));
                    make.bottom.equalTo(self).offset(-Bottom_iPhoneX_SPACE);
        }];
        
        [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.bottom.left.right.equalTo(self);
            make.top.equalTo(self).offset(0);
        }];
        
        
        
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self.bgView viewShadowPathWithColor:[UIColor colorWithRed:0.808 green:0.808 blue:0.808 alpha:0.25] shadowOpacity:1 shadowRadius:6 shadowPathType:QJShadowPathTop shadowPathWidth:6];
}

- (void)setTitle:(NSString *)title {
    _title = title;
    [_btn setTitle:title forState:UIControlStateNormal];
}


#pragma mark-------- getter

- (UIButton *)btn {
    if (!_btn) {
        _btn = [UIButton new];
        [_btn addTarget:self action:@selector(btnAction:) forControlEvents:UIControlEventTouchUpInside];
        [_btn setTitle:@"+添加收货地址" forState:UIControlStateNormal];
        _btn.titleLabel.font = FontSemibold(14);
        [_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        [_btn setBackgroundImage:[UIImage imageWithColor:kColorWithHexString(@"FF9500")] forState:UIControlStateNormal];
        [_btn setBackgroundImage:[UIImage imageWithColor:kColorWithHexString(@"#C8CACC")] forState:UIControlStateDisabled];
        _btn.layer.cornerRadius = kWScale*20;
        _btn.layer.masksToBounds = YES;
    }
    return _btn;
}

- (UIView *)bgView {
    if (!_bgView) {
        _bgView = [UIView new];
        _bgView.backgroundColor = [UIColor whiteColor];
    }
    return _bgView;
}


#pragma mark ----- action

- (void)btnAction:(UIButton *)sender {
    DLog(@"添加收货地址");
    if (self.bottomBtnBlock) {
        self.bottomBtnBlock();
    }
}



@end
