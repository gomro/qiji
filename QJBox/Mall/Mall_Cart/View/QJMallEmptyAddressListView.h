//
//  QJMallEmptyAddressListView.h
//  QJBox
//
//  Created by wxy on 2022/8/25.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/// 空地址占位视图
@interface QJMallEmptyAddressListView : UIView

@property (nonatomic, copy) NSString *imageName;

@property (nonatomic, copy) NSString *text;


@end

NS_ASSUME_NONNULL_END
