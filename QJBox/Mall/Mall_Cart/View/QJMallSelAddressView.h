//
//  QJMallSelAddressView.h
//  QJBox
//
//  Created by macm on 2022/9/5.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void(^QJMallSelAddressViewBlock)(NSArray *dataArray);

@interface QJMallSelAddressView : UIView
@property (nonatomic, copy) QJMallSelAddressViewBlock selAddressBlock;
// 定位地址数组
@property (nonatomic, strong) NSArray *locationArray;
- (void)show;
- (void)fadeOut;
@end

NS_ASSUME_NONNULL_END
