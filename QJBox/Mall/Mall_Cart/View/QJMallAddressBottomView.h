//
//  QJMallAddressBottomView.h
//  QJBox
//
//  Created by wxy on 2022/8/25.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/// 地址底部包含按钮的视图
@interface QJMallAddressBottomView : UIView

@property (nonatomic, copy) void (^bottomBtnBlock)(void);

@property (nonatomic, copy) NSString *title;

@property (nonatomic, strong) UIButton *btn;
@end

NS_ASSUME_NONNULL_END
