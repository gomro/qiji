//
//  QJMallBaseTextField.m
//  QJBox
//
//  Created by wxy on 2022/8/26.
//

#import "QJMallBaseTextField.h"

@implementation QJMallBaseTextField

 
//控制placeHolder的位置
-(CGRect)placeholderRectForBounds:(CGRect)bounds
{
    CGRect inset = CGRectMake(bounds.origin.x+8, bounds.origin.y, bounds.size.width -8, bounds.size.height);
    return inset;
}


//控制显示文本的位置
-(CGRect)textRectForBounds:(CGRect)bounds
{
    CGRect inset = CGRectMake(bounds.origin.x+8, bounds.origin.y, bounds.size.width -8, bounds.size.height);
    return inset;
}





@end
