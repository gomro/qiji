//
//  QJMallSelAddressView.m
//  QJBox
//
//  Created by macm on 2022/9/5.
//

#import "QJMallSelAddressView.h"
#import "UIView+XWAddForRoundedCorner.h"
#import "QJMalSelAddressTableViewCell.h"
#import "QJMallSelAddressDetailTableViewCell.h"
#import "QJMallCnAreaModel.h"

#define BgViewHeight (kScreenHeight-124-NavigationBar_Bottom_Y)

@interface QJMallSelAddressView ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *addressTableView;
@property (nonatomic, strong) UITableView *addressDetailTableView;
@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIButton *closeButton;
@property (strong, nonatomic) MASConstraint *tableHeightConstraint;
@property (nonatomic, strong) UIView *lineView;
@property (nonatomic, strong) UILabel *statusLabel;
@property (nonatomic, assign) CGFloat tableViewHeigth;

// 选择的地址数组
@property (nonatomic, strong) NSMutableArray *selArray;
// 省市区
@property (nonatomic, strong) NSMutableArray *provinceArray;
@property (nonatomic, strong) NSMutableArray *cityArray;
@property (nonatomic, strong) NSMutableArray *districtArray;
@property (nonatomic, strong) NSMutableArray *streetArray;

// 当前选择的城市所有地区
@property (nonatomic, strong) NSMutableArray *addressArray;
// 首字母
@property (nonatomic, strong) NSMutableArray *firstCharArray;
// 当前选择的行数
@property (nonatomic, assign) NSInteger selectRow;

@end

@implementation QJMallSelAddressView

- (instancetype)init {
    self = [super init];
    if (self) {
         
        self.frame = [UIScreen mainScreen].bounds;
        self.backgroundColor = RGBAlpha(0, 0, 0, 0.4);
        self.tableViewHeigth = 0;
        [self createView];
        
    }
    return self;
}

- (void)createView {
    [self addSubview:self.bgView];
    [self.bgView addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(30);
        make.centerX.mas_equalTo(self.bgView);
    }];
    
    [self.bgView addSubview:self.closeButton];
    [self.closeButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(18);
        make.right.mas_equalTo(-16);
        make.width.height.mas_equalTo(30);
    }];
    
    [self.bgView addSubview:self.addressTableView];
    [self.addressTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(80);
        make.left.right.mas_equalTo(0);
        self.tableHeightConstraint = make.height.mas_equalTo(self.tableViewHeigth);
    }];
    
    [self.bgView addSubview:self.lineView];
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.addressTableView.mas_bottom).mas_equalTo(0);
        make.left.right.mas_equalTo(0);
        make.height.mas_equalTo(1);
    }];
    
    [self.bgView addSubview:self.statusLabel];
    [self.statusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.lineView.mas_bottom).mas_equalTo(16);
        make.left.mas_equalTo(16);
    }];
    
    [self.bgView addSubview:self.addressDetailTableView];
    [self.addressDetailTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.statusLabel.mas_bottom).mas_equalTo(8);
        make.left.right.mas_equalTo(0);
        make.bottom.mas_equalTo(-Bottom_iPhoneX_SPACE);
    }];
    
    NSArray *pArray = LSUserDefaultsGET(@"QJCnAreaProvinceArray");
    for (NSDictionary *dic in pArray) {
        QJMallCnAreaModel *model = [QJMallCnAreaModel modelWithDictionary:dic];
        [self.provinceArray addObject:model];
    }
    
    NSArray *cArray = LSUserDefaultsGET(@"QJCnAreaCityArray");
    for (NSDictionary *dic in cArray) {
        QJMallCnAreaModel *model = [QJMallCnAreaModel modelWithDictionary:dic];
        [self.cityArray addObject:model];
    }
    
    NSArray *dArray = LSUserDefaultsGET(@"QJCnAreaDistrictArray");
    for (NSDictionary *dic in dArray) {
        QJMallCnAreaModel *model = [QJMallCnAreaModel modelWithDictionary:dic];
        [self.districtArray addObject:model];
    }
    
    NSArray *sArray = LSUserDefaultsGET(@"QJCnAreaStreetArray");
    for (NSDictionary *dic in sArray) {
        QJMallCnAreaModel *model = [QJMallCnAreaModel modelWithDictionary:dic];
        [self.streetArray addObject:model];
    }
    
    QJMallCnAreaModel *model = [[QJMallCnAreaModel alloc] init];
    model.level = -1;
    [self reloadAddressDetailList:model];
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (tableView == self.addressTableView) {
        return 1;
    } else {
        return 1;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == self.addressTableView) {
        return self.selArray.count;
    } else {
        return self.addressArray.count;
    }

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.addressTableView) {
        QJMalSelAddressTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QJMalSelAddressTableViewCell" forIndexPath:indexPath];
        QJMallCnAreaModel *model = self.selArray[indexPath.row];
        
        [cell.topLineView setHidden:NO];
        [cell.bottomLineView setHidden:NO];
        cell.leftView.backgroundColor = kColorOrange;
        if (indexPath.row == 0) {
            [cell.topLineView setHidden:YES];
        }
        if (indexPath.row == self.selArray.count-1) {
            [cell.bottomLineView setHidden:YES];
            cell.leftView.backgroundColor = [UIColor whiteColor];
        }
        
        if (self.selArray.count == 4) {
            QJMallCnAreaModel *model = self.selArray.lastObject;
            if (model.areaCode.length != 0) {
                cell.leftView.backgroundColor = kColorOrange;
            }
        }
        
        if (self.selectRow == indexPath.row) {
            cell.addressLabel.textColor = kColorOrange;
        } else {
            cell.addressLabel.textColor = UIColorFromRGB(0x474849);
        }
        cell.addressLabel.text = model.name;
        return cell;
    } else {
        QJMallSelAddressDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QJMallSelAddressDetailTableViewCell" forIndexPath:indexPath];
        QJMallCnAreaModel *model = self.addressArray[indexPath.row];
         
        cell.leftLabel.text = self.firstCharArray[indexPath.row];

        cell.addressLabel.text = model.name;
        return cell;
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.addressTableView) {
        self.selectRow = indexPath.row;
        [self.addressTableView reloadData];
        
        if (indexPath.row == 0) {
            QJMallCnAreaModel *model = [[QJMallCnAreaModel alloc] init];
            model.level = -1;
            [self reloadAddressDetailList:model];
        } else {
            QJMallCnAreaModel *model = self.selArray[indexPath.row-1];
            [self reloadAddressDetailList:model];
        }
        
    } else {
        
        QJMallCnAreaModel *model = self.addressArray[indexPath.row];
        [self reloadAddressDetailList:model];
        
        if (model.level == 0 && self.selArray.count > 1) {
            [self.selArray removeAllObjects];
            [self reloadAddressList:model];
            return;
        }
        
        if (model.level == 1 && self.selArray.count > 2) {
            QJMallCnAreaModel *pModel = self.selArray[0];
            [self.selArray removeAllObjects];
            [self.selArray addObject:pModel];
            [self reloadAddressList:model];
            return;
        }
        
        if (model.level == 2 && self.selArray.count > 3) {
            QJMallCnAreaModel *pModel = self.selArray[0];
            QJMallCnAreaModel *cModel = self.selArray[1];
            [self.selArray removeAllObjects];
            [self.selArray addObject:pModel];
            [self.selArray addObject:cModel];
            [self reloadAddressList:model];
            return;
        }
        
        if (self.selArray.count > 1) {
            [self.selArray removeLastObject];
        }
        
        [self reloadAddressList:model];
    }
}

// 刷新已选择的地址
- (void)reloadAddressList:(QJMallCnAreaModel *)model {
    [self.selArray addObject:model];
    if (self.selArray.count < 4) {
        NSString *tip = @"";
        if (model.level == 0) {
            tip = @"请选择城市";
            self.selectRow = 1;
        }
        if (model.level == 1) {
            tip = @"请选择城县";
            self.selectRow = 2;
        }
        if (model.level == 2) {
            tip = @"请选择街道";
            self.selectRow = 3;
        }
        QJMallCnAreaModel *tipModel = [[QJMallCnAreaModel alloc] init];
        tipModel.name = tip;
        [self.selArray addObject:tipModel];
    } else {
        if (self.selAddressBlock) {
            self.selAddressBlock(self.selArray);
        }
    }

    self.tableViewHeigth = self.selArray.count * 30 + 20;
    [self.tableHeightConstraint uninstall];
    [self.addressTableView mas_updateConstraints:^(MASConstraintMaker *make) {
        self.tableHeightConstraint = make.height.mas_equalTo(self.tableViewHeigth);
    }];
    [self.addressTableView reloadData];
}

// 刷新选择的地址分组
- (void)reloadAddressDetailList:(QJMallCnAreaModel *)model {

    if (model.level == -1) {
        [self.addressArray removeAllObjects];
        [self.addressArray addObjectsFromArray:self.provinceArray];
        [self.addressDetailTableView reloadData];
    }
    
    if (model.level == 0) {
        [self.addressArray removeAllObjects];
        
        for (QJMallCnAreaModel *city in self.cityArray) {
            if ([city.parentCode isEqualToString:model.areaCode]) {
                [self.addressArray addObject:city];
            }
        }
        
        [self.addressDetailTableView reloadData];
        
    }
    
    if (model.level == 1) {
        [self.addressArray removeAllObjects];
        
        for (QJMallCnAreaModel *district in self.districtArray) {
            if ([district.parentCode isEqualToString:model.areaCode]) {
                [self.addressArray addObject:district];
            }
        }
        
        [self.addressDetailTableView reloadData];
    }
    
    if (model.level == 2) {
        [self.addressArray removeAllObjects];
        
        for (QJMallCnAreaModel *street in self.streetArray) {
            if ([street.parentCode isEqualToString:model.areaCode]) {
                [self.addressArray addObject:street];
            }
        }
        QJMallCnAreaModel *normalStreet = [[QJMallCnAreaModel alloc] init];
        normalStreet.name = @"暂不选择";
        normalStreet.level = 3;
        [self.addressArray addObject:normalStreet];
        [self.addressDetailTableView reloadData];
    }
    
    if (model.level == 3) {
        [self fadeOut];
    }
    
    // 首字母设置
    [self.firstCharArray removeAllObjects];
    int i = 0;
    for (QJMallCnAreaModel *item in self.addressArray) {
        [self firstCharactor:item.name row:[NSString stringWithFormat:@"%d",i]];
        i++;
    }
    
}

//获取汉字的首字母
- (void)firstCharactor:(NSString *)aString row:(NSString *)row {
    NSMutableString *str = [NSMutableString stringWithString:aString];
    CFStringTransform((CFMutableStringRef)str,NULL, kCFStringTransformMandarinLatin,NO);
    CFStringTransform((CFMutableStringRef)str,NULL, kCFStringTransformStripDiacritics,NO);
   
    NSString *pinYin = [str capitalizedString];
   
    NSString *firatCharactors = [NSMutableString string];
    for (int i = 0; i < pinYin.length; i++) {
        if ([pinYin characterAtIndex:i] >= 'A' && [pinYin characterAtIndex:i] <= 'Z') {
            firatCharactors = [firatCharactors stringByAppendingString:[NSString stringWithFormat:@"%C",[pinYin characterAtIndex:i]]];
        }
    }
    if (firatCharactors.length > 0) {
        firatCharactors = [firatCharactors substringToIndex:1];
        if ([self.firstCharArray containsObject:firatCharactors] || [aString isEqualToString:@"暂不选择"]) {
            [self.firstCharArray addObject:@""];
        } else {
            [self.firstCharArray addObject:firatCharactors];
        }
    }

}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 30;
}

- (void)show {
    UIWindow *keywindow = [[UIApplication sharedApplication] keyWindow];
    [keywindow addSubview:self];
    [self fadeIn];
    
    if (self.locationArray.count > 0) {
        
        for (QJMallCnAreaModel *model in self.locationArray) {
            if (model.areaCode.length == 0 && model.name.length == 0) {
                return;
            }
        }
        [self.selArray removeAllObjects];
        for (QJMallCnAreaModel *model in self.locationArray) {
            if (model.areaCode.length != 12) {
                for (NSInteger i = model.areaCode.length; i < 12; i++) {
                    model.areaCode = [NSString stringWithFormat:@"%@0",model.areaCode];
                }
            }
            [self.selArray addObject:model];
        }
        self.selectRow = 3;
        
        self.tableViewHeigth = self.selArray.count * 30 + 20;
        [self.tableHeightConstraint uninstall];
        [self.addressTableView mas_updateConstraints:^(MASConstraintMaker *make) {
            self.tableHeightConstraint = make.height.mas_equalTo(self.tableViewHeigth);
        }];
        [self.addressTableView reloadData];
        
        QJMallCnAreaModel *model = self.selArray[2];
        [self reloadAddressDetailList:model];
    }
}

- (void)fadeIn {
    self.alpha = 0;
    [UIView animateWithDuration:.35 animations:^{
        self.bgView.transform = CGAffineTransformMakeTranslation(0, - (BgViewHeight));
        self.alpha = 1;
    }];
}

- (void)fadeOut {
    if (self.selAddressBlock) {
        self.selAddressBlock(self.selArray);
    }
    [UIView animateWithDuration:.35 animations:^{
        self.bgView.transform = CGAffineTransformMakeTranslation(0, kScreenHeight);
        self.alpha = 0.0;
    }];
}

- (UIView *)bgView {
    if (!_bgView) {
        _bgView = [[UIView alloc] initWithFrame:CGRectMake(0, kScreenHeight, kScreenWidth, BgViewHeight)];
        [self.bgView showCorner:10 rectCorner:UIRectCornerTopLeft | UIRectCornerTopRight];
        _bgView.backgroundColor = [UIColor whiteColor];
    }
    return _bgView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.text = @"请选择所在地区";
        _titleLabel.textColor = UIColorFromRGB(0x16191c);
        [_titleLabel setFont:FontSemibold(18)];
    }
    return _titleLabel;
}

- (UIButton *)closeButton {
    if (!_closeButton) {
        _closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_closeButton setImage:[UIImage imageNamed:@"qj_close_24"] forState:UIControlStateNormal];
        [_closeButton addTarget:self action:@selector(cancelBtnAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _closeButton;
}

- (void)cancelBtnAction {
    [self fadeOut];
}

- (UILabel *)statusLabel {
    if (!_statusLabel) {
        _statusLabel = [[UILabel alloc] init];
        _statusLabel.text = @"选择省份/地区";
        _statusLabel.textColor = UIColorFromRGB(0x16191c);
        [_statusLabel setFont:FontRegular(16)];
    }
    return _statusLabel;
}

- (UIView *)lineView {
    if (!_lineView) {
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor = UIColorFromRGB(0xebedf0);
    }
    return _lineView;
}

- (UITableView *)addressTableView {
    if (!_addressTableView) {
        _addressTableView = [[UITableView alloc] init];
        _addressTableView.backgroundColor = [UIColor clearColor];
        _addressTableView.dataSource = self;
        _addressTableView.delegate = self;
        _addressTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _addressTableView.showsVerticalScrollIndicator = NO;
        _addressTableView.showsHorizontalScrollIndicator = NO;
        [_addressTableView registerClass:[QJMalSelAddressTableViewCell class] forCellReuseIdentifier:@"QJMalSelAddressTableViewCell"];
        _addressTableView.scrollEnabled = NO;
        if (@available(iOS 11.0, *)) {
            _addressTableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
 
    }
    return _addressTableView;
}

- (UITableView *)addressDetailTableView {
    if (!_addressDetailTableView) {
        _addressDetailTableView = [[UITableView alloc] init];
        _addressDetailTableView.backgroundColor = [UIColor clearColor];
        _addressDetailTableView.dataSource = self;
        _addressDetailTableView.delegate = self;
        _addressDetailTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _addressDetailTableView.showsVerticalScrollIndicator = NO;
        _addressDetailTableView.showsHorizontalScrollIndicator = NO;
        [_addressDetailTableView registerClass:[QJMallSelAddressDetailTableViewCell class] forCellReuseIdentifier:@"QJMallSelAddressDetailTableViewCell"];

        if (@available(iOS 11.0, *)) {
            _addressDetailTableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
 
    }
    return _addressDetailTableView;
}

- (NSMutableArray *)selArray {
    if (!_selArray) {
        _selArray = [NSMutableArray array];
    }
    return _selArray;
}

- (NSMutableArray *)addressArray {
    if (!_addressArray) {
        _addressArray = [NSMutableArray array];
    }
    return _addressArray;
}

- (NSMutableArray *)provinceArray {
    if (!_provinceArray) {
        _provinceArray = [NSMutableArray array];
    }
    return _provinceArray;
}

- (NSMutableArray *)cityArray {
    if (!_cityArray) {
        _cityArray = [NSMutableArray array];
    }
    return _cityArray;
}

- (NSMutableArray *)districtArray {
    if (!_districtArray) {
        _districtArray = [NSMutableArray array];
    }
    return _districtArray;
}

- (NSMutableArray *)streetArray {
    if (!_streetArray) {
        _streetArray = [NSMutableArray array];
    }
    return _streetArray;
}

- (NSMutableArray *)firstCharArray {
    if (!_firstCharArray) {
        _firstCharArray = [NSMutableArray array];
    }
    return _firstCharArray;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];

    UIView *view = [touch view];

    if (view == self) {
        [self fadeOut];
    }
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
