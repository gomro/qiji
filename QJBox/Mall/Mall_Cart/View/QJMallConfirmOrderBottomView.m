//
//  QJMallConfirmOrderBottomView.m
//  QJBox
//
//  Created by wxy on 2022/8/24.
//

#import "QJMallConfirmOrderBottomView.h"


@interface QJMallConfirmOrderBottomView ()

@property (nonatomic, strong) UILabel *contentLabel;//价格内容




@end


@implementation QJMallConfirmOrderBottomView


- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self addSubview:self.contentLabel];
        [self addSubview:self.okBtn];
        self.backgroundColor = [UIColor whiteColor];
        [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.okBtn);
            make.right.equalTo(self.okBtn.mas_left).offset(-16);
            
        }];
        [self.okBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self).offset(-(16 + Bottom_iPhoneX_SPACE));
            make.right.equalTo(self).offset(-16);
            make.width.equalTo(@(106*kWScale));
            make.height.equalTo(@(40*kWScale));
        }];
        
    }
    return self;
}



#pragma mark -------- getter

- (UILabel *)contentLabel {
    if (!_contentLabel) {
        _contentLabel = [UILabel new];
        _contentLabel.textAlignment = NSTextAlignmentRight;
    }
    return _contentLabel;
}


- (UIButton *)okBtn {
    if (!_okBtn) {
        _okBtn = [UIButton new];
        [_okBtn addTarget:self action:@selector(confirmOrder:) forControlEvents:UIControlEventTouchUpInside];
        [_okBtn setBackgroundColor:kColorWithHexString(@"#C8CACC")];
        [_okBtn setTitle:@"确定兑换" forState:UIControlStateNormal];
        _okBtn.titleLabel.font = FontSemibold(14);
        _okBtn.layer.cornerRadius =kWScale* 20;
        _okBtn.layer.masksToBounds = YES;
        [_okBtn setEnabled:NO];
    }
    return _okBtn;
}


#pragma mark -----setter

- (void)confirmOrder:(UIButton *)sender {
    DLog(@"确认订单");
    if (self.confirmOrderBlock) {
        self.confirmOrderBlock();
    }
}



- (void)setContentStr:(NSString *)contentStr {
    _contentStr = contentStr;
    NSString *price = [NSString stringWithFormat:@"合计：%@ 奇迹币",contentStr];
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:price];
    [attrStr addAttribute:NSFontAttributeName
                    value:FontSemibold(16)
                    range:NSMakeRange(0, 3)];
    [attrStr addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0x000000) range:NSMakeRange(0, 3)];

    [attrStr addAttribute:NSFontAttributeName
                    value:FontMedium(24)
                    range:NSMakeRange(3, price.length-6)];
    [attrStr addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0xff9500) range:NSMakeRange(3, price.length-6)];

    [attrStr addAttribute:NSFontAttributeName
                    value:FontRegular(12)
                    range:NSMakeRange(price.length-3, 3)];
    [attrStr addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0xff9500) range:NSMakeRange(price.length-3, 3)];
    self.contentLabel.attributedText = attrStr;
}





@end
