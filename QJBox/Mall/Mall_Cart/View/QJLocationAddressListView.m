//
//  QJLocationAddressListView.m
//  QJBox
//
//  Created by rui on 2022/9/5.
//

#import "QJLocationAddressListView.h"
#import "QJLocationAddressCell.h"

@interface QJLocationAddressListView ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;

@end


@implementation QJLocationAddressListView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor whiteColor];
        [self initUI];
    }
    return self;
}

- (void)initUI{
    [self addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.bottom.equalTo(self);
    }];
}

#pragma mark - Lazy Loading
-(UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.width , self.height) style:UITableViewStyleGrouped];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = [UIColor whiteColor];
        [_tableView registerClass:[QJLocationAddressCell class] forCellReuseIdentifier:@"QJLocationAddressCell"];
        if (@available(iOS 11.0, *)) {
            _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
            _tableView.estimatedRowHeight = 0;
            _tableView.estimatedSectionFooterHeight = 0;
            _tableView.estimatedSectionHeaderHeight = 0;
        }
    }
    return _tableView;
}

- (void)setDataArray:(NSMutableArray *)dataArray{
    _dataArray = dataArray;
    [self.tableView reloadData];
}

#pragma mark - UItableView
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 30;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.width, 30)];
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(26, 0, self.width-26, 30)];
    titleLabel.font = kFont(12);
    titleLabel.text = @"根据定位获取附近信息";
    titleLabel.textColor = UIColorFromRGB(0x262626);
    [headerView addSubview:titleLabel];
    return headerView;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    QJLocationAddressCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QJLocationAddressCell" forIndexPath:indexPath];
    cell.model = [self.dataArray safeObjectAtIndex:indexPath.row];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    AMapPOI *model = [self.dataArray safeObjectAtIndex:indexPath.row];
    NSString *nameString = [NSString stringWithFormat:@"%@%@%@",model.province?:@"",model.city?:@"",model.district?:@""];
    NSString *addressString = [NSString stringWithFormat:@"%@%@",model.address?:@"",model.name?:@""];

    if (self.localAddress) {
        self.localAddress(nameString,addressString,model);
    }
}

@end
