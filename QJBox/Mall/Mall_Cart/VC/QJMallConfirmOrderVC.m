//
//  QJMallConfirmOrderVC.m
//  QJBox
//
//  Created by wxy on 2022/8/24.
//

#import "QJMallConfirmOrderVC.h"
#import "QJMallConfirmOrderBottomView.h"
#import "QJMallCartNoRequest.h"
#import "QJMallConfirmFillAddressCell.h"
#import "QJMallConfirmAddressCell.h"
#import "QJMallConfirmProductCell.h"
#import "QJMallCofirmOrderRequest.h"
#import "QJMallConfirmCartModel.h"
#import "QJMallExchangeSucceededVC.h"
#import "QJMallAddressModel.h"
#import "QJAddressViewController.h"
#import "QJMallUnPayRequest.h"
#import "QJMallOrderDetailGoodsTableViewCell.h"
#import "QJMallOrderRequest.h"
#import "QJMallAddressModel.h"
#import "QJAddressViewController.h"

@interface QJMallConfirmOrderVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) QJMallConfirmOrderBottomView *bottomView;

@property (nonatomic, strong) NSMutableArray *dataArr;//数据源

@property (nonatomic, strong) QJMallCartNoRequest *cartNoReq;//请求生成的订单信息

@property (nonatomic, strong) QJMallCofirmOrderRequest *cofirmOrderReq;//提交订单

@property (nonatomic, strong) QJMallConfirmCartModel *cartModel;
@property (nonatomic, strong) QJMallOrderDetailModel *detailModel;
@property (nonatomic, strong) QJMallAddressModel *addressModel;
@property (nonatomic, assign) BOOL productType; // 收否是实体订单
@property (nonatomic, assign) BOOL showAddress; // 是否有收货地址
@end



@implementation QJMallConfirmOrderVC


- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"支付订单";
    self.dataArr = [NSMutableArray array];
    self.navBar.backgroundColor = [UIColor clearColor];
    
    [self makeUI];
    [self sendRequest];
}

- (void)sendRequest {
    [QJAppTool showHUDLoading];
    @weakify(self);
    QJMallOrderRequest *request = [[QJMallOrderRequest alloc] init];
    [request requestShopOrderSubmitOrderId:self.cartNo completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        @strongify(self);

        if (isSuccess) {
            NSDictionary *data = EncodeDicFromDic(request.responseJSONObject, @"data");
            self.cartModel = [QJMallConfirmCartModel modelWithJSON:data];

            [self sendRequestDetail];
        }else{
            [QJAppTool hideHUDLoading];
        }
    }];
}

// 订单详情
- (void)sendRequestDetail {
    @weakify(self);

    QJMallOrderRequest *request = [[QJMallOrderRequest alloc] init];
    [request requestShopOrderUnPayDetailOrderId:self.cartModel.idStr completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        @strongify(self);

        if (isSuccess) {
            NSDictionary *data = EncodeDicFromDic(request.responseJSONObject, @"data");

            self.detailModel = [QJMallOrderDetailModel modelWithJSON:data];
            self.bottomView.contentStr = self.detailModel.qjcoinPaid;
            if ([self.detailModel.productType isEqualToString:@"1"]) {
                self.productType = YES;
                [self sendRequestAddress];
                
            } else {
                [QJAppTool hideHUDLoading];
                [self.bottomView.okBtn setEnabled:YES];
                [self.bottomView.okBtn setBackgroundColor:UIColorFromRGB(0xff9500)];
                [self.tableView reloadData];
            }

        }else{
            [QJAppTool hideHUDLoading];
        }
    }];
}

// 地址详情
- (void)sendRequestAddress {
    @weakify(self);

    QJMallOrderRequest *request = [[QJMallOrderRequest alloc] init];
    [request requestRecAddressDefault_completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        @strongify(self);
        [QJAppTool hideHUDLoading];
        if (isSuccess) {
            NSDictionary *data = EncodeDicFromDic(request.responseJSONObject, @"data");
            if (data != nil) {
                self.addressModel = [QJMallAddressModel modelWithJSON:data];
                [self.bottomView.okBtn setEnabled:YES];
                [self.bottomView.okBtn setBackgroundColor:UIColorFromRGB(0xff9500)];
                self.showAddress = YES;
            }

            [self.tableView reloadData];

        }
        
    }];
}

// 支付
- (void)sendRequestPay {
    [QJAppTool showHUDLoading];
    @weakify(self);
    QJMallOrderRequest *request = [[QJMallOrderRequest alloc] init];
    [request requestShopOrderPayId:self.detailModel.idStr addressId:self.addressModel.ID completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        @strongify(self);
        [QJAppTool hideHUDLoading];
        if (isSuccess) {
            // 兑换成功后，跳转成功页面
            NSInteger count = self.navigationController.viewControllers.count;
            if(count > 3) {
                count = count - 3;
            } else {
                count = 1;
            }
            [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:count] animated:YES];
            [[NSNotificationCenter defaultCenter] postNotificationName:QJMallConfirmOrderSuccess object:self.detailModel.productType];

        }
        
    }];
}

#pragma mark ---- make UI

- (void)makeUI {
    self.view.backgroundColor = RGB(244, 244, 244);
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.bottomView];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view).offset(16);
            make.right.equalTo(self.view).offset(-16);
            make.top.equalTo(self.navBar.mas_bottom);
            make.bottom.equalTo(self.bottomView.mas_top);
    }];
    
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.equalTo(self.view);
            make.height.equalTo(@(72*kWScale+Bottom_iPhoneX_SPACE));
    }];
}

#pragma mark ----- getter

- (QJMallConfirmOrderBottomView *)bottomView {
    if (!_bottomView) {
        WS(weakSelf)
        _bottomView = [QJMallConfirmOrderBottomView new];
        _bottomView.confirmOrderBlock = ^{
            [weakSelf sendRequestPay];
        };
    }
    return _bottomView;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.backgroundColor = RGB(244, 244, 244);
        _tableView.delegate = self;
        _tableView.dataSource = self;
        [_tableView registerClass:[QJMallConfirmFillAddressCell class] forCellReuseIdentifier:@"QJMallConfirmFillAddressCell"];
        [_tableView registerClass:[QJMallConfirmAddressCell class] forCellReuseIdentifier:@"QJMallConfirmAddressCell"];
        [_tableView registerClass:[QJMallConfirmProductCell class] forCellReuseIdentifier:@"QJMallConfirmProductCell"];
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.showsHorizontalScrollIndicator = NO;
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.estimatedRowHeight = 52 *kWScale;
    }
    return _tableView;
}


- (QJMallCofirmOrderRequest *)cofirmOrderReq {
    if (!_cofirmOrderReq) {
        _cofirmOrderReq = [QJMallCofirmOrderRequest new];
    }
    return _cofirmOrderReq;
}

-(QJMallCartNoRequest *)cartNoReq {
    if (!_cartNoReq) {
        _cartNoReq = [QJMallCartNoRequest new];
    }
    return _cartNoReq;
}

#pragma mark  --------------UITableViewDelegate UITableViewDataSource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.productType ? 2 : 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return UITableViewAutomaticDimension;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [UIView new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 15;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    if (self.productType) {
        if (indexPath.section == 0) {
            if (self.showAddress) {
                QJMallConfirmFillAddressCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QJMallConfirmFillAddressCell"];
                cell.model = self.addressModel;
                return cell;
                
            } else {
                QJMallConfirmAddressCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QJMallConfirmAddressCell"];
                return cell;
            }
        } else {
            QJMallConfirmProductCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QJMallConfirmProductCell"];
            cell.model = self.detailModel;
            return cell;
        }
        
        
    } else {
        QJMallConfirmProductCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QJMallConfirmProductCell"];
        cell.model = self.detailModel;
        return cell;
    }

}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        QJAddressViewController *vc = [[QJAddressViewController alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
        @weakify(self);
        vc.selectedAddress = ^(QJMallAddressModel * _Nonnull model) {
            @strongify(self);
            self.addressModel = model;
            self.showAddress = YES;
            [self.bottomView.okBtn setEnabled:YES];
            [self.bottomView.okBtn setBackgroundColor:UIColorFromRGB(0xff9500)];
            [self.tableView reloadData];
        };
        
    }

}


@end
