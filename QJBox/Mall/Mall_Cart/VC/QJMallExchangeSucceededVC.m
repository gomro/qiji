//
//  QJMallExchangeSucceededVC.m
//  QJBox
//
//  Created by wxy on 2022/8/25.
//

#import "QJMallExchangeSucceededVC.h"

@interface QJMallExchangeSucceededVC ()

@property (nonatomic, strong) UIImageView *centerImageView;

@property (nonatomic, strong) UILabel *firstLabel;

@property (nonatomic, strong) UILabel *subLabel;

@property (nonatomic, strong) UIButton *okButton;

@end

@implementation QJMallExchangeSucceededVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    self.navBar.backgroundColor = [UIColor clearColor];
    [self.navBar setHidden:YES];

    
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.centerImageView];
    [self.view addSubview:self.firstLabel];
    [self.view addSubview:self.subLabel];
    [self.view addSubview:self.okButton];
    
    [self.centerImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view);
        make.width.equalTo(@(240*kWScale));
        make.height.equalTo(@(240*kWScale));
        make.top.equalTo(self.view).offset(NavigationBar_Bottom_Y+72);
    }];
    [self.firstLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view);
        make.height.mas_equalTo((28*kWScale));
        make.top.equalTo(self.centerImageView.mas_bottom).offset(-65);
    }];
    [self.subLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view);
        make.height.mas_equalTo((28*kWScale));
        make.top.equalTo(self.centerImageView.mas_bottom).offset(-28);
    }];
    
    [self.okButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(NavigationBar_Bottom_Y-40);
        make.right.mas_equalTo(-15);
        make.width.mas_equalTo(50*kWScale);
        make.height.mas_equalTo(30*kWScale);
    }];
    
}

 

#pragma mark ------ okBtnAction

- (void)okBtnAction {
    [self dismissViewControllerAnimated:YES completion:^{
        if (self.QJMallConfirmOrderBlock) {
            self.QJMallConfirmOrderBlock();
        }
    }];
}


#pragma mark  ------ getter

- (UILabel *)firstLabel {
    if (!_firstLabel) {
        _firstLabel = [UILabel new];
        _firstLabel.font = FontSemibold(16);
        _firstLabel.textColor = kColorWithHexString(@"#000000");
        _firstLabel.text = @"兑换成功~";
        _firstLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _firstLabel;
}

- (UILabel *)subLabel {
    if (!_subLabel) {
        _subLabel = [UILabel new];
        _subLabel.font = FontRegular(12);
        _subLabel.textColor = kColorWithHexString(@"#000000");
        _subLabel.text = @"订单可在商城中订单管理中查看哦~";
        _subLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _subLabel;
}

- (UIImageView *)centerImageView {
    if (!_centerImageView) {
        _centerImageView = [UIImageView new];
        _centerImageView.image = [UIImage imageNamed:@"empty_no_finish"];
    }
    return _centerImageView;
}

- (UIButton *)okButton {
    if (!_okButton) {
        _okButton = [UIButton new];
        [_okButton setTitle:@"完成" forState:UIControlStateNormal];
        [_okButton addTarget:self action:@selector(okBtnAction) forControlEvents:UIControlEventTouchUpInside];
        [_okButton setTitleColor:RGB(46, 104, 251) forState:UIControlStateNormal];
        [_okButton.titleLabel setFont:FontRegular(17)];
    }
    return _okButton;
}

@end
