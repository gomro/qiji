//
//  QJMallConfirmOrderVC.h
//  QJBox
//
//  Created by wxy on 2022/8/24.
//

#import "QJBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

/// 支付订单页面
@interface QJMallConfirmOrderVC : QJBaseViewController


@property (nonatomic, copy) NSString *cartNo;

@end

NS_ASSUME_NONNULL_END
