//
//  QJEditeAddressViewController.m
//  QJBox
//
//  Created by wxy on 2022/8/24.
//

#import "QJEditeAddressViewController.h"
#import "QJMallAddressBottomView.h"
#import "QJMallAddressTextFieldCell.h"

#import <CoreLocation/CLLocationManager.h>
#import <MAMapKit/MAMapKit.h>
#import <AMapFoundationKit/AMapFoundationKit.h>
#import <AMapSearchKit/AMapSearchKit.h>
#import "QJLocationAddressListView.h"
#import "QJMallAddressDeleteRequest.h"
#import "QJMallAddressAddOrEditRequest.h"

#import "QJDetailAlertView.h"
#import "TYAlertController.h"
#import "QJMallSelAddressView.h"
#import "QJMallCnAreaModel.h"

@interface QJEditeAddressViewController ()<UITableViewDelegate,UITableViewDataSource,CLLocationManagerDelegate,MAMapViewDelegate,AMapSearchDelegate,UITextViewDelegate>
@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) QJMallAddressBottomView *bottomView;

@property (nonatomic, strong) IQTextView *textView;//详细地址

@property (nonatomic, strong) UISwitch *switchBtn;

@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) AMapSearchAPI *searchApi;
@property (nonatomic, strong) NSMutableArray *poiArray;
@property (nonatomic, strong) QJLocationAddressListView *addressView;
@property (nonatomic, strong) NSString *nameString;
@property (nonatomic, strong) NSString *phoneString;
@property (nonatomic, strong) NSString *cityString;
@property (nonatomic, strong) NSString *addressString;
@property (nonatomic, strong) NSString *isDefault;
@property (nonatomic, strong) AMapPOI *choosePoi;
@property (nonatomic, strong) NSString *districtName;
@property (nonatomic, strong) NSString *districtCode;
@property (nonatomic, strong) NSString *adCode;
@property (nonatomic, strong) QJMallSelAddressView *selAddressView;

@end

@implementation QJEditeAddressViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navBar.backgroundColor = RGB(244, 244, 244);
    if (self.type == QJEditeAddressType_Create) {
        self.title = @"添加收货地址";
    }else if (self.type == QJEditeAddressType_Edite) {
        self.title = @"编辑收货地址";
        UIButton *btn = [UIButton new];
        [btn setTitle:@"删除" forState:UIControlStateNormal];
        btn.titleLabel.font = kFont(14);
        [btn setTitleColor:kColorWithHexString(@"#FF3B30") forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(deleteAction) forControlEvents:UIControlEventTouchUpInside];
        self.navBar.rightBarItems = @[btn];
    }
    
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.bottomView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.navBar.mas_bottom);
            make.left.right.equalTo(self.view);
            make.bottom.equalTo(self.bottomView.mas_top);
            
    }];
    
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.equalTo(self.view);
            make.height.equalTo(@(Bottom_iPhoneX_SPACE + 56*kWScale));
    }];
    [self.tableView reloadData];
    
    self.poiArray = [NSMutableArray array];
    self.isDefault = @"0";
    self.choosePoi = [AMapPOI new];
    [self initGps];
    
    [AMapSearchAPI updatePrivacyShow:AMapPrivacyShowStatusDidShow privacyInfo:AMapPrivacyInfoStatusDidContain];
    [AMapSearchAPI updatePrivacyAgree:AMapPrivacyAgreeStatusDidAgree];

    self.searchApi = [[AMapSearchAPI alloc] init];
    self.searchApi.delegate = self;
    
    self.addressView = [[QJLocationAddressListView alloc] initWithFrame:CGRectMake(0, 0, QJScreenWidth - (16*3+70), 284*kWScale)];
    self.addressView.layer.shadowColor = UIColorHex(CECECE40).CGColor;
    self.addressView.layer.shadowOpacity = 3;
    
    self.addressView.layer.shadowRadius = 6;
    self.addressView.layer.shadowOffset = CGSizeMake(0, 0);

    WS(weakSelf)
    self.addressView.localAddress = ^(NSString * _Nonnull city, NSString * _Nonnull address, AMapPOI * _Nonnull model) {
        weakSelf.choosePoi = model;
        weakSelf.addressView.hidden = YES;
        weakSelf.cityString = city;
        if (address.length > 50) {
            address = [address substringToIndex:50];
        }
        [weakSelf getAdCode:model.city];
        [weakSelf getAddressCode:model];
        weakSelf.addressString = address;
        weakSelf.textView.text = address;
        [weakSelf.tableView reloadData];
        [weakSelf getPostDic];
    };
    self.addressView.backgroundColor = [UIColor redColor];
    [self.view addSubview:self.addressView];
    [self.addressView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(61*kWScale*3+NavigationBar_Bottom_Y-8);
        make.left.equalTo(self.view).offset(16*2+70);
        make.right.equalTo(self.view).offset(-16);
        make.height.equalTo(@(284*kWScale));
    }];
    self.addressView.hidden = YES;
    
    [self resetTableData];
//    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickHiddenAddressView)];
//    [self.view addGestureRecognizer:tap];
}

- (void)resetTableData{
    if (self.model) {
        self.nameString = self.model.linkman;
        self.cityString = [NSString stringWithFormat:@"%@%@%@%@",self.model.province?:@"",self.model.city?:@"",self.model.area?:@"",self.model.street?:@""];
        
        self.choosePoi.province = self.model.province;
        self.choosePoi.pcode = self.model.provinceAreacode;
        self.choosePoi.city = self.model.city;
        self.adCode = self.model.cityeAreacode;
        self.choosePoi.district = self.model.area;
        self.choosePoi.adcode = self.model.areaAreacode;
        self.districtName = self.model.street;
        self.districtCode = self.model.streetAreacode;
        
        self.addressString = self.model.detail;
        self.phoneString = self.model.mobile;
        self.isDefault = @"0";
        if (self.model.isDefault == YES) {
            self.isDefault = @"1";
        }
        self.switchBtn.on = self.model.isDefault;
        self.textView.text = self.addressString;
        
        [self changeBottomBtn];
    }
    [self.tableView reloadData];
}

- (void)clickHiddenAddressView{
    self.addressView.hidden = YES;
}

- (void)initGps {
    _locationManager = [[CLLocationManager alloc]init];
    
    if (![CLLocationManager locationServicesEnabled]) {
        [self.view makeToast:@"定位服务当前可能尚未打开，请在设置中打开！"];
        return;
    }
    
    if([_locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]){
        //如果没有授权则请求用户授权
        if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusRestricted){
            [_locationManager requestWhenInUseAuthorization];
        }
    }
    
    _locationManager.delegate = self;
    //设置定位精度
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    //定位频率,每隔多少米定位一次
    CLLocationDistance distance = 10;
    _locationManager.distanceFilter = distance;
    //启动跟踪定位
    [_locationManager startUpdatingLocation];
}

#pragma mark -- CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    //如果不需要实时定位，使用完即使关闭定位服务
    CLLocation *location = [locations firstObject];//取出第一个位置
    CLLocationCoordinate2D coordinate=location.coordinate;//位置坐标
    [self getAddressByLatitude:coordinate.latitude longitude:coordinate.longitude];
    [self.locationManager stopUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    DLog(@"定位失败，请点击重试GPS定位")
}

- (void)getAddressByLatitude:(CLLocationDegrees)latitude longitude:(CLLocationDegrees)longitude {
    CLGeocoder *_geocoder = [[CLGeocoder alloc]init];
    // 保存 Device 的现语言
    NSMutableArray *userDefaultLanguages = [[NSUserDefaults standardUserDefaults]
                                            objectForKey:@"AppleLanguages"];
    // 强制 成 简体中文
    [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:@"zh-hans",nil]
                                              forKey:@"AppleLanguages"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    //反地理编码
    CLLocation *location=[[CLLocation alloc]initWithLatitude:latitude longitude:longitude];
    [_geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
        if(error){
            DLog(@"error===%@",error);
        }
        CLPlacemark *placemark=[placemarks firstObject];
        if(placemarks){
            //1100 1600
            NSString *pro = placemark.administrativeArea;
            //获取城市
            NSString *cityNew = @"";
            NSString *city = placemark.locality;
            cityNew = city;
            city = [city stringByReplacingOccurrencesOfString:@"市" withString:@""];
            if (!city) {
                //如果city为空，则为直辖市
                city = pro;
            }
            [QJAppTool shareManager].selCity = cityNew;
            [QJAppTool shareManager].selPro = pro;
            [QJAppTool shareManager].latitude = latitude;
            [QJAppTool shareManager].longitude = longitude;
            DLog(@"latitude = %f longitude = %f",latitude,longitude);
        }
        // 还原Device 的语言
        [[NSUserDefaults standardUserDefaults] setObject:userDefaultLanguages forKey:@"AppleLanguages"];
    }];
}

- (void)POISearch{
    AMapPOIAroundSearchRequest *request = [[AMapPOIAroundSearchRequest alloc] init];
    request.location = [AMapGeoPoint locationWithLatitude:[QJAppTool shareManager].latitude longitude:[QJAppTool shareManager].longitude];
    request.keywords            = @"住宿服务|商务住宅|公司企业";
    request.sortrule            = 0;
    request.offset = 25;
    request.showFieldsType    = AMapPOISearchShowFieldsTypeAll;
    [self.searchApi AMapPOIAroundSearch:request];
}

- (void)getAdCode:(NSString *)keyworld{
    AMapDistrictSearchRequest *dist = [[AMapDistrictSearchRequest alloc] init];
    dist.keywords = keyworld;
    dist.requireExtension = YES;
    [self.searchApi AMapDistrictSearch:dist];
}

- (void)getAddressCode:(AMapPOI *)mapPoi{
    AMapReGeocodeSearchRequest *regeo = [[AMapReGeocodeSearchRequest alloc] init];
    regeo.location                    = [AMapGeoPoint locationWithLatitude:mapPoi.location.latitude longitude:mapPoi.location.longitude];
    regeo.requireExtension            = YES;
    [self.searchApi AMapReGoecodeSearch:regeo];
}

/* 逆地理编码回调. */
- (void)onReGeocodeSearchDone:(AMapReGeocodeSearchRequest *)request response:(AMapReGeocodeSearchResponse *)response
{
    if (response.regeocode != nil)
    {
        AMapAddressComponent *address = response.regeocode.addressComponent;
        if (address) {
            self.adCode = address.adcode;
            if (address.township.length > 0) {
                self.cityString = [self.cityString stringByAppendingString:address.township];
            }
            self.districtName = address.township;
            self.districtCode = address.towncode;
            [self.tableView reloadData];
        }
    }
}

- (void)onDistrictSearchDone:(AMapDistrictSearchRequest *)request response:(AMapDistrictSearchResponse *)response
{
    if (response.districts.count == 0){
        return;
    }
    [response.districts enumerateObjectsUsingBlock:^(AMapDistrict * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        self.adCode = obj.adcode;
    }];
}

/* POI 搜索回调. */
- (void)onPOISearchDone:(AMapPOISearchBaseRequest *)request response:(AMapPOISearchResponse *)response
{
    if (response.pois.count == 0){
        return;
    }
    [self.poiArray removeAllObjects];
    [response.pois enumerateObjectsUsingBlock:^(AMapPOI * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [self.poiArray addObject:obj];
    }];
    self.addressView.hidden = NO;
    self.addressView.dataArray = self.poiArray;
}

- (void)changeBottomBtn {
    if (self.nameString.length > 0 && self.phoneString.length > 0 && self.cityString.length > 0 && self.addressString.length > 0) {
        self.bottomView.btn.enabled = YES;
    }else{
        self.bottomView.btn.enabled = NO;
    }
}

#pragma mark ----- getter
- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        [_tableView registerClass:[QJMallAddressTextFieldCell class] forCellReuseIdentifier:@"QJMallAddressTextFieldCell"];
        [_tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"UITableViewCell"];
        [_tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"UITableViewCellSwitch"];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.contentInset = UIEdgeInsetsMake(16, 0, 0, 0);
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _tableView;
}

- (QJMallAddressBottomView *)bottomView {
    if (!_bottomView) {
        _bottomView = [QJMallAddressBottomView new];
        _bottomView.title = @"保存";
        _bottomView.btn.enabled = NO;
        WS(weakSelf)
        _bottomView.bottomBtnBlock = ^{
            [weakSelf addAddressNetWork:nil];
        };
    }
    return _bottomView;
}
 
- (IQTextView *)textView {
    if (!_textView) {
        _textView = [IQTextView new];
        _textView.attributedPlaceholder = [[NSAttributedString alloc]initWithString:@"小区楼栋/乡村名称" attributes:@{NSFontAttributeName : FontRegular(14),NSForegroundColorAttributeName : kColorWithHexString(@"#919599")}];
        _textView.font = FontRegular(14);
        _textView.textColor = kColorWithHexString(@"#16191C");
        _textView.backgroundColor = kColorWithHexString(@"#F5F5F5");
        _textView.layer.cornerRadius = 8;
        _textView.delegate = self;
    }
    return _textView;
}


- (UISwitch *)switchBtn {
    if (!_switchBtn) {
        _switchBtn = [UISwitch new];
        [_switchBtn addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];
//        _switchBtn.on = YES;
    }
    return _switchBtn;
}

- (QJMallSelAddressView *)selAddressView {
    if (!_selAddressView) {
        _selAddressView = [[QJMallSelAddressView alloc] init];
    }
    return _selAddressView;
}

#pragma mark ------ action
- (void)onBack:(id)sender{
    if (self.nameString.length > 0 || self.phoneString.length > 0 || self.cityString.length > 0 || self.addressString.length > 0) {
        QJDetailAlertView *alertView = [[QJDetailAlertView alloc] initWithFrame:CGRectZero titleType:QJDetailAlertViewTitleTypeDefault buttonType:QJDetailAlertViewButtonTypeDefault];
        [alertView showTitleText:@"提示" describeText:@"退出后本次编辑操作将不会保留"];
        [alertView.leftButton setTitle:@"退出" forState:UIControlStateNormal];
        [alertView.rightButton setTitle:@"继续" forState:UIControlStateNormal];
        [alertView.rightButton setTitleColor:UIColorFromRGB(0x007AFF) forState:UIControlStateNormal];
        WS(weakSelf)
        [alertView returnClick:^(QJDetailAlertViewButtonClickType type) {
            [[QJAppTool getCurrentViewController] dismissViewControllerAnimated:YES completion:nil];
            if (type == QJDetailAlertViewButtonClickTypeLeft) {
                [weakSelf.navigationController popViewControllerAnimated:YES];
            }
        }];
        TYAlertController *alertController = [TYAlertController alertControllerWithAlertView:alertView preferredStyle:TYAlertControllerStyleAlert];
        [self.navigationController presentViewController:alertController animated:YES completion:nil];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)deleteAction {
    QJDetailAlertView *alertView = [[QJDetailAlertView alloc] initWithFrame:CGRectZero titleType:QJDetailAlertViewTitleTypeDefault buttonType:QJDetailAlertViewButtonTypeDefault];
    [alertView showTitleText:@"提示" describeText:@"确定要删除地址吗？"];
    [alertView.leftButton setTitle:@"取消" forState:UIControlStateNormal];
    [alertView.rightButton setTitle:@"确定" forState:UIControlStateNormal];
    [alertView.rightButton setTitleColor:UIColorFromRGB(0x007AFF) forState:UIControlStateNormal];
    WS(weakSelf)
    [alertView returnClick:^(QJDetailAlertViewButtonClickType type) {
        [[QJAppTool getCurrentViewController] dismissViewControllerAnimated:YES completion:nil];
        if (type == QJDetailAlertViewButtonClickTypeRight) {
            [weakSelf deleteAddress:weakSelf.model.ID];
        }
    }];
    TYAlertController *alertController = [TYAlertController alertControllerWithAlertView:alertView preferredStyle:TYAlertControllerStyleAlert];
    [self.navigationController presentViewController:alertController animated:YES completion:nil];
}

- (void)deleteAddress:(NSString *)ID{
    QJMallAddressDeleteRequest *deleteReq = [QJMallAddressDeleteRequest new];
    [deleteReq deleteAddressWithID:ID];
    WS(weakSelf)
    deleteReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            [weakSelf.view makeToast:@"删除成功"];
            if (weakSelf.updateAddress) {
                weakSelf.updateAddress();
            }
            [weakSelf.navigationController popViewControllerAnimated:YES];
        }else{
            NSString *msg = EncodeStringFromDic(request.responseObject, @"message");
            [weakSelf.view makeToast:msg];
        }
    };
    deleteReq.failureCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        [weakSelf.view makeToast:@"数据异常 请稍后再试"];
    };
}

- (void)addAddressNetWork:(QJMallAddressModel *)model {
    DLog(@"新增地址");
    NSMutableDictionary *muDic = [NSMutableDictionary dictionary];
    [muDic setValue:[NSNumber numberWithString:self.isDefault] forKey:@"isDefault"];
    [muDic setValue:self.nameString forKey:@"linkman"];
    [muDic setValue:self.phoneString forKey:@"mobile"];
    [muDic setValue:self.choosePoi.province forKey:@"province"];
    [muDic setValue:self.choosePoi.pcode forKey:@"provinceAreacode"];
    [muDic setValue:self.choosePoi.city forKey:@"city"];
    [muDic setValue:self.adCode forKey:@"cityeAreacode"];
    [muDic setValue:self.choosePoi.district forKey:@"area"];
    [muDic setValue:self.choosePoi.adcode forKey:@"areaAreacode"];
    if (self.model.ID) {
        [muDic setValue:self.model.ID forKey:@"id"];
    }
    if (self.districtName.length > 0) {
        [muDic setValue:self.districtName forKey:@"street"];
    }
    if (self.districtCode.length > 0) {
        [muDic setValue:self.districtCode forKey:@"streetAreacode"];
    }
    [muDic setValue:self.addressString forKey:@"detail"];
    QJMallAddressAddOrEditRequest *addAddressApi = [QJMallAddressAddOrEditRequest new];
    addAddressApi.dic = muDic.copy;
    [addAddressApi start];
    WS(weakSelf)
    addAddressApi.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            if (weakSelf.type == QJEditeAddressType_Create) {
                [weakSelf.view makeToast:@"添加成功"];
            }else if (weakSelf.type == QJEditeAddressType_Edite) {
                [weakSelf.view makeToast:@"修改成功"];
            }
            if (weakSelf.updateAddress) {
                weakSelf.updateAddress();
            }
            [weakSelf.navigationController popViewControllerAnimated:YES];
        }else{
            NSString *msg = EncodeStringFromDic(request.responseObject, @"message");
            [weakSelf.view makeToast:msg];
        }
    };
    addAddressApi.failureCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        [weakSelf.tableView reloadData];
        [weakSelf.view makeToast:@"数据异常 请稍后再试"];
    };
}

- (void)switchAction:(UISwitch *)sender {
    if (_switchBtn.on == YES) {
        self.isDefault = @"1";
    }else{
        self.isDefault = @"0";
    }
}


#pragma mark ------ UITableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 3) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UITableViewCell" forIndexPath:indexPath];
        cell.backgroundColor = [UIColor whiteColor];
        UILabel *firstLabel = [UILabel new];
        firstLabel.font = FontSemibold(16);
        firstLabel.textColor = kColorWithHexString(@"#000000");
        firstLabel.text = @"详细地址";
        firstLabel.textAlignment = NSTextAlignmentLeft;
        [cell.contentView addSubview:firstLabel];
        [firstLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.equalTo(cell.contentView).offset(16);
                    make.top.equalTo(cell.contentView);
                    make.width.equalTo(@(70*kWScale));
            make.height.equalTo(@(37*kWScale));
        }];
        
        [cell.contentView addSubview:self.textView];
        [self.textView mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.right.equalTo(cell.contentView).offset(-16);
                    make.top.equalTo(cell.contentView);
            make.left.equalTo(firstLabel.mas_right).offset(16);
            make.height.equalTo(@(79*kWScale));
        }];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
        
    }else if (indexPath.row == 4){
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UITableViewCellSwitch" forIndexPath:indexPath];
        cell.backgroundColor = [UIColor whiteColor];
        UILabel *firstLabel = [UILabel new];
        firstLabel.font = FontSemibold(16);
        firstLabel.textColor = kColorWithHexString(@"#000000");
        firstLabel.text = @"设为默认地址";
        firstLabel.textAlignment = NSTextAlignmentLeft;
        [cell.contentView addSubview:firstLabel];
        
        
        [firstLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.equalTo(cell.contentView).offset(16);
                    make.centerY.equalTo(cell.contentView);
                    make.width.equalTo(@(110*kWScale));
                    
        }];
        
        [cell.contentView addSubview:self.switchBtn];
        [self.switchBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.right.equalTo(cell.contentView).offset(-16);
                    make.centerY.equalTo(cell.contentView);
        }];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
        
    }else{
        QJMallAddressTextFieldCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QJMallAddressTextFieldCell" forIndexPath:indexPath];
        WS(weakSelf)
        cell.textInputBlock = ^{
            [weakSelf getPostDic];
        };
        if (indexPath.row == 0) {
            cell.maxCount = 10;
            cell.nameLabel.text = @"收货人";
            cell.textField.attributedPlaceholder = [[NSAttributedString alloc]initWithString:@"名字" attributes:@{NSFontAttributeName : FontRegular(14),NSForegroundColorAttributeName : kColorWithHexString(@"#919599")}];
            cell.textField.userInteractionEnabled = YES;
            cell.textField.text = self.nameString?:@"";
            cell.btn.hidden = YES;
        }else if (indexPath.row == 1) {
            cell.maxCount = 11;
            cell.nameLabel.text = @"手机号码";
            cell.textField.attributedPlaceholder = [[NSAttributedString alloc]initWithString:@"手机号" attributes:@{NSFontAttributeName : FontRegular(14),NSForegroundColorAttributeName : kColorWithHexString(@"#919599")}];
            cell.textField.userInteractionEnabled = YES;
            cell.textField.text = self.phoneString?:@"";
            cell.btn.hidden = YES;
        }else if (indexPath.row == 2) {
            cell.nameLabel.text = @"所在地区";
            cell.textField.attributedPlaceholder = [[NSAttributedString alloc]initWithString:@"省、市、区、街道" attributes:@{NSFontAttributeName : FontRegular(14),NSForegroundColorAttributeName : kColorWithHexString(@"#919599")}];
            cell.textField.userInteractionEnabled = NO;
            cell.textField.text = self.cityString?:@"";
            cell.btn.hidden = NO;
            cell.localAddress = ^{
                DLog(@"推荐周边地址");
                if ([QJDeviceConstant hasLocationAuth] == NO) {
                    [self initGps];
                }
                [self POISearch];
            };
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    return nil;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 3) {
        return 103*kWScale;
    }else{
        return 61*kWScale;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 2) {
        WS(weakSelf)
        QJMallCnAreaModel *modelPro = [QJMallCnAreaModel new];
        modelPro.level = 0;
        modelPro.name = self.choosePoi.province;
        modelPro.areaCode = self.choosePoi.pcode;
        QJMallCnAreaModel *modelCity = [QJMallCnAreaModel new];
        modelCity.level = 1;
        modelCity.name = self.choosePoi.city;
        modelCity.areaCode = self.adCode;
        QJMallCnAreaModel *modelArea = [QJMallCnAreaModel new];
        modelArea.level = 2;
        modelArea.name = self.choosePoi.district;
        modelArea.areaCode = self.choosePoi.adcode;
        QJMallCnAreaModel *modelDist = [QJMallCnAreaModel new];
        modelDist.level = 3;
        modelDist.name = self.districtName;
        modelDist.areaCode = self.districtCode;
        NSArray *codeArray = @[modelPro,modelCity,modelArea,modelDist];
        self.selAddressView.selAddressBlock = ^(NSArray * _Nonnull dataArray) {
            QJMallCnAreaModel *proModel = [dataArray safeObjectAtIndex:0];
            QJMallCnAreaModel *cityModel = [dataArray safeObjectAtIndex:1];
            QJMallCnAreaModel *areaModel = [dataArray safeObjectAtIndex:2];
            QJMallCnAreaModel *districtModel = [dataArray safeObjectAtIndex:3];
            weakSelf.choosePoi.province = proModel.name;
            weakSelf.choosePoi.pcode = proModel.areaCode;
            weakSelf.choosePoi.city = cityModel.name;
            weakSelf.adCode = cityModel.areaCode;
            weakSelf.choosePoi.citycode = cityModel.areaCode;
            weakSelf.choosePoi.district = areaModel.name;
            weakSelf.choosePoi.adcode = areaModel.areaCode;
            weakSelf.districtName = districtModel.name;
            weakSelf.districtCode = districtModel.areaCode;
            NSString *proStr = proModel.name?:@"";
            if ([[weakSelf tipArray] containsObject:proStr]) {
                proStr = @"";
            }
            NSString *cityStr = cityModel.name?:@"";
            if ([[weakSelf tipArray] containsObject:cityStr]) {
                cityStr = @"";
            }
            NSString *areaStr = areaModel.name?:@"";
            if ([[weakSelf tipArray] containsObject:areaStr]) {
                areaStr = @"";
            }
            NSString *districtStr = districtModel.name?:@"";
            if ([[weakSelf tipArray] containsObject:districtStr]) {
                districtStr = @"";
            }
            weakSelf.cityString = [NSString stringWithFormat:@"%@%@%@%@",proStr,cityStr,areaStr,districtStr];
            weakSelf.textView.text = @"";
            weakSelf.addressString = @"";
            [weakSelf.tableView reloadData];
            [weakSelf changeBottomBtn];
        };

        self.selAddressView.locationArray = codeArray;
        [self.selAddressView show];
    }
}

- (NSArray *)tipArray  {
    return @[@"请选择城市",@"请选择城县",@"请选择街道",@"暂不选择"];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    self.addressView.hidden = YES;
}

- (void)getPostDic{
    QJMallAddressTextFieldCell *nameCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    QJMallAddressTextFieldCell *phoneCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    QJMallAddressTextFieldCell *areaCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
    self.nameString = nameCell.textField.text;
    self.phoneString = phoneCell.textField.text;
    self.cityString = areaCell.textField.text;
    [self changeBottomBtn];
    DLog(@"name = %@,phone = %@,area = %@",nameCell.textField.text,phoneCell.textField.text,areaCell.textField.text);
}

#pragma mark - UITextView
-(void)textViewDidChange:(UITextView *)textView {
    UITextRange *selectedRange = [textView markedTextRange];
    UITextPosition *pos = [textView positionFromPosition:selectedRange.start offset:0];
    if (selectedRange && pos) {
        return;
    }
    if (textView.text.length > 50) {
        textView.text = [textView.text substringToIndex:50];
    }
    self.addressString = textView.text;
    [self changeBottomBtn];
}

@end
