//
//  QJAddressViewController.h
//  QJBox
//
//  Created by wxy on 2022/8/24.
//

#import "QJBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN
@class QJMallAddressModel;
/// 地址列表
@interface QJAddressViewController : QJBaseViewController

@property (nonatomic, copy) void(^selectedAddress)(QJMallAddressModel *model);

@property (nonatomic, strong) QJMallAddressModel *model;

@property (nonatomic, assign) BOOL isFromMine;

@end

NS_ASSUME_NONNULL_END
