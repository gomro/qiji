//
//  QJMallExchangeSucceededVC.h
//  QJBox
//
//  Created by wxy on 2022/8/25.
//

#import "QJBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

/// 兑换成功
@interface QJMallExchangeSucceededVC : QJBaseViewController
@property (nonatomic, copy) void(^QJMallConfirmOrderBlock)(void);
@end

NS_ASSUME_NONNULL_END
