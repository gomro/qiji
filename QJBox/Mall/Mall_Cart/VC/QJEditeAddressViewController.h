//
//  QJEditeAddressViewController.h
//  QJBox
//
//  Created by wxy on 2022/8/24.
//

#import "QJBaseViewController.h"
#import "QJMallAddressListModel.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, QJEditeAddressType) {
    QJEditeAddressType_Create,//新建
    QJEditeAddressType_Edite,//编辑
     
};

/// 新建，编辑地址
@interface QJEditeAddressViewController : QJBaseViewController

@property (nonatomic, assign) QJEditeAddressType type;

@property (nonatomic, strong) QJMallAddressModel *model;

@property (nonatomic, copy) void(^updateAddress)(void);

@end

NS_ASSUME_NONNULL_END
