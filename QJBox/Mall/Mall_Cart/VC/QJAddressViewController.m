//
//  QJAddressViewController.m
//  QJBox
//
//  Created by wxy on 2022/8/24.
//

#import "QJAddressViewController.h"
#import "QJMallAddressListCell.h"
#import "QJEditeAddressViewController.h"
#import "QJMallAddressBottomView.h"
#import "QJMallAddressListModel.h"
#import "QJMallAddressModel.h"
#import "QJMallAddressListRequest.h"

@interface QJAddressViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) QJMallAddressBottomView *bottomView;

@property (nonatomic, strong) QJMallAddressListRequest *addressListReq;

@property (nonatomic, strong) QJMallAddressListModel *pageModel;

@end

@implementation QJAddressViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"我的收货地址";
    self.navBar.backgroundColor = RGB(244, 244, 244);
    self.view.backgroundColor = UIColorFromRGB(0xF5F5F5);//RGB(244, 244, 244);
    self.pageModel = [[QJMallAddressListModel alloc] init];
    [self makeUI];
    [self refreshData];
}


#pragma mark -------- make UI
- (void)makeUI {
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.bottomView];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.navBar.mas_bottom);
            make.left.right.equalTo(self.view);
            make.bottom.equalTo(self.bottomView.mas_top);
    }];
    
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.view);
            make.left.right.equalTo(self.view);
            make.height.equalTo(@(Bottom_iPhoneX_SPACE + 56*kWScale));
    }];
    WS(weakSelf)
    self.bottomView.bottomBtnBlock = ^{
        QJEditeAddressViewController *vc = [QJEditeAddressViewController new];
        vc.updateAddress = ^{
            [weakSelf queryAddressList];
        };
        [weakSelf.navigationController pushViewController:vc animated:YES];
    };
}
 
#pragma mark ------网络请求
- (void)queryAddressList {
    WS(weakSelf)
    NSMutableDictionary *muDic = [NSMutableDictionary dictionaryWithDictionary:_pageModel.toParams];
    self.addressListReq.dic = muDic.copy;
    [self.addressListReq start];
    self.addressListReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            [weakSelf.pageModel configObj:[QJMallAddressListModel modelWithDictionary:EncodeDicFromDic(request.responseObject, @"data")]];
            [weakSelf.tableView reloadData];
        }
        [weakSelf.tableView.mj_header endRefreshing];
        [weakSelf.tableView.mj_footer endRefreshing];
        QJEmptyCommonView *emptyView = [QJEmptyCommonView new];
        emptyView.string = @"您还没有收货地址";
        emptyView.topSpace = 100*kWScale;
        emptyView.backgroundColor = UIColorFromRGB(0xF5F5F5);
        [weakSelf.tableView tableViewDisplayWhenHaveNoDataWithView:emptyView ifNecessaryForRowCount:weakSelf.pageModel.records.count];

    };
    self.addressListReq.failureCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        [weakSelf.tableView.mj_header endRefreshing];
        [weakSelf.tableView.mj_footer endRefreshing];
        QJEmptyCommonView *emptyView = [QJEmptyCommonView new];
        emptyView.string = @"您还没有收货地址";
        emptyView.topSpace = 100*kWScale;
        emptyView.backgroundColor = UIColorFromRGB(0xF5F5F5);
        [weakSelf.tableView tableViewDisplayWhenHaveNoDataWithView:emptyView ifNecessaryForRowCount:weakSelf.pageModel.records.count];
        
    };
}


#pragma mark -------getter
- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        [_tableView registerClass:[QJMallAddressListCell class] forCellReuseIdentifier:@"QJMallAddressListCell"];
        _tableView.estimatedRowHeight = 77*kWScale;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = UIColorFromRGB(0xFFFFFF);
        _tableView.mj_header = [QJRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(refreshData)];
        _tableView.mj_footer = [QJRefreshFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreHomeData)];
    }
    return _tableView;
}

- (QJMallAddressBottomView *)bottomView {
    if (!_bottomView) {
        
        _bottomView = [QJMallAddressBottomView new];
        
    }
    return _bottomView;
}

-(QJMallAddressListRequest *)addressListReq {
    if (!_addressListReq) {
        _addressListReq = [QJMallAddressListRequest new];
        
    }
    return _addressListReq;
}
#pragma mark --------UITableViewDelegate,UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.pageModel.records.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (self.pageModel.records.count == 0) {
        return 0.001;
    } else {
        return 12;
    }
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (self.pageModel.records.count == 0) {
        return [UIView new];
    } else {
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, QJScreenWidth, 12)];
        headerView.backgroundColor = [UIColor whiteColor];
        [headerView showCorner:10 rectCorner:UIRectCornerTopLeft | UIRectCornerTopRight];
        return headerView;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    QJMallAddressModel *model = [self.pageModel.records safeObjectAtIndex:indexPath.row];
    QJMallAddressListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QJMallAddressListCell" forIndexPath:indexPath];
    cell.model = model;
    WS(weakSelf)
    cell.editBlock = ^{
        DLog(@"编辑");
        QJEditeAddressViewController *vc = [QJEditeAddressViewController new];
        vc.model = model;
        vc.type = QJEditeAddressType_Edite;
        vc.updateAddress = ^{
            [weakSelf queryAddressList];
        };
        [weakSelf.navigationController pushViewController:vc animated:YES];
    };
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    QJMallAddressModel *model = [self.pageModel.records safeObjectAtIndex:indexPath.row];
    if (self.selectedAddress) {
        self.selectedAddress(model);
    }
    if (!self.isFromMine) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - 请求方法
- (void)refreshData {
    _pageModel.willLoadMore = NO;
    [self queryAddressList];
}

- (void)loadMoreHomeData {
    DLog(@"加载更多");
    if(_pageModel.hasNext){
        _pageModel.willLoadMore=YES;
        [self queryAddressList];
    }else{
        [self.tableView.mj_footer endRefreshing];
        [self.tableView.mj_footer endRefreshingWithNoMoreData];
    }
}

@end
