//
//  QJMallConfirmFillAddressCell.m
//  QJBox
//
//  Created by wxy on 2022/8/24.
//

#import "QJMallConfirmFillAddressCell.h"


@interface QJMallConfirmFillAddressCell ()

@property (nonatomic, strong) UIView *backView;

@property (nonatomic, strong) UIImageView *leftImageView;

@property (nonatomic, strong) UILabel *areaLabe;

@property (nonatomic, strong) UILabel *personLabel;

@property (nonatomic, strong) UIImageView *arrowImageView;
@end



@implementation QJMallConfirmFillAddressCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.contentView.backgroundColor = RGB(244, 244, 244);
        
        [self.contentView addSubview:self.backView];
        
        [self.contentView addSubview:self.leftImageView];
        [self.contentView addSubview:self.areaLabe];
        [self.contentView addSubview:self.personLabel];
        [self.contentView addSubview:self.arrowImageView];
        [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.contentView);
        }];
        
        [self.leftImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.contentView).offset(10);
            make.left.equalTo(self.contentView).offset(8);
            make.width.height.equalTo(@(16*kWScale));
        }];
        
        [self.areaLabe mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.contentView).offset(8);
            make.left.equalTo(self.contentView).offset(32);
            make.right.equalTo(self.arrowImageView.mas_left).offset(-16);
                    
        }];
        
        [self.personLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.left.equalTo(self.areaLabe);
            make.top.equalTo(self.areaLabe.mas_bottom).offset(8);
            make.bottom.equalTo(self.contentView).offset(-8);
        }];
        
        [self.arrowImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.contentView).offset(-8);
            make.centerY.equalTo(self.contentView);
            make.width.height.equalTo(@(16*kWScale));
        }];
        
        
    }
    return self;
}

- (void)setModel:(QJMallAddressModel *)model {
    _model = model;
    self.personLabel.text = [NSString stringWithFormat:@"%@ %@", self.model.linkman,self.model.mobile];
    self.areaLabe.text = [NSString stringWithFormat:@"%@%@%@%@", self.model.province,self.model.city,self.model.area,self.model.detail];
}

#pragma mark ------- getter
- (UIView *)backView {
    if(!_backView) {
        _backView = [UIView new];
        _backView.backgroundColor = [UIColor whiteColor];
        _backView.layer.cornerRadius = 8;
        _backView.layer.masksToBounds = YES;
    }
    return _backView;
}

-(UIImageView *)leftImageView {
    if (!_leftImageView) {
        _leftImageView = [UIImageView new];
        _leftImageView.image = [UIImage imageNamed:@"mall_cart_location_16"];
    }
    return _leftImageView;
}


- (UILabel *)areaLabe {
    if (!_areaLabe) {
        _areaLabe = [UILabel new];
        _areaLabe.font = FontRegular(14);
        _areaLabe.textColor = [UIColor blackColor];
        _areaLabe.textAlignment = NSTextAlignmentLeft;
//        _areaLabe.numberOfLines = 0;
    }
    return _areaLabe;
}


- (UILabel *)personLabel {
    if (!_personLabel) {
        _personLabel = [UILabel new];
        _personLabel.font = FontRegular(14);
        _personLabel.textColor = kColorWithHexString(@"#16191C");
        _personLabel.textAlignment = NSTextAlignmentLeft;
    }
    return _personLabel;
}

- (UIImageView *)arrowImageView {
    if (!_arrowImageView) {
        _arrowImageView = [UIImageView new];
        _arrowImageView.image = [UIImage imageNamed:@"mall_arrow_right16"];
    }
    return _arrowImageView;
}


@end

 
