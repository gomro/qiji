//
//  QJMallConfirmProductCell.m
//  QJBox
//
//  Created by wxy on 2022/8/24.
//

#import "QJMallConfirmProductCell.h"
#import "QJMallOrderModel.h"

@interface QJMallConfirmProductCell ()

@property (nonatomic, strong) UIView *backView;

@property (nonatomic, strong) UIImageView *productImageView;

@property (nonatomic, strong) UILabel *productNameLabel;

@property (nonatomic, strong) UILabel *skuLabel;

@property (nonatomic, strong) UILabel *productPriceLabel;

@property (nonatomic, strong) UILabel *productCountLabel;

@property (nonatomic, strong) UIView *lineView;

@property (nonatomic, strong) UILabel *sendTypeLabel;

@property (nonatomic, strong) UILabel *sendContentLabel;

@property (nonatomic, strong) UILabel *totalPriceLabel;

@property (nonatomic, strong) UILabel *totalPriceContentLabel;

@property (nonatomic, strong) UILabel *freightLabel;

@property (nonatomic, strong) UILabel *freightContentLabel;

@property (nonatomic, strong) UILabel *finalPriceLabel;//实付金额


@end


@implementation QJMallConfirmProductCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.contentView.backgroundColor = RGB(244, 244, 244);
        
        [self.contentView addSubview:self.backView];
        [self.contentView addSubview:self.productImageView];
        [self.contentView addSubview:self.productNameLabel];
        [self.contentView addSubview:self.skuLabel];
        [self.contentView addSubview:self.productPriceLabel];
        [self.contentView addSubview:self.productCountLabel];
        [self.contentView addSubview:self.lineView];
        [self.contentView addSubview:self.sendTypeLabel];
        [self.contentView addSubview:self.sendContentLabel];
        [self.contentView addSubview:self.totalPriceLabel];
        [self.contentView addSubview:self.totalPriceContentLabel];
        [self.contentView addSubview:self.freightLabel];
        [self.contentView addSubview:self.freightContentLabel];
        [self.contentView addSubview:self.finalPriceLabel];
        
        [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.contentView);
        }];
        
        [self.productImageView mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.top.left.equalTo(self.contentView).offset(16);
                    make.width.height.equalTo(@(64*kWScale));
                    
        }];
        
        [self.productNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.top.equalTo(self.contentView).offset(16);
                    make.left.equalTo(self.productImageView.mas_right).offset(16);
                    make.right.equalTo(self.productCountLabel.mas_left);
        }];
        
        [self.skuLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.productNameLabel.mas_bottom).mas_offset(2);
            make.left.equalTo(self.productImageView.mas_right).offset(16);
            make.right.mas_equalTo(-16);
        }];
        
        [self.productCountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.right.equalTo(self.contentView).offset(-24);
                    make.centerY.equalTo(self.productNameLabel);
                    
        }];
        [self.productCountLabel setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
        
        [self.productPriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.equalTo(self.productNameLabel);
                    make.top.equalTo(self.skuLabel.mas_bottom).offset(0);
                    make.right.equalTo(self.contentView).offset(-16);
        }];
        
        [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.equalTo(self.contentView).offset(16);
                    make.right.equalTo(self.contentView).offset(-16);
                    make.height.equalTo(@1);
                    make.top.equalTo(self.productImageView.mas_bottom).offset(16);
        }];
        
        [self.sendTypeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.equalTo(self.lineView);
                    make.top.equalTo(self.lineView.mas_bottom).offset(16);
                    make.width.equalTo(@(80*kWScale));
        }];
        
        [self.sendContentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.centerY.equalTo(self.sendTypeLabel);
                    make.right.equalTo(self.contentView).offset(-16);
                    
        }];
        
        [self.totalPriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.equalTo(self.lineView);
                    make.top.equalTo(self.sendContentLabel.mas_bottom).offset(16);
                    make.width.equalTo(@(80*kWScale));
        }];
        
        [self.totalPriceContentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.centerY.equalTo(self.totalPriceLabel);
                    make.right.equalTo(self.contentView).offset(-16);
                    
        }];
        
        [self.freightLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.equalTo(self.lineView);
                    make.top.equalTo(self.totalPriceLabel.mas_bottom).offset(16);
                    make.width.equalTo(@(80*kWScale));
        }];
        
        [self.freightContentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.centerY.equalTo(self.freightLabel);
                    make.right.equalTo(self.contentView).offset(-16);
                    
        }];
        
        [self.finalPriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.right.equalTo(self.contentView).offset(-16);
                    make.top.equalTo(self.freightContentLabel.mas_bottom).offset(16);
                    make.height.equalTo(@(24*kWScale));
            make.bottom.equalTo(self.contentView).offset(-16);
        }];
        
        
    }
    return self;
}

- (void)setModel:(QJMallOrderDetailModel *)model {
    _model = model;
    QJMallOrderItemModel *item = _model.items.firstObject;
    [self.productImageView setImageWithURL:[NSURL URLWithString:[item.picture checkImageUrlString]] placeholder:[UIImage imageNamed:@"mine_head"]];
    self.productNameLabel.text = item.name;
    NSString *price = [NSString stringWithFormat:@"%@ 奇迹币",item.qjcoinPartPrice];
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:price];
    [attrStr addAttribute:NSFontAttributeName
                    value:FontRegular(12)
                    range:NSMakeRange(price.length-3, 3)];
    self.productPriceLabel.attributedText = attrStr;
    self.productCountLabel.text = [NSString stringWithFormat:@"x%@",item.quantity];
    
    self.sendContentLabel.text = [self getDeliveryType:self.model.deliveryType.integerValue];
    self.freightContentLabel.text = [NSString stringWithFormat:@"￥%@",self.model.freightAmount];
    self.totalPriceContentLabel.text = [NSString stringWithFormat:@"%@奇迹币",self.model.qjcoinTotal];
    self.finalPriceLabel.text = [NSString stringWithFormat:@"实付金额: %@奇迹币",self.model.qjcoinPaid];

    NSMutableArray *propertieArray = [NSMutableArray array];
    for (QJMallOrderItemPropertieModel *propertie in item.properties) {
        [propertieArray addObject:propertie.value];
    }
    self.skuLabel.text = [propertieArray componentsJoinedByString:@" "];
}

- (NSString *)getDeliveryType:(NSInteger)type {
    switch (type) {
        case 0:
            return @"无需配送";
            break;
        case 1:
            return @"物流配送";
            break;
            
        default:
            return @"";
            break;
    }
}

#pragma mark ------- getter

- (UIView *)backView {
    if(!_backView) {
        _backView = [UIView new];
        _backView.backgroundColor = [UIColor whiteColor];
        _backView.layer.cornerRadius = 8;
        _backView.layer.masksToBounds = YES;
    }
    return _backView;
}

-(UIImageView *)productImageView {
    if (!_productImageView) {
        _productImageView = [UIImageView new];
        _productImageView.layer.cornerRadius = 4;
        _productImageView.layer.masksToBounds = YES;
    }
    return _productImageView;
}

- (UILabel *)productNameLabel {
    if (!_productNameLabel) {
        _productNameLabel = [UILabel new];
        _productNameLabel.font = FontMedium(14);
        _productNameLabel.textColor = kColorWithHexString(@"#000000");
        _productNameLabel.textAlignment = NSTextAlignmentLeft;
    }
    return _productNameLabel;
}

- (UILabel *)skuLabel {
    if (!_skuLabel) {
        _skuLabel = [[UILabel alloc] init];
        _skuLabel.text = @"比克大魔王 比克大魔王 比克大魔王 比克大魔王 比克大魔王v 比克大魔王 比克大魔王 比克大魔王";
        _skuLabel.textColor = UIColorFromRGB(0x919599);
        [_skuLabel setFont:FontRegular(14)];
        _skuLabel.numberOfLines = 0;
    }
    return _skuLabel;
}

- (UILabel *)productPriceLabel {
    if (!_productPriceLabel) {
        _productPriceLabel = [UILabel new];
        _productPriceLabel.font = FontMedium(20);
        _productPriceLabel.textColor = kColorWithHexString(@"#FF9500");
        _productPriceLabel.textAlignment = NSTextAlignmentLeft;
    }
    return _productPriceLabel;
}

- (UILabel *)productCountLabel {
    if (!_productCountLabel) {
        _productCountLabel = [UILabel new];
        _productCountLabel.font = FontRegular(14);
        _productCountLabel.textColor = kColorWithHexString(@"000000");
        _productCountLabel.textAlignment = NSTextAlignmentRight;
    }
    return _productCountLabel;
}

- (UIView *)lineView {
    if (!_lineView) {
        _lineView = [UIView new];
        _lineView.backgroundColor = kColorWithHexString(@"#EBEDF0");
    }
    return _lineView;
}

- (UILabel *)sendTypeLabel {
    if (!_sendTypeLabel) {
        _sendTypeLabel = [UILabel new];
        _sendTypeLabel.font = FontRegular(12);
        _sendTypeLabel.textColor = kColorWithHexString(@"#16191C");
        _sendTypeLabel.textAlignment = NSTextAlignmentLeft;
        _sendTypeLabel.text = @"配送方式:";
    }
    return _sendTypeLabel;
}

- (UILabel *)totalPriceLabel {
    if (!_totalPriceLabel) {
        _totalPriceLabel = [UILabel new];
        _totalPriceLabel.font = FontRegular(12);
        _totalPriceLabel.textColor = kColorWithHexString(@"#16191C");
        _totalPriceLabel.textAlignment = NSTextAlignmentLeft;
        _totalPriceLabel.text = @"商品总额:";
    }
    return _totalPriceLabel;
}

- (UILabel *)freightLabel {
    if (!_freightLabel) {
        _freightLabel = [UILabel new];
        _freightLabel.font = FontRegular(12);
        _freightLabel.textColor = kColorWithHexString(@"#16191C");
        _freightLabel.textAlignment = NSTextAlignmentLeft;
        _freightLabel.text = @"运费:";
    }
    return _freightLabel;
}


- (UILabel *)sendContentLabel {
    if (!_sendContentLabel) {
        _sendContentLabel = [UILabel new];
        _sendContentLabel.font = FontRegular(12);
        _sendContentLabel.textColor = kColorWithHexString(@"#919599");
        _sendContentLabel.textAlignment = NSTextAlignmentRight;
        _sendContentLabel.text = @"快递";
    }
    return _sendContentLabel;
}


- (UILabel *)totalPriceContentLabel {
    if (!_totalPriceContentLabel) {
        _totalPriceContentLabel = [UILabel new];
        _totalPriceContentLabel.font = FontRegular(12);
        _totalPriceContentLabel.textColor = kColorWithHexString(@"#919599");
        _totalPriceContentLabel.textAlignment = NSTextAlignmentRight;
        _totalPriceContentLabel.text = @"xx奇迹币";
    }
    return _totalPriceContentLabel;
}

- (UILabel *)freightContentLabel {
    if (!_freightContentLabel) {
        _freightContentLabel = [UILabel new];
        _freightContentLabel.font = FontRegular(12);
        _freightContentLabel.textColor = kColorWithHexString(@"#919599");
        _freightContentLabel.textAlignment = NSTextAlignmentRight;
        _freightContentLabel.text = @"¥0.00";
    }
    return _freightContentLabel;
}

 
- (UILabel *)finalPriceLabel {
    if (!_finalPriceLabel) {
        _finalPriceLabel = [UILabel new];
        _finalPriceLabel.font = FontRegular(12);
        _finalPriceLabel.textColor = kColorWithHexString(@"#16191C");
        _finalPriceLabel.textAlignment = NSTextAlignmentRight;
        _finalPriceLabel.text = @"实付金额：";
    }
    return _finalPriceLabel;
}



@end

