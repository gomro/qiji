//
//  QJLocationAddressCell.h
//  QJBox
//
//  Created by rui on 2022/9/5.
//

#import <UIKit/UIKit.h>
#import <AMapSearchKit/AMapSearchKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJLocationAddressCell : UITableViewCell

@property (nonatomic, strong) AMapPOI *model;

@end

NS_ASSUME_NONNULL_END
