//
//  QJMallConfirmFillAddressCell.h
//  QJBox
//
//  Created by wxy on 2022/8/24.
//

#import "QJBaseTableViewCell.h"
#import "QJMallAddressModel.h"
NS_ASSUME_NONNULL_BEGIN

/// 填充后的地址
@interface QJMallConfirmFillAddressCell : QJBaseTableViewCell
@property (nonatomic, strong) QJMallAddressModel *model;

@end

NS_ASSUME_NONNULL_END
