//
//  QJMalSelAddressTableViewCell.h
//  QJBox
//
//  Created by macm on 2022/9/5.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJMalSelAddressTableViewCell : UITableViewCell
@property (nonatomic, strong) UIView *topLineView;
@property (nonatomic, strong) UIView *leftView;
@property (nonatomic, strong) UIView *bottomLineView;
@property (nonatomic, strong) UILabel *addressLabel;

@end

NS_ASSUME_NONNULL_END
