//
//  QJMallAddressListCell.m
//  QJBox
//
//  Created by wxy on 2022/8/25.
//

#import "QJMallAddressListCell.h"
#import "QJMallAddressModel.h"

@interface QJMallAddressListCell ()

@property (nonatomic, strong) UILabel *nameLabel;

@property (nonatomic, strong) UILabel *phoneLabel;

@property (nonatomic, strong) UILabel *defaultLabel;

@property (nonatomic, strong) UILabel *detailAddressLabel;

@property (nonatomic, strong) UIButton *editBtn;
@end


@implementation QJMallAddressListCell


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
//        self.contentView.backgroundColor = RGB(244, 244, 244);
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [self.contentView addSubview:self.nameLabel];
        [self.contentView addSubview:self.phoneLabel];
        [self.contentView addSubview:self.defaultLabel];
        [self.contentView addSubview:self.detailAddressLabel];
        [self.contentView addSubview:self.editBtn];
        
        [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(24);
            make.top.equalTo(self.contentView).offset(12);
        }];
        
        [self.nameLabel setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
        
        [self.phoneLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.nameLabel.mas_right).offset(8);
            make.centerY.equalTo(self.nameLabel);
        }];
        
        [self.phoneLabel setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
        [self.defaultLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.nameLabel);
            make.width.equalTo(@(32*kWScale));
            make.height.equalTo(@(16*kWScale));
            make.left.equalTo(self.phoneLabel.mas_right).offset(8);
        }];
        
        [self.detailAddressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(24);
            make.top.equalTo(self.nameLabel.mas_bottom).offset(8);
            make.bottom.equalTo(self.contentView).offset(-12);
//                    make.height.equalTo(@(21*kWScale));
            make.right.equalTo(self.editBtn.mas_left).offset(-16);
        }];
        
        [self.editBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.height.equalTo(@(16*kWScale));
            make.right.equalTo(self.contentView).offset(-24);
            make.bottom.equalTo(self.contentView).offset(-22.5);
        }];
    }
    return self;
}


#pragma  mark ------ getter

- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [UILabel new];
        _nameLabel.font = FontSemibold(16);
        _nameLabel.textColor = kColorWithHexString(@"#16191C");
        _nameLabel.textAlignment = NSTextAlignmentLeft;
    }
    return _nameLabel;
}

- (UILabel *)phoneLabel {
    if (!_phoneLabel) {
        _phoneLabel = [UILabel new];
        _phoneLabel.font = FontRegular(14);
        _phoneLabel.textColor = kColorWithHexString(@"#16191C");
        _phoneLabel.textAlignment = NSTextAlignmentLeft;
    }
    return _phoneLabel;
}


- (UILabel *)defaultLabel {
    if (!_defaultLabel) {
        _defaultLabel = [UILabel new];
        _defaultLabel.font = FontRegular(12);
        _defaultLabel.textColor = kColorWithHexString(@"#FF9500");
        _defaultLabel.layer.cornerRadius = 2;
        _defaultLabel.layer.borderColor = kColorWithHexString(@"#FF9500").CGColor;
        _defaultLabel.layer.borderWidth = 1;
        _defaultLabel.text = @"默认";
        _defaultLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _defaultLabel;
}


-(UILabel *)detailAddressLabel {
    if (!_detailAddressLabel) {
        _detailAddressLabel = [UILabel new];
        _detailAddressLabel.font = FontRegular(14);
        _detailAddressLabel.textColor = kColorWithHexString(@"#000000");
        _detailAddressLabel.textAlignment = NSTextAlignmentLeft;
        _detailAddressLabel.numberOfLines = 0;
    }
    return _detailAddressLabel;
}

- (UIButton *)editBtn {
    if (!_editBtn) {
        _editBtn = [UIButton new];
        [_editBtn addTarget:self action:@selector(editAction:) forControlEvents:UIControlEventTouchUpInside];
        [_editBtn setImage:[UIImage imageNamed:@"edit_gray"] forState:UIControlStateNormal];
        [_editBtn setImage:[UIImage imageNamed:@"edit_gray"] forState:UIControlStateHighlighted];
        [_editBtn setEnlargeEdgeWithTop:7 right:7 bottom:7 left:7];
    }
    return _editBtn;
}

- (void)setModel:(QJMallAddressModel *)model {
    _model = model;
    self.nameLabel.text = model.linkman;
    self.phoneLabel.text = model.mobile;
    self.detailAddressLabel.text = [NSString stringWithFormat:@"%@%@%@%@%@",model.province?:@"",model.city?:@"",model.area?:@"",model.street?:@"",model.detail?:@""];
    if (model.isDefault) {
        self.defaultLabel.hidden = NO;
    }else{
        self.defaultLabel.hidden = YES;
    }
}

#pragma mark ---- editAction
- (void)editAction:(UIButton *)sender {
    if (self.editBlock) {
        self.editBlock();
    }
}

@end
