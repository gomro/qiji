//
//  QJMalSelAddressTableViewCell.m
//  QJBox
//
//  Created by macm on 2022/9/5.
//

#import "QJMalSelAddressTableViewCell.h"

@interface QJMalSelAddressTableViewCell ()
@property (nonatomic, strong) UIImageView *rightImageView;
@end

@implementation QJMalSelAddressTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor clearColor];
        [self setupUI];
    }
    return self;
}

- (void)setupUI {
    [self.contentView addSubview:self.leftView];
    [self.leftView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(12);
        make.left.mas_equalTo(32);
        make.width.height.mas_equalTo(8*kWScale);
    }];
    
    [self.contentView addSubview:self.topLineView];
    [self.topLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.bottom.mas_equalTo(self.leftView.mas_top);
        make.centerX.mas_equalTo(self.leftView);
        make.width.mas_equalTo(1);
    }];
    
    [self.contentView addSubview:self.bottomLineView];
    [self.bottomLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.leftView.mas_bottom);
        make.centerX.mas_equalTo(self.leftView);
        make.bottom.mas_equalTo(0);
        make.width.mas_equalTo(1);
    }];
    
    [self.contentView addSubview:self.addressLabel];
    [self.addressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.leftView.mas_right).mas_offset(8);
        make.centerY.mas_equalTo(self.leftView);
    }];
    
    [self.contentView addSubview:self.rightImageView];
    [self.rightImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-40);
        make.centerY.mas_equalTo(self.leftView);
    }];
   
}

- (UIView *)leftView {
    if (!_leftView) {
        _leftView = [[UIView alloc] init];
        _leftView.backgroundColor = kColorOrange;
        _leftView.layer.cornerRadius = 4;
        _leftView.layer.borderColor = kColorOrange.CGColor;
        _leftView.layer.borderWidth = 1;
    }
    return _leftView;
}

- (UIView *)topLineView {
    if (!_topLineView) {
        _topLineView = [[UIView alloc] init];
        _topLineView.backgroundColor = kColorOrange;
    }
    return _topLineView;
}

- (UIView *)bottomLineView {
    if (!_bottomLineView) {
        _bottomLineView = [[UIView alloc] init];
        _bottomLineView.backgroundColor = kColorOrange;
    }
    return _bottomLineView;
}

- (UILabel *)addressLabel {
    if (!_addressLabel) {
        _addressLabel = [[UILabel alloc] init];
        _addressLabel.textColor = UIColorFromRGB(0x474849);
        [_addressLabel setFont:FontRegular(14)];
    }
    return _addressLabel;
}

- (UIImageView *)rightImageView {
    if (!_rightImageView) {
        _rightImageView = [[UIImageView alloc] init];
        _rightImageView.image = [UIImage imageNamed:@"mine_AI_more_right"];
    }
    return _rightImageView;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
