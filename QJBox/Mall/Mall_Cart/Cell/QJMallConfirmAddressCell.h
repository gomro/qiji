//
//  QJMallConfirmAddressCell.h
//  QJBox
//
//  Created by wxy on 2022/8/24.
//

#import "QJBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

/// 占位地址
@interface QJMallConfirmAddressCell : QJBaseTableViewCell

@end


NS_ASSUME_NONNULL_END
