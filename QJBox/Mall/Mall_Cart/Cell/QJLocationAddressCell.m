//
//  QJLocationAddressCell.m
//  QJBox
//
//  Created by rui on 2022/9/5.
//

#import "QJLocationAddressCell.h"

@interface QJLocationAddressCell ()

@property (nonatomic, strong) UILabel *nameLabel;

@property (nonatomic, strong) UILabel *addressLabel;

@end

@implementation QJLocationAddressCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.contentView.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:self.nameLabel];
        [self.contentView addSubview:self.addressLabel];
        
        [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.equalTo(self.contentView).offset(8);
            make.right.equalTo(self.contentView).offset(-8);
            make.height.equalTo(@(20*kWScale));
        }];
        
        [self.addressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(8);
            make.top.equalTo(self.nameLabel.mas_bottom);
            make.right.equalTo(self.contentView).offset(-8);
            make.height.equalTo(@(20*kWScale));
        }];
    }
    return self;
}


#pragma  mark ------ getter
- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [UILabel new];
        _nameLabel.font = kFont(14);
        _nameLabel.textColor = kColorWithHexString(@"#FF9500");
        _nameLabel.textAlignment = NSTextAlignmentLeft;
    }
    return _nameLabel;
}

- (UILabel *)addressLabel {
    if (!_addressLabel) {
        _addressLabel = [UILabel new];
        _addressLabel.font = kFont(12);
        _addressLabel.textColor = kColorWithHexString(@"#262626");
        _addressLabel.textAlignment = NSTextAlignmentLeft;
    }
    return _addressLabel;
}

- (void)setModel:(AMapPOI *)model{
    _model = model;
    self.nameLabel.text = model.name;
    self.addressLabel.text = [NSString stringWithFormat:@"%@%@%@%@",model.province?:@"",model.city?:@"",model.district?:@"",model.address?:@""];
}

@end
