//
//  QJMallConfirmAddressCell.m
//  QJBox
//
//  Created by wxy on 2022/8/24.
//

#import "QJMallConfirmAddressCell.h"

@interface QJMallConfirmAddressCell ()

@property (nonatomic, strong) UIView *backView;

@property (nonatomic, strong) UIImageView *addImageView;

@property (nonatomic, strong) UILabel *contentLabel;

@property (nonatomic, strong) UIImageView *arrowImageView;


@end


@implementation QJMallConfirmAddressCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
      
        self.contentView.backgroundColor = RGB(244, 244, 244);
        
        [self.contentView addSubview:self.backView];
        
        [self.contentView addSubview:self.addImageView];
        [self.contentView addSubview:self.contentLabel];
        [self.contentView addSubview:self.arrowImageView];
        
        [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.contentView);
        }];
        
        [self.addImageView mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.equalTo(self.contentView).offset(8);
            make.top.equalTo(self.contentView.mas_top).offset(14*kWScale);
            make.bottom.equalTo(self.contentView.mas_bottom).offset(-14*kWScale);

            make.width.height.equalTo(@(24*kWScale));
        }];
        
        [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.equalTo(self.addImageView.mas_right).offset(8);
                    make.centerY.equalTo(self.contentView);
                    make.right.equalTo(self.arrowImageView.mas_left).offset(-16);
        }];
        
        [self.arrowImageView mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.right.equalTo(self.contentView).offset(-8);
                    make.width.height.equalTo(@(16*kWScale));
                    make.centerY.equalTo(self.contentView);
        }];
    }
    return self;
}


#pragma mark  ----- getter setter

- (UIView *)backView {
    if(!_backView) {
        _backView = [UIView new];
        _backView.backgroundColor = [UIColor whiteColor];
        _backView.layer.cornerRadius = 8;
        _backView.layer.masksToBounds = YES;
    }
    return _backView;
}

-(UIImageView *)addImageView {
    if (!_addImageView) {
        _addImageView = [UIImageView new];
        _addImageView.image = [UIImage imageNamed:@"addIcon"];
    }
    return _addImageView;
}

- (UILabel *)contentLabel {
    if (!_contentLabel) {
        _contentLabel = [UILabel new];
        _contentLabel.font = FontRegular(14);
        _contentLabel.textColor = [UIColor blackColor];
        _contentLabel.text = @"请添加收货地址";
    }
    return _contentLabel;
}

- (UIImageView *)arrowImageView {
    if (!_arrowImageView) {
        _arrowImageView = [UIImageView new];
        _arrowImageView.image = [UIImage imageNamed:@"mall_arrow_right16"];
    }
    return _arrowImageView;
}



@end
