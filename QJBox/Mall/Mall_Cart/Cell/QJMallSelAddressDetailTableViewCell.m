//
//  QJMallSelAddressDetailTableViewCell.m
//  QJBox
//
//  Created by macm on 2022/9/5.
//

#import "QJMallSelAddressDetailTableViewCell.h"

@implementation QJMallSelAddressDetailTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.contentView.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:self.leftLabel];
        [self.contentView addSubview:self.addressLabel];
        
        [self.leftLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(16);
            make.centerY.equalTo(self.contentView);
        }];
        
        [self.addressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(41);
            make.centerY.equalTo(self.contentView);
        }];
    }
    return self;
}


#pragma  mark ------ getter
- (UILabel *)leftLabel {
    if (!_leftLabel) {
        _leftLabel = [UILabel new];
        _leftLabel.font = kFont(12);
        _leftLabel.textColor = kColorWithHexString(@"#474849");
    }
    return _leftLabel;
}

- (UILabel *)addressLabel {
    if (!_addressLabel) {
        _addressLabel = [UILabel new];
        _addressLabel.font = kFont(12);
        _addressLabel.textColor = kColorWithHexString(@"#474849");
    }
    return _addressLabel;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
