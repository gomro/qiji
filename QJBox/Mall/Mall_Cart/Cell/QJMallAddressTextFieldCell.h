//
//  QJMallAddressTextFieldCell.h
//  QJBox
//
//  Created by wxy on 2022/8/25.
//

#import <UIKit/UIKit.h>
#import "QJMallBaseTextField.h"
NS_ASSUME_NONNULL_BEGIN

@interface QJMallAddressTextFieldCell : UITableViewCell

@property (nonatomic, assign) NSInteger maxCount;

@property (nonatomic, strong) UILabel *nameLabel;

@property (nonatomic, strong) UITextField *textField;

@property (nonatomic, strong) UIButton *btn;

@property (nonatomic, copy) void(^localAddress)(void);

@property (nonatomic, copy) void(^textInputBlock)(void);

@end

NS_ASSUME_NONNULL_END
