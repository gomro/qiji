//
//  QJMallConfirmProductCell.h
//  QJBox
//
//  Created by wxy on 2022/8/24.
//

#import "QJBaseTableViewCell.h"
#import "QJMallOrderDetailModel.h"

NS_ASSUME_NONNULL_BEGIN

/// 商品信息
@interface QJMallConfirmProductCell : QJBaseTableViewCell

@property (nonatomic, strong) QJMallOrderDetailModel *model;


@end


NS_ASSUME_NONNULL_END
