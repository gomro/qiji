//
//  QJMallAddressTextFieldCell.m
//  QJBox
//
//  Created by wxy on 2022/8/25.
//

#import "QJMallAddressTextFieldCell.h"


@interface QJMallAddressTextFieldCell ()<UITextFieldDelegate>


@end


@implementation QJMallAddressTextFieldCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.backgroundColor = [UIColor whiteColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self.contentView addSubview:self.nameLabel];
        [self.contentView addSubview:self.textField];
        [self.contentView addSubview:self.btn];
        [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(16);
            make.centerY.equalTo(self.textField);
            make.width.equalTo(@(70*kWScale));
        }] ;
        
        [self.textField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.nameLabel.mas_right).offset(16);
            make.top.equalTo(self.contentView);
            make.height.equalTo(@(37*kWScale));
            make.right.equalTo(self.contentView).offset(-16);
        }];
        
        [self.btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.height.equalTo(@(37*kWScale));
            make.centerY.equalTo(self.textField);
            make.right.equalTo(self.contentView).offset(-12);
        }];
        
    }
    return self;
}



#pragma mark ------ getter

- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [UILabel new];
        _nameLabel.font = FontSemibold(16);
        _nameLabel.textColor = kColorWithHexString(@"#000000");
        _nameLabel.textAlignment = NSTextAlignmentLeft;
    }
    return _nameLabel;
}

- (UITextField *)textField {
    if (!_textField) {
        _textField = [UITextField new];
        _textField.textColor = kColorWithHexString(@"#16191C");
        _textField.font = FontRegular(14);
        _textField.layer.cornerRadius = 8;
        _textField.leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 37)];
        _textField.leftViewMode = UITextFieldViewModeAlways;
//        _textField.rightView = self.btn;
//        _textField.rightViewMode = UITextFieldViewModeAlways;
        _textField.layer.masksToBounds = YES;
        _textField.backgroundColor = kColorWithHexString(@"#F5F5F5");
        _textField.delegate = self;
        [_textField addTarget:self action:@selector(textFeildChange:) forControlEvents:UIControlEventEditingChanged];

    }
    return _textField;
}




- (UIButton *)btn {
    if (!_btn) {
        _btn = [UIButton new];
        _btn.frame = CGRectMake(0, 0, 37*kWScale, 37*kWScale);
        [_btn setImage:[UIImage imageNamed:@"mall_cart_location_16"] forState:UIControlStateNormal];
        [_btn setEnlargeEdgeWithTop:7 right:7 bottom:7 left:7];
        [_btn addTarget:self action:@selector(btnAction) forControlEvents:UIControlEventTouchUpInside];
        _btn.hidden = YES;
        _btn.backgroundColor = [UIColor clearColor];
    }
    return _btn;
}

//
- (void)btnAction {
    if (self.localAddress) {
        self.localAddress();
    }
}

- (void)textFeildChange:(UITextField *)textFeild {
    if (textFeild.markedTextRange == nil) {
        NSString *toBeString = textFeild.text;
        if (toBeString.length > _maxCount) {
            textFeild.text = [toBeString substringToIndex:_maxCount];
        }
        if (self.textInputBlock) {
            self.textInputBlock();
        }
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    NSString * text = textField.text;
    if (text.length > _maxCount) {
        textField.text = [text substringToIndex:_maxCount];
    }
    if (self.textInputBlock) {
        self.textInputBlock();
    }
    return YES;
}

@end
