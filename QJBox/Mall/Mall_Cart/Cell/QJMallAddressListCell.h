//
//  QJMallAddressListCell.h
//  QJBox
//
//  Created by wxy on 2022/8/25.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class QJMallAddressModel;
/// 地址列表cell
@interface QJMallAddressListCell : UITableViewCell

@property (nonatomic, copy) void(^editBlock)(void);

@property (nonatomic, strong) QJMallAddressModel *model;
@end

NS_ASSUME_NONNULL_END
