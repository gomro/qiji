//
//  QJMallSelAddressDetailTableViewCell.h
//  QJBox
//
//  Created by macm on 2022/9/5.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJMallSelAddressDetailTableViewCell : UITableViewCell
@property (nonatomic, strong) UILabel *leftLabel;
@property (nonatomic, strong) UILabel *addressLabel;
@end

NS_ASSUME_NONNULL_END
