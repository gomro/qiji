//
//  QJMallOrderLogisticsDetailTableViewCell.h
//  QJBox
//
//  Created by macm on 2022/8/25.
//

#import <UIKit/UIKit.h>
#import "QJMallOrderModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface QJMallOrderLogisticsDetailTableViewCell : UITableViewCell
@property (nonatomic, strong) QJMallOrderTraceModel *model;
@property (nonatomic, strong) UIView *topLineView;
@property (nonatomic, strong) UIView *leftView;
@property (nonatomic, strong) UIView *bottomLineView;

@property (nonatomic, strong) UILabel *statusLabel;
@property (nonatomic, strong) UILabel *timeLabel;
@property (nonatomic, strong) UILabel *detailLabel;
@end

NS_ASSUME_NONNULL_END
