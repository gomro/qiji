//
//  QJMallOrderDetailPriceTableViewCell.m
//  QJBox
//
//  Created by macm on 2022/8/24.
//

#import "QJMallOrderDetailPriceTableViewCell.h"

@interface QJMallOrderDetailPriceTableViewCell ()
@property (nonatomic, strong) UIView *bgView;

@end

@implementation QJMallOrderDetailPriceTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor clearColor];
        [self setupUI];
    }
    return self;
}

- (void)setupUI {
    [self.contentView addSubview:self.bgView];
    [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            
        make.top.mas_equalTo(0);
        make.left.mas_equalTo(16);
        make.right.mas_equalTo(-16);
        make.bottom.mas_equalTo(-24);
    }];
    
    [self.bgView addSubview:self.priceLabel];
    [self.priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(5);
        make.right.mas_equalTo(-16);
        make.bottom.mas_equalTo(-20);
    }];
    
}

- (void)layoutSubviews{
    [super layoutSubviews];
    [self.bgView layoutIfNeeded];
    [self.bgView showCorner:8 rectCorner:UIRectCornerBottomLeft | UIRectCornerBottomRight];
}

- (UIView *)bgView {
    if (!_bgView) {
        _bgView = [[UIView alloc] init];
        _bgView.backgroundColor = [UIColor whiteColor];
    }
    return _bgView;
}

- (UILabel *)priceLabel {
    if (!_priceLabel) {
        _priceLabel = [[UILabel alloc] init];
        _priceLabel.text = @"实付金额: 0奇迹币";
        _priceLabel.textColor = UIColorFromRGB(0x16191c);
        [_priceLabel setFont:FontRegular(12)];
        _priceLabel.textAlignment = NSTextAlignmentRight;
    }
    return _priceLabel;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
