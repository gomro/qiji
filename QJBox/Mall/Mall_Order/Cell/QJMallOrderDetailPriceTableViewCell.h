//
//  QJMallOrderDetailPriceTableViewCell.h
//  QJBox
//
//  Created by macm on 2022/8/24.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJMallOrderDetailPriceTableViewCell : UITableViewCell
@property (nonatomic, strong) UILabel *priceLabel;

@end

NS_ASSUME_NONNULL_END
