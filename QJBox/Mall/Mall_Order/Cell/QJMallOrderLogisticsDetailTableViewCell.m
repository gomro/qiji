//
//  QJMallOrderLogisticsDetailTableViewCell.m
//  QJBox
//
//  Created by macm on 2022/8/25.
//

#import "QJMallOrderLogisticsDetailTableViewCell.h"

@interface QJMallOrderLogisticsDetailTableViewCell ()

@end

@implementation QJMallOrderLogisticsDetailTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor clearColor];
        [self setupUI];
    }
    return self;
}

- (void)setupUI {
    [self.contentView addSubview:self.leftView];
    [self.leftView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(12);
        make.left.mas_equalTo(32);
        make.width.height.mas_equalTo(8*kWScale);
    }];
    
    [self.contentView addSubview:self.topLineView];
    [self.topLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.bottom.mas_equalTo(self.leftView.mas_top);
        make.centerX.mas_equalTo(self.leftView);
        make.width.mas_equalTo(1);
    }];
    
    [self.contentView addSubview:self.bottomLineView];
    [self.bottomLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.leftView.mas_bottom);
        make.centerX.mas_equalTo(self.leftView);
        make.bottom.mas_equalTo(0);
        make.width.mas_equalTo(1);
    }];
    
    [self.contentView addSubview:self.statusLabel];
    [self.statusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.leftView.mas_right).mas_offset(8);
        make.centerY.mas_equalTo(self.leftView);
    }];
    
    [self.contentView addSubview:self.timeLabel];
    [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.statusLabel.mas_right).mas_offset(8);
        make.centerY.mas_equalTo(self.leftView);
    }];
    
    [self.contentView addSubview:self.detailLabel];
    [self.detailLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.statusLabel.mas_bottom).mas_offset(8);
        make.left.mas_equalTo(self.statusLabel);
        make.bottom.mas_equalTo(-8);
        make.right.mas_equalTo(-32);
    }];
   
}

- (void)setModel:(QJMallOrderTraceModel *)model {
    _model = model;
    if (_model.time.length > 10) {
        self.timeLabel.text = [_model.time substringToIndex:10];
    } else {
        self.timeLabel.text = _model.time;
    }
    
    self.statusLabel.text = [self getTraceString:_model.state.integerValue];
    self.detailLabel.text = _model.desc;
}

- (UIView *)leftView {
    if (!_leftView) {
        _leftView = [[UIView alloc] init];
        _leftView.backgroundColor = kColorOrange;
        _leftView.layer.cornerRadius = 4;
    }
    return _leftView;
}

- (UIView *)topLineView {
    if (!_topLineView) {
        _topLineView = [[UIView alloc] init];
        _topLineView.backgroundColor = UIColorFromRGB(0xc8cacc);
    }
    return _topLineView;
}

- (UIView *)bottomLineView {
    if (!_bottomLineView) {
        _bottomLineView = [[UIView alloc] init];
        _bottomLineView.backgroundColor = UIColorFromRGB(0xc8cacc);
    }
    return _bottomLineView;
}

- (UILabel *)timeLabel {
    if (!_timeLabel) {
        _timeLabel = [[UILabel alloc] init];
        _timeLabel.text = @"20:34";
        _timeLabel.textColor = kColorOrange;
        [_timeLabel setFont:FontMedium(14)];
    }
    return _timeLabel;
}

- (UILabel *)statusLabel {
    if (!_statusLabel) {
        _statusLabel = [[UILabel alloc] init];
        _statusLabel.text = @"待发货";
        _statusLabel.textColor = kColorOrange;
        [_statusLabel setFont:FontSemibold(16)];
    }
    return _statusLabel;
}

- (UILabel *)detailLabel {
    if (!_detailLabel) {
        _detailLabel = [[UILabel alloc] init];
        _detailLabel.text = @"包裹正在等待揽收";
        _detailLabel.textColor = UIColorFromRGB(0x000000);
        [_detailLabel setFont:FontRegular(14)];
        _detailLabel.numberOfLines = 0;
    }
    return _detailLabel;
}

- (NSString *)getTraceString:(NSInteger)status {
    switch (status) {
        case 0:
            return @"已下单";
            break;
        case 1:
            return @"已支付";
            break;
        case 2:
            return @"已发货";
            break;
        case 3:
            return @"已揽件";
            break;
        case 4:
            return @"运输中";
            break;
        case 5:
            return @"派送中";
            break;
        case 6:
            return @"已签收";
            break;
        
        default:
            return @"暂无";
            break;
    }
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
