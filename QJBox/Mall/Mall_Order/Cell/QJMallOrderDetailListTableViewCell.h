//
//  QJMallOrderDetailListTableViewCell.h
//  QJBox
//
//  Created by macm on 2022/8/24.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJMallOrderDetailListTableViewCell : UITableViewCell
@property (nonatomic, strong) UILabel *leftLabel;
@property (nonatomic, strong) UILabel *rightLabel;
@end

NS_ASSUME_NONNULL_END
