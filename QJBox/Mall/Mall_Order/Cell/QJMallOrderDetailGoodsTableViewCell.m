//
//  QJMallOrderDetailGoodsTableViewCell.m
//  QJBox
//
//  Created by macm on 2022/8/24.
//

#import "QJMallOrderDetailGoodsTableViewCell.h"

@interface QJMallOrderDetailGoodsTableViewCell ()
@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) UIImageView *itemImageView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *skuLabel;
@property (nonatomic, strong) UILabel *numLabel;
@property (nonatomic, strong) UILabel *priceLabel;
@property (nonatomic, strong) UIView *lineView;
@end

@implementation QJMallOrderDetailGoodsTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor clearColor];
        [self setupUI];
    }
    return self;
}

- (void)setupUI {
    [self.contentView addSubview:self.bgView];
    [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.mas_equalTo(16);
        make.right.mas_equalTo(-16);
        make.bottom.mas_equalTo(0);
    }];

    [self.bgView addSubview:self.itemImageView];
    [self.itemImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(16);
        make.left.mas_equalTo(16);
        make.width.height.mas_equalTo(64*kWScale);
    }];
    
    [self.bgView addSubview:self.nameLabel];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.itemImageView.mas_top).mas_offset(0);
        make.left.mas_equalTo(self.itemImageView.mas_right).mas_offset(16);
        make.right.mas_equalTo(-44);
    }];
    
    [self.bgView addSubview:self.skuLabel];
    [self.skuLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.nameLabel.mas_bottom).mas_offset(2);
        make.left.mas_equalTo(self.itemImageView.mas_right).mas_offset(16);
        make.right.mas_equalTo(-16);
    }];

    [self.bgView addSubview:self.numLabel];
    [self.numLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-20);
        make.centerY.mas_equalTo(self.nameLabel);
    }];
    
    [self.bgView addSubview:self.priceLabel];
    [self.priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-20);
        make.left.mas_equalTo(self.itemImageView.mas_right).mas_offset(16);
        make.top.mas_equalTo(self.skuLabel.mas_bottom).mas_offset(1);
    }];
    
    [self.bgView addSubview:self.lineView];
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo (self.priceLabel.mas_bottom).mas_offset(16);
        make.left.mas_equalTo(16);
        make.right.mas_equalTo(-16);
        make.height.mas_equalTo(1);
        make.bottom.mas_equalTo(-16);
    }];

}

- (void)layoutSubviews{
    [super layoutSubviews];
    [self.bgView layoutIfNeeded];
    [self.bgView showCorner:8 rectCorner:UIRectCornerTopLeft | UIRectCornerTopRight];
}

- (void)setModel:(QJMallOrderItemModel *)model {
    _model = model;
    [self.itemImageView setImageWithURL:[NSURL URLWithString:[_model.picture checkImageUrlString]] placeholder:[UIImage imageNamed:@"mine_head"]];
    self.nameLabel.text = _model.name;
    NSString *price = [NSString stringWithFormat:@"%@ 奇迹币",_model.qjcoinPartPrice];
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:price];
    [attrStr addAttribute:NSFontAttributeName
                    value:FontRegular(10)
                    range:NSMakeRange(price.length-3, 3)];
    self.priceLabel.attributedText = attrStr;
    self.numLabel.text = [NSString stringWithFormat:@"x%@",_model.quantity];

    NSMutableArray *propertieArray = [NSMutableArray array];
    for (QJMallOrderItemPropertieModel *propertie in _model.properties) {
        [propertieArray addObject:propertie.value];
    }
    self.skuLabel.text = [propertieArray componentsJoinedByString:@" "];
}

- (UIView *)bgView {
    if (!_bgView) {
        _bgView = [[UIView alloc] init];
        _bgView.backgroundColor = [UIColor whiteColor];
    }
    return _bgView;
}

- (UIImageView *)itemImageView {
    if (!_itemImageView) {
        _itemImageView = [[UIImageView alloc] init];
        _itemImageView.layer.cornerRadius = 4;
        _itemImageView.layer.masksToBounds = YES;
        _itemImageView.image = [UIImage imageNamed:@"mine_head"];
        _itemImageView.contentMode = UIViewContentModeScaleAspectFill;

    }
    return _itemImageView;
}

- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc] init];
        _nameLabel.text = @" ";
        _nameLabel.textColor = UIColorFromRGB(0x000000);
        [_nameLabel setFont:FontMedium(14)];
    }
    return _nameLabel;
}

- (UILabel *)skuLabel {
    if (!_skuLabel) {
        _skuLabel = [[UILabel alloc] init];
        _skuLabel.text = @"比克大魔王 比克大魔王 比克大魔王 比克大魔王 比克大魔王v 比克大魔王 比克大魔王 比克大魔王";
        _skuLabel.textColor = UIColorFromRGB(0x919599);
        [_skuLabel setFont:FontRegular(14)];
        _skuLabel.numberOfLines = 0;
    }
    return _skuLabel;
}

- (UILabel *)numLabel {
    if (!_numLabel) {
        _numLabel = [[UILabel alloc] init];
        _numLabel.text = @"x1";
        _numLabel.textColor = UIColorFromRGB(0x000000);
        [_numLabel setFont:FontRegular(14)];
    }
    return _numLabel;
}

- (UILabel *)priceLabel {
    if (!_priceLabel) {
        _priceLabel = [[UILabel alloc] init];
        _priceLabel.textColor = kColorOrange;
        [_priceLabel setFont:FontMedium(18)];
    }
    return _priceLabel;
}

- (UIView *)lineView {
    if (!_lineView) {
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor = UIColorFromRGB(0xe8edf0);
    }
    return _lineView;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
