//
//  QJMallOrderDetailGoodsTableViewCell.h
//  QJBox
//
//  Created by macm on 2022/8/24.
//

#import <UIKit/UIKit.h>
#import "QJMallOrderModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface QJMallOrderDetailGoodsTableViewCell : UITableViewCell
@property (nonatomic, strong) QJMallOrderItemModel *model;
@end

NS_ASSUME_NONNULL_END
