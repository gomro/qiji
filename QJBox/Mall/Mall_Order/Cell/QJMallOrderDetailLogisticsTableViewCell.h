//
//  QJMallOrderDetailLogisticsTableViewCell.h
//  QJBox
//
//  Created by macm on 2022/8/24.
//

#import <UIKit/UIKit.h>
#import "QJMallOrderDetailModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface QJMallOrderDetailLogisticsTableViewCell : UITableViewCell
@property (nonatomic, strong) QJMallOrderDetailModel *model;
@end

NS_ASSUME_NONNULL_END
