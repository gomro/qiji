//
//  QJMallOrderListTableViewCell.m
//  QJBox
//
//  Created by macm on 2022/8/24.
//

#import "QJMallOrderListTableViewCell.h"

@interface QJMallOrderListTableViewCell ()
@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) UIImageView *itemImageView;
@property (nonatomic, strong) UILabel *timeLabel;
@property (nonatomic, strong) UILabel *statusLabel;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *skuLabel;
@property (nonatomic, strong) UILabel *numLabel;
@property (nonatomic, strong) UILabel *priceLabel;

@property (nonatomic, strong) UIView *logisticsBgView;
@property (nonatomic, strong) UILabel *logisticsStatusLabel;
@property (nonatomic, strong) UILabel *logisticsDetailLabel;

@property (nonatomic, strong) UIButton *rightButton;
@property (nonatomic, strong) UIButton *leftButton;
@property (strong, nonatomic) MASConstraint *bottomConstraint;

@end

@implementation QJMallOrderListTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor clearColor];
        [self setupUI];
    }
    return self;
}

- (void)setupUI {
    [self.contentView addSubview:self.bgView];
    [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            
        make.top.mas_equalTo(8);
        make.left.mas_equalTo(16);
        make.right.mas_equalTo(-16);
        make.bottom.mas_equalTo(0);
    }];
    
    [self.bgView addSubview:self.timeLabel];
    [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.mas_equalTo(16);
        make.height.mas_equalTo(14*kWScale);
    }];
    
    [self.bgView addSubview:self.statusLabel];
    [self.statusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-16);
        make.centerY.mas_equalTo(self.timeLabel);
    }];
    
    [self.bgView addSubview:self.itemImageView];
    [self.itemImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.timeLabel.mas_bottom).mas_offset(9);
        make.left.mas_equalTo(16);
        make.width.height.mas_equalTo(64*kWScale);
    }];
    
    [self.bgView addSubview:self.nameLabel];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.itemImageView.mas_top).mas_offset(0);
        make.left.mas_equalTo(self.itemImageView.mas_right).mas_offset(16);
        make.right.mas_equalTo(-44);
    }];
    
    [self.bgView addSubview:self.skuLabel];
    [self.skuLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.nameLabel.mas_bottom).mas_offset(2);
        make.left.mas_equalTo(self.itemImageView.mas_right).mas_offset(16);
        make.right.mas_equalTo(-16);
    }];

    [self.bgView addSubview:self.numLabel];
    [self.numLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-20);
        make.centerY.mas_equalTo(self.nameLabel);
    }];
    
    [self.bgView addSubview:self.priceLabel];
    [self.priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-20);
        make.left.mas_equalTo(self.itemImageView.mas_right).mas_offset(16);
        make.top.mas_equalTo(self.skuLabel.mas_bottom).mas_offset(1);
        self.bottomConstraint = make.bottom.mas_equalTo(-19);
    }];
    
    [self.bgView addSubview:self.logisticsBgView];
    [self.logisticsBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.itemImageView.mas_bottom).mas_offset(8);
        make.left.mas_equalTo(16);
        make.right.mas_equalTo(-16);
        make.height.mas_equalTo(30*kWScale);

    }];
    
    [self.logisticsBgView addSubview:self.logisticsStatusLabel];
    [self.logisticsStatusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(8);
        make.width.mas_equalTo(40*kWScale);
        make.centerY.mas_equalTo(self.logisticsBgView);

    }];
    
    [self.logisticsBgView addSubview:self.logisticsDetailLabel];
    [self.logisticsDetailLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.logisticsStatusLabel.mas_right).mas_equalTo(4);
        make.right.mas_equalTo(-8);
        make.centerY.mas_equalTo(self.logisticsBgView);

    }];
    
    [self.bgView addSubview:self.moreButton];
    [self.moreButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(16);
        make.bottom.mas_equalTo(-23);
        make.width.mas_equalTo(30*kWScale);
        make.height.mas_equalTo(15*kWScale);
    }];
    
    [self.bgView addSubview:self.rightButton];
    [self.rightButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(-16);
        make.right.mas_equalTo(-16);
        make.width.mas_equalTo(78*kWScale);
        make.height.mas_equalTo(28*kWScale);
    }];
    
    [self.bgView addSubview:self.leftButton];
    [self.leftButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(-16);
        make.right.mas_equalTo(-110);
        make.width.mas_equalTo(78*kWScale);
        make.height.mas_equalTo(28*kWScale);
    }];
    
}

- (void)setModel:(QJMallOrderModel *)model {
    _model = model;
    NSInteger bottomCon = -19;
    
    if (_model.createTime.length > 10) {
        self.timeLabel.text = [_model.createTime substringToIndex:10];
    } else {
        self.timeLabel.text = _model.createTime;
    }
    
    self.statusLabel.text = [self getStatusString:_model.status.integerValue];
    if (_model.status.integerValue == 4) {
        self.statusLabel.textColor = UIColorFromRGB(0x34c759);
    } else {
        self.statusLabel.textColor = kColorOrange;
    }
    
    QJMallOrderItemModel *item = _model.items.firstObject;
    [self.itemImageView setImageWithURL:[NSURL URLWithString:[item.picture checkImageUrlString]] placeholder:[UIImage imageNamed:@"mine_head"]];
    self.nameLabel.text = item.name;
    NSString *price = [NSString stringWithFormat:@"%@ 奇迹币",item.qjcoinPartPrice];
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:price];
    [attrStr addAttribute:NSFontAttributeName
                    value:FontRegular(10)
                    range:NSMakeRange(price.length-3, 3)];
    self.priceLabel.attributedText = attrStr;
    self.numLabel.text = [NSString stringWithFormat:@"x%@",item.quantity];
    
    NSMutableArray *propertieArray = [NSMutableArray array];
    for (QJMallOrderItemPropertieModel *propertie in item.properties) {
        [propertieArray addObject:propertie.value];
    }
    self.skuLabel.text = [propertieArray componentsJoinedByString:@" "];
    
    // 设置按钮
//    if (_model.buttons.count == 1) {
//        [self.leftButton setHidden:YES];
//        [self.rightButton setHidden:NO];
//        QJMallOrderButtonModel *rightButton = _model.buttons.firstObject;
//        [self.rightButton setTitle:[self getButtonString:rightButton.button.integerValue] forState:UIControlStateNormal];
//    } else if (_model.buttons.count == 2) {
//        [self.leftButton setHidden:NO];
//        [self.rightButton setHidden:NO];
//        QJMallOrderButtonModel *leftButton = _model.buttons.firstObject;
//        [self.leftButton setTitle:[self getButtonString:leftButton.button.integerValue] forState:UIControlStateNormal];
//        QJMallOrderButtonModel *rightButton = _model.buttons.lastObject;
//        [self.rightButton setTitle:[self getButtonString:rightButton.button.integerValue] forState:UIControlStateNormal];
//    } else {
//        [self.leftButton setHidden:YES];
//        [self.rightButton setHidden:YES];
//    }
    
    if (_model.traces.count > 0 && ![_model.status isEqualToString:@"1"]) {
        [self.logisticsBgView setHidden:NO];
        QJMallOrderTraceModel *trace = _model.traces.firstObject;
        self.logisticsStatusLabel.text = [self getTraceString:trace.state.integerValue];
        self.logisticsDetailLabel.text = trace.desc;
        bottomCon -= 38;
    } else {
        [self.logisticsBgView setHidden:YES];
    }
    
//    if (_model.status.integerValue > 3) {
//        bottomCon -= 40;
//        [self.moreButton setHidden:NO];
//    } else {
//        if (_model.buttons.count > 0) {
//            bottomCon -= 40;
//        }
//        [self.moreButton setHidden:YES];
//    }
    
    // 待发货
    if (_model.status.integerValue == 1) {
        [self.moreButton setHidden:YES];
        [self.leftButton setHidden:YES];
        [self.rightButton setHidden:YES];
        
    // 待收货
    } else if (_model.status.integerValue == 2) {
        bottomCon -= 40;
        [self.moreButton setHidden:YES];
        if (_model.productType.integerValue == 1 && _model.traces.count > 0) {
            [self.leftButton setHidden:NO];
            [self.rightButton setHidden:NO];
            [self.leftButton setTitle:@"查看物流" forState:UIControlStateNormal];
            [self.rightButton setTitle:@"确认收货" forState:UIControlStateNormal];
        } else {
            [self.leftButton setHidden:YES];
            [self.rightButton setHidden:NO];
            [self.rightButton setTitle:@"查看卡券" forState:UIControlStateNormal];
        }
        
    // 待评价
    } else if (_model.status.integerValue == 3) {
        bottomCon -= 40;
        [self.moreButton setHidden:YES];
        if (_model.productType.integerValue == 1) {
            [self.leftButton setHidden:YES];
            [self.rightButton setHidden:NO];
            [self.rightButton setTitle:@"去评价" forState:UIControlStateNormal];
        } else {
            [self.leftButton setHidden:YES];
            [self.rightButton setHidden:YES];
        }
    // 已完成
    } else if (_model.status.integerValue == 4) {
        bottomCon -= 40;
        [self.moreButton setHidden:NO];
        if (_model.productType.integerValue == 1) {
            [self.leftButton setHidden:YES];
            [self.rightButton setHidden:NO];
            [self.rightButton setTitle:@"查看评价" forState:UIControlStateNormal];
        } else {
            [self.leftButton setHidden:YES];
            [self.rightButton setHidden:NO];
            [self.rightButton setTitle:@"查看卡券" forState:UIControlStateNormal];
        }
        
    // 已取消 其他
    } else {
        bottomCon -= 40;
        [self.moreButton setHidden:NO];
        [self.leftButton setHidden:YES];
        [self.rightButton setHidden:YES];
    }
    
    [self.bottomConstraint uninstall];
    [self.priceLabel mas_updateConstraints:^(MASConstraintMaker *make) {
         self.bottomConstraint = make.bottom.mas_equalTo(bottomCon);
    }];
}

- (void)moreAction {
    if (self.delegate && [self.delegate respondsToSelector:@selector(QJMallOrderListTableViewCellMoreAction:type:row:)]) {
        [self.delegate QJMallOrderListTableViewCellMoreAction:self type:@"更多" row:self.row];
    }
}

- (void)leftAction:(UIButton *)sender {
//    QJMallOrderButtonModel *leftButton = _model.buttons.firstObject;
    if (self.delegate && [self.delegate respondsToSelector:@selector(QJMallOrderListTableViewCellMoreAction:type:row:)]) {
        [self.delegate QJMallOrderListTableViewCellMoreAction:self type:sender.titleLabel.text row:self.row];
    }
}

- (void)rightAction:(UIButton *)sender {
//    QJMallOrderButtonModel *rightButton = _model.buttons.lastObject;
    if (self.delegate && [self.delegate respondsToSelector:@selector(QJMallOrderListTableViewCellMoreAction:type:row:)]) {
        [self.delegate QJMallOrderListTableViewCellMoreAction:self type:sender.titleLabel.text row:self.row];
    }
}

- (UIView *)bgView {
    if (!_bgView) {
        _bgView = [[UIView alloc] init];
        _bgView.backgroundColor = [UIColor whiteColor];
        _bgView.layer.cornerRadius = 8;
    }
    return _bgView;
}

- (UIImageView *)itemImageView {
    if (!_itemImageView) {
        _itemImageView = [[UIImageView alloc] init];
        _itemImageView.layer.cornerRadius = 4;
        _itemImageView.layer.masksToBounds = YES;
        _itemImageView.image = [UIImage imageNamed:@"mine_head"];
        _itemImageView.contentMode = UIViewContentModeScaleAspectFill;

    }
    return _itemImageView;
}

- (UILabel *)timeLabel {
    if (!_timeLabel) {
        _timeLabel = [[UILabel alloc] init];
        _timeLabel.text = @"2020-09-09";
        _timeLabel.textColor = kColorGray;
        [_timeLabel setFont:FontRegular(12)];
    }
    return _timeLabel;
}

- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc] init];
        _nameLabel.text = @"比克大魔王";
        _nameLabel.textColor = UIColorFromRGB(0x000000);
        [_nameLabel setFont:FontMedium(14)];
    }
    return _nameLabel;
}

- (UILabel *)skuLabel {
    if (!_skuLabel) {
        _skuLabel = [[UILabel alloc] init];
        _skuLabel.text = @"比克大魔王 比克大魔王 比克大魔王 比克大魔王 比克大魔王v 比克大魔王 比克大魔王 比克大魔王";
        _skuLabel.textColor = UIColorFromRGB(0x919599);
        [_skuLabel setFont:FontRegular(14)];
        _skuLabel.numberOfLines = 0;
    }
    return _skuLabel;
}

- (UILabel *)statusLabel {
    if (!_statusLabel) {
        _statusLabel = [[UILabel alloc] init];
        _statusLabel.text = @"待发货";
        _statusLabel.textColor = kColorOrange;
        [_statusLabel setFont:FontMedium(14)];
    }
    return _statusLabel;
}

- (UILabel *)numLabel {
    if (!_numLabel) {
        _numLabel = [[UILabel alloc] init];
        _numLabel.text = @"x1";
        _numLabel.textColor = UIColorFromRGB(0x000000);
        [_numLabel setFont:FontRegular(14)];
    }
    return _numLabel;
}

- (UILabel *)priceLabel {
    if (!_priceLabel) {
        _priceLabel = [[UILabel alloc] init];
        _priceLabel.textColor = kColorOrange;
        [_priceLabel setFont:FontMedium(18)];
    }
    return _priceLabel;
}


- (UIView *)logisticsBgView {
    if (!_logisticsBgView) {
        _logisticsBgView = [[UIView alloc] init];
        _logisticsBgView.backgroundColor = UIColorFromRGB(0xfafafa);
        _logisticsBgView.layer.cornerRadius = 4;
    }
    return _logisticsBgView;
}

- (UILabel *)logisticsStatusLabel {
    if (!_logisticsStatusLabel) {
        _logisticsStatusLabel = [[UILabel alloc] init];
        _logisticsStatusLabel.text = @"派送中";
        _logisticsStatusLabel.textColor = kColorOrange;
        [_logisticsStatusLabel setFont:FontSemibold(12)];
    }
    return _logisticsStatusLabel;
}

- (UILabel *)logisticsDetailLabel {
    if (!_logisticsDetailLabel) {
        _logisticsDetailLabel = [[UILabel alloc] init];
        _logisticsDetailLabel.text = @"预计今天到达";
        _logisticsDetailLabel.textColor = kColorOrange;
        [_logisticsDetailLabel setFont:FontRegular(12)];
    }
    return _logisticsDetailLabel;
}

- (UIButton *)moreButton {
    if (!_moreButton) {
        _moreButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_moreButton setTitle:@"更多" forState:UIControlStateNormal];
        [_moreButton setTitleColor:UIColorFromRGB(0x919599) forState:UIControlStateNormal];
        [_moreButton.titleLabel setFont:FontRegular(14)];
        [_moreButton addTarget:self action:@selector(moreAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _moreButton;
}

- (UIButton *)rightButton {
    if (!_rightButton) {
        _rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _rightButton.layer.borderWidth = 1;
        _rightButton.layer.borderColor = UIColorFromRGB(0xff9500).CGColor;
        _rightButton.layer.cornerRadius = 14;

        [_rightButton setTitle:@"查看评价" forState:UIControlStateNormal];
        [_rightButton setTitleColor:UIColorFromRGB(0xff9500) forState:UIControlStateNormal];
        [_rightButton.titleLabel setFont:FontMedium(12)];
        [_rightButton addTarget:self action:@selector(rightAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _rightButton;
}

- (UIButton *)leftButton {
    if (!_leftButton) {
        _leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _leftButton.layer.borderWidth = 1;
        _leftButton.layer.borderColor = UIColorFromRGB(0xc8cacc).CGColor;
        _leftButton.layer.cornerRadius = 14;

        [_leftButton setTitle:@"查看物流" forState:UIControlStateNormal];
        [_leftButton setTitleColor:UIColorFromRGB(0x919599) forState:UIControlStateNormal];
        [_leftButton.titleLabel setFont:FontMedium(12)];
        [_leftButton addTarget:self action:@selector(leftAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _leftButton;
}

- (NSString *)getStatusString:(NSInteger)status {
    switch (status) {
        case 1:
            return @"待发货";
            break;
        case 2:
            return @"待收货";
            break;
        case 3:
            return @"待评价";
            break;
        case 4:
            return @"已完成";
            break;
        case 5:
            return @"已取消";
            break;
            
        default:
            return @"";
            break;
    }
}

- (NSString *)getButtonString:(NSInteger)status {
    switch (status) {
        case 2:
            return @"查看物流";
            break;
        case 3:
            return @"确定收货";
            break;
        case 4:
            return @"查看卡券";
            break;
        case 5:
            return @"去评价";
            break;
        case 6:
            return @"查看评价";
            break;
        case 7:
            return @"删除订单";
            break;
            
        default:
            return @"";
            break;
    }
}

- (NSString *)getTraceString:(NSInteger)status {
    switch (status) {
        case 0:
            return @"已下单";
            break;
        case 1:
            return @"已支付";
            break;
        case 2:
            return @"已发货";
            break;
        case 3:
            return @"已揽件";
            break;
        case 4:
            return @"运输中";
            break;
        case 5:
            return @"派送中";
            break;
        case 6:
            return @"已签收";
            break;
        
        default:
            return @"暂无";
            break;
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
