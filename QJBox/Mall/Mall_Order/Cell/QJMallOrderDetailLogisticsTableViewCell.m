//
//  QJMallOrderDetailLogisticsTableViewCell.m
//  QJBox
//
//  Created by macm on 2022/8/24.
//

#import "QJMallOrderDetailLogisticsTableViewCell.h"

@interface QJMallOrderDetailLogisticsTableViewCell ()
@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) UIView *statusLeftView;
@property (nonatomic, strong) UIView *leftLineView;
@property (nonatomic, strong) UIView *addressLeftView;

@property (nonatomic, strong) UILabel *statusLabel;
@property (nonatomic, strong) UILabel *timeLabel;
@property (nonatomic, strong) UILabel *moreLabel;
@property (nonatomic, strong) UIImageView *moreImageView;
@property (nonatomic, strong) UILabel *statusDetailLabel;

@property (nonatomic, strong) UILabel *addressLabel;
@property (nonatomic, strong) UILabel *nameLabel;

@end

@implementation QJMallOrderDetailLogisticsTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor clearColor];
        [self setupUI];
    }
    return self;
}

- (void)setupUI {
    [self.contentView addSubview:self.bgView];
    [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            
        make.top.mas_equalTo(0);
        make.left.mas_equalTo(16);
        make.right.mas_equalTo(-16);
        make.bottom.mas_equalTo(-16);
    }];
    
    [self.bgView addSubview:self.statusLeftView];
    [self.statusLeftView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(20);
        make.left.mas_equalTo(16);
        make.width.height.mas_equalTo(8*kWScale);
    }];
    
    [self.bgView addSubview:self.statusLabel];
    [self.statusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.statusLeftView.mas_right).mas_offset(8);
        make.centerY.mas_equalTo(self.statusLeftView);
    }];
    
    [self.bgView addSubview:self.timeLabel];
    [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.statusLabel.mas_right).mas_offset(8);
        make.centerY.mas_equalTo(self.statusLeftView);
    }];
    
    [self.bgView addSubview:self.moreImageView];
    [self.moreImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-8);
        make.centerY.mas_equalTo(self.statusLeftView);
    }];
    
    [self.bgView addSubview:self.moreLabel];
    [self.moreLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-24);
        make.centerY.mas_equalTo(self.statusLeftView);
    }];
    
    [self.bgView addSubview:self.statusDetailLabel];
    [self.statusDetailLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.statusLabel);
        make.top.mas_equalTo(self.statusLabel.mas_bottom);
        make.right.mas_equalTo(-16);
    }];
    
    [self.bgView addSubview:self.addressLeftView];
    [self.addressLeftView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.statusDetailLabel.mas_bottom).mas_offset(16);
        make.left.mas_equalTo(16);
        make.width.height.mas_equalTo(8*kWScale);
    }];
    
    [self.bgView addSubview:self.leftLineView];
    [self.leftLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.statusLeftView.mas_bottom);
        make.bottom.mas_equalTo(self.addressLeftView.mas_top);
        make.width.mas_equalTo(1);
        make.centerX.mas_equalTo(self.statusLeftView);
    }];
    
    [self.bgView addSubview:self.addressLabel];
    [self.addressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.addressLeftView.mas_right).mas_offset(8);
        make.right.mas_equalTo(-16);
        make.top.mas_equalTo(self.addressLeftView.mas_top).mas_offset(-5);
    }];
    
    [self.bgView addSubview:self.nameLabel];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.addressLabel);
        make.top.mas_equalTo(self.addressLabel.mas_bottom);
        make.bottom.mas_equalTo(-16);
    }];
    
}

- (void)setModel:(QJMallOrderDetailModel *)model {
    _model = model;
    
    self.nameLabel.text = [NSString stringWithFormat:@"%@ %@", self.model.traces.userReceiveAddress.linkman,self.model.traces.userReceiveAddress.mobile];
    self.addressLabel.text = [NSString stringWithFormat:@"%@%@%@%@", self.model.traces.userReceiveAddress.province,self.model.traces.userReceiveAddress.city,self.model.traces.userReceiveAddress.area,self.model.traces.userReceiveAddress.detail];
    
    QJMallOrderTraceModel *trace = self.model.traces.traces.firstObject;
    self.statusLabel.text = [self getTraceString:trace.state.integerValue];
    if (trace.time.length > 10) {
        self.timeLabel.text = [trace.time substringToIndex:10];
    } else {
        self.timeLabel.text = trace.time;
    }
    self.statusDetailLabel.text = [NSString stringWithFormat:@"%@ %@",self.model.traces.delivery.name,trace.desc];
}

- (UIView *)bgView {
    if (!_bgView) {
        _bgView = [[UIView alloc] init];
        _bgView.backgroundColor = [UIColor whiteColor];
        _bgView.layer.cornerRadius = 8;
    }
    return _bgView;
}

- (UIView *)statusLeftView {
    if (!_statusLeftView) {
        _statusLeftView = [[UIView alloc] init];
        _statusLeftView.backgroundColor = kColorOrange;
        _statusLeftView.layer.cornerRadius = 4;
    }
    return _statusLeftView;
}


- (UILabel *)statusLabel {
    if (!_statusLabel) {
        _statusLabel = [[UILabel alloc] init];
        _statusLabel.text = @"已揽件";
        _statusLabel.textColor = kColorOrange;
        [_statusLabel setFont:FontSemibold(14)];
    }
    return _statusLabel;
}

- (UILabel *)statusDetailLabel {
    if (!_statusDetailLabel) {
        _statusDetailLabel = [[UILabel alloc] init];
        _statusDetailLabel.text = @"顺丰快递 已收件";
        _statusDetailLabel.textColor = kColorGray;
        [_statusDetailLabel setFont:FontRegular(12)];
        _statusDetailLabel.numberOfLines = 1;
    }
    return _statusDetailLabel;
}

- (UILabel *)timeLabel {
    if (!_timeLabel) {
        _timeLabel = [[UILabel alloc] init];
        _timeLabel.text = @"2020-09-09";
        _timeLabel.textColor = kColorOrange;
        [_timeLabel setFont:FontMedium(12)];
    }
    return _timeLabel;
}

- (UIImageView *)moreImageView {
    if (!_moreImageView) {
        _moreImageView = [[UIImageView alloc] init];
        _moreImageView.image = [UIImage imageNamed:@"mall_order_more"];
    }
    return _moreImageView;
}

- (UILabel *)moreLabel {
    if (!_moreLabel) {
        _moreLabel = [[UILabel alloc] init];
        _moreLabel.text = @"详细信息";
        _moreLabel.textColor = kColorOrange;
        [_moreLabel setFont:FontRegular(12)];
    }
    return _moreLabel;
}

- (UILabel *)addressLabel {
    if (!_addressLabel) {
        _addressLabel = [[UILabel alloc] init];
        _addressLabel.text = @"送至河南省商丘市永城市李寨镇9幢163室";
        _addressLabel.textColor = UIColorFromRGB(0x000000);
        [_addressLabel setFont:FontSemibold(14)];
        _addressLabel.numberOfLines = 0;
    }
    return _addressLabel;
}

- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc] init];
        _nameLabel.text = @"魔王 13909876553";
        _nameLabel.textColor = kColorGray;
        [_nameLabel setFont:FontMedium(12)];
    }
    return _nameLabel;
}

- (UIView *)addressLeftView {
    if (!_addressLeftView) {
        _addressLeftView = [[UIView alloc] init];
        _addressLeftView.backgroundColor = [UIColor whiteColor];
        _addressLeftView.layer.cornerRadius = 4;
        _addressLeftView.layer.borderWidth = 1;
        _addressLeftView.layer.borderColor = kColorOrange.CGColor;
    }
    return _addressLeftView;
}

- (UIView *)leftLineView {
    if (!_leftLineView) {
        _leftLineView = [[UIView alloc] init];
        _leftLineView.backgroundColor = kColorOrange;
    }
    return _leftLineView;
}

- (NSString *)getTraceString:(NSInteger)status {
    switch (status) {
        case 0:
            return @"已下单";
            break;
        case 1:
            return @"已支付";
            break;
        case 2:
            return @"已发货";
            break;
        case 3:
            return @"已揽件";
            break;
        case 4:
            return @"运输中";
            break;
        case 5:
            return @"派送中";
            break;
        case 6:
            return @"已签收";
            break;
        
        default:
            return @"暂无";
            break;
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
