//
//  QJMallOrderListTableViewCell.h
//  QJBox
//
//  Created by macm on 2022/8/24.
//

#import <UIKit/UIKit.h>
#import "QJMallOrderModel.h"

NS_ASSUME_NONNULL_BEGIN

@class QJMallOrderListTableViewCell;

typedef NS_ENUM(NSInteger, QJMallOrderListCellDidType){
    QJMallOrderListCellDidMore = 1,  //更多
    QJMallOrderListCellDidLogistics,  //2-查看物流： 实物 + 待发货及之后
    QJMallOrderListCellDidTakeGoods,   //3-确定收货： 实物 + 待收货
    QJMallOrderListCellDidCoupon,    //4-查看卡券： 电子卡
    QJMallOrderListCellDidCreateEvaluation,     //5-去评价: 实物 + 待评价
    QJMallOrderListCellDidEvaluation,     //6-查看评价： 实物 + 已完成
    QJMallOrderListCellDidDelete,     //7-删除订单： 已完成、已取消
};

@protocol QJMallOrderListTableViewCellDelegate <NSObject>
// 点击更多按钮出现删除
- (void)QJMallOrderListTableViewCellMoreAction:(QJMallOrderListTableViewCell *)cell type:(NSString *)type row:(NSInteger)row;
@end

@interface QJMallOrderListTableViewCell : UITableViewCell
@property (nonatomic, strong) UIButton *moreButton;
@property (nonatomic, weak) id<QJMallOrderListTableViewCellDelegate>delegate;
@property (nonatomic, strong) QJMallOrderModel *model;
@property (nonatomic, assign) NSInteger row;
@end

NS_ASSUME_NONNULL_END
