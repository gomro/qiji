//
//  QJMallOrderListPageView.m
//  QJBox
//
//  Created by macm on 2022/8/24.
//

#import "QJMallOrderListPageView.h"
#import "QJMallOrderListTableViewCell.h"
#import "QJMallOrderDetailViewController.h"
#import "QJMallOrderRequest.h"
#import "QJMallCardConvertViewController.h"
#import "QJMallOrderFinishViewController.h"
#import "QJMallOrderLogisticsDetailView.h"
#import "QJMallShopEvaluateViewController.h"
#import "QJMallOrderConfirmReceiptView.h"
#import "QJMallCommentViewController.h"

@interface QJMallOrderListPageView ()<UITableViewDelegate,UITableViewDataSource,QJMallOrderListTableViewCellDelegate>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, assign) NSInteger page;
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, copy) NSString *status;
@property (nonatomic, assign) NSInteger delRow;

@end

@implementation QJMallOrderListPageView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.page = 1;
        self.dataArray = [NSMutableArray array];
        [self createView];
    }
    return self;
}
/**
 * 刷新数据
 * type：1-待发货、2-待收货、3-待评价、4-已完成可用
 */
- (void)reloadPageView:(NSString *)status {
    self.status = status;
    [self loadData];
}

- (void)loadData {
    _page = 1;
    [self.dataArray removeAllObjects];
    [self sendRequest];
}

- (void)loadMoreData {
    _page++;
    [self sendRequest];
}

- (void)sendRequest {

    @weakify(self);
    QJMallOrderRequest *request = [[QJMallOrderRequest alloc] init];
    [request requestShopOrderListStatus:self.status page:self.page completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        @strongify(self);
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];

        if (isSuccess) {
            NSArray *array = [NSArray modelArrayWithClass:[QJMallOrderModel class] json:EncodeArrayFromDic(request.responseJSONObject[@"data"], @"records")];
            [self.dataArray addObjectsFromArray:array];
            [self.tableView reloadData];

            if ([request.responseJSONObject[@"data"][@"hasNext"] integerValue] == 0 || array.count == 0) {
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
            }
        }
        QJEmptyCommonView *empty = [QJEmptyCommonView new];
        empty.string = @"暂无相关订单";
        [self.tableView tableViewDisplayWhenHaveNoDataWithView:empty ifNecessaryForRowCount:self.dataArray.count];

    }];
    
}

- (void)createView {
    [self addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.left.right.mas_equalTo(0);
    }];
    
    [self addSubview:self.delButton];
    
}

- (void)delAction {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:@"确认要删除订单吗?" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *nextAction = [UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self sendRequestDelete];
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDestructive handler:nil];
    
    [alert addAction:cancelAction];
    [alert addAction:nextAction];
    [[QJAppTool getCurrentViewController] presentViewController:alert animated:YES completion:nil];
    [cancelAction setValue:UIColorFromRGB(0x919599) forKey:@"titleTextColor"];
    [nextAction setValue:UIColorFromRGB(0x007aff) forKey:@"titleTextColor"];
}

- (void)sendRequestDelete {
    [QJAppTool showHUDLoading];
    QJMallOrderModel *model = [self.dataArray safeObjectAtIndex:self.delRow];

    @weakify(self);
    QJMallOrderRequest *request = [[QJMallOrderRequest alloc] init];
    [request requestShopOrderDeleteOrderId:model.idStr completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        @strongify(self);
        
        [QJAppTool hideHUDLoading];
        if (isSuccess) {
            [self.delButton setHidden:YES];
            
            [self.dataArray removeObjectAtIndex:self.delRow];
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.delRow inSection:0];
            [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        }

    }];
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    QJMallOrderListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QJMallOrderListTableViewCell" forIndexPath:indexPath];
    cell.delegate = self;
    cell.model = [self.dataArray safeObjectAtIndex:indexPath.row];
    cell.row = indexPath.row;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    QJMallOrderModel *model = [self.dataArray safeObjectAtIndex:indexPath.row];
    [self.delButton setHidden:YES];

    QJMallOrderDetailViewController *vc = [[QJMallOrderDetailViewController alloc] init];
    vc.idStr = model.idStr;
    [[QJAppTool getCurrentViewController].navigationController pushViewController:vc animated:YES];
}

#pragma mark - UITableViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self.delButton setHidden:YES];
}
    
#pragma mark - QJMallOrderListTableViewCellDelegate
    
- (void)QJMallOrderListTableViewCellMoreAction:(QJMallOrderListTableViewCell *)cell type:(NSString *)type row:(NSInteger)row {
    // 点击更多
    if ([type isEqualToString:@"更多"]) {
        CGPoint buttonCenter = CGPointMake(cell.moreButton.bounds.origin.x + cell.moreButton.bounds.size.width/2,
                                           cell.moreButton.bounds.origin.y + cell.moreButton.bounds.size.height/2);
        CGPoint btnorigin = [cell.moreButton convertPoint:buttonCenter toView:self];

        [self.delButton setHidden:NO];
        self.delButton.hx_y = btnorigin.y+15;
        
        self.delRow = row;
    }
    // 确认收货
    if ([type isEqualToString:@"确认收货"]) {
        QJMallOrderModel *model = [self.dataArray safeObjectAtIndex:row];

        QJMallOrderItemModel *item = model.items.firstObject;
        QJMallOrderConfirmReceiptView *view = [[QJMallOrderConfirmReceiptView alloc] initWithOrderId:model.idStr itemImage:item.picture];
        view.clickConfirmButton = ^{
            QJMallOrderFinishViewController *vc = [[QJMallOrderFinishViewController alloc] init];
            [[QJAppTool getCurrentViewController].navigationController pushViewController:vc animated:YES];
        };
        [view show];
        
    }
    // 查看卡券
    if ([type isEqualToString:@"查看卡券"]) {
        QJMallOrderModel *model = [self.dataArray safeObjectAtIndex:row];
        QJMallCardConvertViewController *vc = [[QJMallCardConvertViewController alloc] init];
        vc.idStr = model.idStr;
        [[QJAppTool getCurrentViewController].navigationController pushViewController:vc animated:YES];
    }
    // 查看评价
    if ([type isEqualToString:@"查看评价"]) {
        QJMallOrderModel *model = [self.dataArray safeObjectAtIndex:row];
        QJMallOrderItemModel *item = model.items.firstObject;
        QJMallCommentViewController *vc = [[QJMallCommentViewController alloc] init];
        vc.mallID = item.idStr;
        [[QJAppTool getCurrentViewController].navigationController pushViewController:vc animated:YES];
    }
    // 查看物流
    if ([type isEqualToString:@"查看物流"]) {
        QJMallOrderModel *model = [self.dataArray safeObjectAtIndex:row];
        QJMallOrderLogisticsDetailView *detailView = [[QJMallOrderLogisticsDetailView alloc] initWithTitles:nil orderId:model.idStr];
        [detailView show];
    }
    // 去评价
    if ([type isEqualToString:@"去评价"]) {
        QJMallOrderModel *model = [self.dataArray safeObjectAtIndex:row];
        QJMallShopEvaluateViewController *vc = [[QJMallShopEvaluateViewController alloc] init];
        QJMallOrderItemModel *item = model.items.firstObject;
        vc.idStr = item.idStr;
        [[QJAppTool getCurrentViewController].navigationController pushViewController:vc animated:YES];
    }
    // 删除订单
    if ([type isEqualToString:@"删除订单"]) {
        self.delRow = row;
        [self delAction];
    }
        
}

- (UITableView *)tableView {
    if (!_tableView) {
        self.tableView = [[UITableView alloc] init];
        self.tableView.backgroundColor = [UIColor clearColor];
        self.tableView.dataSource = self;
        self.tableView.delegate = self;
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.tableView.showsVerticalScrollIndicator = NO;
        self.tableView.showsHorizontalScrollIndicator = NO;
        [self.tableView registerClass:[QJMallOrderListTableViewCell class] forCellReuseIdentifier:@"QJMallOrderListTableViewCell"];
        self.tableView.estimatedRowHeight = 100;
        if (@available(iOS 11.0, *)) {
            self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
        self.tableView.mj_header = [QJRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadData)];
        self.tableView.mj_footer = [QJRefreshFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
 
    }
    return _tableView;
}

- (UIButton *)delButton {
    if (!_delButton) {
        _delButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _delButton.frame = CGRectMake(32, 0, 80*kWScale, 34*kWScale);
        [_delButton setBackgroundColor:[UIColor whiteColor]];
        [_delButton setTitle:@"删除订单" forState:UIControlStateNormal];
        [_delButton setTitleColor:UIColorFromRGB(0x474849) forState:UIControlStateNormal];
        [_delButton.titleLabel setFont:FontRegular(12)];
        [_delButton addTarget:self action:@selector(delAction) forControlEvents:UIControlEventTouchUpInside];
        _delButton.layer.shadowColor = UIColorHex(00000014).CGColor;
        _delButton.layer.shadowOpacity = 10;
        _delButton.layer.shadowRadius = 10;
        _delButton.layer.shadowOffset = CGSizeMake(0, 4);
        [_delButton setHidden:YES];
    }
    return _delButton;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
