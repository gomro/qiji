//
//  QJMallOrderLogisticsDetailView.m
//  QJBox
//
//  Created by macm on 2022/8/25.
//

#import "QJMallOrderLogisticsDetailView.h"
#import "QJMallOrderLogisticsDetailTableViewCell.h"
#import "QJMallOrderRequest.h"

#define ContentViewHeight (Bottom_iPhoneX_SPACE+610*kWScale)

@interface QJMallOrderLogisticsDetailView ()<UITableViewDelegate,UITableViewDataSource>

/** 控件View */
@property (nonatomic, strong) UIView *contentView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIButton *closeButton;
@property (nonatomic, strong) UIImageView *logisticsImageView;
@property (nonatomic, strong) UILabel *logisticsLabel;
@property (nonatomic, strong) UILabel *logisticsNumLabel;
@property (nonatomic, strong) UIButton *logisticsNumButton;
@property (nonatomic, strong) UIView *topLineView;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UIView *bottomLineView;
@property (nonatomic, strong) UIImageView *addressImageView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *addressLabel;
@property (nonatomic, strong) QJMallOrderDetailTraceModel *model;

@end

@implementation QJMallOrderLogisticsDetailView

- (instancetype)initWithTitles:(QJMallOrderDetailTraceModel *_Nullable)model orderId:(NSString *_Nullable)orderId {
    if (self = [super init]) {
        self.frame = [UIScreen mainScreen].bounds;
        self.backgroundColor = RGBAlpha(0, 0, 0, 0.4);
        if (orderId != nil) {
            [self sendRequest:orderId];
        } else {
            self.model = model;
        }
        
        [self setupUI];
        
        [self reloadTableData];

    }
    return self;
}

- (void)sendRequest:(NSString *)orderId {
    [QJAppTool showHUDLoading];
    @weakify(self);
    QJMallOrderRequest *request = [[QJMallOrderRequest alloc] init];
    [request requestShopOrderTracesOrderId:orderId completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        @strongify(self);
        [QJAppTool hideHUDLoading];
        
        if (isSuccess) {
            self.model = [QJMallOrderDetailTraceModel modelWithDictionary:request.responseJSONObject[@"data"]];
            [self reloadTableData];
            [self.tableView reloadData];
        }
        [self.tableView tableViewDisplayWhenHaveNoDataWithView:[QJEmptyCommonView new] ifNecessaryForRowCount:self.model.traces.count];
    }];
    
}

- (void)reloadTableData {
    self.logisticsLabel.text = self.model.delivery.name;
    self.logisticsNumLabel.text = self.model.delivery.idStr;
    [self.logisticsImageView setImageWithURL:[NSURL URLWithString:[self.model.delivery.logo checkImageUrlString]] placeholder:[UIImage imageNamed:@"mine_head"]];
    self.nameLabel.text = [NSString stringWithFormat:@"%@ %@", self.model.userReceiveAddress.linkman,self.model.userReceiveAddress.mobile];
    self.addressLabel.text = [NSString stringWithFormat:@"%@%@%@%@", self.model.userReceiveAddress.province,self.model.userReceiveAddress.city,self.model.userReceiveAddress.area,self.model.userReceiveAddress.detail];
}

- (void)setupUI {
    
    UIView *cView = [[UIView alloc] init];
    cView.backgroundColor = [UIColor whiteColor];
    [self addSubview:cView];
    self.contentView = cView;
   
    self.contentView.layer.masksToBounds = YES;
     
    self.contentView.frame = CGRectMake(0, kScreenHeight, kScreenWidth, ContentViewHeight);
    [self.contentView showCorner:16 rectCorner:UIRectCornerTopLeft | UIRectCornerTopRight];

    [self.contentView addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(30);
        make.centerX.mas_equalTo(self.contentView);
    }];
    
    [self.contentView addSubview:self.closeButton];
    [self.closeButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(18);
        make.right.mas_equalTo(-16);
        make.width.height.mas_equalTo(30*kWScale);
    }];
     
    [self.contentView addSubview:self.logisticsImageView];
    [self.logisticsImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(84);
        make.left.mas_equalTo(24);
        make.width.height.mas_equalTo(24*kWScale);
    }];
    
    [self.contentView addSubview:self.logisticsLabel];
    [self.logisticsLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.logisticsImageView.mas_right).mas_offset(8);
        make.centerY.mas_equalTo(self.logisticsImageView);
    }];
    
    [self.contentView addSubview:self.logisticsNumLabel];
    [self.logisticsNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.logisticsLabel.mas_right).mas_offset(4);
        make.centerY.mas_equalTo(self.logisticsImageView);
    }];
    
    [self.contentView addSubview:self.logisticsNumButton];
    [self.logisticsNumButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-24);
        make.centerY.mas_equalTo(self.logisticsImageView);
        make.width.mas_equalTo(30*kWScale);
        make.height.mas_equalTo(30*kWScale);
    }];
    
    [self.contentView addSubview:self.topLineView];
    [self.topLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(123);
        make.height.mas_equalTo(1);
    }];
    
    [self.contentView addSubview:self.addressLabel];
    [self.addressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(72);
        make.right.mas_equalTo(-16);
        make.bottom.mas_equalTo(-Bottom_iPhoneX_SPACE-16);
    }];
    
    [self.contentView addSubview:self.nameLabel];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.addressLabel);
        make.bottom.mas_equalTo(self.addressLabel.mas_top).mas_offset(-4);
    }];
    
    [self.contentView addSubview:self.addressImageView];
    [self.addressImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(37);
        make.top.mas_equalTo(self.nameLabel.mas_top).mas_offset(10);
        make.width.height.mas_equalTo(24*kWScale);
    }];
    
    [self.contentView addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(130);
        make.bottom.mas_equalTo(self.nameLabel.mas_top).mas_offset(-20);
    }];
    
    [self.contentView addSubview:self.bottomLineView];
    [self.bottomLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.height.mas_equalTo(1);
        make.top.mas_equalTo(self.tableView.mas_bottom);
    }];
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.model.traces.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    QJMallOrderLogisticsDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QJMallOrderLogisticsDetailTableViewCell" forIndexPath:indexPath];
    cell.model = self.model.traces[indexPath.row];
    
    [cell.topLineView setHidden:NO];
    [cell.bottomLineView setHidden:NO];
    if (indexPath.row == 0) {
        [cell.topLineView setHidden:YES];
        cell.leftView.backgroundColor = kColorOrange;
        cell.statusLabel.textColor = kColorOrange;
        cell.timeLabel.textColor = kColorOrange;
        cell.detailLabel.textColor = UIColorFromRGB(0x16191c);
    } else {
        cell.leftView.backgroundColor = UIColorFromRGB(0xc8cacc);
        cell.statusLabel.textColor = UIColorFromRGB(0x919599);
        cell.timeLabel.textColor = UIColorFromRGB(0x919599);
        cell.detailLabel.textColor = UIColorFromRGB(0x919599);
    }
    if (indexPath.row == self.model.traces.count-1) {
        [cell.bottomLineView setHidden:YES];
    }
    
    return cell;
}

#pragma mark --  action


- (void)cancelBtnAction {
    [self fadeOut];
}

- (void)show {
    UIWindow *keywindow = [[UIApplication sharedApplication] keyWindow];
    [keywindow addSubview:self];
    [self fadeIn];
}

- (void)fadeIn {
//    self.transform = CGAffineTransformMakeScale(1.3, 1.3);
    self.alpha = 0;
    [UIView animateWithDuration:0.20 animations:^{
        self.alpha = 1;
//        self.transform = CGAffineTransformMakeScale(1, 1);
        self.contentView.frame = CGRectMake(0, kScreenHeight - ContentViewHeight  , kScreenWidth, ContentViewHeight);

    }];
}

- (void)fadeOut {

    [UIView animateWithDuration:0.35 animations:^{
//        self.transform = CGAffineTransformMakeScale(1.3, 1.3);
        self.alpha = 0.0;
        self.contentView.frame = CGRectMake(0,[UIScreen mainScreen].bounds.size.height, kScreenWidth, ContentViewHeight);
    } completion:^(BOOL finished) {
        if (finished) {
            [self removeFromSuperview];
        }
    }];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];

    UIView *view = [touch view];

    if (view == self) {
        [self fadeOut];
    }
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.text = @"详细信息";
        _titleLabel.textColor = UIColorFromRGB(0x16191c);
        [_titleLabel setFont:FontSemibold(18)];
    }
    return _titleLabel;
}

- (UIButton *)closeButton {
    if (!_closeButton) {
        _closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_closeButton setImage:[UIImage imageNamed:@"qj_close_24"] forState:UIControlStateNormal];
        [_closeButton addTarget:self action:@selector(cancelBtnAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _closeButton;
}

- (UIImageView *)logisticsImageView {
    if (!_logisticsImageView) {
        _logisticsImageView = [[UIImageView alloc] init];
        _logisticsImageView.layer.cornerRadius = 12;
        _logisticsImageView.layer.masksToBounds = YES;
        _logisticsImageView.contentMode = UIViewContentModeScaleAspectFill;
    }
    return _logisticsImageView;
}

- (UILabel *)logisticsLabel {
    if (!_logisticsLabel) {
        _logisticsLabel = [[UILabel alloc] init];
        _logisticsLabel.text = @"顺丰快递";
        _logisticsLabel.textColor = UIColorFromRGB(0x000000);
        [_logisticsLabel setFont:FontMedium(14)];
    }
    return _logisticsLabel;
}

- (UILabel *)logisticsNumLabel {
    if (!_logisticsNumLabel) {
        _logisticsNumLabel = [[UILabel alloc] init];
        _logisticsNumLabel.text = @"7813264987162";
        _logisticsNumLabel.textColor = UIColorFromRGB(0x16191c);
        [_logisticsNumLabel setFont:FontMedium(14)];
    }
    return _logisticsNumLabel;
}

- (UIButton *)logisticsNumButton {
    if (!_logisticsNumButton) {
        _logisticsNumButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_logisticsNumButton setTitle:@"复制" forState:UIControlStateNormal];
        [_logisticsNumButton setTitleColor:UIColorFromRGB(0x474849) forState:UIControlStateNormal];
        [_logisticsNumButton.titleLabel setFont:FontRegular(14)];
        [_logisticsNumButton addTarget:self action:@selector(logisticsNumAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _logisticsNumButton;
}

- (void)logisticsNumAction {
    UIPasteboard *pab = [UIPasteboard generalPasteboard];
    pab.string = self.logisticsNumLabel.text;
    if (pab == nil) {
        [QJAppTool showToast:@"复制失败"];
    } else {
        [QJAppTool showToast:@"已复制"];
    }
}

- (UIView *)topLineView {
    if (!_topLineView) {
        _topLineView = [[UIView alloc] init];
        _topLineView.backgroundColor = UIColorFromRGB(0xe8edf0);
    }
    return _topLineView;
}

- (UITableView *)tableView {
    if (!_tableView) {
        self.tableView = [[UITableView alloc] init];
        self.tableView.backgroundColor = [UIColor clearColor];
        self.tableView.dataSource = self;
        self.tableView.delegate = self;
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.tableView.showsVerticalScrollIndicator = NO;
        self.tableView.showsHorizontalScrollIndicator = NO;
        [self.tableView registerClass:[QJMallOrderLogisticsDetailTableViewCell class] forCellReuseIdentifier:@"QJMallOrderLogisticsDetailTableViewCell"];

        if (@available(iOS 11.0, *)) {
            self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
        self.tableView.estimatedRowHeight = 0.01;
 
    }
    return _tableView;
}

- (UIView *)bottomLineView {
    if (!_bottomLineView) {
        _bottomLineView = [[UIView alloc] init];
        _bottomLineView.backgroundColor = UIColorFromRGB(0xe8edf0);
    }
    return _bottomLineView;
}

- (UIImageView *)addressImageView {
    if (!_addressImageView) {
        _addressImageView = [[UIImageView alloc] init];
        _addressImageView.image = [UIImage imageNamed:@"mall_order_location"];
    }
    return _addressImageView;
}

- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc] init];
        _nameLabel.text = @"刘达 13076543456";
        _nameLabel.textColor = UIColorFromRGB(0x16191c);
        [_nameLabel setFont:FontRegular(14)];
    }
    return _nameLabel;
}

- (UILabel *)addressLabel {
    if (!_addressLabel) {
        _addressLabel = [[UILabel alloc] init];
        _addressLabel.text = @"河南省商丘市永城市李寨镇9幢163室";
        _addressLabel.textColor = UIColorFromRGB(0x000000);
        [_addressLabel setFont:FontRegular(14)];
        _addressLabel.numberOfLines = 0;
    }
    return _addressLabel;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
