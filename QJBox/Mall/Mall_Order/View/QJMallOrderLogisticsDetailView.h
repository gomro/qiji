//
//  QJMallOrderLogisticsDetailView.h
//  QJBox
//
//  Created by macm on 2022/8/25.
//

#import <UIKit/UIKit.h>
#import "QJMallOrderDetailModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface QJMallOrderLogisticsDetailView : UIView
- (instancetype)initWithTitles:(QJMallOrderDetailTraceModel *_Nullable)model orderId:(NSString *_Nullable)orderId;
- (void)show;
@end

NS_ASSUME_NONNULL_END
