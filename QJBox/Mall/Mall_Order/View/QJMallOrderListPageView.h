//
//  QJMallOrderListPageView.h
//  QJBox
//
//  Created by macm on 2022/8/24.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJMallOrderListPageView : UIView
@property (nonatomic, strong) UIButton *delButton;
/**
 * 刷新数据
 * type：1-待发货、2-待收货、3-待评价、4-已完成可用
 */
- (void)reloadPageView:(NSString *)status;
@end

NS_ASSUME_NONNULL_END
