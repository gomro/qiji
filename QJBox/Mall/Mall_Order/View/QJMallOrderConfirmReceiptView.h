//
//  QJMallOrderConfirmReceiptView.h
//  QJBox
//
//  Created by macm on 2022/8/25.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJMallOrderConfirmReceiptView : UIView
@property (nonatomic, copy) void(^clickConfirmButton)(void);

- (instancetype)initWithOrderId:(NSString *)orderId itemImage:(NSString *)itemImage;
- (void)show;
@end

NS_ASSUME_NONNULL_END
