//
//  QJMallOrderConfirmReceiptView.m
//  QJBox
//
//  Created by macm on 2022/8/25.
//

#import "QJMallOrderConfirmReceiptView.h"
#import "QJMallOrderRequest.h"

#define ContentViewHeight (Bottom_iPhoneX_SPACE+268*kWScale)

@interface QJMallOrderConfirmReceiptView ()

/** 控件View */
@property (nonatomic, strong) UIView *contentView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIButton *closeButton;

@property (nonatomic, strong) UIImageView *goodsImageView;
@property (nonatomic, strong) UILabel *tipLabel;
@property (nonatomic, strong) UIButton *confirmButton;
@property (nonatomic, copy) NSString *orderId;
@end

@implementation QJMallOrderConfirmReceiptView

- (instancetype)initWithOrderId:(NSString *)orderId itemImage:(NSString *)itemImage {
    if (self = [super init]) {
        self.frame = [UIScreen mainScreen].bounds;
        self.backgroundColor = RGBAlpha(0, 0, 0, 0.4);
        self.orderId = orderId;
        
        [self setupUI];
        
        [self.goodsImageView setImageWithURL:[NSURL URLWithString:[itemImage checkImageUrlString]] placeholder:[UIImage imageNamed:@"mine_head"]];

    }
    return self;
}

- (void)setupUI {
    
    UIView *cView = [[UIView alloc] init];
    cView.backgroundColor = [UIColor whiteColor];
    [self addSubview:cView];
    self.contentView = cView;
   
    self.contentView.layer.masksToBounds = YES;
     
    self.contentView.frame = CGRectMake(0, kScreenHeight, kScreenWidth, ContentViewHeight);
    [self.contentView showCorner:16 rectCorner:UIRectCornerTopLeft | UIRectCornerTopRight];

    [self.contentView addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(30);
        make.centerX.mas_equalTo(self.contentView);
    }];
    
    [self.contentView addSubview:self.closeButton];
    [self.closeButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(16);
        make.right.mas_equalTo(-16);
        make.width.height.mas_equalTo(30*kWScale);
    }];
     
    [self.contentView addSubview:self.goodsImageView];
    [self.goodsImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(64);
        make.centerX.mas_equalTo(self.contentView);
        make.width.height.mas_equalTo(64*kWScale);
    }];
    
    [self.contentView addSubview:self.tipLabel];
    [self.tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(47);
        make.right.mas_equalTo(-47);
        make.top.mas_equalTo(self.goodsImageView.mas_bottom).mas_offset(24);
    }];
    
    [self.contentView addSubview:self.confirmButton];
    [self.confirmButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(37);
        make.right.mas_equalTo(-37);
        make.top.mas_equalTo(self.tipLabel.mas_bottom).mas_offset(24);
        make.height.mas_equalTo(34*kWScale);
    }];

}

#pragma mark --  action


- (void)cancelBtnAction {
    [self fadeOut];
}

- (void)show {
    UIWindow *keywindow = [[UIApplication sharedApplication] keyWindow];
    [keywindow addSubview:self];
    [self fadeIn];
}

- (void)fadeIn {
//    self.transform = CGAffineTransformMakeScale(1.3, 1.3);
    self.alpha = 0;
    [UIView animateWithDuration:0.20 animations:^{
        self.alpha = 1;
//        self.transform = CGAffineTransformMakeScale(1, 1);
        self.contentView.frame = CGRectMake(0, kScreenHeight - ContentViewHeight  , kScreenWidth, ContentViewHeight);

    }];
}

- (void)fadeOut {

    [UIView animateWithDuration:0.35 animations:^{
//        self.transform = CGAffineTransformMakeScale(1.3, 1.3);
        self.alpha = 0.0;
        self.contentView.frame = CGRectMake(0,[UIScreen mainScreen].bounds.size.height, kScreenWidth, ContentViewHeight);
    } completion:^(BOOL finished) {
        if (finished) {
            [self removeFromSuperview];
        }
    }];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];

    UIView *view = [touch view];

    if (view == self) {
        [self fadeOut];
    }
}

- (void)confirmAction {
    [self sendRecRequest];
    
}

// 确认收货
- (void)sendRecRequest {
    [QJAppTool showHUDLoading];
    @weakify(self);
    QJMallOrderRequest *request = [[QJMallOrderRequest alloc] init];
    [request requestShopOrderRecOrderId:self.orderId completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        @strongify(self);
        [QJAppTool hideHUDLoading];
        
        if (isSuccess) {
            if (self.clickConfirmButton) {
                self.clickConfirmButton();
            }
            [self fadeOut];
        }

    }];
    
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.text = @"确认收到货了吗";
        _titleLabel.textColor = UIColorFromRGB(0x16191c);
        [_titleLabel setFont:FontSemibold(16)];
    }
    return _titleLabel;
}

- (UIButton *)closeButton {
    if (!_closeButton) {
        _closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_closeButton setImage:[UIImage imageNamed:@"qj_close_24"] forState:UIControlStateNormal];
        [_closeButton addTarget:self action:@selector(cancelBtnAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _closeButton;
}

- (UIImageView *)goodsImageView {
    if (!_goodsImageView) {
        _goodsImageView = [[UIImageView alloc] init];
        _goodsImageView.layer.cornerRadius = 4;
        _goodsImageView.layer.masksToBounds = YES;
        _goodsImageView.image = [UIImage imageNamed:@"mine_head"];
    }
    return _goodsImageView;
}

- (UILabel *)tipLabel {
    if (!_tipLabel) {
        _tipLabel = [[UILabel alloc] init];
        _tipLabel.text = @"为了保证您的权益，请收到商品后确认无误后再确认收货";
        _tipLabel.textColor = UIColorFromRGB(0x16191c);
        [_tipLabel setFont:FontSemibold(14)];
        _tipLabel.numberOfLines = 0;
        _tipLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _tipLabel;
}

- (UIButton *)confirmButton {
    if (!_confirmButton) {
        _confirmButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_confirmButton setTitle:@"确认" forState:UIControlStateNormal];
        [_confirmButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_confirmButton setBackgroundColor:kColorOrange];
        [_confirmButton.titleLabel setFont:FontMedium(14)];
        _confirmButton.layer.cornerRadius = 17;
        [_confirmButton addTarget:self action:@selector(confirmAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _confirmButton;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
