//
//  QJMallOrderRequest.h
//  QJBox
//
//  Created by macm on 2022/9/21.
//

#import "QJBaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

typedef void(^RequestBlock)(BOOL isSuccess,YTKBaseRequest * _Nonnull request);

@interface QJMallOrderRequest : QJBaseRequest

/**订单列表
 *  status    订单状态，1-待发货、2-待收货、3-待评价、4-已完成,可用值:CANCEL,FINISH,UN_COMMENT,UN_DELIVERY,UN_REC    query    false string
 */
- (void)requestShopOrderListStatus:(NSString *)status page:(NSInteger)page completion:(RequestBlock)completion;

/**订单详情、查看评价
 */
- (void)requestShopOrderDetailOrderId:(NSString *)orderId completion:(RequestBlock)completion;

/**确认收货
 */
- (void)requestShopOrderRecOrderId:(NSString *)orderId completion:(RequestBlock)completion;

/**查看物流
 */
- (void)requestShopOrderTracesOrderId:(NSString *)orderId completion:(RequestBlock)completion;

/**删除订单
 */
- (void)requestShopOrderDeleteOrderId:(NSString *)orderId completion:(RequestBlock)completion;

/**下单
 */
- (void)requestShopOrderSubmitOrderId:(NSString *)orderId completion:(RequestBlock)completion;

/**待付款订单详情
 */
- (void)requestShopOrderUnPayDetailOrderId:(NSString *)orderId completion:(RequestBlock)completion;

/**获取默认地址
 */
- (void)requestRecAddressDefault_completion:(RequestBlock)completion;

/**支付
 */
- (void)requestShopOrderPayId:(NSString *)orderId addressId:(NSString *)addressId completion:(RequestBlock)completion;

@end

NS_ASSUME_NONNULL_END
