//
//  QJMallOrderRequest.m
//  QJBox
//
//  Created by macm on 2022/9/21.
//

#import "QJMallOrderRequest.h"

@implementation QJMallOrderRequest

- (void)sendRequest_completion:(RequestBlock)completion {
    [self start];
    
    [self setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        if (completion) {
            completion(ResponseSuccess, request);
        }
        if (!ResponseSuccess) {
            [[QJAppTool getCurrentViewController].view makeToast:EncodeStringFromDic(request.responseObject, @"message") duration:2 position:CSToastPositionCenter image:[UIImage imageNamed:@"tips_toast"]];
        }
    }];
    
    [self setFailureCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        if (completion) {
            completion(NO, request);
        }
        [[QJAppTool getCurrentViewController].view makeToast:@"网络请求失败" duration:2 position:CSToastPositionCenter image:[UIImage imageNamed:@"tips_toast"]];
    }];
}

/**订单列表
 *  status    订单状态，1-待发货、2-待收货、3-待评价、4-已完成,可用值:CANCEL,FINISH,UN_COMMENT,UN_DELIVERY,UN_REC    query    false string
 */
- (void)requestShopOrderListStatus:(NSString *)status page:(NSInteger)page completion:(RequestBlock)completion {
    self.dic = @{@"page":@(page), @"size":@(10), @"status":status};

    self.urlStr = [NSString stringWithFormat:@"/shop/order/list"];
    self.requestType = YTKRequestMethodGET;
    
    [self sendRequest_completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        if (completion) {
            completion(isSuccess, request);
        }
    }];
    
}

/**订单详情、查看评价
 */
- (void)requestShopOrderDetailOrderId:(NSString *)orderId completion:(RequestBlock)completion {

    self.urlStr = [NSString stringWithFormat:@"/shop/order/detail/%@", orderId];
    self.requestType = YTKRequestMethodGET;
    
    [self sendRequest_completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        if (completion) {
            completion(isSuccess, request);
        }
    }];
    
}

/**确认收货
 */
- (void)requestShopOrderRecOrderId:(NSString *)orderId completion:(RequestBlock)completion {

    self.urlStr = [NSString stringWithFormat:@"/shop/order/rec/%@", orderId];
    self.requestType = YTKRequestMethodPUT;
    
    [self sendRequest_completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        if (completion) {
            completion(isSuccess, request);
        }
    }];
    
}

/**查看物流
 */
- (void)requestShopOrderTracesOrderId:(NSString *)orderId completion:(RequestBlock)completion {

    self.urlStr = [NSString stringWithFormat:@"/shop/traces/%@", orderId];
    self.requestType = YTKRequestMethodGET;
    
    [self sendRequest_completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        if (completion) {
            completion(isSuccess, request);
        }
    }];
    
}

/**删除订单
 */
- (void)requestShopOrderDeleteOrderId:(NSString *)orderId completion:(RequestBlock)completion {

    self.urlStr = [NSString stringWithFormat:@"/shop/order/delete/%@", orderId];
    self.requestType = YTKRequestMethodPUT;
    
    [self sendRequest_completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        if (completion) {
            completion(isSuccess, request);
        }
    }];
    
}

/**下单
 */
- (void)requestShopOrderSubmitOrderId:(NSString *)orderId completion:(RequestBlock)completion {
    self.dic = @{@"prods":@[@{@"mallSkuId":orderId}]};
    
    self.urlStr = [NSString stringWithFormat:@"/shop/order/submit"];
    self.requestType = YTKRequestMethodPOST;
    self.serializerType = YTKRequestSerializerTypeJSON;
    
    [self sendRequest_completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        if (completion) {
            completion(isSuccess, request);
        }
    }];
    
}

/**待付款订单详情
 */
- (void)requestShopOrderUnPayDetailOrderId:(NSString *)orderId completion:(RequestBlock)completion {

    self.urlStr = [NSString stringWithFormat:@"/shop/order/unPayDetail/%@",orderId];
    self.requestType = YTKRequestMethodGET;
    
    [self sendRequest_completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        if (completion) {
            completion(isSuccess, request);
        }
    }];
    
}

/**获取默认地址
 */
- (void)requestRecAddressDefault_completion:(RequestBlock)completion {

    self.urlStr = [NSString stringWithFormat:@"/recAddress/default"];
    self.requestType = YTKRequestMethodGET;
    
    [self sendRequest_completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        if (completion) {
            completion(isSuccess, request);
        }
    }];
    
}

/**支付
 */
- (void)requestShopOrderPayId:(NSString *)orderId addressId:(NSString *)addressId completion:(RequestBlock)completion {
    if (addressId == nil) {
        self.urlStr = [NSString stringWithFormat:@"/shop/order/pay/%@",orderId];
    } else {
        self.urlStr = [NSString stringWithFormat:@"/shop/order/pay/%@?recAddressId=%@",orderId,addressId];
    }
    self.requestType = YTKRequestMethodPUT;
    
    [self sendRequest_completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        if (completion) {
            completion(isSuccess, request);
        }
    }];
    
}


@end
