//
//  QJMallOrderDetailModel.m
//  QJBox
//
//  Created by macm on 2022/9/21.
//

#import "QJMallOrderDetailModel.h"


@implementation QJMallOrderDetailDeliveryModel
+ (NSDictionary *)modelCustomPropertyMapper{
    return @{@"idStr":@"id"};
}
@end

@implementation QJMallOrderDetailAdderssModel
+ (NSDictionary *)modelCustomPropertyMapper{
    return @{@"idStr":@"id"};
}
@end

@implementation QJMallOrderDetailTraceModel
+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{
             @"traces" : [QJMallOrderTraceModel class],
    };
}
@end

@implementation QJMallOrderDetailModel
+ (NSDictionary *)modelCustomPropertyMapper{
    return @{@"idStr":@"id"};
}
+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{
             @"items" : [QJMallOrderItemModel class],
    };
}
@end
