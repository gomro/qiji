//
//  QJMallOrderModel.m
//  QJBox
//
//  Created by macm on 2022/9/21.
//

#import "QJMallOrderModel.h"

@implementation QJMallOrderButtonModel

@end

@implementation QJMallOrderItemPropertieModel

@end

@implementation QJMallOrderItemModel
+ (NSDictionary *)modelCustomPropertyMapper{
    return @{@"idStr":@"id"};
}
+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"properties" : [QJMallOrderItemPropertieModel class],
    };
}
@end

@implementation QJMallOrderTraceModel
+ (NSDictionary *)modelCustomPropertyMapper{
    return @{@"desc":@"description"};
}
@end

@implementation QJMallOrderModel
+ (NSDictionary *)modelCustomPropertyMapper{
    return @{@"idStr":@"id"};
}
+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"buttons" : [QJMallOrderButtonModel class],
             @"items" : [QJMallOrderItemModel class],
             @"traces" : [QJMallOrderTraceModel class],
    };
}
@end





