//
//  QJMallOrderModel.h
//  QJBox
//
//  Created by macm on 2022/9/21.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJMallOrderButtonModel : NSObject
@property (nonatomic, copy) NSString *button;
//2-查看物流： 实物 + 待发货及之后
//3-确定收货： 实物 + 待收货
//4-查看卡券： 电子卡
//5-去评价: 实物 + 待评价
//6-查看评价： 实物 + 已完成
//7-删除订单： 已完成、已取消,可用值:DO_COMMENT,DO_DELETE,DO_DELIVERY,DO_RECEIVE,VIEW_CARD,VIEW_COMMENT,VIEW_TRACE    string
@property (nonatomic, copy) NSString *desc; //        string
@end

@interface QJMallOrderItemPropertieModel : NSObject
@property (nonatomic, copy) NSString *name; //    属性名    string
@property (nonatomic, copy) NSString *value; //    属性值    string
@end

@interface QJMallOrderItemModel : NSObject
@property (nonatomic, copy) NSString *idStr; //        string
@property (nonatomic, copy) NSString *moneyPartPrice; //    金额部分售价    number
@property (nonatomic, copy) NSString *name; //    商品名称    string
@property (nonatomic, copy) NSString *picture; //    商品图片    string
@property (nonatomic, copy) NSString *qjcoinPartPrice; //    商品支付奇迹币数    integer
@property (nonatomic, copy) NSString *quantity; //    商品数量    integer
@property (nonatomic, strong) NSArray *properties; //    SKU属性信息    array    ShopSkuProperty
@property (nonatomic, strong) NSDictionary *commentVo; // 我的评价
@end

@interface QJMallOrderTraceModel : NSObject
@property (nonatomic, copy) NSString *desc; //    发生详情    string
@property (nonatomic, copy) NSString *state; //    流转状态,可用值:DELIVERY,DISPATCH,PAY,PICK_UP,SIGN_FOR,SUBMIT,TRANS    string
@property (nonatomic, copy) NSString *time; //    发生时间    string
@end

@interface QJMallOrderModel : NSObject
@property (nonatomic, strong) NSArray *buttons; //    展示的按钮    array    按钮

@property (nonatomic, assign) BOOL commented; //    是否有评价（实物+已完成：是否做出过评价，true时展示按钮'查看评价'）    boolean
@property (nonatomic, copy) NSString *createTime; //    订单创建时间    string
@property (nonatomic, copy) NSString *idStr; //   订单id    string
@property (nonatomic, strong) NSArray *items; //    订单明细    array    订单明细

@property (nonatomic, copy) NSString *moneyPaid; //    金额实付    number
@property (nonatomic, copy) NSString *moneyTotal; //    金额原价    number
@property (nonatomic, copy) NSString *productType; //    类型: 实物商品=1, 电子卡=2, 优惠券=3,可用值:ECARD,PHYSICAL_PRODUCT    string
@property (nonatomic, copy) NSString *qjcoinPaid; //    奇迹币实付    integer
@property (nonatomic, copy) NSString *qjcoinTotal; //    奇迹币原价    integer
@property (nonatomic, copy) NSString *sn; //    订单编号    string
@property (nonatomic, copy) NSString *status; //    订单状态，1-待发货、2-待收货、3-待评价、4-已完成、5-已取消,可用值:CANCEL,FINISH,UN_COMMENT,UN_DELIVERY,UN_REC    string
@property (nonatomic, copy) NSString *statusMsg; //    订单状态，1-待发货、2-待收货、3-待评价、4-已完成、5-已取消    string
@property (nonatomic, strong) NSArray *traces; //    物流信息    array    物流跟踪

@end

NS_ASSUME_NONNULL_END
