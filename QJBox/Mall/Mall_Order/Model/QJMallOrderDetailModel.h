//
//  QJMallOrderDetailModel.h
//  QJBox
//
//  Created by macm on 2022/9/21.
//

#import <Foundation/Foundation.h>
#import "QJMallOrderModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJMallOrderDetailDeliveryModel : NSObject
@property (nonatomic, copy) NSString *aliType; //    阿里标志码    string
@property (nonatomic, copy) NSString *createTime; //        string
@property (nonatomic, copy) NSString *createrId; //        string
@property (nonatomic, copy) NSString *idStr; //        string
@property (nonatomic, copy) NSString *logo; //    公司logo    string
@property (nonatomic, copy) NSString *name; //    公司名称    string
@property (nonatomic, copy) NSString *phone; //    公司电话    string
@property (nonatomic, copy) NSString *site; //    公司主页    string
@property (nonatomic, copy) NSString *status; //    状态 0：正常 1:禁用,可用值:BANNED,NORMAL    string
@property (nonatomic, copy) NSString *updatedTime; //        string
@property (nonatomic, copy) NSString *updaterId; //        string

@end

@interface QJMallOrderDetailAdderssModel : NSObject
@property (nonatomic, copy) NSString *area; //    区    string
@property (nonatomic, assign) NSInteger areaAreacode; //    区区域码    integer
@property (nonatomic, copy) NSString *city; //    城市    string
@property (nonatomic, assign) NSInteger cityeAreacode; //    城市区域码    integer
@property (nonatomic, copy) NSString *createTime; //        string
@property (nonatomic, copy) NSString *detail; //    详细地址    string
@property (nonatomic, copy) NSString *idStr; //       string
@property (nonatomic, assign) BOOL isDefault; //    是否默认地址，1是0否，默认0    boolean
@property (nonatomic, copy) NSString *linkman; //    联系人    string
@property (nonatomic, copy) NSString *mobile; //    联系方式    string
@property (nonatomic, copy) NSString *province; //    省份    string
@property (nonatomic, assign) NSInteger provinceAreacode; //    省份区域码    integer
@property (nonatomic, copy) NSString *street; //    街道或村镇    string
@property (nonatomic, assign) NSInteger streetAreacode; //    街道区域码    integer
@property (nonatomic, copy) NSString *updatedTime; //        string
@property (nonatomic, copy) NSString *userId; //    用户id    string
@property (nonatomic, assign) BOOL userVisable; //    用户可见    boolean
@end

@interface QJMallOrderDetailTraceModel : NSObject
@property (nonatomic, strong) QJMallOrderDetailDeliveryModel *delivery; //    物流公司信息    ShopDelivery对象    ShopDelivery对象
@property (nonatomic, strong) NSArray *traces; //    物流信息    array    物流跟踪
@property (nonatomic, strong) QJMallOrderDetailAdderssModel *userReceiveAddress; //    收货地址    UserReceiveAddress对象    UserReceiveAddress对象

@end

@interface QJMallOrderDetailModel : NSObject
@property (nonatomic, copy) NSString *deliveryTime; //    订单发货时间    string(date-time)
@property (nonatomic, copy) NSString *deliveryType; //    配送方式,0-无需配送，1-物流配送,   string
@property (nonatomic, copy) NSString *freightAmount; //   运费    number(bigdecimal)
@property (nonatomic, copy) NSString *overTime; //    订单完成时间    string(date-time)
@property (nonatomic, copy) NSString *payTime; //    订单付款时间    string(date-time)
@property (nonatomic, assign) BOOL commented; //    是否有评价（实物+已完成：是否做出过评价，true时展示按钮'查看评价'）    boolean
@property (nonatomic, copy) NSString *createTime; //    订单创建时间    string
@property (nonatomic, copy) NSString *idStr; //   订单id    string
@property (nonatomic, strong) NSArray *items; //    订单明细    array    订单明细
@property (nonatomic, copy) NSString *moneyPaid; //    金额实付    number
@property (nonatomic, copy) NSString *moneyTotal; //    金额原价    number
@property (nonatomic, copy) NSString *productType; //    类型: 实物商品=1, 电子卡=2, 优惠券=3,可用值:ECARD,PHYSICAL_PRODUCT    string
@property (nonatomic, copy) NSString *qjcoinPaid; //    奇迹币实付    integer
@property (nonatomic, copy) NSString *qjcoinTotal; //    奇迹币原价    integer
@property (nonatomic, copy) NSString *sn; //    订单编号    string
@property (nonatomic, copy) NSString *status; //    订单状态，1-待发货、2-待收货、3-待评价、4-已完成、5-已取消,可用值:CANCEL,FINISH,UN_COMMENT,UN_DELIVERY,UN_REC    string
@property (nonatomic, copy) NSString *statusMsg; //    订单状态，1-待发货、2-待收货、3-待评价、4-已完成、5-已取消    string
@property (nonatomic, strong) QJMallOrderDetailTraceModel *traces; //    物流信息        物流跟踪

@end

NS_ASSUME_NONNULL_END
