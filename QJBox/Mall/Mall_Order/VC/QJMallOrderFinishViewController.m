//
//  QJMallOrderFinishViewController.m
//  QJBox
//
//  Created by macm on 2022/8/25.
//

#import "QJMallOrderFinishViewController.h"
#import "QJMallShopEvaluateViewController.h"

@interface QJMallOrderFinishViewController ()
@property (nonatomic, strong) UIImageView *finishImageView;
@property (nonatomic, strong) UILabel *tipLabel;
@property (nonatomic, strong) UIButton *shopButton;
@property (nonatomic, strong) UIButton *commentButton;
@end

@implementation QJMallOrderFinishViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navBar.backgroundColor = [UIColor clearColor];
    [self createView];
}

- (void)createView {
    [self.view addSubview:self.finishImageView];
    [self.finishImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view);
        make.width.equalTo(@(240*kWScale));
        make.height.equalTo(@(240*kWScale));
        make.top.equalTo(self.view).offset(NavigationBar_Bottom_Y+72);
    }];
    
    [self.view addSubview:self.tipLabel];
    [self.tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view);
        make.height.mas_equalTo(28*kWScale);
        make.top.equalTo(self.finishImageView.mas_bottom).offset(-65);
    }];
    
    [self.view addSubview:self.shopButton];
    [self.shopButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.tipLabel.mas_left).mas_offset(10);
        make.top.mas_equalTo(self.tipLabel.mas_bottom).mas_offset(50);
        make.width.mas_equalTo(78*kWScale);
        make.height.mas_equalTo(28*kWScale);
    }];
    
    [self.view addSubview:self.commentButton];
    [self.commentButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.tipLabel.mas_right).mas_offset(-10);
        make.top.mas_equalTo(self.tipLabel.mas_bottom).mas_offset(50);
        make.width.mas_equalTo(78*kWScale);
        make.height.mas_equalTo(28*kWScale);
    }];
    
}

- (void)shopAction {
    [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:1] animated:YES];
}

- (void)commentAction {
    QJMallShopEvaluateViewController *vc = [[QJMallShopEvaluateViewController alloc] init];
    vc.idStr = self.idStr;
    [[QJAppTool getCurrentViewController].navigationController pushViewController:vc animated:YES];
    @weakify(self);
    vc.clickShopEvaluate = ^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
        if (self.clickOrderFinish) {
            self.clickOrderFinish();
        }
    };
}

- (UIImageView *)finishImageView {
    if (!_finishImageView) {
        _finishImageView = [[UIImageView alloc] init];
        _finishImageView.image = [UIImage imageNamed:@"empty_no_finish"];
    }
    return _finishImageView;
}

- (UILabel *)tipLabel {
    if (!_tipLabel) {
        _tipLabel = [[UILabel alloc] init];
        _tipLabel.text = @"交易成功";
        _tipLabel.textColor = UIColorFromRGB(0x000000);
        [_tipLabel setFont:FontSemibold(16)];
    }
    return _tipLabel;
}

- (UIButton *)shopButton {
    if (!_shopButton) {
        _shopButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _shopButton.layer.borderWidth = 1;
        _shopButton.layer.borderColor = kColorOrange.CGColor;
        _shopButton.layer.cornerRadius = 14;

        [_shopButton setTitle:@"返回商城" forState:UIControlStateNormal];
        [_shopButton setTitleColor:kColorOrange forState:UIControlStateNormal];
        [_shopButton.titleLabel setFont:FontMedium(12)];
        [_shopButton addTarget:self action:@selector(shopAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _shopButton;
}

- (UIButton *)commentButton {
    if (!_commentButton) {
        _commentButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _commentButton.layer.borderWidth = 1;
        _commentButton.layer.borderColor = kColorOrange.CGColor;
        _commentButton.layer.cornerRadius = 14;

        [_commentButton setTitle:@"立即评价" forState:UIControlStateNormal];
        [_commentButton setTitleColor:kColorOrange forState:UIControlStateNormal];
        [_commentButton.titleLabel setFont:FontMedium(12)];
        [_commentButton addTarget:self action:@selector(commentAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _commentButton;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
