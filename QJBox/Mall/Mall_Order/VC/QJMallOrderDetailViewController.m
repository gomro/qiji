//
//  QJMallOrderDetailViewController.m
//  QJBox
//
//  Created by macm on 2022/8/24.
//

#import "QJMallOrderDetailViewController.h"
#import "QJMallOrderDetailLogisticsTableViewCell.h"
#import "QJMallOrderDetailListTableViewCell.h"
#import "QJMallOrderDetailGoodsTableViewCell.h"
#import "QJMallOrderDetailPriceTableViewCell.h"
#import "QJMallOrderLogisticsDetailView.h"
#import "QJMallOrderConfirmReceiptView.h"
#import "QJMallOrderFinishViewController.h"
#import "QJMallOrderRequest.h"
#import "QJMallOrderDetailModel.h"
#import "QJMallShopEvaluateViewController.h"
#import "QJMallCardConvertViewController.h"
#import "QJMallCommentViewCell.h"
#import "QJMallCommentVideoVC.h"

@interface QJMallOrderDetailViewController ()<UITableViewDelegate,UITableViewDataSource,QJMallCommentViewCellDelegate>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, strong) UIView *bottomView;
@property (nonatomic, strong) UIButton *leftButton;
@property (nonatomic, strong) UIButton *rightButton;
@property (nonatomic, strong) QJMallOrderDetailModel *model;
@property (nonatomic, strong) QJMallCommentModel *commentModel;
@property (nonatomic, strong) NSArray *itemArray;
@end

@implementation QJMallOrderDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    [self createView];
    [self sendRequest];
}

- (void)sendRequest {
    [QJAppTool showHUDLoading];
    @weakify(self);
    QJMallOrderRequest *request = [[QJMallOrderRequest alloc] init];
    [request requestShopOrderDetailOrderId:self.idStr completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        @strongify(self);
        [QJAppTool hideHUDLoading];
        
        if (isSuccess) {
            self.model = [QJMallOrderDetailModel modelWithDictionary:request.responseJSONObject[@"data"]];

            QJMallOrderItemModel *item = self.model.items.firstObject;
            self.commentModel = [QJMallCommentModel QJMallCommentWithDictionary:item.commentVo];
            self.commentModel.isMineComment = YES;
            
            [self configItemArray];
            
            [self reloadDetailView];
            
        }

    }];
    
}

- (void)configItemArray {
    if ([self.model.productType isEqualToString:@"1"]) {
        if ([self.model.status isEqualToString:@"1"]) {
            self.itemArray = @[@{@"订单编号:":self.model.idStr},@{@"创建时间:":self.model.createTime},@{@"商品总额:":[NSString stringWithFormat:@"%@奇迹币",self.model.qjcoinTotal]},@{@"运费:":[NSString stringWithFormat:@"￥%@",self.model.freightAmount]}];
        } else {
            self.itemArray = @[@{@"订单编号:":self.model.idStr},@{@"创建时间:":self.model.createTime},@{@"发货时间:":self.model.deliveryTime},@{@"配送方式:":[self getDeliveryType:self.model.deliveryType.integerValue]},@{@"商品总额:":[NSString stringWithFormat:@"%@奇迹币",self.model.qjcoinTotal]},@{@"运费:":[NSString stringWithFormat:@"￥%@",self.model.freightAmount]}];
        }
    } else {
        if ([self.model.status isEqualToString:@"1"] || [self.model.status isEqualToString:@"2"] || [self.model.status isEqualToString:@"3"]) {
            self.itemArray = @[@{@"订单编号:":self.model.idStr},@{@"创建时间:":self.model.createTime},@{@"商品总额:":[NSString stringWithFormat:@"%@奇迹币",self.model.qjcoinTotal]}];
        } else {
            self.itemArray = @[@{@"订单编号:":self.model.idStr},@{@"创建时间:":self.model.createTime},@{@"完成时间:":self.model.overTime},@{@"商品总额:":[NSString stringWithFormat:@"%@奇迹币",self.model.qjcoinTotal]}];
        }
    }
    
}

- (void)reloadDetailView {
    
    [self.tableView reloadData];
    
    self.title = [self getStatusString:self.model.status.integerValue];
    
    if ([self.model.productType isEqualToString:@"1"]) {
        if ([self.model.status isEqualToString:@"1"]) {
            [self.bottomView setHidden:YES];
        } else if ([self.model.status isEqualToString:@"2"]) {
            [self.bottomView setHidden:NO];
            [self.leftButton setHidden:YES];
            [self.rightButton setTitle:@"确认收货" forState:UIControlStateNormal];
        } else if ([self.model.status isEqualToString:@"3"]) {
            [self.bottomView setHidden:NO];
            [self.leftButton setHidden:YES];
            [self.rightButton setTitle:@"去评价" forState:UIControlStateNormal];
        } else {
            [self.bottomView setHidden:NO];
            [self.leftButton setHidden:YES];
            [self.rightButton setTitle:@"删除订单" forState:UIControlStateNormal];
            self.rightButton.layer.borderWidth = 1;
            self.rightButton.layer.borderColor = kColorOrange.CGColor;
            [self.rightButton setTitleColor:kColorOrange forState:UIControlStateNormal];
            [self.rightButton setBackgroundColor:[UIColor whiteColor]];
        }
    } else {
        if ([self.model.status isEqualToString:@"1"] || [self.model.status isEqualToString:@"2"] || [self.model.status isEqualToString:@"3"]) {
            [self.bottomView setHidden:NO];
            [self.leftButton setHidden:YES];
            [self.rightButton setTitle:@"查看卡券" forState:UIControlStateNormal];
        } else if ([self.model.status isEqualToString:@"4"] || [self.model.status isEqualToString:@"5"]) {
            [self.bottomView setHidden:NO];
            [self.leftButton setTitle:@"删除订单" forState:UIControlStateNormal];
            [self.rightButton setTitle:@"查看卡券" forState:UIControlStateNormal];
        } else {
            [self.bottomView setHidden:YES];
        }
    }
    
    
        
}

- (void)createView {
    self.navBar.backgroundColor = [UIColor clearColor];

    [self.view addSubview:self.bottomView];
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(0);
        make.height.mas_equalTo(72*kWScale+Bottom_iPhoneX_SPACE);
    }];
    
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(NavigationBar_Bottom_Y);
        make.left.right.mas_equalTo(0);
        make.bottom.mas_equalTo(self.bottomView.mas_top);
    }];
    
    [self.bottomView addSubview:self.rightButton];
    [self.rightButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-16);
        make.top.mas_equalTo(16);
        make.width.mas_equalTo(106*kWScale);
        make.height.mas_equalTo(40*kWScale);
    }];
    
    [self.bottomView addSubview:self.leftButton];
    [self.leftButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.rightButton.mas_left).mas_offset(-16);
        make.top.mas_equalTo(16);
        make.width.mas_equalTo(106*kWScale);
        make.height.mas_equalTo(40*kWScale);
    }];
    
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0 && (self.model.traces.traces.count == 0 || self.model.status.integerValue == 1)) {
        return 0;
    }
    if (section == 2) {
        return self.itemArray.count;
    }
    if (section == 4) {
        return self.model.commented ? 1 : 0;
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        QJMallOrderDetailLogisticsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QJMallOrderDetailLogisticsTableViewCell" forIndexPath:indexPath];
        cell.model = self.model;
        return cell;
    } else if (indexPath.section == 1) {
        QJMallOrderDetailGoodsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QJMallOrderDetailGoodsTableViewCell" forIndexPath:indexPath];
        cell.model = self.model.items.firstObject;
        return cell;
    } else if (indexPath.section == 3) {
        QJMallOrderDetailPriceTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QJMallOrderDetailPriceTableViewCell" forIndexPath:indexPath];
        cell.priceLabel.text = [NSString stringWithFormat:@"实付金额: %@奇迹币",self.model.qjcoinPaid];
        return cell;
    } else if (indexPath.section == 4) {
        NSString *cellMallComment = @"cellMallCommentId";
        QJMallCommentStyle style = QJMallCommentStyleOneImage;
        style = self.commentModel.commentStyle;
        cellMallComment = [NSString stringWithFormat:@"cellMallCommentId%ld", style];
        QJMallCommentViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellMallComment];
        if (!cell) {
            cell = [[QJMallCommentViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellMallComment cellStyle:style];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cell.delegate = self;
        [cell configureCellWithData:self.commentModel];
        return cell;
    } else {
        QJMallOrderDetailListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QJMallOrderDetailListTableViewCell" forIndexPath:indexPath];
        NSDictionary *dic = self.itemArray[indexPath.row];
        cell.leftLabel.text = dic.allKeys.firstObject;
        cell.rightLabel.text = dic.allValues.firstObject;
        return cell;
    }

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        QJMallOrderLogisticsDetailView *detailView = [[QJMallOrderLogisticsDetailView alloc] initWithTitles:self.model.traces orderId:nil];
        [detailView show];
    }

}

#pragma mark - QJMallCommentViewCellDelegate
- (void)didShowReloadModel:(QJMallCommentModel *)model {
    [self.tableView reloadRow:0 inSection:4 withRowAnimation:UITableViewRowAnimationFade];
}

- (void)didVideoImageUrl:(NSString *)videoStr {
    NSURL *videoUrl = [NSURL URLWithString:videoStr];

    QJMallCommentVideoVC *videoPlayer = [[QJMallCommentVideoVC alloc] init];
    videoPlayer.videoUrl = videoUrl;
    [self.navigationController pushViewController:videoPlayer animated:YES];
}

// 底部右侧按钮点击事件
- (void)rightAction:(UIButton *)sender {
    
    if ([sender.titleLabel.text isEqualToString:@"确认收货"]) {
        QJMallOrderItemModel *item = self.model.items.firstObject;
        QJMallOrderConfirmReceiptView *view = [[QJMallOrderConfirmReceiptView alloc] initWithOrderId:self.model.idStr itemImage:item.picture];
        @weakify(self);
        view.clickConfirmButton = ^{
            @strongify(self);

            self.model.status = @"3";
            [self reloadDetailView];
            
            QJMallOrderFinishViewController *vc = [[QJMallOrderFinishViewController alloc] init];
            [[QJAppTool getCurrentViewController].navigationController pushViewController:vc animated:YES];
            @weakify(self);
            vc.clickOrderFinish = ^{
                @strongify(self);

                self.model.status = @"4";
                [self reloadDetailView];

            };
        };
        [view show];
    }
    if ([sender.titleLabel.text isEqualToString:@"去评价"]) {
        QJMallShopEvaluateViewController *vc = [[QJMallShopEvaluateViewController alloc] init];
        QJMallOrderItemModel *item = self.model.items.firstObject;
        vc.idStr = item.idStr;
        [[QJAppTool getCurrentViewController].navigationController pushViewController:vc animated:YES];
        @weakify(self);
        vc.clickShopEvaluate = ^{
            @strongify(self);

            self.model.status = @"4";
            [self reloadDetailView];

        };
    }
    if ([sender.titleLabel.text isEqualToString:@"删除订单"]) {
        [self deleteOrderAction];
    }
    if ([sender.titleLabel.text isEqualToString:@"查看卡券"]) {
        QJMallCardConvertViewController *vc = [[QJMallCardConvertViewController alloc] init];
        vc.idStr = self.model.idStr;
        [[QJAppTool getCurrentViewController].navigationController pushViewController:vc animated:YES];
    }
    
}

- (void)leftAction:(UIButton *)sender {
    [self deleteOrderAction];
}

- (void)deleteOrderAction {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:@"确认要删除订单吗?" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *nextAction = [UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self sendRequestDelete];
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDestructive handler:nil];
    
    [alert addAction:cancelAction];
    [alert addAction:nextAction];
    [[QJAppTool getCurrentViewController] presentViewController:alert animated:YES completion:nil];
    [cancelAction setValue:UIColorFromRGB(0x919599) forKey:@"titleTextColor"];
    [nextAction setValue:UIColorFromRGB(0x007aff) forKey:@"titleTextColor"];
}

- (void)sendRequestDelete {
    [QJAppTool showHUDLoading];

    @weakify(self);
    QJMallOrderRequest *request = [[QJMallOrderRequest alloc] init];
    [request requestShopOrderDeleteOrderId:self.model.idStr completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        @strongify(self);
        
        [QJAppTool hideHUDLoading];
        if (isSuccess) {
            [self.navigationController popViewControllerAnimated:YES];
        }

    }];
}

- (UITableView *)tableView {
    if (!_tableView) {
        self.tableView = [[UITableView alloc] init];
        self.tableView.backgroundColor = [UIColor clearColor];
        self.tableView.dataSource = self;
        self.tableView.delegate = self;
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.tableView.showsVerticalScrollIndicator = NO;
        self.tableView.showsHorizontalScrollIndicator = NO;
        [self.tableView registerClass:[QJMallOrderDetailLogisticsTableViewCell class] forCellReuseIdentifier:@"QJMallOrderDetailLogisticsTableViewCell"];
        [self.tableView registerClass:[QJMallOrderDetailGoodsTableViewCell class] forCellReuseIdentifier:@"QJMallOrderDetailGoodsTableViewCell"];
        [self.tableView registerClass:[QJMallOrderDetailListTableViewCell class] forCellReuseIdentifier:@"QJMallOrderDetailListTableViewCell"];
        [self.tableView registerClass:[QJMallOrderDetailPriceTableViewCell class] forCellReuseIdentifier:@"QJMallOrderDetailPriceTableViewCell"];
//        [self.tableView registerClass:[QJMallCommentViewCell class] forCellReuseIdentifier:@"QJMallCommentViewCell"];

        if (@available(iOS 11.0, *)) {
            self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
        self.tableView.estimatedRowHeight = 0.01;
 
    }
    return _tableView;
}

- (UIView *)bottomView {
    if (!_bottomView) {
        _bottomView = [[UIView alloc] init];
        _bottomView.backgroundColor = [UIColor whiteColor];
        [_bottomView setHidden:YES];
    }
    return _bottomView;
}

- (UIButton *)rightButton {
    if (!_rightButton) {
        _rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _rightButton.layer.cornerRadius = 20;
        [_rightButton setBackgroundColor:kColorOrange];
        
        [_rightButton setTitle:@"确认收货" forState:UIControlStateNormal];
        [_rightButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_rightButton.titleLabel setFont:FontSemibold(14)];
        [_rightButton addTarget:self action:@selector(rightAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _rightButton;
}

- (UIButton *)leftButton {
    if (!_leftButton) {
        _leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _leftButton.layer.borderWidth = 1;
        _leftButton.layer.borderColor = kColorOrange.CGColor;
        _leftButton.layer.cornerRadius = 20;

        [_leftButton setTitle:@"删除订单" forState:UIControlStateNormal];
        [_leftButton setTitleColor:kColorOrange forState:UIControlStateNormal];
        [_leftButton.titleLabel setFont:FontSemibold(14)];
        [_leftButton addTarget:self action:@selector(leftAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _leftButton;
}

- (NSString *)getStatusString:(NSInteger)status {
    switch (status) {
        case 1:
            return @"等待发货";
            break;
        case 2:
            return @"等待收货";
            break;
        case 3:
            return @"待评价";
            break;
        case 4:
            return @"已完成";
            break;
        case 5:
            return @"已取消";
            break;
            
        default:
            return @"";
            break;
    }
}

- (NSString *)getDeliveryType:(NSInteger)type {
    switch (type) {
        case 0:
            return @"无需配送";
            break;
        case 1:
            return @"物流配送";
            break;
            
        default:
            return @"";
            break;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
