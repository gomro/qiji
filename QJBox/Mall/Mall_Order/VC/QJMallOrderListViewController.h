//
//  QJMallOrderListViewController.h
//  QJBox
//
//  Created by macm on 2022/8/24.
//

#import "QJBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJMallOrderListViewController : QJBaseViewController
@property (nonatomic, copy) NSString *status;
@end

NS_ASSUME_NONNULL_END
