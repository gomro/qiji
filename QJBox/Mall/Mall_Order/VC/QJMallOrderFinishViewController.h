//
//  QJMallOrderFinishViewController.h
//  QJBox
//
//  Created by macm on 2022/8/25.
//

#import "QJBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJMallOrderFinishViewController : QJBaseViewController
@property (nonatomic, copy) NSString *idStr;
@property (nonatomic, copy) void(^clickOrderFinish)(void);

@end

NS_ASSUME_NONNULL_END
