//
//  QJMallOrderListViewController.m
//  QJBox
//
//  Created by macm on 2022/8/24.
//

#import "QJMallOrderListViewController.h"
#import "QJSegmentLineView.h"
#import "QJMallOrderListPageView.h"

@interface QJMallOrderListViewController ()<UIScrollViewDelegate>

/* 切换视图 */
@property (nonatomic, strong) UIView *segmentView;
@property (nonatomic, strong) QJSegmentLineView *segment;

@property (nonatomic, strong) QJMallOrderListPageView *pageView;
@end

@implementation QJMallOrderListViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.pageView reloadPageView:self.status];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"订单管理";
    if (self.status.length == 0) {
        self.status = @"";
    } else {
        [self.segment setSegmentSelectedIndex:self.status.integerValue];
    }
    [self createView];
}

- (void)createView {
    self.navBar.backgroundColor = [UIColor clearColor];
    
    [self.view addSubview:self.segmentView];
    [self.segmentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(NavigationBar_Bottom_Y);
        make.left.right.equalTo(self.view);
        make.height.equalTo(@(50*kWScale));
    }];
    
    [self.segmentView addSubview:self.segment];
    [self.segment mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.segmentView);
        make.left.equalTo(self.segmentView);
        make.height.equalTo(@(50*kWScale));
        make.width.mas_equalTo(kScreenWidth);
    }];
    
    [self.view addSubview:self.pageView];
    [self.pageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.segmentView.mas_bottom);
        make.bottom.equalTo(self.view);
        make.left.right.equalTo(self.view);
    }];
}

#pragma mark - Lazy Loading

- (UIView *)segmentView{
    if (!_segmentView) {
        _segmentView = [UIView new];
        _segmentView.backgroundColor = [UIColor clearColor];
    }
    return _segmentView;
}

- (QJSegmentLineView *)segment{
    if (!_segment) {
        _segment = [[QJSegmentLineView alloc] initWithFrame:CGRectMake(10, NavigationBar_Bottom_Y, kScreenWidth, 50*kWScale)];
        _segment.backgroundColor = [UIColor clearColor];
        _segment.segmentWidthStyle = QJSegmentViewWidthStyleEqual;
        _segment.itemWidth = 65*kWScale;
        _segment.font = kFont(16);
        _segment.selectedFont = kboldFont(18);
        _segment.textColor = UIColorFromRGB(0x000000);
        _segment.selectedTextColor = UIColorFromRGB(0x000000);
        _segment.showBottomLine = NO;
        _segment.tag = 10000;
        [_segment setTitleArray:@[@"全部",@"待发货",@"待收货",@"待评价",@"已完成"]];
        WS(weakSelf)
        [_segment setSegmentedItemSelectedBlock:^(QJSegmentLineView * _Nonnull segment, NSInteger selectedIndex) {
                    
            if (selectedIndex > 0) {
                weakSelf.status = [NSString stringWithFormat:@"%ld", selectedIndex];
            } else {
                weakSelf.status = @"";
            }
            [weakSelf.pageView reloadPageView:weakSelf.status];
            [weakSelf.pageView.delButton setHidden:YES];
        }];
    }
    return _segment;
}

- (QJMallOrderListPageView *)pageView {
    if (!_pageView) {
        _pageView = [[QJMallOrderListPageView alloc] init];
    }
    return _pageView;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
