//
//  QJMallOrderDetailViewController.h
//  QJBox
//
//  Created by macm on 2022/8/24.
//

#import "QJBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJMallOrderDetailViewController : QJBaseViewController
@property (nonatomic, copy) NSString *idStr;
@end

NS_ASSUME_NONNULL_END
