//
//  AppDelegate.m
//  QJBox
//
//  Created by wxy on 2022/5/31.
//

#import "AppDelegate.h"
#import "AppDelegate+QJ.h"
#import "XHLaunchAd.h"
#import "QJStartAPPRequest.h"
#import "QJAdvertisementModel.h"
#import <SJBaseVideoPlayer/SJRotationManager.h>

@interface AppDelegate ()<XHLaunchAdDelegate>
@property (nonatomic, strong) NSMutableArray *advertisementArray;

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [self initWindow];

    [self initNetWork];
    
    [self initLaunchImage];
    
    [self initThirdItem];
    

    [self setAdvertisementView];

    return YES;
}

#pragma mark - 设置广告
- (NSMutableArray *)advertisementArray {
    if (!_advertisementArray) {
        self.advertisementArray = [NSMutableArray arrayWithCapacity:0];
    }
    return _advertisementArray;
}

- (void)setAdvertisementView {
    [XHLaunchAd setLaunchSourceType:SourceTypeLaunchScreen];
    [XHLaunchAd setWaitDataDuration:2];
    
    QJStartAPPRequest *startRequest = [[QJStartAPPRequest alloc] init];
    [startRequest netWorkGetAdvertisement];
    [startRequest setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSInteger timers = 0;
        if (ResponseSuccess) {
            for (NSDictionary *dic in EncodeArrayFromDic(request.responseObject, @"data")) {
                QJAdvertisementModel *model = [QJAdvertisementModel modelWithDictionary:dic];
                if(![kCheckStringNil(model.imgUrl) isEqualToString:@""]) {
                    timers = model.waitTime + timers;
                    if (self.advertisementArray.count != 2) {
                        [self.advertisementArray addObject:model];
                    }
                }             
            }
        }

        if (self.advertisementArray.count) {
            QJAdvertisementModel *model = self.advertisementArray[0];
            [self showAdvertisementWithModel:model durationTime:timers];
        }
    }];
}

- (void)showAdvertisementWithModel:(QJAdvertisementModel *)model durationTime:(NSInteger)durationTime {
    XHLaunchImageAdConfiguration *imageAdconfiguration = [
        XHLaunchImageAdConfiguration new];
    //广告停留时间
    imageAdconfiguration.duration = durationTime;
    //广告frame
    imageAdconfiguration.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    //广告图片URLString/或本地图片名(.jpg/.gif请带上后缀)
    //注意本地广告图片,直接放在工程目录,不要放在Assets里面,否则不识别,此处涉及到内存优化
    NSString *urlStr = [model.imgUrl checkImageUrlString];
    imageAdconfiguration.imageNameOrURLString = urlStr;
    //设置GIF动图是否只循环播放一次(仅对动图设置有效)
    imageAdconfiguration.GIFImageCycleOnce = NO;
    //缓存机制(仅对网络图片有效)
    //为告展示效果更好,可设置为XHLaunchAdImageCacheInBackground,先缓存,下次显示
    imageAdconfiguration.imageOption = XHLaunchAdImageCacheInBackground;
    //图片填充模式
    imageAdconfiguration.contentMode = UIViewContentModeScaleAspectFill;
    //广告点击打开页面参数(openModel可为NSString,模型,字典等任意类型)
//    imageAdconfiguration.openModel = kCheckStringNil(model.h5Url);
    //广告显示完成动画
    imageAdconfiguration.showFinishAnimate = ShowFinishAnimateLite;
    //广告显示完成动画时间
    imageAdconfiguration.showFinishAnimateTime = 0.8;
    //跳过按钮类型
//    imageAdconfiguration.skipButtonType = SkipTypeRoundText;
    imageAdconfiguration.customSkipView = [self customSkipView];
    //后台返回时,是否显示广告
    imageAdconfiguration.showEnterForeground = NO;
    //显示开屏广告
    [XHLaunchAd imageAdWithImageAdConfiguration:imageAdconfiguration delegate:self];
}

#pragma mark - XHLaunchAdDelegate
- (void)xhLaunchAd:(XHLaunchAd *)launchAd customSkipView:(UIView *)customSkipView duration:(NSInteger)duration {
    if (self.advertisementArray.count == 2) {
        QJAdvertisementModel *modelFirst = self.advertisementArray[0];
        if (duration == modelFirst.waitTime && !modelFirst.isFinish) {
            modelFirst.isFinish = YES;
            QJAdvertisementModel *modelSecond = self.advertisementArray[1];
            [self showAdvertisementWithModel:modelSecond durationTime:modelSecond.waitTime - 1];
        }
    }
}

- (BOOL)xhLaunchAd:(XHLaunchAd *)launchAd clickAtOpenModel:(id)openModel clickPoint:(CGPoint)clickPoint {
//    DLog(@"点击--%@", openModel);
    return NO;
}

//自定义跳过按钮
- (UIView *)customSkipView{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.backgroundColor = RGBAlpha(0, 0, 0, 0.6);
    button.layer.cornerRadius = 2;
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button setTitle:@"跳过" forState:UIControlStateNormal];
    button.titleLabel.font = kFont(14);
    CGFloat tabbH = [QJDeviceConstant sysStatusBarHeight];
    if(!tabbH) {
        tabbH = 30;
    }
    CGFloat y = tabbH + 10;
    button.frame = CGRectMake([UIScreen mainScreen].bounds.size.width - 96, y, 72, 32);
    [button addTarget:self action:@selector(skipAction) forControlEvents:UIControlEventTouchUpInside];
    return button;
}

//跳过按钮点击事件
- (void)skipAction{
    //移除广告
    [XHLaunchAd removeAndAnimated:YES];
}

- (UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window {
    return [SJRotationManager supportedInterfaceOrientationsForWindow:window];

}
@end
