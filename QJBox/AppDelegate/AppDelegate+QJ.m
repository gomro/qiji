//
//  AppDelegate+QJ.m
//  QJBox
//
//  Created by wxy on 2022/6/1.
//

#import "AppDelegate+QJ.h"
#import "LLDynamicLaunchScreen.h"
#import "QJStartAPPRequest.h"
#import "XHLaunchAdCache.h"
#import "XHLaunchAd.h"
#import "QJMineRequest.h"

#import <AMapFoundationKit/AMapFoundationKit.h>

#define QJATAUTHSDKINFO @"z2kxM34oPKId8URsRJGIsGdfReu9EZWWE3wTkYGkrKwKSWB8TGDbTZ5AMyKHN5FxNEHEaEU2SxAHn5SSzN2GvSCpuQl1FYRzia+WWplrsCbN1S8Wv5MRFm1qCovRuc5wG71yR1DQ2FKPnYB52ZK3V6hpKEUOmJ4qsjyCsHgKPZOEcTgF4S7gy5HyaZFQQhf2Yh31lvwQL64rkndx2JTe5DHowlGZx3ddancA7NiASqcEFUMEACTX8ulmwgga3MfsdQC/C7112zo="//阿里云 一键登录秘钥信息
@implementation AppDelegate (QJ)


//初始化视图
- (void)initWindow {
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    QJAppTabBarController *tabController = [[QJAppTabBarController alloc] init];
    self.window.rootViewController = tabController;
    
    //active
    [self.window makeKeyAndVisible];
    //适配iOS15 默认给 plain 的 tableview 添加 sectionheader 的 padding
    if (@available(iOS 15.0, *)) {
        [UITableView appearance].sectionHeaderTopPadding = 0;
        [UITableView appearance].separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    NSString * token = LSUserDefaultsGET(kQJToken);
    if (!IsStrEmpty(token)) {
        [QJUserManager shareManager].isLogin = YES;
        [QJUserManager shareManager].isNetLogout = NO;//登录成功状态改变
    }
    NSNumber *isReal = LSUserDefaultsGET(kQJIsRealName);
    if (isReal.boolValue) {
        [QJUserManager shareManager].isVerifyState = YES;
    }
    NSString *userid= LSUserDefaultsGET(kQJUserId);
    if (!IsStrEmpty(userid)) {
        [QJUserManager shareManager].userID = userid;
    }
    //全局关闭系统分割线
    [UITableView appearance].separatorStyle = UITableViewCellSeparatorStyleNone;
}


//初始化网络配置
- (void)initNetWork {
    //
#if QJ_DEBUG_ENABLE
    NSDictionary *envDic = [[NSUserDefaults standardUserDefaults] objectForKey:QJ_ENV_CONFIGURATION];
    if ([[envDic allKeys] containsObject:@"envType"]) {
        NSInteger envType = [QJEncodeNumberFromDic(envDic, @"envType") integerValue];
        [QJEnvironmentConfigure shareInstance].currentEnvType = envType;
    }else{
        [QJEnvironmentConfigure shareInstance].currentEnvType = QJ_DEFAULT_ENV;//如果APP是联调环境默认测试
    }
    
#else
    [QJEnvironmentConfigure shareInstance].currentEnvType = QJ_DEFAULT_ENV;//如果是线上环境默认生产
    
    
#endif
    
}

//初始化第三方
- (void)initThirdItem {
    
    [AMapServices sharedServices].apiKey = @"0d055a1ed504bf133ae54ce3b1e224f0";
    [AMapServices sharedServices].enableHTTPS = YES;
    
    [TencentOAuth setIsUserAgreedAuthorization:YES];
    [QJQQAPIMANAGER setupSDK];
    [QJWXMANAGER setupSDK];
    
    //阿里云一键登录
    [[TXCommonHandler sharedInstance] setAuthSDKInfo:QJATAUTHSDKINFO
                                            complete:^(NSDictionary * _Nonnull resultDic) {
        DLog(@"设置秘钥结果：%@", resultDic);
    }];
    
    [IQKeyboardManager sharedManager].shouldResignOnTouchOutside = YES;
}


#pragma mark - UIApplicationDelegate
- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey, id> *)options{
    [QJQQAPIMANAGER handleOpenURL:url];
    [QJWXMANAGER handleOpenURL:url];
    return YES;
}

- (BOOL)application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void(^)(NSArray<id<UIUserActivityRestoring>> * __nullable restorableObjects))restorationHandler {
    [QJQQAPIMANAGER handleUniversalLink:userActivity];
    [QJWXMANAGER handleUniversalLink:userActivity];
    NSURL *url = userActivity.webpageURL;
    if([url.host isEqualToString:@"iosqjbox.sxmu.com"]){
        DLog(@"path:%@,query=%@",url.path,url.query);
        NSString *query = url.query;
        NSArray *arr = [query componentsSeparatedByString:@"&"];
        NSMutableDictionary *muDic = [NSMutableDictionary dictionary];
        for (NSString *str in arr) {
            NSArray *strArr = [str componentsSeparatedByString:@"="];
            [muDic setValue:[strArr safeObjectAtIndex:1]forKey:strArr.firstObject];
         }
        NSDictionary *targetDic = @{@"params":muDic?:@"",@"code":url.path?:@""};
        [[QJMediator sharedInstance] pushQJMediatorViewController:targetDic];
    }
    return  YES;
}



//初始化启动图
- (void)initLaunchImage {
    [self getBaseImageURL];
    sleep(2);
}

//获取图片服务器地址
- (void)getBaseImageURL {
    NSString *baseImage = LSUserDefaultsGET(@"QJBaseImageURL");
    if (!IsStrEmpty(baseImage)) {
        [QJEnvironmentConfigure shareInstance].baseImageURL = kCheckStringNil(baseImage);
        DLog(@"baseUrl = %@",[QJEnvironmentConfigure shareInstance].baseImageURL);
    }
    QJStartAPPRequest *startRequest = [[QJStartAPPRequest alloc] init];
    [startRequest netWorkGetImageAddress];
    [startRequest setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            NSDictionary *dataDic = EncodeDicFromDic(request.responseJSONObject, @"data");
            QJStartAPPModel *appModel = [QJStartAPPModel modelWithJSON:dataDic];
            
            // 获取服务器最新版本号，判断是否更新本地地址信息
            NSString *localCnareaVersion = LSUserDefaultsGET(@"QJCnareaConfigVersion");
            if (appModel.cnareaConfig.lastVersion > localCnareaVersion.integerValue) {
                [self requestGetCnareaConfigVersion:[NSString stringWithFormat:@"%ld",appModel.cnareaConfig.lastVersion]];
            }
            
            [QJUserManager shareManager].userPolicy = appModel.privacy.policy;
            [QJUserManager shareManager].userProtocol = appModel.privacy.protocol;
            NSString *imageUrl = appModel.config.img_info.base_url;
            if (!IsStrEmpty(imageUrl)) {
                [QJEnvironmentConfigure shareInstance].baseImageURL = imageUrl;
                
                LSUserDefaultsSET(kCheckStringNil(imageUrl), @"QJBaseImageURL");
                LSUserDefaultsSynchronize;
            }
        }
         
    
    }];
}

// 获取服务器最新地区数据
- (void)requestGetCnareaConfigVersion:(NSString *)version {
    QJStartAPPRequest *startRequest = [[QJStartAPPRequest alloc] init];
    [startRequest netWorkGetRecAddressTreeCnarea];
    [startRequest setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            
            NSDictionary *dataDic = EncodeDicFromDic(request.responseJSONObject, @"data");
            NSArray *array = dataDic[@"list"];
            
            NSMutableArray *provinceArray = [NSMutableArray array]; // 省
            NSMutableArray *cityArray = [NSMutableArray array]; // 市
            NSMutableArray *districtArray = [NSMutableArray array]; // 区
            NSMutableArray *streetArray = [NSMutableArray array]; // 街道

            for (NSDictionary *dic in array) {
                NSString *level = [NSString stringWithFormat:@"%@",dic[@"level"]];
                if (level.integerValue == 0) {
                    [provinceArray addObject:dic];
                }
                if (level.integerValue == 1) {
                    [cityArray addObject:dic];
                }
                if (level.integerValue == 2) {
                    [districtArray addObject:dic];
                }
                if (level.integerValue == 3) {
                    [streetArray addObject:dic];
                }
            }
            
            LSUserDefaultsSET(version, @"QJCnareaConfigVersion");
            
            LSUserDefaultsSET(provinceArray, @"QJCnAreaProvinceArray");
            LSUserDefaultsSET(cityArray, @"QJCnAreaCityArray");
            LSUserDefaultsSET(districtArray, @"QJCnAreaDistrictArray");
            LSUserDefaultsSET(streetArray, @"QJCnAreaStreetArray");

            LSUserDefaultsSynchronize;

        }
    
    }];
}

@end
