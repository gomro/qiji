//
//  AppDelegate.h
//  QJBox
//
//  Created by wxy on 2022/5/31.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

