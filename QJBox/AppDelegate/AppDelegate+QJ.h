//
//  AppDelegate+QJ.h
//  QJBox
//
//  Created by wxy on 2022/6/1.
//

#import "AppDelegate.h"

NS_ASSUME_NONNULL_BEGIN

@interface AppDelegate (QJ)

//初始化视图
- (void)initWindow;

//初始化网络配置
- (void)initNetWork;

//初始化启动图
- (void)initLaunchImage;

//初始化第三方
- (void)initThirdItem;
@end

NS_ASSUME_NONNULL_END
