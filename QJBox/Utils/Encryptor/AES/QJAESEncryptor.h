

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJAESEncryptor : NSObject

+ (NSString *)getAESRandomStringKey;

//加密
+ (NSData *)AES128EncryptWithData:(NSData *)data key:(NSString*)key;
//解密
+ (NSData *)AES128DecryptWithData:(NSData *)data key:(NSString*)key;

//字符串加密
+ (NSString *)AES128EncryptWithPlainText:(NSString *)plain key:(NSString*)key;
//字符串解密
+ (NSString *)AES128DecryptWithCiphertext:(NSString *)ciphertexts key:(NSString*)key;

@end

NS_ASSUME_NONNULL_END
