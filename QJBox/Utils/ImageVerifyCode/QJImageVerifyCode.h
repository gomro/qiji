//
//  QJImageVerifyCode.h
//  QJBox
//
//  Created by wxy on 2022/7/13.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol QJImageVerifyCodeDelegate <NSObject>


/// 完成验证之后的回调
/// @param result 验证结果 BOOL:YES/NO
/// @param validate 二次校验数据，如果验证结果为false，validate返回空
/// @param message 结果描述信息
- (void)QJImageVerifyCodeValidateFinish:(BOOL)result validate:(NSString *)validate message:(NSString *)message;

@end

/// 图片验证码
@interface QJImageVerifyCode : NSObject

@property (nonatomic, weak) id<QJImageVerifyCodeDelegate> delegate;

- (void)showView;


@end

NS_ASSUME_NONNULL_END
