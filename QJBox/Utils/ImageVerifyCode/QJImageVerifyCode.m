//
//  QJImageVerifyCode.m
//  QJBox
//
//  Created by wxy on 2022/7/13.
//

#import "QJImageVerifyCode.h"


@interface QJImageVerifyCode ()<NTESVerifyCodeManagerDelegate>

@property(nonatomic,strong) NTESVerifyCodeManager *manager;

@end


@implementation QJImageVerifyCode


- (void)showView {
    self.manager =  [NTESVerifyCodeManager getInstance];
    if (self.manager) {
        
        // 如果需要了解组件的执行情况,则实现回调
        self.manager.delegate = self;
        
        // captchaid的值是每个产品从后台生成的,比如 @""
        
        // 传统验证码
        NSString *captchaid = @"6f37eac888ba48369b2abdfd4950a9a6";
        self.manager.mode = NTESVerifyCodeNormal;
        
        NTESVerifyCodeStyleConfig *styleConfig = [[NTESVerifyCodeStyleConfig alloc] init];
        styleConfig.capBarTextAlign = NTESCapBarTextAlignCenter;
        styleConfig.capBarTextColor = @"#25D4D0";
        styleConfig.capBarTextSize = 15;
        styleConfig.capBarTextWeight = @"bold";
        styleConfig.borderColor = @"#25D4D0";
        [self.manager configureVerifyCode:captchaid timeout:7.0 styleConfig:styleConfig];
        
        // 设置语言
        self.manager.lang = NTESVerifyCodeLangCN;
        
        // 设置透明度
        self.manager.alpha = 0.3;
        
        self.manager.shouldCloseByTouchBackground = NO;
        
        // 设置颜色
        self.manager.color = [UIColor blackColor];
        
        // 设置frame
        self.manager.frame = CGRectNull;
        self.manager.protocol = NTESVerifyCodeProtocolHttps;
        
        // 是否开启降级方案
        self.manager.openFallBack = YES;
        self.manager.fallBackCount = 3;

        
        // 是否隐藏关闭按钮
        self.manager.closeButtonHidden = NO;
        

        // 显示验证码
        [self.manager openVerifyCodeView:nil customLoading:NO customErrorPage:NO];
    }
}


#pragma mark - NTESVerifyCodeManagerDelegate
/**
 * 验证码组件初始化完成
 */
- (void)verifyCodeInitFinish{
    
    DLog(@"收到初始化完成的回调");
}

/**
 * 验证码组件初始化出错
 *
 * @param error 错误信息
 */
- (void)verifyCodeInitFailed:(NSArray *)error {
    DLog(@"收到初始化失败的回调:%@",error);
    
}
/**
 * 完成验证之后的回调
 *
 * @param result 验证结果 BOOL:YES/NO
 * @param validate 二次校验数据，如果验证结果为false，validate返回空
 * @param message 结果描述信息
 *
 */
- (void)verifyCodeValidateFinish:(BOOL)result validate:(NSString *)validate message:(NSString *)message{
    NSLog(@"收到验证结果的回调:(%d,%@,%@)", result, validate, message);
    if (self.delegate && [self.delegate respondsToSelector:@selector(QJImageVerifyCodeValidateFinish:validate:message:)]) {
        [self.delegate QJImageVerifyCodeValidateFinish:result validate:validate message:message];
    }
}

/**
 * 关闭验证码窗口后的回调
 */
- (void)verifyCodeCloseWindow{
    //用户关闭验证后执行的方法
    NSLog(@"收到关闭验证码视图的回调");
    
}

- (void)verifyCodeCloseWindow:(NTESVerifyCodeClose)close {
    
}




@end
