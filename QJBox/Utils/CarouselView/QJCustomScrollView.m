//
//  QJCustomScrollView.m
//  QJBox
//
//  Created by rui on 2022/7/26.
//

#import "QJCustomScrollView.h"

#define  kWidth  ceilf(self.bounds.size.width) // 加ceil取上整，是为了保证scrollView.contentOffset.x能够更加准确的和kwidth作比较，例如在scrollViewDidScroll:方法中就有这样的比较，当外界进行了下拉放大时，kWidth可能是一个很长的浮点数，而scrollView.contentOffset.x直接依赖于kWidth会导致比较不准确，所以kWidth也取上整

#define  kHeight self.bounds.size.height

typedef NS_ENUM(NSInteger, QJCycleScrollViewImagesDataType){
    QJCycleScrollViewImagesDataTypeInLocal,// 本地图片标记
    QJCycleScrollViewImagesDataTypeInURL   // URL图片标记
};

@interface QJCustomScrollView () <UIScrollViewDelegate>

@property(strong, nonatomic) UIScrollView *scrollView;

// 前一个视图,当前视图,下一个视图
@property(strong, nonatomic) UIImageView *lastImgView;
@property(strong, nonatomic) UIImageView *currentImgView;
@property(strong, nonatomic) UIImageView *nextImgView;

// 图片来源(本地或URL)
@property(nonatomic) QJCycleScrollViewImagesDataType carouseImagesType;

@property(strong, nonatomic) NSTimer *timer;

// kImageCount = array.count,图片数组个数
@property(assign, nonatomic) NSInteger kImageCount;

// 记录nextImageView的下标 默认从1开始
@property(assign, nonatomic) NSInteger nextPhotoIndex;
// 记录lastImageView的下标 默认从 _kImageCount - 1 开始
@property(assign, nonatomic) NSInteger lastPhotoIndex;

@property (nonatomic, strong) NSOperationQueue *queue;

@end

static NSString *cache;

@implementation QJCustomScrollView

#pragma mark - 初始化方法
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        [self initialize];
    }
    return self;
}

- (void)initialize {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeStatus:) name:QJCircleScrollSyncNotification object:nil];

    cache = [[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject] stringByAppendingPathComponent:@"XLsn0wLoop"];
    BOOL isDir = NO;
    BOOL isExists = [[NSFileManager defaultManager] fileExistsAtPath:cache isDirectory:&isDir];
    if (!isExists || !isDir) {
        [[NSFileManager defaultManager] createDirectoryAtPath:cache withIntermediateDirectories:YES attributes:nil error:nil];
    }
}

- (void)changeStatus:(NSNotification *)notification{
    if ([[notification object] isKindOfClass:[NSString class]]) {
        NSString *notiStr = [notification object];
        if ([notiStr isEqualToString:@"YES"]) {
            [_scrollView setContentOffset:CGPointMake(kWidth*2, 0) animated:YES];
        }
        if ([notiStr isEqualToString:@"first"]) {
            _nextImgView.image = _currentImgView.image;
            _currentImgView.image = _lastImgView.image;
            // 将轮播图的偏移量设回中间位置
            _scrollView.contentOffset = CGPointMake(kWidth, 0);
            _lastImgView.image = nil;
            // 一定要小于等于，否则数组中只有一张图片时会出错
            if (_lastPhotoIndex <= 0) {
                _lastPhotoIndex = _kImageCount - 1;
                _nextPhotoIndex = _lastPhotoIndex - (_kImageCount - 2);
            } else {
                _lastPhotoIndex--;
                if (_nextPhotoIndex == 0) {
                    _nextPhotoIndex = _kImageCount - 1;
                } else {
                    _nextPhotoIndex--;
                }
            }
            [self setImageView:_lastImgView withSubscript:_lastPhotoIndex];
        }
        if ([notiStr isEqualToString:@"last"]) {
            _lastImgView.image = _currentImgView.image;
            _currentImgView.image = _nextImgView.image;
            _scrollView.contentOffset = CGPointMake(kWidth, 0);
            _nextImgView.image = nil;
            // 一定要是大于等于，否则数组中只有一张图片时会出错
            if (_nextPhotoIndex >= _kImageCount - 1 ) {
                _nextPhotoIndex = 0;
                _lastPhotoIndex = _nextPhotoIndex + (_kImageCount - 2);
            } else{
                _nextPhotoIndex++;
                if (_lastPhotoIndex == _kImageCount - 1) {
                    _lastPhotoIndex = 0;
                } else {
                    _lastPhotoIndex++;
                }
            }
            [self setImageView:_nextImgView withSubscript:_nextPhotoIndex];
        }
    }
}

#pragma mark - Public Method
// 如果是本地图片调用此方法
+ (QJCustomScrollView *)cycleScrollViewWithFrame:(CGRect)frame localImages:(NSArray<NSString *> *)localImages placeholderImage:(UIImage *)image {
    QJCustomScrollView *cycleScrollView = [[QJCustomScrollView alloc] initWithFrame:frame];
    cycleScrollView.placeholderImage = image;
    // 调用set方法
    cycleScrollView.localImages = localImages;
    return cycleScrollView;
}

// 如果是网络图片调用此方法
+ (QJCustomScrollView *)cycleScrollViewWithFrame:(CGRect)frame urlImages:(NSArray<NSString *> *)urlImages placeholderImage:(UIImage *)image {
    QJCustomScrollView *cycleScrollView = [[QJCustomScrollView alloc] initWithFrame:frame];
    cycleScrollView.placeholderImage = image;
    // 调用set方法
    cycleScrollView.urlImages = urlImages;
    return cycleScrollView;
}

- (void)adjustWhenControllerViewWillAppear {
    // 将轮播图的偏移量设回中间位置
    if (self.kImageCount > 1) {
        self.scrollView.contentOffset = CGPointMake(kWidth, 0);
    }
}

#pragma maek - Private Method
- (void)configure{
    [self addSubview:self.scrollView];
    // 添加最初的三张imageView
    if (self.kImageCount > 1) {
        [self.scrollView addSubview:self.lastImgView];
        [self.scrollView addSubview:self.currentImgView];
        [self.scrollView addSubview:self.nextImgView];
        
        // 将上一张图片设置为数组中最后一张图片
        [self setImageView:_lastImgView withSubscript:(_kImageCount-1)];
        // 将当前图片设置为数组中第一张图片
        [self setImageView:_currentImgView withSubscript:0];
        // 将下一张图片设置为数组中第二张图片,如果数组只有一张图片，则上、中、下图片全部是数组中的第一张图片
        [self setImageView:_nextImgView withSubscript:_kImageCount == 1 ? 0 : 1];
    } else {
        [self.scrollView addSubview:self.currentImgView];
        [self setImageView:_currentImgView withSubscript:0];
    }
    
    self.nextPhotoIndex = 1;
    self.lastPhotoIndex = _kImageCount - 1;
    
    [self layoutIfNeeded];
}

// 根据下标设置imgView的image
- (void)setImageView:(UIImageView *)imgView withSubscript:(NSInteger)subcript{
    if (_placeholderImage) { // 先给一张
        imgView.image = _placeholderImage;
    }
    if (self.carouseImagesType == QJCycleScrollViewImagesDataTypeInLocal) {
        imgView.image = [UIImage imageNamed:self.localImages[subcript]];
    } else{
        // 网络图片设置
        imgView.imageURL = [NSURL URLWithString:self.urlImages[subcript]];
    }
}

#pragma mark - setter
// 本地图片
- (void)setLocalImages:(NSArray<NSString *> *)localImages {
    if (localImages.count == 0) return;
    if (![_localImages isEqualToArray:localImages]) {
        _localImages = nil;
        _localImages = [localImages copy];
        // 标记图片来源
        self.carouseImagesType = QJCycleScrollViewImagesDataTypeInLocal;
        //获取数组个数
        self.kImageCount = _localImages.count;
        [self configure];
    }
}

// 网络图片
- (void)setUrlImages:(NSArray<NSString *> *)urlImages {
    if (urlImages.count == 0) return;
    if (![_urlImages isEqualToArray:urlImages]) {
        _urlImages = nil;
        _urlImages = [urlImages copy];
        // 标记图片来源
        self.carouseImagesType = QJCycleScrollViewImagesDataTypeInURL;
        self.kImageCount = _urlImages.count;
        [self configure];
    }
}

// 设置imageView的内容模式
- (void)setImageMode:(UIViewContentMode)imageMode {
    _imageMode = imageMode;
    self.nextImgView.contentMode = self.currentImgView.contentMode = self.lastImgView.contentMode = imageMode;
}

#pragma mark 清除沙盒中的图片缓存
+ (void)clearDiskCache {
    NSArray *contents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:cache error:NULL];
    for (NSString *fileName in contents) {
        [[NSFileManager defaultManager] removeItemAtPath:[cache stringByAppendingPathComponent:fileName] error:nil];
    }
}

#pragma mark - 手势点击事件
- (void)layoutSubviews {
    [super layoutSubviews];

    self.scrollView.frame = self.bounds;
    //有导航控制器时，会默认在scrollview上方添加64的内边距，这里强制设置为0
    self.scrollView.contentInset = UIEdgeInsetsZero;
    
    if (self.kImageCount > 1) {
        // 重新设置contentOffset和contentSize对于轮播图下拉放大以及里面的图片跟随放大起着关键作用，因为scrollView放大了，如果不手动设置contentOffset和contentSize，则会导致scrollView的容量不够大，从而导致图片越出scrollview边界的问题
        self.scrollView.contentSize = CGSizeMake(kWidth * 3, 0);
        // 这里如果采用动画效果设置偏移量将不起任何作用
        self.scrollView.contentOffset = CGPointMake(kWidth, 0);
        
        self.lastImgView.frame = CGRectMake(0, 0, kWidth, kHeight);
        self.currentImgView.frame = CGRectMake(kWidth, 0, kWidth, kHeight);
        self.nextImgView.frame = CGRectMake(kWidth * 2, 0, kWidth, kHeight);

    } else {
        self.scrollView.contentSize = CGSizeZero;
        self.scrollView.contentOffset = CGPointMake(0, 0);
        self.currentImgView.frame = CGRectMake(0, 0, kWidth, kHeight);
    }
}

#pragma mark - 懒加载
-(UIScrollView *)scrollView{
    if (_scrollView == nil) {
        _scrollView = [[UIScrollView alloc] init];
        _scrollView.pagingEnabled = YES;
        _scrollView.bounces = NO;
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.showsVerticalScrollIndicator = NO;
        _scrollView.delegate = self;
        _scrollView.clipsToBounds = YES;
        _scrollView.layer.masksToBounds = YES;
    }
    return _scrollView;
}

- (UIImageView *)lastImgView{
    if (_lastImgView == nil) {
        _lastImgView = [[UIImageView alloc] init];
        _lastImgView.layer.masksToBounds = YES;
    }
    return _lastImgView;
}

- (UIImageView *)currentImgView{
    if (_currentImgView == nil) {
        _currentImgView = [[UIImageView alloc] init];
        _currentImgView.layer.masksToBounds = YES;
    }
    return _currentImgView;
}

- (UIImageView *)nextImgView{
    if (_nextImgView == nil) {
        _nextImgView = [[UIImageView alloc] init];
        _nextImgView.layer.masksToBounds = YES;
    }
    return _nextImgView;
}

- (NSOperationQueue *)queue {
    if (!_queue) {
        _queue = [[NSOperationQueue alloc] init];
    }
    return _queue;
}

-(void)dealloc {
    _scrollView.delegate = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:QJCircleScrollSyncNotification object:nil];
}

@end

