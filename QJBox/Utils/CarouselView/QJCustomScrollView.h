//
//  QJCustomScrollView.h
//  QJBox
//
//  Created by rui on 2022/7/26.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJCustomScrollView : UIView

+ (QJCustomScrollView *)cycleScrollViewWithFrame:(CGRect)frame urlImages:(nonnull NSArray<NSString *> *)urlImages placeholderImage:(nullable UIImage *)image;

@property(strong, nonatomic) NSArray<NSString *> *urlImages; // 网络图片
@property(strong, nonatomic) NSArray<NSString *> *localImages; // 本地图片

@property (nonatomic, strong) UIImage *placeholderImage; // 占位图,默认nil,必须在设置图片数组之前设置才有效

@property (nonatomic, assign) UIViewContentMode imageMode; // 设置图片的内容模式，默认为UIViewContentModeScaleToFill

- (void)adjustWhenControllerViewWillAppear; // 解决viewWillAppear时出现时轮播图卡在一半的问题，在控制器viewWillAppear时调用此方法
+ (void)clearDiskCache;

@end

NS_ASSUME_NONNULL_END
