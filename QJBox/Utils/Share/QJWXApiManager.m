//
//  QJWXApiManager.m
//  QJBox
//
//  Created by rui on 2022/6/29.
//

#import "QJWXApiManager.h"
#define WWXX_APP_ID                   @"wxa1909ff8ce9104ee"
#define QJ_UNIVERSAL_LINK   @"https://iosqjbox.sxmu.com/wx_conn"
@implementation QJWXApiManager

#pragma mark - LifeCycle
+(instancetype)sharedManager {
    static dispatch_once_t onceToken;
    static QJWXApiManager *instance;
    dispatch_once(&onceToken, ^{
        instance = [[QJWXApiManager alloc] init];
    });
    return instance;
}


- (void)setupSDK {
#if DEBUG
    
    //在 register 之前打开 log , 后续可以根据 log 排查问题
    [WXApi startLogByLevel:WXLogLevelDetail logBlock:^(NSString *log) {
        DLog(@"WeChatSDK: %@", log);
    }];
    
#else
    
    [WXApi stopLog];
#endif
    //向微信注册
       [WXApi registerApp:WWXX_APP_ID
   universalLink:QJ_UNIVERSAL_LINK];
#if DEBUG
    //调用自检函数
//    [WXApi checkUniversalLinkReady:^(WXULCheckStep step, WXCheckULStepResult* result) {
//        DLog(@"%@, %u, %@, %@", @(step), result.success, result.errorInfo, result.suggestion);
//    }];

    
#endif
    
}

- (BOOL)handleOpenURL:(NSURL *)url {
    
    if(url){
        DLog(@"%@", url.scheme);
        if([url.scheme isEqualToString:WWXX_APP_ID]){
            
            return [WXApi handleOpenURL:url delegate:self];
        }
    }
    return NO;
}

- (BOOL)handleUniversalLink:(NSUserActivity *)userActivity {
    NSURL *url = userActivity.webpageURL;
    if (url) {
        DLog(@"%@", url.scheme);
        return [WXApi handleOpenUniversalLink:userActivity delegate:self];
    }
    
    return NO;
}

- (BOOL)isWXInstall {
    BOOL installed = [WXApi isWXAppInstalled] && [WXApi isWXAppSupportApi];
    
    if(!installed){
        DLog(@"当前设备未安装微信应用或版本过低");
    }
    
    return installed;
}


- (void)WXAuth {
    
    if([self isWXInstall]){//判断用户是否已安装微信App
        
        SendAuthReq *req = [[SendAuthReq alloc] init];
        req.state = @"wx_oauth_authorization_state";//用于保持请求和回调的状态，授权请求或原样带回
        req.scope = @"snsapi_userinfo";//授权作用域：获取用户个人信息
        
        [WXApi sendReq:req completion:^(BOOL success) {
            
            NSLog(@"唤起微信:%@", success ? @"成功" : @"失败");
        }];
    }
}
- (void)WXShareWithTitle:(NSString *)title description:(NSString *)description image:(UIImage *)image url:(NSString *)shareUrl scene:(int)scene {//scene 0:QQ好友 1:QQ空间
    
    if([self isWXInstall]){
        
        SendMessageToWXReq *sendReq = [[SendMessageToWXReq alloc] init];
        sendReq.bText = NO;//不使用文本信息
        sendReq.scene = scene;//0 = 好友列表 1 = 朋友圈 2 = 收藏
        
        //创建分享内容对象
        WXMediaMessage *urlMessage = [WXMediaMessage message];
        urlMessage.title = title;//标题
        urlMessage.description = description;//描述
        [urlMessage setThumbImage:image];//设置图片
        
        //创建多媒体对象
        WXWebpageObject *webObj = [WXWebpageObject object];
        webObj.webpageUrl = shareUrl;//链接
        
        //完成发送对象实例
        urlMessage.mediaObject = webObj;
        sendReq.message = urlMessage;
        
        //发送分享信息
        [WXApi sendReq:sendReq completion:^(BOOL success) {
            
            NSLog(@"唤起微信:%@", success ? @"成功" : @"失败");
        }];
    }
}



#pragma mark - WXApiDelegate
- (void)onResp:(BaseResp *)resp {
    if ([resp isKindOfClass:[SendMessageToWXResp class]]) {
        if (_delegate
            && [_delegate respondsToSelector:@selector(managerDidRecvMessageResponse:)]) {
            SendMessageToWXResp *messageResp = (SendMessageToWXResp *)resp;
            [_delegate managerDidRecvMessageResponse:messageResp];
        }
    } else if ([resp isKindOfClass:[SendAuthResp class]]) {
        if (_delegate
            && [_delegate respondsToSelector:@selector(managerDidRecvAuthResponse:)]) {
            SendAuthResp *authResp = (SendAuthResp *)resp;
            [_delegate managerDidRecvAuthResponse:authResp];
        }
    } else if ([resp isKindOfClass:[AddCardToWXCardPackageResp class]]) {
        if (_delegate
            && [_delegate respondsToSelector:@selector(managerDidRecvAddCardResponse:)]) {
            AddCardToWXCardPackageResp *addCardResp = (AddCardToWXCardPackageResp *)resp;
            [_delegate managerDidRecvAddCardResponse:addCardResp];
        }
    } else if ([resp isKindOfClass:[WXChooseCardResp class]]) {
        if (_delegate
            && [_delegate respondsToSelector:@selector(managerDidRecvChooseCardResponse:)]) {
            WXChooseCardResp *chooseCardResp = (WXChooseCardResp *)resp;
            [_delegate managerDidRecvChooseCardResponse:chooseCardResp];
        }
    }else if ([resp isKindOfClass:[WXChooseInvoiceResp class]]){
        if (_delegate
            && [_delegate respondsToSelector:@selector(managerDidRecvChooseInvoiceResponse:)]) {
            WXChooseInvoiceResp *chooseInvoiceResp = (WXChooseInvoiceResp *)resp;
            [_delegate managerDidRecvChooseInvoiceResponse:chooseInvoiceResp];
        }
    }else if ([resp isKindOfClass:[WXSubscribeMsgResp class]]){
        if ([_delegate respondsToSelector:@selector(managerDidRecvSubscribeMsgResponse:)])
        {
            [_delegate managerDidRecvSubscribeMsgResponse:(WXSubscribeMsgResp *)resp];
        }
    }else if ([resp isKindOfClass:[WXLaunchMiniProgramResp class]]){
        if ([_delegate respondsToSelector:@selector(managerDidRecvLaunchMiniProgram:)]) {
            [_delegate managerDidRecvLaunchMiniProgram:(WXLaunchMiniProgramResp *)resp];
        }
    }else if([resp isKindOfClass:[WXInvoiceAuthInsertResp class]]){
        if ([_delegate respondsToSelector:@selector(managerDidRecvInvoiceAuthInsertResponse:)]) {
            [_delegate managerDidRecvInvoiceAuthInsertResponse:(WXInvoiceAuthInsertResp *) resp];
        }
    }else if([resp isKindOfClass:[WXNontaxPayResp class]]){
        if ([_delegate respondsToSelector:@selector(managerDidRecvNonTaxpayResponse:)]) {
            [_delegate managerDidRecvNonTaxpayResponse:(WXNontaxPayResp *)resp];
        }
    }else if ([resp isKindOfClass:[WXPayInsuranceResp class]]){
        if ([_delegate respondsToSelector:@selector(managerDidRecvPayInsuranceResponse:)]) {
            [_delegate managerDidRecvPayInsuranceResponse:(WXPayInsuranceResp *)resp];
        }
    }
}

- (void)onReq:(BaseReq *)req {
    if ([req isKindOfClass:[ShowMessageFromWXReq class]]) {
        if (_delegate
            && [_delegate respondsToSelector:@selector(managerDidRecvShowMessageReq:)]) {
            ShowMessageFromWXReq *showMessageReq = (ShowMessageFromWXReq *)req;
            [_delegate managerDidRecvShowMessageReq:showMessageReq];
        }
    } else if ([req isKindOfClass:[LaunchFromWXReq class]]) {
        if (_delegate
            && [_delegate respondsToSelector:@selector(managerDidRecvLaunchFromWXReq:)]) {
            LaunchFromWXReq *launchReq = (LaunchFromWXReq *)req;
            [_delegate managerDidRecvLaunchFromWXReq:launchReq];
        }
    }
}

@end
