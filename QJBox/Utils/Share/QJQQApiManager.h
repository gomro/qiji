//
//  QJQQApiManager.h
//  QJBox
//
//  Created by rui on 2022/6/29.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

#define QJQQAPIMANAGER [QJQQApiManager sharedManager]

@protocol QQApiManagerDelegate <NSObject>

@optional

- (void)managerDidReceiveQQRequest:(QQBaseReq *)req;

- (void)managerDidRecvQQResponse:(QQBaseResp *)resp;

- (void)getResultDic:(NSDictionary *)resultDic;

@end

@interface QJQQApiManager : NSObject<QQApiInterfaceDelegate>

@property (nonatomic, weak) id<QQApiManagerDelegate> delegate;

+ (instancetype)sharedManager;

- (void)setupSDK;

- (BOOL)handleOpenURL:(NSURL *)url;
- (BOOL)handleUniversalLink:(NSUserActivity *)userActivity;


- (BOOL)isQQInstall;
- (void)QQAuth;
- (void)QQShareWithTitle:(NSString *)title description:(NSString *)description imageUrl:(NSString *)imageUrl url:(NSString *)shareUrl scene:(int)scen;    //scene 0:微信好友 1:朋友圈



@end

NS_ASSUME_NONNULL_END
