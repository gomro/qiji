//
//  QJQQApiManager.m
//  QJBox
//
//  Created by rui on 2022/6/29.
//

#import "QJQQApiManager.h"
#define Qj_APPID    @"102015660"
#define QJ_UNIVERSAL_LINK   @"https://iosqjbox.sxmu.com/qq_conn/102015660"


@interface QJQQApiManager ()<TencentSessionDelegate>

@property (nonatomic, strong) TencentOAuth *tencentOAuth;

@end

@implementation QJQQApiManager

#pragma mark -LifeCycle
+(instancetype)sharedManager {
    static dispatch_once_t onceToken;
    static QJQQApiManager *instance;
    dispatch_once(&onceToken, ^{
        instance = [[QJQQApiManager alloc] init];
    });
    return instance;
}



- (void)setupSDK {
    if (!self.tencentOAuth) {
        self.tencentOAuth =  [[TencentOAuth alloc] initWithAppId:Qj_APPID enableUniveralLink:YES universalLink:QJ_UNIVERSAL_LINK delegate:self];
    }
    
#if DEBUG
    if (self.tencentOAuth) {
        [QQApiInterface startLogWithBlock:^(NSString *logStr) {
            DLog(@"QQ LOG === %@",logStr);
        }];
        DLog(@"Tencent openSDK初始化成功");
    }
#else
    
    [QQApiInterface stopLog];
#endif
    
    
    
}


/// 登录
- (void)QQAuth {
    NSArray *permissions = [NSArray arrayWithObjects:kOPEN_PERMISSION_GET_INFO, kOPEN_PERMISSION_GET_USER_INFO, kOPEN_PERMISSION_GET_SIMPLE_USER_INFO, nil];
    self.tencentOAuth.authMode = kAuthModeServerSideCode;
    [self.tencentOAuth authorize:permissions];
}

- (BOOL)handleOpenURL:(NSURL *)url{
    
    if(url){
        DLog(@"%@", url.scheme);
        if([url.scheme isEqualToString:[NSString stringWithFormat:@"tencent%@", Qj_APPID]]){
            [QQApiInterface handleOpenURL:url delegate:self];
            return [TencentOAuth HandleOpenURL:url];
        }
    }
    
    return NO;
}

- (void)QQShareWithTitle:(NSString *)title description:(NSString *)description imageUrl:(NSString *)imageUrl url:(NSString *)shareUrl scene:(int)scene {
    
    //用于分享图片内容的对象
    if([self isQQInstall]){
        QQApiNewsObject *newsObj = [QQApiNewsObject objectWithURL:[NSURL URLWithString:shareUrl] title:title description:description previewImageURL:[NSURL URLWithString:imageUrl]];
        SendMessageToQQReq *req = [SendMessageToQQReq reqWithContent:newsObj];

        switch (scene) {
            case 0:
            {
                [QQApiInterface sendReq:req];
            }
                break;
            case 1:
            {
                [QQApiInterface SendReqToQZone:req];
            }
                break;
                
            default:
            break;
        }
    }
}

- (BOOL)isQQInstall{
    
    BOOL installed = [QQApiInterface isQQInstalled] && [QQApiInterface isQQSupportApi];
    
    if(!installed){
        DLog(@"当前设备未安装QQ应用或版本过低");
    }

    return installed;
}


- (BOOL)handleUniversalLink:(NSUserActivity *)userActivity{
    
    NSURL *url = userActivity.webpageURL;
    
    if(url){
        DLog(@"%@", url.scheme);
        if([TencentOAuth CanHandleUniversalLink:url]){
            [QQApiInterface handleOpenUniversallink:url delegate:self];
            return [TencentOAuth HandleUniversalLink:url];
        }
    }
    
    return NO;
}

#pragma mark - TencentLoginDelegate第三方应用实现登录的回调协议
// 登录成功后的回调
- (void)tencentDidLogin{
    
    NSString *code = [_tencentOAuth getServerSideCode];
    
    DLog(@"qqqqqq = %@",code);
  
    if (self.delegate && [self.delegate respondsToSelector:@selector(getResultDic:)]) {
        [self.delegate getResultDic:@{@"code":code?:@"",@"success":@(1),@"msg":@"登录成功"}];
    }
}

// 登录失败后的回调
- (void)tencentDidNotLogin:(BOOL)cancelled{
    if (self.delegate && [self.delegate respondsToSelector:@selector(getResultDic:)]) {
        [self.delegate getResultDic:@{@"code":@"",@"success":@(0),@"msg":@"登录失败"}];
    }
    DLog(@"QQ登录失败");
}

// 登录时网络有问题的回调
- (void)tencentDidNotNetWork{
    if (self.delegate && [self.delegate respondsToSelector:@selector(getResultDic:)]) {
        [self.delegate getResultDic:@{@"code":@"",@"success":@(0),@"msg":@"网络问题,登录失败"}];
    }
    DLog(@"QQ登录时网络有问题");
}

#pragma mark -QQApiDelegate
- (void)onReq:(QQBaseReq *)req{
    
}

- (void)onResp:(QQBaseResp *)resp{
    if (_delegate
        && [_delegate respondsToSelector:@selector(managerDidRecvQQResponse:)]) {
        QQBaseResp *messageResp = (QQBaseResp *)resp;
        [_delegate managerDidRecvQQResponse:messageResp];
    }
}

- (void)isOnlineResponse:(NSDictionary *)response {
    
}


#pragma mark ----

 


@end
