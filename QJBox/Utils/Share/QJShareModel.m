//
//  QJShareModel.m
//  QJBox
//
//  Created by rui on 2022/6/29.
//

#import "QJShareModel.h"

@implementation QJShareModel

-(instancetype)init{
    self = [super init];
    if (self) {
        
    }
    return self;
}

+(QJShareModel *)shareModelFactoryWithData:(id)data{
    QJShareModel *shareInfo = [[QJShareModel alloc]init];
    shareInfo.shareTitle = @"";
    shareInfo.shareDes = @"";
    shareInfo.shareImg = @"";
    shareInfo.sharedUrl = @"";
    return shareInfo;
}

@end
