//
//  QJShareCollectionCell.h
//  QJBox
//
//  Created by rui on 2022/6/29.
//

#import <UIKit/UIKit.h>

#import "QJShareModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJShareCollectionCell : UICollectionViewCell

-(void)configCCellWithData:(QJShareModel *)model;

+(CGSize)ccellSize;

@end

NS_ASSUME_NONNULL_END
