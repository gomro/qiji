//
//  QJShareManager.h
//  QJBox
//
//  Created by rui on 2022/6/29.
//

#import <Foundation/Foundation.h>
#import "QJShareModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJShareManager : NSObject

+ (id)sharedManager;

/**
 * @author: zjr
 * @date: 2022-6-29
 * @desc: 分享面板，传入分享model数据
 */
-(void)sharWithShareModel:(QJShareModel *)model;

@end

NS_ASSUME_NONNULL_END
