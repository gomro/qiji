//
//  QJShareView.m
//  QJBox
//
//  Created by rui on 2022/6/29.
//

#import "QJShareView.h"

#import "QJShareCollectionCell.h"

@interface QJShareView ()<UIGestureRecognizerDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong) QJShareModel *model;
@property (nonatomic, strong) UITapGestureRecognizer *tapGesture;
@property (nonatomic, strong) UILabel *lab_title;
@property (nonatomic, strong) UICollectionView *collectView;
@property (nonatomic, strong) UICollectionViewFlowLayout *layOut;
@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) UIButton *cancleBtn;
@property (nonatomic, strong) UIView *lineView;

@property (nonatomic, strong) NSMutableArray *icon_array;
@property (nonatomic , copy) ShareBackHandler handler;

@property (nonatomic, strong) NSMutableArray *animatableConstraints;
@property (nonatomic, assign) int padding;
@property (nonatomic, assign) BOOL animating;

@end

@implementation QJShareView

+(instancetype)initShowShareViewWithModel:(QJShareModel *)model back:(ShareBackHandler)handler{
    QJShareView *view = [[QJShareView alloc]initShowShareViewWithModel:model back:handler];
    return view;
}

-(instancetype)initShowShareViewWithModel:(QJShareModel *)model back:(ShareBackHandler)handler{
    self = [super init];
    if (self) {
        self.handler = handler;
        self.model = model;
        [self initView];
    }
    return self;
}

/**
 * @author: zjr
 * @date: 2022-6-29
 * @desc: 初始化UI
 */
-(void)initView{
    self.backgroundColor = [UIColor colorWithWhite:0 alpha:0.4];
    [[UIApplication sharedApplication].keyWindow addSubview:self];
    [self mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo([UIApplication sharedApplication].keyWindow);
    }];
    
    if (!_tapGesture) {
        _tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(removeViews:)];
        _tapGesture.delegate = self;
        [self addGestureRecognizer:_tapGesture];
    }
    
    if (!_bgView) {
        _bgView = [[UIView alloc]init];
        _bgView.backgroundColor = [UIColor whiteColor];
        [self addSubview:_bgView];
    }
    [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.bottom.equalTo(self).offset([QJShareCollectionCell ccellSize].height + Bottom_SN_iPhoneX_OR_LATER_SPACE + 50 + 8 + 40);
    }];

    if (!_cancleBtn) {
        _cancleBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_cancleBtn addTarget:self action:@selector(removeViews:) forControlEvents:UIControlEventTouchUpInside];
        _cancleBtn.titleLabel.font = kFont(16);
        _cancleBtn.backgroundColor = [UIColor whiteColor];
        [_cancleBtn setTitle:@"取消" forState:UIControlStateNormal];
        [_cancleBtn setTitleColor:[UIColor colorWithHexString:@"999999"] forState:UIControlStateNormal];
        [self.bgView addSubview:_cancleBtn];
    }
    [self.cancleBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.bgView.mas_bottom).offset(-(Bottom_SN_iPhoneX_OR_LATER_SPACE + 50));
        make.left.right.equalTo(self.bgView);
        make.height.mas_equalTo(50);
    }];
    
    if (!_lineView) {
        _lineView = [[UIView alloc]init];
        _lineView.backgroundColor = [UIColor colorWithHexString:@"F5F6FA"];
        [self.bgView addSubview:_lineView];
    }
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.cancleBtn.mas_top);
        make.left.right.equalTo(self.bgView);
        make.height.mas_equalTo(8);
    }];

    if (!_layOut) {
        _layOut = [[UICollectionViewFlowLayout alloc] init];
        _layOut.minimumInteritemSpacing = 0;
        _layOut.minimumLineSpacing = 0;
        _layOut.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    }
    if (!_collectView) {
        _collectView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:self.layOut];
        _collectView.backgroundColor = [UIColor whiteColor];
        _collectView.showsHorizontalScrollIndicator = NO;
        _collectView.delegate = self;
        _collectView.dataSource = self;
        [self.bgView addSubview:_collectView];
    }
    [self.collectView registerClass:[QJShareCollectionCell class] forCellWithReuseIdentifier:@"QJShareCollectionCell"];
    [self.collectView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.lineView.mas_top);
        make.left.right.equalTo(self.bgView);
        make.height.mas_equalTo([QJShareCollectionCell ccellSize].height);
    }];
    
    if (!_lab_title) {
        _lab_title = [UILabel new];
        _lab_title.font = kFont(14);
        _lab_title.textColor = UIColorFromRGB(0x333333);
        _lab_title.text = @"分享到";
        [self.bgView addSubview:_lab_title];
    }
    [self.lab_title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.collectView.mas_top);
        make.left.equalTo(self.bgView).offset(15);
        make.right.equalTo(self.bgView).offset(-15);
        make.height.mas_equalTo(40);
    }];
    
    
    [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.lab_title);
    }];
    
    [self.bgView layoutIfNeeded];
    [self.bgView showCorner:8 rectCorner:UIRectCornerAllCorners];
    
    CAKeyframeAnimation *opacity = [CAKeyframeAnimation animationWithKeyPath:@"opacity"];
    opacity.values = @[@(0) ,@(1)];
    opacity.duration = .25;
    [self.bgView.layer addAnimation:opacity forKey:nil];
    [self layoutIfNeeded];
    [UIView animateWithDuration:.25 animations:^{
        [self.bgView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self).offset(0);
        }];
        [self layoutIfNeeded];
    }];
}

#pragma mark - UICollectionView
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return [QJShareCollectionCell ccellSize];
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 16, 0, 4);
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.icon_array.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    QJShareCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"QJShareCollectionCell" forIndexPath:indexPath];
    [cell configCCellWithData:self.icon_array[indexPath.row]];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    QJShareModel *data = self.icon_array[indexPath.row];
    if (self.handler) {
        self.handler(data);
    }
    [self removeViews:nil];
}

#pragma mark - UIGestureRecognizer
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    if([touch.view isDescendantOfView:self.collectView]){
        return NO;
    }
    return YES;
}

/**
 * @author: zjr
 * @date: 2022-6-29
 * @desc: 点击事件
 */
-(void)removeViews:(id)sender{
    CAKeyframeAnimation *opacity = [CAKeyframeAnimation animationWithKeyPath:@"opacity"];
    opacity.values = @[@(1) ,@(0)];
    opacity.duration = .25;
    [self.bgView.layer addAnimation:opacity forKey:nil];
    [self layoutIfNeeded];
    [UIView animateWithDuration:0.25 animations:^{
        [self.bgView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self).offset([QJShareCollectionCell ccellSize].height + Bottom_SN_iPhoneX_OR_LATER_SPACE + 50 + 8 + 40);
        }];
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        self.alpha = 0;
        //移除所有子视图
        [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        [self removeFromSuperview];
    }];
}

#pragma mark - 数据源
- (NSMutableArray *)icon_array{
    if (!_icon_array) {
        NSArray *array = @[
            @{
                @"logo":@"icon_share_wechat",
                @"title":@"微信好友",
                @"tag":@1
            },
            @{
                @"logo":@"icon_share_wechatline",
                @"title":@"微信朋友圈",
                @"tag":@2
            },
            @{
                @"logo":@"icon_share_qq",
                @"title":@"QQ好友",
                @"tag":@3
            },
            @{
                @"logo":@"icon_share_qqzone",
                @"title":@"QQ空间",
                @"tag":@4
            },
        ];
        _icon_array = [NSMutableArray arrayWithArray:[NSArray modelArrayWithClass:[QJShareModel class] json:array]];
    }
    return _icon_array;
}

@end
