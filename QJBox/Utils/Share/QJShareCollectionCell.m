//
//  QJShareCollectionCell.m
//  QJBox
//
//  Created by rui on 2022/6/29.
//

#import "QJShareCollectionCell.h"

@interface QJShareCollectionCell ()

/* 图片icon */
@property (nonatomic, strong) UIImageView *imv_logo;
/* 分享标题label */
@property (nonatomic, strong) UILabel *logoLabel;

@end

@implementation QJShareCollectionCell

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self createView];
    }
    return self;
}

/**
 * @author: zjr
 * @date: 2022-6-29
 * @desc: 初始化UI
 */
-(void)createView{
    if (!_logoLabel) {
        _logoLabel = [UILabel new];
        _logoLabel.font = kFont(12);
        _logoLabel.textColor = UIColorFromRGB(0x4F4F4F);
        _logoLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:_logoLabel];
    }
    [self.logoLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.contentView);
        make.bottom.equalTo(self.contentView).offset(-16);
        make.height.mas_equalTo(16);
    }];
    
    if (!_imv_logo) {
        _imv_logo = [[UIImageView alloc]init];
        [self.contentView addSubview:_imv_logo];
    }
    [self.imv_logo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.contentView);
        make.bottom.equalTo(self.logoLabel.mas_top).offset(-8);
        make.size.mas_equalTo(CGSizeMake(32, 32));
    }];
}

/**
 * @author: zjr
 * @date: 2022-6-29
 * @desc: 数据源赋值
 */
-(void)configCCellWithData:(QJShareModel *)model{
    self.imv_logo.image = [UIImage imageNamed:model.logo];
    self.logoLabel.text = model.title;
}

+(CGSize)ccellSize{
    NSInteger width = (kScreenWidth - 32)/4;
    return CGSizeMake(width, 16 + 16 + 8 + 32 + 16);
}

@end

