//
//  QJShareModel.h
//  QJBox
//
//  Created by rui on 2022/6/29.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJShareModel : NSObject

/* 分享类型 1微信 2微信朋友圈 3qq 4QQ空间 */
@property (nonatomic, assign) NSInteger shareChannel;
/* 分享标题 */
@property (nonatomic, copy) NSString *shareTitle;
/* 分享的描述 */
@property (nonatomic, copy) NSString *shareDes;
/* 分享图片 支持UImage,Url,NSData */
@property (nonatomic, strong) id shareImg;
/* 分享链接 */
@property (nonatomic, copy) NSString *sharedUrl;
/* 分享数据链接 */
@property (nonatomic, strong) id shareData;


/* 分享面板icon */
@property (nonatomic, copy) NSString *logo;
/* 分享面板标题 */
@property (nonatomic, copy) NSString *title;
/* 分享面板类型区分  1微信 2微信朋友圈 3qq 4QQ空间 */
@property (nonatomic, assign) NSInteger tag;

/// 分享数据创建
/// @param data 数据源
/// QJShareModel
+(QJShareModel *)shareModelFactoryWithData:(id)data;

@end

NS_ASSUME_NONNULL_END
