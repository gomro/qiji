//
//  QJWXApiManager.h
//  QJBox
//
//  Created by rui on 2022/6/29.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

#define QJWXMANAGER [QJWXApiManager sharedManager]

@protocol WXApiManagerDelegate <NSObject>

@optional

/**
 * @author: zjr
 * @date: 2022-6-29
 * @desc: 微信各种Resp回调
 */
- (void)managerDidRecvShowMessageReq:(ShowMessageFromWXReq *)request;

- (void)managerDidRecvLaunchFromWXReq:(LaunchFromWXReq *)request;

- (void)managerDidRecvMessageResponse:(SendMessageToWXResp *)response;

- (void)managerDidRecvAuthResponse:(SendAuthResp *)response;

- (void)managerDidRecvAddCardResponse:(AddCardToWXCardPackageResp *)response;

- (void)managerDidRecvChooseCardResponse:(WXChooseCardResp *)response;

- (void)managerDidRecvChooseInvoiceResponse:(WXChooseInvoiceResp *)response;

- (void)managerDidRecvSubscribeMsgResponse:(WXSubscribeMsgResp *)response;

- (void)managerDidRecvLaunchMiniProgram:(WXLaunchMiniProgramResp *)response;

- (void)managerDidRecvInvoiceAuthInsertResponse:(WXInvoiceAuthInsertResp *)response;

- (void)managerDidRecvNonTaxpayResponse:(WXNontaxPayResp *)response;

- (void)managerDidRecvPayInsuranceResponse:(WXPayInsuranceResp *)response;

@end

@interface QJWXApiManager : NSObject<WXApiDelegate>

@property (nonatomic, assign) id<WXApiManagerDelegate> delegate;

+ (instancetype)sharedManager;


- (void)setupSDK;

- (BOOL)handleOpenURL:(NSURL *)url;
- (BOOL)handleUniversalLink:(NSUserActivity *)userActivity;

- (BOOL)isWXInstall;
- (void)WXAuth;
- (void)WXShareWithTitle:(NSString *)title description:(NSString *)description image:(UIImage *)image url:(NSString *)shareUrl scene:(int)scene;    //scene 0:QQ好友 1:QQ空间


@end

NS_ASSUME_NONNULL_END
