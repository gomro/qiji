//
//  QJShareView.h
//  QJBox
//
//  Created by rui on 2022/6/29.
//

#import <UIKit/UIKit.h>

#import "QJShareModel.h"

NS_ASSUME_NONNULL_BEGIN

typedef void(^ShareBackHandler)(QJShareModel *model);

@interface QJShareView : UIView

/**
 * @author: zjr
 * @date: 2022-6-30
 * @desc: 初始化方法
 */
+(instancetype)initShowShareViewWithModel:(QJShareModel *)model back:(ShareBackHandler)handler;

-(instancetype)initShowShareViewWithModel:(QJShareModel *)model back:(ShareBackHandler)handler;

@end

NS_ASSUME_NONNULL_END
