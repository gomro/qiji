//
//  QJShareManager.m
//  QJBox
//
//  Created by rui on 2022/6/29.
//

#import "QJShareManager.h"

#import "QJShareView.h"
#import "QJWXApiManager.h"
#import "QJQQApiManager.h"

#define Qj_APPID    @"102015660"
#define QJ_UNIVERSAL_LINK   @"https://iosqjbox.sxmu.com/qq_conn/102015660"

@interface QJShareManager ()<WXApiManagerDelegate,QQApiManagerDelegate>

@property (nonatomic, strong) QJShareModel *model;
@property (nonatomic, strong) TencentOAuth *tencentOAuth;

@end

@implementation QJShareManager

static QJShareManager *shared_manager = nil;
static dispatch_once_t onceToken;

+ (instancetype)sharedManager {
    dispatch_once(&onceToken, ^{
        shared_manager = [[self alloc] init];
    });
    return shared_manager;
}

-(void)sharWithShareModel:(QJShareModel *)model{

    self.model = model;
    __weak typeof(self) weakSelf = self;
    [QJShareView initShowShareViewWithModel:self.model back:^(QJShareModel * _Nonnull model) {
        weakSelf.model.shareChannel = model.tag;
        [weakSelf shareContent];
    }];
}

-(void)shareContent{
    if (self.model.shareChannel == 1 || self.model.shareChannel == 2) {//分享微信
        if (![WXApi isWXAppInstalled]){
            DLog(@"请安装微信");
            return;
        }
        [QJWXApiManager sharedManager].delegate = self;
        [self shareWeChat];
    }else if (self.model.shareChannel == 3 || self.model.shareChannel == 4){//qq分享
        [QJQQApiManager sharedManager].delegate = self;
        [self shareQQ];
    }
}

/**
 * @author: zjr
 * @date: 2022-6-29
 * @desc: 分享微信
 */
-(void)shareWeChat{
    WXMediaMessage *message = [WXMediaMessage message];
    message.title = @"奇迹盒子";//self.model.shareTitle;
    NSString *desc = @"奇迹盒子";//self.model.shareDes;
    desc = desc.length > 40 ? [desc substringToIndex:39]:desc;
    message.description = desc;
//    if ([self.model.shareImg isKindOfClass:[NSString class]] && [[NSString stringWithFormat:@"%@",self.model.shareImg] containsString:@"http"]) {
        NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:@"http://static.dhsf.xqhuyu.com/box/banner/621c3c2f3aee7.jpg"]];//self.model.shareImg
        self.model.shareImg = [[UIImage alloc]initWithData:data];
//    }
//    self.model.shareImg = self.model.shareImg;
//    [message setThumbImage:[[UIImage alloc]initWithData:[self.model.shareImg dataSmallerThan:1024 * 64]]];
    if (self.model.shareData && [self.model.shareData isKindOfClass:[UIImage class]]) {
        WXImageObject *object = [WXImageObject object];
        object.imageData = UIImagePNGRepresentation(self.model.shareData);
        message.mediaObject = object;
    }else{
        WXWebpageObject *object = [WXWebpageObject object];
        object.webpageUrl = @"http://static.dhsf.xqhuyu.com/box/banner/621c3c2f3aee7.jpg";//self.model.sharedUrl;
        message.mediaObject = object;
    }
    SendMessageToWXReq *req = [[SendMessageToWXReq alloc] init];
    req.bText = NO;
    req.message = message;
    req.scene = self.model.shareChannel == 1 ? WXSceneSession:WXSceneTimeline;
    [WXApi sendReq:req completion:^(BOOL success) {
        DLog(@"发送成功");
    }];
}

/**
 * @author: zjr
 * @date: 2022-6-29
 * @desc: 分享QQ
 */
-(void)shareQQ{
    self.tencentOAuth = [[TencentOAuth alloc] initWithAppId:Qj_APPID andDelegate:(id<TencentSessionDelegate>)self];
    NSString *title = @"奇迹盒子";//self.model.shareTitle;
    NSString *desc = @"奇迹盒子";//self.model.shareDes;
    desc = desc.length > 40 ? [desc substringToIndex:39]:desc;
//    if ([self.model.shareImg isKindOfClass:[NSString class]] && [[NSString stringWithFormat:@"%@",self.model.shareImg] containsString:@"http"]) {
        NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:@"http://static.dhsf.xqhuyu.com/box/banner/621c3c2f3aee7.jpg"]];//self.model.shareImg
        self.model.shareImg = [[UIImage alloc]initWithData:data];
//    }
//    self.model.shareImg = self.model.shareImg;
    id message;
    if (self.model.shareData && [self.model.shareData isKindOfClass:[UIImage class]]) {
        NSData *data = UIImagePNGRepresentation(self.model.shareData);
        if (self.model.shareChannel == 3) {
            message = [QQApiImageObject objectWithData:data
                                      previewImageData:nil
                                                 title:@""
                                           description:@""];
        }else{
            message = [QQApiImageArrayForQZoneObject objectWithimageDataArray:@[data] title:@"" extMap:nil];
        }
    }else{
        NSData *data = UIImagePNGRepresentation(self.model.shareData);
        //self.model.sharedUrl [self.model.shareImg dataSmallerThan:1024 * 100]
        message = [QQApiNewsObject objectWithURL:[NSURL URLWithString:@"http://static.dhsf.xqhuyu.com/box/banner/621c3c2f3aee7.jpg"] title:title description:desc previewImageData:data];
    }
    
    SendMessageToQQReq *req = [SendMessageToQQReq reqWithContent:message];
    QQApiSendResultCode sent;
    if (self.model.shareChannel == 3) {
        sent = [QQApiInterface sendReq:req];
    }else {
        sent = [QQApiInterface SendReqToQZone:req];
    }
    [self handleSendResult:sent];
}

/**
 * @author: zjr
 * @date: 2022-6-29
 * @desc: 微信代理回调
 */
- (void)managerDidRecvMessageResponse:(SendMessageToWXResp *)response{
    switch (response.errCode) {
        case WXSuccess: {
            DLog(@"分享成功");
            break;
        }
        case WXErrCodeUserCancel: {
            DLog(@"分享取消");
            break;
        }
        default: {
            DLog(@"分享失败");
            break;
        }
    }
}

/**
 * @author: zjr
 * @date: 2022-6-29
 * @desc: QQ发送请求
 */
- (void)handleSendResult:(QQApiSendResultCode)sendResult
{
    switch (sendResult)
    {
        case EQQAPISENDSUCESS:
        {
            
        }
            break;
        case EQQAPIAPPNOTREGISTED://App未注册
        case EQQAPIMESSAGECONTENTINVALID://发送参数错误
        case EQQAPIMESSAGECONTENTNULL://发送参数错误
        case EQQAPIMESSAGETYPEINVALID://发送参数错误
        case EQQAPIQQNOTINSTALLED://未安装手Q
        case EQQAPIQQNOTSUPPORTAPI://API接口不支持
        case EQQAPISENDFAILD://发送失败
        {
            DLog(@"发送失败");
        }
            break;
        
        default:
        {
            break;
        }
    }
}

- (void)responseDidReceived:(APIResponse*)response forMessage:(NSString *)message{
    
}

/**
 * @author: zjr
 * @date: 2022-6-29
 * @desc: QQ代理回调
 */
- (void)managerDidRecvQQResponse:(QQBaseResp *)resp{
    if ([resp.result integerValue] == 0) {
        DLog(@"分享成功");
        [[UIApplication sharedApplication].keyWindow makeToast:@"分享成功"];
    }
}

@end

