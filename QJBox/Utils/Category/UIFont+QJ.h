//
//  UIFont+QJ.h
//  QJBox
//
//  Created by wxy on 2022/10/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIFont (QJ)
+ (UIFont *)QJDisplayFontWithSize:(CGFloat)fontSize
                             bold:(BOOL)bold itatic:(BOOL)italic weight:(UIFontWeight)weight;
@end

NS_ASSUME_NONNULL_END
