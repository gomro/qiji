//
//  UICollectionView+Common.m
//  QJBox
//
//  Created by rui on 2022/7/7.
//

#import "UICollectionView+Common.h"

#import "QJEmptyView.h"

@implementation UICollectionView (Common)

- (void)collectionViewDisplayWhenHaveNoDataWithMsg:(NSString *)message centerImage:(UIImage *)centerImage  ifNecessaryForRowCount:(NSInteger)rowCount {
    if (rowCount == 0) {
        QJEmptyView *emptyView = [[QJEmptyView alloc] initWithMessage:message image:centerImage];
        emptyView.userInteractionEnabled = NO;
        self.backgroundView = emptyView;
    } else {
        if (!(floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_8_4)) {
            for (UIView *subView in self.subviews) {
                if ([subView isKindOfClass:[QJEmptyView class]]) {
                    [subView removeFromSuperview];
                }
            }
        }
        self.backgroundView = nil;
    }
}

- (void)collectionViewDisplayWhenHaveNoDataWithView:(UIView *)view ifNecessaryForRowCount:(NSInteger)rowCount{
    if (rowCount == 0) {
        self.backgroundView = view;
    } else {
        if (!(floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_8_4)) {
            for (UIView *subView in self.subviews) {
                if ([subView isKindOfClass:[QJEmptyView class]]) {
                    [subView removeFromSuperview];
                }
            }
        }
        self.backgroundView = nil;
    }
}

@end
