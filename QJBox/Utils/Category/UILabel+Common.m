

#import "UILabel+Common.h"
#import "QJLabelAttributeModel.h"
#import <CoreText/CoreText.h>

@implementation UILabel (Common)

-(void)alignBottom
{
    CGSize fontSize = [self.text sizeWithAttributes:@{NSFontAttributeName:self.font}];
    double height = fontSize.height*self.numberOfLines;
    double width = self.frame.size.width;
    CGSize stringSize = [self.text boundingRectWithSize:CGSizeMake(width, height) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:self.font} context:nil].size;
    
    NSInteger line = (height - stringSize.height) / fontSize.height;
    // 前面补齐换行符
    for (int i = 0; i < line; i++) {
        self.text = [NSString stringWithFormat:@" \n%@", self.text];
    }
}

+ (void)changeLineSpaceForLabel:(UILabel *)label WithSpace:(float)space {
    if (label.text) {
        NSString *labelText = label.text;
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:labelText];
        if (label.attributedText) {
            attributedString = label.attributedText.mutableCopy;
        }
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setLineSpacing:space];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [labelText length])];
        label.attributedText = attributedString;
        [label sizeToFit];
    }
}

+ (void)changeWordSpaceForLabel:(UILabel *)label WithSpace:(float)space {
    
    NSString *labelText = label.text;
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:labelText attributes:@{NSKernAttributeName:@(space)}];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [labelText length])];
    label.attributedText = attributedString;
    [label sizeToFit];
    
}

+ (void)changeSpaceForLabel:(UILabel *)label withLineSpace:(float)lineSpace WordSpace:(float)wordSpace {
    
    NSString *labelText = label.text;
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:labelText attributes:@{NSKernAttributeName:@(wordSpace)}];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:lineSpace];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [labelText length])];
    label.attributedText = attributedString;
    [label sizeToFit];
    
}

- (CGFloat)numberOfText{
    // 获取单行时候的内容的size
    CGSize singleSize = [self.text sizeWithAttributes:@{NSFontAttributeName:self.font}];
    // 获取多行时候,文字的size
    CGSize textSize = [self.text boundingRectWithSize:CGSizeMake(self.frame.size.width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:self.font} context:nil].size;
    // 返回计算的行数
    return ceil( textSize.height / singleSize.height);
}

/*
 *单行 自适应宽高
 */
- (void)setSingleStr:(NSString *)str{
    if(!str ||  (NSNull *)str == [NSNull null]){
        return;
    }
    self.text = str;
    
    CGSize resultSize = [str getSizeWithFont:self.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)];
    CGRect frame = self.frame;
    frame.size.height = self.font.pointSize;
    frame.size.width = resultSize.width;
    [self setFrame:frame];
}

- (void)setSingleStr:(NSString *)str withHeight:(CGFloat)height{
    if(!str ||  (NSNull *)str == [NSNull null]){
        return;
    }
    self.text = str;
    
    CGSize resultSize = [str getSizeWithFont:self.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)];
    CGRect frame = self.frame;
    frame.size.height = height;
    frame.size.width = resultSize.width;
    [self setFrame:frame];
}

- (void)setSingleStr:(NSString *)str withHeight:(CGFloat)height withSpace:(CGFloat)space{
    if(!str ||  (NSNull *)str == [NSNull null]){
        return;
    }
    self.text = str;
    
    CGSize resultSize = [str getSizeWithFont:self.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)];
    CGRect frame = self.frame;
    frame.size.height = height;
    frame.size.width = resultSize.width + space;
    [self setFrame:frame];
}

/*
 *单行 自适应宽高 不同字体
 */
- (void)setSingleStr:(NSString *)str range:(NSRange)range font:(UIFont *)font{
    [self setSingleStr:str range:range font:font color:nil numberOfLines:1];
}

/*
 *单行 自适应宽高 不同字体
 */
- (void)setSingleStr:(NSString *)str range:(NSRange)range color:(UIColor *)color{
    [self setSingleStr:str range:range font:nil color:color numberOfLines:1];
}

/*
 *单行 自适应宽高 不同字体
 */
- (void)setSingleStr:(NSString *)str range:(NSRange)range font:(UIFont *)font color:(UIColor *)color numberOfLines:(CGFloat)number{
    if(!str ||  (NSNull *)str == [NSNull null]){
        return ;
    }
//    if (number) {
        self.numberOfLines = number;
//    }else{
//        self.numberOfLines = 1;
//    }
    
    NSMutableAttributedString * aString = [[NSMutableAttributedString alloc] initWithString:str];
    NSMutableParagraphStyle * pStyle = [[NSMutableParagraphStyle alloc] init];
    [aString addAttribute:NSParagraphStyleAttributeName value:pStyle range:NSMakeRange(0, [str length])];
    if (font && str.length>0) {
        [aString addAttribute:NSFontAttributeName value:font range:range];
    }
    if (color && str.length>0) {
        [aString addAttribute:NSForegroundColorAttributeName value:color range:range];
    }
    self.attributedText = aString;
    
    [self sizeToFit];
}

/*
 *单行 自适应宽高 不同字体 数组
 */
- (void)setSingleStr:(NSString *)str andArrayModel:(NSMutableArray *)modelArray{
    if(!str ||  (NSNull *)str == [NSNull null]){
        return ;
    }
    self.numberOfLines = 1;
    
    NSMutableAttributedString * aString = [[NSMutableAttributedString alloc] initWithString:str];
    NSMutableParagraphStyle * pStyle = [[NSMutableParagraphStyle alloc] init];
    [aString addAttribute:NSParagraphStyleAttributeName value:pStyle range:NSMakeRange(0, [str length])];
    for (QJLabelAttributeModel *model in modelArray) {
        [aString addAttribute:NSFontAttributeName value:model.font range:model.range];
        [aString addAttribute:NSForegroundColorAttributeName value:model.textColor range:model.range];
    }
    self.attributedText = aString;
    
    [self sizeToFit];
}

/* 单行 最宽不过设置的宽度 */
- (void)setSingleStrFitWidth:(NSString *)str{
    if(!str ||  (NSNull *)str == [NSNull null]){
        return;
    }
    self.text = str;
    
    CGSize resultSize = [str getSizeWithFont:self.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)];
    CGRect frame = self.frame;
    frame.size.height = self.font.pointSize;
    if (self.width>0) {//设置了宽度
        if (resultSize.width > self.width) {
            frame.size.width = self.width;
        }else{
            frame.size.width = resultSize.width;
        }
    }else{//没有设置宽度
        frame.size.width = resultSize.width;
    }
    [self setFrame:frame];
}

- (void)setSingleStr:(NSString *)str maxWidth:(CGFloat)maxWidth{
    if(!str ||  (NSNull *)str == [NSNull null]){
        return;
    }
    self.text = str;
    
   NSDictionary *attrs = @{NSFontAttributeName : self.font};
   CGRect resultSize = [str boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:attrs context:nil];
   
   /* 实际宽度 */
   CGFloat width = resultSize.size.width;
   
   if (width > maxWidth) {
       width = maxWidth;
       self.lineBreakMode = NSLineBreakByTruncatingTail;
   }
   
   CGRect frame = self.frame;
   frame.size.height = self.font.pointSize;
   frame.size.width = width;
   [self setFrame:frame];
}

/*
 *单行 自适应宽高 不同字体 数组
 */
- (void)setSingleStr:(NSString *)str andArrayModel:(NSMutableArray *)modelArray maxWidth:(CGFloat)maxWidth{
    if(!str ||  (NSNull *)str == [NSNull null]){
        return ;
    }
    self.numberOfLines = 1;
    
    NSMutableAttributedString * aString = [[NSMutableAttributedString alloc] initWithString:str];
    NSMutableParagraphStyle * pStyle = [[NSMutableParagraphStyle alloc] init];
    [aString addAttribute:NSParagraphStyleAttributeName value:pStyle range:NSMakeRange(0, [str length])];
    for (QJLabelAttributeModel *model in modelArray) {
        if (model.font) {
            [aString addAttribute:NSFontAttributeName value:model.font range:model.range];
        }
        if (model.textColor) {
            [aString addAttribute:NSForegroundColorAttributeName value:model.textColor range:model.range];
        }
    }
    self.attributedText = aString;
    
    NSDictionary *attrs = @{NSFontAttributeName : self.font};
    CGRect resultSize = [str boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:attrs context:nil];
    /* 实际宽度 */
    CGFloat width = resultSize.size.width;

    if (width > maxWidth) {
        width = maxWidth;
        self.lineBreakMode = NSLineBreakByTruncatingTail;
    }
    CGRect frame = self.frame;
    frame.size.height = self.font.pointSize;
    frame.size.width = width;
    [self setFrame:frame];
}

#pragma mark - 单行
/*
 *  单行自适应宽度
*/
- (void)setSingleStr:(NSString *)str height:(CGFloat)height maxWidth:(CGFloat)maxWidth{
     if(!str ||  (NSNull *)str == [NSNull null]){
         return;
     }
    self.text = str;
     
    NSDictionary *attrs = @{NSFontAttributeName : self.font};
    CGRect resultSize = [str boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:attrs context:nil];
    /* 实际宽度 */
    CGFloat width = resultSize.size.width;
    if (width > maxWidth) {
        width = maxWidth;
        self.lineBreakMode = NSLineBreakByTruncatingTail;
    }
    CGRect frame = self.frame;
    frame.size.height = height;
    frame.size.width = width;
    [self setFrame:frame];
}

/*
 *  单行自适应宽度 关键字变色
*/
- (void)setSingleStr:(NSString *)str keyStr:(NSString *)keyStr color:(UIColor *)color height:(CGFloat)height maxWidth:(CGFloat)maxWidth{
    if(!str ||  (NSNull *)str == [NSNull null]){
        return ;
    }
    self.numberOfLines = 1;
    
    NSMutableAttributedString * aString = [[NSMutableAttributedString alloc] initWithString:str];
    NSMutableParagraphStyle * pStyle = [[NSMutableParagraphStyle alloc] init];
    [aString addAttribute:NSParagraphStyleAttributeName value:pStyle range:NSMakeRange(0, [str length])];
    if (!keyStr) {
        keyStr = @"";
    }
    [aString addAttribute:NSForegroundColorAttributeName value:color range:[str rangeOfString:keyStr]];
    self.attributedText = aString;
    
    NSDictionary *attrs = @{NSFontAttributeName : self.font};
    CGRect resultSize = [str boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:attrs context:nil];
    /* 实际宽度 */
    CGFloat width = resultSize.size.width;
    if (width > maxWidth) {
        width = maxWidth;
        self.lineBreakMode = NSLineBreakByTruncatingTail;
    }
    CGRect frame = self.frame;
    frame.size.height = height;
    frame.size.width = width;
    [self setFrame:frame];
}

@end
