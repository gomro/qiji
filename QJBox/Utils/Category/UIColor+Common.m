//
//  UIColor+Common.m
//  QJBox
//
//  Created by rui on 2022/7/19.
//

#import "UIColor+Common.h"

@implementation UIColor (Common)

+ (UIColor *)colorWithHexString:(NSString *)stringToConvert andAlpha:(CGFloat)alpha{
    UIColor *color = [UIColor colorWithHexString:stringToConvert];
    return [UIColor colorWithRed:color.red green:color.green blue:color.blue alpha:alpha];
}

@end
