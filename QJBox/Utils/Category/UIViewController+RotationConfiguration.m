//
//  UIViewController+RotationConfiguration.m
//  QJBox
//
//  Created by Sun on 2022/8/1.
//

#import "UIViewController+RotationConfiguration.h"

@implementation UIViewController (RotationConfiguration)
- (BOOL)shouldAutorotate {
    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}
@end
