//
//  UITableView+Common.h
//  QJBox
//
//  Created by rui on 2022/6/28.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UITableView (Common)

/**
 table 没有数据时展示UI

 @param message     提示文字
 @param centerImage 中间提示图片
 @param rowCount    数据源数组元素个数
 */
- (void)tableViewDisplayWhenHaveNoDataWithMsg:(NSString *)message centerImage:(UIImage *)centerImage  ifNecessaryForRowCount:(NSInteger)rowCount;

/**
 table 没有数据时展示UI，带有点击事件

 @param message     提示文字
 @param centerImage 中间提示图片
 @param rowCount    数据源数组元素个数
 @param title    点击按钮标题
 @param complete    点击回调
 */
- (void)tableViewDisplayWhenHaveNoDataWithMsg:(NSString *)message centerImage:(UIImage *)centerImage  ifNecessaryForRowCount:(NSInteger)rowCount Title:(NSString *)title Complete:(void(^ __nullable)(void))complete;

- (void)tableViewDisplayWhenHaveNoDataWithMsg:(NSString *)message centerImage:(UIImage *)centerImage  ifNecessaryForRowCount:(NSInteger)rowCount Title:(NSString *)title TitleColor:(NSString *)titleColor BackGroundColor:(NSString *)backGroundColor Complete:(void(^ __nullable)(void))complete;


- (void)tableViewDisplayWhenHaveNoDataWithView:(UIView *)view ifNecessaryForRowCount:(NSInteger)rowCount;

@end

NS_ASSUME_NONNULL_END
