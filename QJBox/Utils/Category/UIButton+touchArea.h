//
//  UIButton+touchArea.h
//  QJBox
//
//  Created by wxy on 2022/6/13.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, QJLayoutStatus){
    /** 正常位置，图左字右 */
    QJLayoutStatusNormal,
    /** 图右字左 */
    QJLayoutStatusImageRight,
    /** 图上字下 */
    QJLayoutStatusImageTop,
    /** 图下字上 */
    QJLayoutStatusImageBottom,
};

@interface UIButton (touchArea)

- (void)setEnlargeEdgeWithTop:(CGFloat) top right:(CGFloat) right bottom:(CGFloat) bottom left:(CGFloat) left;

- (void)setEnlargeEdge:(CGFloat) size;

- (void)layoutWithStatus:(QJLayoutStatus)status andMargin:(CGFloat)margin;


@end

NS_ASSUME_NONNULL_END
