//
//  UIViewController+QJ_Login.m
//  QJBox
//
//  Created by wxy on 2022/6/22.
//

#import "UIViewController+QJ_Login.h"
#import "QJLoginViewController.h"
#import "QJLoginBgView.h"
#import "QJFindWordViewController.h"
#import "QJThirdLoginViewController.h"
#import "QJLoginAppRequest.h"

@interface UIViewController (QJ_Login)

@property (nonatomic, assign) BOOL isCheck;//是否勾选的协议

@property (nonatomic, copy) LoginOrRegisterSuccessBlock loginOrRegisterSuccessBlock;


@end


@implementation UIViewController (QJ_Login)


- (void)showLoginVCCompleted:(void (^)(BOOL isLogin))completionHandler {
    self.loginOrRegisterSuccessBlock = completionHandler;
    UIViewController *tab = [UIApplication sharedApplication].delegate.window.rootViewController;
    if (!tab) {
        return;
    }
    
    for (UIViewController *vc in self.navigationController.viewControllers) {
        if([vc isKindOfClass:[QJLoginViewController class]]){
            return;
        }
    }
    
    if([QJUserManager shareManager].isNetLogout){
        return;
    }
    
    
    [self.view showHUDIndicator];
    TXCustomModel *model = [self buildCustomView];
    self.isCheck = NO;
    [IQKeyboardManager sharedManager].keyboardDistanceFromTextField = 300;
    
    __weak typeof(self) weakSelf = self;
    [[TXCommonHandler sharedInstance] getLoginTokenWithTimeout:3.0
                                                    controller:tab
                                                         model:model
                                                      complete:^(NSDictionary * _Nonnull resultDic) {
        NSString *resultCode = [resultDic objectForKey:@"resultCode"];
        if ([PNSCodeLoginControllerPresentSuccess isEqualToString:resultCode]) {
            NSLog(@"授权页拉起成功回调：%@", resultDic);
            [self.view hideHUDIndicator];
            [MBProgressHUD hideHUDForView:weakSelf.view animated:YES];
        } else if ([PNSCodeLoginControllerClickCancel isEqualToString:resultCode] ||
                   [PNSCodeLoginControllerClickChangeBtn isEqualToString:resultCode] ||
                   [PNSCodeLoginControllerClickLoginBtn isEqualToString:resultCode] ||
                   [PNSCodeLoginControllerClickCheckBoxBtn isEqualToString:resultCode]) {
            NSLog(@"页面点击事件回调：%@", resultDic);
            if ([PNSCodeLoginControllerClickCancel isEqualToString:resultCode]) {
                return;
            }
            self.isCheck = [[resultDic objectForKey:@"isChecked"] boolValue];
            if (!self.isCheck && [PNSCodeLoginControllerClickLoginBtn isEqualToString:resultCode]) {
                [[UIApplication sharedApplication].delegate.window  makeToast:@"请勾选同意《用户授权协议》和《隐私条款》" duration:3 position:CSToastPositionCenter image:[UIImage imageNamed:@"tips_toast"]];
                return;
            }
        }else if([PNSCodeLoginControllerClickProtocol isEqualToString:resultCode]){
            NSString *privacyUrl = [resultDic objectForKey:@"url"];
            NSString *privacyName = [resultDic objectForKey:@"urlName"];
            NSLog(@"如果TXCustomModel的privacyVCIsCustomized设置成YES，则SDK内部不会跳转协议页，需要自己实现");
            
            if(model.privacyVCIsCustomized){
                QJWKWebViewController *vc = [QJWKWebViewController new];
                vc.url = privacyUrl;
                vc.title = privacyName;
                vc.isHiddenNavgationBar = YES;
                UINavigationController *navigationController = weakSelf.navigationController;
                if (weakSelf.presentedViewController) {
                    //如果授权页成功拉起，这个时候则需要使用授权页的导航控制器进行跳转
                    navigationController = (UINavigationController *)weakSelf.presentedViewController;
                }
                [navigationController pushViewController:vc animated:YES];
            }
        } else if ([PNSCodeSuccess isEqualToString:resultCode]) {
            DLog(@"获取LoginToken成功回调：%@", resultDic);
            NSString *token = EncodeStringFromDic(resultDic, @"token");
            DLog(@"功能暂不支持，选择其他登录方式");
            
            QJLoginAppRequest *login = [QJLoginAppRequest new];
            WS(weakSelf)
            login.dic = @{@"onceTouchToken" : token,
                          @"type" : @"O",
                          
            };
            login.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
                if (ResponseSuccess) {
                    NSDictionary *data = EncodeDicFromDic(request.responseObject, @"data");
                    NSString *userid = EncodeStringFromDic(data, @"uid");
                    [IQKeyboardManager sharedManager].keyboardDistanceFromTextField = 10;
                        //登录成功
                        [weakSelf.view makeToast:@"登录成功" duration:3 position:CSToastPositionCenter image:[UIImage imageNamed:@"success_login"]];
                        LSUserDefaultsSET(EncodeStringFromDic(data, @"token"), kQJToken);
                    LSUserDefaultsSET(EncodeNumberFromDic(data, @"verifyState"), kQJIsRealName);
                    LSUserDefaultsSET(userid, kQJUserId);
                    [QJUserManager shareManager].isLogin = YES;
                    [QJUserManager shareManager].isNetLogout = NO;//登录成功状态改变
                    [QJUserManager shareManager].isVerifyState = [EncodeNumberFromDic(data, @"verifyState") boolValue];
                    [QJUserManager shareManager].userID = userid;
                        
                     
                    [weakSelf dismissLoginVC:^{
                        if (weakSelf.loginOrRegisterSuccessBlock) {
                            weakSelf.loginOrRegisterSuccessBlock(YES);
                        }
                        
                    }];
                    
                }else{
                    [weakSelf.view makeToast:ResponseMsg];
                }
            };
            
            login.failureCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
                [weakSelf.view makeToast:ResponseFailToastMsg];
            };
            
            [login start];
            
            
            
        } else {
            [self.view hideHUDIndicator];
            DLog(@"获取LoginToken或拉起授权页失败回调：%@", resultDic);
                QJLoginViewController *vc = [QJLoginViewController new];
                QJNavigationController *nav = [[QJNavigationController alloc]initWithRootViewController:vc];
                vc.loginOrRegisterSuccessBlock = completionHandler;
                UINavigationController *navigationController = (UINavigationController *)tab;
                if (weakSelf.presentedViewController) {
                    //如果授权页成功拉起，这个时候则需要使用授权页的导航控制器进行跳转
                    navigationController = (UINavigationController *)weakSelf.presentedViewController;
                }
                nav.modalPresentationStyle = UIModalPresentationFullScreen;
                [navigationController presentViewController:nav animated:YES completion:nil];
          
           
        }
    }];
    
    
   
    
}


- (void)dismissLoginVC:(void (^)(void))completion {
    UIViewController *tab = [UIApplication sharedApplication].delegate.window.rootViewController;
    [tab dismissViewControllerAnimated:YES completion:completion];
}


/// 看是否登录了
- (void)checkLogin:(void (^)(BOOL isLogin))completionHandler {
    if (![QJUserManager shareManager].isLogin) {
        [self showLoginVCCompleted:^(BOOL isLogin) {
            completionHandler(isLogin);
        }];
    }else{
        completionHandler(YES);
    }
}


- (TXCustomModel *)buildCustomView {
    TXCustomModel *model = [[TXCustomModel alloc] init];
    model.supportedInterfaceOrientations = UIInterfaceOrientationMaskPortrait;
    model.navBackImage = [UIImage imageNamed:@"backimage_black"];
    model.navColor = [UIColor clearColor];
    model.navTitle = [[NSAttributedString alloc] init];
    model.logoIsHidden = YES;
    model.changeBtnIsHidden = YES;
    model.privacyVCIsCustomized = YES;
    UIImage *unCheck = [UIImage imageNamed:@"unSelectedAgreementIcon"];
    UIImage *check = [UIImage imageNamed:@"selectedAgreementIcon"];
    model.checkBoxImages = @[unCheck,check];
    model.loginBtnText = [[NSAttributedString alloc]initWithString:@"本机号码一键登录" attributes:@{NSFontAttributeName:FontSemibold(16),NSForegroundColorAttributeName:[UIColor whiteColor]}];
    model.numberColor = [UIColor colorWithHexString:@"#16191C"];
    model.numberFont = FontSemibold(24);
    
    model.privacyOne = @[@"《用户协议》", [QJUserManager shareManager].userProtocol];
    model.privacyTwo = @[@"《隐私政策》", [QJUserManager shareManager].userPolicy];
    model.sloganIsHidden = YES;
     
   
    model.numberFrameBlock = ^CGRect(CGSize screenSize, CGSize superViewSize, CGRect frame) {
        frame.origin.y = superViewSize.height  - 216 - Bottom_iPhoneX_SPACE - NavigationBar_Bottom_Y;
        return frame;
    };
    
    UIImage *normal = [UIImage imageNamed:@"qjLoginSubmitBtn_bg"];
    model.loginBtnBgImgs = @[normal,normal,normal];
    
    model.loginBtnFrameBlock = ^CGRect(CGSize screenSize, CGSize superViewSize, CGRect frame) {
        frame.size.width = kScreenWidth - 2*37.5;
        frame.size.height = 48;
        frame.origin.y = superViewSize.height - 168 - Bottom_iPhoneX_SPACE - NavigationBar_Bottom_Y;
        frame.origin.x = 37.5;
        return frame;
    };
    model.checkBoxWH = 20;
    model.privacyConectTexts = @[@"与",@"及"];
    model.privacyOperatorIndex = 2;
    model.privacyFont = FontRegular(12);
    model.privacyColors = @[[UIColor colorWithHexString:@"#919599"],[UIColor colorWithHexString:@"#000000"]];
    model.privacyOperatorPreText = @"《";
    model.privacyOperatorSufText = @"》";
    model.privacyFrameBlock = ^CGRect(CGSize screenSize, CGSize superViewSize, CGRect frame) {
        frame.size.width = kScreenWidth - 37.5*2;
        frame.origin.x = 37.5;
        return frame;
    };
    
    UILabel *otherPhoneLoginLabel = [UILabel new];
    otherPhoneLoginLabel.font = kFont(12);
    otherPhoneLoginLabel.textColor = [UIColor grayColor];
    otherPhoneLoginLabel.text = @"其他手机号登录";
    otherPhoneLoginLabel.textAlignment = NSTextAlignmentRight;
    
    
    UIButton *_otherPhoneLoginBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_otherPhoneLoginBtn addTarget:self action:@selector(otherPhoneBtnAction) forControlEvents:UIControlEventTouchUpInside];
    _otherPhoneLoginBtn.backgroundColor = [UIColor clearColor];
    
    
    UILabel *_vLine = [UILabel new];
    _vLine.backgroundColor = [UIColor colorWithHexString:@"#919599"];
    
    
    UILabel *_thridLoginLabel = [UILabel new];
    _thridLoginLabel.font = kFont(12);
    _thridLoginLabel.textColor = [UIColor grayColor];
    _thridLoginLabel.text = @"其他方式登录";
    _thridLoginLabel.textAlignment = NSTextAlignmentLeft;
    
    
    UIButton *_thridLoginBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_thridLoginBtn addTarget:self action:@selector(thridLoginBtnAction) forControlEvents:UIControlEventTouchUpInside];
    _thridLoginBtn.backgroundColor = [UIColor clearColor];
    QJLoginBgView *backgroundView = [QJLoginBgView new];
    
    model.customViewBlock = ^(UIView * _Nonnull superCustomView) {
        [superCustomView addSubview:backgroundView];
        [superCustomView addSubview:otherPhoneLoginLabel];
        [superCustomView addSubview:_otherPhoneLoginBtn];
        [superCustomView addSubview:_vLine];
        [superCustomView addSubview:_thridLoginLabel];
        [superCustomView addSubview:_thridLoginBtn];
    };
    model.customViewLayoutBlock = ^(CGSize screenSize, CGRect contentViewFrame, CGRect navFrame, CGRect titleBarFrame, CGRect logoFrame, CGRect sloganFrame, CGRect numberFrame, CGRect loginFrame, CGRect changeBtnFrame, CGRect privacyFrame) {
        backgroundView.frame = CGRectMake(0,
                                          -CGRectGetMaxY(navFrame),
                                          contentViewFrame.size.width,
                                          contentViewFrame.size.height);
        _vLine.frame = CGRectMake(kScreenWidth/2.0 - 0.5, CGRectGetMaxY(loginFrame) + 27, 1, 10);
        
        [otherPhoneLoginLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.right.equalTo(_vLine.mas_left).offset(-40);
                    make.centerY.equalTo(_vLine);
        }];
        
        [_otherPhoneLoginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.center.equalTo(otherPhoneLoginLabel);
                    make.width.equalTo(@200);
                    make.height.equalTo(@25);
        }];
        
        [_thridLoginLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.equalTo(_vLine.mas_right).offset(40);
                    make.centerY.equalTo(_vLine);
                    
        }];
        
        
        [_thridLoginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.center.equalTo(_thridLoginLabel);
                    make.width.equalTo(@200);
                    make.height.equalTo(@25);
        }];
        
    };
    return model;
}


#pragma mark  setter && getter

- (BOOL)isCheck {
    return [objc_getAssociatedObject(self, _cmd) boolValue];
}

- (void)setIsCheck:(BOOL)isCheck {
    objc_setAssociatedObject(self, @selector(isCheck), @(isCheck), OBJC_ASSOCIATION_ASSIGN);
}

- (LoginOrRegisterSuccessBlock)loginOrRegisterSuccessBlock {
    return objc_getAssociatedObject(self, _cmd);
}

- (void)setLoginOrRegisterSuccessBlock:(LoginOrRegisterSuccessBlock)loginOrRegisterSuccessBlock {
    objc_setAssociatedObject(self, @selector(loginOrRegisterSuccessBlock), loginOrRegisterSuccessBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
}


#pragma mark ----- btnAction
- (void)thridLoginBtnAction {
    DLog(@"三方登录");
    if (!self.isCheck) {
        
        [[UIApplication sharedApplication].delegate.window  makeToast:@"请勾选同意《用户授权协议》和《隐私条款》" duration:3 position:CSToastPositionCenter image:[UIImage imageNamed:@"tips_toast"]];

        return;
    }
    
    QJThirdLoginViewController *vc = [QJThirdLoginViewController new];
    vc.loginOrRegisterSuccessBlock = self.loginOrRegisterSuccessBlock;
    QJNavigationController *nav = [[QJNavigationController alloc]initWithRootViewController:vc];
    nav.modalPresentationStyle = UIModalPresentationCustom;
    UINavigationController *navigationController = self.navigationController;
    if (self.presentedViewController) {
        //如果授权页成功拉起，这个时候则需要使用授权页的导航控制器进行跳转
        navigationController = (UINavigationController *)self.presentedViewController;
    }
    
    [navigationController presentViewController:nav animated:NO completion:nil];
    
}

- (void)otherPhoneBtnAction {
    DLog(@"其他手机号登录");
    if (!self.isCheck) {
        [[UIApplication sharedApplication].delegate.window  makeToast:@"请勾选同意《用户授权协议》和《隐私条款》" duration:3 position:CSToastPositionCenter image:[UIImage imageNamed:@"tips_toast"]];

        return;
    }
    [self phoneBtnAction];
}

/// 手机号登录
- (void)phoneBtnAction {
    QJFindWordViewController *vc = [QJFindWordViewController new];
    vc.loginOrRegisterSuccessBlock = self.loginOrRegisterSuccessBlock;
    vc.type = QJGetVerifyCodeVCType_phoneLogin;
    vc.isHiddenNavgationBar = YES;
    UINavigationController *navigationController = self.navigationController;
    if (self.presentedViewController) {
        //如果授权页成功拉起，这个时候则需要使用授权页的导航控制器进行跳转
        navigationController = (UINavigationController *)self.presentedViewController;
    }
    [navigationController pushViewController:vc animated:YES];
    
}



@end
