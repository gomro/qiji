//
//  UIView+QJActivityIndicator.m
//  QJBox
//
//  Created by wxy on 2022/6/2.
//

#import "UIView+QJActivityIndicator.h"


#define HUBViewTag  666


@implementation UIView (QJActivityIndicator)




///显示网络请求加载器
- (void)showHUDIndicator {
    [self showHUDIndicatorViewAtCenter:@""];
}

/// 隐藏网络请求加载器
- (void)hideHUDIndicator {
    [self hideHUDIndicatorViewAtCenter];
}


/// 显示网络请求加载器+文字
/// @param indiTitle 文字
- (void)showHUDIndicatorViewAtCenter:(NSString *)indiTitle {
    [self showHUDIndicatorViewAtCenter:indiTitle hiddenAfterDelay:0];
}


/// 展示加载器 多少秒后自动消失  大于0生效
/// @param indiTitle 文字
/// @param afterDelay 几秒 大于0生效
- (void)showHUDIndicatorViewAtCenter:(NSString*)indiTitle hiddenAfterDelay:(NSInteger) afterDelay {
    MBProgressHUD *hud = [self getHUDIndicatorViewAtCenter];
    if (hud == nil){
        hud = [self createHUDIndicatorViewAtCenter:indiTitle yOffset:0];
        [hud showAnimated:YES];
        
    }else{
        hud.label.text = indiTitle;
    }
    
    if (afterDelay > 0) {//大于生效
        [hud hideAnimated:YES afterDelay:afterDelay];
    }
}

/// 隐藏加载器
- (void)hideHUDIndicatorViewAtCenter {
    MBProgressHUD *hud = [self getHUDIndicatorViewAtCenter];
    
    [hud hideAnimated:YES];
}

/// 展示有一个距离初始位置y的偏移    加载器+文字
/// @param indiTitle 文字
/// @param y 偏移量
- (void)showHUDIndicatorViewAtCenter:(NSString *)indiTitle yOffset:(CGFloat)y {
    MBProgressHUD *hud = [self getHUDIndicatorViewAtCenter];
    if (hud == nil){
        hud = [self createHUDIndicatorViewAtCenter:indiTitle yOffset:y];
        [hud showAnimated:YES];
        
    }else{
        hud.label.text = indiTitle;
    }
    
}

/// 展示加载器包含文字  自定义mode
/// @param indiTitle 文字
/// @param mode 样式
- (void)showHUDIndicatorViewAtCenter:(NSString *)indiTitle mode:(MBProgressHUDMode)mode {
    MBProgressHUD *hud = [self getHUDIndicatorViewAtCenter];
    if (hud == nil){
        hud = [self createHUDIndicatorViewAtCenter:indiTitle yOffset:0];
        hud.mode = mode;
        [hud showAnimated:YES];
        
    }else{
        hud.mode = mode;
        hud.label.text = indiTitle;
    }
    
}

/// 创建一个加载器
/// @param indiTitle 文字
/// @param y 偏移
- (MBProgressHUD *)createHUDIndicatorViewAtCenter:(NSString *)indiTitle yOffset:(CGFloat)y {
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self];
    hud.layer.zPosition = 10;
    hud.graceTime = 0;//多少秒后展示
    hud.offset = CGPointMake(0, y);
    hud.removeFromSuperViewOnHide = YES;
    hud.label.text = indiTitle;
    [self addSubview:hud];
    hud.tag = HUBViewTag;
    return hud;
}


/// 得到一个加载器
- (MBProgressHUD *)getHUDIndicatorViewAtCenter {
    
    UIView *view = [self viewWithTagNotDeepCounting:HUBViewTag];
    
    if (view != nil && [view isKindOfClass:[MBProgressHUD class]]){
        
        return (MBProgressHUD *)view;
    }
    else
    {
        return nil;
    }
}


/// 展示一个自定义的加载器
/// @param customView  自定义视图
- (void)showHUDIndicatorWithCustomView:(id)customView {
    
    MBProgressHUD *hud = [self getHUDIndicatorViewAtCenter];
    if (hud == nil){
        hud = [self createHUDIndicatorViewAtCenter:@"" yOffset:0];
        hud.mode = MBProgressHUDModeCustomView;
        hud.customView = customView;
        [hud showAnimated:YES];
        
    }else{
        hud.mode = MBProgressHUDModeCustomView;
        hud.customView = customView;
        
    }
}




- (UIView *)viewWithTagNotDeepCounting:(NSInteger)tag
{
    for (UIView *view in self.subviews)
    {
        if (view.tag == tag) {
            return view;
            break;
        }
    }
    return nil;
}



@end
