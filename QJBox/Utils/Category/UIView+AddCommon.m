//
//  UIView+AddCommon.m
//  QJBox
//
//  Created by rui on 2022/6/27.
//

#import "UIView+AddCommon.h"

@implementation UIView (AddCommon)

- (void)showCorner:(CGFloat)radius rectCorner:(UIRectCorner)rectCorner{
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds byRoundingCorners:rectCorner cornerRadii:CGSizeMake(radius, radius)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.bounds;
    maskLayer.path = maskPath.CGPath;
    self.layer.mask = maskLayer;
}

//显示圆角
- (void)showCorner:(CGFloat)radius{
    self.layer.cornerRadius  = radius;
    self.layer.masksToBounds = YES;
}

/* 两个颜色渐变 左到右 */
- (void)graduateLeftColor:(UIColor* )startColor ToColor:(UIColor* )endColor {
    NSArray<CALayer *> *subLayers = self.layer.sublayers;
      if (subLayers.count > 0) {
          NSArray<CALayer *> *removedLayers = [subLayers filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id  _Nullable evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
              return [evaluatedObject isKindOfClass:[CAGradientLayer class]];
          }]];
          [removedLayers enumerateObjectsUsingBlock:^(CALayer * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
              [obj removeFromSuperlayer];
          }];
      }
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
    gradientLayer.frame = self.bounds;
    gradientLayer.startPoint = CGPointMake(0, 0);
    gradientLayer.endPoint = CGPointMake(1, 0);
    gradientLayer.colors = @[(__bridge id)startColor.CGColor,
                             (__bridge id)endColor.CGColor];
    gradientLayer.locations = @[@(0.0f), @(1.0f)];
    [self.layer insertSublayer:gradientLayer atIndex:0];
}

/* 两个颜色渐变 左到右 开始位置和结束位置 */
- (void)graduateLeftColor:(UIColor* )startColor ToColor:(UIColor* )endColor startPoint:(CGPoint)startPoint endPoint:(CGPoint)endPoint{
    NSArray<CALayer *> *subLayers = self.layer.sublayers;
      if (subLayers.count > 0) {
          NSArray<CALayer *> *removedLayers = [subLayers filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id  _Nullable evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
              return [evaluatedObject isKindOfClass:[CAGradientLayer class]];
          }]];
          [removedLayers enumerateObjectsUsingBlock:^(CALayer * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
              [obj removeFromSuperlayer];
          }];
      }
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
    gradientLayer.frame = self.bounds;
    gradientLayer.startPoint = startPoint;
    gradientLayer.endPoint = endPoint;
    gradientLayer.colors = @[(__bridge id)startColor.CGColor,
                             (__bridge id)endColor.CGColor];
    gradientLayer.locations = @[@(0.0f), @(1.0f)];
    [self.layer insertSublayer:gradientLayer atIndex:0];
}


- (void)viewShadowPathWithColor:(UIColor *)shadowColor shadowOpacity:(CGFloat)shadowOpacity shadowRadius:(CGFloat)shadowRadius shadowPathType:(QJShadowPathType)shadowPathType shadowPathWidth:(CGFloat)shadowPathWidth {
    
    self.layer.masksToBounds = NO;//必须要等于NO否则会把阴影切割隐藏掉
    self.layer.shadowColor = shadowColor.CGColor;// 阴影颜色
    self.layer.shadowOpacity = shadowOpacity;// 阴影透明度，默认0
    self.layer.shadowOffset = CGSizeZero;//shadowOffset阴影偏移，默认(0, -3),这个跟shadowRadius配合使用
    self.layer.shadowRadius = shadowRadius;//阴影半径，默认3
    CGRect shadowRect = CGRectZero;
    CGFloat originX,originY,sizeWith,sizeHeight;
    originX = 0;
    originY = 0;
    sizeWith = self.bounds.size.width;
    sizeHeight = self.bounds.size.height;
    
    if (shadowPathType == QJShadowPathTop) {
        shadowRect = CGRectMake(originX, originY-shadowPathWidth/2, sizeWith, shadowPathWidth);
    }else if (shadowPathType == QJShadowPathBottom){
        shadowRect = CGRectMake(originY, sizeHeight-shadowPathWidth/2, sizeWith, shadowPathWidth);
    }else if (shadowPathType == QJShadowPathLeft){
        shadowRect = CGRectMake(originX-shadowPathWidth/2, originY, shadowPathWidth, sizeHeight);
    }else if (shadowPathType == QJShadowPathRight){
        shadowRect = CGRectMake(sizeWith-shadowPathWidth/2, originY, shadowPathWidth, sizeHeight);
    }else if (shadowPathType == QJShadowPathCommon){
        shadowRect = CGRectMake(originX-shadowPathWidth/2, 2, sizeWith+shadowPathWidth, sizeHeight+shadowPathWidth/2);
    }else if (shadowPathType == QJShadowPathAround){
        shadowRect = CGRectMake(originX-shadowPathWidth/2, originY-shadowPathWidth/2, sizeWith+shadowPathWidth, sizeHeight+shadowPathWidth);
    }
    UIBezierPath *bezierPath = [UIBezierPath bezierPathWithRect:shadowRect];
    self.layer.shadowPath = bezierPath.CGPath;//阴影路径
}

#pragma mark - 控件虚线边框
/**
 给控件设置虚线边框

 @param strokeColor 边框颜色
 @param fillColor 控件填充颜色
 @param rect 控件的bound
 @param width 虚线的宽度
 @param pattern 虚线的间隔：数组
 @param radius 控件的圆角
 @return layer
 */
-(CAShapeLayer *)newLayer:(UIColor *)strokeColor fillColor:(UIColor *)fillColor rect:(CGRect)rect width:(CGFloat)width pattern:(NSArray *)pattern radius:(CGFloat)radius{
    
    CAShapeLayer *layer = [CAShapeLayer layer];
    layer.strokeColor = strokeColor.CGColor;
    layer.fillColor = fillColor.CGColor;
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:rect cornerRadius:radius];
    layer.path = path.CGPath;
    layer.frame = rect;
    layer.lineWidth = width;
    layer.lineDashPattern = pattern;
    layer.masksToBounds = YES;
    return layer;
}

@end
