

#import <UIKit/UIKit.h>

@interface UILabel (Common)

/**
 *  改变行间距
 */
+ (void)changeLineSpaceForLabel:(UILabel *)label WithSpace:(float)space;

/**
 *  改变字间距
 */
+ (void)changeWordSpaceForLabel:(UILabel *)label WithSpace:(float)space;

/**
 *  改变行间距和字间距
 */
+ (void)changeSpaceForLabel:(UILabel *)label withLineSpace:(float)lineSpace WordSpace:(float)wordSpace;
/**
 *  居下显示
 */
-(void)alignBottom;

/**
 *  获取Label文字内容的行数
 *
 *  @return 返回行数
 */
- (CGFloat)numberOfText;

/*
 *************************** 单行 自适应宽高
 */
- (void)setSingleStr:(NSString *)str;

- (void)setSingleStr:(NSString *)str withHeight:(CGFloat)height;

- (void)setSingleStr:(NSString *)str withHeight:(CGFloat)height withSpace:(CGFloat)space;

/*
 * 不同字体
 */
- (void)setSingleStr:(NSString *)str range:(NSRange)range font:(UIFont *)font;

/*
 * 不同颜色
 */
- (void)setSingleStr:(NSString *)str range:(NSRange)range color:(UIColor *)color;

/*
 * 不同颜色和不同颜色
 */
- (void)setSingleStr:(NSString *)str range:(NSRange)range font:(UIFont *)font color:(UIColor *)color numberOfLines:(CGFloat)number;

/*
 *单行 自适应宽高 不同字体 数组
 */
- (void)setSingleStr:(NSString *)str andArrayModel:(NSMutableArray *)modelArray;

/*
 单行 最宽不过设置的宽度
 */
- (void)setSingleStrFitWidth:(NSString *)str;

/*
单行 不足一行按照实际宽度 超过最大值按照最大值并且省略号(主要是防止超出屏幕)
*/
- (void)setSingleStr:(NSString *)str maxWidth:(CGFloat)maxWidth;

/*
单行 数组 颜色 字体 不足一行按照实际宽度 超过最大值按照最大值并且省略号(主要是防止超出屏幕)
*/
- (void)setSingleStr:(NSString *)str andArrayModel:(NSMutableArray *)modelArray maxWidth:(CGFloat)maxWidth;

#pragma mark - 单行
/*
 *  单行自适应宽度
*/
- (void)setSingleStr:(NSString *)str height:(CGFloat)height maxWidth:(CGFloat)maxWidth;

/*
 *  单行自适应宽度 关键字变色
*/
- (void)setSingleStr:(NSString *)str keyStr:(NSString *)keyStr color:(UIColor *)color height:(CGFloat)height maxWidth:(CGFloat)maxWidth ;

@end
