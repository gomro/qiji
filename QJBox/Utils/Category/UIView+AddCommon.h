//
//  UIView+AddCommon.h
//  QJBox
//
//  Created by rui on 2022/6/27.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN


typedef NS_ENUM(NSInteger , QJShadowPathType) {
    QJShadowPathTop    = 1,
    QJShadowPathBottom = 2,
    QJShadowPathLeft   = 3,
    QJShadowPathRight  = 4,
    QJShadowPathCommon = 5,
    QJShadowPathAround = 6,
    
};

@interface UIView (AddCommon)

/**
 * @author: zjr
 * @date: 2022-6-27
 * @desc: 指定某个边角设置圆角，radius--幅度 rectCorner---边角
 */
- (void)showCorner:(CGFloat)radius rectCorner:(UIRectCorner)rectCorner;
- (void)showCorner:(CGFloat)radius;

/**
 * @author: zjr
 * @date: 2022-6-28
 * @desc: 两个颜色渐变 左到右 设置颜色
 */
- (void)graduateLeftColor:(UIColor* _Nullable )startColor ToColor:(UIColor *_Nullable)endColor;

/**
 * @author: zjr
 * @date: 2022-6-28
 * @desc: 两个颜色渐变 左到右 设置颜色，开始位置，结束位置
 * startPoint与endPoint分别为渐变的起始方向与结束方向，它是以矩形的四个角为基础的，（0，0）为左上角、（1，0）为右上角、（0，1）为左下角、（1，1）为右下角
 */
- (void)graduateLeftColor:(UIColor* )startColor ToColor:(UIColor* )endColor startPoint:(CGPoint)startPoint endPoint:(CGPoint)endPoint;


/**
 给UIView添加阴影

 @param shadowColor 阴影颜色
 @param shadowOpacity 阴影透明度 默认0
 @param shadowRadius 阴影半径 也就是阴影放射程度 默认3
 @param shadowPathType 阴影方向
 @param shadowPathWidth 阴影放射g宽度
 */
- (void)viewShadowPathWithColor:(UIColor *)shadowColor shadowOpacity:(CGFloat)shadowOpacity shadowRadius:(CGFloat)shadowRadius shadowPathType:(QJShadowPathType)shadowPathType shadowPathWidth:(CGFloat)shadowPathWidth;

/**
 给控件设置虚线边框

 @param strokeColor 边框颜色
 @param fillColor 控件填充颜色
 @param rect 控件的bound
 @param width 虚线的宽度
 @param pattern 虚线的间隔：数组
 @param radius 控件的圆角
 @return layer
 */
-(CAShapeLayer *)newLayer:(UIColor *)strokeColor fillColor:(UIColor *)fillColor rect:(CGRect)rect width:(CGFloat)width pattern:(NSArray *)pattern radius:(CGFloat)radius;

@end

NS_ASSUME_NONNULL_END
