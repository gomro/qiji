//
//  UITableView+Common.m
//  QJBox
//
//  Created by rui on 2022/6/28.
//

#import "UITableView+Common.h"

#import "QJEmptyView.h"
#import "QJEmptyBlockView.h"

@implementation UITableView (Common)

- (void)tableViewDisplayWhenHaveNoDataWithMsg:(NSString *)message centerImage:(UIImage *)centerImage  ifNecessaryForRowCount:(NSInteger)rowCount {
    if (rowCount == 0) {
        QJEmptyView *emptyView = [[QJEmptyView alloc] initWithMessage:message image:centerImage];
        self.backgroundView = emptyView;
        self.separatorStyle = UITableViewCellSeparatorStyleNone;
    } else {
        if (!(floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_8_4)) {
            for (UIView *subView in self.subviews) {
                if ([subView isKindOfClass:[QJEmptyView class]]) {
                    [subView removeFromSuperview];
                }
            }
        }
        [self.backgroundView removeFromSuperview];
        self.backgroundView = nil;
        self.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
}

- (void)tableViewDisplayWhenHaveNoDataWithMsg:(NSString *)message centerImage:(UIImage *)centerImage  ifNecessaryForRowCount:(NSInteger)rowCount Title:(NSString *)title TitleColor:(NSString *)titleColor BackGroundColor:(NSString *)backGroundColor Complete:(void(^ __nullable)(void))complete{
    if (rowCount == 0) {
        QJEmptyBlockView *emptyView = [[QJEmptyBlockView alloc] initWithMessage:message image:centerImage title:title titleColor:titleColor BGColor:backGroundColor];
        emptyView.emptyClick = ^{
            if (complete) {
                complete();
            }
        };
        self.backgroundView = emptyView;
        self.separatorStyle = UITableViewCellSeparatorStyleNone;
    } else {
        if (!(floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_8_4)) {
            for (UIView *subView in self.subviews) {
                if ([subView isKindOfClass:[QJEmptyBlockView class]]) {
                    [subView removeFromSuperview];
                }
            }
        }
        [self.backgroundView removeFromSuperview];
        self.backgroundView = nil;
        self.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
}

- (void)tableViewDisplayWhenHaveNoDataWithMsg:(NSString *)message centerImage:(UIImage *)centerImage  ifNecessaryForRowCount:(NSInteger)rowCount Title:(NSString *)title Complete:(void(^ __nullable)(void))complete {
    if (rowCount == 0) {
        QJEmptyBlockView *emptyView = [[QJEmptyBlockView alloc] initWithMessage:message image:centerImage title:title];
        emptyView.emptyClick = ^{
            if (complete) {
                complete();
            }
        };
        self.backgroundView = emptyView;
        self.separatorStyle = UITableViewCellSeparatorStyleNone;
    } else {
        if (!(floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_8_4)) {
            for (UIView *subView in self.subviews) {
                if ([subView isKindOfClass:[QJEmptyBlockView class]]) {
                    [subView removeFromSuperview];
                }
            }
        }
        [self.backgroundView removeFromSuperview];
        self.backgroundView = nil;
        self.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
}

- (void)tableViewDisplayWhenHaveNoDataWithView:(UIView *)view ifNecessaryForRowCount:(NSInteger)rowCount {
    if (rowCount == 0) {
        self.backgroundView = view;
        self.separatorStyle = UITableViewCellSeparatorStyleNone;
    } else {
        if (!(floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_8_4)) {
            for (UIView *subView in self.subviews) {
                if ([subView isKindOfClass:[view class]]) {
                    [subView removeFromSuperview];
                }
            }
        }
        [self.backgroundView removeFromSuperview];
        self.backgroundView = nil;
        self.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
}

@end
