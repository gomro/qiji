//
//  UICollectionView+Common.h
//  QJBox
//
//  Created by rui on 2022/7/7.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UICollectionView (Common)

/**
 table 没有数据时展示UI

 @param message     提示文字
 @param centerImage 中间提示图片
 @param rowCount    数据源数组元素个数
 */
- (void)collectionViewDisplayWhenHaveNoDataWithMsg:(NSString *)message centerImage:(UIImage *)centerImage  ifNecessaryForRowCount:(NSInteger)rowCount;

- (void)collectionViewDisplayWhenHaveNoDataWithView:(UIView *)view ifNecessaryForRowCount:(NSInteger)rowCount;

@end

NS_ASSUME_NONNULL_END
