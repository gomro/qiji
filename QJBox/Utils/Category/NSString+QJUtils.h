//
//  NSString+QJUtils.h
//  QJBox
//
//  Created by wxy on 2022/7/8.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSString (QJUtils)

- (NSString *)checkImageUrlString;

//获取随机文件名字 路径格式如下
//(image|video|doc|app)/13位时间戳_4位随机英文字符.文件后缀 image/1658803191217_asdq.jpg

+ (NSString *)getRandomFileName;

//根据时间戳获取文字  刚刚，1小时前，等等
+ (NSString *)getTimespString:(NSInteger)timesp;

//返回一个字典 bg:标签背景色，title:标题颜色
+ (NSDictionary *)getRandomLabelColorStyle;

/**转换字符（点赞数量等）*/
+ (NSString *)getConvertNumber:(NSInteger)number;


//观看量，点赞数，一万以内正常展示，超过一万变成1.2万
+ (NSString *)changeCountFromString:(NSString *)str;

/**转换字符（点赞数量等）110000->1.1w*/
+ (NSString *)getConvertTenThousandNumber:(NSInteger)number;

@end

NS_ASSUME_NONNULL_END
