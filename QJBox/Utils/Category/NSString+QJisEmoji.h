//
//  NSString+QJisEmoji.h
//  QJBox
//
//  Created by wxy on 2022/6/15.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSString (QJisEmoji)


+ (BOOL)isContainsTwoEmoji:(NSString *)string;

//是否是系统自带九宫格输入 yes-是 no-不是
+ (BOOL)isNineKeyBoard:(NSString *)string;

//判断第三方键盘中的表情
+ (BOOL)hasEmoji:(NSString*)string;

/* 获取多行字符串全部高度 */
- (CGFloat)getLongStringHeightWithFont:(UIFont *)font withWidth:(CGFloat)width withLineSpace:(CGFloat)lineSpace;
/* 获取字符串宽度 */
- (CGFloat)getWidthWithFont:(UIFont *)font withHeight:(CGFloat)height;
- (CGSize)getSizeWithFont:(UIFont *)font constrainedToSize:(CGSize)size;
/// 计算字符显示size
/// @param font 字体
/// @param size size
- (CGFloat)getWidthWithFont:(UIFont *)font constrainedToSize:(CGSize)size;


/**
 Date 转换 NSString
 (自定义 默认格式：@"yyyy-MM-dd HH:mm:ss")
 */
+ (NSString *)getStringForDate:(NSDate *)date format:(NSString *)format;

/** NSString 转换 Date
 (自定义 默认格式：@"yyyy-MM-dd HH:mm:ss")
 */
+ (NSDate *)getDateForString:(NSString *)string format:(NSString *)format;

/**
 DateNSString 转换 NSString
 (默认格式：@"yyyy-MM-dd HH:mm:ss")
 */
+ (NSString *)getStringForDateString:(NSString *)string byFormat:(NSString *)byFormat toFormat:(NSString *)toFormat;

@end

NS_ASSUME_NONNULL_END
