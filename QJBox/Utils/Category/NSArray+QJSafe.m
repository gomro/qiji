//
//  NSArray+QJSafe.m
//  QJBox
//
//  Created by wxy on 2022/5/31.
//

#import "NSArray+QJSafe.h"

@implementation NSArray (QJSafe)

- (id)safeObjectAtIndex:(NSUInteger)index
{
    if (self.count > index)
    {
        return [self objectAtIndex:index];
    }
    return nil;
}


@end



@implementation NSMutableArray (CheckIndex)

- (void)safeReplaceObjectAtIndex:(NSUInteger)index withObject:(id)object
{
    if (self.count > index ) {
        [self replaceObjectAtIndex:index withObject:object];
    }
}

- (void)safeAddObject:(id)anObject
{
//    NSAssert(anObject != nil, @"Argument must be non-nil");
    
    if (anObject) {
        [self addObject:anObject];
    }
}

- (void)safeRemoveObjectAtIndex:(NSUInteger)index
{
    if (self.count > index)
    {
        [self removeObjectAtIndex:index];
    }
}


@end
