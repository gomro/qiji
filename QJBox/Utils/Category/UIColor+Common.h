//
//  UIColor+Common.h
//  QJBox
//
//  Created by rui on 2022/7/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIColor (Common)

+ (UIColor *)colorWithHexString:(NSString *)stringToConvert andAlpha:(CGFloat)alpha;

@end

NS_ASSUME_NONNULL_END
