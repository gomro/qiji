//
//  UIViewController+QJ_Login.h
//  QJBox
//
//  Created by wxy on 2022/6/22.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIViewController (QJ_Login)

- (void)showLoginVCCompleted:(void (^)(BOOL isLogin))completionHandler;


///此方法只在登录业务中退出登录页面时使用,退出登录模块后
- (void)dismissLoginVC:(void (^)(void))completion;

/// 看是否登录了
- (void)checkLogin:(void (^)(BOOL isLogin))completionHandler;
@end

NS_ASSUME_NONNULL_END
