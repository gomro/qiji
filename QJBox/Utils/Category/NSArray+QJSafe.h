//
//  NSArray+QJSafe.h
//  QJBox
//
//  Created by wxy on 2022/5/31.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSArray (QJSafe)

- (id)safeObjectAtIndex:(NSUInteger)index;


@end


@interface NSMutableArray (CheckIndex)

- (void)safeReplaceObjectAtIndex:(NSUInteger)index withObject:(id)object;

- (void)safeAddObject:(id)anObject;

- (void)safeRemoveObjectAtIndex:(NSUInteger)index;



@end

NS_ASSUME_NONNULL_END
