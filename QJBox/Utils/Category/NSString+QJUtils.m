//
//  NSString+QJUtils.m
//  QJBox
//
//  Created by wxy on 2022/7/8.
//

#import "NSString+QJUtils.h"

@implementation NSString (QJUtils)



- (NSString *)checkImageUrlString {
    if ([self hasPrefix:@"http"]) {
        return self;
    } else if ([self hasPrefix:@"/"]) {
        return [NSString stringWithFormat:@"%@%@",[QJEnvironmentConfigure shareInstance].baseImageURL,self];
    } else {
        DLog(@"baseUrl = %@",[QJEnvironmentConfigure shareInstance].baseImageURL);
        return [NSString stringWithFormat:@"%@/%@",[QJEnvironmentConfigure shareInstance].baseImageURL,self];
    }
    
}



+ (NSString *)getRandomFileName {
    
    NSDate *datenow = [NSDate date];
    NSString *timeSp = [NSString stringWithFormat:@"%ld", (long)[datenow timeIntervalSince1970]*1000];
    NSArray *arr = @[@"a",@"b",@"c",@"d",@"e",@"f",@"g",@"h",@"i",@"j",@"k",@"l",@"m",@"n",@"o",@"p",@"q",@"r",@"s",@"t",@"u",@"v",@"w",@"x",@"y",@"z",];
    NSMutableString *mustr = [[NSMutableString alloc]initWithString:@""];
    
    for (int i = 0; i < 4; i++) {
        int index = arc4random() %26;
        NSString *tem = [arr safeObjectAtIndex:index];
        [mustr appendString:tem];
    }
    return [NSString stringWithFormat:@"%@_%@",timeSp,mustr];
}

+ (NSString *)getTimespString:(NSInteger)timesp {
    NSString *str = @"";
    NSDate *date = [NSDate date];
    NSInteger distance = [date timeIntervalSince1970] - timesp;//单位秒
    //
    if (distance <= 60) {
        str = @"刚刚";
    }else if (60 < distance && distance < 3600) {
        NSInteger m = distance/60;
        str = [NSString stringWithFormat:@"%ld分钟前",m];
    }else if (distance >= 3600 && distance < 24*3600) {
        NSInteger h = distance/3600;
        str = [NSString stringWithFormat:@"%ld小时前",h];
    }else if (distance >= 24*3600 && distance <= 96*3600) {
        NSInteger d = distance/(3600*24);
        str = [NSString stringWithFormat:@"%ld天前",d];
    }else{
        NSDate *spDate = [NSDate dateWithTimeIntervalSince1970:timesp];
        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
        formatter.dateFormat = @"yyyy-MM-dd";
        str = [formatter stringFromDate:spDate];
    }
    
    return str;
    
}

+ (NSDictionary *)getRandomLabelColorStyle {
    NSArray *arr = @[@{@"bg":@"#F2E5FF",@"title":@"#7341A3"},@{@"bg":@"#FFE5E9",@"title":@"#A34152"},@{@"bg":@"#E5FBFF",@"title":@"#4193A3"}];
    int index = arc4random()%3;
    NSDictionary *tem = [arr safeObjectAtIndex:index];
    return tem;
}

+ (NSString *)getConvertNumber:(NSInteger)number {
    if (number > 999) {
        return @"999+";
    }
    return [NSString stringWithFormat:@"%ld", number];
}



//观看量，点赞数，一万以内正常展示，超过一万变成1.2万
+ (NSString *)changeCountFromString:(NSString *)str {
    
    NSInteger strCount = str.integerValue;
    if (strCount >= 10000) {
       CGFloat fCount = (strCount / 1000)/10.0;
        str = [NSString stringWithFormat:@"%.1fw",fCount];
    }
    return str;
    
}

+ (NSString *)getConvertTenThousandNumber:(NSInteger)number {
    if (number > 9999) {
        CGFloat convertNum = number / 10000.0;
        if (convertNum == 1) {
            return @"1.0w";
        }
        return [NSString stringWithFormat:@"%.1fw", convertNum];
    }
    return [NSString stringWithFormat:@"%ld", number];

}
@end
