//
//  UIView+QJActivityIndicator.h
//  QJBox
//
//  Created by wxy on 2022/6/2.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIView (QJActivityIndicator)


///显示网络请求加载器
- (void)showHUDIndicator;

/// 隐藏网络请求加载器
- (void)hideHUDIndicator;


/// 显示网络请求加载器+文字
/// @param indiTitle 文字
- (void)showHUDIndicatorViewAtCenter:(NSString *)indiTitle;


/// 展示加载器 多少秒后自动消失 大于0生效
/// @param indiTitle 文字
/// @param afterDelay 几秒
- (void)showHUDIndicatorViewAtCenter:(NSString*)indiTitle hiddenAfterDelay:(NSInteger) afterDelay;

/// 隐藏加载器
- (void)hideHUDIndicatorViewAtCenter;

/// 展示有一个距离初始位置y的偏移    加载器+文字
/// @param indiTitle 文字
/// @param y 偏移量
- (void)showHUDIndicatorViewAtCenter:(NSString *)indiTitle yOffset:(CGFloat)y;

/// 展示加载器包含文字  自定义mode
/// @param indiTitle 文字
/// @param mode 样式
- (void)showHUDIndicatorViewAtCenter:(NSString *)indiTitle mode:(MBProgressHUDMode)mode;

/// 创建一个加载器
/// @param indiTitle 文字
/// @param y 偏移
- (MBProgressHUD *)createHUDIndicatorViewAtCenter:(NSString *)indiTitle yOffset:(CGFloat)y;

/// 得到一个加载器
- (MBProgressHUD *)getHUDIndicatorViewAtCenter;

/// 展示一个自定义的加载器
/// @param customView  自定义视图
- (void)showHUDIndicatorWithCustomView:(id)customView;


@end

NS_ASSUME_NONNULL_END
