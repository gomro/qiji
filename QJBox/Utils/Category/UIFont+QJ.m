//
//  UIFont+QJ.m
//  QJBox
//
//  Created by wxy on 2022/10/19.
//

#import "UIFont+QJ.h"

@implementation UIFont (QJ)
/// SF-Pro-Display-HeavyItalic 斜体加粗
/// @param fontSize 字号
/// @param bold 加粗
/// @param italic 斜体
/// @param weight 加粗量级
+ (UIFont *)QJDisplayFontWithSize:(CGFloat)fontSize
                             bold:(BOOL)bold itatic:(BOOL)italic weight:(UIFontWeight)weight  {

    UIFont *font = [UIFont systemFontOfSize:kWScale*fontSize weight:weight];
    
    UIFontDescriptorSymbolicTraits symbolicTraits = 0;
    CGAffineTransform matrix = CGAffineTransformMake(1, 0, tanf(15 * (CGFloat)M_PI / 180), 1, 0, 0);
    NSValue *affineTransformValue = [NSValue valueWithCGAffineTransform:matrix];
    if (italic) {
        symbolicTraits |= UIFontDescriptorTraitItalic;
    }
    if (bold) {
        symbolicTraits |= UIFontDescriptorTraitBold;
    }
    UIFontDescriptor *desec = [[font fontDescriptor] fontDescriptorWithSymbolicTraits:symbolicTraits];
    if([UIDevice currentDevice].systemVersion.floatValue >=16.0){
        desec = [desec fontDescriptorByAddingAttributes:@{UIFontDescriptorMatrixAttribute:affineTransformValue}];
    }
  
    UIFont *specialFont = [UIFont fontWithDescriptor:desec size:font.pointSize];
    return specialFont;
}
@end
