//
//  QJCustomSegment.m
//  QJBox
//
//  Created by rui on 2022/6/28.
//

#import "QJCustomSegment.h"

@interface QJCustomSegment(){
    NSArray *widthArray;
    NSInteger _allButtonW;
    UIView *_divideView;
    UIView *_divideLineView;
}

@end

@implementation QJCustomSegment

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height-0.5)];
        _scrollView.clipsToBounds = YES;
        _scrollView.backgroundColor = [UIColor whiteColor];
        _scrollView.showsHorizontalScrollIndicator = NO;
        [self addSubview:_scrollView];
        _divideLineView = [[UIView alloc] init];
        _divideLineView.backgroundColor = [UIColor clearColor];
        [_scrollView addSubview:_divideLineView];
        _divideView  = [[UIView alloc] init];
        _divideView.backgroundColor = MainColor;
        [_scrollView addSubview:_divideView];
    }
    return self;
}

- (UIFont*)textFont{
    return _textFont? : kFont(14);
}

- (void)setTextSelectFont:(UIFont *)textSelectFont{
    _textSelectFont = textSelectFont;
}

- (void)setTextNormalFont:(UIFont *)textNormalFont{
    _textNormalFont = textNormalFont;
}

- (void)setBgColor:(UIColor *)bgColor{
    _bgColor = bgColor;
    _scrollView.backgroundColor = _bgColor;
}

- (void)updateChannels:(NSArray*)array{
    if (_hiddenBottomLine) {
        _divideView.hidden = YES;
        _divideLineView.hidden = YES;
    }
    NSMutableArray *widthMutableArray = [NSMutableArray array];
    NSInteger totalW = 0;
    for (int i = 0; i < array.count; i++) {
        
        NSString *string = [array objectAtIndex:i];
        CGFloat buttonW = [string boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, 20) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:self.textFont} context:nil].size.width + (self.itemSpacing == 0 ? 20 : self.itemSpacing);
        [widthMutableArray addObject:@(buttonW)];
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(totalW, 0, buttonW, self.bounds.size.height)];
        button.tag = 1000 + i;
        [button.titleLabel setFont:self.textFont];
        [button setTitleColor:_textColor ? _textColor : UIColorFromRGB(0X444444) forState:UIControlStateNormal];
        [button setTitleColor:_textSelectColor ? _textSelectColor :MainColor forState:UIControlStateSelected];
        [button setTitle:string forState:UIControlStateNormal];
        [button addTarget:self action:@selector(clickSegmentButton:) forControlEvents:UIControlEventTouchUpInside];
        [self.scrollView addSubview:button];
        totalW += buttonW;
        
        if (i == 0) {
            [button setSelected:YES];
            if (_changeFontSize) {
                button.titleLabel.font = self.textSelectFont ? self.textSelectFont : kboldFont(17);
            }
            if (!_hiddenBottomLine) {
                _divideView.frame = CGRectMake((self.itemSpacing == 0 ? 20 : self.itemSpacing) / 2.0, _scrollView.bounds.size.height-2, buttonW - (self.itemSpacing == 0 ? 20 : self.itemSpacing), 2);
            }
            _selectedIndex = 0;
        }
        
    }
    
    _allButtonW = totalW;
    _scrollView.contentSize = CGSizeMake(totalW,0);
    widthArray = [widthMutableArray copy];
}

- (void)updateGradualChannels:(NSArray*)array{
    if (_hiddenBottomLine) {
        _divideView.hidden = YES;
        _divideLineView.hidden = YES;
    }
    NSMutableArray *widthMutableArray = [NSMutableArray array];
    NSInteger totalW = 0;
    for (int i = 0; i < array.count; i++) {
        NSString *string = [array objectAtIndex:i];
        CGFloat buttonW = [string boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, 20) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:self.textFont} context:nil].size.width + (self.itemSpacing == 0 ? 20 : self.itemSpacing);
        [widthMutableArray addObject:@(buttonW)];
        UIButton *button;

        button = [[UIButton alloc] initWithFrame:CGRectMake(totalW, 0, buttonW, self.bounds.size.height)];
        button.tag = 1000 + i;
        [button.titleLabel setFont:self.textFont];
        [button setTitleColor:_textColor ? _textColor : UIColorFromRGB(0X444444) forState:UIControlStateNormal];
        [button setTitleColor:_textSelectColor ? _textSelectColor :MainColor forState:UIControlStateSelected];
        [button setTitle:string forState:UIControlStateNormal];
        [button addTarget:self action:@selector(clickMedalSegmentButton:) forControlEvents:UIControlEventTouchUpInside];
        [self.scrollView addSubview:button];
        
        totalW += buttonW;
        
        if (i == 0) {
            [button setSelected:YES];
            if (_changeFontSize) {
                button.titleLabel.font = self.textSelectFont ? self.textSelectFont : kboldFont(17);
            }
            if (!_hiddenBottomLine) {
                _divideView.frame = CGRectMake((self.itemSpacing == 0 ? 20 : self.itemSpacing) / 2.0-2, _scrollView.bounds.size.height-16, buttonW - (self.itemSpacing == 0 ? 20 : self.itemSpacing)+4, 6);
                [_divideView graduateLeftColor:[UIColor colorWithHexString:@"0xFFED5C"] ToColor:[UIColor colorWithHexString:@"0xFFCE06"] startPoint:CGPointMake(0.96, 0.5) endPoint:CGPointMake(0.05, 0.5)];
                _divideView.layer.cornerRadius = 3;
                _divideView.layer.masksToBounds = YES;
                _divideView.backgroundColor = [UIColor clearColor];
            }
            _selectedIndex = 0;
        }
        
    }
    
    _allButtonW = totalW;
    _scrollView.contentSize = CGSizeMake(totalW,0);
    widthArray = [widthMutableArray copy];
}

- (void)updateEquelWidthChannels:(NSArray*)array{//等间距
    if (_hiddenBottomLine) {
        _divideView.hidden = YES;
        _divideLineView.hidden = YES;
    }
    for (id obj in self.scrollView.subviews) {
        if ([obj isKindOfClass:[UIButton class]]) {
            [obj removeFromSuperview];
        }
    }
    
    NSMutableArray *widthMutableArray = [NSMutableArray array];
    NSInteger totalW = 0;
    for (int i = 0; i < array.count; i++) {
        NSString *string = [array objectAtIndex:i];
        CGFloat buttonW = self.bounds.size.width/array.count;
        if (array.count == 1) {
            buttonW = self.bounds.size.width/2;
        }
        [widthMutableArray addObject:@(buttonW)];
        
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(totalW, 0, buttonW, self.bounds.size.height)];
        button.tag = 1000 + i;
        [button.titleLabel setFont:self.textFont];
        [button setTitleColor:_textColor ? _textColor : UIColorFromRGB(0X444444) forState:UIControlStateNormal];
        [button setTitleColor:_textSelectColor ? _textSelectColor :MainColor forState:UIControlStateSelected];
        [button setTitle:string forState:UIControlStateNormal];
        [button addTarget:self action:@selector(clickCustomSegmentButton:) forControlEvents:UIControlEventTouchUpInside];
        [self.scrollView addSubview:button];
        totalW += buttonW;
        
        if (i == 0) {
            [button setSelected:YES];
            if (_changeFontSize) {
                button.titleLabel.font = self.textSelectFont;
            }
            if (!_hiddenBottomLine) {
                _divideView.frame = CGRectMake(0, _scrollView.bounds.size.height-2, buttonW, 2);
            }
            _selectedIndex = 0;
        }
    }
    _allButtonW = totalW;
    _scrollView.contentSize = CGSizeMake(totalW,0);
    widthArray = [widthMutableArray copy];
}

- (void)clickMedalSegmentButton:(UIButton*)selectedButton{
    UIButton *oldSelectButton = (UIButton*)[_scrollView viewWithTag:(1000 + _selectedIndex)];
    
    [oldSelectButton setSelected:NO];
    if (_changeFontSize) {
        oldSelectButton.titleLabel.font = self.textFont ? self.textFont : kFont(14);
        selectedButton.titleLabel.font = self.textSelectFont ? self.textSelectFont : kboldFont(17);
    }
    [selectedButton setSelected:YES];

    if (_selectedIndex != selectedButton.tag - 1000) {
        _selectedIndex = selectedButton.tag - 1000;
        if ([_delegate respondsToSelector:@selector(QJSegment:didSelectIndex:)]) {
            [_delegate QJSegment:self didSelectIndex:_selectedIndex];
        }
    }
    [self handMedalSlider];
}

- (void)clickSegmentButton:(UIButton*)selectedButton{
    UIButton *oldSelectButton = (UIButton*)[_scrollView viewWithTag:(1000 + _selectedIndex)];
    
    [oldSelectButton setSelected:NO];
    if (_changeFontSize) {
        oldSelectButton.titleLabel.font = self.textFont ? self.textFont : kFont(14);
        selectedButton.titleLabel.font = self.textSelectFont ? self.textSelectFont : kboldFont(17);
    }
    [selectedButton setSelected:YES];
    if (_selectedIndex != selectedButton.tag - 1000) {
        _selectedIndex = selectedButton.tag - 1000;
        if ([_delegate respondsToSelector:@selector(QJSegment:didSelectIndex:)]) {
            [_delegate QJSegment:self didSelectIndex:_selectedIndex];
        }
    }
    [self handleSlider];
}

- (void)clickCustomSegmentButton:(UIButton*)selectedButton{
    UIButton *oldSelectButton = (UIButton*)[_scrollView viewWithTag:(1000 + _selectedIndex)];
    
    [oldSelectButton setSelected:NO];
    if (_changeFontSize) {
        oldSelectButton.titleLabel.font = self.textNormalFont;
        selectedButton.titleLabel.font = self.textSelectFont;
    }
    oldSelectButton.backgroundColor = _itemBgColor ? _itemBgColor : [UIColor clearColor];
    selectedButton.backgroundColor = _itemSelectBgColor ? _itemSelectBgColor : [UIColor clearColor];

    [selectedButton setSelected:YES];
    if (_selectedIndex != selectedButton.tag - 1000) {
        _selectedIndex = selectedButton.tag - 1000;
        if ([_delegate respondsToSelector:@selector(QJSegment:didSelectIndex:)]) {
            [_delegate QJSegment:self didSelectIndex:_selectedIndex];
        }
    }
    [self handleSlider];
}

- (void)handMedalSlider {
    NSInteger totalW = 0;
    for (int i=0; i<_selectedIndex; i++) {
        totalW += [[widthArray objectAtIndex:i] integerValue];
    }
    
    //处理边界
    CGFloat selectW = [[widthArray objectAtIndex:_selectedIndex] integerValue];
    CGFloat offset = totalW + (selectW - self.bounds.size.width) *0.5 ;
    offset = MIN(_allButtonW - self.bounds.size.width, MAX(0, offset));
    if (_scrollView.contentSize.width > QJScreenWidth) {
        [_scrollView setContentOffset:CGPointMake(offset, 0) animated:YES];
    }
    
    if (!_hiddenBottomLine) {
        //滑块
        [UIView animateWithDuration:0.1 animations:^{
            self->_divideView.frame = CGRectMake(totalW + (self.itemSpacing == 0 ? 20 : self.itemSpacing) / 2.0 - 2, self->_divideView.frame.origin.y, selectW - (self.itemSpacing == 0 ? 20 : self.itemSpacing) + 4, self->_divideView.frame.size.height);
            [self->_divideView graduateLeftColor:[UIColor colorWithHexString:@"0xFFED5C"] ToColor:[UIColor colorWithHexString:@"0xFFCE06"] startPoint:CGPointMake(0.96, 0.5) endPoint:CGPointMake(0.05, 0.5)];
            self->_divideView.layer.cornerRadius = 3;
            self->_divideView.layer.masksToBounds = YES;
        }];
    }
}

- (void)handleSlider {
    NSInteger totalW = 0;
    for (int i=0; i<_selectedIndex; i++) {
        totalW += [[widthArray objectAtIndex:i] integerValue];
    }
    
    //处理边界
    CGFloat selectW = [[widthArray objectAtIndex:_selectedIndex] integerValue];
    CGFloat offset = totalW + (selectW - self.bounds.size.width) *0.5 ;
    offset = MIN(_allButtonW - self.bounds.size.width, MAX(0, offset));
    if (_scrollView.contentSize.width > QJScreenWidth) {
        [_scrollView setContentOffset:CGPointMake(offset, 0) animated:YES];
        
    }
    if (!_hiddenBottomLine) {
        //滑块
        [UIView animateWithDuration:0.1 animations:^{
            self->_divideView.frame = CGRectMake(totalW + (self.itemSpacing == 0 ? 20 : self.itemSpacing) / 2.0, self->_divideView.frame.origin.y, selectW - (self.itemSpacing == 0 ? 20 : self.itemSpacing), self->_divideView.frame.size.height);
        }];
    }
}
- (void)didSelectToIndex:(NSInteger)index{
    UIButton *selectedButton = [_scrollView viewWithTag:(1000 + index)];
    UIButton *oldSelectButton = (UIButton*)[_scrollView viewWithTag:(1000 + _selectedIndex)];
    if (_changeFontSize) {
        oldSelectButton.titleLabel.font = self.textNormalFont?self.textNormalFont:kFont(14);
        selectedButton.titleLabel.font = self.textSelectFont?self.textSelectFont:kboldFont(17);
    }
    [oldSelectButton setSelected:NO];
    [selectedButton setSelected:YES];
    if (_showBgColor) {
        oldSelectButton.backgroundColor = _itemBgColor ? _itemBgColor : [UIColor clearColor];
        selectedButton.backgroundColor = _itemSelectBgColor ? _itemSelectBgColor : [UIColor clearColor];
    }
    _selectedIndex = index;
    [self handleSlider];
}

- (void)setSelectedIndex:(NSInteger)selectedIndex {
    if (_selectedIndex != selectedIndex) {
        [self didSelectToIndex:selectedIndex];
    }
}

@end
