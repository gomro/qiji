//
//  QJSegmentImageView.h
//  QJBox
//
//  Created by rui on 2022/7/13.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/* item宽度 */
typedef NS_ENUM(NSUInteger, QJSegmentViewWidthStyle) {
    QJSegmentViewWidthStyleAuto = 0, //自适应宽度
    QJSegmentViewWidthStyleEqual = 1,//等宽
};

@interface QJSegmentImageView : UIView

#pragma mark ------------ 自定义设置 ------------
/* 文字样式 */
@property (nonatomic, assign) QJSegmentViewWidthStyle segmentWidthStyle;
/* LLSegmentViewWidthStyleEqual ----item宽度 */
@property (nonatomic, assign) CGFloat itemWidth;
/* LLSegmentViewWidthStyleAuto ---- item文字到item偏移量 直接加到button*/
@property (nonatomic, assign) CGFloat itemSpace;
/* item到item间隔  默认设置为0 itemSpace满足不了需求时候使用 */
@property (nonatomic, assign) CGFloat itemSpaceAdd;
/* 默认字体颜色 */
@property (nonatomic, strong) UIColor *textColor;
/* 选中字体颜色 */
@property (nonatomic, strong) UIColor *selectedTextColor;
/* 默认字体大小 */
@property (nonatomic, strong) UIFont *font;
/* 选中字体大小 */
@property (nonatomic, strong) UIFont *selectedFont;
/* 第一个item到view左边的偏移量 */
@property (nonatomic, assign) CGFloat leftOffset;
/* 底部图片的顶部距离item文字底部位置 */
@property (nonatomic, assign) CGFloat centerYOfset;
/* 是否显示底部跟随线条*/
@property (nonatomic, assign) BOOL showBottomLine;
/* 默认选中 */
@property (nonatomic, assign) NSInteger defaultIndex;
/* 动画大小 */
@property (nonatomic, assign) CGSize imageSize;
//设置底部图片
@property (nonatomic, copy) NSString *selectedImage;

#pragma mark ------------ 数据 ------------
/* 显示文字数组 */
@property (nonatomic, strong) NSArray <NSString *>*titleArray;
/* item点击回调 */
@property (nonatomic,copy) void (^segmentedSelectedItemBlock)(NSInteger selectedIndex);
/*  手动设置选中位置 并点击*/
- (void)setSegmentCurIndex:(NSInteger)index;
- (void)setSegmentCurIndexAndBlock:(NSInteger)index;
- (NSInteger)getCurIndex;

@end


@interface NSString (LLAdd)
+ (float)getTextWidth:(float)textHeight text:(NSString *)text font:(UIFont *)font;
@end

NS_ASSUME_NONNULL_END

//- (LLSegmentImageView *)segment{
//    if (!_segment) {
//        _segment = [[LLSegmentImageView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 44)];
//        _segment.itemSpace = 12;
//        _segment.leftOffset = 15;
//        _segment.font = kFont(16);
//        _segment.selectedFont = kboldFont(18);
//        _segment.showBottomLine = NO;
//        _segment.backgroundColor = UIColorFromRGB(0xF7F7F7);
//        ESWeak_(self)
//        _segment.segmentedSelectedItemBlock = ^(NSInteger selectedIndex) {
//            [weak_self switchViewWithIndex:selectedIndex];
//        };
//    }
//    return _segment;
//}
