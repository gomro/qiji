//
//  QJSegmentLineView.m
//  QJBox
//
//  Created by rui on 2022/7/13.
//

#import "QJSegmentLineView.h"

static NSInteger const QJButtonTag = 200000;
static CGFloat const QJUnderlineDuration = 0.2f;

@interface QJSegmentLineView ()

/* scrollView */
@property (nonatomic, strong) UIScrollView *scrollView;
/* 跟随线条 */
@property (nonatomic, strong) UIView *bottomLine;
@property (nonatomic, strong) UIView *redView;
/* 底部分割线条 */
@property (nonatomic, strong) UIView *lineView;
/* 默认选中位置 */
@property (nonatomic, assign) NSInteger defaultIndex;

@end

@implementation QJSegmentLineView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self initParams];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initParams];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame withArray:(NSArray *)array{
    self = [super initWithFrame:frame];
    if (self) {
        [self initParams];
        self.titleArray = array;
    }
    return self;
}

- (void)initParams {
    /* 默认自适应 */
    self.segmentWidthStyle = QJSegmentViewWidthStyleAuto;
    self.textColor = UIColorFromRGB(0x333333);
    self.selectedTextColor = UIColorFromRGB(0x333333);
    self.font = kFont(16);
    self.selectedFont = kboldFont(18);
    self.initIndex = 0;
    self.showBottomLine = YES;
    self.lineWidthEqualToTextWidth = YES;
    self.showViewBottomLine = NO;
    self.lineColor = UIColorFromRGB(0xFFF100);
    self.lineHeight = 2;
    self.backgroundColor = UIColorFromRGB(0xf7f7f7);
}

-(void)setTitleArray:(NSArray<NSString *> *)titleArray{
    _titleArray = titleArray;
    if (_titleArray.count > 0) {
        [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        [self.scrollView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        [self buildingUI];
    }
}

- (void)buildingUI {
    if (self.titleArray.count < 1) {
        return;
    }
    self.defaultIndex = self.initIndex;

    [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [self.scrollView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    [self addSubview:self.scrollView];
    [self.scrollView addSubview:self.bottomLine];
    [self.scrollView addSubview:self.redView];
    [self addSubview:self.lineView];

    self.bottomLine.hidden = !self.showBottomLine;
    self.lineView.hidden = !self.showViewBottomLine;

    __weak typeof(self) weakSelf = self;
    __block UIView *lastView;
    
    [self.titleArray enumerateObjectsUsingBlock:^(NSString * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        /* 文字宽度 */
        float textWidth = [NSString getTextWidth:weakSelf.height text:obj font:weakSelf.selectedFont];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [weakSelf.scrollView addSubview:button];
        [button setTitle:obj forState:UIControlStateNormal];
        button.titleLabel.font = (idx == weakSelf.defaultIndex ? weakSelf.selectedFont :weakSelf.font);
        [button setTitleColor:(idx == weakSelf.defaultIndex ? weakSelf.selectedTextColor : weakSelf.textColor) forState:UIControlStateNormal];
        button.tag = QJButtonTag + idx;
        [button addTarget:self action:@selector(buttionClick:) forControlEvents:UIControlEventTouchUpInside];
       
        /* item宽度 */
        switch (weakSelf.segmentWidthStyle) {
            case QJSegmentViewWidthStyleEqual:{
                //等宽
                button.width =  weakSelf.itemWidth;
            }
                break;
            case QJSegmentViewWidthStyleAuto:{
                //自适应宽度
                button.width = textWidth + 2*weakSelf.itemTextOffset;
            }
                break;

            default:
                break;
        }

        button.height = weakSelf.height;
        button.left = lastView ? (lastView.right + weakSelf.itemSpace ) : (weakSelf.leftOffset - weakSelf.itemTextOffset);
        button.centerY = weakSelf.height/2;
       
        lastView = button;
    }];
    weakSelf.scrollView.contentSize = CGSizeMake(lastView.right+(weakSelf.leftOffset - weakSelf.itemTextOffset), self.height);
    
    self.redView.top = 20;
    self.redView.right = self.itemWidth*2 - self.itemWidth/2 + [NSString getTextWidth:self.height text:@"已完成" font:self.selectedFont]/2+5;
    self.redView.hidden = YES;
    
    if (self.bottomLine.superview) {
        self.bottomLine.height = self.lineHeight;
        [self.bottomLine showCorner:self.lineHeight/2];
        UIButton *selectedButton = [self.scrollView viewWithTag:QJButtonTag + self.defaultIndex];
        if (self.lineWidthEqualToTextWidth) {
            /* 文字底部 */
            self.bottomLine.width = [self getUnderlineWidth];
            self.bottomLine.bottom = self.scrollView.bottom-1;//self.height/2+self.selectedFont.pointSize/2+self.lineHeight/2 - self.lineYOffset;
        }else{
            /* view底部 */
            self.bottomLine.width = selectedButton.width;
            self.bottomLine.bottom = self.scrollView.bottom-1;
        }
        self.bottomLine.centerX = selectedButton.centerX;
    }
}

#pragma mark - 按钮点击

- (void)buttionClick:(UIButton *)button {
    if (button.tag - QJButtonTag == self.defaultIndex) {
        return;
    }
    [self setSegmentSelectedIndex:button.tag - QJButtonTag];
    if (self.segmentedItemSelectedBlock) {
        self.segmentedItemSelectedBlock(self, self.defaultIndex);
    }
}

- (void)setSegmentSelectedIndexAndBlock:(NSInteger)selectedIndex{
    if (selectedIndex == self.defaultIndex && selectedIndex) {
        return;
    }
    [self setSegmentSelectedIndex:selectedIndex];
    if (self.segmentedItemSelectedBlock) {
        self.segmentedItemSelectedBlock(self,selectedIndex);
    }
}

- (void)setSegmentSelectedIndex:(NSInteger)selectedIndex {
    if (selectedIndex == self.defaultIndex) {
        return;
    }
    /* 防止数组越界 */
    if (selectedIndex > self.titleArray.count - 1) {
        selectedIndex = self.titleArray.count - 1;
    }
    if (selectedIndex < 0) {
        selectedIndex = 0;
    }
    
    self.defaultIndex = selectedIndex;
    
    UIButton *selectedButton = [self.scrollView viewWithTag:QJButtonTag + selectedIndex];
  
    [UIView animateWithDuration:QJUnderlineDuration animations:^{
        if (self.lineWidthEqualToTextWidth) {
            self.bottomLine.width = [self getUnderlineWidth];
        }
        self.bottomLine.centerX = selectedButton.centerX;
    }];
    
    for (UIView *subview in self.scrollView.subviews) {
        if ([subview isKindOfClass:[UIButton class]]) {
            UIButton *button = (UIButton *)subview;
            [button setTitleColor:(button.tag == selectedButton.tag ? self.selectedTextColor : self.textColor) forState:UIControlStateNormal];
            button.titleLabel.font = (button.tag == selectedButton.tag ? self.selectedFont : self.font);
        }
    }
    
    CGFloat selectedSegmentOffset = self.width / 2 - selectedButton.width / 2;
    CGRect rectToScrollTo = selectedButton.frame;
    rectToScrollTo.origin.x -= selectedSegmentOffset;
    rectToScrollTo.size.width += selectedSegmentOffset * 2;
    [self.scrollView scrollRectToVisible:rectToScrollTo animated:YES];
}

- (void)showRedCount:(BOOL)isHidden{
    self.redView.hidden = isHidden;
}

- (void)scrollToOrigin{
    [self.scrollView setContentOffset:CGPointMake(0, 0) animated:NO];
}

#pragma mark - getter methods
- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] initWithFrame:self.bounds];
        _scrollView.backgroundColor = [UIColor clearColor];
        _scrollView.showsVerticalScrollIndicator = NO;
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.directionalLockEnabled = YES;
        _scrollView.bounces = YES;
        _scrollView.tag = 3022;
    }
    return _scrollView;
}

- (UIView *)bottomLine {
    if (!_bottomLine) {
        _bottomLine = [[UIView alloc] initWithFrame:CGRectMake(0, self.height - 1, self.width, 1)];
        _bottomLine.backgroundColor = self.lineColor;
    }
    return _bottomLine;
}

- (UIView *)redView {
    if (!_redView) {
        _redView = [[UIView alloc] initWithFrame:CGRectMake(0, self.height - 1, 4, 4)];
        _redView.layer.cornerRadius = 2;
        _redView.layer.masksToBounds = YES;
        _redView.backgroundColor = [UIColor redColor];
    }
    return _redView;
}

- (UIView *)lineView {
    if (!_lineView) {
        _lineView = [[UIView alloc] initWithFrame:CGRectMake(0, self.height - 0.5, self.width, 0.5)];
        _lineView.backgroundColor = UIColorFromRGB(0xeeeeee);
    }
    return _lineView;
}

- (CGFloat)getUnderlineWidth {
    NSString *selectedTitle = [self.titleArray safeObjectAtIndex:self.defaultIndex];
    float textWidth = [NSString getTextWidth:self.height text:selectedTitle font:self.selectedFont] - self.lineWidthOffset*2;
    return textWidth;
}

- (NSInteger)getCurIndex{
    return self.defaultIndex;
}

@end

@implementation NSString (LLAdd)

+ (float)getTextWidth:(float)textHeight text:(NSString *)text font:(UIFont *)font {
    if (!text.length) {
        return 0;
    }
    float origin = [text boundingRectWithSize:CGSizeMake(MAXFLOAT, textHeight) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName: font} context:nil].size.width;
    return ceilf(origin);
}

@end
