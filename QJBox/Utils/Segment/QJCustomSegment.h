//
//  QJCustomSegment.h
//  QJBox
//
//  Created by rui on 2022/6/28.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class QJCustomSegment;

@protocol QJSegmentDelegate <NSObject>

@optional
- (void)QJSegment:(QJCustomSegment*)segment didSelectIndex:(NSInteger)index;

@end

@interface QJCustomSegment : UIControl

/* 底部滚动视图 */
@property (nonatomic, strong) UIScrollView *scrollView;
/* 文字默认字体大小 */
@property (nonatomic, strong) UIFont *textFont;
/* 普通状态下文字字体大小 */
@property (nonatomic, strong) UIFont *textNormalFont;
/* 普通状态下文字字体大小 */
@property (nonatomic, strong) UIFont *textSelectFont;
/* 普通状态下文字颜色 */
@property (nonatomic, strong) UIColor *textColor;
/* 选中状态下文字颜色 */
@property (nonatomic, strong) UIColor *textSelectColor;
/* 选中item索引 */
@property (nonatomic, assign) NSInteger selectedIndex;
/* item 之间间隔 */
@property (nonatomic, assign) CGFloat itemSpacing;
@property (nonatomic, weak) id<QJSegmentDelegate> delegate;
/* 是否隐藏item底部线条 */
@property (nonatomic, assign) BOOL hiddenBottomLine;
/* 选中后是否改变字体大小 */
@property (nonatomic, assign) BOOL changeFontSize;
/* 是否现实item背景色， */
@property (nonatomic, assign) BOOL showBgColor;
@property (nonatomic, strong) UIColor *bgColor;
/* 普通状态item背景色 */
@property (nonatomic, strong) UIColor *itemBgColor;
/* 选中状态下item背景色 */
@property (nonatomic, strong) UIColor *itemSelectBgColor;

/* 普通布局 */
- (void)updateChannels:(NSArray*)array;
/* 渐变颜色滑块 */
- (void)updateGradualChannels:(NSArray*)array;
/* 等宽布局 */
- (void)updateEquelWidthChannels:(NSArray*)array;
- (void)didSelectToIndex:(NSInteger)index;

@end

NS_ASSUME_NONNULL_END
