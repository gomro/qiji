//
//  QJSegmentLineView.h
//  QJBox
//
//  Created by rui on 2022/7/13.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/* item宽度 */
typedef NS_ENUM(NSUInteger, QJSegmentViewWidthStyle) {
    QJSegmentViewWidthStyleAuto = 0, //自适应宽度
    QJSegmentViewWidthStyleEqual = 1,//等宽
};

@interface QJSegmentLineView : UIView

#pragma mark - @properties
/* 文字样式 */
@property (nonatomic, assign) QJSegmentViewWidthStyle segmentWidthStyle;
/* ------等宽--------- */
/* item宽度 */
@property (nonatomic, assign) CGFloat itemWidth;
/* ------自适应宽度--------- */
/* item文字到item偏移量 */
@property (nonatomic, assign) CGFloat itemTextOffset;
/* item到item间隔  默认设置为0 itemTextOffset满足不了需求时候使用 */
@property (nonatomic, assign) CGFloat itemSpace;

/* 第一个item到view左边的偏移量 */
@property (nonatomic, assign) CGFloat leftOffset;
/* 默认字体颜色 */
@property (nonatomic, strong) UIColor *textColor;
/* 选中字体颜色 */
@property (nonatomic, strong) UIColor *selectedTextColor;
/* 默认字体大小 */
@property (nonatomic, strong) UIFont *font;
/* 选中字体大小 */
@property (nonatomic, strong) UIFont *selectedFont;
/* 底部图片的底部距离item文字底部位置 */
@property (nonatomic, assign) CGFloat bottomOfset;
/* 是否显示底部跟随线条*/
@property (nonatomic, assign) BOOL showBottomLine;
@property (nonatomic, strong) UIColor *lineColor;
@property (nonatomic, assign) CGFloat lineHeight;
@property (nonatomic, assign) CGFloat lineWidthOffset;

/* 文字底部想上 */
@property (nonatomic, assign) CGFloat lineYOffset;
/* 底部跟随线条位于底部跟按钮同款 还是跟文字下面跟文字同款*/
@property (nonatomic, assign) BOOL lineWidthEqualToTextWidth;

/* 是否显示底部分割线条*/
@property (nonatomic, assign) BOOL showViewBottomLine;

#pragma mark - 数据
@property (nonatomic, assign) NSInteger initIndex;
/* 显示文字数组 */
@property (nonatomic, strong) NSArray <NSString *>*titleArray;
/* item点击回调 */
@property (nonatomic,copy) void (^segmentedItemSelectedBlock)(QJSegmentLineView *segment, NSInteger selectedIndex);

/*  手动设置选中位置*/
- (void)setSegmentSelectedIndex:(NSInteger)selectedIndex;
/*  手动设置选中位置 并点击*/
- (void)setSegmentSelectedIndexAndBlock:(NSInteger)selectedIndex;

- (NSInteger)getCurIndex;
/* 回到最左边 */
- (void)scrollToOrigin;

- (void)showRedCount:(BOOL)isHidden;

@end

@interface NSString (LLAdd)

+ (float)getTextWidth:(float)textHeight text:(NSString *)text font:(UIFont *)font;

@end

NS_ASSUME_NONNULL_END
