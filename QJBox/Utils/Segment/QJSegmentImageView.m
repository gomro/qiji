//
//  QJSegmentImageView.m
//  QJBox
//
//  Created by rui on 2022/7/13.
//

#import "QJSegmentImageView.h"

static NSInteger const QJButtonTag = 8000;

#pragma mark ---- QJSegmentItem

@interface QJSegmentItem : UIButton

@property (nonatomic, strong) UILabel *titleLab;
@property (nonatomic, strong) UIImageView *selectImage;
/* 底部图片的顶部距离item文字底部位置 */
@property (nonatomic, assign) CGFloat centerYOfset;
- (void)updateImageSize:(CGSize)size;
- (void)setText:(NSString *)titleStr;

@end

@implementation QJSegmentItem

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self createUI];
    }
    return self;
}

- (void)createUI {
    self.selectImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"segment_line"]];
    [self addSubview:self.selectImage];
    self.selectImage.contentMode = UIViewContentModeScaleAspectFill;
    [self.selectImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.centerY.equalTo(self).offset(16);
        make.size.mas_equalTo(CGSizeMake(24, 4));
    }];
    self.selectImage.hidden = YES;
    
    self.titleLab = [[UILabel alloc] init];
    [self addSubview:self.titleLab];
    [self.titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.centerY.equalTo(self);
    }];
}

- (void)updateImageSize:(CGSize)size{
    [self.selectImage mas_updateConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(size);
    }];
}

- (void)setSelected:(BOOL)selected {
    [super setSelected:selected];
    if (selected) {
        self.selectImage.hidden = NO;
    } else {
        self.selectImage.hidden = YES;
    }
}

-(void)setCenterYOfset:(CGFloat)centerYOfset{
    [self.selectImage mas_updateConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self).offset(centerYOfset);
    }];
}

- (void)setText:(NSString *)titleStr{
    self.titleLab.text = titleStr;
}

@end

#pragma mark ---- QJSegmentImageView

@interface QJSegmentImageView ()
/* scrollView */
@property (nonatomic, strong) UIScrollView *scrollView;
/* 底部分割线条 */
@property (nonatomic, strong) UIView *bottomLine;
/* 默认选中位置 */
@property (nonatomic, assign) NSInteger selectIndex;
@end

@implementation QJSegmentImageView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initParams];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame withArray:(NSArray *)array{
    self = [super initWithFrame:frame];
    if (self) {
        [self initParams];
        self.titleArray = array;
    }
    return self;
}

- (void)initParams {
    self.backgroundColor = UIColorFromRGB(0xffffff);
    self.segmentWidthStyle = QJSegmentViewWidthStyleAuto;
    self.textColor = UIColorFromRGB(0x333333);
    self.selectedTextColor = UIColorFromRGB(0x333333);
    self.font = kFont(16);
    self.selectedFont = FontSemibold(18);
    self.showBottomLine = NO;
    self.selectIndex = 0;
}

-(void)setTitleArray:(NSArray<NSString *> *)titleArray{
    _titleArray = titleArray;
    if (_titleArray.count > 0) {
        [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        [self.scrollView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        [self buildingUI];
    }
}

 

- (void)setSelectedImage:(NSString *)selectedImage {
    _selectedImage = selectedImage;
    [self buildingUI];
}

- (void)buildingUI {
    if (self.titleArray.count < 1) {
        return;
    }
    
    [self addSubview:self.scrollView];
    [self addSubview:self.bottomLine];

    self.bottomLine.hidden = !self.showBottomLine;
    self.selectIndex = self.defaultIndex;

    __weak typeof(self) weakSelf = self;
    __block UIView *lastView;
    [self.titleArray enumerateObjectsUsingBlock:^(NSString * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        /* 文字宽度 */
        float textWidth = idx == weakSelf.defaultIndex ? [NSString getTextWidth:weakSelf.height text:obj font:weakSelf.selectedFont] : [NSString getTextWidth:weakSelf.height text:obj font:weakSelf.font] ;
        
        QJSegmentItem *button = [[QJSegmentItem alloc] init];
        if (self.selectedImage) {
            button.selectImage.image = [UIImage imageNamed:self.selectedImage];
        }
        
        [weakSelf.scrollView addSubview:button];
        [button setText:obj];
        if (idx == weakSelf.defaultIndex) {
            button.selected = YES;
        }
        button.titleLab.font = (idx == weakSelf.defaultIndex ? weakSelf.selectedFont :weakSelf.font);
        button.titleLab.textColor = (idx == weakSelf.defaultIndex ? weakSelf.selectedTextColor :weakSelf.textColor);
        if (weakSelf.imageSize.width > 0) {
            [button updateImageSize:weakSelf.imageSize];
        }
        button.tag = QJButtonTag + idx;
        [button addTarget:self action:@selector(buttionClick:) forControlEvents:UIControlEventTouchUpInside];
        /* item宽度 */
        switch (weakSelf.segmentWidthStyle) {
            case QJSegmentViewWidthStyleEqual:{
                //等宽
                button.width =  weakSelf.itemWidth;
            }
                break;
            case QJSegmentViewWidthStyleAuto:{
                //自适应宽度
                button.width = textWidth + weakSelf.itemSpace;
            }
                break;

            default:
                break;
        }
        button.height = weakSelf.height;
        button.left = lastView ? (lastView.right + weakSelf.itemSpaceAdd) : (weakSelf.leftOffset - weakSelf.itemSpace/2);
        button.centerY = weakSelf.height/2;
        lastView = button;
    }];
    weakSelf.scrollView.contentSize = CGSizeMake(lastView.right+(weakSelf.leftOffset - weakSelf.itemSpace/2), self.height);
}

- (void)buttionClick:(UIButton *)button {
    if (button.tag - QJButtonTag == self.selectIndex) {
        return;
    }
    [self itemChangeWithIndex:button.tag - QJButtonTag];
    if (self.segmentedSelectedItemBlock) {
        self.segmentedSelectedItemBlock( self.selectIndex);
    }
}

/*  手动设置选中位置 并点击*/
- (void)setSegmentCurIndex:(NSInteger)index{
    if (index == self.selectIndex && index) {
        return;
    }
    [self itemChangeWithIndex:index];
}

- (void)setSegmentCurIndexAndBlock:(NSInteger)index{
    if (index == self.selectIndex && index) {
        return;
    }
    [self itemChangeWithIndex:index];
    if (self.segmentedSelectedItemBlock) {
        self.segmentedSelectedItemBlock( self.selectIndex);
    }
}

- (void)itemChangeWithIndex:(NSInteger)index {
    /* 防止数组越界 */
    if (index > self.titleArray.count - 1) {
        index = self.titleArray.count - 1;
    }
    if (index < 0) {
        index = 0;
    }
    
    self.selectIndex = index;
    
    UIButton *selectedButton = [self.scrollView viewWithTag:QJButtonTag + index];
    for (UIView *subview in self.scrollView.subviews) {
        if ([subview isKindOfClass:[QJSegmentItem class]]) {
            QJSegmentItem *button = (QJSegmentItem *)subview;
            button.selected = (button.tag == selectedButton.tag ? YES : NO);
            button.titleLab.font = (button.tag == selectedButton.tag ? self.selectedFont : self.font);
            button.titleLab.textColor = (button.tag == selectedButton.tag ? self.selectedTextColor : self.textColor);
        }
    }
    
    CGFloat selectedSegmentOffset = self.width / 2 - selectedButton.width / 2;
    CGRect rectToScrollTo = selectedButton.frame;
    rectToScrollTo.origin.x -= selectedSegmentOffset;
    rectToScrollTo.size.width += selectedSegmentOffset * 2;
    [self.scrollView scrollRectToVisible:rectToScrollTo animated:YES];
}

- (NSInteger)getCurIndex{
    return self.selectIndex;
}

#pragma mark --- 懒加载
- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] initWithFrame:self.bounds];
        _scrollView.backgroundColor = [UIColor clearColor];
        _scrollView.showsVerticalScrollIndicator = NO;
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.directionalLockEnabled = YES;
        _scrollView.bounces = YES;
        _scrollView.tag = 3022;
    }
    return _scrollView;
}

- (UIView *)bottomLine {
    if (!_bottomLine) {
        _bottomLine = [[UIView alloc] initWithFrame:CGRectMake(0, self.height - 1, self.width, 1)];
        _bottomLine.backgroundColor = UIColorFromRGB(0xe5e5e5);
    }
    return _bottomLine;
}

@end

@implementation NSString (LLAdd)

+ (float)getTextWidth:(float)textHeight text:(NSString *)text font:(UIFont *)font {
    if (!text.length) {
        return 0;
    }
    float origin = [text boundingRectWithSize:CGSizeMake(MAXFLOAT, textHeight) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName: font} context:nil].size.width;
    return ceilf(origin);
}

@end
