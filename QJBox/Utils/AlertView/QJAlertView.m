//
//  QJAlertView.m
//  QJBox
//
//  Created by rui on 2022/6/30.
//

#import "QJAlertView.h"

@interface QJAlertView ()

//容器
@property (strong, nonatomic) UIView *container;
//标题
@property (strong, nonatomic) UILabel *titleLabel;
//内容
@property (strong, nonatomic) UILabel *messageLabel;
//左侧按钮
@property (strong, nonatomic) UIButton *leftBtn;
//右侧按钮
@property (strong, nonatomic) UIButton *rightBtn;
//水平线
@property (strong, nonatomic) UIView *horizontalLine;
//垂直线
@property (strong, nonatomic) UIView *verticalLine;
//标题文字
@property (copy, nonatomic) NSString *title;
//内容文字
@property (copy, nonatomic) NSString *message;
//左侧按钮文字
@property (copy, nonatomic) NSString *leftTitle;
// 确认按钮文字
@property (copy, nonatomic) NSString *rightTitle;

@end

@implementation QJAlertView

- (instancetype)initWithTitle:(NSString *)title message:(NSString *)message leftButtonTitle:(NSString *)leftButtonTitle rightButtonTitle:(NSString *)rightButtonTitle buttonClick:(ButtonDirectionBlock)directionBlock{
    if (self = [super init]) {
        _title = title;
        _message = message;
        _leftTitle = leftButtonTitle;
        _rightTitle = rightButtonTitle;
        self.directionBlock = directionBlock;
        [self createUI];
    }
    return self;
}

/**
 * @author: zjr
 * @date: 2022-6-30
 * @desc: 初始化UI
 */
- (void)createUI{
    self.frame = CGRectMake(0, 0, QJScreenWidth, QJScreenHeight);
    self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.0];

    // 容器
    UIView *container = [[UIView alloc] init];
    container.backgroundColor = [UIColor whiteColor];
    [self addSubview:container];
    _container = container;
    
    // 标题
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.font = kboldFont(16);
    titleLabel.text = _title;
    titleLabel.numberOfLines = 0;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [container addSubview:titleLabel];
    _titleLabel = titleLabel;
    
    // 内容
    UILabel *messageLabel = [[UILabel alloc] init];
    messageLabel.text = _message;
    messageLabel.font = kFont(14);
    messageLabel.textAlignment = NSTextAlignmentCenter;
    messageLabel.numberOfLines = 0;
    [container addSubview:messageLabel];
    _messageLabel = messageLabel;
    
    // 左侧按钮
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [leftBtn setTitle:_leftTitle forState:UIControlStateNormal];
    [leftBtn setTitleColor:UIColorFromRGB(0x333333) forState:UIControlStateNormal];
    leftBtn.backgroundColor = [UIColor whiteColor];
//    [leftBtn showBorderWidth:1 borderColor:UIColorFromRGB(0x129686)];
    [leftBtn addTarget:self action:@selector(leftBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [container addSubview:leftBtn];
    _leftBtn = leftBtn;
    
    // 右侧按钮
    UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [rightBtn setTitle:_rightTitle forState:UIControlStateNormal];
    [rightBtn setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];
    rightBtn.backgroundColor = MainColor;
    [rightBtn addTarget:self action:@selector(rightBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [container addSubview:rightBtn];
    _rightBtn = rightBtn;
}

- (void)layoutSubviews{
    [super layoutSubviews];
    
    CGFloat containerW = QJScreenWidth - 32*2;
    CGFloat titleW = containerW - 2 * 24;

    //按钮高度
    CGFloat btnH = 36;
    CGFloat btnW = (containerW - 24*2 - 30) * 0.5;
    
    //标题高度
    CGFloat titleH = 0;
    if (![self isNullOrEmpty:_title]) {
        titleH = [_title getLongStringHeightWithFont:kboldFont(16) withWidth:titleW withLineSpace:5];
    }
    //内容高度
    CGFloat contentH = 0;
    if (![self isNullOrEmpty:_message]) {
        contentH = [_message getLongStringHeightWithFont:kFont(14) withWidth:titleW withLineSpace:5];
    }
    //白色背景高度
    CGFloat containerH = 24 + titleH + 8 + contentH + 12 + btnH + 24;
    // 只有标题 内容nil
    if ([self isNullOrEmpty:_message]) {
        containerH = 24 + titleH + 24 + btnH + 24;
    }
    //只有内容 标题nil
    if ([self isNullOrEmpty:_title]) {
        containerH = 24 + contentH + 12 + btnH + 24;
    }

    _container.bounds = CGRectMake(0, 0, containerW, containerH);
    _container.center = self.center;
    
    /*
     * 三种情况 容器背景高度
     * 标题+内容  24 + 标题 + 8 + 内容 + 12 + 按钮36 + 24
     * 只有标题   24 + 标题 + 24 + 按钮36 + 24
     * 只有内容   12 + 内容 + 12 + 按钮36 + 24
     */
    
    _titleLabel.frame = CGRectMake(24, 24, titleW, titleH);

    _messageLabel.frame = CGRectMake(24, _titleLabel.bottom + 8, titleW, contentH);
    if ([self isNullOrEmpty:_title]) {
        _messageLabel.top = 24;
    }
    
    //特殊情况 只有内容html
    if (![self isNullOrEmpty:_message] && _isHTML) {
        NSAttributedString *attributedString = [[NSAttributedString alloc] initWithData:[_message dataUsingEncoding:NSUnicodeStringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
        _messageLabel.attributedText = attributedString;
        [_messageLabel sizeToFit];
        contentH = _messageLabel.height;
        containerH = 24 + contentH + 24 + btnH + 24;
        _container.bounds = CGRectMake(0, 0, containerW, containerH);
        _container.center = self.center;
    }
    
    _leftBtn.frame = CGRectMake(24, _messageLabel.bottom + 12,btnW, btnH);
    if ([self isNullOrEmpty:_message]) {
        _leftBtn.top = _titleLabel.bottom + 24;
    }
    _rightBtn.frame = CGRectMake( _container.width * 0.5 + 15, _leftBtn.top, btnW, btnH);

    if (_leftTitle == nil && _rightTitle == nil) {
        [_leftBtn removeFromSuperview];
        [_rightBtn removeFromSuperview];
    }else if (_rightTitle == nil) {
        _leftBtn.frame = CGRectMake(containerW/2 - btnW/2, _messageLabel.bottom + 12, btnW, btnH);
        if ([self isNullOrEmpty:_message]) {
            _leftBtn.top = _titleLabel.bottom + 24;
        }
    }else if (_leftTitle == nil) {
        _rightBtn.frame = CGRectMake(containerW/2 - btnW/2, _messageLabel.bottom + 12, (containerW - 24*2 - 30) * 0.5, btnH);
        if ([self isNullOrEmpty:_message]) {
            _rightBtn.top = _messageLabel.bottom + 24;
        }
        [_leftBtn removeFromSuperview];
        [_verticalLine removeFromSuperview];
    }
    _container.layer.cornerRadius = 8;
    _rightBtn.layer.cornerRadius = 8;
}

/**
 * @author: zjr
 * @date: 2022-6-30
 * @desc: Setter方法赋值
 */
- (void)setLeftTextColor:(UIColor *)leftTextColor{
    _leftTextColor = leftTextColor;
    [_leftBtn setTitleColor:leftTextColor forState:UIControlStateNormal];
}

- (void)setRightTextColor:(UIColor *)rightTextColor{
    _rightTextColor = rightTextColor;
    [_rightBtn setTitleColor:rightTextColor forState:UIControlStateNormal];
}

- (void)setContentTextColor:(UIColor *)contentTextColor{
    _contentTextColor = contentTextColor;
    _messageLabel.textColor = _contentTextColor;
}

/**
 * @author: zjr
 * @date: 2022-6-30
 * @desc: button点击事件
 */
- (void)leftBtnClick {
    if (self.directionBlock) {
        self.directionBlock(ButtonDirectionLeft);
    }
    [self dismiss];
}

- (void)rightBtnClick {
    if (self.directionBlock) {
        self.directionBlock(ButtonDirectionRight);
    }
    [self dismiss];
}

/**
 * @author: zjr
 * @date: 2022-6-30
 * @desc: 视图显示和隐藏
 */
- (void)show {
    [[UIApplication sharedApplication].keyWindow addSubview:self];
    [UIView animateWithDuration:0.15 animations:^{
        self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    }];
}

- (void)dismiss{
    [UIView animateWithDuration:0.15 animations:^{
        self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.0];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (BOOL)isNullOrEmpty:(NSString *)string{
    if ([string isKindOfClass:[NSNull class]] || !string || string.length <= 0) {
        return YES;
    }
    return NO;
}

@end
