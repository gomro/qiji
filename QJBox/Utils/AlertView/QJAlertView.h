//
//  QJAlertView.h
//  QJBox
//
//  Created by rui on 2022/6/30.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, ButtonDirection) {
    ButtonDirectionLeft = 0,    // 点击左侧按钮
    ButtonDirectionRight        // 点击右侧按钮
};

typedef void(^ButtonDirectionBlock)(ButtonDirection direction);

@interface QJAlertView : UIView

//按钮点击block, 回调方向
@property (copy, nonatomic) ButtonDirectionBlock directionBlock;
// 左侧按钮颜色 默认 黑色
@property (strong, nonatomic) UIColor *leftTextColor;
// 右侧按钮颜色 默认 黄色
@property (strong, nonatomic) UIColor *rightTextColor;
// 内容颜色 默认 999999
@property (strong, nonatomic) UIColor *contentTextColor;
// message是不是富文本isHTML
@property (nonatomic,assign) BOOL isHTML;

@property (nonatomic,assign) BOOL isShowTextView;

- (instancetype)initWithTitle:(NSString *)title message:(NSString *)message leftButtonTitle:(NSString *)leftButtonTitle rightButtonTitle:(NSString *)rightButtonTitle buttonClick:(ButtonDirectionBlock)directionBlock;

- (void)show;

@end


NS_ASSUME_NONNULL_END
