//
//  QJMoreDataScrollView.h
//  QJBox
//
//  Created by rui on 2022/7/5.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

#define MoreDataMaxDistance 50

@interface QJMoreDataScrollView : UIView

/* 滑动距离显示不同文案 */
@property (nonatomic,assign) CGFloat curveInset;

@end

NS_ASSUME_NONNULL_END
