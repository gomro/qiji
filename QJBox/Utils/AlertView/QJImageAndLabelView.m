//
//  QJImageAndLabelView.m
//  QJBox
//
//  Created by rui on 2022/7/5.
//

#import "QJImageAndLabelView.h"

@interface QJImageAndLabelView ()

@property (nonatomic, strong) UIImageView *img;
@property (nonatomic, strong) UILabel *lab;
@property (nonatomic, assign) CGFloat imageWidth;
@property (nonatomic, assign) CGFloat imageHeight;
@property (nonatomic, assign) CGFloat space;
@property (nonatomic, assign) QJImageDirectly directly;

@end

@implementation QJImageAndLabelView

//图片+文字+间距
-(instancetype)initWithImage:(UIImage *)image withTitle:(NSString *)title withFont:(UIFont *)font withtextColor:(UIColor *)color withSpace:(CGFloat)space withImageDirectly:(QJImageDirectly)directly{
    self = [super init];
    if (self) {
        _space = space;
        _directly = directly;
        _imageWidth = image.size.width;
        _imageHeight = image.size.height;

        UIImageView *img = [[UIImageView alloc] init];
        [self addSubview:img];
        img.image = image;
        _img = img;

        UILabel *lab = [[UILabel alloc] init];
        lab.font = font;
        lab.textColor = color;
        [self addSubview:lab];
        lab.text = title;
        _lab = lab;
        //左右图片
        CGFloat imageW = image.size.width;
        CGFloat titleW = [title getWidthWithFont:font withHeight:font.lineHeight];
        CGFloat totalW = imageW + space + titleW;
        //上下图片
        CGFloat imageH = image.size.height;
        CGFloat titleH = font.lineHeight;
        CGFloat totalH = imageH + space + titleH;
        if (directly == QJImageLeft) {
            [img mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerY.mas_equalTo(self);
                make.left.mas_equalTo(self.mas_centerX).offset(-totalW/2);
            }];
            [lab mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerY.mas_equalTo(self);
                make.right.mas_equalTo(self.mas_centerX).offset(totalW/2);
            }];
        }else if (directly == QJImageUp) {
            [img mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.mas_equalTo(self);
                make.top.mas_equalTo(self.mas_centerY).offset(-totalH/2);
            }];
            [lab mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.mas_equalTo(self);
                make.bottom.mas_equalTo(self.mas_centerY).offset(totalH/2);
            }];
        }else if (directly == QJImageRight) {
            [img mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerY.mas_equalTo(self);
                make.right.mas_equalTo(self.mas_centerX).offset(totalW/2);
            }];
            [lab mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerY.mas_equalTo(self);
                make.left.mas_equalTo(self.mas_centerX).offset(-totalW/2);
            }];
        }else if (directly == QJImageDown) {
            [img mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.mas_equalTo(self);
                make.bottom.mas_equalTo(self.mas_centerY).offset(totalH/2);
            }];
            [lab mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.mas_equalTo(self);
                make.top.mas_equalTo(self.mas_centerY).offset(-totalH/2);
            }];
        }
    }
    return self;
}

- (void)setText:(NSString *)str withIconImage:(UIImage *)image withImageDirectly:(QJImageDirectly)directly{
    _img.image = image;
    _imageWidth = image.size.width;
    _imageHeight = image.size.height;
    [self setText:str withImageDirectly:directly];
}

- (void)setText:(NSString *)str withImageDirectly:(QJImageDirectly)directly{
    if (str.length  == 0) {
        return;
    }
    _lab.text = str;
    
    //左右图片
    CGFloat imageW = _imageWidth;
    CGFloat titleW = [str getWidthWithFont:_lab.font withHeight:_lab.font.lineHeight];
    CGFloat totalW = imageW + _space + titleW;
    //上下图片
    CGFloat imageH = _imageHeight;
    CGFloat titleH = _lab.font.lineHeight;
    CGFloat totalH = imageH + _space + titleH;

    if (_directly == QJImageLeft) {
        [_img mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.mas_centerX).offset(-totalW/2);
        }];
        [_lab mas_updateConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.mas_centerX).offset(totalW/2);
        }];
        
    }else if (directly == QJImageUp) {
        [_img mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.mas_centerY).offset(-totalH/2);
        }];
        [_lab mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(self.mas_centerY).offset(totalH/2);
        }];
    }else if (directly == QJImageRight) {
        [_img mas_updateConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.mas_centerX).offset(totalW/2);
        }];
        [_lab mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.mas_centerX).offset(-totalW/2);
        }];
    }else if (directly == QJImageDown) {
        [_img mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(self.mas_centerY).offset(totalH/2);
        }];
        [_lab mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.mas_centerY).offset(-totalH/2);
        }];
    }
}

@end
