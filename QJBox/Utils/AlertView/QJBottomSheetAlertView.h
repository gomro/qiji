//
//  QJBottomSheetAlertView.h
//  QJBox
//
//  Created by wxy on 2022/7/22.
//

#import "QJBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN



/// 底部弹出的sheetView弹窗
@interface QJBottomSheetAlertView : QJBaseViewController


@property (nonatomic, copy) void(^clickIndex)(NSInteger index);

- (instancetype)initWithTitles:(NSArray *)titles;
@end

NS_ASSUME_NONNULL_END
