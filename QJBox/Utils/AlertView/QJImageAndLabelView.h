//
//  QJImageAndLabelView.h
//  QJBox
//
//  Created by rui on 2022/7/5.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef enum : NSUInteger {
    QJImageLeft = 0,   //图片在左边
    QJImageUp,     //图片在上边边
    QJImageRight,  //图片在右边
    QJImageDown,   //图片在下边
} QJImageDirectly;

@interface QJImageAndLabelView : UIView

//图片+文字+间距
-(instancetype)initWithImage:(UIImage *)image withTitle:(NSString *)title withFont:(UIFont *)font withtextColor:(UIColor *)color withSpace:(CGFloat)space withImageDirectly:(QJImageDirectly)directly;
//重设文字
- (void)setText:(NSString *)str withImageDirectly:(QJImageDirectly)directly;
- (void)setText:(NSString *)str withIconImage:(UIImage *)image withImageDirectly:(QJImageDirectly)directly;

@end

NS_ASSUME_NONNULL_END
