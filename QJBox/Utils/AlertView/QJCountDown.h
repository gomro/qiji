//
//  QJCountDown.h
//  QJBox
//
//  Created by rui on 2022/7/5.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJCountDown : NSObject

/**
 *  用NSDate日期倒计时
 *  startDate开始时间
 *  finishDate结束时间
 *  block回调返回 天，时，分，秒
 */
-(void)countDownWithStratDate:(NSDate *)startDate finishDate:(NSDate *)finishDate completeBlock:(void (^)(NSInteger day,NSInteger hour,NSInteger minute,NSInteger second))completeBlock;

/**
 *  用时间戳倒计时
 *  starTimeStamp开始时间戳
 *  finishTimeStamp结束时间戳
 *  block回调返回 天，时，分，秒
 */
-(void)countDownWithStratTimeStamp:(long long)starTimeStamp finishTimeStamp:(long long)finishTimeStamp completeBlock:(void (^)(NSInteger day,NSInteger hour,NSInteger minute,NSInteger second))completeBlock;

/**
 *  销毁
 */
-(void)destoryTimer;

/**
 *  时间戳转换时间
 */
-(NSDate *)dateWithLongLong:(long long)longlongValue;

/**
 *  结束的回调
 */
@property (nonatomic, copy) void (^TimerStopComplete)(void);

/**
 *  nsstring转换nsdate
 *  format 自定义格式
 */
- (NSDate *)getDateForString:(NSString *)string format:(NSString *)format;

/**
 *  根据自定义时间倒计时
 *  block回调返回 天，时，分，秒
 */
-(void)countDownWithfinishDate:(NSTimeInterval)timerInterval completeBlock:(void (^)(NSInteger day,NSInteger hour,NSInteger minute,NSInteger second))completeBlock;

@end

NS_ASSUME_NONNULL_END
