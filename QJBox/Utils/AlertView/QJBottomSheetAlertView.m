//
//  QJBottomSheetAlertView.m
//  QJBox
//
//  Created by wxy on 2022/7/22.
//

#import "QJBottomSheetAlertView.h"

#define btnTag  1000
@interface QJBottomSheetAlertView ()

/** 暗色背景 */
@property (nonatomic, strong) UIView *bgView;
/** 控件View */
@property (nonatomic, strong) UIView *contentView;

@property (nonatomic, strong) NSArray *titles;//标题

@property (nonatomic, assign) CGFloat totalHeight;
@end

@implementation QJBottomSheetAlertView

- (instancetype)init {
    if (self = [super init]) {
        self.modalPresentationStyle = UIModalPresentationCustom;
    }
    return self;
}

- (instancetype)initWithTitles:(NSArray *)titles {
    if (self = [super init]) {
        self.modalPresentationStyle = UIModalPresentationCustom;
        self.titles = titles;
        self.totalHeight = self.titles.count*52 + 8 + (self.titles.count-1)*1 + Bottom_SN_iPhoneX_OR_LATER_SPACE + 52;
    }
    return self;
}


- (NSArray *)titles {
    if (!_titles) {
        _titles = [NSArray array];
    }
    return _titles;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navBar.hidden = YES;
    [self setupUI];
   
     
}



-(void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    [self.contentView showCorner:16 rectCorner:UIRectCornerTopLeft | UIRectCornerTopRight];
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
   
    
    [UIView animateWithDuration:0.4 delay:0 usingSpringWithDamping:0.95 initialSpringVelocity:0.05 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        
        self.bgView.alpha = 1.0f ;
        
        self.contentView.frame = CGRectMake(0, [UIScreen mainScreen].bounds.size.height - self.totalHeight , kScreenWidth, self.totalHeight);
        
    } completion:^(BOOL finished) {
    }];
}

- (void)setupUI
{
    
    self.view.backgroundColor = [UIColor clearColor];
    
    UIView *bgView = [[UIView alloc] init];
    bgView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6];
    [self.view addSubview:bgView];
    self.bgView = bgView;
 
    self.bgView.alpha = 0.0f;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickBgView)];
    [bgView addGestureRecognizer:tap];
    
    
    UIView *cView = [[UIView alloc] init];
    cView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:cView];
    self.contentView = cView;
   
    self.contentView.layer.masksToBounds = YES;
    
    [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.view);
    }];
     
    self.contentView.frame = CGRectMake(0, kScreenHeight, kScreenWidth, self.totalHeight);
    
    int i = 0;
    for (NSString *text in self.titles) {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.tag = btnTag + i;
        [btn addTarget:self action:@selector(btnAction:) forControlEvents:UIControlEventTouchUpInside];
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        btn.titleLabel.font = FontRegular(14);
        [btn setTitle:text forState:UIControlStateNormal];
        
        [self.contentView addSubview:btn];
        
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.top.equalTo(self.contentView).offset(i*53);
                    make.left.right.equalTo(self.contentView);
                    make.height.equalTo(@52);
        }];
        
        i++;
    }
     
    
    for (int n = 0; n < self.titles.count - 1; n++) {
        UIView *line = [UIView new];
        line.backgroundColor = kColorWithHexString(@"#EBEDF0");
        [self.contentView addSubview:line];
        
        UIButton *btn = [self.contentView viewWithTag:(btnTag + n)];
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.top.equalTo(btn.mas_bottom);
                    make.height.equalTo(@1);
                    make.left.right.equalTo(self.contentView);
        }];
        
        
        
         
    }
    
    UIButton *cancalBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [cancalBtn addTarget:self action:@selector(cancelBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [cancalBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    cancalBtn.titleLabel.font = FontRegular(14);
    [cancalBtn setTitle:@"取消" forState:UIControlStateNormal];
    [self.contentView addSubview:cancalBtn];
    [cancalBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self.view);
            make.height.equalTo(@52);
            make.bottom.equalTo(self.contentView).offset(-Bottom_SN_iPhoneX_OR_LATER_SPACE);
    }];
    
    UIView *lineView = [UIView new];
    lineView.backgroundColor = kColorWithHexString(@"#F9F9F9");
    [self.contentView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(cancalBtn.mas_top);
            make.height.equalTo(@8);
            make.left.right.equalTo(self.contentView);
    }];
    
    
    
}





#pragma mark - 消失
- (void)clickBgView
{
     
    [UIView animateWithDuration:0.4 delay:0 usingSpringWithDamping:0.95 initialSpringVelocity:0.05 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        
        self.bgView.alpha = 0.0f ;
        self.contentView.frame = CGRectMake(0,[UIScreen mainScreen].bounds.size.height, kScreenWidth, self.totalHeight);
        
    } completion:^(BOOL finished) {
        // 动画Animated必须是NO，不然消失之后，会有0.35s时间，再点击无效
        [self dismissViewControllerAnimated:NO completion:^{
             
        }];
    }];
}

#pragma mark --  action


- (void)btnAction:(UIButton *)sender {
    [UIView animateWithDuration:0.4 delay:0 usingSpringWithDamping:0.95 initialSpringVelocity:0.05 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        
        self.bgView.alpha = 0.0f ;
        self.contentView.frame = CGRectMake(0,[UIScreen mainScreen].bounds.size.height, kScreenWidth, self.totalHeight);
        
    } completion:^(BOOL finished) {
        // 动画Animated必须是NO，不然消失之后，会有0.35s时间，再点击无效
        [self dismissViewControllerAnimated:NO completion:^{
            if (self.clickIndex) {
                self.clickIndex(sender.tag - btnTag);
            }
        }];
    }];
    
}

- (void)cancelBtnAction {
    [self clickBgView];
}




// 这里主动释放一些空间，加速内存的释放，防止有时候消失之后，再点不出来。
- (void)dealloc
{
    NSLog(@"%@ --> dealloc",[self class]);
    [self.bgView removeFromSuperview];
    self.bgView = nil;
    [self.contentView removeFromSuperview];
    self.contentView = nil;
}

@end
