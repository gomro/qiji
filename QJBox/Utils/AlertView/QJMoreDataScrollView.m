//
//  QJMoreDataScrollView.m
//  QJBox
//
//  Created by rui on 2022/7/5.
//

#import "QJMoreDataScrollView.h"

@interface QJMoreDataScrollView ()

/* 显示文案 */
@property (nonatomic,strong) UILabel *tipLab;

@end

@implementation QJMoreDataScrollView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self initChildView];
    }
    return self;
}

- (void)initChildView {
    
    self.tipLab = [[UILabel alloc] initWithFrame:CGRectMake(self.width , 0, 15, self.height)];
    [self addSubview:self.tipLab];
    _tipLab.font = kFont(11);
    _tipLab.textColor = [UIColor blackColor];
    _tipLab.text = @"更多";
    _tipLab.numberOfLines = 0;
    _tipLab.textAlignment = NSTextAlignmentCenter;
}


- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    //重绘贝塞尔曲线
    CGContextRef context = UIGraphicsGetCurrentContext();
    [[UIColor clearColor] setFill];
    CGContextFillRect(context, rect);
    
    CGFloat width = rect.size.width;
    CGFloat height = rect.size.height;
    CGContextMoveToPoint(context, width, 0);
    CGContextAddCurveToPoint(context, width, 0, width - _curveInset * 2, height/2, width, height);
//    [UIColorFromRGB(0xff486d) setFill];
    [UIColorFromRGB(0xFF9500) setFill];
    CGContextDrawPath(context, kCGPathFill);
    
    if (_curveInset < 0) {
        _tipLab.text = @"";
    }else{
        if (_curveInset > 30 ) {
            _tipLab.text = @"释放查看";
        }else{
            _tipLab.text = @"更多";
        }
        if (_curveInset < 15 ) {
            _tipLab.frame = CGRectMake(width - _curveInset, 0, 15, height);
        }else{
            _tipLab.frame = CGRectMake(width - 15, 0, 15, height);
        }
    }
}

- (void)setCurveInset:(CGFloat)curveInset {
    _curveInset = curveInset;
    [self setNeedsDisplay];
}


@end
