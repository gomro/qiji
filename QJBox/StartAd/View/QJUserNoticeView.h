//
//  QJUserNoticeView.h
//  QJBox
//
//  Created by Sun on 2022/9/9.
//

#import <UIKit/UIKit.h>


typedef NS_ENUM(NSInteger, QJUserNoticeViewStyle){
    QJUserNoticeViewStyleDefault,    //初始协议
    QJUserNoticeViewStyleUpdate,     //协议更新  
};

/**同意回调*/
typedef void(^UserNoticeAgreenBlock)(void);
/**拒绝回调*/
typedef void(^UserNoticeNoAgreenBlock)(void);
/**点击隐私  1隐私协议 2用户协议 3三方运营商政策*/
typedef void(^UserNoticeTurnUrlBlock)(NSInteger index);

@interface QJUserNoticeView : UIView

@property (nonatomic, copy) UserNoticeAgreenBlock agreenBlock;
@property (nonatomic, copy) UserNoticeNoAgreenBlock noAgreenBlock;
@property (nonatomic, copy) UserNoticeTurnUrlBlock turnUrlBlock;

- (instancetype)initWithFrame:(CGRect)frame style:(QJUserNoticeViewStyle)style;
- (void)showAnimation:(UIViewController *)VC;
@end

