//
//  QJUserNoticeView.m
//  QJBox
//
//  Created by Sun on 2022/9/9.
//

#import "QJUserNoticeView.h"
#import "TYAlertController.h"

@interface QJUserNoticeView ()
/**背景*/
@property (nonatomic, strong) UIView *bgV;
/**标题*/
@property (nonatomic, strong) UILabel *titleLabel;
/**分割线1*/
@property (nonatomic, strong) UIView *lineW;
/**分割线2*/
@property (nonatomic, strong) UIView *lineH;
/**内容*/
@property (nonatomic, strong) YYLabel *messageL;
/**不同意按钮*/
@property (nonatomic, strong) UIButton *noAgreenBtn;
/**同意按钮*/
@property (nonatomic, strong) UIButton *agreenBtn;

/**状态**/
@property (nonatomic, assign) QJUserNoticeViewStyle noticeStyle;
/**视图**/
@property (nonatomic, strong) TYAlertController *alertController;

@end

@implementation QJUserNoticeView
- (instancetype)initWithFrame:(CGRect)frame style:(QJUserNoticeViewStyle)style {
    self = [super initWithFrame:frame];
    if(self) {
        self.noticeStyle = style;
        if(style == QJUserNoticeViewStyleDefault) {
            self.frame = CGRectMake(0, 0, 270*kWScale, 370*kWScale);
        } else {
            self.frame = CGRectMake(0, 0, 270*kWScale, 154*kWScale);
        }
        [self loadUI];
    }
    return self;
}

-(void)loadUI {
    self.backgroundColor = [UIColor clearColor];
    [self addSubview:self.bgV];
    [self.bgV addSubview:self.titleLabel];
    [self.bgV addSubview:self.lineW];
    [self.bgV addSubview:self.lineH];
    [self.bgV addSubview:self.messageL];
    [self.bgV addSubview:self.noAgreenBtn];
    [self.bgV addSubview:self.agreenBtn];
    [self makeConstraints];
    [self.noAgreenBtn addTarget:self action:@selector(onNoAgreenBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.agreenBtn addTarget:self action:@selector(onAgreenBtn) forControlEvents:UIControlEventTouchUpInside];
    [self setValues];
}

#pragma mark - 控件约束布局
-(void)makeConstraints {
    //背景
    [self.bgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.left.right.equalTo(self);
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(16);
        make.centerX.equalTo(self.bgV);
        make.height.mas_equalTo(22*kWScale);
    }];
    //内容
    [self.messageL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).offset(2);
        make.left.mas_equalTo(16);
        make.right.mas_equalTo(-16);
    }];
    [self.lineW mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.bgV.mas_bottom).offset(-44);
        make.left.right.equalTo(self.bgV);
        make.height.mas_equalTo(1);
    }];
    
    [self.lineH mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.lineW.mas_bottom);
        make.bottom.equalTo(self.bgV);
        make.centerX.equalTo(self.bgV);
        make.width.mas_equalTo(1);
    }];
    
    //不同意按钮
    [self.noAgreenBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.lineW.mas_bottom);
        make.left.bottom.equalTo(self.bgV);
        make.right.equalTo(self.lineH.mas_left);
    }];
    //同意按钮
    [self.agreenBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.lineW.mas_bottom);
        make.right.bottom.equalTo(self.bgV);
        make.left.equalTo(self.lineH.mas_right);
    }];
}

#pragma mark -  赋值
-(void)setValues { 
    NSString *string = @"感谢您选择奇迹盒子APP！我们非常重视您的个人信息和隐私保护。为了更好的保障您的个人权益，在您使用我们的产品前，请无比审慎阅读《隐私政策》和《用户协议》内的所有条款，尤其是：1.我们对您的个人信息的收集/使用/对外提供/保护等规则条款，以及您的用户权力等条款：2.约定我们的限制责任，免责条款。3.其他以颜色或加粗进行标识的重要条款。如您对以上协议有任何疑问，可通过在线客服或发送邮件至xxxx与我们联系。您点击“同意并继续”的行为即表示您已阅读完毕并同意以上协议的全部内容。如您同意以上协议内容，请点击“同意并继续”开始使用我们的产品和服务。";
    if(self.noticeStyle == QJUserNoticeViewStyleUpdate) {
        string = @"为了更好的保障您的合法权益 请您阅读并同意以下协议《用户协议》与《隐私政策》与《第三方运营商政策》。";
    }
    NSMutableAttributedString *mutableAttributedString = [[NSMutableAttributedString alloc] initWithString:string];
    [mutableAttributedString setColor:UIColorHex(000000) range:string.rangeOfAll];
    [mutableAttributedString setFont:MYFONTALL(FONT_REGULAR, 13) range:string.rangeOfAll];

    NSRange privacyRange = [string rangeOfString:@"《隐私政策》"];
    [mutableAttributedString setColor:UIColorHex(007AFF) range:privacyRange];
    YYTextHighlight *privacyHighlight = [YYTextHighlight new];
    [mutableAttributedString setTextHighlight:privacyHighlight range:privacyRange];
    MJWeakSelf
    privacyHighlight.tapAction = ^(UIView * _Nonnull containerView, NSAttributedString * _Nonnull text, NSRange range, CGRect rect) {
        QJWKWebViewController *vc = [QJWKWebViewController new];
        vc.url = [QJUserManager shareManager].userPolicy;
        vc.navTitle = @"隐私政策";
        vc.modalPresentationStyle = UIModalPresentationOverFullScreen;
        [weakSelf.alertController presentViewController:vc animated:YES completion:nil];
    };
    
    NSRange userRange = [string rangeOfString:@"《用户协议》"];
    [mutableAttributedString setColor:UIColorHex(007AFF) range:userRange];
    YYTextHighlight *userHighlight = [YYTextHighlight new];
    [mutableAttributedString setTextHighlight:userHighlight range:userRange];
    userHighlight.tapAction = ^(UIView * _Nonnull containerView, NSAttributedString * _Nonnull text, NSRange range, CGRect rect) {
        QJWKWebViewController *vc = [QJWKWebViewController new];
        vc.url = [QJUserManager shareManager].userProtocol;
        vc.navTitle = @"用户协议";
        vc.modalPresentationStyle = UIModalPresentationOverFullScreen;
        [weakSelf.alertController presentViewController:vc animated:YES completion:nil];
    };
    
    NSRange otherRange = [string rangeOfString:@"《第三方运营商政策》"];
    [mutableAttributedString setColor:UIColorHex(007AFF) range:otherRange];
    YYTextHighlight *otherHighlight = [YYTextHighlight new];
    [mutableAttributedString setTextHighlight:otherHighlight range:otherRange];
    otherHighlight.tapAction = ^(UIView * _Nonnull containerView, NSAttributedString * _Nonnull text, NSRange range, CGRect rect) {
        [self hideAnimation];
        if (self.turnUrlBlock) {
            self.turnUrlBlock(3);
        }
    };
    self.messageL.attributedText = mutableAttributedString;
}

#pragma mark -  懒加载
- (UIView *)bgV {
    if (!_bgV) {
        _bgV = [[UIView alloc] initWithFrame:CGRectZero];
        _bgV.backgroundColor = UIColorHex(F2F2F2CC);
        _bgV.clipsToBounds = YES;
        _bgV.layer.cornerRadius = 14;
    }
    return _bgV;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.font = MYFONTALL(FONT_BOLD, 17);
        _titleLabel.textColor = [UIColor blackColor];
        _titleLabel.text = @"用户协议与隐私政策";
    }
    return _titleLabel;;
}

- (UIView *)lineW {
    if (!_lineW) {
        _lineW = [UIView new];
        _lineW.backgroundColor = UIColorHex(3C3C435C);
    }
    return _lineW;
}

- (UIView *)lineH {
    if (!_lineH) {
        _lineH = [UIView new];
        _lineH.backgroundColor = UIColorHex(3C3C435C);
    }
    return _lineH;
}

- (YYLabel *)messageL {
    if (!_messageL) {
        _messageL = [[YYLabel alloc] initWithFrame:CGRectZero];
        _messageL.numberOfLines = 0;
        _messageL.preferredMaxLayoutWidth = 238*kWScale;
    }
    return _messageL;
}

- (UIButton *)noAgreenBtn {
    if (!_noAgreenBtn) {
        _noAgreenBtn = [UIButton new];
        [_noAgreenBtn setTitle:@"不同意" forState:UIControlStateNormal];
        [_noAgreenBtn setTitleColor:UIColorHex(007AFF) forState:UIControlStateNormal];
        _noAgreenBtn.titleLabel.font = MYFONTALL(FONT_REGULAR, 17);
    }
    return _noAgreenBtn;
}

- (UIButton *)agreenBtn {
    if (!_agreenBtn) {
        _agreenBtn = [UIButton new];
        [_agreenBtn setTitle:@"同意" forState:UIControlStateNormal];
        [_agreenBtn setTitleColor:UIColorHex(007AFF) forState:UIControlStateNormal];
        _agreenBtn.titleLabel.font = MYFONTALL(FONT_BOLD, 17);
    }
    return _agreenBtn;
}

- (void)showAnimation:(UIViewController *)VC {
    TYAlertController *alertController = [TYAlertController alertControllerWithAlertView:self preferredStyle:TYAlertControllerStyleAlert];
    self.alertController = alertController;
    alertController.backgroundColor = UIColorHex(00000099);
    [VC presentViewController:alertController animated:YES completion:nil];
}

- (void)hideAnimation {
    [self.alertController dismissViewControllerAnimated:YES];
}


#pragma mark -  不同意按钮
-(void)onNoAgreenBtn {
    [self exitApplication];
    if (self.noAgreenBlock) {
        self.noAgreenBlock();
    }
}

- (void)exitApplication {
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    [UIView animateWithDuration:0.2f animations:^{
        window.alpha = 0;
        window.frame = CGRectMake(0, window.bounds.size.width, 0, 0);
    } completion:^(BOOL finished) {
        exit(0);
    }];
}

#pragma mark -  同意按钮
-(void)onAgreenBtn {
    [self hideAnimation];
    LSUserDefaultsSET(@"1", kQJUserNoticeView);
    if (self.agreenBlock) {
        self.agreenBlock();
    }
}
@end

