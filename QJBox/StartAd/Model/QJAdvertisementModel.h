//
//  QJAdvertisementModel.h
//  QJBox
//
//  Created by Sun on 2022/6/24.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJAdvertisementModel : NSObject
/**图片名称*/
@property (nonatomic, copy) NSString *name;
/**图片地址*/
@property (nonatomic, copy) NSString *imgUrl;
/**展示时间*/
@property (nonatomic, assign) NSInteger waitTime;
/**h5链接*/

@property (nonatomic, assign) NSString *h5Url;

@property (nonatomic, assign) BOOL isFinish;

@end

NS_ASSUME_NONNULL_END
