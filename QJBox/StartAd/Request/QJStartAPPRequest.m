//
//  QJStartAPPRequest.m
//  QJBox
//
//  Created by Sun on 2022/6/22.
//

#import "QJStartAPPRequest.h"


@implementation QJStartAPPModel

 

@end


@implementation QJStartAPPConfigModel

 

@end

@implementation QJStartAPPCnareaConfigModel

 

@end

@implementation QJStartAPPPrivacyModel

 

@end


@implementation QJStartAPPImageInfoModel

 

@end




@implementation QJStartAPPRequest

- (void)netWorkGetImageAddress {
    self.dic = @{@"version":[QJEnvironmentConfigure shareInstance].serviceVersion};
    self.urlStr = @"/config";
    [self start];    
}

- (void)netWorkGetLaunchImage {
    self.dic = @{};
    self.urlStr = @"/app/bootstrapPic";
    [self start];
}

- (void)netWorkGetAdvertisement {
    self.dic = @{};
    self.urlStr = @"/bootstrap/bootstrapBanner";
    [self start];
}

/**获取中国区域全部地址信息*/
- (void)netWorkGetRecAddressTreeCnarea {
    self.dic = @{};
    self.urlStr = @"/recAddress/treeCnarea";
    [self start];
}

@end
