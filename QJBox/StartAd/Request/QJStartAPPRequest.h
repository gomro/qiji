//
//  QJStartAPPRequest.h
//  QJBox
//
//  Created by Sun on 2022/6/22.
//

#import "QJBaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

@class QJStartAPPConfigModel,QJStartAPPPrivacyModel,QJStartAPPCnareaConfigModel;

@interface QJStartAPPModel : NSObject

@property (nonatomic, strong) QJStartAPPConfigModel *config;
@property (nonatomic, strong) QJStartAPPCnareaConfigModel *cnareaConfig;

@property (nonatomic, strong) QJStartAPPPrivacyModel *privacy;


@end

@interface QJStartAPPImageInfoModel : NSObject

@property (nonatomic, copy) NSString *base_url;

@property (nonatomic, copy) NSString *preview_url;

@end

@interface QJStartAPPConfigModel : NSObject

@property (nonatomic, copy) NSString *yourVersion;

@property (nonatomic, copy) NSString *lastVersion;

@property (nonatomic, strong) QJStartAPPImageInfoModel *img_info;


@end

@interface QJStartAPPCnareaConfigModel : NSObject

@property (nonatomic, assign) NSInteger lastVersion;

@end

@interface QJStartAPPPrivacyModel : NSObject

@property (nonatomic, copy) NSString *policy;

@property (nonatomic, copy) NSString *protocol;


@end


@interface QJStartAPPRequest : QJBaseRequest

/**图片地址获取*/
- (void)netWorkGetImageAddress;

/**启动页获取*/
- (void)netWorkGetLaunchImage;

/**广告页获取*/
- (void)netWorkGetAdvertisement;

/**获取中国区域全部地址信息*/
- (void)netWorkGetRecAddressTreeCnarea;

@end

NS_ASSUME_NONNULL_END
