//
//  DefineConstant.h
//
//
//
//

#import <Foundation/Foundation.h>
#import "QJDeviceConstant.h"

#ifndef DEFINE_CONSTANT_H
#define DEFINE_CONSTANT_H  1

#define IOS13_OR_LATER    ( [QJDeviceConstant iOS13_OR_LATER] )
#define IOS12_OR_LATER    ( [QJDeviceConstant iOS12_OR_LATER] )
#define IOS11_OR_LATER    ( [QJDeviceConstant iOS11_OR_LATER] )
#define IOS10_OR_LATER    ( [QJDeviceConstant iOS10_OR_LATER] )


#define IS_IPAD         ( [QJDeviceConstant isIpad] )

#define DEGREES_TO_RADIANS(d)  ( [QJDeviceConstant degrees_to_radians:d] )

#define kColorWithHexString(hex)  [UIColor colorWithHexString:hex]
//项目主颜色
#define MainColor [UIColor colorWithHexString:@"0xFCBC00"]
//项目按钮背景颜色
#define kColorBtnBg [UIColor colorWithHexString:@"0x1F2A4D"]
//项目灰色颜色
#define kColorGray [UIColor colorWithHexString:@"0x919599"]
//项目橙色颜色
#define kColorOrange [UIColor colorWithHexString:@"0xff9500"]

//字体设置宏
#define kWScale (QJScreenWidth / 375.0)

#define kFont(fontSize) [UIFont systemFontOfSize:((float)fontSize*kWScale)]
#define kboldFont(fontSize) [UIFont boldSystemFontOfSize:((float)fontSize*kWScale)]

#define FontRegular(x)              [UIFont fontWithName:@"PingFangSC-Regular" size:x*kWScale]
#define FontMedium(x)               [UIFont fontWithName:@"PingFangSC-Medium" size:x*kWScale]
#define FontSemibold(x)             [UIFont fontWithName:@"PingFangSC-Semibold" size:x*kWScale]

#define QJScreenHeight ([[QJDeviceConstant sharedInstance] screenHeight])
#define QJScreenWidth  ([[QJDeviceConstant sharedInstance] screenWidth])
#define QJKeyWindow [[[UIApplication sharedApplication] windows] objectAtIndex:0]
 
#define Get375Height(h)  ([[QJDeviceConstant sharedInstance] get375Height:(h)])
#define Get375Width(w)   ([[QJDeviceConstant sharedInstance] get375Width:(w)])

#define Get375HorizontalScreen(w) w*(QJScreenHeight/375.0)

#define kDeviceWidth    (kScreenWidth<kScreenHeight?kScreenWidth:kScreenHeight)
#define kDeviceHeight   (kScreenWidth<kScreenHeight?kScreenHeight:kScreenWidth)

//状态栏高度
#define QJStatusBarHeight  ([QJDeviceConstant sysStatusBarHeight])

 
 
//判断是否为iPad全面屏机型 根据Internal Name（例如iPad8,1）判断特定机型，模拟器默认是返回i386和x86_64 需要真机验证
#define ISFullScreenIPad  ([[QJDeviceConstant sharedInstance] isFullScreenPad])

//iPhoneX顶部部偏移量
#define Top_iPhoneX_SPACE       ( [QJDeviceConstant top_iPhoneX_SPACE] )

//iPhoneX底部偏移量
#define Bottom_iPhoneX_SPACE         ([[QJDeviceConstant sharedInstance] iPhoneXBottomSpace])

//iPhoneX navigationview底部Y坐标
#define NavigationBar_Bottom_Y    ( [QJDeviceConstant navigationBar_Bottom_Y] )

#define HSVCOLOR(h,s,v) [UIColor colorWithHue:h saturation:s value:v alpha:1]
#define HSVACOLOR(h,s,v,a) [UIColor colorWithHue:h saturation:s value:v alpha:a]
#define RGB(r, g, b)             [UIColor colorWithRed:((r) / 255.0) green:((g) / 255.0) blue:((b) / 255.0) alpha:1.0]
#define RGBAlpha(r, g, b, a)     [UIColor colorWithRed:((r) / 255.0) green:((g) / 255.0) blue:((b) / 255.0) alpha:(a)]

#define iPhone5OrLater  ( [QJDeviceConstant isIPhone5OrLater] )

#define ScreenWidthMoreThan320   ( [QJDeviceConstant isScreenWidthMoreThan320] )

#define iPhone5 ( [QJDeviceConstant isIphone5] )
#define iPhone6 ( [QJDeviceConstant isIphone6] )
#define iPhone6Plus ( [QJDeviceConstant isIphone6Plus] )
#define iPhoneX ( [QJDeviceConstant isIphoneX] )
#define iOS7 ( [QJDeviceConstant isIOS7] )
#define ApplicationScreenHeight ( [QJDeviceConstant applicationScreenHeight] )
#define ApplicationScreenWidth ( [QJDeviceConstant applicationScreenWidth] )

#define ResponseSuccess ([EncodeStringFromDic(request.responseObject, @"code") isEqualToString:@"0"])
#define ResponseMsg (EncodeStringFromDic(request.responseObject, @"message"))
#define ResponseFailToastMsg @"数据异常 请重新再试"

//==================================================
// 判断是否为iPhone X 系列(XMax XR XS)
#define IPHONE_X_OR_LATER ( [QJDeviceConstant iPHONE_X_OR_LATER] )

//iPhoneX顶部部偏移量
#define Top_SN_iPhoneX_OR_LATER_SPACE  ( [QJDeviceConstant top_SN_iPhoneX_OR_LATER_SPACE] )
//iPhoneX底部偏移量
#define Bottom_SN_iPhoneX_OR_LATER_SPACE         ([[QJDeviceConstant sharedInstance] iPhoneXOrLaterBottomSpace])

//==================================================

////打印
#ifdef DEBUG
#       define DLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
#       define DLog(...)
#endif





//是否为空或是[NSNull null]
#define NotNilAndNull(_ref)  ( [QJDeviceConstant notNilAndNull:_ref] )
#define IsNilOrNull(_ref)    ( [QJDeviceConstant isNilOrNull:_ref] )

//字符串是否为空
#define IsStrEmpty(_ref)  ( [QJDeviceConstant isStrEmpty:_ref] )
//数组是否为空
#define IsArrEmpty(_ref)    ( [QJDeviceConstant isArrEmpty:_ref] )

//如果为空对象，返回@""
#define kCheckNil(value)\
({id tmp;\
if ([(value) isKindOfClass:[NSNull class]] || \
[[NSString stringWithFormat:@"%@",value] isEqualToString:@"(null)"] || \
[[NSString stringWithFormat:@"%@",value] isEqualToString:@"(NULL)"] || \
[[NSString stringWithFormat:@"%@",value] isEqualToString:@"null"] || \
[[NSString stringWithFormat:@"%@",value] isEqualToString:@"<null>"] || \
[[NSString stringWithFormat:@"%@",value] isEqualToString:@"NULL"]) \
{tmp = @"";}\
else\
{tmp = (value?value:@"");}\
tmp;\
})\
//如果为空对象，返回字符串
#define kCheckStringNil(value) [NSString stringWithFormat:@"%@",kCheckNil(value)]

#define OC(str) [NSString stringWithCString:(str) encoding:NSUTF8StringEncoding]

//16进制色值参数转换
#define UIColorFromRGB(rgbValue) ( [QJDeviceConstant colorFromRGB:rgbValue] )

#define kTableViewSectionHeaderHeightKey    @"kTableViewSectionHeaderHeightKey"

#define kTableViewSectionFootererHeightKey    @"kTableViewSectionFootererHeightKey"

#define kTableViewNumberOfRowsKey           @"kTableViewNumberOfRowsKey"

#define kTableViewCellListKey               @"kTableViewCellListKey"

#define kQJToken        @"QJ_token"
#define kQJUserId        @"QJ_userId"
#define kQJIsRealName     @"QJ_ISRealName"
#define kQJUserHeadImage   @"QJ_userHeadImage"
#define kQJRegisterTime     @"QJ_RegisterTime"
#define kQJUserNoticeView     @"QJ_UserNoticeView"



//便捷方式创建NSNumber类型
#undef    __INT
#define __INT( __x )            [NSNumber numberWithInt:(int)__x]

#undef    __UINT
#define __UINT( __x )            [NSNumber numberWithUnsignedInt:(NSUInteger)__x]

#undef    __FLOAT
#define    __FLOAT( __x )            [NSNumber numberWithFloat:(float)__x]

#undef    __DOUBLE
#define    __DOUBLE( __x )            [NSNumber numberWithDouble:(double)__x]

//便捷创建NSString
#undef  STR_FROM_INT
#define STR_FROM_INT( __x )     [NSString stringWithFormat:@"%d", (__x)]

#undef  STR_FROM_NSINT
#define STR_FROM_NSINT( __x )     [NSString stringWithFormat:@"%ld", (__x)]

//线程执行方法
#define Foreground_Begin  dispatch_async(dispatch_get_main_queue(), ^{
#define Foreground_End    });

#define Background_Begin  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{\
@autoreleasepool {
#define Background_End          }\
});


//全局定义NSUserDefaults的宏定义
#define LSUserDefaultsGET(key) [[NSUserDefaults standardUserDefaults] objectForKey:key] // 取
#define LSUserDefaultsGETSynchronize(key)  [[[NSUserDefaults standardUserDefaults] objectForKey:key] description] // 强取
#define LSUserDefaultsSET(object, key) [[NSUserDefaults standardUserDefaults] setObject:object forKey:key]  // 写
#define LSUserDefaultsSynchronize [[NSUserDefaults standardUserDefaults] synchronize] // 存
#define LSUserDefaultsRemove(key) [[NSUserDefaults standardUserDefaults] removeObjectForKey:key]  // 删

//单例创建
#undef    AS_SINGLETON
#define AS_SINGLETON( __class ) \
+ (__class *)sharedInstance;

#undef    DEF_SINGLETON
#define DEF_SINGLETON( __class ) \
+ (__class *)sharedInstance \
{ \
static dispatch_once_t once; \
static __class * __singleton__; \
dispatch_once( &once, ^{ __singleton__ = [[__class alloc] init]; } ); \
return __singleton__; \
}


//arc custom retain and release
#define ARCRetain(...) void *retainedThing = (__bridge_retained void *)__VA_ARGS__; retainedThing = retainedThing
#define ARCRelease(...) void *retainedThing = (__bridge void *) __VA_ARGS__; id unretainedThing = (__bridge_transfer id)retainedThing; unretainedThing = nil


extern NSString* EncodeStringFromDic(NSDictionary * _Nullable dic, NSString *key);
/**
 * 如果取不到值，返回自定义的默认值
 @brief defValue 自定义默认值
 **/
extern NSString* _Nonnull  EncodeStringFromDicWithDefValue(NSDictionary * _Nullable dic, NSString *key, NSString * _Nonnull defValue);
/**
 * 如果取不到值，返回@""
 **/
extern NSString* _Nonnull  EncodeStringFromDicDefEmtryValue(NSDictionary * _Nullable dic, NSString *key);

extern NSNumber* EncodeNumberFromDic(NSDictionary *dic, NSString *key);
extern NSDictionary *EncodeDicFromDic(NSDictionary *dic, NSString *key);
extern NSArray      *EncodeArrayFromDic(NSDictionary *dic, NSString *key);
extern NSArray      *EncodeArrayFromDicUsingParseBlock(NSDictionary *dic, NSString *key, id(^parseBlock)(NSDictionary *innerDic));


#define EncodeStringFromDic QJEncodeStringFromDic
#define EncodeStringFromDicWithDefValue QJEncodeStringFromDicWithDefValue
#define EncodeStringFromDicDefEmtryValue QJEncodeStringFromDicDefEmtryValue
#define EncodeNumberFromDic QJEncodeNumberFromDic
#define EncodeDicFromDic QJEncodeDicFromDic
#define EncodeArrayFromDic QJEncodeArrayFromDic
#define EncodeArrayFromDicUsingParseBlock QJEncodeArrayFromDicUsingParseBlock

extern NSString* QJEncodeStringFromDic(NSDictionary *dic, NSString *key);
extern NSString* _Nonnull  QJEncodeStringFromDicWithDefValue(NSDictionary *dic, NSString *key, NSString * _Nonnull defValue);
extern NSString* _Nonnull  QJEncodeStringFromDicDefEmtryValue(NSDictionary *dic, NSString *key);
extern NSNumber* QJEncodeNumberFromDic(NSDictionary *dic, NSString *key);
extern NSDictionary *QJEncodeDicFromDic(NSDictionary *dic, NSString *key);
extern NSArray      *QJEncodeArrayFromDic(NSDictionary *dic, NSString *key);
extern NSArray      *QJEncodeArrayFromDicUsingParseBlock(NSDictionary *dic, NSString *key, id(^parseBlock)(NSDictionary *innerDic));

//Font
#define MYFONTALL(name, a)   ([[[UIDevice currentDevice] systemVersion] floatValue] >= 9.0)?[UIFont fontWithName:[NSString stringWithFormat:@"%@",name] size:(a*kWScale)]:[UIFont systemFontOfSize:(a*kWScale)]
#define FONT_ULTRALIGHT    (([[[UIDevice currentDevice] systemVersion] floatValue] >= 9.0)? @"PingFang-SC-UltraLight":@"STHeitiSC-Light")
#define FONT_LIGHT         (([[[UIDevice currentDevice] systemVersion] floatValue] >= 9.0)? @"PingFang-SC-Light":@"STHeitiSC-Light")
#define FONT_MEDIUM        (([[[UIDevice currentDevice] systemVersion] floatValue] >= 9.0)? @"PingFang-SC-Medium":@"STHeitiSC-Light")
#define FONT_REGULAR       (([[[UIDevice currentDevice] systemVersion] floatValue] >= 9.0)? @"PingFang-SC-Regular":@"STHeitiSC-Light")
#define FONT_THIN          (([[[UIDevice currentDevice] systemVersion] floatValue] >= 9.0)? @"PingFang-SC-Thin":@"STHeitiSC-Light")
#define FONT_BOLD          (([[[UIDevice currentDevice] systemVersion] floatValue] >= 9.0)? @"PingFangSC-Semibold":@"STHeitiSC-Light")





//国际化
#undef L
#define L(key) \
[[NSBundle mainBundle] localizedStringForKey:(key) value:@"" table:nil]

//arc 支持performSelector:
#define SuppressPerformSelectorLeakWarning(Stuff) \
do { \
_Pragma("clang diagnostic push") \
_Pragma("clang diagnostic ignored \"-Warc-performSelector-leaks\"") \
Stuff; \
_Pragma("clang diagnostic pop") \
} while (0)

#define HTTP_TIMEOUT 20


#endif      //--------------------------------endLine
