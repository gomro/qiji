//
//  QJDeviceConstant.h
//
//

//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#define WS(weakSelf)  __weak __typeof(&*self)weakSelf = self;
NS_ASSUME_NONNULL_BEGIN

@interface QJDeviceConstant : NSObject

+(QJDeviceConstant*)sharedInstance;

/**
 是否是全面屏iPad ipad pro 11 ipad pro 12.9
 由于模拟器返回i386和x86_64，需要真机验证 模拟器返回NO
 @return YES or NO
 */
- (BOOL)isFullScreenPad;

- (CGFloat)screenWidth;

- (CGFloat)screenHeight;

 

- (float)get375Width:(float)aValue;

- (float)get375Height:(float)aValue;

- (float)iPhoneXBottomSpace;

- (float)iPhoneXOrLaterBottomSpace;

+ (BOOL)iOS13_OR_LATER;
+ (BOOL)iOS12_OR_LATER;
+ (BOOL)iOS11_OR_LATER;
+ (BOOL)iOS10_OR_LATER;
+ (BOOL)isIpad;

+ (CGFloat)degrees_to_radians:(CGFloat)d;

+ (CGFloat)top_iPhoneX_SPACE;

+ (CGFloat)navigationBar_Bottom_Y;


+ (BOOL)isScreenWidthMoreThan320;

+ (BOOL)isIphone5;
+ (BOOL)isIphone6;
+ (BOOL)isIphone6Plus;
+ (BOOL)isIphoneX;
+ (BOOL) isIOS7;

+ (CGFloat)sysStatusBarHeight;

+ (CGFloat)applicationScreenHeight;

+ (CGFloat)applicationScreenWidth;

+ (BOOL)iPHONE_X_OR_LATER;

+ (CGFloat)top_SN_iPhoneX_OR_LATER_SPACE;

+ (BOOL)notNilAndNull:(id)_ref;
+ (BOOL)isNilOrNull:(id)_ref;
+ (BOOL)isStrEmpty:(id)_ref;
+ (BOOL)isArrEmpty:(id)_ref;

+ (UIColor *)colorFromRGB:(unsigned long)rgbValue;

/// 获取手机型号
+ (NSString *)getCurrentDeviceModel;

/// APP版本号
+ (NSString *)appVersion;

/// APP名字
+ (NSString *)appName;

/// bundle id
+ (NSString *)appPackageName;

/// build
+ (NSString *)appBuildID;

/// 获取到UUID后存入系统中的keychain中
+ (NSString *)getUUIDInKeychain;


/// 普通的获取UUID的方法 每次在变化
+ (NSString *)getUUID;

+(BOOL)hasLocationAuth;

@end

NS_ASSUME_NONNULL_END

