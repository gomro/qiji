//
//  QJMediator.m
//  QJBox
//
//  Created by macm on 2022/9/8.
//

#import "QJMediator.h"
#import <objc/runtime.h>
#import "QJMediator+HandyTools.h"

@interface QJMediator ()


@end

@implementation QJMediator

#pragma mark - public methods
+ (instancetype)sharedInstance {
    static QJMediator *mediator;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        mediator = [[QJMediator alloc] init];
    });
    return mediator;
}

- (void)pushQJMediatorViewController:(NSDictionary *)targetPage {
    UIViewController *currentVC = [QJAppTool getCurrentViewController];
    [currentVC checkLogin:^(BOOL isLogin) {
        
        UIViewController *vc = [self getQJMediatorViewController:targetPage];
        if (vc != nil) {
            [self pushViewController:vc animated:YES];
        } else {
            NSString *name = targetPage[@"code"];
            NSMutableDictionary *param = [NSMutableDictionary dictionaryWithDictionary:targetPage[@"params"]];
            
            if ([[self QJMediatorModuleHome].allKeys containsObject:name]) {
                QJMediatorStyle style = (QJMediatorStyle)[[self QJMediatorModuleHome][name] integerValue];
                [self pushModuleHome:style param:param];
                
            } else {
    //            [QJAppTool showWarningToast:@"模块获取失败"];
                
            }
            
        }
    }];

}

// 特殊跳转处理
- (void)pushModuleHome:(QJMediatorStyle)style param:(NSDictionary *)param {
    switch (style) {
        case QJMediatorGameHome:
            [self pushGameHome];
            break;
        case QJMediatorCampDetail:
            [self pushCampDetail:param[@"id"]];
            break;
        case QJMediatorCampHome:
            [self pushCampHome:nil];
            break;
        case QJMediatorInformationHome:
            [self pushInformationHome];
            break;
        case QJMediatorInformationDetail:
            [self pushInformationDetail:param[@"id"]];
            break;
        case QJMediatorHelpCenterHome:
            [self pushHelpCenterHome:param[@"keyword"]];
            break;
        case QJMediatorKnowledge:
            [self pushKnowledgeHome:param[@"id"]];
            break;
        default:
            break;
    }
}

- (UIViewController *)getQJMediatorViewController:(NSDictionary *)targetPage {
    NSString *name = targetPage[@"code"];
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithDictionary:targetPage[@"params"]];
    if ([param.allKeys containsObject:@"title"]) {
        [param setObject:param[@"title"] forKey:@"titleStr"];
    }
    if ([param.allKeys containsObject:@"id"]) {
        [param setObject:param[@"id"] forKey:@"idStr"];
    }
    
    if ([[self QJMediatorModuleClassName].allKeys containsObject:name]) {
        NSString *className = [[self QJMediatorModuleClassName] objectForKey:name];
        UIViewController *viewController = [self getSpecificViewControllerFromInfo:@{@"kClassName":className,@"kProperty":param}];

        return viewController;
    } else {
        return nil;
    }
    
}

- (UIViewController *)getSpecificViewControllerFromInfo:(NSDictionary *)params {
    // 类名
    NSString *class =[NSString stringWithFormat:@"%@", params[@"kClassName"]];
    const char *className = [class cStringUsingEncoding:NSASCIIStringEncoding];
    // 从一个字串返回一个类
    Class newClass = objc_getClass(className);
    if (!newClass) {
        return nil;
    }
    // 创建对象
    id instance = [[newClass alloc] init];
    // 对该对象赋值属性
    NSDictionary * propertys = params[@"kProperty"];
    [propertys enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        // 检测这个对象是否存在该属性
        if ([self checkIsExistPropertyWithInstance:instance verifyPropertyName:key]) {
            // 利用kvc赋值
            [instance setValue:obj forKey:key];
        }
    }];

    return instance;

}

#pragma mark - 检查对象是否存在该属性
- (BOOL)checkIsExistPropertyWithInstance:(id)instance verifyPropertyName:(NSString *)verifyPropertyName {
    unsigned int outCount, i;
    // 获取对象里的属性列表
    objc_property_t * properties = class_copyPropertyList([instance
                                                           class], &outCount);
    for (i = 0; i < outCount; i++) {
        objc_property_t property =properties[i];
        //  属性名转成字符串
        NSString *propertyName = [[NSString alloc] initWithCString:property_getName(property) encoding:NSUTF8StringEncoding];
        // 判断该属性是否存在
        if ([propertyName isEqualToString:verifyPropertyName]) {
            free(properties);
            return YES;
        }
    }
    free(properties);
    return NO;
}

- (UIViewController *)topViewController {
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    
    return topController;
}

- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated {
    UINavigationController *navigationController = (UINavigationController *)[self topViewController];
    
    if ([navigationController isKindOfClass:[UINavigationController class]] == NO) {
        if ([navigationController isKindOfClass:[UITabBarController class]]) {
            UITabBarController *tabbarController = (UITabBarController *)navigationController;
            navigationController = tabbarController.selectedViewController;
            if ([navigationController isKindOfClass:[UINavigationController class]] == NO) {
                navigationController = tabbarController.selectedViewController.navigationController;
            }
        } else {
            navigationController = navigationController.navigationController;
        }
    }
    
    if ([navigationController isKindOfClass:[UINavigationController class]]) {
        [navigationController pushViewController:viewController animated:animated];
    }
}

- (void)presentViewController:(UIViewController *)viewControllerToPresent animated:(BOOL)animated completion:(void (^ _Nullable)(void))completion {
    UIViewController *viewController = [self topViewController];
    if ([viewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController *navigationController = (UINavigationController *)viewController;
        viewController = navigationController.topViewController;
    }
    
    if ([viewController isKindOfClass:[UIAlertController class]]) {
        UIViewController *viewControllerToUse = viewController.presentingViewController;
        [viewController dismissViewControllerAnimated:false completion:nil];
        viewController = viewControllerToUse;
    }
    
    if (viewController) {
        [viewController presentViewController:viewControllerToPresent animated:animated completion:completion];
    }
}

#pragma mark - 可以直接跳转的页面
- (NSDictionary *)QJMediatorModuleClassName {
    return @{@"/notice/simple":@"QJMsgSystemDetailViewController",
             @"/browser":@"QJWKWebViewController",//web页面
             @"/game/detail":@"QJProductDetailViewController",//游戏详情
             @"/qjcoin-mall/home":@"QJMallShopViewController",//奇迹币商城首页
             @"/mall/order/list":@"QJMallOrderListViewController",//商城订单列表
             @"/mall/order/detail":@"QJMallOrderDetailViewController",//商城订单详情
             @"/qj-gift":@"QJCheckInViewController",//奇迹有礼页面
             @"/qj-task/hall":@"QJMoreTaskViewController",//任务大厅
             @"/qj-task/mine":@"QJMyTaskViewController",//我的任务
             @"/help-center/question/detail":@"QJHelpCenterDetailViewController",//帮助中心问题详情
             @"/feedback":@"QJProblemFeedbackVC",//问题反馈
             @"/camp/detail":@"QJCommunityViewController",//营地详情
             
    };
}

#pragma mark - 需要做特殊处理的跳转页面
- (NSDictionary *)QJMediatorModuleHome {
    return @{
             @"/information/home":@(QJMediatorInformationHome),//资讯模块
             @"/information/detail":@(QJMediatorInformationDetail),//资讯详情
             @"/camp/news/detail":@(QJMediatorCampDetail),//营地-动态详情
             @"/camp/home":@(QJMediatorCampHome),//营地模块
             @"/game/home":@(QJMediatorGameHome),//游戏模块
             @"/help-center/question/search":@(QJMediatorHelpCenterHome),//帮助中心搜索结果
             @"/knowledge-base/detail":@(QJMediatorKnowledge),//知识库
    };
}

@end

