//
//  QJNotificationDefine.m
//  QJBox
//
//  Created by wxy on 2022/6/24.
//

#import <Foundation/Foundation.h>



NSNotificationName const QJHomeNestCellScrollNotification = @"QJHomeNestCellScrollNotification";
NSNotificationName const QJProductNestCellScrollNotification = @"QJProductNestCellScrollNotification";
NSNotificationName const QJProductDetailNestCellScrollNotification = @"QJProductDetailNestCellScrollNotification";
 

NSNotificationName const QJHomeNestMainCellScrollNotification = @"QJHomeNestMainCellScrollNotification";
NSNotificationName const QJProductNestMainCellScrollNotification = @"QJProductNestMainCellScrollNotification";
NSNotificationName const QJProductDetailNestMainCellScrollNotification = @"QJProductDetailNestMainCellScrollNotification";


NSNotificationName const QJHomeRefreshAllDataNotification = @"QJHomeRefreshAllDataNotification";

NSNotificationName const QJProductRefreshAllDataNotification = @"QJProductRefreshAllDataNotification";
NSNotificationName const QJProductDetailRefreshAllDataNotification = @"QJProductDetailRefreshAllDataNotification";
NSNotificationName const QJProductRefreshServerTimeNotification = @"QJProductRefreshServerTimeNotification";

NSNotificationName const QJCircleScrollSyncNotification = @"QJCircleScrollSyncNotification";

NSNotificationName const QJEditMyCampsiteList = @"QJEditMyCampsiteList";

NSNotificationName const QJDraftNotification = @"QJDraftNotification";

NSNotificationName const QJCampsiteBackImage = @"QJCampsiteBackImage";

NSNotificationName const QJMyCampsitePageRefresh = @"QJMyCampsitePageRefresh";

NSNotificationName const QJMyCampsiteListRefresh = @"QJMyCampsiteListRefresh";

NSNotificationName const QJEditMyCampsiteType = @"QJEditMyCampsiteType";

NSNotificationName const QJCampsiteClickLikeType = @"QJCampsiteClickLikeType";

NSNotificationName const QJMallConfirmOrderSuccess = @"QJMallConfirmOrderSuccess";

NSNotificationName const QJCommentRefresh = @"QJCommentRefresh";

NSNotificationName const QJPublish = @"QJPublish";




