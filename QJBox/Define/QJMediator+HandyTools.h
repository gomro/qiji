//
//  QJMediator+HandyTools.h
//  QJBox
//
//  Created by macm on 2022/9/9.
//

#import "QJMediator.h"

NS_ASSUME_NONNULL_BEGIN

// 点击事件类型
typedef NS_ENUM(NSInteger, QJMediatorStyle){
    QJMediatorGameHome = 0,  //游戏模块
    QJMediatorCampHome,     //营地模块
    QJMediatorCampDetail,     //指定营地
    QJMediatorInformationHome,     //资讯模块
    QJMediatorInformationDetail,     //资讯详情
    QJMediatorHelpCenterHome,     //帮助中心
    QJMediatorKnowledge,     //知识库

};

@interface QJMediator (HandyTools)
//在跳转时以下方法仅支持在主类使用，不在主类之外调用，跳转一律使用主类中的pushQJMediatorViewController: 方法
- (void)pushGameHome;

- (void)pushCampDetail:(NSString *)queryId;

- (void)pushCampHome:(NSString *_Nullable)campId;

- (void)pushInformationHome;

- (void)pushInformationDetail:(NSString *)queryId;

- (void)pushHelpCenterHome:(NSString *)text;

- (void)pushKnowledgeHome:(NSString *)queryId;

@end

NS_ASSUME_NONNULL_END
