//
//  QJEnvironmentConfigure.h
//  QJBox
//
//  Created by wxy on 2022/6/6.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, QJEnvironmentType) {
    QJEnvironmentTypeDev = 1,
    QJEnvironmentTypeRelease,

};


NS_ASSUME_NONNULL_BEGIN

/// 配置网络环境的类 分为：  测试环境，生产环境
@interface QJEnvironmentConfigure : NSObject

//当前环境
@property (nonatomic, assign) QJEnvironmentType currentEnvType;

//服务器地址
@property (nonatomic, copy) NSString *baseURL;

//图片服务器地址
@property (nonatomic, copy) NSString *baseImageURL;

//服务器版本号
@property (nonatomic, copy) NSString *serviceVersion;

+ (instancetype)shareInstance;

@property (nonatomic, copy) NSURL *videoUrl;
@end

NS_ASSUME_NONNULL_END
