//
//  QJNotificationDefine.h
//  QJBox
//
//  Created by wxy on 2022/6/24.
//

#ifndef QJNotificationDefine_h
#define QJNotificationDefine_h


#import <Foundation/Foundation.h>



UIKIT_EXTERN NSNotificationName const QJHomeNestCellScrollNotification;


UIKIT_EXTERN NSNotificationName const QJHomeNestMainCellScrollNotification;

UIKIT_EXTERN NSNotificationName const QJPublish;



UIKIT_EXTERN NSNotificationName const QJHomeRefreshAllDataNotification;

UIKIT_EXTERN NSNotificationName const QJProductRefreshAllDataNotification;//游戏中心刷新
UIKIT_EXTERN NSNotificationName const QJProductDetailRefreshAllDataNotification;//游戏中心刷新

UIKIT_EXTERN NSNotificationName const QJProductNestMainCellScrollNotification;//游戏中心滚动
UIKIT_EXTERN NSNotificationName const QJProductDetailNestCellScrollNotification;//游戏详情滚动
UIKIT_EXTERN NSNotificationName const QJProductDetailNestMainCellScrollNotification;//游戏详情滚动
UIKIT_EXTERN NSNotificationName const QJProductNestCellScrollNotification;//游戏中心底部
UIKIT_EXTERN NSNotificationName const QJProductRefreshServerTimeNotification;//游戏中心开服表刷新
UIKIT_EXTERN NSNotificationName const QJCircleScrollSyncNotification;

UIKIT_EXTERN NSNotificationName const QJEditMyCampsiteList;

UIKIT_EXTERN NSNotificationName const QJDraftNotification;

UIKIT_EXTERN NSNotificationName const QJCampsiteBackImage;//社区背景图

UIKIT_EXTERN NSNotificationName const QJMyCampsitePageRefresh;

UIKIT_EXTERN NSNotificationName const QJMyCampsiteListRefresh;

UIKIT_EXTERN NSNotificationName const QJEditMyCampsiteType;

UIKIT_EXTERN NSNotificationName const QJCampsiteClickLikeType;

UIKIT_EXTERN NSNotificationName const QJMallConfirmOrderSuccess; //兑换商品成功

UIKIT_EXTERN NSNotificationName const QJCommentRefresh; 

#endif /* QJNotificationDefine_h */

