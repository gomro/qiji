//
//  QJAppTool.h
//  QJBox
//
//  Created by macm on 2022/7/11.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJAppTool : NSObject
//单例
+ (instancetype)shareManager;
/*** 获取当前VC */
+ (UIViewController *)getCurrentViewController;

+ (NSString *)formatDaystringTime:(NSString *)string;

/** 获取时间距离 */
+ (NSString *)formatDayTime:(NSString *)string;

/** 获取当前时间戳 */
+ (NSString *)getCurrentTimestamp;

/** 获取一个随机字符串：当前时间戳13位+随机字母4位 */
+ (NSString *)getRandomString;

/** 隐藏loading */
+ (void)showHUDLoading;

/** 隐藏loading */
+ (void)hideHUDLoading;

/** 展示 警告 Toast*/
+ (void)showWarningToast:(NSString *)str;

/** 展示 成功 Toast*/
+ (void)showSuccessToast:(NSString *)str;

+ (void)showToast:(NSString *)toast;

/** 展示失败 Toast*/
+ (void)showFailToast:(NSString *)str;

//Object转Json
+ (NSString *)dictionaryToJson:(NSDictionary *)dic;
//Json转Object
+ (NSDictionary *)jsonToDictionary:(NSString *)jsonString;

@property (nonatomic, strong) NSString *taskID;//跳转社区模块的任务ID
@property (nonatomic, strong) NSString *campsiteID;//跳转营地模块的营地ID

@property (nonatomic, assign) double latitude;
@property (nonatomic, assign) double longitude;
@property (nonatomic, strong) NSString *selCity;
@property (nonatomic, strong) NSString *selPro;

/**营地数据**/
@property (nonatomic, copy) NSArray *campsiteArr;

@end

NS_ASSUME_NONNULL_END
