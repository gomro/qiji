//
//  QJMediator.h
//  QJBox
//
//  Created by macm on 2022/9/8.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJMediator : NSObject
+ (instancetype _Nonnull)sharedInstance;
- (void)pushQJMediatorViewController:(NSDictionary *)targetPage;
@end

NS_ASSUME_NONNULL_END
