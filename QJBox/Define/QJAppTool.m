//
//  QJAppTool.m
//  QJBox
//
//  Created by macm on 2022/7/11.
//

#import "QJAppTool.h"

@implementation QJAppTool

//单例
+ (instancetype)shareManager {
    static QJAppTool* manager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager =[[self alloc] init];
    });
    return manager;
}

/*** 获取当前VC */
+ (UIViewController *)getCurrentViewController {
    UIViewController* currentViewController = [UIApplication sharedApplication].delegate.window.rootViewController;
    BOOL runLoopFind = YES;
    while (runLoopFind) {
        if (currentViewController.presentedViewController) {
            currentViewController = currentViewController.presentedViewController;
        }
        else if ([currentViewController isKindOfClass:[UINavigationController class]]) {
            UINavigationController* navigationController = (UINavigationController* )currentViewController;
            currentViewController = [navigationController.childViewControllers lastObject];
        }
        else if ([currentViewController isKindOfClass:[UITabBarController class]]) {
            UITabBarController* tabBarController = (UITabBarController* )currentViewController;
            currentViewController = tabBarController.selectedViewController;
        }
        else {
            NSUInteger childViewControllerCount = currentViewController.childViewControllers.count;
            if (childViewControllerCount > 0) {
                currentViewController = currentViewController.childViewControllers.lastObject;
                return currentViewController;
            }
            else {
                return currentViewController;
            }
        }
    }
    return currentViewController;
}

+ (NSDate *)currentDate {
    NSDate *date = [NSDate date];
    NSTimeZone *zone = [NSTimeZone systemTimeZone];
    NSTimeInterval interval = [zone secondsFromGMTForDate:date];
    return [date dateByAddingTimeInterval:interval];
}

+ (NSString *)formatDaystringTime:(NSString *)string{
    NSString *currentMonth = [NSString getStringForDateString:string byFormat:@"yyyy-MM-dd" toFormat:@"yyyy-MM-dd HH:mm:ss"];
    return [self formatDayTime:currentMonth];
}

/** 获取时间距离 */
+ (NSString *)formatDayTime:(NSString *)string {
    if (!string) {
        return @"";
    }
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [dateFormatter dateFromString:string];
    NSTimeZone *zone = [NSTimeZone systemTimeZone];
    NSTimeInterval interval = [zone secondsFromGMTForDate:date];
    NSDate *nowDate = [date dateByAddingTimeInterval:interval];
        
    
    float different = [nowDate timeIntervalSinceDate:[QJAppTool currentDate]];
    if (different < 0) {
      different = - different;
    }
    
    // days = different / (24 * 60 * 60), take the floor value
    float dayDifferent = floor(different / 86400);
    
    int days   = (int)dayDifferent;

    // It belongs to today
    if (dayDifferent <= 0) {
      // lower than 60 seconds
      if (different < 60) {
        return @"刚刚";
      }
      
      // lower than 120 seconds => one minute and lower than 60 seconds
      if (different < 120) {
        return [NSString stringWithFormat:@"1分钟前"];
      }
      
      // lower than 60 minutes
      if (different < 60 * 60) {
        return [NSString stringWithFormat:@"%d分钟前", (int)floor(different / 60)];
      }
      
      // lower than 60 * 2 minutes => one hour and lower than 60 minutes
      if (different < 7200) {
        return [NSString stringWithFormat:@"1小时前"];
      }
      
      // lower than one day
      if (different < 86400) {
        return [NSString stringWithFormat:@"%d小时前", (int)floor(different / 3600)];
      }
    }
    // lower than one week
    else if (days < 4) {
        return [NSString stringWithFormat:@"%d天前", days];
    }

    else {
        NSRange range = {2, 8};
        return [string substringWithRange:range];
    }

    return self.description;

}

/** 获取当前时间戳 */
+ (NSString *)getCurrentTimestamp {
    NSDate *date = [NSDate dateWithTimeIntervalSinceNow:0]; // 获取当前时间0秒后的时间
    NSTimeInterval time = [date timeIntervalSince1970]*1000;// *1000 是精确到毫秒(13位),不乘就是精确到秒(10位)
    NSString *timeString = [NSString stringWithFormat:@"%.0f", time];
    return timeString;
}

/** 隐藏loading */
+ (void)showHUDLoading {
    UIWindow * window = [UIApplication sharedApplication].delegate.window;
    [MBProgressHUD showHUDAddedTo:window animated:YES];
}

/** 隐藏loading */
+ (void)hideHUDLoading {
    UIWindow * window = [UIApplication sharedApplication].delegate.window;
    [MBProgressHUD hideHUDForView:window animated:YES];
}

/** 展示 警告 Toast*/
+ (void)showWarningToast:(NSString *)str {
    UIWindow * window = [UIApplication sharedApplication].delegate.window;
    [window makeToast:str duration:3 position:CSToastPositionCenter image:[UIImage imageNamed:@"tips_toast"]];
}

/** 展示 成功 Toast*/
+ (void)showSuccessToast:(NSString *)str {
    UIWindow * window = [UIApplication sharedApplication].delegate.window;
    [window makeToast:str duration:3 position:CSToastPositionCenter image:[UIImage imageNamed:@"success_login"]];
}

/** 展示失败 Toast*/
+ (void)showFailToast:(NSString *)str {
    UIWindow * window = [UIApplication sharedApplication].delegate.window;
    [window makeToast:str duration:3 position:CSToastPositionCenter image:[UIImage imageNamed:@"fail_login"]];
}



+ (void)showToast:(NSString *)toast {
    UIWindow * window = [UIApplication sharedApplication].delegate.window;
    [window  makeToast:toast];
}

/** 获取一个随机字符串：当前时间戳13位+随机字母4位 */
+ (NSString *)getRandomString {

    NSString *string = [[NSString alloc]init];

    for (int i = 0; i < 4; i++) {

        int number = arc4random() % 36;

        if (number < 10) {

            int figure = arc4random() % 10;

            NSString *tempString = [NSString stringWithFormat:@"%d", figure];

            string = [string stringByAppendingString:tempString];

        }else {

            int figure = (arc4random() % 26) + 97;

            char character = figure;

            NSString *tempString = [NSString stringWithFormat:@"%c", character];

            string = [string stringByAppendingString:tempString];

        }

    }
    
    return [NSString stringWithFormat:@"%@_%@",[self getCurrentTimestamp],string];

}

//Object转Json
+ (NSString *)dictionaryToJson:(NSDictionary *)dic
{
//    NSString *json = @"";
//    NSError *parseError = nil;
//    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:object options:NSJSONWritingPrettyPrinted error:&parseError];
//    json =  [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
//    json = [json stringByReplacingOccurrencesOfString:@"\r\n" withString:@""];
//    json = [json stringByReplacingOccurrencesOfString:@"\n" withString:@""];
//    json = [json stringByReplacingOccurrencesOfString:@"\t" withString:@""];
//    json = [json stringByReplacingOccurrencesOfString:@" " withString:@""];
    // dic 为初始字典
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic options:NSJSONWritingPrettyPrinted error:nil];
    // json 为 转换后的 字符串
    NSString *json = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    return json;
}

//Json转Object
+ (NSDictionary *)jsonToDictionary:(NSString *)jsonString {
    if (jsonString == nil) {
     return nil;
    }

    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *err;
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData
                                                       options:NSJSONReadingMutableContainers
                                                          error:&err];
    if(err) {
//        NSString *log = [NSString stringWithFormat:@"%d, %s | json解析失败：%@", __LINE__, __func__, err];
        return nil;
    }
    return dic;
}


@end
