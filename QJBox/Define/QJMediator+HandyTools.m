//
//  QJMediator+HandyTools.m
//  QJBox
//
//  Created by macm on 2022/9/9.
//

#import "QJMediator+HandyTools.h"
#import "AppDelegate.h"
#import "QJCommunityViewController.h"
#import "QJCampsiteListModel.h"
#import "QJCampsiteRequest.h"
#import "QJMineRequest.h"
#import "QJHelpCenterSearchVC.h"
#import "QJConsultDetailViewController.h"

@implementation QJMediator (HandyTools)

- (void)pushGameHome {
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    QJAppTabBarController *tab = (QJAppTabBarController *)delegate.window.rootViewController;
    tab.selectedIndex = 1;
    [[QJAppTool getCurrentViewController].navigationController popViewControllerAnimated:NO];
}

- (void)pushCampDetail:(NSString *)queryId {
    NSMutableArray *dataArr = [NSMutableArray arrayWithCapacity:0];
    NSMutableArray *idArr = [NSMutableArray arrayWithCapacity:0];
    NSArray *myCampsite = LSUserDefaultsGET(@"kMyCampsiteList");
    for (NSDictionary *dic in myCampsite) {
        QJCampsiteListModel *model = [QJCampsiteListModel modelWithDictionary:dic];
        [dataArr addObject:dic];
        [idArr addObject:model.campsiteID];
    }
                
    // 如果选择的营地，没有在我关注的营地里面，先去添加然后再跳转
    if (![idArr containsObject:queryId]) {
        [idArr addObject:queryId];
        [QJAppTool showHUDLoading];
        QJCampsiteRequest *startRequest = [[QJCampsiteRequest alloc] init];
        [startRequest netWorkPostCampSet:idArr];
        [startRequest setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
            [QJAppTool hideHUDLoading];
            if (ResponseSuccess) {
                [self pushCampHome:queryId];
            } else {
                [QJAppTool showWarningToast:@"跳转营地失败，请重试！"];
            }
        }];
        
    } else {
        [self pushCampHome:queryId];
    }
}

- (void)pushCampHome:(NSString *_Nullable)campId {
    if (campId != nil) {
        [QJAppTool shareManager].campsiteID = campId;
    }
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    QJAppTabBarController *tab = (QJAppTabBarController *)delegate.window.rootViewController;
    QJNavigationController *navController = [tab.viewControllers safeObjectAtIndex:3];
    QJCommunityViewController *communityVC = (QJCommunityViewController *)navController.topViewController;
    communityVC.communitySelectRow = 1;
    tab.selectedIndex = 3;
    [[QJAppTool getCurrentViewController].navigationController popViewControllerAnimated:NO];
}

- (void)pushInformationHome {
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    QJAppTabBarController *tab = (QJAppTabBarController *)delegate.window.rootViewController;
    QJNavigationController *navController = [tab.viewControllers safeObjectAtIndex:3];
    QJCommunityViewController *communityVC = (QJCommunityViewController *)navController.topViewController;
    communityVC.communitySelectRow = 0;
    tab.selectedIndex = 3;
    [[QJAppTool getCurrentViewController].navigationController popViewControllerAnimated:NO];
}

- (void)pushInformationDetail:(NSString *)queryId {
    QJConsultDetailViewController *detailView = [[QJConsultDetailViewController alloc] init];
    detailView.idStr = queryId;
    detailView.isNews = YES;
    [[QJAppTool getCurrentViewController].navigationController pushViewController:detailView animated:YES];
}

- (void)pushHelpCenterHome:(NSString *)text {
    QJHelpCenterSearchVC *vc = [[QJHelpCenterSearchVC alloc] init];
    vc.isAI = YES;
    vc.searchStr = text;
    [[QJAppTool getCurrentViewController].navigationController pushViewController:vc animated:YES];
}

- (void)pushKnowledgeHome:(NSString *)queryId {
    [QJAppTool showHUDLoading];
    QJMineRequest *request = [[QJMineRequest alloc] init];
    [request requestKnowledge_base_question_details_idStr:queryId completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        [QJAppTool hideHUDLoading];
        if (isSuccess) {
            NSDictionary *data = EncodeDicFromDic(request.responseJSONObject, @"data");
            NSString *content = data[@"content"];
            if (content.length > 0 && content != nil) {
                QJWKWebViewController *vc = [QJWKWebViewController new];
                vc.htmlString = content;
                vc.navTitle = @"知识库";
                [[QJAppTool getCurrentViewController].navigationController pushViewController:vc animated:YES];
            }

        }

    }];
}

@end
