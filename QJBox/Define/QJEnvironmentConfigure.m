//
//  QJEnvironmentConfigure.m
//  QJBox
//
//  Created by wxy on 2022/6/6.
//

#import "QJEnvironmentConfigure.h"

@implementation QJEnvironmentConfigure


+ (instancetype)shareInstance {
    static QJEnvironmentConfigure *environ = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (environ == nil) {
            environ = [[QJEnvironmentConfigure alloc]init];
        }
    });
    return environ;
}




#pragma mark -------- getter

- (NSString *)baseURL {
    switch (self.currentEnvType) {
        case QJEnvironmentTypeDev://测试环境
            return @"http://app-api-t.sxmu.com";//@"http://10.0.0.164:18080";//
            break;
        case QJEnvironmentTypeRelease://生产环境
            return @"https://app-api-t.sxmu.com";
            break;
        default:
            return @"";
            break;
    }
}

//默认地址
- (NSString *)baseImageURL {
    if (!_baseImageURL) {
        _baseImageURL = @"https://sxqj.oss-cn-hangzhou.aliyuncs.com";
    }
    return _baseImageURL;
}

- (NSString *)serviceVersion {
    return @"0";
}


@end
