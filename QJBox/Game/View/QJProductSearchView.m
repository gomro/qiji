//
//  QJProductSearchView.m
//  QJBox
//
//  Created by wxy on 2022/7/27.
//

#import "QJProductSearchView.h"

@interface QJProductSearchView ()<UITextFieldDelegate>




@property (nonatomic, strong) UIButton *cancelBtn;


@end


@implementation QJProductSearchView


- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self addSubview:self.searchTF];
        [self addSubview:self.cancelBtn];
        self.cancelBtn.hidden = YES;
    
        [self.cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.centerY.right.equalTo(self);
            make.width.equalTo(@63);
        }];
        [self.searchTF mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.bottom.equalTo(self);
            make.right.equalTo(self.cancelBtn.mas_left);
        }];
        
    }
    return self;
}


#pragma mark   ----- getter

- (UITextField *)searchTF {
    if (!_searchTF) {
        _searchTF = [UITextField new];
        _searchTF.backgroundColor = kColorWithHexString(@"#F5F5F5");
        _searchTF.textColor = UIColorFromRGB(0x474849);
        _searchTF.font = FontRegular(14);
        _searchTF.layer.cornerRadius = 17;
        
        NSMutableAttributedString *placeholderString = [[NSMutableAttributedString alloc] initWithString:@"搜索你想玩的游戏" attributes:@{NSForegroundColorAttributeName : UIColorFromRGB(0xC8CACC), NSFontAttributeName : kFont(14)}];
        _searchTF.attributedPlaceholder = placeholderString;
        UIView *searchView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 34, 30)];
        UIImageView *searchImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_search"]];
        searchImage.frame = CGRectMake(14, 7, 16, 16);
        [searchView addSubview:searchImage];
        _searchTF.leftView = searchView;
        _searchTF.leftViewMode = UITextFieldViewModeAlways;
        _searchTF.clearButtonMode = UITextFieldViewModeWhileEditing;
        _searchTF.delegate = self;
  
        _searchTF.returnKeyType = UIReturnKeySearch;
    }
    return _searchTF;
}

- (void)setHiddenCancelBtn:(BOOL)hiddenCancelBtn {
    _hiddenCancelBtn = hiddenCancelBtn;
    self.cancelBtn.hidden = hiddenCancelBtn;
}

- (UIButton *)cancelBtn {
    if (!_cancelBtn) {
        _cancelBtn = [UIButton new];
        [_cancelBtn setTitle:@"取消" forState:UIControlStateNormal];
        _cancelBtn.titleLabel.font = kFont(14);
        [_cancelBtn setTitleColor:UIColorFromRGB(0x101010) forState:UIControlStateNormal];
        [_cancelBtn addTarget:self action:@selector(resignBtnAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _cancelBtn;
}


- (void)resignBtnAction{
    self.searchTF.text = @"";
    [self.searchTF resignFirstResponder];
    self.cancelBtn.hidden = YES;
    if (self.cancelBlock) {
        self.cancelBlock();
    }
}

// 获得焦点
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    self.cancelBtn.hidden = NO;
    if (self.hiddenCancelBtn) {
        self.cancelBtn.hidden = YES;
    }
    if (self.beginEditBlock) {
        self.beginEditBlock();
    }
    return YES;
}

 

 

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField.markedTextRange == nil) {
        NSString *toBeString = textField.text;
        NSInteger kMaxLength = 10;
        if (toBeString.length > kMaxLength) {
            textField.text = [toBeString substringToIndex:kMaxLength];
          
        }
       
    }
    [self.searchTF resignFirstResponder];
    if (self.searchStrBlock) {
        self.searchStrBlock(textField.text);
    }
    return YES;
    
}

@end
