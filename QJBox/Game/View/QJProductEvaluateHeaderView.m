//
//  QJProductEvaluateHeaderView.m
//  QJBox
//
//  Created by rui on 2022/7/27.
//

#import "QJProductEvaluateHeaderView.h"
/* 评价星星view */
#import "QJEvaluateCustomStarVIew.h"

@interface QJProductEvaluateHeaderView()

/* 标题-评分 */
@property (nonatomic, strong) UILabel *titleLabel;
/* 评分view */
@property (nonatomic, strong) UIView *descView;
/* 游戏评分 */
@property (nonatomic, strong) UILabel *pointLabel;
/* 游戏评分范围-满分5分 */
@property (nonatomic, strong) UILabel *descLabel;
/* 游戏评论总数 */
@property (nonatomic, strong) UILabel *contentLabel;
/* 评价星星view */
@property (nonatomic, strong) QJEvaluateCustomStarVIew *starView;

@end

@implementation QJProductEvaluateHeaderView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        /* 初始化UI */
        [self initChildView];
    }
    return self;
}

/**
 * @author: zjr
 * @date: 2022-8-2
 * @desc: 加载view/约束
 */
- (void)initChildView {
    [self addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(16);
        make.left.equalTo(self).offset(16);
        make.right.equalTo(self).offset(-16);
    }];
    
    [self addSubview:self.descView];
    [self.descView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@(84*kWScale));
        make.left.equalTo(self).offset(16);
        make.right.equalTo(self).offset(-16);
        make.top.equalTo(self.titleLabel.mas_bottom).offset(16);
    }];
    
    [self.descView addSubview:self.pointLabel];
    [self.descView addSubview:self.descLabel];
    [self.descView addSubview:self.contentLabel];
    [self.pointLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.descView).offset(27);
        make.top.equalTo(self.descView);//.offset(10);
    }];
    [self.descLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.descView).offset(-16);
        make.top.equalTo(self.descView).offset(26);
    }];
    [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.descView).offset(-16);
        make.top.equalTo(self.descLabel.mas_bottom).offset(4);
    }];
    
    self.starView.currentScore = 4.3;
    
    [self.descView addSubview:self.starView];
    [self.starView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.descView).offset(-8);
        make.left.equalTo(self.descView).offset(15);
        make.size.mas_equalTo(CGSizeMake(100*kWScale,15*kWScale));
    }];
    
//    [self.starView updateWithIndex:5];
}

#pragma mark - Lazy Loading
- (UIView *)descView{
    if (!_descView) {
        _descView = [UIView new];
        _descView.backgroundColor = UIColorFromRGB(0xF5F5F5);
        _descView.layer.cornerRadius = 8;
        _descView.layer.masksToBounds = YES;
    }
    return _descView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.font = kboldFont(16);
        _titleLabel.textColor = [UIColor blackColor];
        _titleLabel.text = @"评分";
    }
    return _titleLabel;
}

- (UILabel *)pointLabel {
    if (!_pointLabel) {
        _pointLabel = [UILabel new];
        _pointLabel.font = kboldFont(44);
        _pointLabel.textColor = UIColorFromRGB(0xFF9500);
        _pointLabel.text = @"0";
    }
    return _pointLabel;
}

- (UILabel *)descLabel {
    if (!_descLabel) {
        _descLabel = [UILabel new];
        _descLabel.font = kFont(11);
        _descLabel.textColor = UIColorFromRGB(0x919599);
        _descLabel.text = @"满分5分";
    }
    return _descLabel;
}

- (UILabel *)contentLabel {
    if (!_contentLabel) {
        _contentLabel = [UILabel new];
        _contentLabel.font = kFont(11);
        _contentLabel.textColor = UIColorFromRGB(0x919599);
        _contentLabel.text = @"";
    }
    return _contentLabel;
}

- (QJEvaluateCustomStarVIew *)starView{
    if (!_starView) {
        _starView = [[QJEvaluateCustomStarVIew alloc] initWithFrame:CGRectMake(0, 0, 100*kWScale, 15*kWScale)];
        _starView.starSpaceing = 0;
        _starView.rateStyle = QJStarIncompleteStar;
        _starView.backgroundColor = UIColorFromRGB(0xF5F5F5);
        _starView.userInteractionEnabled = NO;
    }
    return _starView;
}

/**
 * @author: zjr
 * @date: 2022-8-2
 * @desc: 数据源赋值
 */
- (void)setDetailModel:(QJSearchGameDetailModel *)detailModel{
    if (_detailModel != detailModel) {
        _detailModel = detailModel;
        _pointLabel.text = kCheckStringNil(_detailModel.point);
        if ([_pointLabel.text isEqualToString:@"0"]) {
            [self.pointLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.centerX.equalTo(self.starView);
                make.top.equalTo(self.descView);//.offset(10);
            }];
        } else {
            [self.pointLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.descView).offset(27);
                make.top.equalTo(self.descView);//.offset(10);
            }];
        }
        _starView.currentScore = [_detailModel.point floatValue];
    }
}

- (void)updateCount:(NSString *)countStr{
    NSString *countAllStr = countStr;
    if (countStr.floatValue > 100000) {
        countAllStr = [NSString stringWithFormat:@"%.1f",countStr.floatValue/10000];
        _contentLabel.text = [NSString stringWithFormat:@"%@万+个评价",countAllStr];
    }else{
        _contentLabel.text = [NSString stringWithFormat:@"%@个评价",countAllStr];
    }
}

@end
