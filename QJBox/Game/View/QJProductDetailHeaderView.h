//
//  QJProductDetailHeaderView.h
//  QJBox
//
//  Created by rui on 2022/7/27.
//

#import <UIKit/UIKit.h>
#import "QJSearchGameListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJProductDetailHeaderView : UIView

/* 数据源 */
@property (nonatomic, strong) QJSearchGameDetailModel *detailModel;

@end

NS_ASSUME_NONNULL_END
