//
//  QJProductCommentListView.m
//  QJBox
//
//  Created by rui on 2022/7/21.
//

#import "QJProductCommentListView.h"
/* 产品评论公共cell */
#import "QJProductCommentCommonCell.h"
/* 弹框 */
#import "QJDetailAlertView.h"
#import "TYAlertController.h"
/* 请求 */
#import "QJProductRequest.h"
#import "QJCampsiteRequest.h"
/* model */
#import "QJProductCommentListModel.h"
/* 底部弹出alertview */
#import "QJBottomSheetAlertView.h"
/* 底部滚动view */
#import "GKPageScrollView.h"

@interface QJProductCommentListView()<GKPageListViewDelegate,UITableViewDelegate,UITableViewDataSource,UIActionSheetDelegate>

/* table */
@property (nonatomic, strong) UITableView *tableView;
/* GKScroll回调 */
@property (nonatomic, copy) void(^listScrollViewScrollBlock)(UIScrollView *scrollView);
/* model */
@property (nonatomic, strong) QJProductCommentListModel *pageModel;

@end

@implementation QJProductCommentListView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.pageModel = [[QJProductCommentListModel alloc] init];
        /* 初始化View */
        [self initChildView];
    }
    return self;
}

/**
 * @author: zjr
 * @date: 2022-8-2
 * @desc: 加载view/约束
 */
- (void)initChildView {
    self.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
}

#pragma mark --Lazy Loading
-(UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = [UIColor clearColor];
        [_tableView registerClass:[QJProductCommentCommonCell class] forCellReuseIdentifier:@"QJProductCommentCommonCell"];
        // 自动计算行高模式
        _tableView.estimatedRowHeight = 100;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        if (@available(iOS 11.0, *)) {
            _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
        if (@available(iOS 15.0, *)) {
            _tableView.sectionHeaderTopPadding = 0;
        }
        _tableView.mj_header = [QJRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(refreshFirst)];
        _tableView.mj_footer = [QJRefreshFooter footerWithRefreshingTarget:self refreshingAction:@selector(refreshMore)];
    }
    return _tableView;
}

#pragma mark --tableview
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.pageModel.records.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    QJProductCommentDetailModel *model = [self.pageModel.records safeObjectAtIndex:indexPath.row];
    QJProductCommentCommonCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QJProductCommentCommonCell" forIndexPath:indexPath];
    cell.model = model;
    WS(weakSelf)
    cell.showAllHeadViewClick = ^(BOOL isShow) {
        QJProductCommentDetailModel *model = self.pageModel.records[indexPath.row];
        model.isShowAll = isShow;
        weakSelf.pageModel.records[indexPath.row] = model;

        [UIView performWithoutAnimation:^{
            [weakSelf.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        }];
    };
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    QJProductCommentDetailModel *model = [self.pageModel.records safeObjectAtIndex:indexPath.row];
    NSString *userIdSave = LSUserDefaultsGET(kQJUserId);
    BOOL isSelf = NO;
    if ([kCheckStringNil(userIdSave) isEqualToString:kCheckStringNil(model.userId)] && ![kCheckStringNil(model.userId) isEqualToString:@""]) {
        isSelf = YES;
    }
    if (isSelf) {
        WS(weakSelf)
        QJBottomSheetAlertView *view = [[QJBottomSheetAlertView alloc]initWithTitles:@[@"删除"]];
        view.clickIndex = ^(NSInteger index) {
            if (index == 0) {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [weakSelf deleteCommentAction:model];
                });
            }
        };
        [[QJAppTool getCurrentViewController] presentViewController:view animated:NO completion:nil];
    }
}

#pragma mark - GKPageListViewDelegate
- (UIScrollView *)listScrollView {
    return self.tableView;
}

- (void)listViewDidScrollCallback:(void (^)(UIScrollView * _Nonnull))callback {
    self.listScrollViewScrollBlock = callback;
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    !self.listScrollViewScrollBlock ? : self.listScrollViewScrollBlock(scrollView);
}

#pragma mark - 请求方法
/**
 * @author: zjr
 * @date: 2022-8-2
 * @desc: 请求方法
 */
- (void)refreshFirst{
    _pageModel.willLoadMore = NO;
    [self sendRequest];
}

- (void)refreshMore{
    if(_pageModel.hasNext){
        _pageModel.willLoadMore=YES;
        [self sendRequest];
    }else{
        [self.tableView.mj_footer endRefreshing];
        [self.tableView.mj_footer endRefreshingWithNoMoreData];
    }
}

- (void)sendRequest {
    QJProductRequest *productReq = [[QJProductRequest alloc] init];
    NSMutableDictionary *muDic = [NSMutableDictionary dictionaryWithDictionary:_pageModel.toParams];
    [muDic setValue:@"20" forKey:@"size"];
    [muDic setValue:@"0" forKey:@"type"];
    if ([self.type isEqual:@"starSort"]) {
        [muDic setValue:@"1" forKey:@"level"];
    }else if ([self.type isEqual:@"myPublish"]) {
        [muDic setValue:@"1" forKey:@"isMe"];
    }
    productReq.dic = muDic.copy;
    [productReq getProductCommentRoot:self.productID];
    
    WS(weakSelf)
    productReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            [weakSelf.pageModel configObj:[QJProductCommentListModel modelWithDictionary:EncodeDicFromDic(request.responseObject, @"data")]];
            if ([weakSelf.type isEqual:@"starSort"]) {
                if (weakSelf.requestSuccessClick) {
                    weakSelf.requestSuccessClick([NSString stringWithFormat:@"%@",weakSelf.pageModel.total]);
                }
            }
        }else{
            NSString *msg = EncodeStringFromDic(request.responseObject, @"message");
            [weakSelf makeToast:msg];
        }
        [weakSelf.tableView reloadData];
        [weakSelf.tableView.mj_header endRefreshing];
        [weakSelf.tableView.mj_footer endRefreshing];
        QJEmptyCommonView *emptyView = [QJEmptyCommonView new];
        emptyView.string = @"该游戏暂无评论";
        emptyView.emptyImage = @"empty_no_data";
        emptyView.topSpace = 100*kWScale;
        [weakSelf.tableView tableViewDisplayWhenHaveNoDataWithView:emptyView ifNecessaryForRowCount:self.pageModel.records.count];
    };
    productReq.failureCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        [weakSelf.tableView reloadData];
        [weakSelf.tableView.mj_header endRefreshing];
        [weakSelf.tableView.mj_footer endRefreshing];
        [weakSelf makeToast:@"数据异常 请稍后再试"];
        QJEmptyCommonView *emptyView = [QJEmptyCommonView new];
        emptyView.string = @"该游戏暂无评论";
        emptyView.emptyImage = @"empty_no_data";
        emptyView.topSpace = 100*kWScale;
        [weakSelf.tableView tableViewDisplayWhenHaveNoDataWithView:emptyView ifNecessaryForRowCount:self.pageModel.records.count];
    };
}

/**
 * @author: zjr
 * @date: 2022-8-2
 * @desc: 删除评论
 */
- (void)deleteCommentAction:(QJProductCommentDetailModel *)model{
    QJDetailAlertView *alertView = [[QJDetailAlertView alloc] initWithFrame:CGRectZero titleType:QJDetailAlertViewTitleTypeDefault buttonType:QJDetailAlertViewButtonTypeDefault];
    [alertView showTitleText:@"提示" describeText:@"确定要删除您的评论吗？"];
    [alertView.leftButton setTitle:@"取消" forState:UIControlStateNormal];
    [alertView.rightButton setTitle:@"删除" forState:UIControlStateNormal];
    MJWeakSelf
    [alertView returnClick:^(QJDetailAlertViewButtonClickType type) {
        [[QJAppTool getCurrentViewController] dismissViewControllerAnimated:YES completion:nil];
        if (type == QJDetailAlertViewButtonClickTypeRight) {
            [weakSelf deleteComment:model];
        }
    }];
    TYAlertController *alertController = [TYAlertController alertControllerWithAlertView:alertView preferredStyle:TYAlertControllerStyleAlert];
    [[QJAppTool getCurrentViewController] presentViewController:alertController animated:YES completion:nil];
}

- (void)deleteComment:(QJProductCommentDetailModel *)model{
    [QJAppTool showHUDLoading];
    QJCampsiteRequest *startRequest = [[QJCampsiteRequest alloc] init];
    [startRequest netWorkDeleteComment:model.commentId newsId:@""];
    [startRequest setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        [QJAppTool hideHUDLoading];
        if (ResponseSuccess) {
            [[QJAppTool getCurrentViewController].view makeToast:@"删除成功"];
            [self resetModelList:model];
        }
    }];
    [startRequest setFailureCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        [QJAppTool hideHUDLoading];
    }];
}

/**
 * @author: zjr
 * @date: 2022-8-2
 * @desc: 删除评论后刷新列表
 */
- (void)resetModelList:(QJProductCommentDetailModel *)model{
    for (QJProductCommentDetailModel *dataModel in self.pageModel.records) {
        if(dataModel.commentId.integerValue == model.commentId.integerValue){
            [self.pageModel.records removeObject:dataModel];
            [self.tableView reloadData];
            break;
        }
    }
}

@end
