//
//  QJProductCommentListEmptyView.h
//  QJBox
//
//  Created by wxy on 2022/9/22.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJProductCommentListEmptyView : UIView
@property (nonatomic, strong) UIColor *bgColor;

@property (nonatomic, copy) NSString *string;
@end

NS_ASSUME_NONNULL_END
