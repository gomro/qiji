//
//  QJProductDetailHeaderView.m
//  QJBox
//
//  Created by rui on 2022/7/27.

#import "QJProductDetailHeaderView.h"
/* 轮播 */
#import "SPCycleScrollView.h"
/* 游戏标签 */
#import "QJMineMsgTagView.h"
/* 自定义带图片的view */
#import "QJImageAndLabelView.h"
/* 游戏评价页面 */
#import "QJProductEvaluateViewController.h"
#import "FMTagsView.h"

@interface QJColorGraduateView : UIView

@end

@implementation QJColorGraduateView

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        
    }
    return self;
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    UIView *view = [super hitTest:point withEvent:event];
    if (view == self) {
        return nil;
    }
    return view;
}

@end

@interface QJProductDetailHeaderView()

/* 轮播图 */
@property (nonatomic, strong) SPCycleScrollView *bannerView;
/* 背景渐变view */
@property (nonatomic, strong) QJColorGraduateView *colorView;
/* 游戏简介 */
@property (nonatomic, strong) UIView *descView;
@property (nonatomic, strong) UIImageView *iconImageView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) FMTagsView *tagView;
/* 评价button */
@property (nonatomic, strong) UIButton *evaluateButton;
@property (nonatomic, strong) QJImageAndLabelView *gradeLabel;
@property (nonatomic, strong) UILabel *evaluateLabel;

/* 游戏评分/排名/下载 */
@property (nonatomic, strong) UIView *contentView;
@property (nonatomic, strong) UILabel *rankLabel;
@property (nonatomic, strong) UILabel *downLoadLabel;
@property (nonatomic, strong) UILabel *onLineLabel;
@property (nonatomic, strong) UILabel *rankContentLabel;
@property (nonatomic, strong) UILabel *downLoadContentLabel;
@property (nonatomic, strong) UILabel *onLineContentLabel;

@end

@implementation QJProductDetailHeaderView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        /* 初始化UI */
        [self initChildView];
    }
    return self;
}

/**
 * @author: zjr
 * @date: 2022-8-2
 * @desc: 加载view/约束
 */
- (void)initChildView {
    [self addSubview:self.bannerView];
    [self.bannerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@(261*kWScale));
        make.top.equalTo(self);
        make.left.equalTo(self);
        make.right.equalTo(self);
    }];
    
    [self addSubview:self.colorView];
    [self.colorView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@(261*kWScale));
        make.top.equalTo(self);
        make.left.equalTo(self);
        make.right.equalTo(self);
    }];
    [self.colorView layoutIfNeeded];
    [self.colorView graduateLeftColor:[UIColor colorWithWhite:0 alpha:0.06] ToColor:UIColorFromRGB(0x05101C) startPoint:CGPointMake(0.5, 0) endPoint:CGPointMake(0.5, 1)];
    
    [self addSubview:self.contentView];
    [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@(65*kWScale));
        make.left.right.equalTo(self);
        make.bottom.equalTo(self);
    }];
    [self.contentView layoutIfNeeded];
    [self.contentView showCorner:8 rectCorner:UIRectCornerTopLeft | UIRectCornerTopRight];
    
    [self.contentView addSubview:self.rankLabel];
    [self.contentView addSubview:self.rankContentLabel];
    [self.contentView addSubview:self.downLoadLabel];
    [self.contentView addSubview:self.downLoadContentLabel];
    [self.contentView addSubview:self.onLineLabel];
    [self.contentView addSubview:self.onLineContentLabel];
    [self.rankContentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@(30*kWScale));
        make.width.equalTo(@(QJScreenWidth/3));
        make.left.equalTo(self.contentView);
        make.top.equalTo(self.contentView).offset(10);
    }];
    
    [self.rankLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@(20*kWScale));
        make.width.equalTo(@(QJScreenWidth/3));
        make.left.equalTo(self.contentView);
        make.top.equalTo(self.contentView).offset(40);
    }];
    
    [self.downLoadContentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@(30*kWScale));
        make.width.equalTo(@(QJScreenWidth/3));
        make.left.equalTo(self.rankContentLabel.mas_right);
        make.top.equalTo(self.contentView).offset(10);;
    }];
    
    [self.downLoadLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@(20*kWScale));
        make.width.equalTo(@(QJScreenWidth/3));
        make.left.equalTo(self.rankLabel.mas_right);
        make.top.equalTo(self.contentView).offset(40);
    }];
    
    [self.onLineContentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@(30*kWScale));
        make.width.equalTo(@(QJScreenWidth/3));
        make.left.equalTo(self.downLoadContentLabel.mas_right);
        make.top.equalTo(self.contentView).offset(10);;
    }];
    
    [self.onLineLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@(20*kWScale));
        make.width.equalTo(@(QJScreenWidth/3));
        make.left.equalTo(self.downLoadLabel.mas_right);
        make.top.equalTo(self.contentView).offset(40);
    }];
    
    [self addSubview:self.descView];
    [self.descView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@(88*kWScale));
        make.left.right.equalTo(self);
        make.bottom.equalTo(self.contentView.mas_top);
    }];
    
    [self.descView addSubview:self.iconImageView];
    [self.descView addSubview:self.titleLabel];
    [self.descView addSubview:self.tagView];
    [self.descView addSubview:self.evaluateButton];
    
    [self.iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.descView).offset(12);
        make.left.equalTo(self.descView).offset(21);
        make.width.equalTo(@(64*kWScale));
        make.height.equalTo(@(64*kWScale));
    }];
    
    [self.evaluateButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.descView);
        make.width.equalTo(@(42*kWScale));
        make.height.equalTo(@(42*kWScale));
        make.right.equalTo(self.descView).offset(-26);
    }];
    [self.evaluateButton addSubview:self.gradeLabel];
    [self.gradeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.evaluateButton);
        make.width.equalTo(@(42*kWScale));
        make.height.equalTo(@(21*kWScale));
        make.left.right.equalTo(self.evaluateButton);
    }];
    [self.evaluateButton addSubview:self.evaluateLabel];
    [self.evaluateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.evaluateButton).offset(21);
        make.width.equalTo(@(42*kWScale));
        make.height.equalTo(@(21*kWScale));
        make.left.right.equalTo(self.evaluateButton);
    }];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.iconImageView);//.offset(3);
        make.left.equalTo(self.iconImageView.mas_right).offset(12);
        make.right.equalTo(self.evaluateButton.mas_left).offset(-12);
        make.height.equalTo(@(20*kWScale));
    }];
    
    [self.tagView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.iconImageView.mas_right).offset(12);
        make.right.equalTo(self.evaluateButton.mas_left).offset(-12);
//        make.height.equalTo(@(22*kWScale));
        make.top.equalTo(self.titleLabel.mas_bottom).offset(5);
        make.bottom.equalTo(self.iconImageView.mas_bottom).offset(5);
    }];
    
}

#pragma mark - Lazy Loading
- (SPCycleScrollView *)bannerView {
    if (!_bannerView) {
        _bannerView = [SPCycleScrollView new];
        _bannerView.pageControl.hidden = YES;
        _bannerView.bottomSpace = 0;
        _bannerView.autoScroll = YES;
        _bannerView.duration = 3;
    }
    return _bannerView;
}

- (QJColorGraduateView *)colorView{
    if (!_colorView) {
        _colorView = [QJColorGraduateView new];
        _colorView.userInteractionEnabled = NO;
    }
    return _colorView;
}

- (UIView *)descView{
    if (!_descView) {
        _descView = [UIView new];
    }
    return _descView;
}

- (UIImageView *)iconImageView{
    if (!_iconImageView) {
        _iconImageView = [UIImageView new];
        _iconImageView.layer.cornerRadius = 14;
        _iconImageView.layer.masksToBounds = YES;
        _iconImageView.contentMode = UIViewContentModeScaleAspectFill;
    }
    return _iconImageView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.font = kFont(20);
        _titleLabel.textColor = [UIColor whiteColor];
        _titleLabel.text = @"";
    }
    return _titleLabel;
}

- (FMTagsView *)tagView {
    if (!_tagView) {
        _tagView = [[FMTagsView alloc] init];
        _tagView.tagBackgroundColor = [UIColor colorWithHexString:@"0xfef2cd"];
        _tagView.lineSpacing = 10;
        _tagView.interitemSpacing = 10;
        _tagView.tagHeight = 17;
        _tagView.tagFont = FontRegular(10);
        _tagView.tagSelectedFont = FontRegular(10);
        _tagView.tagcornerRadius = 4;
        _tagView.backgroundColor = [UIColor clearColor];
        _tagView.contentInsets = UIEdgeInsetsZero;
        _tagView.collectionView.scrollEnabled = NO;
        _tagView.allowsSelection = NO;
        _tagView.tagTextColor = [UIColor colorWithHexString:@"0x333333"];
    }
    return _tagView;
}

- (UIButton *)evaluateButton {
    if (!_evaluateButton) {
        _evaluateButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_evaluateButton addTarget:self action:@selector(btnAction) forControlEvents:UIControlEventTouchUpInside];
        _evaluateButton.backgroundColor = UIColorFromRGB(0x1F2A4D);
        _evaluateButton.layer.cornerRadius = 6;
        _evaluateButton.layer.masksToBounds = YES;
    }
    return _evaluateButton;
}

- (QJImageAndLabelView *)gradeLabel {
    if (!_gradeLabel) {
        _gradeLabel = [[QJImageAndLabelView alloc] initWithImage:[UIImage imageNamed:@"qj_productdetail_fire"] withTitle:@"" withFont:kFont(9) withtextColor:[UIColor colorWithHexString:@"0xFFFFFF"] withSpace:3 withImageDirectly:QJImageLeft];
        _gradeLabel.backgroundColor = UIColorFromRGB(0xFF9500);
        _gradeLabel.userInteractionEnabled = NO;
    }
    return _gradeLabel;
}

- (UILabel *)evaluateLabel {
    if (!_evaluateLabel) {
        _evaluateLabel = [UILabel new];
        _evaluateLabel.font = kFont(9);
        _evaluateLabel.textColor = UIColorFromRGB(0xFF9500);
        _evaluateLabel.textAlignment = NSTextAlignmentCenter;
        _evaluateLabel.backgroundColor = [UIColor whiteColor];
        _evaluateLabel.text = @"去评分";
        _evaluateLabel.userInteractionEnabled = NO;
    }
    return _evaluateLabel;
}

- (UIView *)contentView{
    if (!_contentView) {
        _contentView = [UIView new];
        _contentView.backgroundColor = [UIColor whiteColor];
    }
    return _contentView;
}

- (UILabel *)rankLabel {
    if (!_rankLabel) {
        _rankLabel = [UILabel new];
        _rankLabel.font = kFont(14);
        _rankLabel.textColor = UIColorFromRGB(0x919599);
        _rankLabel.textAlignment = NSTextAlignmentCenter;
        _rankLabel.text = @"推荐榜";
    }
    return _rankLabel;
}

- (UILabel *)downLoadLabel {
    if (!_downLoadLabel) {
        _downLoadLabel = [UILabel new];
        _downLoadLabel.font = kFont(14);
        _downLoadLabel.textColor = UIColorFromRGB(0x919599);
        _downLoadLabel.textAlignment = NSTextAlignmentCenter;
        _downLoadLabel.text = @"下载量";
    }
    return _downLoadLabel;
}

- (UILabel *)onLineLabel {
    if (!_onLineLabel) {
        _onLineLabel = [UILabel new];
        _onLineLabel.font = kFont(14);
        _onLineLabel.textColor = UIColorFromRGB(0x919599);
        _onLineLabel.textAlignment = NSTextAlignmentCenter;
        _onLineLabel.text = @"总在线人数";
    }
    return _onLineLabel;
}

- (UILabel *)rankContentLabel {
    if (!_rankContentLabel) {
        _rankContentLabel = [UILabel new];
        _rankContentLabel.font = kboldFont(18);
        _rankContentLabel.textColor = UIColorFromRGB(0x16191C);
        _rankContentLabel.textAlignment = NSTextAlignmentCenter;
        _rankContentLabel.text = @"第1名";
    }
    return _rankContentLabel;
}

- (UILabel *)downLoadContentLabel {
    if (!_downLoadContentLabel) {
        _downLoadContentLabel = [UILabel new];
        _downLoadContentLabel.font = kboldFont(18);
        _downLoadContentLabel.textColor = UIColorFromRGB(0x16191C);
        _downLoadContentLabel.textAlignment = NSTextAlignmentCenter;
        _downLoadContentLabel.text = @"0";
    }
    return _downLoadContentLabel;
}

- (UILabel *)onLineContentLabel {
    if (!_onLineContentLabel) {
        _onLineContentLabel = [UILabel new];
        _onLineContentLabel.font = kboldFont(18);
        _onLineContentLabel.textColor = UIColorFromRGB(0x16191C);
        _onLineContentLabel.textAlignment = NSTextAlignmentCenter;
        _onLineContentLabel.text = @"0";
    }
    return _onLineContentLabel;
}

/**
 * @author: zjr
 * @date: 2022-8-2
 * @desc: button点击事件
 */
- (void)btnAction{
    QJProductEvaluateViewController *evaluateVC = [[QJProductEvaluateViewController alloc] init];
    evaluateVC.detailModel = self.detailModel;
    [[QJAppTool getCurrentViewController].navigationController pushViewController:evaluateVC animated:YES];
}

/**
 * @author: zjr
 * @date: 2022-8-2
 * @desc: 数据源赋值
 */
- (void)setDetailModel:(QJSearchGameDetailModel *)detailModel{
    if (_detailModel != detailModel) {
        _detailModel = detailModel;
        [self setImages:_detailModel.bannerImg];
        self.titleLabel.text = kCheckStringNil(_detailModel.name);
        self.iconImageView.imageURL = [NSURL URLWithString:[_detailModel.icon.length > 0 ?_detailModel.icon:_detailModel.thumbnail checkImageUrlString]];
        self.tagView.tagsModelArray = _detailModel.tags;
        CGFloat height = [self.tagView getHeight];
        if (height < 20) {
            [self.tagView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self.titleLabel.mas_bottom).offset(15);
            }];
        } else {
            [self.tagView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self.titleLabel.mas_bottom).offset(5);
            }];
        }

        [self.gradeLabel setText:_detailModel.point withImageDirectly:QJImageLeft];
        _rankContentLabel.text = [NSString stringWithFormat:@"第%@名",_detailModel.boardRank];
        _downLoadContentLabel.text = [self updateCount:[NSString stringWithFormat:@"%@",_detailModel.startCount]];
        _onLineContentLabel.text = [self updateCount:[NSString stringWithFormat:@"%@",_detailModel.onlineCount]];
    }
}

- (void)setImages:(NSArray *)urls {
    NSMutableArray *muArr = [NSMutableArray array];
    for (NSString *modelStr in _detailModel.bannerImg) {
        if (!IsStrEmpty(modelStr)) {
            NSString *str = [modelStr checkImageUrlString];
            [muArr safeAddObject:str];
        }
    }
    self.bannerView.urlImages = muArr.copy;
}

- (NSString *)updateCount:(NSString *)countStr{
    NSString *countAllStr = countStr;
    NSString *retString = countStr;
    if (countStr.floatValue > 100000) {
        countAllStr = [NSString stringWithFormat:@"%.1f",countStr.floatValue/10000];
        retString = [NSString stringWithFormat:@"%@万+",countAllStr];
    }else{
        retString = [NSString stringWithFormat:@"%@",countAllStr];
    }
    return retString;
}

@end
