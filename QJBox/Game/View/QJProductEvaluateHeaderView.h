//
//  QJProductEvaluateHeaderView.h
//  QJBox
//
//  Created by rui on 2022/7/27.
//

#import <UIKit/UIKit.h>
#import "QJSearchGameListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJProductEvaluateHeaderView : UIView

/* 数据源 */
@property (nonatomic, strong) QJSearchGameDetailModel *detailModel;
/* 更新评价数量 */
- (void)updateCount:(NSString *)countStr;

@end

NS_ASSUME_NONNULL_END
