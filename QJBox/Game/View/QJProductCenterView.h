//
//  QJProductCenterView.h
//  QJBox
//
//  Created by rui on 2022/9/23.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJProductCenterView : UIView

- (void)refreshData;
@property (nonatomic, copy) void(^requestBlock)(void);
@property (nonatomic, copy) void(^scrollviewBlock)(UIScrollView *scrollView);

@end

NS_ASSUME_NONNULL_END
