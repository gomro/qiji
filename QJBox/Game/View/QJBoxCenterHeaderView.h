//
//  QJBoxCenterHeaderView.h
//  QJBox
//
//  Created by rui on 2022/7/22.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class QJHomeBannerModel;

@interface QJBoxCenterHeaderView : UIView

/* banner点击事件回调 */
@property (nonatomic, copy) void(^bannerClick)(QJHomeBannerModel *model);
/* banner滚动事件回调 */
@property (nonatomic, copy) void(^bannerScrollClick)(QJHomeBannerModel *model);
/* 查看我的更多游戏点击事件回调 */
@property (nonatomic, copy) void (^btnClick)(void);
/* banner数据源 */
@property (nonatomic, strong) NSArray *bannerDataArr;
/* 我的游戏数据源 */
@property (nonatomic, strong) NSArray *myProductDataArr;

@end

NS_ASSUME_NONNULL_END
