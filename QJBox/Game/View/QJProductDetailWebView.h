//
//  QJProductDetailWebView.h
//  QJBox
//
//  Created by rui on 2022/10/18.
//

#import <UIKit/UIKit.h>
#import "QJSearchGameListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJProductDetailWebView : UIView

@property (nonatomic, copy) void(^scrollviewBlock)(UIScrollView *scrollView);
@property (nonatomic, strong) QJSearchGameDetailModel *detailModel;

@end

NS_ASSUME_NONNULL_END
