//
//  QJEvaluateStarView.m
//  QJBox
//
//  Created by rui on 2022/7/27.
//

#import "QJEvaluateStarView.h"

@interface QJEvaluateStarView ()

@property (nonatomic, strong) NSMutableArray *starArray;
@property (nonatomic, strong) NSMutableArray *btnArray;

@end

@implementation QJEvaluateStarView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        /* 初始化UI */
        [self initChildView];
    }
    return self;
}

/**
 * @author: zjr
 * @date: 2022-8-2
 * @desc: 加载view/约束
 */
- (void)initChildView {
    self.backgroundColor = [UIColor clearColor];
    
    _starArray = [NSMutableArray array];
    _btnArray = [NSMutableArray array];

    for (int i = 0; i < 5; i ++) {
        UIButton *star = [UIButton buttonWithType:UIButtonTypeCustom];
        star.tag = 1000 + i;
        [star addTarget:self action:@selector(starClick:) forControlEvents:UIControlEventTouchUpInside];
        star.backgroundColor = [UIColor clearColor];
        [self addSubview:star];
        [_btnArray addObject:star];

        UIImageView *picImg = [[UIImageView alloc] init];
        [star addSubview:picImg];
        picImg.image = [UIImage imageNamed:@"qj_evaluate_starnormal"];
        [picImg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.mas_equalTo(star);
            make.size.mas_equalTo(CGSizeMake(self.height, self.height));
        }];
        [_starArray addObject:picImg];
        
    }
    [_btnArray mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedSpacing:0 leadSpacing:0 tailSpacing:0];
    [_btnArray mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@0);
        make.height.mas_equalTo(self.height);
    }];

}

/**
 * @author: zjr
 * @date: 2022-8-2
 * @desc: button点击事件
 */
- (void)starClick:(UIButton *)sender {
    if (self.noAnimate) {
        return;
    }
    NSInteger index = sender.tag - 1000;
    [_starArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        UIImageView *star = obj;
        if (idx <= index) {
            star.image = [UIImage imageNamed:@"qj_evaluate_star"];
            //点击动画
//            [UIView animateKeyframesWithDuration:0.5 delay:0 options:0 animations: ^{
//               [UIView addKeyframeWithRelativeStartTime:0 relativeDuration:1 / 3.0 animations: ^{
//                   star.transform = CGAffineTransformMakeScale(0.3, 0.3);
//               }];
//               [UIView addKeyframeWithRelativeStartTime:1/3.0 relativeDuration:1/3.0 animations: ^{
//                   star.transform = CGAffineTransformMakeScale(1.2, 1.2);
//               }];
//               [UIView addKeyframeWithRelativeStartTime:2/3.0 relativeDuration:1/3.0 animations: ^{
//                   star.transform = CGAffineTransformMakeScale(1.0, 1.0);
//               }];
//       } completion:nil];
        }else{
            star.image = [UIImage imageNamed:@"qj_evaluate_starnormal"];
        }
    }];
    if (self.starClick) {
        self.starClick(index);
    }
}

/**
 * @author: zjr
 * @date: 2022-8-2
 * @desc: 更新数量
 */
- (void)updateWithIndex:(NSInteger)index {
    if (index < 0) {
        index = 0;
    }
    if (index > _starArray.count - 1) {
        index = _starArray.count - 1;
    }
    
    [_starArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        UIImageView *star = obj;
        if (idx <= index) {
            star.image = [UIImage imageNamed:@"qj_evaluate_star"];
        }else{
            star.image = [UIImage imageNamed:@"qj_evaluate_starnormal"];
        }
    }];
}


@end
