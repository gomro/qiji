//
//  QJBoxCenterHeaderView.m
//  QJBox
//
//  Created by rui on 2022/7/22.
//

#import "QJBoxCenterHeaderView.h"
#import "QJBoxCommonCollectionCell.h"
#import "QJProductDetailViewController.h"

#import "SPCycleScrollView.h"
#import "QJHomeBannerModel.h"

@interface QJBoxCenterHeaderView()<SPCycleScrollViewDelegate>

/* 轮播 */
@property (nonatomic, strong) SPCycleScrollView *bannerView;

@end

@implementation QJBoxCenterHeaderView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        /* 初始化UI */
        [self initChildView];
    }
    return self;
}

/**
 * @author: zjr
 * @date: 2022-8-2
 * @desc: 加载view/约束
 */
- (void)initChildView {
    [self addSubview:self.bannerView];
    [self.bannerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(200*kWScale);
        make.top.equalTo(self).offset(8);
        make.left.equalTo(self).offset(10);
        make.right.equalTo(self).offset(-10);
    }];
}

#pragma mark - Lazy Loading
- (SPCycleScrollView *)bannerView {
    if (!_bannerView) {
        _bannerView = [SPCycleScrollView new];
        _bannerView.bottomSpace = 0;
        _bannerView.pageControl.hidden = YES;
        _bannerView.duration = 3;
        _bannerView.formType = @"productCenter";
        _bannerView.layer.cornerRadius = 8;
        _bannerView.layer.masksToBounds = YES;
        _bannerView.delegate = self;
    }
    return _bannerView;
}

/**
 * @author: zjr
 * @date: 2022-8-2
 * @desc: 数据源赋值
 */
- (void)setBannerDataArr:(NSArray *)bannerDataArr{
    if (_bannerDataArr != bannerDataArr) {
        _bannerDataArr = bannerDataArr;
        [self setImages:self.bannerDataArr];
    }
}

- (void)setImages:(NSArray *)urls {
    NSMutableArray *muArr = [NSMutableArray array];
    for (QJHomeBannerModel *model in self.bannerDataArr) {
        if (!IsStrEmpty(model.imgUrl)) {
            NSString *str = [model.imgUrl checkImageUrlString];
            [muArr safeAddObject:str];
        }
    }
    self.bannerView.urlImages = muArr.copy;

    WS(weakSelf)
    self.bannerView.clickedImageBlock = ^(NSUInteger index) {
        if (weakSelf.bannerClick) {
            weakSelf.bannerClick([weakSelf.bannerDataArr safeObjectAtIndex:index]);
        }
    };
}

/** 图片滚动回调 */
- (void)cycleScrollView:(SPCycleScrollView *)cycleScrollView didScrollToIndex:(NSInteger)index{
    if (self.bannerScrollClick) {
        self.bannerScrollClick([self.bannerDataArr safeObjectAtIndex:index]);
    }
}

@end
