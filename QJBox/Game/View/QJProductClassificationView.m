//
//  QJProductClassificationView.m
//  QJBox
//
//  Created by wxy on 2022/7/25.
//

#import "QJProductClassificationView.h"
#import "QJProductClassificationProductCell.h"
#import "QJSearchGamePostRequest.h"
#import "QJProductClassificationModel.h"
#import "QJSearchGameListModel.h"
#import "QJProductCommentListEmptyView.h"
@interface QJProductClassificationView ()<UITableViewDelegate,UITableViewDataSource>


@property (nonatomic, strong) UITableView *tableView;


@property (nonatomic, strong) QJSearchGamePostRequest *queryListReq;

@property (nonatomic, assign) NSInteger page;

@property (nonatomic, strong) QJSearchGameListModel *listModel;//游戏列表数据源

@property (nonatomic, strong) NSArray *dataArray;//数据源
@end


@implementation QJProductClassificationView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor whiteColor];
        self.isNeedReload = YES;
        self.page = 1;
        [self addSubview:self.tableView];
        [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.top.left.right.equalTo(self);
                    make.bottom.equalTo(self).offset(-Bottom_iPhoneX_SPACE);
        }];
    }
    return self;
}


- (void)layoutSubviews {
    [super layoutSubviews];
    self.layer.cornerRadius = 4;
    self.layer.masksToBounds = YES;
}

- (void)refreshData {
    if (self.page == 1) {
        self.dataArray = @[];
    }
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:@(self.page) forKey:@"page"];
    [dic setValue:@(20) forKey:@"size"];
    NSMutableDictionary *optionDic = [NSMutableDictionary dictionary];
    [optionDic setValue:self.model.values.modelToJSONObject?:@[] forKey:self.model.idStr?:@""];
    [dic setValue:optionDic forKey:@"option"];
    self.queryListReq.dic = dic;
    WS(weakSelf)
    self.queryListReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        UIViewController *vc = [QJAppTool getCurrentViewController];
        [vc.view hideHUDIndicator];
        if (ResponseSuccess) {
            [weakSelf.tableView.mj_header endRefreshing];
            [weakSelf.tableView.mj_footer endRefreshing];
            weakSelf.listModel = [QJSearchGameListModel modelWithDictionary:EncodeDicFromDic(request.responseObject, @"data")];
            NSMutableArray *muArr = [NSMutableArray arrayWithArray:weakSelf.dataArray];
            [muArr addObjectsFromArray:weakSelf.listModel.records];
            weakSelf.dataArray = muArr.copy;
 
            if (weakSelf.listModel.hasNext && self.page == 1) {

                QJRefreshFooter *footer = [QJRefreshFooter footerWithRefreshingTarget:weakSelf refreshingAction:@selector(loadMoreData)];
              
                weakSelf.tableView.mj_footer = footer;
                
            }
            if ( weakSelf.listModel.records.count < 20 && self.page > 1) {
                [weakSelf.tableView.mj_footer endRefreshingWithNoMoreData];
            }
            [weakSelf.tableView reloadData];
        }
        QJEmptyCommonView *emptyView = [QJEmptyCommonView new];
        emptyView.string = @"还没有任何游戏嗷～";
        [weakSelf.tableView tableViewDisplayWhenHaveNoDataWithView:[QJEmptyCommonView new] ifNecessaryForRowCount:weakSelf.dataArray.count];

    };
    
    self.queryListReq.failureCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        UIViewController *vc = [QJAppTool getCurrentViewController];
        [vc.view hideHUDIndicator];
        [weakSelf.tableView.mj_footer endRefreshing];
        [weakSelf.tableView.mj_header endRefreshing];
        QJEmptyCommonView *emptyView = [QJEmptyCommonView new];
        emptyView.string = @"还没有任何游戏嗷～";
        [weakSelf.tableView tableViewDisplayWhenHaveNoDataWithView:[QJEmptyCommonView new] ifNecessaryForRowCount:weakSelf.dataArray.count];

    };
    [self.queryListReq getSearchGameRequest];
     
}

- (void)loadMoreData {
    self.page++;
    [self refreshData];
}

- (void)setModel:(QJProductClassificationFilterOptionsModel *)model {
    _model = model;
    UIViewController *vc = [QJAppTool getCurrentViewController];
    [vc.view showHUDIndicator];
    [self refreshData];
}

#pragma mark  ------ getter
- (NSArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [NSArray array];
    }
    return _dataArray;
}
- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        [_tableView registerClass:[QJProductClassificationProductCell class] forCellReuseIdentifier:@"QJProductClassificationProductCell"];
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.mj_header = [QJRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(refreshData)];
        _tableView.mj_footer = [QJRefreshFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _tableView;
}

-(QJSearchGamePostRequest *)queryListReq {
    if (!_queryListReq) {
        _queryListReq = [QJSearchGamePostRequest new];
        
    }
    return _queryListReq;
}

#pragma mark ------- UITableViewDelegate, UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
 
    return self.dataArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
   
    return 75*kWScale;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    QJProductClassificationProductCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QJProductClassificationProductCell" forIndexPath:indexPath];
    QJSearchGameDetailModel *model = [self.dataArray safeObjectAtIndex:indexPath.row];
    cell.model = model;
 
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    if (self.cellClick) {
        self.cellClick([self.dataArray safeObjectAtIndex:indexPath.row]);
    }
}



@end
