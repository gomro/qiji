//
//  QJProductClassificationAllView.m
//  QJBox
//
//  Created by wxy on 2022/7/25.
//

#import "QJProductClassificationAllView.h"

#import "QJProductClassificationCollectionViewCell.h"
#import "QJProductClassificationModel.h"
#import "QJProductCommentListEmptyView.h"
@interface QJProductClassificationAllView ()<UICollectionViewDelegate,UICollectionViewDataSource>

@property (nonatomic, strong) UIImageView *topView;

@property (nonatomic, strong) UIView *whiteView;

@property (nonatomic, strong) UICollectionView *collectionView;

@property (nonatomic, strong) UILabel *titleLabel;//全部分类


@end



@implementation QJProductClassificationAllView

 
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = kColorWithHexString(@"#F5F5F5");
        [self addSubview:self.topView];
        [self addSubview:self.whiteView];
        [self.whiteView addSubview:self.titleLabel];
        [self.whiteView addSubview:self.collectionView];
        
        [self.topView mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.right.top.equalTo(self);
                    make.height.equalTo(@(Get375Width(67)));
        }];
        
        [self.whiteView mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.right.equalTo(self);
            make.bottom.equalTo(self).offset(10);
            make.top.equalTo(self.topView.mas_bottom).offset(Get375Width(12));
        }];
        
        [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.top.equalTo(self.whiteView).offset(Get375Width(14));
                    make.width.equalTo(@90);
                    make.height.equalTo(@(Get375Width(16)));
        }];
        
        [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.top.equalTo(self.titleLabel.mas_bottom).offset(Get375Width(8));
                    make.left.equalTo(self.whiteView);
                    make.right.equalTo(self.whiteView);
                    make.bottom.equalTo(self.whiteView).offset(-Bottom_iPhoneX_SPACE);
        }];
    }
    return self;
}


- (void)layoutSubviews {
    [super layoutSubviews];
    self.whiteView.layer.cornerRadius = 4;
    
}


#pragma mark  ---- getter

- (UIImageView *)topView {
    if (!_topView) {
        _topView = [UIImageView new];
        _topView.layer.cornerRadius = 4;
        _topView.layer.masksToBounds = YES;
    }
    return _topView;
}


- (UIView *)whiteView {
    if (!_whiteView) {
        _whiteView = [UIView new];
        _whiteView.backgroundColor = [UIColor whiteColor];
    }
    return _whiteView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.font = FontSemibold(14);
        _titleLabel.textColor = kColorWithHexString(@"#000000");
        _titleLabel.textAlignment = NSTextAlignmentLeft;
        _titleLabel.text = @"全部分类";
    }
    return _titleLabel;
}

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
        
        layout.minimumLineSpacing = Get375Width(10);
        layout.minimumInteritemSpacing = Get375Width(26);
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        [_collectionView registerClass:[QJProductClassificationCollectionViewCell class] forCellWithReuseIdentifier:@"QJProductClassificationCollectionViewCell"];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.showsHorizontalScrollIndicator = NO;
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.contentInset = UIEdgeInsetsMake(0, 12.5, 0, 12.5);
        
        
    }
    return _collectionView;
}

 
- (void)setModel:(QJProductClassificationModel *)model {
    _model = model;
    self.topView.imageURL = [NSURL URLWithString:[model.appSystemImg.imgUrl checkImageUrlString]];
    [self.collectionView reloadData];
}

#pragma mark ---  UICollectionViewDelegate,UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    QJEmptyCommonView *emptyView = [QJEmptyCommonView new];
    emptyView.string = @"还没有任何游戏嗷～";
    emptyView.topSpace = 100;
    [collectionView collectionViewDisplayWhenHaveNoDataWithView:emptyView ifNecessaryForRowCount:self.model.allTagsOption.count];

    return self.model.allTagsOption.count;
}


- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    QJProductClassificationCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"QJProductClassificationCollectionViewCell" forIndexPath:indexPath];
    cell.model = [self.model.allTagsOption safeObjectAtIndex:indexPath.item];
    return cell;
    
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
     
    if (self.clickCell) {
        NSInteger index = indexPath.item;
        self.clickCell([self.model.allTagsOption safeObjectAtIndex:index]);
    }
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    
    return CGSizeMake(Get375Width(62), Get375Width(84));;
    
}


@end
