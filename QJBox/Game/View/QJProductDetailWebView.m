//
//  QJProductDetailWebView.m
//  QJBox
//
//  Created by rui on 2022/10/18.
//

#import "QJProductDetailWebView.h"
#import "QJProductIntroWebCell.h"
#import "QJProductTitleContentCell.h"

#import "GKPageScrollView.h"

@interface QJProductDetailWebView()<GKPageListViewDelegate,UITableViewDelegate,UITableViewDataSource,QJProductIntroWebCellDelegate,QJProductTitleContentWebCellDelegate>

/* table */
@property (nonatomic, strong) UITableView *leftTableView;
@property (nonatomic, assign) CGFloat webCellHeight;
@property (nonatomic, assign) CGFloat webContentCellHeight;

/* GKScroll回调 */
@property (nonatomic, copy) void(^listScrollViewScrollBlock)(UIScrollView *scrollView);

@end

@implementation QJProductDetailWebView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        /* 初始化View */
        [self initChildView];
    }
    return self;
}

/**
 * @author: zjr
 * @date: 2022-8-2
 * @desc: 加载view/约束
 */
- (void)initChildView {
    self.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.leftTableView];
    [self.leftTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
}

#pragma mark --Lazy Loading
- (UITableView *)leftTableView {
    if (!_leftTableView) {
        _leftTableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _leftTableView.dataSource = self;
        _leftTableView.delegate = self;
        [_leftTableView registerClass:[QJProductIntroWebCell class] forCellReuseIdentifier:@"QJProductIntroWebCell"];
        [_leftTableView registerClass:[QJProductTitleContentCell class] forCellReuseIdentifier:@"QJProductTitleContentCell"];
        _leftTableView.backgroundColor = [UIColor whiteColor];
        _leftTableView.estimatedRowHeight = 100;
        _leftTableView.rowHeight = UITableViewAutomaticDimension;
        _leftTableView.showsVerticalScrollIndicator = NO;
        //        _leftTableView.mj_footer = [QJRefreshFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreHomeData)];
        if (@available(iOS 11.0, *)) {
            _leftTableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
            _leftTableView.estimatedRowHeight = 0;
            _leftTableView.estimatedSectionFooterHeight = 0;
            _leftTableView.estimatedSectionHeaderHeight = 0;
        }
    }
    return _leftTableView;
}

#pragma mark ------- UITableViewDelegate, UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([kCheckStringNil(self.detailModel.gameInfo) isEqualToString:@""]) {
        QJEmptyCommonView *emptyView = [QJEmptyCommonView new];
        emptyView.string = @"这里什么也没有～";
        emptyView.emptyImage = @"empty_no_data";
        emptyView.topSpace = 100;
        [tableView tableViewDisplayWhenHaveNoDataWithView:emptyView ifNecessaryForRowCount:0];
        return 0;
    }else{
        QJEmptyCommonView *emptyView = [QJEmptyCommonView new];
        emptyView.string = @"这里什么也没有～";
        emptyView.emptyImage = @"empty_no_data";
        emptyView.topSpace = 100;
        [tableView tableViewDisplayWhenHaveNoDataWithView:emptyView ifNecessaryForRowCount:1];
        return 1;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return _webContentCellHeight;
    }else{
        return _webCellHeight;
    }
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return _webContentCellHeight;
    }else{
        return _webCellHeight;
    }
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 40;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.001;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 0.001)];
    return footerView;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (tableView == self.leftTableView) {
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, QJScreenWidth, 40)];
        UILabel *titleLabel = [UILabel new];
        titleLabel.font = FontRegular(16);
        titleLabel.textColor = UIColor.blackColor;
        [headerView addSubview:titleLabel];
        [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(headerView).offset(10);
            make.left.equalTo(headerView).offset(16);
            make.right.equalTo(headerView).offset(-16);
            make.height.equalTo(@30);
        }];
        if (section == 0){
            titleLabel.text = @"游戏简介";
        }else{
            titleLabel.text = @"游戏内容";
        }
        return headerView;
    }

    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        QJProductTitleContentCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QJProductTitleContentCell" forIndexPath:indexPath];
        cell.detailModel = self.detailModel;
        cell.delegate = self;
        return cell;
    }else{
        QJProductIntroWebCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QJProductIntroWebCell" forIndexPath:indexPath];
        cell.detailModel = self.detailModel;
        cell.delegate = self;
        return cell;
    }
}

#pragma mark  -----QJProductIntroWebCellDelegate
- (void)changeHeightWith:(CGFloat)height{
    _webCellHeight = height;
    [_leftTableView reloadData];
}

- (void)changeContentHeightWith:(CGFloat)height{
    _webContentCellHeight = height;
    [_leftTableView reloadData];
}

#pragma mark - GKPageListViewDelegate
- (UIScrollView *)listScrollView {
    return self.leftTableView;
}

- (void)listViewDidScrollCallback:(void (^)(UIScrollView * _Nonnull))callback {
    self.listScrollViewScrollBlock = callback;
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    !self.listScrollViewScrollBlock ? : self.listScrollViewScrollBlock(scrollView);
    if (self.scrollviewBlock) {
        self.scrollviewBlock(scrollView);
    }
}

@end
