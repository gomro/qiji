//
//  QJProductCommentListView.h
//  QJBox
//
//  Created by rui on 2022/7/21.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJProductCommentListView : UIView

/* 产品ID */
@property (nonatomic, copy) NSString *productID;
/* 类型区分 starSort-默认 timeSort-最新 myPublish-我的 */
@property (nonatomic, copy) NSString *type;
/* 请求 */
- (void)refreshFirst;
/* 请求成功回调返回总评论数 */
@property (nonatomic, copy) void(^requestSuccessClick)(NSString *countStr);

@end

NS_ASSUME_NONNULL_END
