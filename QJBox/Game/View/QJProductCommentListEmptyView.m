//
//  QJProductCommentListEmptyView.m
//  QJBox
//
//  Created by wxy on 2022/9/22.
//

#import "QJProductCommentListEmptyView.h"

@interface QJProductCommentListEmptyView ()

@property (nonatomic, strong) UIImageView *topImageView;

@property (nonatomic, strong) UILabel *titleLabel;

@end



@implementation QJProductCommentListEmptyView


- (instancetype)initWithFrame:(CGRect)frame {
    if(self = [super initWithFrame:frame]) {
        [self addSubview:self.topImageView];
        [self addSubview:self.titleLabel];
        [self.topImageView mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.centerX.equalTo(self);
                    make.width.equalTo(@184);
                    make.height.equalTo(@107);
                    make.top.equalTo(self).offset(94);
        }];
        [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.centerX.equalTo(self);
                    make.top.equalTo(self.topImageView.mas_bottom).offset(0);
                    make.left.right.equalTo(self);
        }];
        
    }
    return self;
}




#pragma mark ---------- getter && setter

- (UIImageView *)topImageView {
    if(!_topImageView) {
        _topImageView = [UIImageView new];
        _topImageView.image = [UIImage imageNamed:@"icon_empty_search"];
        
    }
    return _topImageView;
}


- (UILabel *)titleLabel {
    if(!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.font = FontRegular(14);
        _titleLabel.textColor = kColorWithHexString(@"#919599");
        _titleLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _titleLabel;
}

-(void)setBgColor:(UIColor *)bgColor {
    _bgColor = bgColor;
    self.backgroundColor = bgColor;
}

- (void)setString:(NSString *)string {
    _string = string;
    self.titleLabel.text = string;
}


 



@end
