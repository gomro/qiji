//
//  QJEvaluateStarView.h
//  QJBox
//
//  Created by rui on 2022/7/27.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJEvaluateStarView : UIView

/* 是否需要点击事件 */
@property (nonatomic, assign) BOOL noAnimate;
/* 点击事件 */
@property (nonatomic, copy) void(^starClick)(NSInteger index);
/* 更新星星数量 */
- (void)updateWithIndex:(NSInteger)index;

@end

NS_ASSUME_NONNULL_END
