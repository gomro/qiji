//
//  QJProductClassificationView.h
//  QJBox
//
//  Created by wxy on 2022/7/25.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class QJProductClassificationModel,QJProductClassificationFilterOptionsModel,QJSearchGameDetailModel;
@interface QJProductClassificationView : UIView


@property (nonatomic, strong) QJProductClassificationFilterOptionsModel *model;

@property (nonatomic, copy) void(^cellClick)(QJSearchGameDetailModel *model);

@property (nonatomic, copy) void(^cellBtnClick)(QJSearchGameDetailModel *model);

@property (nonatomic, assign) BOOL isNeedReload;


 

@end

NS_ASSUME_NONNULL_END
