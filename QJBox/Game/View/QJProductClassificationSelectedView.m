//
//  QJProductClassificationSelectedView.m
//  QJBox
//
//  Created by wxy on 2022/7/25.
//

#import "QJProductClassificationSelectedView.h"


@interface QJProductClassificationSelectedView ()




@property (nonatomic, strong) UIView *bottomView;





@end


@implementation QJProductClassificationSelectedView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self addSubview:self.topView];
        [self addSubview:self.bottomView];
        self.clipsToBounds = YES;
        self.backgroundColor = kColorWithHexString(@"#F5F5F5");
        [self.topView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.equalTo(@43);
            make.width.equalTo(@134);
            make.bottom.equalTo(self.mas_top).offset(10);
            make.right.equalTo(self);
        }];
        
        [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.equalTo(@43);
            make.width.equalTo(@134);
            make.right.equalTo(self);
            make.top.equalTo(self.mas_bottom).offset(-10);
        }];
        
    }
    return self;
}

-(void)layoutSubviews {
    [super layoutSubviews];
    [self.topView showCorner:8 rectCorner:UIRectCornerBottomLeft | UIRectCornerBottomRight ];
    [self.bottomView showCorner:8 rectCorner:UIRectCornerTopLeft | UIRectCornerTopRight ];
}


#pragma mark ------ getter

- (UIView *)topView {
    if (!_topView) {
        _topView = [UIView new];
        _topView.backgroundColor = [UIColor whiteColor];
    }
    return _topView;
}


- (UIView *)bottomView {
    if (!_bottomView) {
        _bottomView = [UIView new];
        _bottomView.backgroundColor = [UIColor whiteColor];
    }
    return _bottomView;
}



@end
