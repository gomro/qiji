//
//  QJProductClassificationSearchResultView.m
//  QJBox
//
//  Created by wxy on 2022/7/27.
//

#import "QJProductClassificationSearchResultView.h"
#import "QJProductClassificationModel.h"
#import "QJHomeMainCell.h"
#import "QJSegmentLineView.h"
#import "QJSearchGamePostRequest.h"
#import "QJSearchGameListModel.h"
@interface QJProductClassificationSearchResultView ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) QJSegmentLineView *segmentView;

@property (nonatomic, strong) QJSearchGamePostRequest *queryListReq;//获取搜索结果

@property (nonatomic, strong) NSMutableArray *dataArr;

@property (nonatomic, assign) NSInteger page;

@property (nonatomic, copy) NSString *orderID;

@property (nonatomic, strong) QJSearchGameListModel *listModel;//游戏列表数据源

@property (nonatomic, strong) NSArray *dataArray;//数据源

@property (nonatomic, copy) NSString *keyWords;

@end

@implementation QJProductClassificationSearchResultView

 
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.page = 1;
        [self addSubview:self.tableView];
        
        [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.right.equalTo(self);
                    make.bottom.equalTo(self);
                    make.top.equalTo(self).offset(34);
        }];
        
        
    }
    return self;
}

- (void)searchDataWithKeyWords:(NSString *)keyWords {
    self.keyWords = keyWords;
    QJProductClassificationOrderOptionModel *orderModel = [self.model.orderOptions safeObjectAtIndex:0];
    self.orderID = orderModel.idStr;
    self.page = 1;
    [self refreshData];
}

//刷新
- (void)refreshData {
    if (self.page == 1) {
        self.dataArray = @[];
    }
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:@(self.page) forKey:@"page"];
    [dic setValue:@20 forKey:@"size"];
    NSMutableDictionary *optionDic = [NSMutableDictionary dictionary];
    [optionDic setValue:@[self.tagOptionModel.idStr?:@""] forKey:@"1"];
    [dic setValue:optionDic forKey:@"option"];
    [dic setValue:self.orderID?:@"" forKey:@"order"];
    [dic setValue:self.keyWords?:@"" forKey:@"keyword"];
    
    WS(weakSelf)
    self.queryListReq.dic = dic.copy;
    self.queryListReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            [weakSelf.tableView.mj_header endRefreshing];
            [weakSelf.tableView.mj_footer endRefreshing];
            weakSelf.listModel = [QJSearchGameListModel modelWithDictionary:EncodeDicFromDic(request.responseObject, @"data")];
            NSMutableArray *muArr = [NSMutableArray arrayWithArray:weakSelf.dataArray];
            [muArr addObjectsFromArray:weakSelf.listModel.records];
            weakSelf.dataArray = muArr.copy;
            if (weakSelf.listModel.hasNext && self.page == 1) {
                QJRefreshFooter *footer = [QJRefreshFooter footerWithRefreshingTarget:weakSelf refreshingAction:@selector(loadMoreData)];
                [footer setTitle:@"已经到底啦~" forState:MJRefreshStateNoMoreData];
                footer.stateLabel.textColor = kColorWithHexString(@"#919599");
                weakSelf.tableView.mj_footer = footer;
                
                
            }
            if ( weakSelf.listModel.records.count < 20 && self.page > 1) {
                [weakSelf.tableView.mj_footer endRefreshingWithNoMoreData];
            }
            [weakSelf.tableView reloadData];
        }
        QJEmptyCommonView *emptyView = [QJEmptyCommonView new];
        emptyView.string = @"还没有任何游戏嗷～";
        [weakSelf.tableView tableViewDisplayWhenHaveNoDataWithView:[QJEmptyCommonView new] ifNecessaryForRowCount:weakSelf.dataArray.count];

    };
    
    self.queryListReq.failureCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        [weakSelf.tableView.mj_footer endRefreshing];
        [weakSelf.tableView.mj_header endRefreshing];
        QJEmptyCommonView *emptyView = [QJEmptyCommonView new];
        emptyView.string = @"还没有任何游戏嗷～";
        [weakSelf.tableView tableViewDisplayWhenHaveNoDataWithView:[QJEmptyCommonView new] ifNecessaryForRowCount:weakSelf.dataArray.count];

    };
    [self.queryListReq getSearchGameRequest];
}

- (void)loadMoreData {
    self.page++;
    [self refreshData];
}

#pragma mark ----- getter

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        [_tableView registerClass:[QJHomeMainCell class] forCellReuseIdentifier:@"QJHomeMainCell"];
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.contentInset = UIEdgeInsetsMake(13, 0, 0, 0);
        QJRefreshHeader  *header =  [QJRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(refreshData)];
        header.ignoredScrollViewContentInsetTop = 13;
        _tableView.mj_header = header;
        
    }
    return _tableView;
}


- (void)setModel:(QJProductClassificationModel *)model {
    _model = model;
    if (model.orderOptions.count > 0) {
        NSMutableArray *titles = [NSMutableArray array];
        for (QJProductClassificationOrderOptionModel *dto in self.model.orderOptions) {
            [titles safeAddObject:dto.name];
        }
        self.segmentView.frame = CGRectMake(0, 0, kScreenWidth, 34);
        [self.segmentView setTitleArray:titles.copy];
         
        [self addSubview:self.segmentView];
        WS(weakSelf)
        self.segmentView.segmentedItemSelectedBlock = ^(QJSegmentLineView * _Nonnull segment, NSInteger selectedIndex) {
            QJProductClassificationOrderOptionModel *model = [weakSelf.model.orderOptions safeObjectAtIndex:selectedIndex];
            DLog(@"搜索id = %@",model.idStr);
            weakSelf.orderID = model.idStr;
            weakSelf.page = 1;
            [weakSelf refreshData];
        };
        [self.segmentView mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.top.left.right.equalTo(self);
                    make.height.equalTo(@34);
                     
        }];
        
        
    }
    QJProductClassificationOrderOptionModel *orderModel = [self.model.orderOptions safeObjectAtIndex:0];
    self.orderID = orderModel.idStr;
    self.page = 1;
   
    
    
}

- (QJSegmentLineView *)segmentView {
    if (!_segmentView) {
        _segmentView = [QJSegmentLineView new];
        _segmentView.backgroundColor = [UIColor whiteColor];
        _segmentView.font = FontRegular(14);
        _segmentView.selectedFont = FontSemibold(16);
        _segmentView.textColor =  kColorWithHexString(@"#919599");
        _segmentView.selectedTextColor =  kColorWithHexString(@"#FF9500");
        _segmentView.showBottomLine = NO;
        _segmentView.itemSpace = 16;
        _segmentView.leftOffset = 16;
    }
    return _segmentView;
}

- (QJSearchGamePostRequest *)queryListReq {
    if (!_queryListReq) {
        _queryListReq = [QJSearchGamePostRequest new];
    }
    return _queryListReq;
}

#pragma mark ------- UITableViewDelegate, UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.dataArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    QJHomeMainCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QJHomeMainCell" forIndexPath:indexPath];
    QJSearchGameDetailModel *model = [self.dataArray safeObjectAtIndex:indexPath.row];
  
    cell.model = model;
  
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    QJSearchGameDetailModel *model = [self.dataArray safeObjectAtIndex:indexPath.row];
    if (self.didCellBlock) {
        self.didCellBlock(model);
    }
  
}



@end
