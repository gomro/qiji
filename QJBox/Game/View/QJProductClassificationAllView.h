//
//  QJProductClassificationAllView.h
//  QJBox
//
//  Created by wxy on 2022/7/25.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class QJProductClassificationModel,QJProductClassificationAllTagsOptionModel;
/// 全部分类
@interface QJProductClassificationAllView : UIView

@property (nonatomic, copy) void(^clickCell)(QJProductClassificationAllTagsOptionModel *model);

@property (nonatomic, strong) QJProductClassificationModel *model;

 
@end

NS_ASSUME_NONNULL_END
