//
//  QJEvaluateCustomStarVIew.m
//  QJBox
//
//  Created by rui on 2022/7/28.
//

#import "QJEvaluateCustomStarVIew.h"

#define ForegroundStarImage @"qj_evaluate_star"
#define BackgroundStarImage @"qj_evaluate_starnormal"
#define StartWidth 20.

typedef void(^completeBlock)(CGFloat currentScore);

@interface QJEvaluateCustomStarVIew()

/* 选中背景视图 */
@property (nonatomic, strong) UIView *foregroundStarView;
/* 默认背景视图 */
@property (nonatomic, strong) UIView *backgroundStarView;
/* 默认星星数量-5个 */
@property (nonatomic, assign) NSInteger numberOfStars;
/* block回调 */
@property (nonatomic, strong) completeBlock complete;
/* 星星的大小 */
@property (nonatomic, assign) CGSize starSize;

@end

@implementation QJEvaluateCustomStarVIew

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}

#pragma mark - 初始化方法
-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        _numberOfStars = 5;
        _starSize = CGSizeMake(StartWidth, StartWidth);
        _starSpaceing = 10;
        _rateStyle = QJStarWholeStar;
        [self createStarView];
    }
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame numberOfStars:(NSInteger)numberOfStars rateStyle:(QJStarRateStyle)rateStyle isAnination:(BOOL)isAnimation delegate:(id)delegate{
    if (self = [super initWithFrame:frame]) {
        _numberOfStars = numberOfStars;
        _starSize = CGSizeMake(StartWidth, StartWidth);
        _starSpaceing = 10;
        _rateStyle = rateStyle;
        _isAnimation = isAnimation;
        _delegate = delegate;
        [self createStarView];
    }
    return self;
}

#pragma mark - block方式
-(instancetype)initWithFrame:(CGRect)frame finish:(finishBlock)finish{
    if (self = [super initWithFrame:frame]) {
        _numberOfStars = 5;
        _starSize = CGSizeMake(StartWidth, StartWidth);
        _starSpaceing = 10;
        _rateStyle = QJStarWholeStar;
        _complete = ^(CGFloat currentScore){
            finish(currentScore);
        };
        [self createStarView];
    }
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame numberOfStars:(NSInteger)numberOfStars rateStyle:(QJStarRateStyle)rateStyle isAnination:(BOOL)isAnimation finish:(finishBlock)finish{
    if (self = [super initWithFrame:frame]) {
        _numberOfStars = numberOfStars;
        _starSize = CGSizeMake(StartWidth, StartWidth);
        _starSpaceing = 10;
        _rateStyle = rateStyle;
        _isAnimation = isAnimation;
        _complete = ^(CGFloat currentScore){
            finish(currentScore);
        };
        [self createStarView];
    }
    return self;
}

#pragma mark - private Method
-(void)createStarView{ 
    self.backgroundColor = [UIColor whiteColor];
    if ([self.subviews containsObject:self.foregroundStarView]) {
        [self.foregroundStarView removeFromSuperview];
    }
    
    if ([self.subviews containsObject:self.backgroundStarView]) {
        [self.backgroundStarView removeFromSuperview];
    }
    
    self.foregroundStarView = [self createStarViewWithImage:ForegroundStarImage];
    self.backgroundStarView = [self createStarViewWithImage:BackgroundStarImage];
    
    CGFloat foregroundStarViewWidth = 0.;
    if (_currentScore > 0) {
        foregroundStarViewWidth = _currentScore * StartWidth + (ceilf(_currentScore) - 1) * _starSpaceing;
    } else {
        foregroundStarViewWidth = 0;
    }
    
    self.foregroundStarView.frame = CGRectMake(0, 0, foregroundStarViewWidth, self.bounds.size.height);
    
    [self addSubview:self.backgroundStarView];
    [self addSubview:self.foregroundStarView];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userTapRateView:)];
    tapGesture.numberOfTapsRequired = 1;
    [self addGestureRecognizer:tapGesture];
    
}

/**
 * @author: zjr
 * @date: 2022-8-2
 * @desc: 创建image view
 */
- (UIView *)createStarViewWithImage:(NSString *)imageName {
    UIView *view = [[UIView alloc] initWithFrame:self.bounds];
    view.clipsToBounds = YES;
    view.backgroundColor = [UIColor clearColor];
    for (NSInteger i = 0; i < self.numberOfStars; i ++)
    {
        UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageName]];
        imageView.frame = CGRectMake(i * (StartWidth + _starSpaceing), 0, StartWidth, self.bounds.size.height);
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        [view addSubview:imageView];
    }
    return view;
}

/**
 * @author: zjr
 * @date: 2022-8-2
 * @desc: UITapGestureRecognizer点击事件
 */
- (void)userTapRateView:(UITapGestureRecognizer *)gesture {
    CGPoint tapPoint = [gesture locationInView:self];
    CGFloat offset = tapPoint.x;
    //    CGFloat realStarScore = offset / (((StartWidth + _starSpaceing) * self.numberOfStars - _starSpaceing) / self.numberOfStars);
    
    //一个星+一个间距为单位 向下取整
    CGFloat floorStarScore = floorf(offset / (StartWidth + _starSpaceing));
    
    //多于一个完整星（星宽+间距）外的宽度
    CGFloat excessWidth = offset - floorStarScore * (StartWidth + _starSpaceing);
    
    //真实星数目
    CGFloat realStarScore = (offset - floorStarScore * _starSpaceing) / StartWidth;
    ;
    
    if (excessWidth > StartWidth) { //多的是间距。向下取整
        realStarScore = floorf(realStarScore);
    }
    
    switch (_rateStyle) {
        case QJStarWholeStar:
        {
            self.currentScore = ceilf(realStarScore);
            break;
        }
        case QJStarHalfStar:
            self.currentScore = roundf(realStarScore)>=realStarScore ? ceilf(realStarScore):(ceilf(realStarScore)-0.5);
            break;
        case QJStarIncompleteStar:
            self.currentScore = realStarScore;
            break;
        default:
            break;
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];
    __weak QJEvaluateCustomStarVIew *weakSelf = self;
    CGFloat animationTimeInterval = self.isAnimation ? 0.2 : 0;
    [UIView animateWithDuration:animationTimeInterval animations:^{
        
        CGFloat foregroundStarViewWidth = 0.;
        if (self->_currentScore > 0) {
            foregroundStarViewWidth = self->_currentScore * StartWidth + (ceilf(self->_currentScore) - 1) * self->_starSpaceing;
        } else {
            foregroundStarViewWidth = 0;
        }
        weakSelf.foregroundStarView.frame = CGRectMake(0, 0, foregroundStarViewWidth, weakSelf.bounds.size.height);
    }];
}

/**
 * @author: zjr
 * @date: 2022-8-2
 * @desc: setter方法
 */
-(void)setStarSpaceing:(CGFloat)starSpaceing {
    if (_starSpaceing != starSpaceing) {
        _starSpaceing = starSpaceing;
        [self createStarView];
    }
}

-(void)setCurrentScore:(CGFloat)currentScore {
    if (_currentScore == currentScore) {
        return;
    }
    if (currentScore < 0) {
        _currentScore = 0;
    } else if (currentScore > _numberOfStars) {
        _currentScore = _numberOfStars;
    } else {
        _currentScore = currentScore;
    }
    
    if ([self.delegate respondsToSelector:@selector(starRateView:currentScore:)]) {
        [self.delegate starRateView:self currentScore:_currentScore];
    }
    
    if (self.complete) {
        _complete(_currentScore);
    }
    
    [self setNeedsLayout];
}

@end
