//
//  QJEvaluateCustomStarVIew.h
//  QJBox
//
//  Created by rui on 2022/7/28.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class QJEvaluateCustomStarVIew;

typedef void(^finishBlock)(CGFloat currentScore);

typedef NS_ENUM(NSInteger, QJStarRateStyle)
{
    QJStarWholeStar = 0, //只能整星评论
    QJStarHalfStar = 1,  //允许半星评论
    QJStarIncompleteStar = 2  //允许不完整星评论
};

@protocol QJEvaluateCustomStarVIewDelegate <NSObject>

/* 点击回调 */
-(void)starRateView:(QJEvaluateCustomStarVIew *)starRateView currentScore:(CGFloat)currentScore;

@end

@interface QJEvaluateCustomStarVIew : UIView

@property (nonatomic, assign) BOOL isAnimation;       //是否动画显示，默认NO
@property (nonatomic, assign) QJStarRateStyle rateStyle;    //评分样式    默认是WholeStar
@property (nonatomic, weak) id<QJEvaluateCustomStarVIewDelegate>delegate;
@property (nonatomic, assign) CGFloat currentScore;   // 当前评分：0-5  默认0
@property (nonatomic, assign) CGFloat starSpaceing; //星间距 默认 10

/**
 * @author: zjr
 * @date: 2022-8-2
 * @desc: 初始化方法
 */
-(instancetype)initWithFrame:(CGRect)frame;
-(instancetype)initWithFrame:(CGRect)frame numberOfStars:(NSInteger)numberOfStars rateStyle:(QJStarRateStyle)rateStyle isAnination:(BOOL)isAnimation delegate:(id)delegate;


-(instancetype)initWithFrame:(CGRect)frame finish:(finishBlock)finish;
-(instancetype)initWithFrame:(CGRect)frame numberOfStars:(NSInteger)numberOfStars rateStyle:(QJStarRateStyle)rateStyle isAnination:(BOOL)isAnimation finish:(finishBlock)finish;

@end

NS_ASSUME_NONNULL_END
