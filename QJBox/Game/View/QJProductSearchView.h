//
//  QJProductSearchView.h
//  QJBox
//
//  Created by wxy on 2022/7/27.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJProductSearchView : UIView

@property (nonatomic, strong) UITextField *searchTF;

@property (nonatomic, copy) void(^searchStrBlock)(NSString *str);

@property (nonatomic, copy) void(^beginEditBlock)(void);

@property (nonatomic, copy) void(^cancelBlock)(void);

@property (nonatomic, assign) BOOL hiddenCancelBtn;
@end

NS_ASSUME_NONNULL_END
