//
//  QJProductClassificationSelectedView.h
//  QJBox
//
//  Created by wxy on 2022/7/25.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/// 游戏分类左边栏选中时的样式
@interface QJProductClassificationSelectedView : UIView

@property (nonatomic, strong) UIView *topView;


@end

NS_ASSUME_NONNULL_END
