//
//  QJProductClassificationSearchResultView.h
//  QJBox
//
//  Created by wxy on 2022/7/27.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class QJProductClassificationModel,QJProductClassificationAllTagsOptionModel,QJSearchGameDetailModel;
/// 游戏分类点击全部分类item过来的vc的主视图
@interface QJProductClassificationSearchResultView : UIView

@property (nonatomic, strong) QJProductClassificationAllTagsOptionModel *tagOptionModel;

@property (nonatomic, strong) QJProductClassificationModel *model;//赋值后刷新数据


@property (nonatomic, copy) void(^didCellBlock)(QJSearchGameDetailModel *model);

 


- (void)searchDataWithKeyWords:(NSString *)keyWords;

@end

NS_ASSUME_NONNULL_END
