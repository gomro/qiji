//
//  QJProductDetailTimeLineView.m
//  QJBox
//
//  Created by rui on 2022/10/18.
//

#import "QJProductDetailTimeLineView.h"
#import "QJProductRequest.h"
#import "QJProductTimeLineCell.h"
#import "QJSearchGameListModel.h"
#import "QJProductServeListViewController.h"

#import "GKPageScrollView.h"

@interface QJProductDetailTimeLineView()<GKPageListViewDelegate,UITableViewDelegate,UITableViewDataSource>

/* table */
@property (nonatomic, strong) UITableView *rightTableView;
@property (nonatomic, strong) QJSearchGameListModel *pageModel;

/* GKScroll回调 */
@property (nonatomic, copy) void(^listScrollViewScrollBlock)(UIScrollView *scrollView);

@end

@implementation QJProductDetailTimeLineView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        /* 初始化View */
        self.pageModel = [QJSearchGameListModel new];
        [self initChildView];
    }
    return self;
}

/**
 * @author: zjr
 * @date: 2022-8-2
 * @desc: 加载view/约束
 */
- (void)initChildView {
    self.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.rightTableView];
    [self.rightTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
}

#pragma mark --Lazy Loading
- (UITableView *)rightTableView {
    if (!_rightTableView) {
        _rightTableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        _rightTableView.dataSource = self;
        _rightTableView.delegate = self;
        // 自动计算行高模式
        _rightTableView.estimatedRowHeight = 100;
        _rightTableView.rowHeight = UITableViewAutomaticDimension;
        [_rightTableView registerClass:[QJProductTimeLineCell class] forCellReuseIdentifier:@"QJProductTimeLineCell"];
        _rightTableView.backgroundColor = [UIColor whiteColor];
        _rightTableView.showsVerticalScrollIndicator = NO;
        _rightTableView.mj_footer = [QJRefreshFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreHomeData)];
        if (@available(iOS 11.0, *)) {
            _rightTableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
            _rightTableView.estimatedRowHeight = 0;
            _rightTableView.estimatedSectionFooterHeight = 0;
            _rightTableView.estimatedSectionHeaderHeight = 0;
        }
    }
    return _rightTableView;
}

#pragma mark ------- UITableViewDelegate, UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    QJEmptyCommonView *emptyView = [QJEmptyCommonView new];
    emptyView.string = @"这里什么也没有～";
    emptyView.emptyImage = @"empty_no_data";
    emptyView.topSpace = 100;
    [tableView tableViewDisplayWhenHaveNoDataWithView:emptyView ifNecessaryForRowCount:self.pageModel.records.count];
    return self.pageModel.records.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 20;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.001;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 0.001)];
    return footerView;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [[UIView alloc] init];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    QJProductTimeLineCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QJProductTimeLineCell" forIndexPath:indexPath];
    QJSearchGameDetailModel *detailModel = [self.pageModel.records safeObjectAtIndex:indexPath.row];
    QJSearchGameDetailModel *lastModel = [self.pageModel.records safeObjectAtIndex:indexPath.row-1];
    QJSearchGameDetailModel *nextModel = [self.pageModel.records safeObjectAtIndex:indexPath.row+1];
    if (nextModel && nextModel.serverOpen == YES) {
        detailModel.isShowNextOpen = YES;
    }else{
        detailModel.isShowNextOpen = NO;
    }
    if (!lastModel) {
        detailModel.isShowLastOpen = YES;
    }else if (lastModel && lastModel.serverOpen == YES) {
        detailModel.isShowLastOpen = YES;
    }else {
        detailModel.isShowLastOpen = NO;
    }
    detailModel.indexRow = indexPath.row;
    cell.detailModel = detailModel;
    WS(weakSelf)
    cell.buttonClick = ^{
        QJProductServeListViewController *serveListVC = [[QJProductServeListViewController alloc] init];
        serveListVC.detailModel = weakSelf.detailModel;
        [[QJAppTool getCurrentViewController].navigationController pushViewController:serveListVC animated:YES];
    };
    return cell;
}

#pragma mark - GKPageListViewDelegate
- (UIScrollView *)listScrollView {
    return self.rightTableView;
}

- (void)listViewDidScrollCallback:(void (^)(UIScrollView * _Nonnull))callback {
    self.listScrollViewScrollBlock = callback;
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    !self.listScrollViewScrollBlock ? : self.listScrollViewScrollBlock(scrollView);
    if (self.scrollviewBlock) {
        self.scrollviewBlock(scrollView);
    }
}

#pragma mark - 请求方法
- (void)refreshData {
    _pageModel.willLoadMore = NO;
    [self sendRequest];
}

- (void)loadMoreHomeData {
    if(_pageModel.hasNext){
        _pageModel.willLoadMore=YES;
        [self sendRequest];
    }else{
        [self.rightTableView.mj_footer endRefreshing];
        [self.rightTableView.mj_footer endRefreshingWithNoMoreData];
    }
}

/**
 * @author: zjr
 * @date: 2022-7-28
 * @desc: 开服表
 */
- (void)sendRequest{
    QJProductRequest *productReq = [[QJProductRequest alloc] init];
    NSMutableDictionary *muDic = [NSMutableDictionary dictionaryWithDictionary:_pageModel.toParams];
    [muDic setValue:_detailModel.startTime?_detailModel.startTime:[NSString getStringForDate:[NSDate date] format:@"yyyy-MM-dd HH:mm:ss"] forKey:@"startTime"];
    if (_detailModel.ID) {
        [muDic setValue:_detailModel.ID forKey:@"id"];
    }
    productReq.dic = muDic.copy;
    [productReq getProductTimeLineRequest];

    WS(weakSelf)
    productReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            [weakSelf.pageModel configObj:[QJSearchGameListModel modelWithDictionary:EncodeDicFromDic(request.responseObject, @"data")]];
        }else{
            NSString *msg = EncodeStringFromDic(request.responseObject, @"message");
            [weakSelf makeToast:msg];
        }
        [weakSelf.rightTableView reloadData];
        [weakSelf.rightTableView.mj_footer endRefreshing];
    };
    productReq.failureCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        [weakSelf.rightTableView reloadData];
        [weakSelf.rightTableView.mj_footer endRefreshing];
        [weakSelf makeToast:@"数据异常 请稍后再试"];
    };
}
@end
