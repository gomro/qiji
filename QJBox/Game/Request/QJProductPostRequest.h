//
//  QJProductPostRequest.h
//  QJBox
//
//  Created by rui on 2022/7/25.
//

#import "QJBaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJProductPostRequest : QJBaseRequest

/**
 * @author: zjr
 * @date: 2022-7-25
 * @desc:获取游戏列表
 * Post请求
 */
- (void)getProductListRequest;

/**
 * @author: zjr
 * @date: 2022-7-27
 * @desc:删除我的游戏
 * Post请求
 */
- (void)getProductDeleteRequest;

/**
 * @author: zjr
 * @date: 2022-7-27
 * @desc:添加我的游戏
 * Post请求
 */
- (void)getAddProductRequest;

@end

NS_ASSUME_NONNULL_END
