//
//  QJProductSearchRequest.m
//  QJBox
//
//  Created by wxy on 2022/7/26.
//

#import "QJProductSearchRequest.h"

@implementation QJProductSearchRequest

- (void)beginQueryTages {
    self.urlStr = @"/game/search/tags";
    [self start];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

- (YTKRequestSerializerType)requestSerializerType {
    return YTKRequestSerializerTypeHTTP;
}

@end
