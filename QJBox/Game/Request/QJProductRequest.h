//
//  QJProductRequest.h
//  QJBox
//
//  Created by rui on 2022/7/25.
//

#import "QJBaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJProductRequest : QJBaseRequest

/**
 * @author: zjr
 * @date: 2022-7-25
 * @desc:获取游戏中心banner
 * GET请求
 */
- (void)getProductBannerRequest;

/**
 * @author: zjr
 * @date: 2022-7-25
 * @desc:获取我的游戏
 * GET请求
 */
- (void)getUserProductRequest;

/**
 * @author: zjr
 * @date: 2022-7-25
 * @desc:获取游戏详情
 * GET请求
 */
- (void)getProductDetailRequest:(NSString *)productID;

/**
 * @author: zjr
 * @date: 2022-7-25
 * @desc:启动游戏获取自动登陆签名
 * GET请求
 */
- (void)getProductSignatureRequest:(NSString *)productID;

/**
 * @author: zjr
 * @date: 2022-7-28
 * @desc:获取游戏根评论
 * GET请求
 */
- (void)getProductCommentRoot:(NSString *)productID;

/**
 * @author: zjr
 * @date: 2022-8-9
 * @desc:获取游戏评分数据
 * GET请求
 */
- (void)getProductGrade:(NSString *)productID;

/**
 * @author: zjr
 * @date: 2022-7-28
 * @desc:获取开服时间轴
 * GET请求
 */
- (void)getProductTimeLineRequest;


@end

NS_ASSUME_NONNULL_END
