//
//  QJProductSearchRequest.h
//  QJBox
//
//  Created by wxy on 2022/7/26.
//

#import "QJBaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

/// 搜索分类
@interface QJProductSearchRequest : QJBaseRequest

- (void)beginQueryTages;


@end

NS_ASSUME_NONNULL_END
