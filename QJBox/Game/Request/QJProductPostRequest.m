//
//  QJProductPostRequest.m
//  QJBox
//
//  Created by rui on 2022/7/25.
//

#import "QJProductPostRequest.h"

@implementation QJProductPostRequest

/**
 * @author: zjr
 * @date: 2022-7-25
 * @desc:获取游戏列表
 * Post请求
 */
- (void)getProductListRequest{
    self.urlStr = @"/game/search/server";
    [self start];
}

/**
 * @author: zjr
 * @date: 2022-7-27
 * @desc:删除我的游戏
 * Post请求
 */
- (void)getProductDeleteRequest{
    self.urlStr = @"/game/removeMyGame";
    [self start];
}

/**
 * @author: zjr
 * @date: 2022-7-27
 * @desc:添加我的游戏
 * Post请求
 */
- (void)getAddProductRequest{
    self.urlStr = @"/game/saveMyGame";
    [self start];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

- (YTKRequestSerializerType)requestSerializerType {
    return YTKRequestSerializerTypeJSON;
}

@end
