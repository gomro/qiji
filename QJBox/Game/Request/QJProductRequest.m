//
//  QJProductRequest.m
//  QJBox
//
//  Created by rui on 2022/7/25.
//

#import "QJProductRequest.h"

@implementation QJProductRequest

/**
 * @author: zjr
 * @date: 2022-7-25
 * @desc:获取游戏中心banner
 * GET请求
 * APP_HOME:app首页、GAME:游戏首页、INFO:资讯首页,可用值:APP_HOME,GAME,INFO
 */
- (void)getProductBannerRequest{
    self.urlStr = @"/banner";
    [self start];
}

/**
 * @author: zjr
 * @date: 2022-7-25
 * @desc:获取我的游戏
 * GET请求
 */
- (void)getUserProductRequest{
    self.urlStr = @"/game/search/myGameContent";
    [self start];
}

/**
 * @author: zjr
 * @date: 2022-7-25
 * @desc:获取游戏详情
 * GET请求
 */
- (void)getProductDetailRequest:(NSString *)productID{
    self.urlStr = [NSString stringWithFormat:@"/game/search/info/%@",productID];
    [self start];
}

/**
 * @author: zjr
 * @date: 2022-7-25
 * @desc:启动游戏获取自动登陆签名
 * GET请求
 */
- (void)getProductSignatureRequest:(NSString *)productID{
    self.urlStr = [NSString stringWithFormat:@"/game/getSignature/%@",productID];
    [self start];
}

/**
 * @author: zjr
 * @date: 2022-7-28
 * @desc:获取游戏根评论
 * GET请求
 */
- (void)getProductCommentRoot:(NSString *)productID{
    self.urlStr = [NSString stringWithFormat:@"/comment/%@/root", productID];
    [self start];
}

/**
 * @author: zjr
 * @date: 2022-8-9
 * @desc:获取游戏评分数据
 * GET请求
 */
- (void)getProductGrade:(NSString *)productID{
    self.urlStr = [NSString stringWithFormat:@"/game/gamePoint/%@", productID];
    [self start];
}

/**
 * @author: zjr
 * @date: 2022-7-28
 * @desc:获取开服时间轴
 * GET请求
 */
- (void)getProductTimeLineRequest{
    self.urlStr = @"/game/search/singleServer";
    [self start];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

- (YTKRequestSerializerType)requestSerializerType {
    return YTKRequestSerializerTypeHTTP;
}

@end
