//
//  QJProductCommentListModel.h
//  QJBox
//
//  Created by rui on 2022/7/28.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@class QJProductCommentDetailModel;

@interface QJProductCommentListModel : NSObject

/* 数据源数组 */
@property (nonatomic, strong) NSMutableArray<QJProductCommentDetailModel *> *records;
/* 总条数 */
@property (nonatomic, strong) NSNumber *total;
/* 分页 */
@property (nonatomic, strong) NSNumber *page;
/* 当前页 */
@property (nonatomic, strong) NSNumber *current;
/* 是否能分页 */
@property (nonatomic, assign) BOOL hasNext;
/* 是否需要加载更多 */
@property (assign, nonatomic) BOOL willLoadMore;
/* 分页 */
- (NSDictionary *)toParams;
/* 解析model */
-(void)configObj:(QJProductCommentListModel *)model;

@end

@interface QJProductCommentDetailModel : NSObject

/* 评论ID */
@property (nonatomic, strong) NSString *commentId;
/* 评论内容 */
@property (nonatomic, strong) NSString *content;
/* 用户头像 */
@property (nonatomic, strong) NSString *coverImage;
/* 评论星级 */
@property (nonatomic, strong) NSString *levelComment;
/* 时间 */
@property (nonatomic, strong) NSString *timeShow;
/* 用户姓名 */
@property (nonatomic, strong) NSString *userName;
/* 用户ID */
@property (nonatomic, strong) NSString *userId;
/* 是否展开评论 */
@property (nonatomic, assign) BOOL isShowAll;

@end


NS_ASSUME_NONNULL_END
