//
//  QJProductClassificationModel.m
//  QJBox
//
//  Created by wxy on 2022/7/26.
//

#import "QJProductClassificationModel.h"

@implementation QJProductClassificationModel


+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"orderOptions" : [QJProductClassificationOrderOptionModel class],
             @"filterOption" : [QJProductClassificationFilterOptionsModel class],
             @"allTagsOption" : [QJProductClassificationAllTagsOptionModel class],
             
    };
}




@end


@implementation QJProductClassificationAppSystemImgModel

@end

@implementation QJProductClassificationOrderOptionModel

+ (NSDictionary *)modelCustomPropertyMapper{
    return @{@"idStr":@"id"};
}

@end

@implementation QJProductClassificationFilterOptionsModel
+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"values" : [NSString class],
    };
}

+ (NSDictionary *)modelCustomPropertyMapper{
    return @{@"idStr":@"id"};
}
@end

@implementation QJProductClassificationAllTagsOptionModel
+ (NSDictionary *)modelCustomPropertyMapper{
    return @{@"idStr":@"id"};
}
@end
