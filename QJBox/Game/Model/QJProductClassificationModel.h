//
//  QJProductClassificationModel.h
//  QJBox
//
//  Created by wxy on 2022/7/26.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
@class QJProductClassificationAppSystemImgModel,QJProductClassificationOrderOptionModel,QJProductClassificationFilterOptionsModel,QJProductClassificationAllTagsOptionModel;
@interface QJProductClassificationModel : NSObject

@property (nonatomic, strong) NSArray *orderOptions;

@property (nonatomic, strong) NSArray *filterOption;

@property (nonatomic, strong) NSArray *allTagsOption;

@property (nonatomic, strong) QJProductClassificationAppSystemImgModel *appSystemImg;

@end

//二级
@interface QJProductClassificationOrderOptionModel : NSObject

@property (nonatomic, copy) NSString *name;

@property (nonatomic, copy) NSString *idStr;

@end

@interface QJProductClassificationFilterOptionsModel : NSObject

@property (nonatomic, copy) NSString *name;

@property (nonatomic, strong) NSArray *values;

@property (nonatomic, copy) NSString *idStr;

@end

@interface QJProductClassificationAllTagsOptionModel : NSObject

@property (nonatomic, copy) NSString *name;

@property (nonatomic, copy) NSString *idStr;

@property (nonatomic, copy) NSString *icon;

@end

@interface QJProductClassificationAppSystemImgModel : NSObject

@property (nonatomic, copy) NSString *name;

@property (nonatomic, copy) NSString *imgUrl;

@end

NS_ASSUME_NONNULL_END
