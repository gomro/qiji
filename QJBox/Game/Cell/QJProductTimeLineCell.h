//
//  QJProductTimeLineCell.h
//  QJBox
//
//  Created by rui on 2022/7/28.
//

#import <UIKit/UIKit.h>
#import "QJSearchGameListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJProductTimeLineCell : UITableViewCell

/* 数据源 */
@property (nonatomic, strong) QJSearchGameDetailModel *detailModel;
/* 查看开服表事件 */
@property (nonatomic, copy) void(^buttonClick)(void);

@end

NS_ASSUME_NONNULL_END
