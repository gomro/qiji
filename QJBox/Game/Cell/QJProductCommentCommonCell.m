//
//  QJProductCommentCommonCell.m
//  QJBox
//
//  Created by rui on 2022/7/28.
//

#import "QJProductCommentCommonCell.h"
/* 评论星星view */
#import "QJEvaluateStarView.h"

@interface QJProductCommentCommonCell ()

/* 用户头像 */
@property (nonatomic, strong) UIImageView *iconImageView;
/* 姓名 */
@property (nonatomic, strong) UILabel *nameLabel;
/* 时间 */
@property (nonatomic, strong) UILabel *timeLabel;
/* 评分 */
@property (nonatomic, strong) QJEvaluateStarView *starView;
/* 内容 */
@property (nonatomic, strong) YYLabel *contentLabel;
/* 分割线 */
@property (nonatomic, strong) UIView *lineView;

@end

@implementation QJProductCommentCommonCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.contentView.backgroundColor = [UIColor colorWithHexString:@"0xFFFFFF"];
        /* 初始化UI */
        [self initUI];
        /* 添加约束 */
        [self makeConstraints];
    }
    return self;
}

/**
 * @author: zjr
 * @date: 2022-7-7
 * @desc: 初始化UI
 */
- (void)initUI{
    [self.contentView addSubview:self.iconImageView];
    [self.contentView addSubview:self.nameLabel];
    [self.contentView addSubview:self.timeLabel];
    [self.contentView addSubview:self.starView];
    [self.contentView addSubview:self.contentLabel];
    [self.contentView addSubview:self.lineView];
}

/**
 * @author: zjr
 * @date: 2022-7-7
 * @desc: 添加约束
 */
- (void)makeConstraints{
    [self.iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.contentView).mas_offset(24);
        make.left.mas_equalTo(self.contentView).mas_offset(20);
        make.width.mas_equalTo(36*kWScale);
        make.height.mas_equalTo(36*kWScale);
    }];
    
    [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.iconImageView);
        make.right.mas_equalTo(self.contentView).mas_offset(-16);
    }];
    
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.iconImageView);
        make.left.mas_equalTo(self.iconImageView.mas_right).mas_offset(7);
    }];
    
    [self.starView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.iconImageView);
        make.left.mas_equalTo(self.iconImageView.mas_right).mas_offset(7);
        make.size.mas_equalTo(CGSizeMake(70*kWScale,12*kWScale));
    }];
    
    [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.iconImageView.mas_bottom).mas_offset(10);
        make.right.mas_equalTo(self.contentView).mas_offset(-16);
        make.left.mas_equalTo(64);
    }];
    
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.contentLabel.mas_bottom).mas_offset(15);
        make.bottom.mas_equalTo(self.contentView).mas_offset(0);
        make.width.mas_equalTo(QJScreenWidth);
        make.height.mas_equalTo(1);
    }];
}

#pragma mark - Lazy Loading
- (UIImageView *)iconImageView {
    if (!_iconImageView) {
        _iconImageView = [UIImageView new];
        _iconImageView.layer.cornerRadius = 18;
        _iconImageView.layer.masksToBounds = YES;
        _iconImageView.contentMode = UIViewContentModeScaleAspectFill;
    }
    return _iconImageView;
}

- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [UILabel new];
        _nameLabel.font = kFont(14);
        _nameLabel.textColor = [UIColor blackColor];
        _nameLabel.text = @"";
    }
    return _nameLabel;
}

- (UILabel *)timeLabel {
    if (!_timeLabel) {
        _timeLabel = [UILabel new];
        _timeLabel.font = kFont(10);
        _timeLabel.textColor = UIColorFromRGB(0x919599);
        _timeLabel.text = @"";
    }
    return _timeLabel;
}

- (QJEvaluateStarView *)starView{
    if (!_starView) {
        _starView = [[QJEvaluateStarView alloc] initWithFrame:CGRectMake(0, 0, 70, 12)];
        _starView.noAnimate = YES;
        [_starView updateWithIndex:5];
    }
    return _starView;
}

- (YYLabel *)contentLabel {
    if (!_contentLabel) {
        _contentLabel = [[YYLabel alloc] init];
        _contentLabel.backgroundColor = [UIColor clearColor];
        
        //设置最大宽度 ，如果宽度没有设置好，会出现高度计算不准确，不能自动换行的问题
        _contentLabel.preferredMaxLayoutWidth = kScreenWidth-80;
    }
    return _contentLabel;
}

- (UIView *)lineView{
    if (!_lineView) {
        _lineView = [UIView new];
        _lineView.backgroundColor = UIColorFromRGB(0xF5F5F5);
    }
    return _lineView;
}

/**
 * @author: zjr
 * @date: 2022-7-28
 * @desc: 数据赋值
 */
- (void)setModel:(QJProductCommentDetailModel *)model {
    _model = model;
    self.nameLabel.text = kCheckStringNil(_model.userName);
    [self.iconImageView setImageWithURL:[NSURL URLWithString:[model.coverImage checkImageUrlString]] placeholder:[UIImage imageNamed:@"mine_head"]];
    self.timeLabel.text = kCheckStringNil(_model.timeShow);//[QJAppTool formatDaystringTime:_model.timeShow]
    [self.starView updateWithIndex:_model.levelComment.integerValue-1];
    [self setContentLabelNum:kCheckStringNil(_model.content)];
}

// 设置回复文字内容
- (void)setContentLabelNum:(NSString *)content {
    NSMutableAttributedString *contentString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@", content] attributes:@{NSFontAttributeName : FontRegular(14), NSForegroundColorAttributeName : UIColorFromRGB(0x16191C)}];

    self.contentLabel.attributedText = contentString;

    // 如果是展开的状态，Label的行数是0，默认行数是3
    if (_model.isShowAll) {
        self.contentLabel.numberOfLines = 0;
        // 如果是展开的状态，说明文字数量大于两行，文字末尾加上 收起 功能
        [self expandString];
    } else {
        self.contentLabel.numberOfLines = 3;
        // 如果是默认状态，文字可能是多行，更改文字过多时结尾的...,增加点击事件
        [self addSeeMoreButtonInLabel:self.contentLabel];
    }
}

// 多行显示内容的尾部将 ... 改成 查看全部并添加点击事件
- (void)addSeeMoreButtonInLabel:(YYLabel *)label {
    UIFont *font = FontRegular(14);
 
    NSString *moreString = @" 全部";
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@", moreString]];
    NSRange expandRange = [text.string rangeOfString:moreString];
    
    [text addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0x007aff) range:expandRange];
    [text addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0x474849) range:NSMakeRange(0, expandRange.location)];

    //添加点击事件
    YYTextHighlight *hi = [YYTextHighlight new];
    [text setTextHighlight:hi range:[text.string rangeOfString:moreString]];
    
    __weak typeof(self) weakSelf = self;
    hi.tapAction = ^(UIView *containerView, NSAttributedString *text, NSRange range, CGRect rect) {
        //点击展开
        if (weakSelf.showAllHeadViewClick) {
            weakSelf.showAllHeadViewClick(YES);
        }
    };
    
    text.font = font;
    
    YYLabel *seeMore = [YYLabel new];
    seeMore.attributedText = text;
    [seeMore sizeToFit];
    
    NSAttributedString *truncationToken = [NSAttributedString attachmentStringWithContent:seeMore contentMode:UIViewContentModeCenter attachmentSize:seeMore.frame.size alignToFont:text.font alignment:YYTextVerticalAlignmentTop];
    
    label.truncationToken = truncationToken;
}

- (void)expandString {
    NSMutableAttributedString *attri = [_contentLabel.attributedText mutableCopy];
    [attri appendAttributedString:[self appendAttriStringWithFont:attri.font]];
    self.contentLabel.attributedText = attri;
}

- (NSAttributedString *)appendAttriStringWithFont:(UIFont *)font {
    if (!font) {
        font = FontRegular(14);
    }

    NSString *appendText = @" 收起 ";
    NSMutableAttributedString *append = [[NSMutableAttributedString alloc] initWithString:appendText attributes:@{NSFontAttributeName : font, NSForegroundColorAttributeName : UIColorFromRGB(0x007aff)}];
    
    YYTextHighlight *hi = [YYTextHighlight new];
    [append setTextHighlight:hi range:[append.string rangeOfString:appendText]];
    
    __weak typeof(self) weakSelf = self;
    hi.tapAction = ^(UIView *containerView, NSAttributedString *text, NSRange range, CGRect rect) {
        //点击收起
        if (weakSelf.showAllHeadViewClick) {
            weakSelf.showAllHeadViewClick(NO);
        }
    };
    
    return append;
}

@end
