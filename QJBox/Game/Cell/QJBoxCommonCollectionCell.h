//
//  QJBoxCommonCollectionCell.h
//  QJBox
//
//  Created by rui on 2022/7/22.
//

#import <UIKit/UIKit.h>
#import "QJSearchGameListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJBoxCommonCollectionCell : UICollectionViewCell

/* 数据源 */
@property (nonatomic, strong) QJSearchGameDetailModel *model;

@end

NS_ASSUME_NONNULL_END
