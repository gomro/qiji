//
//  QJProductClassificationCollectionViewCell.h
//  QJBox
//
//  Created by wxy on 2022/7/25.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class QJProductClassificationAllTagsOptionModel;
/// 游戏分类中的cell
@interface QJProductClassificationCollectionViewCell : UICollectionViewCell


@property (nonatomic, strong) QJProductClassificationAllTagsOptionModel *model;

@end

NS_ASSUME_NONNULL_END
