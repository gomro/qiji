//
//  QJProductCommentCommonCell.h
//  QJBox
//
//  Created by rui on 2022/7/28.
//

#import <UIKit/UIKit.h>
#import "QJProductCommentListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJProductCommentCommonCell : UITableViewCell
/* 数据源 */
@property (nonatomic, strong) QJProductCommentDetailModel *model;
/* 点击查看更多事件 */
@property (nonatomic, copy) void(^showAllHeadViewClick)(BOOL isShow);

@end

NS_ASSUME_NONNULL_END
