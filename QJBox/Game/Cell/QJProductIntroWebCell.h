//
//  QJProductIntroWebCell.h
//  QJBox
//
//  Created by rui on 2022/7/28.
//

#import <UIKit/UIKit.h>
#import "QJSearchGameListModel.h"

NS_ASSUME_NONNULL_BEGIN

@protocol QJProductIntroWebCellDelegate <NSObject>

/* delegate回调高度 */
@optional
- (void)changeHeightWith:(CGFloat)height;

@end

@interface QJProductIntroWebCell : UITableViewCell

/* 数据源 */
@property (nonatomic, strong) QJSearchGameDetailModel *detailModel;
/* 回调代理 */
@property (nonatomic, weak) id<QJProductIntroWebCellDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
