//
//  QJProductClassificationCollectionViewCell.m
//  QJBox
//
//  Created by wxy on 2022/7/25.
//

#import "QJProductClassificationCollectionViewCell.h"
#import "QJProductClassificationModel.h"
@interface QJProductClassificationCollectionViewCell ()

@property (nonatomic, strong) UIImageView *imageView;


@property (nonatomic, strong) UILabel *label;



@end


@implementation QJProductClassificationCollectionViewCell


- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self.contentView addSubview:self.imageView];
        [self.contentView addSubview:self.label];
        self.backgroundColor = [UIColor whiteColor];
        [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.equalTo(self.contentView).offset(Get375Width(8));
            make.width.height.equalTo(@(Get375Width(46)));
        }];
        
        [self.label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.imageView.mas_bottom).offset(Get375Width(10));
            make.centerX.equalTo(self.contentView);
            make.width.equalTo(self);
            
        }];
    }
    return self;
}



#pragma mark  ----- getter


- (UIImageView *)imageView {
    if (!_imageView) {
        _imageView = [UIImageView new];
    }
    return _imageView;
}


- (UILabel *)label {
    if (!_label) {
        _label = [UILabel new];
        _label.font = FontRegular(12);
        _label.textColor = kColorWithHexString(@"#000000");
        _label.textAlignment = NSTextAlignmentCenter;
    }
    return _label;
}


- (void)setModel:(QJProductClassificationAllTagsOptionModel *)model {
    _model = model;
    self.imageView.imageURL = [NSURL URLWithString:[model.icon checkImageUrlString]];
    self.label.text = model.name;
}

@end
