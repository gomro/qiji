//
//  QJProductNestCell.h
//  QJBox
//
//  Created by rui on 2022/7/27.
//

#import "QJBaseTableViewCell.h"
#import "QJSearchGameListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJProductNestCell : QJBaseTableViewCell

/* 数据源 */
@property (nonatomic, strong) QJSearchGameDetailModel *detailModel;
/* 是否能滚动 */
@property (nonatomic, assign) BOOL canScroll;
/* 选中页面的page */
@property (nonatomic, assign) NSInteger currentPage;
/* scroll滚动切换回调 */
@property (nonatomic, copy) void(^scrollCurrentClick)(NSInteger currentPage);
/* 子table内容偏移量重置 */
- (void)scrollTableView;

@end

NS_ASSUME_NONNULL_END
