//
//  QJBoxCommonCollectionCell.m
//  QJBox
//
//  Created by rui on 2022/7/22.
//

#import "QJBoxCommonCollectionCell.h"

@interface QJBoxCommonCollectionCell ()

/* 标题 */
@property (nonatomic, strong) UIImageView *iconImageView;
/* 标题 */
@property (nonatomic, strong) UILabel *titleLabel;
/* 来源 */
@property (nonatomic, strong) UILabel *tagLabel;

@end

@implementation QJBoxCommonCollectionCell

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        /* 初始化UI */
        [self initUI];
        /* 添加约束 */
        [self makeConstraints];
    }
    return self;
}

/**
 * @author: zjr
 * @date: 2022-7-5
 * @desc: 初始化UI
 */
- (void)initUI{
    [self.contentView addSubview:self.iconImageView];
    [self.contentView addSubview:self.tagLabel];
    [self.contentView addSubview:self.titleLabel];
}

/**
 * @author: zjr
 * @date: 2022-7-5
 * @desc: 添加约束
 */
- (void)makeConstraints{
    [self.iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@42);
        make.height.equalTo(@42);
        make.top.equalTo(self.contentView).offset(10);
        make.centerX.equalTo(self.contentView);
    }];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.iconImageView.mas_bottom).offset(6);
        make.left.equalTo(self.contentView).offset(5);
        make.right.equalTo(self.contentView).offset(-5);
        make.height.equalTo(@14);
    }];
    
    [self.tagLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.contentView);
        make.top.equalTo(self.titleLabel.mas_bottom).offset(5);
        make.height.equalTo(@17);
        make.width.equalTo(@([@"天使之子" getWidthWithFont:kFont(8) withHeight:16]+10));
    }];
}

#pragma mark - Lazy Loading
- (UIImageView *)iconImageView{
    if (!_iconImageView) {
        _iconImageView = [UIImageView new];
        _iconImageView.layer.cornerRadius = 8;
        _iconImageView.layer.masksToBounds = YES;
        _iconImageView.contentMode = UIViewContentModeScaleAspectFill;
    }
    return _iconImageView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.font = kFont(14);
        _titleLabel.textColor = [UIColor colorWithHexString:@"0x000000"];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.text = @"";
    }
    return _titleLabel;
}

- (UILabel *)tagLabel {
    if (!_tagLabel) {
        _tagLabel = [UILabel new];
        _tagLabel.font = FontRegular(10);
        _tagLabel.textColor = [UIColor hx_colorWithHexStr:@"#8D3C0F" alpha:0.4];
        _tagLabel.textAlignment = NSTextAlignmentCenter;
        _tagLabel.text = @"";
        _tagLabel.backgroundColor = [UIColor hx_colorWithHexStr:@"#C85C0E" alpha:0.08];
        _tagLabel.layer.cornerRadius = 2;
    }
    return _tagLabel;
}

/**
 * @author: zjr
 * @date: 2022-7-7
 * @desc: 数据赋值
 */
- (void)setModel:(QJSearchGameDetailModel *)model{
    _model = model;
    self.titleLabel.text = kCheckStringNil(_model.name);
    self.iconImageView.imageURL = [NSURL URLWithString:[_model.icon.length > 0 ?_model.icon:_model.thumbnail checkImageUrlString]];
    _tagLabel.hidden = NO;
    if (_model.tags.count > 0) {
        QJSearchGameDetailModel *tagModel = [_model.tags safeObjectAtIndex:0];
        _tagLabel.text = tagModel.name;
        [self.tagLabel mas_updateConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(@([_tagLabel.text getWidthWithFont:FontRegular(10) withHeight:16]+10));
        }];
        _tagLabel.textColor = [UIColor hx_colorWithHexStr:tagModel.color alpha:0.4];
        _tagLabel.backgroundColor = [UIColor hx_colorWithHexStr:tagModel.color alpha:0.08];
    }else{
        _tagLabel.hidden = YES;
        _tagLabel.textColor = [UIColor whiteColor];
        _tagLabel.backgroundColor = [UIColor whiteColor];
    }
}

@end
