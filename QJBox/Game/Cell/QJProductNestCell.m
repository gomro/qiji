//
//  QJProductNestCell.m
//  QJBox
//
//  Created by rui on 2022/7/27.
//

#import "QJProductNestCell.h"
/* 请求 */
#import "QJProductRequest.h"
/* 开服表model */
#import "QJSearchGameListModel.h"
/* 时间轴 */
#import "QJProductTimeLineCell.h"
/* webview */
#import "QJProductIntroWebCell.h"
#import "QJProductTitleContentCell.h"
/* 开服表 */
#import "QJProductServeListViewController.h"

@interface QJProductNestCell ()<UITableViewDelegate, UITableViewDataSource,QJProductIntroWebCellDelegate,QJProductTitleContentWebCellDelegate>

/* 底部scroll */
@property (nonatomic, strong) UIScrollView *bgScrollView;
/* 左边详情table */
@property (nonatomic, strong) UITableView *leftTableView;
/* webview高度 */
@property (nonatomic, assign) CGFloat webCellHeight;
@property (nonatomic, assign) CGFloat webContentCellHeight;
/* 右边开服表table */
@property (nonatomic, strong) UITableView *rightTableView;
/* 开服表model */
@property (nonatomic, strong) QJSearchGameListModel *pageModel;

@end

@implementation QJProductNestCell

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.webCellHeight = 0;
        self.webContentCellHeight = 0;
        
        [self.contentView addSubview:self.bgScrollView];
        self.pageModel = [QJSearchGameListModel new];
        [self.bgScrollView addSubview:self.leftTableView];
        [self.bgScrollView addSubview:self.rightTableView];
        
        [self.bgScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.contentView);
        }];
        
        [self.leftTableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.bgScrollView);
            make.height.equalTo(self.bgScrollView);
            make.width.equalTo(@(kScreenWidth));
            make.top.equalTo(self.bgScrollView);
        }];
        
        [self.rightTableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.leftTableView.mas_right);
            make.top.equalTo(self.leftTableView);
            make.width.equalTo(@(kScreenWidth));
            make.bottom.equalTo(self.leftTableView);
        }];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeStatus) name:QJProductDetailNestMainCellScrollNotification object:nil];//外部通知这里可以滑动
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changePageCount) name:QJProductDetailRefreshAllDataNotification object:nil];//外部下拉刷新这里把内部的网络请求的page改为1
    }
    return self;
}

/**
 * @author: zjr
 * @date: 2022-8-2
 * @desc: 通知方法
 */
- (void)changeStatus{
    self.canScroll = YES;
}

- (void)changePageCount {
    [self refreshData];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.bgScrollView.contentSize = CGSizeMake(2*kScreenWidth, self.height);
}

/**
 * @author: zjr
 * @date: 2022-8-2
 * @desc: 赋值/重置
 */
- (void)setCurrentPage:(NSInteger)currentPage {
    _currentPage = currentPage;
    [self.bgScrollView setContentOffset:CGPointMake(currentPage *kScreenWidth, 0) animated:YES];
}

- (void)scrollTableView{
    [self.leftTableView setContentOffset:CGPointMake(0, 0) animated:NO];
    [self.rightTableView setContentOffset:CGPointMake(0, 0) animated:NO];
}

- (void)setDetailModel:(QJSearchGameDetailModel *)detailModel{
    if (_detailModel != detailModel) {
        _detailModel = detailModel;
        [self.leftTableView reloadData];
    }
}

#pragma mark ------- Lazy Loading
- (UIScrollView *)bgScrollView {
    if (!_bgScrollView) {
        _bgScrollView = [[UIScrollView alloc]init];
        _bgScrollView.pagingEnabled = YES;
        _bgScrollView.bounces = NO;
        _bgScrollView.delegate = self;
        _bgScrollView.showsHorizontalScrollIndicator = NO;
    }
    return _bgScrollView;
}

- (UITableView *)leftTableView {
    if (!_leftTableView) {
        _leftTableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _leftTableView.dataSource = self;
        _leftTableView.delegate = self;
        [_leftTableView registerClass:[QJProductIntroWebCell class] forCellReuseIdentifier:@"QJProductIntroWebCell"];
        [_leftTableView registerClass:[QJProductTitleContentCell class] forCellReuseIdentifier:@"QJProductTitleContentCell"];
        _leftTableView.backgroundColor = [UIColor whiteColor];
        _leftTableView.estimatedRowHeight = 100;
        _leftTableView.rowHeight = UITableViewAutomaticDimension;
        _leftTableView.showsVerticalScrollIndicator = NO;
    }
    return _leftTableView;
}

- (UITableView *)rightTableView {
    if (!_rightTableView) {
        _rightTableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        _rightTableView.dataSource = self;
        _rightTableView.delegate = self;
        // 自动计算行高模式
        _rightTableView.estimatedRowHeight = 100;
        _rightTableView.rowHeight = UITableViewAutomaticDimension;
        [_rightTableView registerClass:[QJProductTimeLineCell class] forCellReuseIdentifier:@"QJProductTimeLineCell"];
        _rightTableView.backgroundColor = [UIColor whiteColor];
        _rightTableView.showsVerticalScrollIndicator = NO;
        _rightTableView.mj_footer = [QJRefreshFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreHomeData)];
//        _rightTableView.mj_footer = [QJRefreshFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreHomeData)];
    }
    return _rightTableView;
}

#pragma mark ------- UITableViewDelegate, UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (tableView == self.leftTableView) {
        return 2;
    }
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == self.leftTableView) {
        if ([kCheckStringNil(self.detailModel.gameInfo) isEqualToString:@""]) {
            QJEmptyCommonView *emptyView = [QJEmptyCommonView new];
            emptyView.string = @"这里什么也没有～";
            emptyView.emptyImage = @"empty_no_data";
            emptyView.topSpace = 100;
            [tableView tableViewDisplayWhenHaveNoDataWithView:emptyView ifNecessaryForRowCount:0];

            return 0;
        }else{
            QJEmptyCommonView *emptyView = [QJEmptyCommonView new];
            emptyView.string = @"这里什么也没有～";
            emptyView.emptyImage = @"empty_no_data";
            emptyView.topSpace = 100;
            [tableView tableViewDisplayWhenHaveNoDataWithView:emptyView ifNecessaryForRowCount:1];
            return 1;
        }
    }else if (tableView == self.rightTableView) {
        QJEmptyCommonView *emptyView = [QJEmptyCommonView new];
        emptyView.string = @"这里什么也没有～";
        emptyView.emptyImage = @"empty_no_data";
        emptyView.topSpace = 100;
        [tableView tableViewDisplayWhenHaveNoDataWithView:emptyView ifNecessaryForRowCount:self.pageModel.records.count];

        return self.pageModel.records.count;
    }
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == self.leftTableView) {
        if (indexPath.section == 0) {
            return _webContentCellHeight;
        }else{
            return _webCellHeight;
        }
    }
    if (tableView == self.rightTableView) {
        return UITableViewAutomaticDimension;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (tableView == self.leftTableView) {
        return 40;
    }

    if (tableView == self.rightTableView) {
        return 20;
    }
    return 0.001;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.001;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 0.001)];
    return footerView;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (tableView == self.leftTableView) {
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, QJScreenWidth, 40)];
        UILabel *titleLabel = [UILabel new];
        titleLabel.font = FontRegular(16);
        titleLabel.textColor = UIColor.blackColor;
        [headerView addSubview:titleLabel];
        [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(headerView).offset(10);
            make.left.equalTo(headerView).offset(16);
            make.right.equalTo(headerView).offset(-16);
            make.height.equalTo(@30);
        }];
        if (section == 0){
            titleLabel.text = @"游戏简介";
        }else{
            titleLabel.text = @"游戏内容";
        }
        return headerView;
    }
    
    if (tableView == self.rightTableView) {
        return [[UIView alloc] init];
    }
    return nil;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == self.leftTableView) {
        if (indexPath.section == 0) {
            return _webContentCellHeight;
        }else{
            return _webCellHeight;
        }
    }
    if (tableView == self.rightTableView) {
        return UITableViewAutomaticDimension;
    }
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.leftTableView) {
        if (indexPath.section == 0) {
            QJProductTitleContentCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QJProductTitleContentCell" forIndexPath:indexPath];
            cell.detailModel = self.detailModel;
            cell.delegate = self;
            return cell;
        }else{
            QJProductIntroWebCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QJProductIntroWebCell" forIndexPath:indexPath];
            cell.detailModel = self.detailModel;
            cell.delegate = self;
            return cell;
        }
    }else if (tableView == self.rightTableView) {
        QJProductTimeLineCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QJProductTimeLineCell" forIndexPath:indexPath];
        QJSearchGameDetailModel *detailModel = [self.pageModel.records safeObjectAtIndex:indexPath.row];
        QJSearchGameDetailModel *lastModel = [self.pageModel.records safeObjectAtIndex:indexPath.row-1];
        QJSearchGameDetailModel *nextModel = [self.pageModel.records safeObjectAtIndex:indexPath.row+1];
        if (nextModel && nextModel.serverOpen == YES) {
            detailModel.isShowNextOpen = YES;
        }else{
            detailModel.isShowNextOpen = NO;
        }
        if (!lastModel) {
            detailModel.isShowLastOpen = YES;
        }else if (lastModel && lastModel.serverOpen == YES) {
            detailModel.isShowLastOpen = YES;
        }else {
            detailModel.isShowLastOpen = NO;
        }
        detailModel.indexRow = indexPath.row;
        cell.detailModel = detailModel;
        WS(weakSelf)
        cell.buttonClick = ^{
            QJProductServeListViewController *serveListVC = [[QJProductServeListViewController alloc] init];
            serveListVC.detailModel = weakSelf.detailModel;
            [[QJAppTool getCurrentViewController].navigationController pushViewController:serveListVC animated:YES];
        };
        return cell;
    }
    return [UITableViewCell new];
}

#pragma mark  -----QJProductIntroWebCellDelegate
- (void)changeHeightWith:(CGFloat)height{
    _webCellHeight = height;
    [_leftTableView reloadData];
}

- (void)changeContentHeightWith:(CGFloat)height{
    _webContentCellHeight = height;
    [_leftTableView reloadData];
}

#pragma mark  -----UIScrollViewDelegate
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (scrollView != self.bgScrollView) {
        if (!self.canScroll) {
            scrollView.contentOffset = CGPointZero;
        }
        if (scrollView.contentOffset.y < 0 ) {
            self.canScroll = NO;
            scrollView.contentOffset = CGPointZero;
            [[NSNotificationCenter defaultCenter] postNotificationName:QJProductDetailNestCellScrollNotification object:nil];//到顶通知父视图改变状态
        }
    }else{
        CGFloat x = scrollView.contentOffset.x;
        if (x == 0) {
            if (self.leftTableView.contentOffset.y == 0) {
                self.leftTableView.contentOffset = CGPointMake(0, 1);
            }
            self.currentPage = 0;
            if (self.scrollCurrentClick) {
                self.scrollCurrentClick(0);
            }
        }else if (x == kScreenWidth) {
            if (self.rightTableView.contentOffset.y == 0) {
                self.rightTableView.contentOffset = CGPointMake(0, 1);
            }
            self.currentPage = 1;
            if (self.scrollCurrentClick) {
                self.scrollCurrentClick(1);
            }
        }
    }
}

#pragma mark - 请求方法
- (void)refreshData {
    _pageModel.willLoadMore = NO;
    [self sendRequest];
}

- (void)loadMoreHomeData {
    if(_pageModel.hasNext){
        _pageModel.willLoadMore=YES;
        [self sendRequest];
    }else{
        [self.rightTableView.mj_footer endRefreshing];
        [self.rightTableView.mj_footer endRefreshingWithNoMoreData];
    }
}

/**
 * @author: zjr
 * @date: 2022-7-28
 * @desc: 开服表
 */
- (void)sendRequest{
    QJProductRequest *productReq = [[QJProductRequest alloc] init];
    NSMutableDictionary *muDic = [NSMutableDictionary dictionaryWithDictionary:_pageModel.toParams];
    [muDic setValue:_detailModel.startTime?_detailModel.startTime:[NSString getStringForDate:[NSDate date] format:@"yyyy-MM-dd HH:mm:ss"] forKey:@"startTime"];
    if (_detailModel.ID) {
        [muDic setValue:_detailModel.ID forKey:@"id"];
    }
    productReq.dic = muDic.copy;
    [productReq getProductTimeLineRequest];

    WS(weakSelf)
    productReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            [weakSelf.pageModel configObj:[QJSearchGameListModel modelWithDictionary:EncodeDicFromDic(request.responseObject, @"data")]];
        }else{
            NSString *msg = EncodeStringFromDic(request.responseObject, @"message");
            [weakSelf makeToast:msg];
        }
        [weakSelf.rightTableView reloadData];
        [weakSelf.rightTableView.mj_footer endRefreshing];
    };
    productReq.failureCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        [weakSelf.rightTableView reloadData];
        [weakSelf.rightTableView.mj_footer endRefreshing];
        [weakSelf makeToast:@"数据异常 请稍后再试"];
    };
}

@end
