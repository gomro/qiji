//
//  QJBoxCenterCell.h
//  QJBox
//
//  Created by rui on 2022/7/25.
//

#import "QJBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJBoxCenterCell : QJBaseTableViewCell

/* 是否能够滚动 */
@property (nonatomic, assign) BOOL canScroll;

@end

NS_ASSUME_NONNULL_END
