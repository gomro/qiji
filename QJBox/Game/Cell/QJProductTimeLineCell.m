//
//  QJProductTimeLineCell.m
//  QJBox
//
//  Created by rui on 2022/7/28.
//

#import "QJProductTimeLineCell.h"
#import "QJSearchFilterCustomLayout.h"

/**
 * @author: zjr
 * @date: 2022-8-2
 * @desc: 自定义时间轴collectioncell
 */
@interface QJProductTimeLineCollectionCell : UICollectionViewCell

/* 开服表名字 */
@property (nonatomic, strong) UILabel *nameLabel;
/* 开服状态 */
@property (nonatomic, strong) UILabel *statusLabel;

@end

@implementation QJProductTimeLineCollectionCell

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        /* 初始化UI */
        [self initUI];
        /* 添加约束 */
        [self makeConstraints];
    }
    return self;
}

/**
 * @author: zjr
 * @date: 2022-7-5
 * @desc: 初始化UI
 */
- (void)initUI{
    [self.contentView addSubview:self.nameLabel];
    [self.contentView addSubview:self.statusLabel];
}

/**
 * @author: zjr
 * @date: 2022-7-5
 * @desc: 添加约束
 */
- (void)makeConstraints{
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@15);
        make.top.equalTo(self.contentView);
        make.left.equalTo(self.contentView).offset(5);
        make.right.equalTo(self.contentView).offset(-5);
        make.centerX.equalTo(self.contentView);
    }];
    
    [self.statusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.nameLabel.mas_bottom).offset(6);
        make.left.equalTo(self.contentView).offset(5);
        make.right.equalTo(self.contentView).offset(-5);
        make.height.equalTo(@14);
    }];
}

#pragma mark - Lazy Loading
- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [UILabel new];
        _nameLabel.font = kFont(14);
        _nameLabel.textColor = [UIColor colorWithHexString:@"0x000000"];
        _nameLabel.textAlignment = NSTextAlignmentLeft;
        _nameLabel.text = @"";
    }
    return _nameLabel;
}

- (UILabel *)statusLabel {
    if (!_statusLabel) {
        _statusLabel = [UILabel new];
        _statusLabel.font = kFont(12);
        _statusLabel.textColor = [UIColor colorWithHexString:@"0x000000"];
        _statusLabel.textAlignment = NSTextAlignmentLeft;
        _statusLabel.text = @"";
    }
    return _statusLabel;
}

@end

@interface QJProductTimeLineCell ()<UICollectionViewDelegate,UICollectionViewDataSource>

/* 时间 */
@property (nonatomic, strong) UILabel *timeLabel;
@property (nonatomic, strong) UILabel *todayLabel;
/* 开服名 */
@property (nonatomic, strong) UICollectionView *serviceCollection;
/* 内容 */
@property (nonatomic, strong) UILabel *statusLabel;
/* 查看更多 */
@property (nonatomic, strong) UIButton *lookButton;
/* 时间轴 */
@property (nonatomic, strong) UIView *topLineView;
@property (nonatomic, strong) UIView *lineView;
@property (nonatomic, strong) UIImageView *timeImageView;

@end

@implementation QJProductTimeLineCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.contentView.backgroundColor = [UIColor whiteColor];
        /* 初始化UI */
        [self initUI];
        /* 添加约束 */
        [self makeConstraints];
    }
    return self;
}

/**
 * @author: zjr
 * @date: 2022-7-7
 * @desc: 初始化UI
 */
- (void)initUI{
    [self.contentView addSubview:self.timeLabel];
    [self.contentView addSubview:self.todayLabel];
    [self.contentView addSubview:self.serviceCollection];
    [self.contentView addSubview:self.lookButton];
    [self.contentView addSubview:self.topLineView];
    [self.contentView addSubview:self.lineView];
    [self.contentView addSubview:self.timeImageView];
}

/**
 * @author: zjr
 * @date: 2022-7-7
 * @desc: 添加约束
 */
- (void)makeConstraints{
    [self.topLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.contentView);
        make.width.mas_equalTo(3);
        make.height.mas_equalTo(16);
        make.left.mas_equalTo(self.contentView).mas_offset(95);
    }];
    
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.topLineView.mas_bottom);
        make.bottom.mas_equalTo(self.contentView);
        make.width.mas_equalTo(3);
        make.left.mas_equalTo(self.contentView).mas_offset(95);
    }];
    
    [self.timeImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.contentView).offset(16);
        make.centerX.mas_equalTo(self.lineView);
        make.width.mas_equalTo(10);
        make.height.mas_equalTo(10);
    }];
    
    [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.contentView);
        make.left.mas_equalTo(self.contentView);
        make.width.mas_equalTo(95);
    }];

    [self.todayLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.timeLabel.mas_bottom).mas_offset(5);
        make.left.mas_equalTo(self.contentView);
        make.width.mas_equalTo(95);
    }];

    [self.lookButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.contentView).mas_offset(10);
        make.right.mas_equalTo(self.contentView).mas_offset(-18);
    }];
    
    [self.serviceCollection mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(self.contentView);
        make.left.equalTo(self.lineView.mas_right).mas_offset(18);
        make.right.equalTo(self.lookButton.mas_left).mas_offset(-10);
        make.height.mas_equalTo(40);
    }];    
}

/**
 * @author: zjr
 * @date: 2022-8-2
 * @desc: model赋值
 */
- (void)setDetailModel:(QJSearchGameDetailModel *)detailModel{
    if (_detailModel != detailModel) {
        _detailModel = detailModel;
        
        _lookButton.hidden = YES;
        _lookButton.userInteractionEnabled = NO;
        if (_detailModel.indexRow == 0) {
            _lookButton.hidden = NO;
            _lookButton.userInteractionEnabled = YES;
        }
        self.timeLabel.text = kCheckStringNil(_detailModel.openTime);
        if (_detailModel.serverOpen) {
            self.timeImageView.image = [UIImage imageNamed:@"qj_product_timeline_yellow"];
            self.timeLabel.textColor = UIColorFromRGB(0xFF9500);
            self.todayLabel.textColor = UIColorFromRGB(0xFF9500);
            if (_detailModel.isShowLastOpen && _detailModel.serverOpen) {
                self.topLineView.backgroundColor = UIColorFromRGB(0xFF9500);
            }else{
                self.topLineView.backgroundColor = UIColorFromRGB(0x919599);
            }
            if (_detailModel.isShowNextOpen && _detailModel.serverOpen) {
                self.lineView.backgroundColor = UIColorFromRGB(0xFF9500);
            }else{
                self.lineView.backgroundColor = UIColorFromRGB(0x919599);
            }
        }else{
            self.timeImageView.image = [UIImage imageNamed:@"qj_product_timeline_gray"];
            self.timeLabel.textColor = UIColorFromRGB(0x919599);
            self.todayLabel.textColor = UIColorFromRGB(0x000000);
            self.topLineView.backgroundColor = UIColorFromRGB(0x919599);
            self.lineView.backgroundColor = UIColorFromRGB(0x919599);
        }
        
        [self.serviceCollection reloadData];
        [self.serviceCollection layoutIfNeeded];
        DLog(@"array.count = %ld",_detailModel.serverName.count);
        [self.serviceCollection mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(self.serviceCollection.contentSize.height);
        }];
    }
}

#pragma mark - Lazy Loading
- (UIImageView *)timeImageView {
    if (!_timeImageView) {
        _timeImageView = [UIImageView new];
    }
    return _timeImageView;
}

- (UILabel *)timeLabel {
    if (!_timeLabel) {
        _timeLabel = [UILabel new];
        _timeLabel.font = kFont(16);
        _timeLabel.textColor = UIColorFromRGB(0xFF9500);
        _timeLabel.textAlignment = NSTextAlignmentCenter;
        _timeLabel.text = @"";
    }
    return _timeLabel;
}

- (UILabel *)todayLabel {
    if (!_todayLabel) {
        _todayLabel = [UILabel new];
        _todayLabel.font = kFont(9);
        _todayLabel.textColor = [UIColor blackColor];
        _todayLabel.textAlignment = NSTextAlignmentCenter;
        _todayLabel.text = @"今日";
    }
    return _todayLabel;
}

- (UIView *)topLineView{
    if (!_topLineView) {
        _topLineView = [UIView new];
        _topLineView.backgroundColor = UIColorFromRGB(0xFF9500);
    }
    return _topLineView;
}

- (UIView *)lineView{
    if (!_lineView) {
        _lineView = [UIView new];
        _lineView.backgroundColor = UIColorFromRGB(0x919599);
    }
    return _lineView;
}

- (UICollectionView *)serviceCollection{
    if (!_serviceCollection) {
        QJSearchFilterCustomLayout *layout = [[QJSearchFilterCustomLayout alloc]init];
        layout.alignment = FlowAlignmentLeft;
        layout.minimumLineSpacing = 0;
        layout.minimumInteritemSpacing = 0;
        _serviceCollection = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, QJScreenWidth-16*2, 95) collectionViewLayout:layout];
        [_serviceCollection registerClass:[QJProductTimeLineCollectionCell class] forCellWithReuseIdentifier:@"QJProductTimeLineCollectionCell"];
        _serviceCollection.delegate = self;
        _serviceCollection.dataSource = self;
        _serviceCollection.backgroundColor = [UIColor clearColor];
        _serviceCollection.showsVerticalScrollIndicator = NO;
        _serviceCollection.showsHorizontalScrollIndicator = NO;
    }
    return _serviceCollection;
}

- (UIButton *)lookButton {
    if (!_lookButton) {
        _lookButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_lookButton addTarget:self action:@selector(btnAction) forControlEvents:UIControlEventTouchUpInside];
        [_lookButton setTitleColor:UIColorFromRGB(0x919599) forState:UIControlStateNormal];
        _lookButton.titleLabel.font = kFont(12);
        [_lookButton setTitle:@"查看" forState:UIControlStateNormal];
    }
    return _lookButton;
}

- (void)btnAction{
    if (self.buttonClick) {
        self.buttonClick();
    }
}

#pragma mark  -------- UICollectionViewDelegate,UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _detailModel.serverName.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    QJProductTimeLineCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"QJProductTimeLineCollectionCell" forIndexPath:indexPath];
    cell.nameLabel.text = [_detailModel.serverName safeObjectAtIndex:indexPath.item];
    if (_detailModel.serverOpen) {
        cell.statusLabel.text = @"已开服";
        cell.nameLabel.textColor = UIColorFromRGB(0xFF9500);
        cell.statusLabel.textColor = UIColorFromRGB(0xFF9500);
    }else{
        cell.statusLabel.text = @"未开服";
        cell.nameLabel.textColor = UIColorFromRGB(0x919599);
        cell.statusLabel.textColor = UIColorFromRGB(0x919599);
    }
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger num = 2;
    CGFloat width = CGRectGetWidth(collectionView.bounds)/num;
    CGFloat height = 50;
    if(indexPath.row == 0){
        //第一列的width比其他列稍大一些，消除item之间的间隙
        CGFloat realWidth = CGRectGetWidth(collectionView.bounds) - floor(width) * (num - 1);
        return CGSizeMake(realWidth, height);
    }else{
        return CGSizeMake(floor(width), height);
    }
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsZero;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 0;
}

@end
