//
//  QJProductIntroWebCell.m
//  QJBox
//
//  Created by rui on 2022/7/28.
//

#import "QJProductIntroWebCell.h"

@interface QJProductIntroWebCell ()<WKUIDelegate,WKNavigationDelegate>

/* webview */
@property (nonatomic, strong) WKWebView *webView;
@property (nonatomic, assign) CGFloat height;

@end

@implementation QJProductIntroWebCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        /* 初始化UI */
        [self initUI];
        /* 添加约束 */
        [self makeConstraints];
    }
    return self;
}

- (void)dealloc{
    [_webView.scrollView removeObserver:self forKeyPath:@"contentSize"];
}

/**
 * @author: zjr
 * @date: 2022-7-7
 * @desc: 初始化UI
 */
- (void)initUI{
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.contentView.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:self.webView];
}

/**
 * @author: zjr
 * @date: 2022-7-7
 * @desc: 添加约束
 */
- (void)makeConstraints{
    [self.webView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(self.contentView);
        make.left.equalTo(self.contentView).offset(16);
        make.right.equalTo(self.contentView).offset(-16);
        make.height.mas_equalTo(40);
    }];
}

/**
 * @author: zjr
 * @date: 2022-8-2
 * @desc: 数据源赋值
 */
- (void)setDetailModel:(QJSearchGameDetailModel *)detailModel{
    if (_detailModel != detailModel) {
        _detailModel = detailModel;
        NSString *headerString = @"<header><meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no'></header>";
        [self.webView loadHTMLString:[headerString stringByAppendingString:kCheckStringNil(_detailModel.gameInfo)] baseURL:nil];
    }
}

#pragma mark - Lazy Loading
- (WKWebView *)webView{
    if (!_webView) {
        _webView = [[WKWebView alloc] initWithFrame:CGRectZero];
        _webView.navigationDelegate = self;
        _webView.UIDelegate=self;
        _webView.scrollView.scrollEnabled = NO;
        _webView.backgroundColor = [UIColor whiteColor];
        [_webView.scrollView addObserver:self forKeyPath:@"contentSize" options:NSKeyValueObservingOptionNew context:nil];
    }
    return _webView;
}

/**
 * @author: zjr
 * @date: 2022-8-2
 * @desc: 观察者
 */
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context{
    CGFloat newHeight = _webView.scrollView.contentSize.height;
    if (self.height&&newHeight==self.height) {
        return;
    }
    self.height=newHeight;
    _webView.frame=CGRectMake(0, 0, QJScreenWidth, newHeight);
    [self.webView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(newHeight);
    }];
    self.webView.height = newHeight;
    if (self.delegate && [self.delegate respondsToSelector:@selector(changeHeightWith:)]) {
        [self.delegate changeHeightWith:newHeight];
    }
}

#pragma mark WKWebView
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    NSString *js1 = [NSString stringWithFormat:@"document.documentElement.style.webkitUserSelect='none';"];
    NSString *js2 = [NSString stringWithFormat:@"document.documentElement.style.webkitTouchCallout='none';"];
    [self.webView evaluateJavaScript:js1 completionHandler:nil];
    [self.webView evaluateJavaScript:js2 completionHandler:nil];

//    [webView evaluateJavaScript:@"document.body.scrollHeight;" completionHandler:^(id _Nullable result, NSError * _Nullable error) {
//        CGFloat webH = [result floatValue];
//        DLog(@"webH = %f",webH);
//        [self.webView layoutIfNeeded];
//        [self.webView mas_updateConstraints:^(MASConstraintMaker *make) {
//            make.height.mas_equalTo(webH);
//        }];
//    }];
    
//    [webView evaluateJavaScript:@"document.body.scrollWidth" completionHandler:^(id _Nullable result,NSError *_Nullable error) {
//    //获取页面宽度
//    //在全局属性定义一个宽度
//        CGFloat webH= [result doubleValue];
//        NSLog(@"scrollWidth 即为所求：%ff", webH);
//        [webView evaluateJavaScript:@"document.body.scrollHeight"completionHandler:^(id _Nullable result,NSError*_Nullable error) {
//            //获取页面高度
//            CGFloat scrollHeight = [result doubleValue];
//            //计算出缩放比，屏幕宽除以webview宽
//            CGFloat ratio =  CGRectGetWidth(self.webView.frame) /webH;
//            //此处就能求出页面缩放比例后的高度
//            //取到的高度在乘以缩放比即可得到准确高度
//            double webHeight = scrollHeight * ratio;
//            NSLog(@"webView 即为所求：%ff", webHeight);
//            [self.webView mas_updateConstraints:^(MASConstraintMaker *make) {
//                make.height.mas_equalTo(webH);
//            }];
//            self.webView.height = webHeight;
////            [strongSelf.tableView reloadData];
//        }];
//    }];
}

- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(nonnull WKNavigationAction *)navigationAction decisionHandler:(nonnull void (^)(WKNavigationActionPolicy))decisionHandler {
//    NSString *requestString = [navigationAction.request.URL.absoluteString stringByRemovingPercentEncoding];
//    NSArray *components = [requestString componentsSeparatedByString:@":"];
    decisionHandler(WKNavigationActionPolicyAllow);
}

@end
