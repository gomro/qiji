//
//  QJProductTitleContentCell.h
//  QJBox
//
//  Created by rui on 2022/9/22.
//

#import <UIKit/UIKit.h>
#import "QJSearchGameListModel.h"

NS_ASSUME_NONNULL_BEGIN

@protocol QJProductTitleContentWebCellDelegate <NSObject>

/* delegate回调高度 */
@optional
- (void)changeContentHeightWith:(CGFloat)height;

@end

@interface QJProductTitleContentCell : UITableViewCell

/* 数据源 */
@property (nonatomic, strong) QJSearchGameDetailModel *detailModel;
/* 回调代理 */
@property (nonatomic, weak) id<QJProductTitleContentWebCellDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
