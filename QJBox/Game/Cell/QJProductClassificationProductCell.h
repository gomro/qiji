//
//  QJProductClassificationProductCell.h
//  QJBox
//
//  Created by wxy on 2022/7/25.
//

#import "QJBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN
@class  QJSearchGameDetailModel;
/// 游戏分类游戏cell
@interface QJProductClassificationProductCell : QJBaseTableViewCell


 

@property (nonatomic, strong) QJSearchGameDetailModel *model;


@end

NS_ASSUME_NONNULL_END
