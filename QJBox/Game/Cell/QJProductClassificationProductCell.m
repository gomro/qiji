//
//  QJProductClassificationProductCell.m
//  QJBox
//
//  Created by wxy on 2022/7/25.
//

#import "QJProductClassificationProductCell.h"
#import "QJProductCommonLabel.h"
#import "QJSearchGameListModel.h"
#import "QJHomeSaveToMyListRequest.h"
#define kViewT 10000
@interface QJProductClassificationProductCell ()

@property (nonatomic, strong) UIView *whiteBgView;//白色底图

@property (nonatomic, strong) UIImageView *productImageView;

@property (nonatomic, strong) UILabel *titleLabel;//标题

@property (nonatomic, strong) UILabel *desLabel;//描述

@property (nonatomic, strong) UIButton *rightBtn;//右侧按钮

@property (nonatomic, strong) NSMutableArray *viewArray;//管理标签

@property (nonatomic, strong) QJHomeSaveToMyListRequest *saveTolistReq;
@end



@implementation QJProductClassificationProductCell





- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self addsubViews];
    }
    return self;
}



#pragma mark ---------- method

- (void)addsubViews {
    self.contentView.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:self.whiteBgView];
    [self.whiteBgView addSubview:self.productImageView];
    [self.whiteBgView addSubview:self.titleLabel];
    
    [self.whiteBgView addSubview:self.desLabel];
    [self.whiteBgView addSubview:self.rightBtn];
    
    [self.whiteBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(0);
        make.right.equalTo(self.contentView).offset(0);
        make.top.equalTo(self.contentView).offset(0);
        make.bottom.equalTo(self.contentView).offset(0);
    }];
    
    [self.productImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.whiteBgView).offset(12);
        make.top.equalTo(self.whiteBgView).offset(15);
        make.width.height.equalTo(@(56*kWScale));
        
    }];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.productImageView).offset(2);
        make.left.equalTo(self.productImageView.mas_right).offset(6);
        make.height.equalTo(@(15*kWScale));
        make.right.equalTo(self.rightBtn.mas_left).offset(-(Get375Width(16)));
    }];
    
    
    [self.desLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.titleLabel);
        make.top.equalTo(self.titleLabel.mas_bottom).offset(4);
        make.right.equalTo(self.rightBtn.mas_left).offset(-8);
        make.height.equalTo(@(Get375Width(14)));
    }];
    
    
    
    
    [self.rightBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.productImageView);
        make.width.equalTo(@(54*kWScale));
        make.height.equalTo(@(24*kWScale));
        make.right.equalTo(self.whiteBgView).offset(-12);
    }];
    
    
}

#pragma mark  ------ getter

- (UIView *)whiteBgView {
    if (!_whiteBgView) {
        _whiteBgView = [UIView new];
        _whiteBgView.backgroundColor = [UIColor whiteColor];
         
        _whiteBgView.layer.masksToBounds = YES;
    }
    return _whiteBgView;
}

- (UIImageView *)productImageView {
    if (!_productImageView) {
        _productImageView = [UIImageView new];
        _productImageView.layer.cornerRadius = 8;
        _productImageView.layer.masksToBounds = YES;
       
        
    }
    return _productImageView;
}


- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.font = FontRegular(14);
        _titleLabel.textColor = [UIColor colorWithHexString:@"#000000"];
        _titleLabel.textAlignment = NSTextAlignmentLeft;
        
    }
    return _titleLabel;
}


 


- (UILabel *)desLabel {
    if (!_desLabel) {
        _desLabel = [UILabel new];
        _desLabel.font = FontRegular(10);
        _desLabel.textColor = [UIColor colorWithHexString:@"#919599"];
        _desLabel.textAlignment = NSTextAlignmentLeft;
        
    }
    return _desLabel;
}

- (NSMutableArray *)viewArray {
    if (!_viewArray) {
        _viewArray = [NSMutableArray array];
    }
    return _viewArray;
}

- (UIButton *)rightBtn {
    if (!_rightBtn) {
        _rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_rightBtn addTarget:self action:@selector(btnAction:) forControlEvents:UIControlEventTouchUpInside];
        _rightBtn.titleLabel.font = FontSemibold(12);
        [_rightBtn setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithHexString:@"#ffffff"]] forState:UIControlStateNormal];
        [_rightBtn setTitle:@"获取" forState:UIControlStateNormal];
        [_rightBtn setTitleColor:kColorWithHexString(@"#1F2A4D") forState:UIControlStateNormal];
        _rightBtn.layer.cornerRadius = 4;
        _rightBtn.layer.borderWidth = 1;
        _rightBtn.layer.borderColor = kColorWithHexString(@"000000").CGColor;
        _rightBtn.layer.masksToBounds = YES;
        
    }
    return _rightBtn;
}

- (QJHomeSaveToMyListRequest *)saveTolistReq {
    if (!_saveTolistReq) {
        _saveTolistReq = [QJHomeSaveToMyListRequest new];
    }
    return _saveTolistReq;
}


- (void)setModel:(QJSearchGameDetailModel *)model {
    _model = model;
        self.titleLabel.text = model.name?:@"";
        self.productImageView.imageURL = [NSURL URLWithString:[model.icon.length > 0 ?model.icon:model.thumbnail checkImageUrlString]];
        self.desLabel.text = [NSString stringWithFormat:@"%@人在线",model.onlineCount];
        for (UIView *view in self.viewArray.copy) {
            view.hidden = YES;
        }
        int i = 0;
        for (QJSearchGameDetailModel *tagModel in model.tags) {
            i++;
            QJProductCommonLabel *view = [self.whiteBgView viewWithTag:(kViewT + i)];
            if (!view) {
                view = [QJProductCommonLabel new];
                view.tag = kViewT + i;
            }
            view.hidden = NO;
            view.text = tagModel.name;
            view.textColor = [UIColor colorWithHexString:tagModel.color];
            view.backgroundColor = [UIColor colorWithHexString:tagModel.backgroundColor];
            [self.whiteBgView addSubview:view];
            if (![self.viewArray containsObject:view]) {
                [self.viewArray safeAddObject:view];
            }
            //布局
            if (i == 1) {
                [view mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.equalTo(self.titleLabel);
                    make.top.equalTo(self.desLabel.mas_bottom).offset(Get375Width(4));
                }];
            }else {
                UIView *leftView = [self.whiteBgView viewWithTag:(kViewT + i-1)];
                [view mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.equalTo(leftView.mas_right).offset(6);
                    make.centerY.equalTo(leftView);
                }];
            }

        }

  

}

#pragma mark -------- action

- (void)btnAction:(UIButton *)sender {
    DLog(@"启动");
    UIViewController *vc = [QJAppTool getCurrentViewController];
    [vc checkLogin:^(BOOL isLogin) {
        if(isLogin){
            DLog(@"启动");
            NSString *idStr = @"";
            NSString *iPkgModes = @"";
            NSString *iUrls = @"";
            NSString *iCusInsUrls = @"";
        
            if (self.model) {
                idStr = self.model.ID;
                iPkgModes = self.model.iPkgMode;
                iUrls = self.model.iUrl;
                iCusInsUrls = self.model.iCusInsUrl;
            }
            self.saveTolistReq.dic = @{@"id":idStr};
            [self.saveTolistReq start];
            
            if ([iPkgModes isEqualToString:@"0"]) {//0-appstore 1-custom
                if (iUrls.length > 0) {
                    NSURL *iUrl = [NSURL URLWithString:iUrls];
                    if ([[UIApplication sharedApplication] canOpenURL:iUrl]) {
                        [[UIApplication sharedApplication] openURL:iUrl options:@{} completionHandler:nil];
                    }
                }
                
            }else if ([iPkgModes isEqualToString:@"1"]) {//0-appstore 1-custom
                if (iCusInsUrls.length > 0) {
                    NSURL *iCusInsUrl = [NSURL URLWithString:iCusInsUrls];
                    if ([[UIApplication sharedApplication] canOpenURL:iCusInsUrl]) {
                        [[UIApplication sharedApplication] openURL:iCusInsUrl options:@{} completionHandler:nil];
                    }
                }
               
            }
            
           
        }
    }];
 
  
    
}







@end
