//
//  QJBoxCenterCell.m
//  QJBox
//
//  Created by rui on 2022/7/25.
//

#import "QJBoxCenterCell.h"
/* 游戏公共cell */
#import "QJGameCommonCell.h"
/* model */
#import "QJSearchGameListModel.h"
/* 请求 */
#import "QJProductPostRequest.h"
/* 详情页面 */
#import "QJProductDetailViewController.h"

@interface QJBoxCenterCell ()<UITableViewDelegate, UITableViewDataSource>
/* 底部scroll */
@property (nonatomic, strong) UIScrollView *bgScrollView;
/* 开服表table */
@property (nonatomic, strong) UITableView *leftTableView;
/* 开服表model */
@property (nonatomic, strong) QJSearchGameListModel *pageModel;
/* 分割数据源数组 */
@property (nonatomic, strong) NSArray *allDataArr;
/* 第一次刷新数据 */
@property (nonatomic, assign) BOOL isRefreshTime;

@property (nonatomic, strong) NSString *startTime;
@property (nonatomic, strong) NSString *serverTime;

@end

@implementation QJBoxCenterCell

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self.contentView addSubview:self.bgScrollView];
        self.isRefreshTime = YES;
        self.startTime = @"";
        self.pageModel = [QJSearchGameListModel new];
        self.allDataArr = [NSMutableArray array];
        [self.bgScrollView addSubview:self.leftTableView];
        
        [self.bgScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.contentView);
        }];
        
        [self.leftTableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.bgScrollView);
            make.height.equalTo(self.bgScrollView);
            make.width.equalTo(@(kScreenWidth));
            make.top.equalTo(self.bgScrollView);
        }];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeStatus) name:QJProductNestMainCellScrollNotification object:nil];//外部通知这里可以滑动
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changePageCount) name:QJProductRefreshAllDataNotification object:nil];//外部下拉刷新这里把内部的网络请求的page改为1
    }
    return self;
}

/**
 * @author: zjr
 * @date: 2022-8-2
 * @desc: 通知方法
 */
- (void)changeStatus{
    self.canScroll = YES;
}

- (void)changePageCount {
    [self refreshData];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.bgScrollView.contentSize = CGSizeMake(kScreenWidth, self.height);
}

#pragma mark ------- Lazy Loading
- (UIScrollView *)bgScrollView {
    if (!_bgScrollView) {
        _bgScrollView = [[UIScrollView alloc]init];
        _bgScrollView.pagingEnabled = YES;
        _bgScrollView.bounces = NO;
        _bgScrollView.delegate = self;
        _bgScrollView.showsHorizontalScrollIndicator = NO;
    }
    return _bgScrollView;
}

- (UITableView *)leftTableView {
    if (!_leftTableView) {
        _leftTableView = [[UITableView alloc]initWithFrame:self.contentView.frame style:UITableViewStyleGrouped];
        _leftTableView.dataSource = self;
        _leftTableView.delegate = self;
        _leftTableView.backgroundColor = [UIColor whiteColor];
        [_leftTableView registerClass:[QJGameCommonCell class] forCellReuseIdentifier:@"QJBOXServerCell"];
        _leftTableView.showsVerticalScrollIndicator = NO;
        _leftTableView.mj_footer = [QJRefreshFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreHomeData)];
//        _leftTableView.mj_header = [QJRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(refreshData)];

//        _leftTableView.mj_footer = [QJRefreshFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreHomeData)];
        if (@available(iOS 11.0, *)) {
            _leftTableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
            _leftTableView.estimatedRowHeight = 0;
            _leftTableView.estimatedSectionFooterHeight = 0;
            _leftTableView.estimatedSectionHeaderHeight = 0;
        }
    }
    return _leftTableView;
}

#pragma mark - 请求方法
- (void)refreshData {
    self.isRefreshTime = YES;
    self.startTime = @"";
    _pageModel.willLoadMore = NO;
    [self sendRequest];
//    _leftTableView.mj_footer = [QJRefreshFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreHomeData)];
}

- (void)loadMoreHomeData {
    self.isRefreshTime = NO;
    if(_pageModel.hasNext){
        _pageModel.willLoadMore=YES;
        [self sendRequest];
    }else{
        [self.leftTableView.mj_footer endRefreshing];
        [self.leftTableView.mj_footer endRefreshingWithNoMoreData];
    }
}

/**
 * @author: zjr
 * @date: 2022-7-7
 * @desc: 搜索游戏
 */
- (void)sendRequest{
    QJProductPostRequest *productReq = [[QJProductPostRequest alloc] init];
    NSMutableDictionary *muDic = [NSMutableDictionary dictionaryWithDictionary:_pageModel.toParams];
    [muDic setValue:self.startTime?self.startTime:[NSString getStringForDate:[NSDate date] format:@"yyyy-MM-dd HH:mm:ss"] forKey:@"startTime"];
    productReq.dic = muDic.copy;
    [productReq getProductListRequest];

    WS(weakSelf)
    productReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            [weakSelf.pageModel configObj:[QJSearchGameListModel modelWithDictionary:EncodeDicFromDic(request.responseObject, @"data")]];
            weakSelf.allDataArr = [weakSelf filterMaxItemsArray:weakSelf.pageModel.records filterKey:@"serverTime"];
            if (weakSelf.isRefreshTime) {
                if (self.allDataArr > 0) {
                    NSMutableArray *records = [self.allDataArr safeObjectAtIndex:0];
                    QJSearchGameDetailModel *model = [records safeObjectAtIndex:0];
                    weakSelf.startTime = model.startTime;
                    weakSelf.serverTime = model.serverTime;
                    DLog(@"startTime = %@",model.startTime);
                    [[NSNotificationCenter defaultCenter] postNotificationName:QJProductRefreshServerTimeNotification object:model];//到顶通知父视图改变状态
                }
            }
        }else{
            NSString *msg = EncodeStringFromDic(request.responseObject, @"message");
            [weakSelf makeToast:msg];
        }
        [weakSelf.leftTableView reloadData];
        [weakSelf.leftTableView.mj_footer endRefreshing];
        QJEmptyCommonView *empty = [QJEmptyCommonView new];
        empty.string = @"还没有任何开服游戏哦～";
        [weakSelf.leftTableView tableViewDisplayWhenHaveNoDataWithView:empty ifNecessaryForRowCount:weakSelf.allDataArr.count];
    };
    productReq.failureCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        [weakSelf.leftTableView reloadData];
        [weakSelf.leftTableView.mj_footer endRefreshing];
        [weakSelf makeToast:@"数据异常 请稍后再试"];
        QJEmptyCommonView *empty = [QJEmptyCommonView new];
        empty.string = @"还没有任何开服游戏哦～";
        [weakSelf.leftTableView tableViewDisplayWhenHaveNoDataWithView:empty ifNecessaryForRowCount:weakSelf.allDataArr.count];
    };
}

#pragma mark ------- UITableViewDelegate, UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (self.allDataArr.count > 0) {
        return self.allDataArr.count;
    }
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSMutableArray *records = [self.allDataArr safeObjectAtIndex:section];
    if (records.count > 0) {
        return records.count;
    }
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    NSMutableArray *records = [self.allDataArr safeObjectAtIndex:section];
    QJSearchGameDetailModel *model = [records safeObjectAtIndex:0];
    if (model.serverOpen == YES || section == 0) {
        return nil;
    }
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 40)];
    headerView.backgroundColor = [UIColor whiteColor];
    
    UILabel *timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, kScreenWidth-30, 40)];
    timeLabel.text = model.serverTime;
    timeLabel.textColor = UIColorFromRGB(0x000000);
    timeLabel.font = FontSemibold(19);
    [headerView addSubview:timeLabel];
    [timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(headerView);
        make.centerX.equalTo(headerView).offset(-30);
    }];
    
    UILabel *statusLabel = [UILabel new];
    statusLabel.font = kFont(12);
    statusLabel.textColor = UIColorFromRGB(0x474849);
    statusLabel.textAlignment = NSTextAlignmentCenter;
    statusLabel.layer.cornerRadius = 4;
    statusLabel.layer.masksToBounds = YES;
    statusLabel.text = @"未开服";
    [headerView addSubview:statusLabel];
    [statusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(timeLabel.mas_bottom).offset(-4);
        make.height.equalTo(@16);
        make.width.equalTo(@50);
        make.left.equalTo(timeLabel.mas_right).offset(6);
    }];
    
    UIView *leftLineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 57, 1)];
    leftLineView.backgroundColor = UIColorFromRGB(0x000000);
    [headerView addSubview:leftLineView];
    [leftLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(headerView);
        make.right.equalTo(timeLabel.mas_left).offset(-22);
        make.width.equalTo(@57);
        make.height.equalTo(@1);
    }];
    
    UIView *rightLineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 57, 1)];
    rightLineView.backgroundColor = UIColorFromRGB(0x000000);
    [headerView addSubview:rightLineView];
    [rightLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(headerView);
        make.left.equalTo(statusLabel.mas_right).offset(22);
        make.width.equalTo(@57);
        make.height.equalTo(@1);
    }];

    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    NSMutableArray *records = [self.allDataArr safeObjectAtIndex:section];
    QJSearchGameDetailModel *model = [records safeObjectAtIndex:0];
    if (model.serverOpen == YES || section == 0) {
        return 0.001;
    }
    return 40;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.001;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 84;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSMutableArray *records = [self.allDataArr safeObjectAtIndex:indexPath.section];
    QJSearchGameDetailModel *model = [records safeObjectAtIndex:indexPath.row];
    QJGameCommonCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QJBOXServerCell" forIndexPath:indexPath];
    cell.model = model;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSMutableArray *records = [self.allDataArr safeObjectAtIndex:indexPath.section];
    QJSearchGameDetailModel *model = [records safeObjectAtIndex:indexPath.row];
    QJProductDetailViewController *detailVC = [[QJProductDetailViewController alloc] init];
    detailVC.model = model;
    [[QJAppTool getCurrentViewController].navigationController pushViewController:detailVC animated:YES];
}

#pragma mark  -----UIScrollViewDelegate
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGFloat offsetY = scrollView.contentOffset.y;
    if (scrollView != self.bgScrollView) {
        if (!self.canScroll) {
            scrollView.contentOffset = CGPointZero;
        }
        if (scrollView.contentOffset.y < 0 ) {
            self.canScroll = NO;
            scrollView.contentOffset = CGPointZero;
            [[NSNotificationCenter defaultCenter] postNotificationName:QJProductNestCellScrollNotification object:nil];//到顶通知父视图改变状态
        }
        if (scrollView.contentOffset.y > 0) {
            for (NSInteger i = 0; i < self.allDataArr.count; i ++) {
                CGFloat offset = [self.leftTableView rectForSection:i].origin.y;
                CGFloat height = [self.leftTableView rectForSection:i].size.height;
                if (offsetY >= offset && offsetY <= offset + height) {
                    NSMutableArray *records = [self.allDataArr safeObjectAtIndex:i];
                    QJSearchGameDetailModel *model = [records safeObjectAtIndex:0];
                    if (![self.serverTime isEqualToString:model.serverTime]) {
                        self.serverTime = model.serverTime;
                        [[NSNotificationCenter defaultCenter] postNotificationName:QJProductRefreshServerTimeNotification object:model];
                    }
                }
            }
        }
    }else{
        CGFloat x = scrollView.contentOffset.x;
        if (x == 0) {
            if (self.leftTableView.contentOffset.y == 0) {
                self.leftTableView.contentOffset = CGPointMake(0, 1);
            }
        }
    }
}

/**
 * @author: zjr
 * @date: 2022-8-2
 * @desc: 分割数组
 */
- (NSMutableArray *)filterMaxItemsArray:(NSMutableArray *)array filterKey:(NSString *)key {
    NSMutableArray *origArray = [NSMutableArray arrayWithArray:array];
    NSMutableArray *filerArray = [NSMutableArray array];
    
    while (origArray.count > 0) {
        id obj = origArray.firstObject;
        NSPredicate *predic = nil;
        id value = [obj valueForKey:key];
        predic = [NSPredicate predicateWithFormat:@"self.%@ == %@",key,value];
        
        NSMutableArray *pArray = [origArray filteredArrayUsingPredicate:predic].mutableCopy;
        [filerArray addObject:pArray];
        [origArray removeObjectsInArray:pArray];
    }
    
    return filerArray;
}

@end
