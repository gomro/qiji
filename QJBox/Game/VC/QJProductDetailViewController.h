//
//  QJProductDetailViewController.h
//  QJBox
//
//  Created by rui on 2022/7/21.
//

#import "QJBaseViewController.h"
#import "QJSearchGameListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJProductDetailViewController : QJBaseViewController

@property (nonatomic, strong) QJSearchGameDetailModel *model;
// 游戏ID
@property (nonatomic, strong) NSString *idStr;

@end

NS_ASSUME_NONNULL_END
