//
//  QJProductCenterViewController.m
//  QJBox
//
//  Created by rui on 2022/9/23.
//

#import "QJProductCenterViewController.h"
#import "GKPageScrollView.h"
#import "QJCommunityScrollView.h"

#import "QJProductCenterView.h"
#import "QJCustomScrollView.h"
#import "QJBoxCenterHeaderView.h"
/* 我的游戏列表 */
#import "QJMyBoxListViewController.h"
/* 请求 */
#import "QJProductRequest.h"
/* model */
#import "QJHomeBannerModel.h"
#import "QJSearchGameListModel.h"
/* 游戏分类 */
#import "QJProductClassificationViewController.h"

@interface QJProductCenterViewController ()<GKPageScrollViewDelegate,GKPageTableViewGestureDelegate,UIScrollViewDelegate>

/* 自定义导航View */
@property (nonatomic, strong) UIView *navView;
/* 搜索按钮 */
@property (nonatomic, strong) UIButton *searchButton;
/* 我的游戏 */
//@property (nonatomic, strong) UIButton *myProductButton;
/* 标题 */
@property (nonatomic, strong) UILabel *titleLabel;
/* 背景渐变色 */
@property (nonatomic, strong) QJCustomScrollView *colorView;
/* View */
@property (nonatomic, strong) QJCommunityScrollView  *pageScrollView;
@property (nonatomic, strong) NSMutableArray    *childVCs;
@property (nonatomic, strong) UIScrollView      *scrollView;
@property (nonatomic, strong) UIView            *pageView;
@property (nonatomic, strong) UIView *segment;
@property (nonatomic, strong) NSArray *bannerDataArr;//banner数据
/* 我的游戏数据源 */
@property (nonatomic, strong) NSArray *myProductDataArr;//banner数据
/* 动画view */
@property (nonatomic, strong) UIView *animationView;
/* 开服时间 */
@property (nonatomic, strong) UILabel *timeLabel;
/* 开服状态 */
@property (nonatomic, strong) UILabel *statusLabel;
/* 自定义评价headerView */
@property (nonatomic, strong) QJBoxCenterHeaderView *headerView;
/* GKScrollView回调 */
@property (nonatomic,   copy) void(^listScrollViewDidScroll)(UIScrollView *scrollView);


@end

@implementation QJProductCenterViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    /* 调整自定义导航视图层次 */
    [self.view bringSubviewToFront:self.navView];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    self.pageScrollView.ceilPointHeight = 0;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navBar.hidden = YES;
    /* 加载自定义导航 */
    [self initNav];
    /* 加载UI */
    [self initUI];
    [self refreshData];
}

/**
 * @author: zjr
 * @date: 2022-7-22
 * @desc: 自定义导航
 */
- (void)initNav{
    self.view.backgroundColor = UIColorFromRGB(0xF5F5F5);
    self.navView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, QJScreenWidth, NavigationBar_Bottom_Y)];
    self.navView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.navView];
    
    self.searchButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.searchButton setImage:[UIImage imageNamed:@"icon_search"] forState:UIControlStateNormal];
    [self.searchButton setTitle:@"输入关键词搜索游戏" forState:UIControlStateNormal];
    self.searchButton.frame = CGRectMake(15, [QJDeviceConstant top_iPhoneX_SPACE]+27, QJScreenWidth - 30, 34);
    self.searchButton.titleLabel.font = kFont(14);
    [self.searchButton setTitleColor:UIColorFromRGB(0x919599) forState:UIControlStateNormal];
    self.searchButton.backgroundColor = [UIColor whiteColor];
    [self.searchButton addTarget:self action:@selector(btnAction) forControlEvents:UIControlEventTouchUpInside];
    self.searchButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [self.searchButton setImageEdgeInsets:UIEdgeInsetsMake(0, 10, 0, 10)];
    [self.searchButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 20, 0, -20)];
    [self.navView addSubview:self.searchButton];
    [self.searchButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.navView).offset(15);
        make.right.equalTo(self.navView).offset(-15);
        make.top.equalTo(self.navView).offset([QJDeviceConstant sysStatusBarHeight]+4);
        make.bottom.equalTo(self.navView).offset(-5);
    }];
    [self.searchButton layoutIfNeeded];
    self.searchButton.layer.cornerRadius = self.searchButton.height/2;
    
//    self.myProductButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    [self.myProductButton setImage:[UIImage imageNamed:@"qj_product_shoubing"] forState:UIControlStateNormal];
//    self.myProductButton.frame = CGRectMake(QJScreenWidth-34-15, [QJDeviceConstant top_iPhoneX_SPACE]+27, 34, 34);
//    self.myProductButton.backgroundColor = [UIColor whiteColor];
//    [self.myProductButton addTarget:self action:@selector(productBtnAction) forControlEvents:UIControlEventTouchUpInside];
//    [self.navView addSubview:self.myProductButton];
//    self.myProductButton.hidden = YES;
}

- (void)initUI{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeServerTime:) name:QJProductRefreshServerTimeNotification object:nil];

    self.colorView = [[QJCustomScrollView alloc] initWithFrame:CGRectMake(0, 0, QJScreenWidth, 254*kWScale)];
    [self.view addSubview:self.colorView];
    
    [self.view addSubview:self.pageScrollView];
    [self.pageScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.top.equalTo(self.view).offset(NavigationBar_Bottom_Y);
    }];
    
    [self.pageScrollView addSubview:self.headerView];
    [self.pageScrollView sendSubviewToBack:self.headerView];

    WS(weakSelf)
    self.headerView.btnClick = ^{
        QJMyBoxListViewController *myBoxListVC = [[QJMyBoxListViewController alloc] init];
        [weakSelf.navigationController pushViewController:myBoxListVC animated:YES];
    };
#warning TODO banner点击事件
    self.headerView.bannerClick = ^(QJHomeBannerModel * _Nonnull model) {
        
    };
    
    QJProductCenterView *view = [[QJProductCenterView alloc] initWithFrame:CGRectMake(0, 0, QJScreenWidth, QJScreenHeight - NavigationBar_Bottom_Y)];
    [view refreshData];
    [self.childVCs addObject:view];
    view.requestBlock = ^{
        [weakSelf refreshData];
    };
    [self.childVCs enumerateObjectsUsingBlock:^(UIView *vc, NSUInteger idx, BOOL * _Nonnull stop) {
        [self.scrollView addSubview:vc];
        vc.frame = CGRectMake(idx * QJScreenWidth, 0, self.scrollView.width, self.scrollView.height);
    }];
    _scrollView.contentSize = CGSizeMake(self.childVCs.count * QJScreenWidth, 0);
    
    [self.pageScrollView reloadData];
}

/**
 * @author: zjr
 * @date: 2022-6-28
 * @desc: button点击事件
 */
- (void)btnAction{
    DLog(@"游戏中心搜索");
    QJProductClassificationViewController *vc = [QJProductClassificationViewController new];
    vc.isFromGameCenter = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)productBtnAction{
    QJMyBoxListViewController *myBoxListVC = [[QJMyBoxListViewController alloc] init];
    [self.navigationController pushViewController:myBoxListVC animated:YES];
}

#pragma mark - Request
/**
 * @author: zjr
 * @date: 2022-7-26
 * @desc: 数据请求
 */
- (void)refreshData {
    [self requestBanner];
}

/**
 * @author: zjr
 * @date: 2022-7-25
 * @desc: 游戏banner
 */
- (void)requestBanner{
    QJProductRequest *productReq = [[QJProductRequest alloc] init];
    NSMutableDictionary *muDic = [NSMutableDictionary dictionary];
    [muDic setValue:@"GAME" forKey:@"location"];
    productReq.dic = muDic.copy;
    [productReq getProductBannerRequest];
    
    WS(weakSelf)
    productReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            weakSelf.bannerDataArr = [NSArray modelArrayWithClass:[QJHomeBannerModel class] json:EncodeArrayFromDic(request.responseJSONObject, @"data")];
            weakSelf.headerView.bannerDataArr = weakSelf.bannerDataArr;
            [weakSelf setBackGroundImage];
        }else{
            NSString *msg = EncodeStringFromDic(request.responseObject, @"message");
            [weakSelf.view makeToast:msg];
        }
    };
}

/**
 * @author: zjr
 * @date: 2022-8-2
 * @desc: 设置背景轮播view
 */
- (void)setBackGroundImage{
    NSMutableArray *muArr = [NSMutableArray array];
    for (QJHomeBannerModel *model in self.bannerDataArr) {
        if (!IsStrEmpty(model.backImgUrl)) {
            NSString *str = [model.backImgUrl checkImageUrlString];
            [muArr safeAddObject:str];
        }
    }
    self.colorView.urlImages = muArr.copy;
}

#pragma mark - Lazy Loading
- (QJCommunityScrollView *)pageScrollView {
    if (!_pageScrollView) {
        _pageScrollView = [[QJCommunityScrollView alloc] initWithDelegate:self];
        _pageScrollView.mainTableView.backgroundColor = [UIColor clearColor];
        _pageScrollView.isControlVerticalIndicator = YES;
        _pageScrollView.isAllowListRefresh = YES;
        self.pageScrollView.mainTableView.isAllowCustomHit = YES;
        NSInteger insetTopInt = 220*kWScale;
        self.pageScrollView.contentInsetTop = insetTopInt;
    }
    return _pageScrollView;
}

- (UIView *)pageView {
    if (!_pageView) {
        _pageView = [UIView new];
        _pageView.backgroundColor = [UIColor whiteColor];
        [_pageView addSubview:self.segment];
        [_pageView addSubview:self.scrollView];
    }
    return _pageView;
}

- (UIView *)segment {
    if (!_segment) {
        _segment = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 50*kWScale)];
        _segment.backgroundColor = [UIColor whiteColor];
        UIImageView *titleIconImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"qj_product_service"]];
        [_segment addSubview:titleIconImage];
        [titleIconImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_segment);
            make.left.equalTo(_segment).offset(15);
        }];
        
        UIView *animationView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 50)];
        animationView.backgroundColor = [UIColor whiteColor];
        [_segment addSubview:animationView];
        [animationView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_segment);
            make.height.equalTo(@(50*kWScale));
            make.width.equalTo(@(80*kWScale));
            make.right.equalTo(_segment).offset(-5);
        }];
        self.animationView = animationView;
        
        UILabel *timeLabel = [UILabel new];
        timeLabel.font = kboldFont(18);
        timeLabel.textColor = UIColorFromRGB(0x000000);
        timeLabel.textAlignment = NSTextAlignmentCenter;
        timeLabel.text = @"";
        [self.animationView addSubview:timeLabel];
        [timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.animationView).offset(6);
            make.height.equalTo(@(22*kWScale));
            make.left.right.equalTo(self.animationView);
        }];
        self.timeLabel = timeLabel;
        
        UILabel *statusLabel = [UILabel new];
        statusLabel.font = kFont(12);
        statusLabel.textColor = UIColorFromRGB(0xFFFFFF);
        statusLabel.backgroundColor = UIColorFromRGB(0xFFFFFF);
        statusLabel.textAlignment = NSTextAlignmentCenter;
        statusLabel.layer.cornerRadius = 4;
        statusLabel.layer.masksToBounds = YES;
        statusLabel.text = @"";
        [self.animationView addSubview:statusLabel];
        [statusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(@30);
            make.height.equalTo(@(16*kWScale));
            make.width.equalTo(@(50*kWScale));
            make.centerX.equalTo(self.animationView);
        }];
        self.statusLabel = statusLabel;
        
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 50*kWScale-1, kScreenWidth, 1)];
        lineView.backgroundColor = UIColorFromRGB(0xF5F5F5);
        [_segment addSubview:lineView];
    }
    return _segment;
}

/**
 * @author: zjr
 * @date: 2022-7-26
 * @desc: 通知事件
 */
- (void)changeServerTime:(NSNotification *)notification{
    QJSearchGameDetailModel * model = [notification object];
    if(model){
        [UIView transitionWithView:self.animationView duration:1 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
            dispatch_async(dispatch_get_main_queue(), ^{
                if (model.serverOpen == NO) {
                    self.statusLabel.text = @"未开服";
                    self.statusLabel.textColor = UIColorFromRGB(0x474849);
                    self.statusLabel.backgroundColor = UIColorFromRGB(0xFFFFFF);
                }else{
                    self.statusLabel.text = @"已开服";
                    self.statusLabel.textColor = UIColorFromRGB(0xFFFFFF);
                    self.statusLabel.backgroundColor = UIColorFromRGB(0xFF9500);
                }
                self.timeLabel.text = model.serverTime;
            });
        } completion:^(BOOL finished) {

        }];
//        [self shake];
    }
}

- (void)shake
{
    CGAffineTransform translateLeft = CGAffineTransformScale(CGAffineTransformIdentity, -1.0, 1.0); //沿y轴向左翻
    self.animationView.transform = translateLeft;
    [UIView animateWithDuration:1 delay:0.0 options:UIViewAnimationOptionTransitionFlipFromBottom animations:^{
        self.animationView.transform = translateLeft;
    } completion:^(BOOL finished) {
        if(finished)
        {
            [UIView animateWithDuration:1 delay:0.0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
                self.animationView.transform =CGAffineTransformIdentity;
            } completion:NULL];
        }
    }];
}

- (UIScrollView *)scrollView {
    if (!_scrollView) {
        CGFloat scrollW = self.view.width;
        CGFloat scrollH = QJScreenHeight - 50*kWScale - NavigationBar_Bottom_Y - (49+ Bottom_iPhoneX_SPACE);
        
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 50*kWScale, scrollW, scrollH)];
        _scrollView.backgroundColor = [UIColor whiteColor];
        _scrollView.pagingEnabled = YES;
        _scrollView.directionalLockEnabled = YES;
//        _scrollView.scrollEnabled = NO;
        _scrollView.bounces = NO;
        _scrollView.delegate = self;
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.showsHorizontalScrollIndicator = NO;
    }
    return _scrollView;
}

- (QJBoxCenterHeaderView *)headerView {
    if (!_headerView) {
        _headerView = [[QJBoxCenterHeaderView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 220*kWScale)];
    }
    return _headerView;
}

- (NSMutableArray *)childVCs {
    if (!_childVCs) {
        _childVCs = [NSMutableArray array];
    }
    return _childVCs;
}

#pragma mark - GKPageScrollView
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

- (BOOL)mainTableViewGestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

- (UIView *)headerViewInPageScrollView:(GKPageScrollView *)pageScrollView {
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 0)];
    return header;
}

- (NSArray<id<GKPageListViewDelegate>> *)listViewsInPageScrollView:(GKPageScrollView *)pageScrollView {
    return self.childVCs;
}

- (UIView *)pageViewInPageScrollView:(GKPageScrollView *)pageScrollView {
    return self.pageView;
}

- (void)listViewDidScrollCallback:(void (^)(UIScrollView * _Nonnull))callback {
    self.listScrollViewDidScroll = callback;
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [self.pageScrollView horizonScrollViewWillBeginScroll];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    [self.pageScrollView horizonScrollViewDidEndedScroll];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    [self.pageScrollView horizonScrollViewDidEndedScroll];
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    !self.listScrollViewDidScroll ? : self.listScrollViewDidScroll(scrollView);
}

- (void)mainTableViewDidScroll:(UIScrollView *)scrollView isMainCanScroll:(BOOL)isMainCanScroll {
    CGFloat offsetY = scrollView.contentOffset.y + 220*kWScale;
    CGFloat alp = offsetY/(220*kWScale);
    /* 导航渐变背景色 */
    if (alp <= 0 ) {
        alp = 0;
    }else if (alp >= 1) {
        alp = 1;
    }
    
    self.navView.backgroundColor = [UIColor colorWithWhite:1 alpha:alp];
    if (alp == 1) {
        self.searchButton.backgroundColor = UIColorFromRGB(0xF5F5F5);
//        [self.searchButton mas_updateConstraints:^(MASConstraintMaker *make) {
//            make.left.equalTo(self.navView).offset(15);
//            make.right.equalTo(self.navView).offset(-15-34-15);
//        }];
//        self.myProductButton.hidden = NO;
    }else{
        self.searchButton.backgroundColor = UIColorFromRGB(0xFFFFFF);
//        [self.searchButton mas_updateConstraints:^(MASConstraintMaker *make) {
//            make.left.equalTo(self.navView).offset(15);
//            make.right.equalTo(self.navView).offset(-15);
//        }];
//        self.myProductButton.hidden = YES;
    }
}

@end
