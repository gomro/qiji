//
//  QJProductClassificationSearchResultVC.h
//  QJBox
//
//  Created by wxy on 2022/7/26.
//

#import "QJBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN
@class QJProductClassificationModel,QJProductClassificationAllTagsOptionModel;
/// 分类页搜索页面
@interface QJProductClassificationSearchResultVC : QJBaseViewController


@property (nonatomic, strong) QJProductClassificationModel *model;


@property (nonatomic, strong) QJProductClassificationAllTagsOptionModel *tagOptionModel;
@end

NS_ASSUME_NONNULL_END
