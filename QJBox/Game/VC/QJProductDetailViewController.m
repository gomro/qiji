//
//  QJProductDetailViewController.m
//  QJBox
//
//  Created by rui on 2022/7/21.
//

#import "QJProductDetailViewController.h"
/* 自定义产品详情header */
#import "QJProductDetailHeaderView.h"
/* segmentview */
#import "QJSegmentLineView.h"
/* 自定义底部 */
#import "QJNestScrollView.h"
#import "QJHomeListTableView.h"
#import "QJProductNestCell.h"

/* 请求 */
#import "QJProductRequest.h"
/* model */
#import "QJSearchGameListModel.h"

@interface QJProductDetailViewController ()<UITableViewDelegate,UITableViewDataSource>

/* 自定义导航View */
@property (nonatomic, strong) UIView *navView;
/* 返回按钮 */
@property (nonatomic, strong) UIButton *backButton;
/* 标题 */
@property (nonatomic, strong) UILabel *titleLabel;
/* 头部view */
@property (nonatomic, strong) QJProductDetailHeaderView *headerView;
@property (nonatomic, strong) QJSegmentLineView *segment;
/* 主table view */
@property (nonatomic, strong) QJHomeListTableView *tableView;
@property (nonatomic, strong) QJProductNestCell *nestCell;
/* 底部scroll view */
@property (nonatomic, strong) QJNestScrollView *bgScrollView;
/* headerview偏移量 */
@property (nonatomic, assign) CGFloat bottomCellOffset;
/* table是否能滑动 */
@property (nonatomic, assign) BOOL tableViewCanScroll;
/* 底部scroll是否能滑动 */
@property (nonatomic, assign) BOOL isBgScrollCanScroll;
/* 获取button */
@property (nonatomic, strong) UIButton *getButton;
@property (nonatomic, strong) UIView *bottomView;

@property (nonatomic, strong) UIButton *topButton;
@property (nonatomic, strong) QJSearchGameDetailModel *detailModel;

@end

@implementation QJProductDetailViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    /* 调整自定义导航视图层次 */
    [self.view bringSubviewToFront:self.navView];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navBar.hidden = YES;
    self.detailModel = [[QJSearchGameDetailModel alloc] init];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    /* 加载自定义导航 */
    [self initNav];
    /* 加载UI */
    [self initUI];
}

/**
 * @author: zjr
 * @date: 2022-8-2
 * @desc: 视图加载后请求方法
 */
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self refreshData];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDarkContent];
}

/**
 * @author: zjr
 * @date: 2022-7-13
 * @desc: 自定义导航
 */
- (void)initNav{
    self.view.backgroundColor = UIColorFromRGB(0xFFFFFF);//UIColorFromRGB(0xF5F5F5);
    self.navView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, QJScreenWidth, NavigationBar_Bottom_Y)];
    self.navView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.navView];
    self.backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.backButton setImage:[UIImage imageNamed:@"backImage_white"] forState:UIControlStateNormal];
    [self.backButton setImage:[UIImage imageNamed:@"backimage_black"] forState:UIControlStateSelected];
    self.backButton.frame = CGRectMake(9, [QJDeviceConstant top_iPhoneX_SPACE]+27, 30, 30);
    [self.backButton addTarget:self action:@selector(backBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [self.navView addSubview:self.backButton];
    
    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, QJScreenWidth-60, 30)];
    self.titleLabel.centerY = self.backButton.centerY;
    self.titleLabel.centerX = QJScreenWidth/2;
    self.titleLabel.text = self.model.name?self.model.name:@"";
    self.titleLabel.font = kboldFont(17);
    self.titleLabel.textColor = [UIColor colorWithWhite:0 alpha:0];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.navView addSubview:self.titleLabel];
}

/**
 * @author: zjr
 * @date: 2022-8-2
 * @desc: 加载UI/约束
 */
- (void)initUI{
    self.tableViewCanScroll = YES;
    self.isBgScrollCanScroll = NO;//底部视图开始不能动
    
    [self.view addSubview:self.bgScrollView];
    [self.bgScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.top.equalTo(self.view);//.offset(NavigationBar_Bottom_Y);
        make.bottom.equalTo(self.view).offset(-((Bottom_iPhoneX_SPACE + 56)*kWScale));
    }];
    
    self.headerView = [[QJProductDetailHeaderView alloc] initWithFrame:CGRectMake(0, 0, QJScreenWidth, 306*kWScale)];
    [self.bgScrollView addSubview:self.headerView];
    [self.headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@(306*kWScale));
        make.top.equalTo(self.bgScrollView);
        make.left.equalTo(self.view);
        make.width.equalTo(@(kScreenWidth));
    }];
    
    [self.bgScrollView addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view).offset(-((Bottom_iPhoneX_SPACE + 56)*kWScale));
        make.top.equalTo(self.bgScrollView);
    }];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeStatus) name:QJProductDetailNestCellScrollNotification object:nil];
    NSInteger insetTopInt = 306*kWScale;
    self.bottomCellOffset = insetTopInt;
    self.tableView.contentInset = UIEdgeInsetsMake(self.bottomCellOffset, 0, 0, 0);
    self.tableView.tableHeaderView = [self creatHeaderView];
    
    [self.view addSubview:self.bottomView];
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.left.right.equalTo(self.view);
        make.height.mas_equalTo((Bottom_iPhoneX_SPACE + 56)*kWScale);
    }];
    [self.bottomView addSubview:self.getButton];
    [self.getButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.bottomView.mas_top).offset(16);
        make.left.equalTo(self.bottomView.mas_left).offset(37);
        make.right.equalTo(self.bottomView.mas_right).offset(-37);
        make.height.mas_equalTo(40*kWScale);
    }];
        
    self.topButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.topButton addTarget:self action:@selector(goTopButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [self.topButton setImage:[UIImage imageNamed:@"qj_productdetail_top"] forState:UIControlStateNormal];
    [self.view addSubview:self.topButton];
    self.topButton.hidden = YES;
    [self.topButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@(44*kWScale));
        make.height.equalTo(@(44*kWScale));
        make.bottom.equalTo(self.view).offset(-173);
        make.right.equalTo(self.view).offset(-24);
    }];
}

/**
 * @author: zjr
 * @date: 2022-8-2
 * @desc: 通知方法
 */
- (void)changeStatus{
    self.tableViewCanScroll = YES;
}

/**
 * @author: zjr
 * @date: 2022-6-28
 * @desc: button点击事件
 */
- (void)backBtnAction{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)getbuttonAction{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:_detailModel.iUrl] options:@{} completionHandler:^(BOOL success) {
        DLog(@"是否打开 = %@",success?@"YES":@"NO");
    }];
}

#pragma mark - Lazy Loading
- (UIView *)bottomView {
    if (!_bottomView) {
        _bottomView = [UIView new];
        _bottomView.backgroundColor = [UIColor whiteColor];
        _bottomView.layer.shadowColor = UIColorHex(CECECE40).CGColor;
        _bottomView.layer.shadowOpacity = 1;
        _bottomView.layer.shadowRadius = 6;
        _bottomView.layer.shadowOffset = CGSizeMake(0, -6);
    }
    return _bottomView;
}

- (UIButton *)getButton {
    if (!_getButton) {
        _getButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_getButton addTarget:self action:@selector(getbuttonAction) forControlEvents:UIControlEventTouchUpInside];
        _getButton.titleLabel.font = kFont(16);
        _getButton.backgroundColor = UIColorFromRGB(0x1F2A4D);
        [_getButton setTitle:@"获取" forState:UIControlStateNormal];
        [_getButton setTitleColor:UIColorFromRGB(0xFFFFFF) forState:UIControlStateNormal];
        _getButton.layer.cornerRadius = 4;
        _getButton.layer.borderWidth = 1;
        _getButton.layer.borderColor = kColorWithHexString(@"000000").CGColor;
        _getButton.layer.masksToBounds = YES;
    }
    return _getButton;
}

- (QJNestScrollView *)bgScrollView{
    if (!_bgScrollView) {
        _bgScrollView = [[QJNestScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.width, self.view.height)];
        _bgScrollView.backgroundColor = [UIColor clearColor];
        _bgScrollView.bounces = YES;
        _bgScrollView.delegate = self;
        _bgScrollView.showsVerticalScrollIndicator = NO;
        _bgScrollView.showsHorizontalScrollIndicator = NO;
        _bgScrollView.mj_header = [QJRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(refreshData)];
        if (@available(iOS 11.0, *)) {
            _bgScrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
        _bgScrollView.contentSize = CGSizeMake(QJScreenWidth, QJScreenHeight);
    }
    return _bgScrollView;
}

-(QJHomeListTableView *)tableView{
    if (!_tableView) {
        _tableView = [[QJHomeListTableView alloc] initWithFrame:CGRectMake(0, 0, self.view.width , self.view.height) style:UITableViewStylePlain];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = [UIColor clearColor];
        [_tableView registerClass:[QJProductNestCell class] forCellReuseIdentifier:@"QJProductDetailNestTableViewCell"];
        //去掉顶部偏移
        if (@available(iOS 11.0, *)) {
            _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
        if (@available(iOS 13.0, *)) {
            _tableView.automaticallyAdjustsScrollIndicatorInsets = NO;
        }
    }
    return _tableView;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return kScreenHeight - Bottom_iPhoneX_SPACE - 48*kWScale - NavigationBar_Bottom_Y-50;
}

- (UIView *)creatHeaderView{
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 50)];
    header.backgroundColor = [UIColor whiteColor];
    
    _segment = [[QJSegmentLineView alloc] initWithFrame:CGRectMake(0, 0, QJScreenWidth, 49)];
    _segment.backgroundColor = [UIColor whiteColor];
    _segment.segmentWidthStyle = QJSegmentViewWidthStyleAuto;
    _segment.itemWidth = QJScreenWidth/2;
    _segment.font = kFont(14);
    _segment.selectedFont = FontSemibold(18);
    _segment.textColor = UIColorFromRGB(0x919599);
    _segment.selectedTextColor = UIColorFromRGB(0x16191C);
    _segment.lineHeight = 2;
    _segment.leftOffset = 16;
    _segment.itemSpace = 16;
    _segment.lineColor = UIColorFromRGB(0xFF9500);
    _segment.showBottomLine = YES;
    _segment.tag = 10000;
    [_segment setTitleArray:@[@"详情",@"开服表",]];
    [header addSubview:_segment];
    WS(weakSelf)
    [_segment setSegmentedItemSelectedBlock:^(QJSegmentLineView * _Nonnull segment, NSInteger selectedIndex) {
        weakSelf.nestCell.currentPage = selectedIndex;
    }];
    
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 49, kScreenWidth, 1)];
    lineView.backgroundColor = UIColorFromRGB(0xF5F5F5);
    [header addSubview:lineView];
    return header;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    QJProductNestCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QJProductDetailNestTableViewCell" forIndexPath:indexPath];
    self.nestCell = cell;
    WS(weakSelf)
    cell.scrollCurrentClick = ^(NSInteger currentPage) {
        [weakSelf.segment setSegmentSelectedIndex:currentPage];
    };
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

#pragma mark - scrollview
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (scrollView == self.tableView) {
        DLog(@"scrollView.contentOffset.y = %f",scrollView.contentOffset.y);
        /* 导航渐变背景色 */
        CGFloat offset = scrollView.contentOffset.y + self.bottomCellOffset;
        CGFloat alp = offset/(self.bottomCellOffset-NavigationBar_Bottom_Y);
        if (alp <= 0 ) {
            alp = 0;
            self.backButton.selected = NO;
        }else if (alp >= 1) {
            alp = 1;
            self.backButton.selected = YES;
        }
//        if (alp == 0) {
//            self.backButton.selected = NO;
//        }else{
//            self.backButton.selected = YES;
//        }
        if (alp == 1) {
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDarkContent];
        } else {
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        }
        self.navView.backgroundColor = [UIColor hx_colorWithHexStr:@"0xF5F5F5" alpha:alp];
//        self.navView.backgroundColor = [UIColor colorWithWhite:1 alpha:alp];
        self.titleLabel.textColor = [UIColor colorWithWhite:0 alpha:alp];
        if (scrollView.contentOffset.y == -self.bottomCellOffset) {
            self.isBgScrollCanScroll = YES;
        }else{
            self.isBgScrollCanScroll = NO;
        }
        
        /*
         当 底层滚动图滚动到指定位置时，
         停止滚动，开始滚动子视图
         */
        CGFloat bottomCellOffset = -(NavigationBar_Bottom_Y);
        if (scrollView.contentOffset.y == bottomCellOffset) {
            self.topButton.hidden = NO;
        }else{
            self.topButton.hidden = YES;
        }
        if (scrollView.contentOffset.y <= bottomCellOffset) {
            //在下面时可以滑动
            if (!self.tableViewCanScroll) {//
                scrollView.contentOffset = CGPointMake(0, bottomCellOffset);
            }else{
                if (scrollView.contentOffset.y <= -self.bottomCellOffset) {
                    scrollView.contentOffset = CGPointMake(0, -self.bottomCellOffset);
                }
            }
        }else{
            //开始悬浮了
            scrollView.contentOffset = CGPointMake(0, bottomCellOffset);
            
            [self.view bringSubviewToFront:self.navBar];
            [[NSNotificationCenter defaultCenter] postNotificationName:QJProductDetailNestMainCellScrollNotification object:nil];
            if (self.tableViewCanScroll) {
                self.tableViewCanScroll = NO;
            }
        }
    }else{
        if (!self.isBgScrollCanScroll) {
            scrollView.contentOffset = CGPointMake(0, 0);
        }
    }
}

#pragma mark - 请求方法
- (void)refreshData{
    [self requestProductDetail];
}

- (void)requestProductDetail{
    QJProductRequest *productReq = [[QJProductRequest alloc] init];
    if (_model) {
        [productReq getProductDetailRequest:_model.ID];
    }
    
    if (self.idStr) {
        [productReq getProductDetailRequest:self.idStr];
    }

    WS(weakSelf)
    productReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        [weakSelf.bgScrollView.mj_header endRefreshing];
        if (ResponseSuccess) {
            weakSelf.detailModel = [QJSearchGameDetailModel modelWithDictionary:EncodeDicFromDic(request.responseObject, @"data")];
            weakSelf.titleLabel.text = weakSelf.detailModel.name?weakSelf.detailModel.name:@"";
            weakSelf.headerView.detailModel = weakSelf.detailModel;
            weakSelf.nestCell.detailModel = weakSelf.detailModel;
            [weakSelf resetGetButtonTitle];
            [[NSNotificationCenter defaultCenter] postNotificationName:QJProductDetailRefreshAllDataNotification object:nil];
        }else{
            NSString *msg = EncodeStringFromDic(request.responseObject, @"message");
            [weakSelf.view makeToast:msg];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [weakSelf.navigationController popViewControllerAnimated:YES];
            });
        }
    };
    productReq.failureCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        [weakSelf.view makeToast:@"数据异常 请稍后再试"];
    };
}

- (void)resetGetButtonTitle{
    [self.getButton setTitle:[NSString stringWithFormat:@"获取(%@)",[self fileSizeWithInteger:_detailModel.iPkgSize.integerValue]] forState:UIControlStateNormal];
}

// 根据数据计算出大小
- (NSString *)fileSizeWithInteger:(NSInteger)size{
    // 1K = 1024dB, 1M = 1024K,1G = 1024M
    if (size < 1024) {// 小于1k
        return [NSString stringWithFormat:@"%ldB",(long)size];
    }else if (size < 1024 * 1024){// 小于1m
        CGFloat aFloat = size / 1024.0;
        return [NSString stringWithFormat:@"%.0fK",aFloat];
    }else if (size < 1024 * 1024 * 1024){// 小于1G
        CGFloat aFloat = size / 1024.0 / 1024.0;
        return [NSString stringWithFormat:@"%.2fM",aFloat];
    }else{
        CGFloat aFloat = size / 1024.0 / 1024.0 / 1024.0;
        return [NSString stringWithFormat:@"%.2fG",aFloat];
    }
}

- (void)goTopButtonAction{
    self.tableViewCanScroll = YES;
    self.isBgScrollCanScroll = NO;
    self.nestCell.canScroll = NO;
    [self.nestCell scrollTableView];
    [self.tableView scrollToTop];
}

@end
