//
//  QJMyBoxListViewController.m
//  QJBox
//
//  Created by rui on 2022/7/22.
//

#import "QJMyBoxListViewController.h"
/* 游戏公共Cell */
#import "QJGameCommonCell.h"
/* 游戏公共Model */
#import "QJSearchGameListModel.h"
/* 游戏详情页面 */
#import "QJProductDetailViewController.h"
/* 请求 */
#import "QJProductRequest.h"
#import "QJProductPostRequest.h"
#import "QJDetailAlertView.h"
#import "TYAlertController.h"

#import "QJDemoTestViewController.h"

@interface QJMyBoxListViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) QJSearchGameListModel *pageModel;
@property (nonatomic, strong) NSMutableArray *idsArray; // 存放数据id
@property (nonatomic, strong) UIButton *editBtn;
@property (nonatomic, strong) UIView *deleteView;
@property (nonatomic, strong) UIButton *allChooseBtn;
@property (nonatomic, strong) UIButton *cancelBtn;
@property (nonatomic, strong) UIButton *deleteBtn;

@end

@implementation QJMyBoxListViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    /* 加载请求 */
    if (self.editBtn.selected == NO) {
        [self refreshData];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"我的游戏";
    self.pageModel = [QJSearchGameListModel new];
    self.navBar.bottomLine.hidden = NO;
    self.idsArray = [NSMutableArray array];
    UIButton *editBtn = [UIButton new];
    [editBtn setImage:[UIImage imageNamed:@"qj_myproduct_edit"] forState:UIControlStateNormal];
    [editBtn addTarget:self action:@selector(editAction) forControlEvents:UIControlEventTouchUpInside];
    self.editBtn.selected = NO;
    self.editBtn = editBtn;
    self.navBar.rightBarItems = @[self.self.editBtn];

    /* 加载UI */
    [self initUI];
    [self creatBottomView];
    /* 加载请求 */
//    [self refreshData];
    
//    UIButton *testButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    testButton.frame = CGRectMake(0, 100, 100, 100);
//    testButton.backgroundColor = [UIColor redColor];
//    [testButton addTarget:self action:@selector(testClick) forControlEvents:UIControlEventTouchUpInside];
//    [self.view addSubview:testButton];
}

- (void)testClick{
    QJDemoTestViewController *testVC = [[QJDemoTestViewController alloc] init];
    [self.navigationController pushViewController:testVC animated:YES];
}

/**
 * @author: zjr
 * @date: 2022-8-2
 * @desc: 加载UI/约束
 */
- (void)initUI{
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view);
        make.top.equalTo(self.view).offset(NavigationBar_Bottom_Y);
    }];
}

#pragma mark - Lazy Loading
-(UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.width , self.view.height) style:UITableViewStylePlain];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = [UIColor whiteColor];
        [_tableView registerClass:[QJGameCommonCell class] forCellReuseIdentifier:@"QJGameListCommonCell"];
        if (@available(iOS 11.0, *)) {
            _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
            _tableView.estimatedRowHeight = 0;
            _tableView.estimatedSectionFooterHeight = 0;
            _tableView.estimatedSectionHeaderHeight = 0;
        }
        _tableView.mj_header = [QJRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(refreshData)];
        _tableView.mj_footer = [QJRefreshFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreHomeData)];
    }
    return _tableView;
}

#pragma mark - UItableView
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.pageModel.records.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 84*kWScale;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    QJSearchGameDetailModel *model = [self.pageModel.records safeObjectAtIndex:indexPath.row];
    QJGameCommonCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QJGameListCommonCell" forIndexPath:indexPath];
    model.type = @"0";
    cell.model = model;
    WS(weakSelf)
//    cell.btnClick = ^(QJSearchGameDetailModel * _Nonnull model) {
//        NSMutableArray *idsArray = [NSMutableArray array];
//        [idsArray addObject:model.ID];
//        [weakSelf deleteAlertAction:idsArray];
//    };
    cell.chooseClick = ^(QJSearchGameDetailModel * _Nonnull chooseModel) {
        [weakSelf reloadSelectTableViewSection:indexPath.section row:indexPath.row];
    };
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.editBtn.selected == NO) {
        QJSearchGameDetailModel *model = [self.pageModel.records safeObjectAtIndex:indexPath.row];
        UIViewController *vc = [QJAppTool getCurrentViewController];
        [vc checkLogin:^(BOOL isLogin) {
            if (isLogin) {
                QJProductDetailViewController *detailVC = [[QJProductDetailViewController alloc] init];
                detailVC.model = model;
                [self.navigationController pushViewController:detailVC animated:YES];
            }
        }];
    }
}

//设Cell可编辑
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

//-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
//    return UITableViewCellEditingStyleDelete;
//}
//
///*改变删除按钮的title*/
-(NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath{
    return @"删除";
}

/*删除用到的函数*/
-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        QJSearchGameDetailModel *model = [self.pageModel.records safeObjectAtIndex:indexPath.row];
        NSMutableArray *chooseDeleteArray = [NSMutableArray array];
        [chooseDeleteArray addObject:model.ID];
        [self deleteAlertAction:chooseDeleteArray];
    }
}

- (void)tableView:(UITableView *)tableView willBeginEditingRowAtIndexPath:(NSIndexPath *)indexPath {
// 在 iOS11 以下系统,因为方法线程问题,需要放到主线程执行, 不然没有效果
    dispatch_async(dispatch_get_main_queue(), ^{
        [self setupSlideBtnWithEditingIndexPath:indexPath];
    });
}

//- (NSArray<UITableViewRowAction *> *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
//    UITableViewRowAction *action = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"删除" handler:^(UITableViewRowAction * _Nonnull action, NSIndexPath * _Nonnull indexPath) {
//        // 删除点击后的操作
//        [tableView setEditing:NO animated:YES];
//    }];
//    action.backgroundColor = [UIColor redColor];
//    return @[action];
//}

//MARK: 设置左滑按钮的样式
- (void)setupSlideBtnWithEditingIndexPath:(NSIndexPath *)editingIndexPath {
// 判断系统是否是 iOS13 及以上版本
    if (@available(iOS 13.0, *)) {
        for (UIView *subView in self.tableView.subviews) {
            if ([subView isKindOfClass:NSClassFromString(@"_UITableViewCellSwipeContainerView")] && [subView.subviews count] >= 1) {
            // 修改图片
                UIView *remarkContentView = subView.subviews.firstObject;
                [self setupRowActionView:remarkContentView];
            }
        }
        return;
    }

// 判断系统是否是 iOS11 及以上版本
    if (@available(iOS 11.0, *)) {
        for (UIView *subView in self.tableView.subviews) {
            if ([subView isKindOfClass:NSClassFromString(@"UISwipeActionPullView")] && [subView.subviews count] >= 1) {
            // 修改图片
            UIView *remarkContentView = subView;
            [self setupRowActionView:remarkContentView];
            }
        }
        return;
    }

// iOS11 以下的版本
    QJGameCommonCell *cell = [self.tableView cellForRowAtIndexPath:editingIndexPath];
    for (UIView *subView in cell.subviews) {
        if ([subView isKindOfClass:NSClassFromString(@"UITableViewCellDeleteConfirmationView")] && [subView.subviews count] >= 1) {
// 修改图片
            UIView *remarkContentView = subView;
            [self setupRowActionView:remarkContentView];
        }
    }
}

- (void)setupRowActionView:(UIView *)rowActionView {
// 切割圆角
//    [rowActionView showCorner:9 rectCorner:UIRectCornerTopLeft | UIRectCornerTopRight];
    rowActionView.layer.cornerRadius = 9.0;
    rowActionView.clipsToBounds = YES;
// 改变父 View 的frame，这句话是因为我在 contentView 里加了另一个 View，为了使划出的按钮能与其达到同一高度
    CGRect frame = rowActionView.frame;
    frame.origin.y += 7;
    frame.size.height -= 13;
    rowActionView.frame = frame;
// 拿到按钮,设置图片
    UIButton *button = rowActionView.subviews.firstObject;
    NSLog(@"title:%@",button.titleLabel.text);
    [button setTitle:@"删除" forState:UIControlStateNormal];
//    [button setImage:[UIImage imageNamed:@"qj_product_delete"] forState:UIControlStateNormal];
    button.layer.cornerRadius = 9.0;
//    [button showCorner:9 rectCorner:UIRectCornerTopLeft | UIRectCornerTopRight];
    button.clipsToBounds = YES;
}

#pragma mark - 请求方法
- (void)refreshData {
    _pageModel.willLoadMore = NO;
    [self sendRequest];
}

- (void)loadMoreHomeData {
    DLog(@"加载更多");
    if(_pageModel.hasNext){
        _pageModel.willLoadMore=YES;
        [self sendRequest];
    }else{
        [self.tableView.mj_footer endRefreshing];
        [self.tableView.mj_footer endRefreshingWithNoMoreData];
    }
}

/**
 * @author: zjr
 * @date: 2022-7-7
 * @desc: 搜索游戏
 */
- (void)sendRequest{
    QJProductRequest *productReq = [[QJProductRequest alloc] init];
    NSMutableDictionary *muDic = [NSMutableDictionary dictionaryWithDictionary:_pageModel.toParams];
    [muDic setValue:@"3" forKey:@"state"];
    productReq.dic = muDic.copy;
    [productReq getUserProductRequest];

    WS(weakSelf)
    productReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            [weakSelf.pageModel configObj:[QJSearchGameListModel modelWithDictionary:EncodeDicFromDic(request.responseObject, @"data")]];
        }else{
            NSString *msg = EncodeStringFromDic(request.responseObject, @"message");
            [weakSelf.view makeToast:msg];
        }
        [weakSelf.tableView reloadData];
        [weakSelf.tableView.mj_header endRefreshing];
        [weakSelf.tableView.mj_footer endRefreshing];
        QJEmptyCommonView *emptyView = [QJEmptyCommonView new];
        emptyView.string = @"还没有任何游戏噢～";
        emptyView.emptyImage = @"empty_no_data";
        emptyView.topSpace = 72;
        [weakSelf.tableView tableViewDisplayWhenHaveNoDataWithView:emptyView ifNecessaryForRowCount:weakSelf.pageModel.records.count];
        if (weakSelf.pageModel.records.count == 0) {
            self.editBtn.hidden = YES;
        } else {
            self.editBtn.hidden = NO;
        }
    };
    productReq.failureCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        [weakSelf.tableView reloadData];
        [weakSelf.tableView.mj_header endRefreshing];
        [weakSelf.tableView.mj_footer endRefreshing];
        [weakSelf.view makeToast:@"数据异常 请稍后再试"];
        QJEmptyCommonView *emptyView = [QJEmptyCommonView new];
        emptyView.string = @"还没有任何游戏噢～";
        emptyView.emptyImage = @"empty_no_data";
        emptyView.topSpace = 72;
        [weakSelf.tableView tableViewDisplayWhenHaveNoDataWithView:emptyView ifNecessaryForRowCount:weakSelf.pageModel.records.count];
    };
}

/**
 * @author: zjr
 * @date: 2022-8-2
 * @desc: 删除游戏
 */
- (void)deleteProduct:(NSMutableArray *)array{
    [QJAppTool showHUDLoading];
    
    QJProductPostRequest *productReq = [[QJProductPostRequest alloc] init];
    NSMutableDictionary *muDic = [NSMutableDictionary dictionary];
    [muDic setValue:array forKey:@"ids"];
    productReq.dic = muDic.copy;
    [productReq getProductDeleteRequest];

    WS(weakSelf)
    productReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        [QJAppTool hideHUDLoading];
        if (ResponseSuccess) {
            [weakSelf.view makeToast:@"删除成功"];
            [weakSelf resetArrayClick];
//            [weakSelf resetModelList:array];
            [weakSelf refreshData];
        }else{
            NSString *msg = EncodeStringFromDic(request.responseObject, @"message");
            [weakSelf.view makeToast:msg];
        }
        [weakSelf.tableView.mj_header endRefreshing];
    };
    productReq.failureCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        [QJAppTool hideHUDLoading];
        [weakSelf.tableView reloadData];
        [weakSelf.tableView.mj_header endRefreshing];
        [weakSelf.view makeToast:@"数据异常 请稍后再试"];
    };
}

/**
 * @author: zjr
 * @date: 2022-8-2
 * @desc: 删除游戏后刷新列表
 */
- (void)resetModelList:(NSMutableArray *)array{
    [self.pageModel.records enumerateObjectsUsingBlock:^(QJSearchGameDetailModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        QJSearchGameDetailModel *dataModel = (QJSearchGameDetailModel *)obj;
        [array enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSString *idStr = (NSString *)obj;
            if(dataModel.ID.integerValue == idStr.integerValue){
                [self.pageModel.records removeObject:dataModel];
                [self.tableView reloadData];
            }
        }];
    }];
    [self resetArrayClick];
}

- (void)editAction{
    [self.pageModel.records enumerateObjectsUsingBlock:^(QJSearchGameDetailModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        obj.isDelete = NO;
    }];
    if (self.editBtn.selected == NO) {
        self.editBtn.selected = YES;
        _deleteView.hidden = NO;
        _deleteView.hidden = NO;
        [self.tableView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.view).offset(-62);
        }];
        [self.pageModel.records enumerateObjectsUsingBlock:^(QJSearchGameDetailModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            obj.isEditing = YES;
        }];
        _tableView.mj_header = nil;
        _tableView.mj_footer = nil;
    } else {
        self.editBtn.selected = NO;
        [self resetArrayClick];
        [self.pageModel.records enumerateObjectsUsingBlock:^(QJSearchGameDetailModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            obj.isEditing = NO;
        }];
    }
    [self.tableView reloadData];
}

- (void)creatBottomView{
    self.deleteView = [[UIView alloc] initWithFrame:CGRectMake(0, QJScreenHeight - (49+ Bottom_iPhoneX_SPACE)-62, QJScreenWidth, 62)];
    [self.view addSubview:self.deleteView];
    _deleteView.backgroundColor = UIColorFromRGB(0xF4F4F4);
    _deleteView.hidden = YES;
    
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _deleteView.width, 1)];
    [_deleteView addSubview:line];
    line.backgroundColor = UIColorFromRGB(0xeeeeee);
    
    UIButton *downBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_deleteView addSubview:downBtn];
    [downBtn setTitle:@"删除" forState:UIControlStateNormal];
    downBtn.titleLabel.font = kFont(14);
    [downBtn setTitleColor:UIColorFromRGB(0xFFFFFF) forState:UIControlStateNormal];
    [downBtn addTarget:self action:@selector(deleteClick) forControlEvents:UIControlEventTouchUpInside];
    downBtn.frame = CGRectMake(QJScreenWidth/2+15, 20, 126, 34);
    downBtn.backgroundColor = UIColorFromRGB(0xFF9500);
    downBtn.layer.cornerRadius = 17;
    downBtn.layer.masksToBounds = YES;
    self.deleteBtn = downBtn;
    [self.deleteBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.deleteView).offset(-16);
        make.height.equalTo(@34);
        make.width.equalTo(@126);
        make.top.equalTo(self.deleteView).offset(14);
    }];
    
    UIButton *resetBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_deleteView addSubview:resetBtn];
    [resetBtn setTitle:@"取消" forState:UIControlStateNormal];
    resetBtn.titleLabel.font = kFont(14);
    [resetBtn setTitleColor:UIColorFromRGB(0x101010) forState:UIControlStateNormal];
    [resetBtn addTarget:self action:@selector(resetArrayClick) forControlEvents:UIControlEventTouchUpInside];
    resetBtn.frame = CGRectMake(QJScreenWidth/2-126-15, 20, 126, 34);
    resetBtn.backgroundColor = UIColorFromRGB(0xFFFFFF);
    resetBtn.layer.borderWidth = 1;
    resetBtn.layer.borderColor = [UIColor colorWithRed:0.57 green:0.585 blue:0.6 alpha:1].CGColor;
    resetBtn.layer.cornerRadius = 17;
    resetBtn.layer.masksToBounds = YES;
    self.cancelBtn = resetBtn;
    [self.cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.deleteBtn.mas_left).offset(-16);
        make.height.equalTo(@34);
        make.width.equalTo(@126);
        make.top.equalTo(self.deleteView).offset(14);
    }];
    
    UIButton *allBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_deleteView addSubview:allBtn];
    [allBtn setTitle:@"全选" forState:UIControlStateNormal];
    [allBtn setImage:[UIImage imageNamed:@"qj_myproduct_normal"] forState:UIControlStateNormal];
    [allBtn setImage:[UIImage imageNamed:@"qj_myproduct_choose"] forState:UIControlStateSelected];
    allBtn.titleLabel.font = kFont(14);
    [allBtn setTitleColor:UIColorFromRGB(0x000000) forState:UIControlStateNormal];
    [allBtn addTarget:self action:@selector(allArrayClick) forControlEvents:UIControlEventTouchUpInside];
    allBtn.frame = CGRectMake(QJScreenWidth/2-126-15, 20, 126, 34);
    self.allChooseBtn = allBtn;
    self.allChooseBtn.selected = NO;
    [self.allChooseBtn layoutWithStatus:QJLayoutStatusNormal andMargin:10];
    
    [self.allChooseBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.deleteView).offset(16);
        make.width.equalTo(@55);
        make.height.equalTo(@34);
        make.centerY.equalTo(self.deleteBtn);
    }];
}

- (void)allArrayClick{
    if (self.allChooseBtn.selected == NO) {
        self.allChooseBtn.selected = YES;
        [self reloadAllChooseData:YES];
    } else {
        self.allChooseBtn.selected = NO;
        [self reloadAllChooseData:NO];
    }
}

- (void)resetArrayClick{
    self.editBtn.selected = NO;
    _deleteView.hidden = YES;
    self.allChooseBtn.selected = NO;
    [self.idsArray removeAllObjects];
    [self.deleteBtn setTitle:@"删除" forState:UIControlStateNormal];
    [self.pageModel.records enumerateObjectsUsingBlock:^(QJSearchGameDetailModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        obj.isEditing = NO;
        obj.isDelete = NO;
    }];
    [self.tableView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.view);
    }];
    [self.tableView reloadData];
    _tableView.mj_header = [QJRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(refreshData)];
    _tableView.mj_footer = [QJRefreshFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreHomeData)];
}

- (void)deleteClick{
    if (self.idsArray.count > 0) {
        [self deleteAlertAction:self.idsArray];
    }else{
        [self.view makeToast:@"请选择要删除的游戏"];
    }
}

- (void)deleteAlertAction:(NSMutableArray *)array{
    QJDetailAlertView *alertView = [[QJDetailAlertView alloc] initWithFrame:CGRectZero titleType:QJDetailAlertViewTitleTypeDefault buttonType:QJDetailAlertViewButtonTypeDefault];
    [alertView showTitleText:@"提示" describeText:@"是否要删除游戏？"];
    [alertView.leftButton setTitle:@"取消" forState:UIControlStateNormal];
    [alertView.leftButton setTitleColor:UIColorFromRGB(0x007AFF) forState:UIControlStateNormal];
    [alertView.rightButton setTitle:@"确认" forState:UIControlStateNormal];
    [alertView returnClick:^(QJDetailAlertViewButtonClickType type) {
        [[QJAppTool getCurrentViewController] dismissViewControllerAnimated:YES completion:nil];
        if (type == QJDetailAlertViewButtonClickTypeRight) {
            [self deleteProduct:array];
        }
    }];
    TYAlertController *alertController = [TYAlertController alertControllerWithAlertView:alertView preferredStyle:TYAlertControllerStyleAlert];
    [[QJAppTool getCurrentViewController] presentViewController:alertController animated:YES completion:nil];
}

- (void)reloadAllChooseData:(BOOL)isChoose{
    [self.idsArray removeAllObjects];
    if (isChoose) {
        [self.pageModel.records enumerateObjectsUsingBlock:^(QJSearchGameDetailModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            obj.isEditing = YES;
            obj.isDelete = YES;
            if (![self.idsArray containsObject:obj.ID]) {
                [self.idsArray addObject:obj.ID];
            }
        }];
    } else {
        [self.pageModel.records enumerateObjectsUsingBlock:^(QJSearchGameDetailModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            obj.isEditing = YES;
            obj.isDelete = NO;
        }];
    }
    [self.tableView reloadData];
    if (self.idsArray.count > 0) {
        [self.deleteBtn setTitle:[NSString stringWithFormat:@"删除(%ld)",self.idsArray.count] forState:UIControlStateNormal];
    } else {
        [self.deleteBtn setTitle:@"删除" forState:UIControlStateNormal];
    }
}

// 刷新选择
- (void)reloadSelectTableViewSection:(NSInteger)section row:(NSInteger)row {
    // 更改数据源
    QJSearchGameDetailModel *model = [self.pageModel.records safeObjectAtIndex:row];
    model.isDelete = !model.isDelete;
    self.pageModel.records[row] = model;
    // 加入id到删除数组
    if ([self.idsArray containsObject:model.ID]) {
        [self.idsArray removeObject:model.ID];
    } else {
        [self.idsArray addObject:model.ID];
    }
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:section];
    //为了避免重新加载时出现不需要的动画（又名“闪烁”)
    BOOL animationsEnabled = [UIView areAnimationsEnabled];
    [UIView setAnimationsEnabled:NO];
    [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath,nil] withRowAnimation:UITableViewRowAnimationNone];
    [UIView setAnimationsEnabled:animationsEnabled];
    
    if (self.idsArray.count > 0) {
        [self.deleteBtn setTitle:[NSString stringWithFormat:@"删除(%ld)",self.idsArray.count] forState:UIControlStateNormal];
    } else {
        [self.deleteBtn setTitle:@"删除" forState:UIControlStateNormal];
    }
    if (self.idsArray.count == self.pageModel.records.count) {
        self.allChooseBtn.selected = YES;
    } else {
        self.allChooseBtn.selected = NO;
    }
}

@end
