//
//  QJProductClassificationViewController.m
//  QJBox
//
//  Created by wxy on 2022/7/25.
//

#import "QJProductClassificationViewController.h"
#import "QJProductClassificationSelectedView.h"
#import "QJProductClassificationAllView.h"
#import "QJProductClassificationProductCell.h"
#import "QJProductClassificationView.h"
#import "QJProductSearchRequest.h"
#import "QJProductClassificationModel.h"
#import "QJSearchGameViewController.h"
#import "QJProductClassificationSearchResultVC.h"
#import "QJProductDetailViewController.h"
#import "QJMyBoxListViewController.h"
#define QJTAG  1000
#define QJTabele  2000
@interface QJProductClassificationViewController ()

@property (nonatomic, strong) UIButton *searchBtn;

//@property (nonatomic, strong) UIButton *rightBtn;

@property (nonatomic, strong) QJProductClassificationSelectedView *selectedView;

@property (nonatomic, strong) UIView *leftBgView;

@property (nonatomic, strong) NSMutableArray *btnArr;

@property (nonatomic, strong) QJProductClassificationAllView *allView;//全部分类

@property (nonatomic, strong) QJProductClassificationView *classificationView;//其他具体游戏列表

@property (nonatomic, strong) QJProductSearchRequest *queryhReq;

@property (nonatomic, strong) QJProductClassificationModel *model;

@property (nonatomic, strong) NSMutableArray *tableViewArr;//存放tableview
@end

@implementation QJProductClassificationViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self setUI];
    [self queryData];
}


#pragma mark -- setUI

- (void)setUI {
    self.navBar.backgroundColor = kColorWithHexString(@"#F5F5F5");
    self.view.backgroundColor =  kColorWithHexString(@"#F5F5F5");
    [self.searchBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.navBar.backButton.mas_right).offset(8);
//        make.centerY.equalTo(self.navBar.backButton);
//        make.height.equalTo(@34);
        make.right.equalTo(self.navBar).offset(-15);
        make.top.equalTo(self.navBar).offset([QJDeviceConstant sysStatusBarHeight]+4);
        make.bottom.equalTo(self.navBar).offset(-5);
    }];
    
//    self.navBar.rightBarItems = @[self.rightBtn];
    
    [self.view addSubview:self.leftBgView];
    
    [self.leftBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view);
        make.top.equalTo(self.navBar.mas_bottom).offset(Get375Width(10));
        make.width.equalTo(@(Get375Width(92)));
        make.bottom.equalTo(self.view);
    }];
    
    [self.leftBgView addSubview:self.selectedView];

}

- (void)queryData {
    WS(weakSelf)
   
    self.queryhReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        DLog(@"成功");
//        [self.view hideHUDIndicator];
        if (ResponseSuccess) {
            NSDictionary *data = EncodeDicFromDic(request.responseJSONObject, @"data");
            weakSelf.model = [QJProductClassificationModel modelWithJSON:data];
            
            [weakSelf reloadData];
        }else{
            [weakSelf.view makeToast:ResponseMsg];
        }
    };
    self.queryhReq.failureCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        [weakSelf.view makeToast:ResponseFailToastMsg];
    };
    [self.queryhReq beginQueryTages];
}


- (void)reloadData {
    self.allView.model = self.model;
    self.btnArr = [NSMutableArray array];
    NSMutableArray *muArr = [NSMutableArray arrayWithArray:self.model.filterOption];
    QJProductClassificationFilterOptionsModel *first = [QJProductClassificationFilterOptionsModel new];
    first.name = @"全部";
    [muArr insertObject:first atIndex:0];
    
    for (int i = 0; i < muArr.count; i++) {
        QJProductClassificationFilterOptionsModel *model = [muArr safeObjectAtIndex:i];
        UIButton *btn = [UIButton new];
        [btn setTitle:model.name forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(btnAction:) forControlEvents:UIControlEventTouchUpInside];
        btn.tag = QJTAG + i;
        btn.backgroundColor = [UIColor clearColor];
        btn.titleLabel.font = FontRegular(14);
        btn.titleLabel.numberOfLines = 2;
        btn.titleLabel.textAlignment = NSTextAlignmentCenter;
        [btn setTitleColor:kColorWithHexString(@"#000000") forState:UIControlStateNormal];
        [self.leftBgView addSubview:btn];
        [self.btnArr safeAddObject:btn];
        
        if (i == 0) {
             
            btn.titleLabel.font = FontSemibold(14);
            [btn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerY.equalTo(self.leftBgView.mas_top).offset(27);
                make.left.right.equalTo(self.leftBgView);
                make.height.equalTo(@55);
            }];
            [self.selectedView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.right.centerY.equalTo(btn);
                make.height.equalTo(@70);
            }];
        }else{
            
            UIButton *lastBtn = self.btnArr[i-1];
            [btn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(lastBtn.mas_bottom);
                make.height.left.right.equalTo(lastBtn);
                
            }];
        }
        
    }
    
    [self.view addSubview:self.allView];
    WS(weakSelf)
    self.allView.clickCell = ^(QJProductClassificationAllTagsOptionModel * _Nonnull model) {
        QJProductClassificationSearchResultVC *vc = [QJProductClassificationSearchResultVC new];
        vc.model = weakSelf.model;
        vc.tagOptionModel = model;
        [weakSelf.navigationController pushViewController:vc animated:YES];
        
    };
    [self.allView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.leftBgView);
            make.left.equalTo(self.leftBgView.mas_right).offset(10);
            make.right.equalTo(self.view).offset(-10);
            make.bottom.equalTo(self.view);
    }];
    
    
    
    self.tableViewArr = [NSMutableArray array];
    for (int i = 0; i < self.model.filterOption.count; i++) {
        QJProductClassificationView *classificationView = [QJProductClassificationView new];
        classificationView.hidden = YES;
        classificationView.tag = QJTabele + i;
        [self.tableViewArr safeAddObject:classificationView];
        [self.view addSubview:classificationView];
        classificationView.cellClick = ^(QJSearchGameDetailModel * _Nonnull model) {
            DLog(@"进入详情");
            QJProductDetailViewController *detailVC = [QJProductDetailViewController new];
            detailVC.model = model;
            [weakSelf.navigationController pushViewController:detailVC animated:YES];
        };
        
        classificationView.cellBtnClick = ^(QJSearchGameDetailModel * _Nonnull model) {
            DLog(@"获取游戏");
            
        };
        [classificationView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self.leftBgView);
                make.left.equalTo(self.leftBgView.mas_right).offset(10);
                make.right.equalTo(self.view).offset(-10);
                make.bottom.equalTo(self.view);
        }];
    }
  
    
    
}

#pragma mark ------ getter

- (UIButton *)searchBtn {
    if (!_searchBtn) {
        _searchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_searchBtn setImage:[UIImage imageNamed:@"icon_search"] forState:UIControlStateNormal];
        [_searchBtn setTitle:@"搜索您想玩的游戏" forState:UIControlStateNormal];
        _searchBtn.titleLabel.font = kFont(14);
        [_searchBtn setTitleColor:kColorWithHexString(@"#C8CACC") forState:UIControlStateNormal];
        _searchBtn.backgroundColor = [UIColor whiteColor];
        _searchBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        _searchBtn.layer.cornerRadius = 17;
        [_searchBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 8, 0, 0)];
        [_searchBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 10, 0, 0)];
        [_searchBtn addTarget:self action:@selector(searchBtnAction) forControlEvents:UIControlEventTouchUpInside];
        [self.navBar addSubview:_searchBtn];
    }
    return _searchBtn;
}


//- (UIButton *)rightBtn {
//    if (!_rightBtn) {
//        _rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//        [_rightBtn setImage:[UIImage imageNamed:@"qj_product_shoubing"] forState:UIControlStateNormal];
//        [_rightBtn addTarget:self action:@selector(myProduct) forControlEvents:UIControlEventTouchUpInside];
//
//    }
//    return _rightBtn;
//}


- (UIView *)leftBgView {
    if (!_leftBgView) {
        _leftBgView = [UIView new];
        _leftBgView.backgroundColor = [UIColor whiteColor];
    }
    return _leftBgView;
}

- (QJProductClassificationSelectedView *)selectedView {
    if (!_selectedView) {
        _selectedView = [QJProductClassificationSelectedView new];
        _selectedView.topView.hidden = YES;
    }
    return _selectedView;
}


- (QJProductClassificationAllView *)allView {
    if (!_allView) {
        _allView = [QJProductClassificationAllView new];
        
    }
    return _allView;
}


- (QJProductClassificationView *)classificationView {
    if (!_classificationView) {
        _classificationView = [QJProductClassificationView new];
        
        _classificationView.hidden = YES;
    }
    return _classificationView;
}

- (QJProductSearchRequest *)queryhReq {
    if (!_queryhReq) {
        _queryhReq = [QJProductSearchRequest new];
        
    }
    return _queryhReq;
}
#pragma mark  ---- btnAction

- (void)searchBtnAction {
    QJSearchGameViewController *checkInVC = [[QJSearchGameViewController alloc] init];
    checkInVC.isFromGameCenter = self.isFromGameCenter;
    [self.navigationController pushViewController:checkInVC animated:YES];
}

- (void)myProduct {
    DLog(@"我的游戏");
    QJMyBoxListViewController *myBoxVC = [QJMyBoxListViewController new];
    [self.navigationController pushViewController:myBoxVC animated:YES];
    
}


- (void)btnAction:(UIButton *)sender {
    [self.selectedView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.centerY.equalTo(sender);
        make.height.equalTo(@70);
    }];
    for (UIButton *btn in self.btnArr.copy) {
        btn.titleLabel.font = FontRegular(14);
    }
    sender.titleLabel.font = FontSemibold(14);
    NSInteger index = sender.tag - QJTAG;
    if (index ==  0) {
        self.selectedView.topView.hidden = YES;
    }else{
        self.selectedView.topView.hidden = NO;
    }
    DLog(@"下标=%ld",index);
    if (index == 0) {
        self.allView.hidden = NO;
        
        for (QJProductClassificationView *view in self.tableViewArr.copy) {
            view.hidden = YES;
        }
    }else{
        self.allView.hidden = YES;
        int i = 0;
        for (QJProductClassificationView *view in self.tableViewArr.copy) {
            
            if (i == (index-1)) {
                view.hidden = NO;
                if (view.isNeedReload) {
                    view.isNeedReload = NO;
                    DLog(@"请求接口刷新");
                    view.model = [self.model.filterOption safeObjectAtIndex:(i)];
                }
            }else{
                view.hidden = YES;
            }
            i++;
        }
    }
}
@end
