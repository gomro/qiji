//
//  QJGameCenterViewController.m
//  QJBox
//
//  Created by rui on 2022/7/22.
//

#import "QJGameCenterViewController.h"
#import "QJGameCenterHeaderView.h"
#import "QJProductClassificationViewController.h"
@interface QJGameCenterViewController ()<UITableViewDelegate,UITableViewDataSource>

/* 自定义导航View */
@property (nonatomic, strong) UIView *navView;
/* 返回按钮 */
@property (nonatomic, strong) UIButton *searchButton;
/* 标题 */
@property (nonatomic, strong) UILabel *titleLabel;

@property (nonatomic, strong) UIView *colorView;

@property (nonatomic, strong) QJGameCenterHeaderView *headerView;

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, assign) CGFloat bottomCellOffset;
@property (nonatomic,assign) BOOL tableViewCanScroll;//tabelvie是否能动

@end

@implementation QJGameCenterViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    /* 调整自定义导航视图层次 */
    [self.view bringSubviewToFront:self.navView];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navBar.hidden = YES;
    self.tableViewCanScroll = YES;
    /* 加载自定义导航 */
    [self initNav];
    [self initUI];
}

/**
 * @author: zjr
 * @date: 2022-7-22
 * @desc: 自定义导航
 */
- (void)initNav{
    self.view.backgroundColor = HexColor(0xF5F5F5);
    self.navView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, QJScreenWidth, NavigationBar_Bottom_Y)];
    self.navView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.navView];
    
    self.searchButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.searchButton setImage:[UIImage imageNamed:@"icon_search"] forState:UIControlStateNormal];
    [self.searchButton setTitle:@"搜索您想玩的游戏" forState:UIControlStateNormal];
    self.searchButton.frame = CGRectMake(15, [QJDeviceConstant top_iPhoneX_SPACE]+27, QJScreenWidth - 30, 30);
    self.searchButton.titleLabel.font = kFont(14);
    [self.searchButton setTitleColor:HexColor(0x919599) forState:UIControlStateNormal];
    self.searchButton.backgroundColor = [UIColor whiteColor];
    [self.searchButton addTarget:self action:@selector(btnAction) forControlEvents:UIControlEventTouchUpInside];
    self.searchButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.searchButton.layer.cornerRadius = 15;
    [self.searchButton setImageEdgeInsets:UIEdgeInsetsMake(0, 10, 0, 10)];
    [self.searchButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 20, 0, -20)];
    [self.navView addSubview:self.searchButton];
    
    self.colorView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, QJScreenWidth, 254)];
    [self.view addSubview:self.colorView];
    [self.colorView graduateLeftColor:HexColor(0xDF5D25) ToColor:HexColor(0xF5F5F5) startPoint:CGPointMake(0.5, 0) endPoint:CGPointMake(0.5, 1)];
    
    self.headerView = [[QJGameCenterHeaderView alloc] initWithFrame:CGRectMake(0, NavigationBar_Bottom_Y, QJScreenWidth, 318)];
    [self.view addSubview:self.headerView];
}

- (void)initUI{
    [self.view addSubview:self.headerView];
    
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.edges.equalTo(self.view);
        make.left.right.bottom.equalTo(self.view);
        make.top.equalTo(self.view).offset(NavigationBar_Bottom_Y);
    }];
    self.bottomCellOffset = 318;
    self.tableView.contentInset = UIEdgeInsetsMake(318, 0, 0, 0);
}

/**
 * @author: zjr
 * @date: 2022-6-28
 * @desc: button点击事件
 */
- (void)btnAction{
    DLog(@"游戏中心搜索");
    QJProductClassificationViewController *vc = [QJProductClassificationViewController new];
    [self.navigationController pushViewController:vc animated:YES];
}

-(UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.width , self.view.height) style:UITableViewStyleGrouped];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = [UIColor clearColor];
        [_tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"PhotoHomeFocusCell"];
        if (@available(iOS 11.0, *)) {
            _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
            _tableView.estimatedRowHeight = 0;
            _tableView.estimatedSectionFooterHeight = 0;
            _tableView.estimatedSectionHeaderHeight = 0;
        }
    }
    return _tableView;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 10;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 100;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 45)];
    header.backgroundColor = [UIColor whiteColor];
    
    UILabel *titleLabel = [UILabel new];
    titleLabel.font = kFont(18);
    titleLabel.textColor = HexColor(0x000000);
    titleLabel.text = @"开服表";
    [header addSubview:titleLabel];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@44);
        make.left.equalTo(header).offset(15);
    }];
    
    UILabel *timeLabel = [UILabel new];
    timeLabel.font = kFont(18);
    timeLabel.textColor = HexColor(0x000000);
    timeLabel.text = @"14:00";
    [header addSubview:timeLabel];
    [timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(header);
        make.height.equalTo(@22);
        make.right.equalTo(header).offset(-15);
    }];
    
    UILabel *statusLabel = [UILabel new];
    statusLabel.font = kFont(12);
    statusLabel.textColor = HexColor(0xFFFFFF);
    statusLabel.backgroundColor = HexColor(0xFF9500);
    statusLabel.textAlignment = NSTextAlignmentCenter;
    statusLabel.layer.cornerRadius = 4;
    statusLabel.layer.masksToBounds = YES;
    statusLabel.text = @"已开服";
    [header addSubview:statusLabel];
    [statusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@25);
        make.height.equalTo(@16);
        make.width.equalTo(@50);
        make.right.equalTo(header).offset(-15);
    }];
    
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 44, kScreenWidth, 1)];
    lineView.backgroundColor = HexColor(0xF5F5F5);
    [header addSubview:lineView];
    return header;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 45;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PhotoHomeFocusCell" forIndexPath:indexPath];
    cell.contentView.backgroundColor = [UIColor redColor];
    cell.backgroundColor = [UIColor redColor];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    DLog(@"外部tableview=====滚动==%.2F,self.bottomCellOffset = %f",scrollView.contentOffset.y,self.bottomCellOffset);

    if (scrollView == self.tableView) {
        CGFloat bottomCellOffset = 0;//-(NavigationBar_Bottom_Y);
        if (scrollView.contentOffset.y <= bottomCellOffset) {
            //在下面时可以滑动
            if (!self.tableViewCanScroll) {
                self.tableViewCanScroll = YES;
            }
//            if (!self.tableViewCanScroll) {
//                scrollView.contentOffset = CGPointMake(0, bottomCellOffset);
//                self.tableViewCanScroll = YES;
//            }else{
//                if (scrollView.contentOffset.y <= -self.bottomCellOffset) {
//                    scrollView.contentOffset = CGPointMake(0, -self.bottomCellOffset);
//                }
//            }
        }else{
            //开始悬浮了
            scrollView.contentOffset = CGPointMake(0, bottomCellOffset);
            if (self.tableViewCanScroll) {
                self.tableViewCanScroll = NO;
            }
        }
    }
}

@end
