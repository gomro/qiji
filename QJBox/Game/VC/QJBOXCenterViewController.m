//
//  QJBOXCenterViewController.m
//  QJBox
//
//  Created by rui on 2022/7/22.
//

#import "QJBOXCenterViewController.h"
/* 我的游戏列表 */
#import "QJMyBoxListViewController.h"
/* 自定义view */
#import "QJBoxCenterHeaderView.h"
#import "QJNestScrollView.h"
#import "QJHomeListTableView.h"
#import "QJBoxCenterCell.h"
#import "QJCustomScrollView.h"
/* 游戏分类 */
#import "QJProductClassificationViewController.h"
/* 请求 */
#import "QJProductRequest.h"
/* model */
#import "QJHomeBannerModel.h"
#import "QJSearchGameListModel.h"

@interface QJBOXCenterViewController ()<UITableViewDelegate,UITableViewDataSource>

/* 自定义导航View */
@property (nonatomic, strong) UIView *navView;
/* 搜索按钮 */
@property (nonatomic, strong) UIButton *searchButton;
/* 我的游戏 */
@property (nonatomic, strong) UIButton *myProductButton;
/* 标题 */
@property (nonatomic, strong) UILabel *titleLabel;
/* 背景渐变色 */
@property (nonatomic, strong) QJCustomScrollView *colorView;
/* 头部view */
@property (nonatomic, strong) QJBoxCenterHeaderView *headerView;
/* 主table view */
@property (nonatomic, strong) QJHomeListTableView *tableView;
/* 底部scroll view */
@property (nonatomic, strong) QJNestScrollView *bgScrollView;
/* headerview偏移量 */
@property (nonatomic, assign) CGFloat bottomCellOffset;
/* table是否能滑动 */
@property (nonatomic, assign) BOOL tableViewCanScroll;
/* 底部scroll是否能滑动 */
@property (nonatomic, assign) BOOL isBgScrollCanScroll;
/* banner数据源 */
@property (nonatomic, strong) NSArray *bannerDataArr;//banner数据
/* 我的游戏数据源 */
@property (nonatomic, strong) NSArray *myProductDataArr;//banner数据
/* 动画view */
@property (nonatomic, strong) UIView *animationView;
/* 开服时间 */
@property (nonatomic, strong) UILabel *timeLabel;
/* 开服状态 */
@property (nonatomic, strong) UILabel *statusLabel;

@end

@implementation QJBOXCenterViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    /* 调整自定义导航视图层次 */
    [self.view bringSubviewToFront:self.navView];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navBar.hidden = YES;
    /* 加载自定义导航 */
    [self initNav];
    /* 加载UI */
    [self initUI];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self refreshData];
}

/**
 * @author: zjr
 * @date: 2022-7-22
 * @desc: 自定义导航
 */
- (void)initNav{
    self.view.backgroundColor = UIColorFromRGB(0xF5F5F5);
    self.navView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, QJScreenWidth, NavigationBar_Bottom_Y)];
    self.navView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.navView];
    
    self.searchButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.searchButton setImage:[UIImage imageNamed:@"icon_search"] forState:UIControlStateNormal];
    [self.searchButton setTitle:@"输入关键词搜索游戏" forState:UIControlStateNormal];
    self.searchButton.frame = CGRectMake(15, [QJDeviceConstant top_iPhoneX_SPACE]+27, QJScreenWidth - 30, 34);
    self.searchButton.titleLabel.font = kFont(14);
    [self.searchButton setTitleColor:UIColorFromRGB(0x919599) forState:UIControlStateNormal];
    self.searchButton.backgroundColor = [UIColor whiteColor];
    [self.searchButton addTarget:self action:@selector(btnAction) forControlEvents:UIControlEventTouchUpInside];
    self.searchButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.searchButton.layer.cornerRadius = 15;
    [self.searchButton setImageEdgeInsets:UIEdgeInsetsMake(0, 10, 0, 10)];
    [self.searchButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 20, 0, -20)];
    [self.navView addSubview:self.searchButton];
    
    self.myProductButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.myProductButton setImage:[UIImage imageNamed:@"qj_product_shoubing"] forState:UIControlStateNormal];
    self.myProductButton.frame = CGRectMake(QJScreenWidth-34-15, [QJDeviceConstant top_iPhoneX_SPACE]+27, 34, 34);
    self.myProductButton.backgroundColor = [UIColor whiteColor];
    [self.myProductButton addTarget:self action:@selector(productBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [self.navView addSubview:self.myProductButton];
    self.myProductButton.hidden = YES;
    
    self.colorView = [[QJCustomScrollView alloc] initWithFrame:CGRectMake(0, 0, QJScreenWidth, 254)];
    [self.view addSubview:self.colorView];
    
    self.headerView = [[QJBoxCenterHeaderView alloc] initWithFrame:CGRectMake(0, NavigationBar_Bottom_Y, QJScreenWidth, 318)];
    WS(weakSelf)
    self.headerView.btnClick = ^{
        QJMyBoxListViewController *myBoxListVC = [[QJMyBoxListViewController alloc] init];
        [weakSelf.navigationController pushViewController:myBoxListVC animated:YES];
    };
    self.headerView.bannerClick = ^(QJHomeBannerModel * _Nonnull model) {
        
    };
}

/**
 * @author: zjr
 * @date: 2022-7-26
 * @desc: 加载UI
 */
- (void)initUI{
    self.tableViewCanScroll = YES;
    self.isBgScrollCanScroll = NO;//底部视图开始不能动
    
//    [self.view addSubview:self.headerView];
//    [self.headerView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.height.equalTo(@(318+46));
//        make.top.equalTo(self.view).offset(NavigationBar_Bottom_Y);
//        make.left.equalTo(self.view);
//        make.width.equalTo(@(kScreenWidth));
//    }];
    
    [self.view addSubview:self.bgScrollView];
    [self.bgScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.top.equalTo(self.view).offset(NavigationBar_Bottom_Y);
    }];
    
    [self.bgScrollView addSubview:self.headerView];
    [self.headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@(318+46));
        make.top.equalTo(self.bgScrollView);
        make.left.equalTo(self.view);
        make.width.equalTo(@(kScreenWidth));
    }];
    
    [self.bgScrollView addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.top.equalTo(self.bgScrollView);
    }];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeStatus) name:QJProductNestCellScrollNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeServerTime:) name:QJProductRefreshServerTimeNotification object:nil];
    self.bottomCellOffset = 318+46;
    self.tableView.contentInset = UIEdgeInsetsMake(self.bottomCellOffset, 0, 0, 0);
    self.tableView.tableHeaderView = [self creatHeaderView];
}

/**
 * @author: zjr
 * @date: 2022-7-26
 * @desc: 通知事件
 */
- (void)changeServerTime:(NSNotification *)notification{
    QJSearchGameDetailModel * model = [notification object];
    if(model){
        [UIView transitionWithView:self.animationView duration:1 options:UIViewAnimationOptionTransitionFlipFromBottom animations:^{
            dispatch_async(dispatch_get_main_queue(), ^{
                if (model.serverOpen == NO) {
                    self.statusLabel.text = @"未开服";
                    self.statusLabel.textColor = UIColorFromRGB(0x474849);
                    self.statusLabel.backgroundColor = UIColorFromRGB(0xFFFFFF);
                }else{
                    self.statusLabel.text = @"已开服";
                    self.statusLabel.textColor = UIColorFromRGB(0xFFFFFF);
                    self.statusLabel.backgroundColor = UIColorFromRGB(0xFF9500);
                }
                self.timeLabel.text = model.serverTime;
            });
        } completion:^(BOOL finished) {
            
        }];
    }
}

- (void)changeStatus{
    self.tableViewCanScroll = YES;
}

/**
 * @author: zjr
 * @date: 2022-6-28
 * @desc: button点击事件
 */
- (void)btnAction{
    DLog(@"游戏中心搜索");
    QJProductClassificationViewController *vc = [QJProductClassificationViewController new];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)productBtnAction{
    QJMyBoxListViewController *myBoxListVC = [[QJMyBoxListViewController alloc] init];
    [self.navigationController pushViewController:myBoxListVC animated:YES];
}

#pragma mark - Request
/**
 * @author: zjr
 * @date: 2022-7-26
 * @desc: 数据请求
 */
- (void)refreshData {
    [[NSNotificationCenter defaultCenter] postNotificationName:QJProductRefreshAllDataNotification object:nil];
    [self requestBanner];
    [self requestMyProduct];
}

/**
 * @author: zjr
 * @date: 2022-7-25
 * @desc: 游戏banner
 */
- (void)requestBanner{
    QJProductRequest *productReq = [[QJProductRequest alloc] init];
    NSMutableDictionary *muDic = [NSMutableDictionary dictionary];
    [muDic setValue:@"GAME" forKey:@"location"];
    productReq.dic = muDic.copy;
    [productReq getProductBannerRequest];
    
    WS(weakSelf)
    productReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        [weakSelf.bgScrollView.mj_header endRefreshing];
        if (ResponseSuccess) {
            weakSelf.bannerDataArr = [NSArray modelArrayWithClass:[QJHomeBannerModel class] json:EncodeArrayFromDic(request.responseJSONObject, @"data")];
            weakSelf.headerView.bannerDataArr = weakSelf.bannerDataArr;
            [weakSelf setBackGroundImage];
        }else{
            NSString *msg = EncodeStringFromDic(request.responseObject, @"message");
            [weakSelf.view makeToast:msg];
        }
    };
    productReq.failureCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        [weakSelf.bgScrollView.mj_header endRefreshing];
        [weakSelf.view makeToast:@"数据异常 请稍后再试"];
    };
}

/**
 * @author: zjr
 * @date: 2022-8-2
 * @desc: 设置背景轮播view
 */
- (void)setBackGroundImage{
    NSMutableArray *muArr = [NSMutableArray array];
    for (QJHomeBannerModel *model in self.bannerDataArr) {
        if (!IsStrEmpty(model.backImgUrl)) {
            NSString *str = [model.backImgUrl checkImageUrlString];
            [muArr safeAddObject:str];
        }
    }
    self.colorView.urlImages = muArr.copy;
}

/**
 * @author: zjr
 * @date: 2022-7-25
 * @desc: 我的游戏
 */
- (void)requestMyProduct{
    QJProductRequest *productReq = [[QJProductRequest alloc] init];
    NSMutableDictionary *muDic = [NSMutableDictionary dictionary];
    [muDic setValue:@"3" forKey:@"state"];
    [muDic setValue:@"1" forKey:@"page"];
    [muDic setValue:@"3" forKey:@"size"];
    productReq.dic = muDic.copy;
    [productReq getUserProductRequest];

    WS(weakSelf)
    productReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            weakSelf.myProductDataArr = [NSArray modelArrayWithClass:[QJSearchGameDetailModel class] json:EncodeArrayFromDic(request.responseJSONObject[@"data"], @"records")];
            weakSelf.headerView.myProductDataArr = weakSelf.myProductDataArr;
        }else{
            NSString *msg = EncodeStringFromDic(request.responseObject, @"message");
            [weakSelf.view makeToast:msg];
        }
    };
    productReq.failureCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        [weakSelf.view makeToast:@"数据异常 请稍后再试"];
    };
}

#pragma mark - Lazy Loading
- (QJNestScrollView *)bgScrollView{
    if (!_bgScrollView) {
        _bgScrollView = [[QJNestScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.width, self.view.height)];
        _bgScrollView.backgroundColor = [UIColor clearColor];
//        _bgScrollView.bounces = YES;
        _bgScrollView.delegate = self;
        _bgScrollView.showsVerticalScrollIndicator = NO;
        _bgScrollView.showsHorizontalScrollIndicator = NO;
        _bgScrollView.mj_header = [QJRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(refreshData)];
        if (@available(iOS 11.0, *)) {
            _bgScrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
        _bgScrollView.contentSize = CGSizeMake(QJScreenWidth, QJScreenHeight);
    }
    return _bgScrollView;
}

-(QJHomeListTableView *)tableView{
    if (!_tableView) {
        _tableView = [[QJHomeListTableView alloc] initWithFrame:CGRectMake(0, 0, self.view.width , self.view.height) style:UITableViewStylePlain];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = [UIColor clearColor];
        [_tableView registerClass:[QJBoxCenterCell class] forCellReuseIdentifier:@"QJBoxNestTableViewCell"];
        //去掉顶部偏移
        if (@available(iOS 11.0, *)) {
            _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
        if (@available(iOS 13.0, *)) {
            _tableView.automaticallyAdjustsScrollIndicatorInsets = NO;
        }
    }
    return _tableView;
}

#pragma mark - UITableView
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return kScreenHeight - NavigationBar_Bottom_Y - 50 - Bottom_iPhoneX_SPACE - 49;
}

/**
 * @author: zjr
 * @date: 2022-8-2
 * @desc: 创建开服表view
 */
- (UIView *)creatHeaderView{
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 50)];
    header.backgroundColor = [UIColor whiteColor];
    
    UIImageView *titleIconImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"qj_product_service"]];
    [header addSubview:titleIconImage];
    [titleIconImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(header);
        make.left.equalTo(header).offset(15);
    }];
    
    UIView *animationView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 50)];
    animationView.backgroundColor = [UIColor whiteColor];
    [header addSubview:animationView];
    [animationView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(header);
        make.height.equalTo(@50);
        make.width.equalTo(@80);
        make.right.equalTo(header).offset(-15);
    }];
    self.animationView = animationView;
    
    UILabel *timeLabel = [UILabel new];
    timeLabel.font = kboldFont(18);
    timeLabel.textColor = UIColorFromRGB(0x000000);
    timeLabel.textAlignment = NSTextAlignmentCenter;
    timeLabel.text = @"";
    [self.animationView addSubview:timeLabel];
    [timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.animationView).offset(6);
        make.height.equalTo(@22);
        make.left.right.equalTo(self.animationView);
    }];
    self.timeLabel = timeLabel;
    
    UILabel *statusLabel = [UILabel new];
    statusLabel.font = kFont(12);
    statusLabel.textColor = UIColorFromRGB(0xFFFFFF);
    statusLabel.backgroundColor = UIColorFromRGB(0xFFFFFF);
    statusLabel.textAlignment = NSTextAlignmentCenter;
    statusLabel.layer.cornerRadius = 4;
    statusLabel.layer.masksToBounds = YES;
    statusLabel.text = @"";
    [self.animationView addSubview:statusLabel];
    [statusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@30);
        make.height.equalTo(@16);
        make.width.equalTo(@50);
        make.centerX.equalTo(self.animationView);
    }];
    self.statusLabel = statusLabel;
    
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 49, kScreenWidth, 1)];
    lineView.backgroundColor = UIColorFromRGB(0xF5F5F5);
    [header addSubview:lineView];
    return header;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    QJBoxCenterCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QJBoxNestTableViewCell" forIndexPath:indexPath];
    return cell;
}

#pragma mark - scrollview
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
//    if (scrollView == self.bgScrollView) {
//        if (scrollView.contentOffset.y >= 0.01) {
//            self.isBgScrollCanScroll = NO;
//        }else{
//            self.isBgScrollCanScroll = YES;
//        }
//    }else{
//        if (scrollView.contentOffset.y == -self.bottomCellOffset) {
//            self.isBgScrollCanScroll = YES;
//        }else{
//            self.isBgScrollCanScroll = NO;
//        }
//    }
    
    if (scrollView == self.tableView) {
        DLog(@"外部tableview=====滚动==%.2F",scrollView.contentOffset.y);
        /* 导航渐变背景色 */
        CGFloat offset = scrollView.contentOffset.y + self.bottomCellOffset;
        CGFloat alp = offset/self.bottomCellOffset;
        if (alp <= 0 ) {
            alp = 0;
        }else if (alp >= 1) {
            alp = 1;
        }
        
        self.navView.backgroundColor = [UIColor colorWithWhite:1 alpha:alp];
        if (alp == 1) {
            self.searchButton.backgroundColor = UIColorFromRGB(0xF5F5F5);
            self.searchButton.width = QJScreenWidth - 30 - 45;
            self.myProductButton.hidden = NO;
        }else{
            self.searchButton.backgroundColor = UIColorFromRGB(0xFFFFFF);
            self.searchButton.width = QJScreenWidth - 30;
            self.myProductButton.hidden = YES;
        }
        
        if (scrollView.contentOffset.y == -self.bottomCellOffset) {
            self.isBgScrollCanScroll = YES;
        }else{
            self.isBgScrollCanScroll = NO;
        }
        
        /*
         当 底层滚动图滚动到指定位置时，
         停止滚动，开始滚动子视图
         */
        CGFloat bottomCellOffset = 0;//-(NavigationBar_Bottom_Y);
        if (scrollView.contentOffset.y <= bottomCellOffset) {
            //在下面时可以滑动
            if (!self.tableViewCanScroll) {//
                scrollView.contentOffset = CGPointMake(0, bottomCellOffset);
            }else{
                if (scrollView.contentOffset.y <= -self.bottomCellOffset) {
                    scrollView.contentOffset = CGPointMake(0, -self.bottomCellOffset);
                }
            }
        }else{
            //开始悬浮了
            scrollView.contentOffset = CGPointMake(0, bottomCellOffset);
            
            [self.view bringSubviewToFront:self.navBar];
            [[NSNotificationCenter defaultCenter] postNotificationName:QJProductNestMainCellScrollNotification object:nil];
            if (self.tableViewCanScroll) {
                self.tableViewCanScroll = NO;
            }
        }
    }else{
//        DLog(@"bgScrollview------滚动==%.2F",scrollView.contentOffset.y);
        if (!self.isBgScrollCanScroll) {
            scrollView.contentOffset = CGPointMake(0, 0);
        }
    }
}

@end
