//
//  QJProductEvaluateViewController.h
//  QJBox
//
//  Created by rui on 2022/7/27.
//

#import "QJBaseViewController.h"
#import "QJSearchGameListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJProductEvaluateViewController : QJBaseViewController

@property (nonatomic, strong) QJSearchGameDetailModel *detailModel;

@end

NS_ASSUME_NONNULL_END
