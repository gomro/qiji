//
//  QJProductClassificationViewController.h
//  QJBox
//
//  Created by wxy on 2022/7/25.
//

#import "QJBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

/// 游戏分类模块
@interface QJProductClassificationViewController : QJBaseViewController

@property (nonatomic, assign) BOOL isFromGameCenter;




@end

NS_ASSUME_NONNULL_END
