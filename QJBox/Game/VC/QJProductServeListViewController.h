//
//  QJProductServeListViewController.h
//  QJBox
//
//  Created by rui on 2022/7/28.
//

#import "QJBaseViewController.h"
#import "QJSearchGameListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJProductServeListViewController : QJBaseViewController

@property (nonatomic, strong) QJSearchGameDetailModel *detailModel;

@end

NS_ASSUME_NONNULL_END
