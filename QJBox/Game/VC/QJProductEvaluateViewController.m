//
//  QJProductEvaluateViewController.m
//  QJBox
//
//  Created by rui on 2022/7/27.
//

#import "QJProductEvaluateViewController.h"
/* 发表评价页面 */
#import "QJPublishEvaluateViewController.h"

#import "QJSegmentLineView.h"
/* 自定义评价列表headerview */
#import "QJProductEvaluateHeaderView.h"
/* 自定义产品评论view */
#import "QJProductCommentListView.h"
/* 请求 */
#import "QJProductRequest.h"

#import "GKPageScrollView.h"

@interface QJProductEvaluateViewController ()<GKPageScrollViewDelegate,GKPageTableViewGestureDelegate,UIScrollViewDelegate>

/* View */
@property (nonatomic, strong) GKPageScrollView  *pageScrollView;
@property (nonatomic, strong) NSMutableArray    *childVCs;
@property (nonatomic, strong) UIScrollView      *scrollView;
@property (nonatomic, strong) UIView            *pageView;
@property (nonatomic, strong) QJSegmentLineView *segment;
/* 发表button */
@property (nonatomic, strong) UIButton *publishButton;
/* 自定义评价headerView */
@property (nonatomic, strong) QJProductEvaluateHeaderView *headerView;
/* GKScrollView回调 */
@property (nonatomic,   copy) void(^listScrollViewDidScroll)(UIScrollView *scrollView);

@end

@implementation QJProductEvaluateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"评论";
    self.navBar.bottomLine.hidden = NO;
    /* 加载UI */
    [self initUI];
    /* 请求数据 */
    [self initData];
}

/**
 * @author: zjr
 * @date: 2022-8-2
 * @desc: 加载UI
 */
- (void)initUI{
    [self.view addSubview:self.pageScrollView];
    [self.pageScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.top.equalTo(self.view.mas_top).offset(NavigationBar_Bottom_Y);
    }];
    
    for (int i = 0; i < 3 ; i ++) {
        QJProductCommentListView *view = [[QJProductCommentListView alloc] initWithFrame:CGRectMake(0, 0, QJScreenWidth, QJScreenHeight - NavigationBar_Bottom_Y)];
        if (i == 0) {
            view.type = @"starSort";
        }else if (i == 1) {
            view.type = @"timeSort";
        }else {
            view.type = @"myPublish";
        }
        view.requestSuccessClick = ^(NSString * _Nonnull countStr) {
            [self.headerView updateCount:countStr];
        };
        view.productID = self.detailModel.ID;
        [view refreshFirst];
        [self.childVCs addObject:view];
    }
    [self.childVCs enumerateObjectsUsingBlock:^(UIView *vc, NSUInteger idx, BOOL * _Nonnull stop) {
        [self.scrollView addSubview:vc];
        vc.frame = CGRectMake(idx * QJScreenWidth, 0, QJScreenWidth, QJScreenHeight - 44*kWScale - NavigationBar_Bottom_Y);
    }];
    _scrollView.contentSize = CGSizeMake(self.childVCs.count * QJScreenWidth, 0);
    
    [self.pageScrollView reloadData];
    
    // 发布
    _publishButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [_publishButton setImage:[UIImage imageNamed:@"qj_productdetail_publishevaluate"] forState:UIControlStateNormal];
    [_publishButton addTarget:self action:@selector(publishEvaBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_publishButton];
    [self.publishButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.view).offset(-108);
        make.right.equalTo(self.view).offset(-12);
    }];
    
}

/**
 * @author: zjr
 * @date: 2022-8-2
 * @desc: header赋值
 */
- (void)initData{
    QJProductRequest *productReq = [[QJProductRequest alloc] init];
    [productReq getProductGrade:self.detailModel.ID];
    
    WS(weakSelf)
    productReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            NSString *point = EncodeStringFromDic(request.responseObject, @"data");
            weakSelf.detailModel.point = point;
            weakSelf.headerView.detailModel = weakSelf.detailModel;
        }
    };
    productReq.failureCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        weakSelf.headerView.detailModel = weakSelf.detailModel;
    };
}

/**
 * @author: zjr
 * @date: 2022-8-2
 * @desc: button点击事件
 */
- (void)publishEvaBtn{
    QJPublishEvaluateViewController *publishVC = [[QJPublishEvaluateViewController alloc] init];
    publishVC.detailModel = self.detailModel;
    WS(weakSelf)
    publishVC.pulishSuccessClick = ^{
        [weakSelf initData];
        
        [weakSelf.childVCs enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            QJProductCommentListView *view = (QJProductCommentListView *)obj;
            [view refreshFirst];
        }];
    };
    [self.navigationController pushViewController:publishVC animated:YES];
}

#pragma mark - Lazy Loading
- (GKPageScrollView *)pageScrollView {
    if (!_pageScrollView) {
        _pageScrollView = [[GKPageScrollView alloc] initWithDelegate:self];
        _pageScrollView.ceilPointHeight = 0;
        _pageScrollView.mainTableView.gestureDelegate = self;
        _pageScrollView.isAllowListRefresh = YES;
    }
    return _pageScrollView;
}

- (QJProductEvaluateHeaderView *)headerView {
    if (!_headerView) {
        _headerView = [[QJProductEvaluateHeaderView alloc] initWithFrame:CGRectMake(0, 0, self.view.width, 144*kWScale)];
        _headerView.backgroundColor = [UIColor whiteColor];
    }
    return _headerView;
}

- (UIView *)pageView {
    if (!_pageView) {
        _pageView = [UIView new];
        _pageView.backgroundColor = [UIColor whiteColor];
        [_pageView addSubview:self.segment];
        [_pageView addSubview:self.scrollView];
    }
    return _pageView;
}

- (QJSegmentLineView *)segment {
    if (!_segment) {
        _segment = [[QJSegmentLineView alloc] initWithFrame:CGRectMake(0, 0, QJScreenWidth, 44*kWScale)];
        _segment.backgroundColor = [UIColor whiteColor];
        _segment.segmentWidthStyle = QJSegmentViewWidthStyleAuto;
        _segment.font = kFont(16);
        _segment.selectedFont = kboldFont(16);
        _segment.textColor = UIColorFromRGB(0x16191C);
        _segment.selectedTextColor = UIColorFromRGB(0xFF9500);
        _segment.leftOffset = 16;
        _segment.itemSpace = 16;
        _segment.showBottomLine = NO;
        _segment.tag = 10000;
        [_segment setTitleArray:@[@"默认",@"最新",@"我的",]];
        WS(weakSelf)
        [_segment setSegmentedItemSelectedBlock:^(QJSegmentLineView * _Nonnull segment, NSInteger selectedIndex) {
            [weakSelf.scrollView setContentOffset:CGPointMake(QJScreenWidth * selectedIndex, 0) animated:NO];
        }];
    }
    return _segment;
}

- (UIScrollView *)scrollView {
    if (!_scrollView) {
        CGFloat scrollW = self.view.width;
        CGFloat scrollH = QJScreenHeight - 44*kWScale - NavigationBar_Bottom_Y;
        
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 44, scrollW, scrollH)];
        _scrollView.backgroundColor = [UIColor whiteColor];
        _scrollView.pagingEnabled = YES;
        _scrollView.directionalLockEnabled = YES;
//        _scrollView.scrollEnabled = NO;
        _scrollView.bounces = NO;
        _scrollView.delegate = self;
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.showsHorizontalScrollIndicator = NO;
    }
    return _scrollView;
}

- (NSMutableArray *)childVCs {
    if (!_childVCs) {
        _childVCs = [NSMutableArray array];
    }
    return _childVCs;
}

#pragma mark - GKPageScrollView
- (BOOL)mainTableViewGestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    //禁止categoryView左右滑动的时候，上下和左右都可以滚动
    NSArray *disableViewTag = @[@103019, @103022,@112233];
    if ( [disableViewTag containsObject:@(otherGestureRecognizer.view.tag)]) {
        return NO;
    }
    return [gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]] && [otherGestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]];
}

- (UIView *)headerViewInPageScrollView:(GKPageScrollView *)pageScrollView {
    return self.headerView;
}

- (NSArray<id<GKPageListViewDelegate>> *)listViewsInPageScrollView:(GKPageScrollView *)pageScrollView {
    return self.childVCs;
}

- (UIView *)pageViewInPageScrollView:(GKPageScrollView *)pageScrollView {
    return self.pageView;
}

- (void)listViewDidScrollCallback:(void (^)(UIScrollView * _Nonnull))callback {
    self.listScrollViewDidScroll = callback;
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [self.pageScrollView horizonScrollViewWillBeginScroll];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    int page = (_scrollView.contentOffset.x+self.view.width/2.0) / self.view.width;
    [_segment setSegmentSelectedIndex:page];

    [self.pageScrollView horizonScrollViewDidEndedScroll];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    [self.pageScrollView horizonScrollViewDidEndedScroll];
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    !self.listScrollViewDidScroll ? : self.listScrollViewDidScroll(scrollView);
}

@end
