//
//  QJProductClassificationSearchResultVC.m
//  QJBox
//
//  Created by wxy on 2022/7/26.
//

#import "QJProductClassificationSearchResultVC.h"
#import "QJProductClassificationModel.h"
#import "QJProductSearchView.h"
#import "QJHomeMainCell.h"
#import "QJSegmentLineView.h"
#import "QJSearchGamePostRequest.h"
#import "QJProductClassificationSearchResultView.h"
#import "QJProductDetailViewController.h"
@interface QJProductClassificationSearchResultVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) QJProductSearchView *searchView;

@property (nonatomic, strong) QJProductClassificationSearchResultView *originalView;

@property (nonatomic, strong) QJProductClassificationSearchResultView *keyWrodResultView;//关键字搜索的结果页
@end

@implementation QJProductClassificationSearchResultVC



- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self makeUI];
    
    
    
}

#pragma mark --- method

-(void)makeUI {
    WS(weakSelf)
    self.navBar.backgroundColor = kColorWithHexString(@"#Ffffff");
    [self.navBar addSubview:self.searchView];
    self.searchView.searchStrBlock = ^(NSString * _Nonnull str) {
        weakSelf.keyWrodResultView.model = weakSelf.model;
        weakSelf.keyWrodResultView.tagOptionModel = weakSelf.tagOptionModel;
        [weakSelf.keyWrodResultView searchDataWithKeyWords:str];
        weakSelf.keyWrodResultView.hidden = NO;
        weakSelf.originalView.hidden = YES;
    };
    
    self.searchView.beginEditBlock = ^{
        [weakSelf.searchView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.left.equalTo(weakSelf.navBar).offset(13);
                    make.centerY.equalTo(weakSelf.navBar.backButton);
                    make.height.equalTo(@34);
                    make.right.equalTo(weakSelf.navBar);
        }];
        
    };
    
    self.searchView.cancelBlock = ^{
        weakSelf.keyWrodResultView.hidden = YES;
        weakSelf.originalView.hidden = NO;
        [weakSelf.searchView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(weakSelf.navBar.backButton.mas_right).offset(8);
            make.centerY.equalTo(weakSelf.navBar.backButton);
            make.height.equalTo(@34);
            make.right.equalTo(weakSelf.navBar);
        }];
        
        
    };
    [self.searchView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.navBar.backButton.mas_right).offset(8);
        make.centerY.equalTo(self.navBar.backButton);
        make.height.equalTo(@34);
        make.right.equalTo(self.navBar);
    }];
    
    [self.view addSubview:self.originalView];
    [self.view addSubview:self.keyWrodResultView];
 
    self.originalView.didCellBlock = ^(QJSearchGameDetailModel * _Nonnull model) {
        DLog(@"进入游戏详情");
        QJProductDetailViewController *vc = [QJProductDetailViewController new];
        vc.model = model;
        [weakSelf.navigationController pushViewController:vc animated:YES];
    };
    
   
    self.keyWrodResultView.didCellBlock = ^(QJSearchGameDetailModel * _Nonnull model) {
        DLog(@"进入游戏详情");
        QJProductDetailViewController *vc = [QJProductDetailViewController new];
        vc.model = model;
        [weakSelf.navigationController pushViewController:vc animated:YES];
    };
    [self.originalView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.navBar.mas_bottom).offset(4);
            make.left.right.equalTo(self.view);
            make.bottom.equalTo(self.view).offset(-Bottom_iPhoneX_SPACE);
    }];
    
    [self.keyWrodResultView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.originalView);
    }];
    
    self.originalView.tagOptionModel = self.tagOptionModel;
    self.originalView.model = self.model;
    [self.originalView searchDataWithKeyWords:@""];
}

#pragma  mark  ----- getter

- (QJProductSearchView *)searchView {
    if (!_searchView) {
        _searchView = [QJProductSearchView new];
        
    }
    return _searchView;
}

- (QJProductClassificationSearchResultView *)originalView {
    if (!_originalView) {
        _originalView = [QJProductClassificationSearchResultView new];
        
    }
    return _originalView;
}
 

- (QJProductClassificationSearchResultView *)keyWrodResultView {
    if (!_keyWrodResultView) {
        _keyWrodResultView = [QJProductClassificationSearchResultView new];
        _keyWrodResultView.hidden = YES;
    }
    return _keyWrodResultView;
}
@end
