//
//  QJPublishEvaluateViewController.h
//  QJBox
//
//  Created by rui on 2022/7/27.
//

#import "QJBaseViewController.h"
#import "QJSearchGameListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJPublishEvaluateViewController : QJBaseViewController

@property (nonatomic, strong) QJSearchGameDetailModel *detailModel;
/* 发表成功回调 */
@property (nonatomic, copy) void(^pulishSuccessClick)(void);

@end

NS_ASSUME_NONNULL_END
