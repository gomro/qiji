//
//  QJPublishEvaluateViewController.m
//  QJBox
//
//  Created by rui on 2022/7/27.
//

#import "QJPublishEvaluateViewController.h"
/* 自定义星星 */
#import "QJEvaluateStarView.h"
/* 弹框 */
#import "QJDetailAlertView.h"
#import "TYAlertController.h"
/* 请求 */
#import "QJMineMsgRequest.h"

@interface QJPublishEvaluateViewController ()<UITextViewDelegate>

/* 文本输入 */
@property (nonatomic, strong) UITextView *textView;
/* 数量提示 */
@property (nonatomic, strong) UILabel *numLabel;
/* 默认文案 */
@property (nonatomic, strong) UILabel *placeholderLabel;
/* 分割线 */
@property (nonatomic, strong) UIView *lineView;
/* 标题 */
@property (nonatomic, strong) UILabel *evaluateLabel;
/* 发布按钮 */
@property (nonatomic, strong) UIButton *publishButton;
/* 评分 */
@property (nonatomic, strong) QJEvaluateStarView *starView;
/* 评分状态 */
@property (nonatomic, strong) UILabel *satrStatusLabel;
/* 选择评分数量 */
@property (nonatomic, strong) NSString *chooseStar;

@end

@implementation QJPublishEvaluateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"发表评论";
    self.view.backgroundColor = [UIColor whiteColor];
    self.navBar.bottomLine.hidden = NO;
    self.chooseStar = @"5";
    /* 加载UI */
    [self initUI];
}

/**
 * @author: zjr
 * @date: 2022-8-2
 * @desc: 返回事件判断内容
 */
- (void)onBack:(id)sender{
    if (self.textView.text.length > 0) {
        QJDetailAlertView *alertView = [[QJDetailAlertView alloc] initWithFrame:CGRectZero titleType:QJDetailAlertViewTitleTypeDefault buttonType:QJDetailAlertViewButtonTypeDefault];
        [alertView showTitleText:@"提示" describeText:@"您还没发表评论确定要退出吗？"];
        [alertView.leftButton setTitle:@"取消" forState:UIControlStateNormal];
        [alertView.rightButton setTitle:@"确认" forState:UIControlStateNormal];
        [alertView.rightButton setTitleColor:UIColorFromRGB(0x007AFF) forState:UIControlStateNormal];
        WS(weakSelf)
        [alertView returnClick:^(QJDetailAlertViewButtonClickType type) {
            [[QJAppTool getCurrentViewController] dismissViewControllerAnimated:YES completion:nil];
            if (type == QJDetailAlertViewButtonClickTypeRight) {
                [weakSelf.navigationController popViewControllerAnimated:YES];
            }
        }];
        TYAlertController *alertController = [TYAlertController alertControllerWithAlertView:alertView preferredStyle:TYAlertControllerStyleAlert];
        [self.navigationController presentViewController:alertController animated:YES completion:nil];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}

/**
 * @author: zjr
 * @date: 2022-8-2
 * @desc: 加载view/约束
 */
- (void)initUI{
    [self.view addSubview:self.textView];
    [self.textView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(NavigationBar_Bottom_Y+8);
        make.left.right.mas_equalTo(0);
        make.height.mas_equalTo(284*kWScale);
    }];
    
    [self.view addSubview:self.numLabel];
    [self.numLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.textView.mas_right).mas_offset(-10);
        make.bottom.mas_equalTo(self.textView.mas_bottom).mas_offset(-10);
    }];
    
    [self.textView addSubview:self.placeholderLabel];
    [self.placeholderLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(10);
        make.left.mas_equalTo(20);
    }];
    
    self.lineView = [[UIView alloc] initWithFrame:CGRectMake(.0f,self.textView.bottom+NavigationBar_Bottom_Y,kScreenWidth,0.5)];
    self.lineView.backgroundColor = [UIColor colorWithRed:230/255.0 green:230/255.0 blue:230/255.0 alpha:1];
    [self.view addSubview:self.lineView];
    
    [self.view addSubview:self.evaluateLabel];
    [self.evaluateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.textView.mas_bottom).offset(25);
        make.left.equalTo(self.view).offset(16);
    }];
    
    [self.view addSubview:self.starView];
    [self.starView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.evaluateLabel);
        make.left.equalTo(self.evaluateLabel.mas_right).offset(5);
        make.size.mas_equalTo(CGSizeMake(148*kWScale,20*kWScale));
    }];
    [self.starView updateWithIndex:5];
    WS(weakSelf)
    self.starView.starClick = ^(NSInteger index) {
        DLog(@"选中了几个星星 = %ld",index);
        if (index == 0) {
            weakSelf.satrStatusLabel.text = @"非常差";
        }else if (index == 1) {
            weakSelf.satrStatusLabel.text = @"差";
        }else if (index == 2) {
            weakSelf.satrStatusLabel.text = @"一般";
        }else if (index == 3) {
            weakSelf.satrStatusLabel.text = @"好";
        }else{
            weakSelf.satrStatusLabel.text = @"非常好";
        }
        weakSelf.chooseStar = [NSString stringWithFormat:@"%ld",index+1];
        [weakSelf resetPublishButton];
    };
    
    [self.view addSubview:self.satrStatusLabel];
    [self.satrStatusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.starView);
        make.left.equalTo(self.starView.mas_right).offset(10);
    }];
    
    self.placeholderLabel.hidden = NO;
    
    self.publishButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.publishButton addTarget:self action:@selector(publishButtonAction) forControlEvents:UIControlEventTouchUpInside];
    self.publishButton.titleLabel.font = kFont(16);
    self.publishButton.backgroundColor = UIColorFromRGB(0xC8CACC);//0x1F2A4D
    [self.publishButton setTitle:@"发表" forState:UIControlStateNormal];
    [self.publishButton setTitleColor:UIColorFromRGB(0xFFFFFF) forState:UIControlStateNormal];
    self.publishButton.layer.cornerRadius = 4;
    self.publishButton.layer.masksToBounds = YES;
    self.publishButton.userInteractionEnabled = NO;
    [self.view addSubview:self.publishButton];
    [self.publishButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view);
        make.width.equalTo(@(300*kWScale));
        make.height.equalTo(@(48*kWScale));
        make.bottom.equalTo(self.view).offset(-Bottom_iPhoneX_SPACE);
    }];
}

/**
 * @author: zjr
 * @date: 2022-8-2
 * @desc: 重置发布按钮状态
 */
- (void)resetPublishButton{
    if (self.chooseStar.integerValue > 0 && self.textView.text.length > 0) {
        self.publishButton.userInteractionEnabled = YES;
        self.publishButton.backgroundColor = UIColorFromRGB(0x1F2A4D);
    }else{
        self.publishButton.userInteractionEnabled = NO;
        self.publishButton.backgroundColor = UIColorFromRGB(0xC8CACC);
    }
}

/**
 * @author: zjr
 * @date: 2022-8-2
 * @desc: button点击事件-发布
 */
- (void)publishButtonAction{
    [QJAppTool showHUDLoading];
    @weakify(self);
    QJMineMsgRequest *request = [[QJMineMsgRequest alloc] init];
    [request requestAddComment:self.textView.text replyCommentId:@"" sourceId:self.detailModel.ID levelComment:self.chooseStar type:@"0" completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        [QJAppTool hideHUDLoading];
        @strongify(self);
        if (isSuccess) {
            [self.view makeToast:@"发表成功"];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                if (self.pulishSuccessClick) {
                    self.pulishSuccessClick();
                }
                [self.navigationController popViewControllerAnimated:YES];
            });
        }
    }];
}

#pragma mark - UITextView
- (void)textViewDidChangeSelection:(UITextView *)textView {
    if ([textView.text isEqualToString:@""]){
        [self.placeholderLabel setHidden:NO];
    }else{
        [self.placeholderLabel setHidden:YES];
    }
}

-(void)textViewDidChange:(UITextView *)textView {
    UITextRange *selectedRange = [textView markedTextRange];
    [self resetPublishButton];
    //获取高亮部分
    UITextPosition *pos = [textView positionFromPosition:selectedRange.start offset:0];
   //如果在变化中是高亮部分在变，就不要计算字符了
    if (selectedRange && pos) {
        return;
    }
    if (textView.text.length > 500) {
        textView.text = [textView.text substringToIndex:500];
    }

    self.numLabel.text = [NSString stringWithFormat:@"%ld/500",textView.text.length];
    
}

#pragma mark - Lazy Loading
- (UITextView *)textView {
    if (!_textView) {
        _textView = [[UITextView alloc] initWithFrame:CGRectMake(16, 100, kScreenWidth - 32, 200*kWScale)];
        _textView.backgroundColor = [UIColor whiteColor];
        _textView.textContainerInset = UIEdgeInsetsMake(10, 10, 10, 10);
        _textView.textColor = UIColorFromRGB(0x16191c);
        _textView.font = FontRegular(16);
        _textView.delegate = self;
    }
    return _textView;
}

- (UILabel *)numLabel {
    if (!_numLabel) {
        _numLabel = [[UILabel alloc] init];
        _numLabel.textColor = UIColorFromRGB(0xC8CACC);
        _numLabel.font = FontRegular(14);
        _numLabel.text = @"0/500";
    }
    return _numLabel;
}

- (UILabel *)evaluateLabel {
    if (!_evaluateLabel) {
        _evaluateLabel = [[UILabel alloc] init];
        _evaluateLabel.textColor = UIColorFromRGB(0x919599);
        _evaluateLabel.font = FontRegular(16);
        _evaluateLabel.text = @"评论";
    }
    return _evaluateLabel;
}

- (UILabel *)satrStatusLabel {
    if (!_satrStatusLabel) {
        _satrStatusLabel = [[UILabel alloc] init];
        _satrStatusLabel.textColor = UIColorFromRGB(0xFF9500);
        _satrStatusLabel.font = FontRegular(14);
        _satrStatusLabel.text = @"非常好";
    }
    return _satrStatusLabel;
}

- (QJEvaluateStarView *)starView{
    if (!_starView) {
        _starView = [[QJEvaluateStarView alloc] initWithFrame:CGRectMake(0, 0, 148*kWScale, 20*kWScale)];
    }
    return _starView;
}

- (UILabel *)placeholderLabel {
    if (!_placeholderLabel) {
        _placeholderLabel = [[UILabel alloc] init];
        _placeholderLabel.text = @"分享您对这款游戏的想法";
        _placeholderLabel.textColor = UIColorFromRGB(0x919599);
        _placeholderLabel.font = FontRegular(16);
    }
    return _placeholderLabel;
}

@end
