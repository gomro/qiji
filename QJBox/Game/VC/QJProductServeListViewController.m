//
//  QJProductServeListViewController.m
//  QJBox
//
//  Created by rui on 2022/7/28.
//

#import "QJProductServeListViewController.h"
#import "QJProductTimeLineCell.h"

#import "QJSearchGameListModel.h"
#import "QJProductRequest.h"

@interface QJProductServeListViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) QJSearchGameListModel *pageModel;

@end

@implementation QJProductServeListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"开服表";
    self.navBar.backgroundColor = UIColorFromRGB(0xF5F5F5);
    self.pageModel = [QJSearchGameListModel new];
    /* 加载UI */
    [self initUI];
    [self refreshData];
}

- (void)initUI{
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.top.equalTo(self.view).offset(NavigationBar_Bottom_Y+1);
    }];
}

-(UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.width , self.view.height) style:UITableViewStylePlain];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = [UIColor whiteColor];
        [_tableView registerClass:[QJProductTimeLineCell class] forCellReuseIdentifier:@"QJProductTimeLineListCell"];
        _tableView.estimatedRowHeight = 100;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        if (@available(iOS 11.0, *)) {
            _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
        if (@available(iOS 15.0, *)) {
            _tableView.sectionHeaderTopPadding = 0;
        }
        _tableView.mj_header = [QJRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(refreshData)];
        _tableView.mj_footer = [QJRefreshFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreHomeData)];
//        _tableView.mj_footer = [QJRefreshFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreHomeData)];
    }
    return _tableView;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.pageModel.records.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 20;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [[UIView alloc] init];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    QJSearchGameDetailModel *model = [self.pageModel.records safeObjectAtIndex:indexPath.row];
    QJSearchGameDetailModel *lastModel = [self.pageModel.records safeObjectAtIndex:indexPath.row-1];
    QJSearchGameDetailModel *nextModel = [self.pageModel.records safeObjectAtIndex:indexPath.row+1];
    if (nextModel && nextModel.serverOpen == YES) {
        model.isShowNextOpen = YES;
    }else{
        model.isShowNextOpen = NO;
    }
    if (!lastModel) {
        model.isShowLastOpen = YES;
    }else if (lastModel && lastModel.serverOpen == YES) {
        model.isShowLastOpen = YES;
    }else {
        model.isShowLastOpen = NO;
    }
    QJProductTimeLineCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QJProductTimeLineListCell" forIndexPath:indexPath];
    model.indexRow = indexPath.row+1;
    cell.detailModel = model;
    return cell;
}

#pragma mark - 请求方法
- (void)refreshData {
    _pageModel.willLoadMore = NO;
    [self sendRequest];
}

- (void)loadMoreHomeData {
    DLog(@"加载更多");
    if(_pageModel.hasNext){
        _pageModel.willLoadMore=YES;
        [self sendRequest];
    }else{
        [self.tableView.mj_footer endRefreshing];
        [self.tableView.mj_footer endRefreshingWithNoMoreData];
    }
}

/**
 * @author: zjr
 * @date: 2022-7-7
 * @desc: 搜索游戏
 */
- (void)sendRequest{
    QJProductRequest *productReq = [[QJProductRequest alloc] init];
    NSMutableDictionary *muDic = [NSMutableDictionary dictionaryWithDictionary:_pageModel.toParams];
    [muDic setValue:@"10" forKey:@"size"];
    if (_detailModel.ID) {
        [muDic setValue:_detailModel.ID forKey:@"id"];
    }
    productReq.dic = muDic.copy;
    [productReq getProductTimeLineRequest];

    WS(weakSelf)
    productReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            [weakSelf.pageModel configObj:[QJSearchGameListModel modelWithDictionary:EncodeDicFromDic(request.responseObject, @"data")]];
        }else{
            NSString *msg = EncodeStringFromDic(request.responseObject, @"message");
            [weakSelf.view makeToast:msg];
        }
        [weakSelf.tableView reloadData];
        [weakSelf.tableView.mj_header endRefreshing];
        QJEmptyCommonView *emptyView = [QJEmptyCommonView new];
        emptyView.string = @"暂无内容";
        emptyView.emptyImage = @"empty_no_data";
        emptyView.topSpace = 100;
        [weakSelf.tableView tableViewDisplayWhenHaveNoDataWithView:emptyView ifNecessaryForRowCount:weakSelf.pageModel.records.count];
    };
    productReq.failureCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        [weakSelf.tableView reloadData];
        [weakSelf.tableView.mj_header endRefreshing];
        [weakSelf.view makeToast:@"数据异常 请稍后再试"];
        QJEmptyCommonView *emptyView = [QJEmptyCommonView new];
        emptyView.string =@"暂无内容";
        emptyView.emptyImage = @"empty_no_data";
        emptyView.topSpace = 100;
        [weakSelf.tableView tableViewDisplayWhenHaveNoDataWithView:emptyView ifNecessaryForRowCount:weakSelf.pageModel.records.count];
    };
    
}

@end

