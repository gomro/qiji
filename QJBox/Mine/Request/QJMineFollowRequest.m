//
//  QJMineFollowRequest.m
//  QJBox
//
//  Created by wxy on 2022/7/19.
//

#import "QJMineFollowRequest.h"

@implementation QJMineFollowRequest



- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"/user/%@/star/%@",self.userID,self.type];
}


 

- (YTKRequestMethod)requestMethod {
    
    return YTKRequestMethodPOST;
}


- (YTKRequestSerializerType)requestSerializerType {
    
    return YTKRequestSerializerTypeHTTP;
}





@end
