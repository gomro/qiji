//
//  QJMineRequest.m
//  QJBox
//
//  Created by macm on 2022/7/19.
//

#import "QJMineRequest.h"

@implementation QJMineRequest

- (void)sendRequest_completion:(RequestBlock)completion {
    [self start];
    
    [self setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        if (completion) {
            completion(ResponseSuccess, request);
        }
        if (!ResponseSuccess) {
            [[QJAppTool getCurrentViewController].view makeToast:EncodeStringFromDic(request.responseObject, @"message") duration:2 position:CSToastPositionCenter image:[UIImage imageNamed:@"tips_toast"]];
        }
    }];
    
    [self setFailureCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        if (completion) {
            completion(NO, request);
        }
        [[QJAppTool getCurrentViewController].view makeToast:@"网络请求失败" duration:2 position:CSToastPositionCenter image:[UIImage imageNamed:@"tips_toast"]];
    }];
}

/**获取'我的'个人信息*/
- (void)requestUserInfoMine_completion:(RequestBlock)completion {
    self.urlStr = @"/user/info/mine";
    self.requestType = YTKRequestMethodGET;
    [self start];
    
    [self setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        if (completion) {
            completion(ResponseSuccess, request);
        }
        
    }];
    
    [self setFailureCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        if (completion) {
            completion(NO, request);
        }
    }];
    
}

/**获取用户关注列表*/
- (void)requestUserStars:(NSString *)userId page:(NSInteger)page completion:(RequestBlock)completion {
    self.dic = @{@"page":@(page), @"size":@(10)};

    self.urlStr = [NSString stringWithFormat:@"/user/stars/%@", userId];
    self.requestType = YTKRequestMethodGET;
    
    [self sendRequest_completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        if (completion) {
            completion(isSuccess, request);
        }
    }];
    
}

/**获取用户粉丝列表*/
- (void)requestUserFans:(NSString *)userId page:(NSInteger)page completion:(RequestBlock)completion {
    self.dic = @{@"page":@(page), @"size":@(10)};

    self.urlStr = [NSString stringWithFormat:@"/user/fans/%@", userId];
    self.requestType = YTKRequestMethodGET;
    
    [self sendRequest_completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        if (completion) {
            completion(isSuccess, request);
        }
    }];
    
}

/**获取用户浏览足迹*/
- (void)requestUserZoneRecentViewPage:(NSInteger)page completion:(RequestBlock)completion {
    self.dic = @{@"page":@(page), @"size":@(20)};

    self.urlStr = @"/user/zone/recentView";
    self.requestType = YTKRequestMethodGET;
    
    [self sendRequest_completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        if (completion) {
            completion(isSuccess, request);
        }
    }];
    
}

/**
 * 批量删除用户浏览足迹
 *  ids    ids    query    true  string
 */
- (void)requestUserZoneRecentView_del:(NSString *)ids completion:(RequestBlock)completion {
    self.dic = @{@"ids":ids};

    self.urlStr = @"/user/zone/recentView";
    self.requestType = YTKRequestMethodDELETE;
    
    [self sendRequest_completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        if (completion) {
            completion(isSuccess, request);
        }
    }];
    
}

/**
 * 清空用户浏览足迹
 */
- (void)requestUserZoneRecentView_clear_completion:(RequestBlock)completion {
    self.urlStr = @"/user/zone/recentView/clear";
    self.requestType = YTKRequestMethodDELETE;
    
    [self sendRequest_completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        if (completion) {
            completion(isSuccess, request);
        }
    }];
    
}

/**
 * 实名认证
 *  name    name
 *  idCard    idCard
 */
- (void)requestUserVerifyReal_name:(NSString *)name idCard:(NSString *)idCard completion:(RequestBlock)completion {
    self.dic = @{@"name":name,
                 @"idCard":idCard};

    self.urlStr = @"/user/verify/real";
    self.requestType = YTKRequestMethodPUT;
    
    [self sendRequest_completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        if (completion) {
            completion(isSuccess, request);
        }
    }];
}

- (void)requestAccountListPage:(NSInteger)page yearAndMonth:(NSString *)yearAndMonth {
    self.dic = @{
        @"page":@(page),
        @"size":@"10",
        @"yearAndMonth":yearAndMonth
    };
    self.urlStr = @"/user/qjcoin/record";
    [self start];
}

- (void)requestHelpCenterHome {
    self.dic = @{};
    self.urlStr = @"/help-center/home";
    [self start];
}

- (void)requestHelpCenterMoreQuestionType:(NSString *)typeId page:(NSInteger)page {
    self.dic = @{
        @"page":@(page),
        @"size":@"10",
        @"typeId":typeId
    };
    self.requestType = YTKRequestMethodPOST;
    self.urlStr = @"/help-center/get_type/questions";
    [self start];
}

- (void)requestHelpCenterSearchQuestion:(NSString *)keyString {
    self.dic = @{
        @"questionContent":keyString
    };
    self.requestType = YTKRequestMethodPOST;
//    self.serializerType = YTKRequestSerializerTypeJSON;
    self.urlStr = @"/help-center/query";
    [self start];
}

/**
 * ai智能客服首页
 *  queryContent    用户输入内容
 *  queryType    类型查询 value = 0 游戏，1 营地，2资讯 ，3帮助中心，4 知识库，5初始化页面,6用户输入未点击选项，7结束会话
 */
- (void)requestAiCustomerServiceQueryContent:(NSString *)queryContent queryType:(NSInteger)queryType completion:(RequestBlock)completion {
    self.dic = @{@"queryContent":queryContent,@"queryType":@(queryType)};

    self.urlStr = @"/ai_customer_service/query";
    self.requestType = YTKRequestMethodPOST;
    self.serializerType = YTKRequestSerializerTypeJSON;
    
    [self sendRequest_completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        if (completion) {
            completion(isSuccess, request);
        }
    }];
}

/**
 * 知识库
 */
- (void)requestKnowledge_base_question_details_idStr:(NSString *)idStr completion:(RequestBlock)completion {
    self.dic = @{@"id":idStr};

    self.urlStr = @"/knowledge_base_question/question_details";
    self.requestType = YTKRequestMethodPOST;
    
    [self sendRequest_completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        if (completion) {
            completion(isSuccess, request);
        }
    }];
}

/**
 帮助中心-问题详情*/
- (void)requestHelpCenterQuestionDetailTypeID:(NSString *)typeId {
    self.dic = @{
        @"id":typeId
    };
    self.requestType = YTKRequestMethodPOST;
//    self.serializerType = YTKRequestSerializerTypeJSON;
    self.urlStr = @"/help-center/question_details";
    [self start];
}

@end
