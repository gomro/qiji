//
//  QJMineRequest.h
//  QJBox
//
//  Created by macm on 2022/7/19.
//

#import "QJBaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

typedef void(^RequestBlock)(BOOL isSuccess,YTKBaseRequest * _Nonnull request);

@interface QJMineRequest : QJBaseRequest

/**获取'我的'个人信息*/
- (void)requestUserInfoMine_completion:(RequestBlock)completion;

/**获取用户关注列表*/
- (void)requestUserStars:(NSString *)userId page:(NSInteger)page completion:(RequestBlock)completion;

/**获取用户粉丝列表*/
- (void)requestUserFans:(NSString *)userId page:(NSInteger)page completion:(RequestBlock)completion;

/**获取用户浏览足迹*/
- (void)requestUserZoneRecentViewPage:(NSInteger)page completion:(RequestBlock)completion;

/**
 * 批量删除用户浏览足迹
 *  ids    ids    query    true  string
 */
- (void)requestUserZoneRecentView_del:(NSString *)ids completion:(RequestBlock)completion;

/**
 * 清空用户浏览足迹
 */
- (void)requestUserZoneRecentView_clear_completion:(RequestBlock)completion;

/**
 * 实名认证
 *  name    name
 *  idCard    idCard
 */
- (void)requestUserVerifyReal_name:(NSString *)name idCard:(NSString *)idCard completion:(RequestBlock)completion;

/**
 奇迹币明细
 page    页码
 yearAndMonth  查询年月，格式如yyyy-MM*/
- (void)requestAccountListPage:(NSInteger)page yearAndMonth:(NSString *)yearAndMonth;

/**
 帮助中心首页*/
- (void)requestHelpCenterHome;

/**
 * ai智能客服首页
 *  queryContent    用户输入内容
 *  queryType    类型查询 value = 0 游戏，1 营地，2资讯 ，3帮助中心，4 知识库，5初始化页面,6用户输入未点击选项，7结束会话
 */
- (void)requestAiCustomerServiceQueryContent:(NSString *)queryContent queryType:(NSInteger)queryType completion:(RequestBlock)completion;

/**
 * 知识库
 */
- (void)requestKnowledge_base_question_details_idStr:(NSString *)idStr completion:(RequestBlock)completion;

/**帮助中心-更多问题*/
- (void)requestHelpCenterMoreQuestionType:(NSString *)typeId page:(NSInteger)page;

/**
 帮助中心-搜索问题*/
- (void)requestHelpCenterSearchQuestion:(NSString *)keyString;

/**
 帮助中心-问题详情*/
- (void)requestHelpCenterQuestionDetailTypeID:(NSString *)typeId;

@end

NS_ASSUME_NONNULL_END
