//
//  QJMineFollowRequest.h
//  QJBox
//
//  Created by wxy on 2022/7/19.
//

#import "QJBaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJMineFollowRequest : QJBaseRequest

@property (nonatomic, copy) NSString *userID;//被关注用户id

@property (nonatomic, copy) NSString *type;//    1关注，0取消关注
@end

NS_ASSUME_NONNULL_END
