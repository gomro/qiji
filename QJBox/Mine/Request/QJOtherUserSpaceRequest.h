//
//  QJOtherUserSpaceRequest.h
//  QJBox
//
//  Created by wxy on 2022/7/19.
//

#import "QJBaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

/// 其他人的空间
@interface QJOtherUserSpaceRequest : QJBaseRequest

@property (nonatomic, copy) NSString *userID;


@end

NS_ASSUME_NONNULL_END
