//
//  QJCommitProblemFeedbackReq.m
//  QJBox
//
//  Created by wxy on 2022/8/17.
//

#import "QJCommitProblemFeedbackReq.h"

@implementation QJCommitProblemFeedbackReq
- (NSString *)requestUrl {
    return @"/problem_feedback/submit_feedback";
}

- (YTKRequestMethod)requestMethod {
    
    return YTKRequestMethodPOST;
}


- (YTKRequestSerializerType)requestSerializerType {
    
    return YTKRequestSerializerTypeJSON;
}

@end
