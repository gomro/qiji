//
//  QJMineUserBeCollectedMsgRequest.m
//  QJBox
//
//  Created by wxy on 2022/7/22.
//

#import "QJMineUserBeCollectedMsgRequest.h"

@implementation QJMineUserBeCollectedMsgRequest
- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"/user/beCollected/summary/%@",self.userID];
}

- (YTKRequestMethod)requestMethod {
    
    return YTKRequestMethodGET;
}


- (YTKRequestSerializerType)requestSerializerType {
    
    return YTKRequestSerializerTypeHTTP;
}



@end
