//
//  QJTakeRewardRequest.m
//  QJBox
//
//  Created by wxy on 2022/8/16.
//

#import "QJTakeRewardRequest.h"



@implementation QJTakeRewardRequest


- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"/task/%@/reward",self.idStr];
}

- (YTKRequestMethod)requestMethod {
    
    return YTKRequestMethodPUT;
}


- (YTKRequestSerializerType)requestSerializerType {
    
    return YTKRequestSerializerTypeHTTP;
}



@end
