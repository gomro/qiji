//
//  QJCommitProblemFeedbackReq.h
//  QJBox
//
//  Created by wxy on 2022/8/17.
//

#import "QJBaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJCommitProblemFeedbackReq : QJBaseRequest

@end

NS_ASSUME_NONNULL_END
