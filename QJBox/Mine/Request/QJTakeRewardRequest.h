//
//  QJTakeRewardRequest.h
//  QJBox
//
//  Created by wxy on 2022/8/16.
//

#import "QJBaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJTakeRewardRequest : QJBaseRequest

@property (nonatomic, copy) NSString *idStr;


@end

NS_ASSUME_NONNULL_END
