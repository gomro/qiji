//
//  QJMineUserBeCollectedMsgRequest.h
//  QJBox
//
//  Created by wxy on 2022/7/22.
//

#import "QJBaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

/// 获取用户被收藏总结信息
@interface QJMineUserBeCollectedMsgRequest : QJBaseRequest
@property (nonatomic, copy) NSString *userID;

@end

NS_ASSUME_NONNULL_END
