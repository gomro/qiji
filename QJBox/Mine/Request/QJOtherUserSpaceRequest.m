//
//  QJOtherUserSpaceRequest.m
//  QJBox
//
//  Created by wxy on 2022/7/19.
//

#import "QJOtherUserSpaceRequest.h"

@implementation QJOtherUserSpaceRequest

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"/user/zone/%@",self.userID];
}

- (YTKRequestMethod)requestMethod {
    
    return YTKRequestMethodGET;
}


- (YTKRequestSerializerType)requestSerializerType {
    
    return YTKRequestSerializerTypeHTTP;
}



@end
