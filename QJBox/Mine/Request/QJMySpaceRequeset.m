//
//  QJMySpaceRequeset.m
//  QJBox
//
//  Created by wxy on 2022/7/18.
//

#import "QJMySpaceRequeset.h"

@implementation QJMySpaceRequeset


- (NSString *)requestUrl {
    return @"/user/zone/mine";
}


 

- (YTKRequestMethod)requestMethod {
    
    return YTKRequestMethodGET;
}


- (YTKRequestSerializerType)requestSerializerType {
    
    return YTKRequestSerializerTypeHTTP;
}



@end
