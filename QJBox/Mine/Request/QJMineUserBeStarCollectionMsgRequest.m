//
//  QJMineUserBeStarCollectionMsgRequest.m
//  QJBox
//
//  Created by wxy on 2022/7/22.
//

#import "QJMineUserBeStarCollectionMsgRequest.h"

@implementation QJMineUserBeStarCollectionMsgRequest
- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"/user/beLiked/summary/%@",self.userID];
}

- (YTKRequestMethod)requestMethod {
    
    return YTKRequestMethodGET;
}


- (YTKRequestSerializerType)requestSerializerType {
    
    return YTKRequestSerializerTypeHTTP;
}



@end
