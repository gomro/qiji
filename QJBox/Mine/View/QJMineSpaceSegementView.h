//
//  QJMineSpaceSegementView.h
//  QJBox
//
//  Created by wxy on 2022/7/21.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/// 我的空间  分组空间
@interface QJMineSpaceSegementView : UIView

@property (nonatomic, assign) NSInteger selectedIndex;//选中位置


@property (nonatomic, copy) NSString *normalColor;

@property (nonatomic, copy) NSString *selectedColor;

@property (nonatomic, strong) UIFont *normalFont;

@property (nonatomic, strong) UIFont *selectedFont;

@property (nonatomic, copy) void(^clickIndex)(NSInteger index);

- (instancetype)initWithFrame:(CGRect)frame titles:(NSArray *)titles;
@end

NS_ASSUME_NONNULL_END
