//
//  QJAccountDetailHeaderView.h
//  QJBox
//
//  Created by Sun on 2022/8/22.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void (^ClickHeaderMonth)(NSString *monthTitle);

@interface QJAccountDetailHeaderView : UIView
- (void)configureHeaderWithTitle:(NSString *)title;
@property (nonatomic, copy) ClickHeaderMonth clickHeader;

@end

NS_ASSUME_NONNULL_END
