//
//  QJMineLevelView.m
//  QJBox
//
//  Created by macm on 2022/7/18.
//

#import "QJMineLevelView.h"

@interface QJMineLevelView ()
/**等级背景图片*/
@property (nonatomic, strong) UIImageView *levelBackImageView;
@property (nonatomic, strong) UIImageView *levelImageView;
@property (nonatomic, strong) UIView *bgView;
@property (strong, nonatomic) MASConstraint *lvConstraint;

@end

@implementation QJMineLevelView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        [self loadUI];
    }
    return self;
}

#pragma mark - Load UI
- (void)loadUI {
    
    [self addSubview:self.levelBackImageView];
    [self.levelBackImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.bottom.right.equalTo(self);
    }];
    
    [self addSubview:self.levelImageView];
    [self.levelImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.centerY.mas_equalTo(self);
    }];
    
    [self addSubview:self.bgView];
    [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {

        make.left.mas_equalTo(50);
        make.right.mas_equalTo(-15);
        make.height.mas_equalTo(3.5);
        make.centerY.mas_equalTo(self);
    }];

    [self.bgView addSubview:self.lvView];
    [self.lvView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.bottom.mas_equalTo(0);
        self.lvConstraint = make.width.mas_equalTo(2);

    }];
    
    [self.bgView addSubview:self.arrowImageView];
    [self.arrowImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.bgView).mas_offset(-1);
        make.left.mas_equalTo(self.lvView.mas_right).mas_offset(-2);
        make.width.mas_equalTo(16);
        make.height.mas_equalTo(11);
    }];
    
    [self addSubview:self.label];
    [self.label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self).mas_offset(-10);
        make.bottom.mas_equalTo(self).mas_offset(-5);
    }];
    
}

// 刷新进度条 currentEx:当前经验 nextEx：下一级需要总经验
- (void)reloadLvView:(NSInteger)currentEx NextEx:(NSInteger)nextEx level:(NSInteger)level {
    if (currentEx != 0 && nextEx != 0) {
        self.label.text = [NSString stringWithFormat:@"再获取%ld经验值可升级",nextEx - currentEx];

        CGFloat bili= (CGFloat)currentEx / nextEx;
        float width = bili * (kScreenWidth-16-16-50-15-16);
        [self.lvConstraint uninstall];
        [self.lvView mas_updateConstraints:^(MASConstraintMaker *make) {
            self.lvConstraint = make.width.mas_equalTo(width);
        }];
        [_lvView layoutIfNeeded];
        
        [_lvView graduateLeftColor:[self leftColor][level-1] ToColor:[self toColor][level-1]];
        _levelImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"mine_level_%ld",level]];
        _levelBackImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"mine_level_bg_%ld",level]];
        _arrowImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"mine_level_arrow_%ld",level]];
        _label.textColor = [self exColor][level-1];
    }
    
}

- (UIImageView *)levelBackImageView {
    if (!_levelBackImageView) {
        _levelBackImageView = [UIImageView new];
        _levelBackImageView.contentMode = UIViewContentModeScaleAspectFill;
        _levelBackImageView.layer.masksToBounds = YES;
    }
    return _levelBackImageView;
}

- (UIImageView *)levelImageView {
    if (!_levelImageView) {
        _levelImageView = [UIImageView new];
    }
    return _levelImageView;
}

- (UIView *)bgView {
    if (!_bgView) {
        _bgView = [[UIView alloc] init];
        _bgView.backgroundColor = RGBAlpha(255, 255, 255, 0.5);
        _bgView.layer.cornerRadius = 1.75;
    }
    return _bgView;
}

- (UIView *)lvView {
    if (!_lvView) {
        _lvView = [[UIView alloc] init];
        _lvView.layer.cornerRadius = 1.75;
        _lvView.layer.masksToBounds = YES;
    }
    return _lvView;
}

- (UIImageView *)arrowImageView {
    if (!_arrowImageView) {
        _arrowImageView = [UIImageView new];
    }
    return _arrowImageView;
}

- (UILabel *)label {
    if (!_label) {
        _label = [[UILabel alloc] init];
        _label.textColor = UIColorFromRGB(0x474849);
        _label.font = FontRegular(8);
        _label.textAlignment = NSTextAlignmentRight;
    }
    return _label;
}

- (NSArray *)exColor {
    return @[RGBAlpha(71, 72, 73, 1),
             RGBAlpha(32, 67, 83, 1),
             RGBAlpha(101, 56, 41, 1),
             RGBAlpha(120, 85, 34, 1),
             RGBAlpha(41, 62, 109, 1),
             RGBAlpha(47, 20, 104, 1),
             RGBAlpha(255, 196, 137, 1),
             RGBAlpha(77, 109, 148, 1),
             RGBAlpha(180, 127, 44, 1),
             RGBAlpha(200, 206, 243, 1)
    ];
}

- (NSArray *)toColor {
    return @[RGBAlpha(71, 72, 73, 1),
             RGBAlpha(43, 90, 111, 1),
             RGBAlpha(101, 56, 41, 1),
             RGBAlpha(120, 85, 34, 1),
             RGBAlpha(41, 62, 109, 1),
             RGBAlpha(47, 20, 104, 1),
             RGBAlpha(255, 182, 117, 1),
             RGBAlpha(103, 145, 197, 1),
             RGBAlpha(240, 170, 59, 1),
             RGBAlpha(200, 206, 243, 1)
    ];
}

- (NSArray *)leftColor {
    return @[RGBAlpha(71, 72, 73, 0.4),
             RGBAlpha(181, 228, 230, 1),
             RGBAlpha(101, 56, 41, 0.4),
             RGBAlpha(120, 85, 34, 0.4),
             RGBAlpha(34, 53, 98, 0.4),
             RGBAlpha(47, 20, 104, 0.4),
             RGBAlpha(255, 212, 169, 1),
             RGBAlpha(208, 230, 255, 1),
             RGBAlpha(244, 183, 79, 0.57),
             RGBAlpha(32, 43, 79, 1)
    ];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
