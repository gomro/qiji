//
//  QJSpaceHeaderView.h
//  QJBox
//
//  Created by Sun on 2022/7/1.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class QJMySpaceModel;

typedef void(^btnBlock)(void);

@interface QJSpaceHeaderView : UIView

@property (nonatomic, strong) QJMySpaceModel *model;

@property (nonatomic, copy) btnBlock fllowBlock;//关注

@property (nonatomic, copy) btnBlock starBlock;//点赞

@property (nonatomic, copy) btnBlock fansBlock;//粉丝

@property (nonatomic, copy) btnBlock collectionBlock;//收藏

@property (nonatomic, copy) btnBlock avatBlock;//点击个人头像

@property (nonatomic, copy) btnBlock qianMingBlock;//点击个性签名

@property (nonatomic, assign) BOOL isCustom;//是否是别人的空间

@property (nonatomic, strong) UIImage *avatImage;//头像
/**昵称*/
@property (nonatomic, strong) UILabel *nickNameLabel;

@property (nonatomic, copy) NSString *avatImageStr;//头像地址


@property (nonatomic, copy) void(^updatTableviewOffset)(CGFloat height);


/// 更新签名
/// @param qianMing  签名
- (void)updateQianMing:(NSString *)qianMing;

//- (void)scrollViewDidScroll:(CGFloat)offsetY;

@end

NS_ASSUME_NONNULL_END
