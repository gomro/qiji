//
//  QJMineUserLabel.m
//  QJBox
//
//  Created by wxy on 2022/7/18.
//

#import "QJMineUserLabel.h"

@interface QJMineUserLabel ()

@property (nonatomic, strong) UILabel *label;

@property (nonatomic, strong) UIImageView *bgImageView;

@end


@implementation QJMineUserLabel


- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
    
        [self addSubview:self.bgImageView];
        [self.bgImageView addSubview:self.label];
        
        [self.bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(self);
            make.left.top.equalTo(self);
        }];
        
        [self.label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(self.bgImageView);
            make.top.equalTo(self.bgImageView).offset(3);
            make.left.equalTo(self.bgImageView).offset(6);
        }];
    }
    return self;
}



#pragma mark  ------- getter

- (UIImageView *)bgImageView {
    if (!_bgImageView) {
        _bgImageView = [UIImageView new];
        
    }
    return _bgImageView;
}

- (UILabel *)label {
    if (!_label) {
        _label = [UILabel new];
        _label.font = FontMedium(8);
        _label.textColor = kColorWithHexString(@"ffffff");
        _label.hidden = YES;
    }
    return _label;
}


- (void)setTitle:(NSString *)title {
    _title = title;
    self.label.text = title;
}


- (void)setBgImageUrl:(NSString *)bgImageUrl {
    _bgImageUrl = bgImageUrl;
    
    [self.bgImageView setImageWithURL:[NSURL URLWithString:[bgImageUrl checkImageUrlString]] placeholder:nil options:kNilOptions completion:^(UIImage * _Nullable image, NSURL * _Nonnull url, YYWebImageFromType from, YYWebImageStage stage, NSError * _Nullable error) {
      
        DLog("图片的size = %@",NSStringFromCGSize(image.size));
        
    }];
}
@end
