//
//  QJMineLevelView.h
//  QJBox
//
//  Created by macm on 2022/7/18.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJMineLevelView : UIView
@property (nonatomic, strong) UILabel *label;
@property (nonatomic, strong) UIView *lvView;
@property (nonatomic, strong) UIImageView *arrowImageView;

// 刷新进度条 currentEx:当前经验 nextEx：下一级需要总经验
- (void)reloadLvView:(NSInteger)currentEx NextEx:(NSInteger)nextEx level:(NSInteger)level;
@end

NS_ASSUME_NONNULL_END
