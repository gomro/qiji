//
//  QJMineCellButton.m
//  QJBox
//
//  Created by Sun on 2022/6/30.
//

#import "QJMineCellButton.h"

@interface QJMineCellButton ()
@property (nonatomic, strong) UILabel *titleLb;
@property (nonatomic, strong) UIImageView *iconButton;
@end

@implementation QJMineCellButton

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:self.iconButton];
        [self addSubview:self.titleLb];
        [self makeConstraints];
    }
    return self;
}

- (void)makeConstraints {
    [self.iconButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.top.equalTo(self.mas_top).offset(10);
        make.width.height.mas_offset(45);
    }];
    
    [self.titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.top.equalTo(self.iconButton.mas_bottom).offset(10);
    }];
}

    
- (UIImageView *)iconButton {
    if (!_iconButton) {
        self.iconButton = [UIImageView new];
    }
    return _iconButton;
}
- (UILabel *)titleLb {
    if (!_titleLb) {
        self.titleLb = [UILabel new];
        self.titleLb.font = kFont(13);
        self.titleLb.textColor = UIColorHex(0x151D2F);
    }
    return _titleLb;
}


- (void)setTitle:(NSString *)title {
    _title = title;
    self.titleLb.text = title;
}

- (void)setIconN:(NSString *)iconN {
    _iconN = iconN;
    self.iconButton.image = [UIImage imageNamed:iconN];
}

- (CGRect)imageRectForContentRect:(CGRect)contentRect {
    
    return CGRectZero;
}

- (CGRect)titleRectForContentRect:(CGRect)contentRect {
    return CGRectZero;

}
@end
