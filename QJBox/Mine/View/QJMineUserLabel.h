//
//  QJMineUserLabel.h
//  QJBox
//
//  Created by wxy on 2022/7/18.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/// 我的空间用户头衔标签
@interface QJMineUserLabel : UIView

@property (nonatomic, copy) NSString *title;

@property (nonatomic, copy) NSString *bgImageUrl;

@end

NS_ASSUME_NONNULL_END
