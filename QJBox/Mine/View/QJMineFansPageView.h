//
//  QJMineFansPageView.h
//  QJBox
//
//  Created by macm on 2022/7/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class QJMineFansModel;
@interface QJMineFansPageView : UIView
/**
 * 刷新数据
 * type：0:关注 1：粉丝
 * uid：用户id
 */
- (void)reloadPageView:(NSInteger)type uid:(NSString *)uid;


@property (nonatomic, copy) void(^didSelectedCell)(QJMineFansModel*model);
@end

NS_ASSUME_NONNULL_END
