//
//  QJHelpCenterHeaderView.h
//  QJBox
//
//  Created by Sun on 2022/8/16.
//

#import <UIKit/UIKit.h>
#import "QJHelpCenterModel.h"

NS_ASSUME_NONNULL_BEGIN
typedef void (^ClickHeaderMore)(NSString *typeId, NSString *typeName);

@interface QJHelpCenterHeaderView : UIView
@property (nonatomic, copy) ClickHeaderMore clickHeader;

- (void)configureHeaderWithData:(QJHelpCenterModel *)model;
@end

@interface QJHelpCenterFooterView : UIView

@end

NS_ASSUME_NONNULL_END
