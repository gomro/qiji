//
//  QJMineSpaceSegementView.m
//  QJBox
//
//  Created by wxy on 2022/7/21.
//

#import "QJMineSpaceSegementView.h"

#define segementTag 1000
#define btnTag  2000
@interface QJMineSpaceSegementView ()

//标签数组
@property (nonatomic, strong) NSMutableArray *array;

@end


@implementation QJMineSpaceSegementView


- (instancetype)initWithFrame:(CGRect)frame titles:(NSArray *)titles {
    if (self = [super initWithFrame:frame]) {
        
        int i = 0;
        self.selectedIndex = 0;
        [self.array removeAllObjects];
        for (NSString *title in titles) {
            
            UILabel *label = [UILabel new];
            label.font = FontRegular(16);
            label.textColor = kColorWithHexString(@"#000000");
            label.tag = segementTag + i;
            label.text = title;
            [label alignBottom];
            [self addSubview:label];
            [self.array safeAddObject:label];
            
         
            UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
            [btn addTarget:self action:@selector(btnAction:) forControlEvents:UIControlEventTouchUpInside];
            btn.tag = btnTag + i;
            [self addSubview:btn];
            [btn mas_makeConstraints:^(MASConstraintMaker *make) {
                            make.centerX.equalTo(label);
                            make.width.equalTo(@80);
                            make.height.equalTo(self);
            }];
            i++;
            
        }
        NSInteger count = titles.count;
        CGFloat leadSpacing = (kScreenWidth - 38*count - 40*(count-1))/2.0;
        if (count > 1) {
            [self.array mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedSpacing:13 leadSpacing:leadSpacing tailSpacing:leadSpacing];
            [self.array mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerY.equalTo(self);
            }];
        }else{
            UILabel *label = [self.array safeObjectAtIndex:0];
            [label mas_makeConstraints:^(MASConstraintMaker *make) {
                            make.center.equalTo(self);
            }];
            
        }
      
        
        
    }
    return self;
}


#pragma mark ---- method


- (void)btnAction:(UIButton *)sender {
    NSInteger tag = sender.tag - btnTag;
    if (self.clickIndex) {
        self.clickIndex(tag);
    }
    [self clickIndex:tag];
}


- (void)clickIndex:(NSInteger)index {
  
    for (UILabel *label in self.array.copy) {
        if (label.tag - index == segementTag) {
            label.font = FontSemibold(18);
            if (self.selectedColor.length > 0) {
                label.textColor = kColorWithHexString(self.selectedColor);
            }else{
                label.textColor = kColorWithHexString(@"#000000");
            }
            
            if (self.selectedFont) {
                label.font = self.selectedFont;
            }else{
                label.font =  FontSemibold(18);
            }
        }else{
            if (self.normalColor.length > 0) {
                label.textColor = kColorWithHexString(self.normalColor);
            }else{
                
                label.textColor = kColorWithHexString(@"#000000");
            }
            if (self.normalFont) {
                label.font = self.normalFont;
            }else{
                label.font = FontRegular(16);
            }
        }
        
    }
    
}

- (void)setSelectedIndex:(NSInteger)selectedIndex {
    _selectedIndex = selectedIndex;
    [self clickIndex:selectedIndex];
}


- (void)setNormalColor:(NSString *)normalColor {
    _normalColor = normalColor;
    [self clickIndex:self.selectedIndex];
}

- (void)setSelectedColor:(NSString *)selectedColor {
    _selectedColor = selectedColor;
    [self clickIndex:self.selectedIndex];
}

- (void)setNormalFont:(UIFont *)normalFont {
    _normalFont = normalFont;
    [self clickIndex:self.selectedIndex];
}

- (void)setSelectedFont:(UIFont *)selectedFont {
    _selectedFont = selectedFont;
    [self clickIndex:self.selectedIndex];
}

 

#pragma mark ------- getter


- (NSMutableArray *)array {
    if (!_array) {
        _array = [NSMutableArray array];
        
    }
    return _array;
}



@end
