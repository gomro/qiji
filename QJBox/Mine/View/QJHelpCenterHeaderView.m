//
//  QJHelpCenterHeaderView.m
//  QJBox
//
//  Created by Sun on 2022/8/16.
//

#import "QJHelpCenterHeaderView.h"
#import "QJImageButton.h"

@interface QJHelpCenterHeaderView ()
/**背景*/
@property (nonatomic, strong) UIView *backView;
/**左边图片*/
@property (nonatomic, strong) UIImageView *leftImage;
/**标题*/
@property (nonatomic, strong) UILabel *titleLabel;
/**右边按钮*/
@property (nonatomic, strong) QJImageButton *rightButton;

@property (nonatomic, copy) NSString *typeId;
@property (nonatomic, copy) NSString *typeName;

@end

@implementation QJHelpCenterHeaderView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = UIColorHex(F5F5F5);
        [self loadUI];
    }
    return self;
}

#pragma mark - Load UI
- (void)loadUI {
    [self addSubview:self.backView];
    [self.backView addSubview:self.leftImage];
    [self.backView addSubview:self.titleLabel];
    [self.backView addSubview:self.rightButton];
    [self makeConstraints];
}

- (void)makeConstraints {
    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).offset(8);
        make.left.equalTo(self.mas_left).offset(16);
        make.right.equalTo(self.mas_right).offset(-16);
        make.height.mas_equalTo(44*kWScale);
    }];
    
    [self.leftImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.backView.mas_top).offset(16);
        make.left.equalTo(self.backView.mas_left).offset(16);
        make.width.height.mas_equalTo(16*kWScale);
    }];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.leftImage.mas_right).offset(4);
        make.centerY.equalTo(self.leftImage);
    }];
    
    [self.rightButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.backView.mas_right).offset(-20);
        make.centerY.equalTo(self.leftImage);
        make.height.mas_equalTo(16*kWScale);
    }];
    [self changeBackRounded];
}

#pragma mark - Lazy Loading

- (UIView *)backView {
    if (!_backView) {
        _backView = [UIView new];
        _backView.backgroundColor = [UIColor whiteColor];
    }
    return _backView;
}

- (UIImageView *)leftImage {
    if (!_leftImage) {
        _leftImage = [UIImageView new];
    }
    return _leftImage;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        self.titleLabel = [UILabel new];
        self.titleLabel.textColor = UIColorHex(000000);
        self.titleLabel.font = MYFONTALL(FONT_BOLD, 16);
        
    }
    return _titleLabel;
}

- (QJImageButton *)rightButton {
    if (!_rightButton) {
        _rightButton = [[QJImageButton alloc] init];
        _rightButton.imageSize = CGSizeMake(16, 16);
        _rightButton.space = 4;
        _rightButton.iconString = @"mine_right";
        _rightButton.titleString = @"全部问题";
        _rightButton.titleLb.font = MYFONTALL(FONT_REGULAR, 12);
        _rightButton.titleLb.textColor = UIColorHex(919599);
        _rightButton.style = ImageButtonStyleRight;
        [_rightButton addTarget:self action:@selector(moreQuestion) forControlEvents:UIControlEventTouchUpInside];
    }
    return _rightButton;
}

- (void)changeBackRounded {
    [self.backView layoutIfNeeded];
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.backView.bounds byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(8, 8)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.backView.bounds;
    maskLayer.path = maskPath.CGPath;
    self.backView.layer.mask = maskLayer;
}

- (void)configureHeaderWithData:(QJHelpCenterModel *)model {
    self.titleLabel.text = kCheckStringNil(model.typeName);
    self.typeId = kCheckStringNil(model.typeId);
    self.typeName = kCheckStringNil(model.typeName);
    NSString *cardImage = [kCheckStringNil(model.url) checkImageUrlString];
    [self.leftImage setImageWithURL:[NSURL URLWithString:cardImage] placeholder:[UIImage imageNamed:@"reportHelperIcon"]];
    
}

- (void)moreQuestion {
    if (self.clickHeader) {
        self.clickHeader(self.typeId, self.typeName);
    }
}
@end


@interface QJHelpCenterFooterView ()
/**背景*/
@property (nonatomic, strong) UIView *backView;
@end
@implementation QJHelpCenterFooterView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = UIColorHex(F5F5F5);
        [self addSubview:self.backView];
        [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.equalTo(self);
            make.left.equalTo(self.mas_left).offset(16);
            make.right.equalTo(self.mas_right).offset(-16);
            make.height.mas_equalTo(16*kWScale);
        }];
        [self.backView layoutIfNeeded];
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.backView.bounds byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii:CGSizeMake(8, 8)];
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = self.backView.bounds;
        maskLayer.path = maskPath.CGPath;
        self.backView.layer.mask = maskLayer;
        
    }
    return self;
}

- (UIView *)backView {
    if (!_backView) {
        _backView = [UIView new];
        _backView.backgroundColor = [UIColor whiteColor];
    }
    return _backView;
}

@end
