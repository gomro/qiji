//
//  QJMineCellButton.h
//  QJBox
//
//  Created by Sun on 2022/6/30.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJMineCellButton : UIButton
/**标题*/
@property (nonatomic, copy) NSString *title;
/**常态*/
@property (nonatomic, copy) NSString *iconN;
@end

NS_ASSUME_NONNULL_END
