//
//  QJSpaceHeaderView.m
//  QJBox
//
//  Created by Sun on 2022/7/1.
//

#import "QJSpaceHeaderView.h"
#import "QJMineHeaderView.h"
#import "QJMineUserLabel.h"
#import "QJMySpaceModel.h"
#define SpaceTag     1000

@interface QJSpaceHeaderView ()
/**背景图*/
@property (nonatomic, strong) UIImageView *headerBackView;

@property (nonatomic, strong) UIView *maskBgView;

/**头像*/
@property (nonatomic, strong) UIImageView *avatarImage;


@property (nonatomic, strong) UIView *genderBgView;

/**性别标签*/
@property (nonatomic, strong) UIImageView *genderImage;



//等级
@property (nonatomic, strong) UIImageView *levelImageView;

//博主资质图标
@property (nonatomic, strong) UIImageView *bloggerLevelImageView;

/**签名*/
@property (nonatomic, strong) UILabel *signatureLabel;


@property (nonatomic, strong) UIButton *signatureButton;

 
 

/**粉丝*/
@property (nonatomic, strong) QJMineHeaderButtonView *fansView;
/**关注*/
@property (nonatomic, strong) QJMineHeaderButtonView *followersView;
/**收藏*/
@property (nonatomic, strong) QJMineHeaderButtonView *favoriteView;
/**点赞*/
@property (nonatomic, strong) QJMineHeaderButtonView *likeView;

@property (nonatomic, assign) CGRect        bgImgFrame;

@end
#pragma mark - init
@implementation QJSpaceHeaderView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
//        self.backgroundColor = UIColorHex(f8f8f8);
        self.backgroundColor = [UIColor clearColor];
        [self loadUI];
    }
    return self;
}


#pragma mark - Load UI
- (void)loadUI {
    [self addSubview:self.headerBackView];
//    [self.headerBackView addSubview:self.maskBgView];
    [self.headerBackView addSubview:self.avatarImage];
    [self.headerBackView addSubview:self.nickNameLabel];
    [self.headerBackView addSubview:self.genderBgView];
    [self.headerBackView addSubview:self.genderImage];
    [self.headerBackView addSubview:self.levelImageView];
    [self.headerBackView addSubview:self.bloggerLevelImageView];
    [self.headerBackView addSubview:self.signatureLabel];
    [self.headerBackView addSubview:self.signatureButton];

    [self.headerBackView addSubview:self.fansView];
    [self.headerBackView addSubview:self.followersView];
    [self.headerBackView addSubview:self.favoriteView];
    [self.headerBackView addSubview:self.likeView];
    
    [self makeConstraints];

    [self.followersView configureLabelString:@"0" bottomString:@"关注"];
    [self.fansView configureLabelString:@"0" bottomString:@"粉丝"];
    [self.favoriteView configureLabelString:@"0" bottomString:@"被收藏"];
    [self.likeView configureLabelString:@"0" bottomString:@"获赞"];
     
}

- (void)makeConstraints {
    [self.headerBackView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    
//    [self.maskBgView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.edges.equalTo(self);
//    }];
    
    
    [self.avatarImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.headerBackView).offset(0);
        make.centerX.equalTo(self.headerBackView);
        make.width.height.mas_equalTo(64*kWScale);
    }];
    
    [self.nickNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.avatarImage.mas_bottom).offset(12);
        make.centerX.equalTo(self.avatarImage);
    }];
    
    [self.genderBgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.bottom.equalTo(self.avatarImage);
            make.width.height.mas_equalTo(16*kWScale);
    }];
    [self.genderImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(self.genderBgView);
            make.width.height.mas_equalTo(10*kWScale);
    }];
     
    
    [self.levelImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.nickNameLabel);
            make.width.equalTo(@(26*kWScale));
            make.height.equalTo(@(12*kWScale));
        make.left.equalTo(self.nickNameLabel.mas_right).offset(4);
    }];
    
    [self.bloggerLevelImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.levelImageView.mas_right).offset(4);
            make.width.equalTo(@(19.5*kWScale));
            make.height.equalTo(@(16.6*kWScale));
            make.centerY.equalTo(self.levelImageView);
    }];
    
    
    
    [self.signatureLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.nickNameLabel.mas_bottom).offset(8);
        make.centerX.equalTo(self.headerBackView);
        make.left.equalTo(self.headerBackView).offset(Get375Width(55.5));
        make.right.equalTo(self.headerBackView).offset(-Get375Width(55.5));
    }];
    [self.signatureButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(self.signatureLabel);
            make.height.equalTo(@(30*kWScale));
            make.width.equalTo(@(kScreenWidth));
    }];
   
    
    NSMutableArray *totalView = [NSMutableArray arrayWithCapacity:0];
    [totalView addObject:self.followersView];
    [totalView addObject:self.fansView];
    [totalView addObject:self.favoriteView];
    [totalView addObject:self.likeView];
    
    //水平方向控件间隔固定等间隔
    CGFloat leadSpacing = (kScreenWidth - 204*kWScale)/2.0;
    [totalView mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedSpacing:24 leadSpacing:leadSpacing tailSpacing:leadSpacing];
    [totalView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.signatureLabel.mas_bottom).offset(16);
        make.height.equalTo(@(34*kWScale));
    }];
}


- (void)setModel:(QJMySpaceModel *)model {
    _model = model;
    WS(weakSelf)
    self.headerBackView.frame = CGRectMake(0, 0, kScreenWidth, self.height);
    self.bgImgFrame = CGRectMake(0, 0, kScreenWidth, self.height);
    self.avatarImage.imageURL = [NSURL URLWithString:[model.coverImage checkImageUrlString]];
//    self.headerBackView.imageURL = [NSURL URLWithString:[model.backgroundImage checkImageUrlString]];
    if(model.level.integerValue == 0){
        self.levelImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"qj_level_%d",1]];
    }else{
        self.levelImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"qj_level_%ld",model.level.integerValue]];
    }
    
    self.bloggerLevelImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"qj_bozhuLevel_%ld",model.bloggerLevel.integerValue]];
    self.favoriteView.clickView = ^{
        DLog(@"收藏数");
        if (weakSelf.collectionBlock) {
            weakSelf.collectionBlock();
        }
    };
    self.likeView.clickView = ^{
        DLog(@"获赞数");
        if (weakSelf.starBlock) {
            weakSelf.starBlock();
        }
    };
    self.fansView.clickView = ^{
        DLog(@"粉丝数");
        if (weakSelf.fansBlock) {
            weakSelf.fansBlock();
        }
    };
    
    self.followersView.clickView = ^{
        DLog(@"关注数");
        if (weakSelf.fllowBlock) {
            weakSelf.fllowBlock();
        }
    };
     
    self.favoriteView.topLabel.text = [NSString changeCountFromString:model.beCollectCount];
    self.likeView.topLabel.text = [NSString changeCountFromString:model.beLikedCount];
    self.fansView.topLabel.text = [NSString changeCountFromString:model.fansCount];
    self.followersView.topLabel.text = [NSString changeCountFromString:model.starCount];
    self.nickNameLabel.text = model.nickname;
    
    if (model.sex == 1) {//男
        _genderBgView.hidden = NO;
        self.genderImage.image = [UIImage imageNamed:@"qj_mySpace_male"];
    }else if (model.sex == 2){//女
        _genderBgView.hidden = NO;
        self.genderImage.image = [UIImage imageNamed:@"qj_mySpace_female"];
    }
    if (self.isCustom) {
        
        self.avatarImage.userInteractionEnabled = NO;
    }else{
        
        self.avatarImage.userInteractionEnabled = YES;
    }
    if (!IsStrEmpty(model.signature)) {
        self.signatureLabel.text = model.signature;
        //先计算两行高度
        CGSize maxSize = [@"333\n" boundingRectWithSize:CGSizeMake(kScreenWidth - Get375Width(55.5*2), CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:self.signatureLabel.font} context:nil].size;
        
        CGSize size = [self.signatureLabel.text boundingRectWithSize:CGSizeMake(kScreenWidth - Get375Width(55.5*2), CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:self.signatureLabel.font} context:nil].size;
        if(size.height >= maxSize.height){
            if(self.updatTableviewOffset){
                CGSize oneline = [@"  " boundingRectWithSize:CGSizeMake(kScreenWidth - Get375Width(55.5*2), CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:self.signatureLabel.font} context:nil].size;
                
                self.updatTableviewOffset(oneline.height);
            }
        }else{
            if(self.updatTableviewOffset){
                self.updatTableviewOffset(0);
            }
        }
            
    }else {
        if (self.isCustom) {
            self.signatureLabel.text = @"这个人很懒 还没有留下签名~";
        }else{
            self.signatureLabel.text = @"点击编辑个性签名";
        }
        if(self.updatTableviewOffset){
            self.updatTableviewOffset(0);
        }
        
    }
   
    /*1.0版本暂时关闭
    NSMutableArray *totalView = [NSMutableArray arrayWithCapacity:0];
    for (int i = 0; i < model.honors.count; i++) {
        QJMineUserLabel *label = [QJMineUserLabel new];
        [totalView addObject:label];
        QJMineUserHonorsModel *honors = [model.honors safeObjectAtIndex:i];
        label.bgImageUrl = honors.icon;
        label.tag = SpaceTag + i;
        [self.headerBackView addSubview:label];
    }
    
    //水平方向控件间隔固定等间隔
    NSInteger count = model.honors.count;
    CGFloat leadSpacing = (kScreenWidth - 44*count - 13*(count-1))/2.0;
    if (count > 1) {
        [totalView mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedSpacing:13 leadSpacing:leadSpacing tailSpacing:leadSpacing];
        [totalView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.signatureLabel.mas_bottom).offset(16);
        }];
    }else if(count > 0){
        QJMineUserLabel *label = [totalView safeObjectAtIndex:0];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.signatureLabel.mas_bottom).offset(16);
            make.centerX.equalTo(self);
        }];
    }
   
    */
    
    
    
}

#pragma mark - Lazy Loading
- (UIImageView *)headerBackView {
    if (!_headerBackView) {
        _headerBackView = [UIImageView new];
        
        _headerBackView.userInteractionEnabled = YES;
    }
    return _headerBackView;
}

- (UIImageView *)avatarImage {
    if (!_avatarImage) {
        self.avatarImage = [UIImageView new];
        self.avatarImage.layer.masksToBounds = YES;
        self.avatarImage.layer.cornerRadius = 32*kWScale;
        self.avatarImage.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapAction)];
        [_avatarImage addGestureRecognizer:tap];
    }
    return _avatarImage;
}

- (UIImageView *)genderImage {
    if (!_genderImage) {
        _genderImage = [UIImageView new];
    }
    return _genderImage;
}


- (UILabel *)nickNameLabel {
    if (!_nickNameLabel) {
        self.nickNameLabel = [UILabel new];
        self.nickNameLabel.font = FontSemibold(20);
        self.nickNameLabel.textColor = [UIColor whiteColor];
        
    }
    return _nickNameLabel;
}

- (UIImageView *)levelImageView {
    if (!_levelImageView) {
        _levelImageView = [UIImageView new];
    }
    return _levelImageView;
}

- (UIImageView *)bloggerLevelImageView {
    if (!_bloggerLevelImageView) {
        _bloggerLevelImageView = [UIImageView new];
    }
    return _bloggerLevelImageView;
}


- (UILabel *)signatureLabel {
    if (!_signatureLabel) {
        self.signatureLabel = [UILabel new];
        self.signatureLabel.font = FontRegular(12);
        self.signatureLabel.textColor = [UIColor whiteColor];
        self.signatureLabel.textAlignment = NSTextAlignmentCenter;
        self.signatureLabel.numberOfLines = 2;
        self.signatureLabel.text = @"   ";
    }
    return _signatureLabel;
}


- (UIButton *)signatureButton {
    if (!_signatureButton) {
        _signatureButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _signatureButton.backgroundColor = [UIColor clearColor];
        [_signatureButton setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        [_signatureButton addTarget:self action:@selector(signatureButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _signatureButton;
}

 

- (QJMineHeaderButtonView *)followersView {
    if (!_followersView) {
        _followersView = [QJMineHeaderButtonView new];
        _followersView.topLabel.font = FontSemibold(16);
        _followersView.topLabel.textColor = [UIColor whiteColor];
        _followersView.bottomLabel.font = FontRegular(12);
        _followersView.bottomLabel.textColor = [UIColor whiteColor];
    }
    return _followersView;
}

- (QJMineHeaderButtonView *)fansView {
    if (!_fansView) {
        _fansView = [QJMineHeaderButtonView new];
        _fansView.topLabel.font = FontSemibold(16);
        _fansView.topLabel.textColor = [UIColor whiteColor];
        _fansView.bottomLabel.font = FontRegular(12);
        _fansView.bottomLabel.textColor = [UIColor whiteColor];
    }
    return _fansView;
}

- (QJMineHeaderButtonView *)favoriteView {
    if (!_favoriteView) {
        _favoriteView = [QJMineHeaderButtonView new];
        _favoriteView.topLabel.font = FontSemibold(16);
        _favoriteView.topLabel.textColor = [UIColor whiteColor];
        _favoriteView.bottomLabel.font = FontRegular(12);
        _favoriteView.bottomLabel.textColor = [UIColor whiteColor];
    }
    return _favoriteView;
}

- (QJMineHeaderButtonView *)likeView {
    if (!_likeView) {
        _likeView = [QJMineHeaderButtonView new];
        _likeView.topLabel.font = FontSemibold(16);
        _likeView.topLabel.textColor = [UIColor whiteColor];
        _likeView.bottomLabel.font = FontRegular(12);
        _likeView.bottomLabel.textColor = [UIColor whiteColor];
    }
    return _likeView;
}

- (UIView *)genderBgView {
    if (!_genderBgView) {
        _genderBgView = [UIView new];
        _genderBgView.backgroundColor = [UIColor whiteColor];
        _genderBgView.layer.cornerRadius = 8*kWScale;
        _genderBgView.hidden = YES;
    }
    return _genderBgView;
}


- (UIView *)maskBgView {
    if(!_maskBgView) {
        _maskBgView = [UIView new];
        _maskBgView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.4];
    }
    return _maskBgView;
}

#pragma mark ----- action

- (void)tapAction {
    if (self.avatBlock) {
        self.avatBlock();
    }
}

 
- (void)signatureButtonAction:(UIButton *)sender {
    DLog(@"点击个性签名");
    if (self.qianMingBlock && !self.isCustom) {
        self.qianMingBlock();
    }
}


- (void)setAvatImage:(UIImage *)avatImage {
    _avatImage = avatImage;
    self.avatarImage.image = avatImage;
}

- (void)setAvatImageStr:(NSString *)avatImageStr {
    _avatImageStr = avatImageStr;
    self.avatarImage.imageURL = [NSURL URLWithString:avatImageStr];
}

- (void)updateQianMing:(NSString *)qianMing {
    
    self.signatureLabel.text = qianMing;
}

//- (void)scrollViewDidScroll:(CGFloat)offsetY {
//    CGRect frame = self.bgImgFrame;
//
//    // 左右放大
//    if (offsetY <= 0) {
//        // 上下放大
//        frame.size.height -= offsetY;
//        frame.origin.y = offsetY;
//
//        frame.size.width = frame.size.height * self.bgImgFrame.size.width / self.bgImgFrame.size.height;
//        frame.origin.x   = (self.frame.size.width - frame.size.width) / 2;
//    }
//
//    self.headerBackView.frame = frame;
//}


@end
