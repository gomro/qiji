//
//  QJMineHeaderView.h
//  QJBox
//
//  Created by Sun on 2022/6/14.
//

#import <UIKit/UIKit.h>
#import "QJMineUserModel.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, QJMineHeaderStyle){
    QJMineHeaderStyleAvatar,   //头像
    QJMineHeaderStyleFans,     //粉丝
    QJMineHeaderStyleFollowers,//关注
    QJMineHeaderStyleFavorite, //收藏
    QJMineHeaderStyleSpace,    //空间
};

typedef void (^ClickHeaderItem)(QJMineHeaderStyle headerType);
@interface QJMineHeaderView : UIView
/**点击回传*/
@property (nonatomic, copy) ClickHeaderItem clickHeader;
@property (nonatomic, strong) QJMineUserModel *model;
@end


typedef void (^ClickView)(void);
@interface QJMineHeaderButtonView : UIView

/**上边数据*/
@property (nonatomic, strong) UILabel *topLabel;
/**下边数据*/
@property (nonatomic, strong) UILabel *bottomLabel;
/**点击回传*/
@property (nonatomic, copy) ClickView clickView;
/**数据传参*/
- (void)configureLabelString:(NSString *)topString bottomString:(NSString *)bottomString;

@end

NS_ASSUME_NONNULL_END
