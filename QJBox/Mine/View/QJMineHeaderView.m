//
//  QJMineHeaderView.m
//  QJBox
//
//  Created by Sun on 2022/6/14.
//

#import "QJMineHeaderView.h"
#import "QJMineLevelView.h"

@interface QJMineHeaderView ()
/**背景图*/
@property (nonatomic, strong) UIView *headerBackView;
/**背景图片*/
@property (nonatomic, strong) UIImageView *headerBackImageView;
/**头像*/
@property (nonatomic, strong) UIImageView *avatarImage;
/**博主等级头像*/
@property (nonatomic, strong) UIImageView *bloggerImage;
/**昵称*/
@property (nonatomic, strong) UILabel *nickNameLabel;
/**性别*/
@property (nonatomic, strong) UIImageView *genderImage;
/**签名*/
//@property (nonatomic, strong) UILabel *signatureLabel;
/**个人空间*/
@property (nonatomic, strong) UIButton *mySpaceBtn;

/**粉丝*/
@property (nonatomic, strong) QJMineHeaderButtonView *fansView;
/**关注*/
@property (nonatomic, strong) QJMineHeaderButtonView *followersView;
/**收藏*/
@property (nonatomic, strong) QJMineHeaderButtonView *favoriteView;

/**等级view*/
@property (nonatomic, strong) QJMineLevelView *levelView;

@end

@implementation QJMineHeaderView

#pragma mark - init
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self loadUI];
    }
    return self;
}

#pragma mark - Load UI
- (void)loadUI {
    [self addSubview:self.headerBackView];
    [self.headerBackView addSubview:self.headerBackImageView];
    [self.headerBackView addSubview:self.avatarImage];
    [self.headerBackView addSubview:self.bloggerImage];
    [self.headerBackView addSubview:self.nickNameLabel];
    [self.headerBackView addSubview:self.genderImage];
//    [self.headerBackView addSubview:self.signatureLabel];
    [self.headerBackView addSubview:self.fansView];
    [self.headerBackView addSubview:self.followersView];
    [self.headerBackView addSubview:self.favoriteView];
    [self.headerBackView addSubview:self.mySpaceBtn];
    [self.headerBackView addSubview:self.levelView];
    [self makeConstraints];
    self.nickNameLabel.text = @"未登录";
//    self.signatureLabel.text = @"有趣的个性签名可以吸引更多人哦～";
    [self.followersView configureLabelString:@"0" bottomString:@"关注"];
    [self.fansView configureLabelString:@"0" bottomString:@"粉丝"];
    [self.favoriteView configureLabelString:@"0" bottomString:@"收藏"];
    
    __weak typeof(self) weakSelf = self;
    self.followersView.clickView = ^{
        if (weakSelf.clickHeader) {
            weakSelf.clickHeader(QJMineHeaderStyleFollowers);
        }
    };
    
    self.fansView.clickView = ^{
        if (weakSelf.clickHeader) {
            weakSelf.clickHeader(QJMineHeaderStyleFans);
        }
    };
    
    self.favoriteView.clickView = ^{
        if (weakSelf.clickHeader) {
            weakSelf.clickHeader(QJMineHeaderStyleFavorite);
        }
    };
    
    UITapGestureRecognizer *avatarTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(avatarTap:)];
    [self.avatarImage addGestureRecognizer:avatarTap];
    [self.mySpaceBtn addTarget:self action:@selector(enterSpace:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)setModel:(QJMineUserModel *)model {
    _model = model;
    self.nickNameLabel.text = _model.nickname.length == 0 ? @"未登录" : _model.nickname;
//    self.signatureLabel.text = _model.signature.length == 0 ? @"有趣的个性签名可以吸引更多人哦～" : _model.signature;
    [self.avatarImage setImageWithURL:[NSURL URLWithString:[model.coverImage checkImageUrlString]] placeholder:[UIImage imageNamed:@"mine_head"]];
    self.followersView.topLabel.text = [self getFansNum:_model.starCount];
    self.fansView.topLabel.text = [self getFansNum:_model.fansCount];
    self.favoriteView.topLabel.text = [self getFansNum:_model.collectCount];
    if (_model.sex == 0) {
        [self.genderImage setHidden:YES];
    } else {
        [self.genderImage setHidden:NO];
        self.genderImage.image = _model.sex == 2 ? [UIImage imageNamed:@"mine_girl"] : [UIImage imageNamed:@"mine_boy"];
    }
    [self.levelView reloadLvView:_model.currentExperience NextEx:_model.nextLevelExperience level:self.model.level];
    self.bloggerImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"qj_bozhuLevel_%ld",self.model.bloggerLevel]];
}

- (NSString *)getFansNum:(NSInteger)num {
    if (num > 9999) {
        return [NSString stringWithFormat:@"%.1fw",(float)num/10000];
    }
    return [NSString stringWithFormat:@"%ld",num];
}

- (void)makeConstraints {
    [self.headerBackView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).offset(0);
        make.left.equalTo(self.mas_left).offset(0);
        make.right.equalTo(self.mas_right).offset(0);
        make.bottom.equalTo(self.mas_bottom).offset(0);
    }];
    
    [self.headerBackImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.bottom.mas_equalTo(self.headerBackView);
    }];
    
    [self.avatarImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.headerBackView.mas_bottom).mas_offset(-122);
        make.left.equalTo(self.headerBackView.mas_left).offset(16);
        make.width.height.mas_equalTo(64*kWScale);
    }];
    [self.bloggerImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.avatarImage.mas_bottom).mas_offset(0);
        make.right.equalTo(self.avatarImage.mas_right).offset(0);
        make.width.mas_equalTo(20*kWScale);
        make.height.mas_equalTo(17*kWScale);
    }];

    [self.nickNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(self.avatarImage.mas_top).offset(5);
        make.left.equalTo(self.avatarImage.mas_right).offset(16);
        make.centerY.equalTo(self.avatarImage);
        make.width.mas_lessThanOrEqualTo(kScreenWidth-150*kWScale);
    }];
    
    [self.genderImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.nickNameLabel.mas_right).offset(8);
        make.centerY.equalTo(self.nickNameLabel);
    }];

//    [self.signatureLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.bottom.equalTo(self.avatarImage.mas_bottom).offset(-10);
//        make.left.equalTo(self.nickNameLabel.mas_left);
//        make.right.mas_equalTo(-16);
//        make.height.mas_equalTo(20);
//    }];

    [self.followersView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.avatarImage.mas_bottom).offset(15);
        make.left.equalTo(self.headerBackView.mas_left).offset(0);
        make.height.mas_equalTo(50*kWScale);
        make.width.mas_equalTo(kScreenWidth/2/3);
    }];

    [self.fansView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.followersView);
        make.left.equalTo(self.followersView.mas_right).offset(0);
        make.height.mas_equalTo(50*kWScale);
        make.width.mas_equalTo(kScreenWidth/2/3);
    }];

    [self.favoriteView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.followersView);
        make.left.equalTo(self.fansView.mas_right).offset(0);
        make.height.mas_equalTo(50*kWScale);
        make.width.mas_equalTo(kScreenWidth/2/3);
    }];

    [self.mySpaceBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.fansView);
        make.right.equalTo(self.headerBackView.mas_right).offset(-24);
        make.height.mas_equalTo(Get375Width(42));
        make.width.mas_equalTo(Get375Width(140));
    }];

    [self.levelView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.headerBackView.mas_bottom).offset(0);
        make.left.equalTo(self.headerBackView).offset(16);
        make.right.equalTo(self.headerBackView).offset(-16);
        make.height.mas_equalTo(48*kWScale);
    }];


    
}

#pragma mark - Lazy Loading
- (UIView *)headerBackView {
    if (!_headerBackView) {
        self.headerBackView = [UIView new];
        self.headerBackView.backgroundColor = [UIColor whiteColor];
    }
    return _headerBackView;
}

- (UIImageView *)headerBackImageView {
    if (!_headerBackImageView) {
        _headerBackImageView = [UIImageView new];
        _headerBackImageView.image = [UIImage imageNamed:@"mine_dragon"];
        _headerBackImageView.contentMode = UIViewContentModeScaleAspectFill;
        _headerBackImageView.layer.masksToBounds = YES;
    }
    return _headerBackImageView;
}

- (UIImageView *)avatarImage {
    if (!_avatarImage) {
        self.avatarImage = [UIImageView new];
        self.avatarImage.image = [UIImage imageNamed:@"mine_head"];
        self.avatarImage.contentMode = UIViewContentModeScaleAspectFill;
        self.avatarImage.layer.masksToBounds = YES;
        self.avatarImage.layer.cornerRadius = 32*kWScale;
        self.avatarImage.userInteractionEnabled = YES;
    }
    return _avatarImage;
}

- (UIImageView *)bloggerImage {
    if (!_bloggerImage) {
        self.bloggerImage = [UIImageView new];
        self.bloggerImage.image = [UIImage imageNamed:@"avatarLevelIcon"];
    }
    return _bloggerImage;
}

- (UILabel *)nickNameLabel {
    if (!_nickNameLabel) {
        self.nickNameLabel = [UILabel new];
        self.nickNameLabel.font = FontSemibold(20);
        self.nickNameLabel.textColor = UIColorHex(16191c);
    }
    return _nickNameLabel;
}

- (UIImageView *)genderImage {
    if (!_genderImage) {
        self.genderImage = [UIImageView new];
    }
    return _genderImage;
}

//- (UILabel *)signatureLabel {
//    if (!_signatureLabel) {
//        self.signatureLabel = [UILabel new];
//        self.signatureLabel.font = FontRegular(12);
//        self.signatureLabel.textColor = UIColorHex(16191c);
//    }
//    return _signatureLabel;
//}

- (QJMineHeaderButtonView *)followersView {
    if (!_followersView) {
        self.followersView = [QJMineHeaderButtonView new];
    }
    return _followersView;
}

- (QJMineHeaderButtonView *)fansView {
    if (!_fansView) {
        self.fansView = [QJMineHeaderButtonView new];
    }
    return _fansView;
}

- (QJMineHeaderButtonView *)favoriteView {
    if (!_favoriteView) {
        self.favoriteView = [QJMineHeaderButtonView new];
    }
    return _favoriteView;
}

- (UIButton *)mySpaceBtn {
    if (!_mySpaceBtn) {
        self.mySpaceBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.mySpaceBtn setBackgroundImage:[UIImage imageNamed:@"mine_frame"] forState:UIControlStateNormal];
    }
    return _mySpaceBtn;
}

- (QJMineLevelView *)levelView {
    if (!_levelView) {
        _levelView = [QJMineLevelView new];
    }
    return _levelView;
}

#pragma mark - Click
- (void)avatarTap:(UITapGestureRecognizer *)sender {
    if (self.clickHeader) {
        self.clickHeader(QJMineHeaderStyleAvatar);
    }
}

- (void)enterSpace:(UIButton *)sender {
    if (self.clickHeader) {
        self.clickHeader(QJMineHeaderStyleSpace);
    }
}
@end

@interface QJMineHeaderButtonView ()
/**点击Button*/
@property (nonatomic, strong) UIButton *clickButton;

@end

@implementation QJMineHeaderButtonView

#pragma mark - init
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self loadUI];
    }
    return self;
}

- (void)loadUI {
    [self addSubview:self.topLabel];
    [self addSubview:self.bottomLabel];
    [self addSubview:self.clickButton];
    [self makeConstraints];
}

- (void)makeConstraints {
    [self.topLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).offset(0);
        make.centerX.equalTo(self);
    }];
    
    [self.bottomLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.top.equalTo(self.topLabel.mas_bottom).offset(4);
        make.width.mas_lessThanOrEqualTo(100);
    }];
    
    [self.clickButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    
//    [self.bottomLabel setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
    
}

#pragma mark - Lazy Loading
- (UIButton *)clickButton {
    if (!_clickButton) {
        _clickButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_clickButton addTarget:self action:@selector(btnAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _clickButton;
}

- (UILabel *)topLabel {
    if (!_topLabel) {
        self.topLabel = [UILabel new];
        self.topLabel.textColor = UIColorHex(16191c);
        self.topLabel.font = FontSemibold(16);
        self.topLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _topLabel;
}

- (UILabel *)bottomLabel {
    if (!_bottomLabel) {
        self.bottomLabel = [UILabel new];
        self.bottomLabel.textColor = UIColorHex(16191c);
        self.bottomLabel.font = FontRegular(12);
    }
    return _bottomLabel;
}

#pragma mark - Configure Data
- (void)configureLabelString:(NSString *)topString bottomString:(NSString *)bottomString {
    self.topLabel.text = topString;
    self.bottomLabel.text = bottomString;
}

#pragma mark - Click
- (void)btnAction {
    if (self.clickView) {
        self.clickView();
    }
}

@end

