//
//  QJAccountDetailHeaderView.m
//  QJBox
//
//  Created by Sun on 2022/8/22.
//

#import "QJAccountDetailHeaderView.h"
#import "QJImageButton.h"
#import "NSDate+BRPickerView.h"

@interface QJAccountDetailHeaderView ()

@property (nonatomic, strong) QJImageButton *monthButton;
@property (nonatomic, copy) NSString *title;
@end
@implementation QJAccountDetailHeaderView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = UIColorHex(F5F5F5);
        [self loadUI];
    }
    return self;
}

- (void)loadUI {
    [self addSubview:self.monthButton];
    [self.monthButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self);
        make.height.mas_equalTo(16*kWScale);
        make.left.equalTo(self.mas_left).offset(16);
    }];
}

- (QJImageButton *)monthButton {
    if (!_monthButton) {
        _monthButton = [[QJImageButton alloc] init];
        _monthButton.titleLb.font = MYFONTALL(FONT_MEDIUM, 16);
        _monthButton.titleLb.textColor = UIColorHex(16191C);
        _monthButton.iconString = @"accountDown";
        _monthButton.imageSize = CGSizeMake(16, 16);
        _monthButton.space = 1;
        _monthButton.style = ImageButtonStyleRight;
        [_monthButton addTarget:self action:@selector(monthPicker:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _monthButton;
}

- (void)monthPicker:(UIButton *)sender {
    if (self.clickHeader) {
        self.clickHeader(kCheckStringNil(self.title));
    }
}


- (void)configureHeaderWithTitle:(NSString *)title {
    NSDate *nowDate = [NSDate br_dateFromString:title dateFormat:@"yyyy-MM"];
    NSString *now = [NSDate br_stringFromDate:nowDate dateFormat:@"yyyy年MM月"];
    _monthButton.titleString = now;
    self.title = title;
}
@end
