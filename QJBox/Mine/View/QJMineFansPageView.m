//
//  QJMineFansPageView.m
//  QJBox
//
//  Created by macm on 2022/7/19.
//

#import "QJMineFansPageView.h"
#import "QJMineFansTableViewCell.h"
#import "QJMineRequest.h"

@interface QJMineFansPageView ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, assign) NSInteger page;
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, assign) NSInteger type;
@property (nonatomic, copy) NSString *uid;
@property (nonatomic, copy) NSString *allNum;//总人数
@end

@implementation QJMineFansPageView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.page = 1;
        self.dataArray = [NSMutableArray array];
        [self createView];
    }
    return self;
}
/**
 * 刷新数据
 * type：0:关注 1：粉丝
 * uid：用户id
 */
- (void)reloadPageView:(NSInteger)type uid:(NSString *)uid {
    self.type = type;
    self.uid = uid;
    [self sendRequest];
}

- (void)loadData {
    _page = 1;
    [self.dataArray removeAllObjects];
    [self sendRequest];
}

- (void)loadMoreData {
    _page++;
    [self sendRequest];
}

- (void)sendRequest {

    @weakify(self);
    QJMineRequest *request = [[QJMineRequest alloc] init];
    if (self.type == 1) {
        [request requestUserFans:self.uid page:_page completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
            @strongify(self);
            [self.tableView.mj_header endRefreshing];
            [self.tableView.mj_footer endRefreshing];

            if (isSuccess) {
                NSArray *array = [NSArray modelArrayWithClass:[QJMineFansModel class] json:EncodeArrayFromDic(request.responseJSONObject[@"data"], @"records")];
                self.allNum = [NSString stringWithFormat:@"%@人", request.responseJSONObject[@"data"][@"total"]];
                [self.dataArray addObjectsFromArray:array];
                [self.tableView reloadData];
                
                if ([request.responseJSONObject[@"data"][@"hasNext"] integerValue] == 0 || array.count == 0) {
                    [self.tableView.mj_footer endRefreshingWithNoMoreData];
                }
            }
            QJEmptyCommonView *empty = [QJEmptyCommonView new];
            empty.string = @"还没有人关注你哦~";
            [self.tableView tableViewDisplayWhenHaveNoDataWithView:empty ifNecessaryForRowCount:self.dataArray.count];

        }];
    } else {
        [request requestUserStars:self.uid page:_page completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
            @strongify(self);
            [self.tableView.mj_header endRefreshing];
            [self.tableView.mj_footer endRefreshing];
            
            DLog(@"-=-=-==-%@",request.responseJSONObject[@"code"]);
            if (isSuccess) {
                NSArray *array = [NSArray modelArrayWithClass:[QJMineFansModel class] json:EncodeArrayFromDic(request.responseJSONObject[@"data"], @"records")];
                self.allNum = [NSString stringWithFormat:@"%@人", request.responseJSONObject[@"data"][@"total"]];
                [self.dataArray addObjectsFromArray:array];
                [self.tableView reloadData];
                
                if ([request.responseJSONObject[@"data"][@"hasNext"] integerValue] == 0 || array.count == 0) {
                    [self.tableView.mj_footer endRefreshingWithNoMoreData];
                }
            }
            QJEmptyCommonView *empty = [QJEmptyCommonView new];
            empty.string = @"还没有关注任何人哦~";
            [self.tableView tableViewDisplayWhenHaveNoDataWithView:empty ifNecessaryForRowCount:self.dataArray.count];
        }];
    }
    
}


- (void)createView {
    [self addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.left.right.mas_equalTo(0);
    }];
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 1;
    }
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
        cell.backgroundColor = [UIColor clearColor];
        cell.textLabel.textAlignment = NSTextAlignmentRight;
        cell.textLabel.text = self.allNum;
        cell.textLabel.font = FontRegular(14);
        cell.textLabel.textColor = UIColorFromRGB(0x919599);
        return cell;
    }
    QJMineFansTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QJMineFansTableViewCell" forIndexPath:indexPath];
    cell.model = [self.dataArray safeObjectAtIndex:indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    QJMineFansModel *mode = [self.dataArray safeObjectAtIndex:indexPath.row];
    if (self.didSelectedCell) {
        self.didSelectedCell(mode);
    }
   
    
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return 30*kWScale;
    }
    return 60*kWScale;
}

- (UITableView *)tableView {
    if (!_tableView) {
        self.tableView = [[UITableView alloc] init];
        self.tableView.backgroundColor = [UIColor whiteColor];
        self.tableView.dataSource = self;
        self.tableView.delegate = self;
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.tableView.showsVerticalScrollIndicator = NO;
        self.tableView.showsHorizontalScrollIndicator = NO;
        [self.tableView registerClass:[QJMineFansTableViewCell class] forCellReuseIdentifier:@"QJMineFansTableViewCell"];
        [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];

        if (@available(iOS 11.0, *)) {
            self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
        self.tableView.mj_header = [QJRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadData)];
        self.tableView.mj_footer = [QJRefreshFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
 
    }
    return _tableView;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
