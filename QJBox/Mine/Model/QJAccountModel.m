//
//  QJAccountModel.m
//  QJBox
//
//  Created by Sun on 2022/8/24.
//

#import "QJAccountModel.h"

@implementation QJAccountModel
- (id)initWithDictionary:(NSDictionary *)dic {
    self = [super init];
    if (self) {
        [self setValuesForKeysWithDictionary:dic];
        NSString *time = kCheckStringNil(dic[@"time"]);
        if ([time isEqualToString:@""]) {
            self.timeDate = [NSDate new];
            self.timeSection = @"";
        } else {
            NSDate *currentDate = [NSString getDateForString:time format:@"yyyy-MM-dd HH:mm:ss"];
            NSString *dateStr = [NSString getStringForDate:currentDate format:@"yyyy-MM"];
            self.timeDate = currentDate;
            self.timeSection = dateStr;
        }
    }
    return self;
}

+ (id)QJAccountWithDictionary:(NSDictionary *)dic {
    return [[self alloc] initWithDictionary:dic];
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key {
    if ([key isEqualToString:@"description"]) {
        self.descriptionStr = value;
    }

}
@end
