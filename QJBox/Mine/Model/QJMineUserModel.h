//
//  QJMineUserModel.h
//  QJBox
//
//  Created by macm on 2022/7/19.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
// 点击事件类型
typedef NS_ENUM(NSInteger, QJMineViewCellStyle){
    QJMineViewCellStyleFoot = 0,  //足迹
    QJMineViewCellStyleAuth,     //认证
    QJMineViewCellStyleAddress,     //收货地址
    QJMineViewCellStyleHelp,    //帮助
    QJMineViewCellStyleKefu,    //客服
    QJMineViewCellStyleHonor,    //荣耀
    QJMineViewCellStyleQJB,
    QJMineViewCellStyleTask, //任务
    QJMineViewCellStyleChange, //兑换
    QJMineViewCellStyleDeal, //买卖
    QJMineViewCellStyleAccount, //账户
};

@interface QJMineUserModel : NSObject
@property (nonatomic, assign) NSInteger collectCount;    //收藏数    integer(int32)
@property (nonatomic, copy) NSString *coverImage;    //头像    string
@property (nonatomic, assign) NSInteger fansCount;    //粉丝数    integer(int32)
@property (nonatomic, copy) NSString *idStr;
@property (nonatomic, copy) NSString *nickname;    //昵称    string
@property (nonatomic, assign) NSInteger qjcoinCurrent;//    奇迹币数量    integer(int32)
@property (nonatomic, assign) NSInteger qjcoinConsumed;//    奇迹币数量（总消费）    integer(int32)
@property (nonatomic, assign) NSInteger qjcoinGot;//    奇迹币数量(总获取)    integer(int32)
@property (nonatomic, assign) NSInteger sex;//   性别    integer(int32) 未设置0 男1 女2
@property (nonatomic, copy) NSString *signature;//    个性签名    string
@property (nonatomic, assign) NSInteger starCount;//    关注数    integer(int32)
@property (nonatomic, assign) NSInteger currentExperience;//    用户当前经验值    integer(int64)
@property (nonatomic, assign) NSInteger nextLevelExperience;//    用户下一等级经验值    integer(int64)
@property (nonatomic, assign) NSInteger level;//    用户等级    integer(int32)
@property (nonatomic, assign) NSInteger bloggerLevel;//    博主等级    integer(int32)

@property (nonatomic, copy) NSString *levelIcon;//    用户等级图标    string
@property (nonatomic, copy) NSString *backgroundImage;//    背景图    string
@property (nonatomic, copy) NSString *realName;//    姓名    string
@property (nonatomic, copy) NSString *idCard;//    身份证号    string
@property (nonatomic, assign) BOOL verifiedState; //   是否实名认证    boolean

@property (nonatomic, copy) NSString *appleName;    //  苹果名称    string
@property (nonatomic, copy) NSString *birthday;    //生日    string(date-time)
@property (nonatomic, assign) BOOL campNewsOpen; //   动态是否可查看    boolean
@property (nonatomic, copy) NSString *cancelTime;  //  注销时间    string(date-time)
@property (nonatomic, assign) BOOL informationOpen; //    创作是否可查看    boolean
@property (nonatomic, copy) NSString *mobile; //    手机号    string
@property (nonatomic, assign) BOOL notificationSettingsCollectMe; //    消息提醒设置: 开关: 收藏我的    boolean
@property (nonatomic, assign) BOOL notificationSettingsLikeMe; //    消息提醒设置: 开关: 赞我的    boolean
@property (nonatomic, assign) BOOL notificationSettingsReplyMe; //    消息提醒设置: 开关: 回复我的    boolean
@property (nonatomic, assign) BOOL notificationSettingsStarMe; //    消息提醒设置: 开关: 关注我的    boolean
@property (nonatomic, copy) NSString *qqName;//    qq名称    string
@property (nonatomic, copy) NSString *wxName;//    微信名称    string
@property (nonatomic, copy) NSString *registerTime;//注册时间   string

@end

NS_ASSUME_NONNULL_END
