//
//  QJHelpCenterModel.m
//  QJBox
//
//  Created by Sun on 2022/8/25.
//

#import "QJHelpCenterModel.h"

@implementation QJHelpCenterModel
+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"helpCenters" : [QJHelpCenterDetailModel class]
    };
}
@end


@implementation QJHelpCenterDetailModel

+ (NSDictionary *)modelCustomPropertyMapper{
    return @{@"detailId":@"id"};
}
@end
