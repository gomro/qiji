//
//  QJMySpaceModel.h
//  QJBox
//
//  Created by wxy on 2022/7/18.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJMySpaceModel : NSObject

@property (nonatomic, copy) NSString *beLikedCount;//获赞数

@property (nonatomic, copy) NSString *bloggerLevel;//博主资质等级

@property (nonatomic, copy) NSString *bloggerState;//博主认证状态

@property (nonatomic, copy) NSString *beCollectCount;//收藏数

@property (nonatomic, copy) NSString *coverImage;//头像

@property (nonatomic, copy) NSString *fansCount;//粉丝数

@property (nonatomic, copy) NSString *level;//用户等级

@property (nonatomic, copy) NSString *nickname;//昵称

@property (nonatomic, assign) NSInteger sex;//性别未设置=0, 男=1, 女=2

@property (nonatomic, copy) NSString *birthday;

@property (nonatomic, copy) NSString *signature;//个性签名

@property (nonatomic, copy) NSString *starCount;//关注数

@property (nonatomic, strong)  NSArray*honors;//荣誉标签列表

@property (nonatomic, copy) NSString *backgroundImage;//背景图

@property (nonatomic, copy) NSString *bloggerLevelIcon;//博主资质等级图标

@property (nonatomic, copy) NSString *levelIcon;//用户等级图标

@property (nonatomic, copy) NSString *stared;//关注状态

@property (nonatomic, assign) BOOL verifiedState;//实名认证状态

@property (nonatomic, copy) NSString *idStr;

@end


@interface QJMineUserHonorsModel : NSObject

@property (nonatomic, copy) NSString *icon;//图标

@property (nonatomic, copy) NSString *name;//名称

@end

NS_ASSUME_NONNULL_END
