//
//  QJHelpCenterModel.h
//  QJBox
//
//  Created by Sun on 2022/8/25.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJHelpCenterModel : NSObject
/**类型标题*/
@property (nonatomic, copy) NSString *typeName;
/**类型id*/
@property (nonatomic, copy) NSString *typeId;
/**问题数组*/
@property (nonatomic, copy) NSArray *helpCenters;
/**类型图片*/
@property (nonatomic, copy) NSString *url;
@end


@interface QJHelpCenterDetailModel : NSObject
/**问题id*/
@property (nonatomic, copy) NSString *detailId;
/**类型id*/
@property (nonatomic, copy) NSString *typeId;
/**问题名称*/
@property (nonatomic, copy) NSString *name;
/**公告内容 (富文本)*/
@property (nonatomic, copy) NSString *content;
/**跳转链接*/
@property (nonatomic, copy) NSString *linkAddress;

@end

NS_ASSUME_NONNULL_END
