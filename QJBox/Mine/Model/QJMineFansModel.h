//
//  QJMineFansModel.h
//  QJBox
//
//  Created by macm on 2022/7/20.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJMineFansModel : NSObject
@property (nonatomic, copy) NSString *uid;
@property (nonatomic, copy) NSString *nickname;    //昵称    string
@property (nonatomic, copy) NSString *signature;//    个性签名    string
@property (nonatomic, copy) NSString *coverImage;    //头像    string

@end

NS_ASSUME_NONNULL_END
