//
//  QJMineFootModel.h
//  QJBox
//
//  Created by macm on 2022/8/2.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJMineFootModel : NSObject
@property (nonatomic, copy) NSString *timeShow;
@property (nonatomic, copy) NSString *categoryName; //    原数据分类    string
@property (nonatomic, copy) NSString *idStr; //        string
@property (nonatomic, copy) NSString *sourceId; //    原数据id    string
@property (nonatomic, copy) NSString *title; //    原数据标题    string
@property (nonatomic, copy) NSString *type; //    0-游戏，1-资讯，2-动态    string
@property (nonatomic, assign) BOOL isDelete; // 是否删除

@end

NS_ASSUME_NONNULL_END
