//
//  QJMySpaceModel.m
//  QJBox
//
//  Created by wxy on 2022/7/18.
//

#import "QJMySpaceModel.h"

@implementation QJMySpaceModel


+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"honors" : [QJMineUserHonorsModel class]};
}


+ (NSDictionary *)modelCustomPropertyMapper{
    return @{@"idStr":@"id"};
}


@end


@implementation QJMineUserHonorsModel

 

@end
