//
//  QJAccountModel.h
//  QJBox
//
//  Created by Sun on 2022/8/24.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJAccountModel : NSObject
/**是否是新增 */
@property (nonatomic, assign) BOOL add;

/**描述*/
@property (nonatomic, copy) NSString *descriptionStr;

/**游戏名*/
@property (nonatomic, copy) NSString *gameName;

/**变动数值*/
@property (nonatomic, assign) NSInteger qjcoinChange;

/**变动时间*/
@property (nonatomic, copy) NSString *time;
/**时间标题*/
@property (nonatomic, copy) NSString *timeSection;
/**时间Date*/
@property (nonatomic, strong) NSDate *timeDate;



- (id)initWithDictionary:(NSDictionary *)dic;
+ (id)QJAccountWithDictionary:(NSDictionary *)dic;
@end

NS_ASSUME_NONNULL_END
