//
//  QJHelpCenterDetailViewController.h
//  QJBox
//
//  Created by Sun on 2022/8/25.
//

#import "QJBaseViewController.h"
#import "QJHelpCenterModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJHelpCenterDetailViewController : QJBaseViewController
@property (nonatomic, copy) NSString *idStr;
@end

NS_ASSUME_NONNULL_END
