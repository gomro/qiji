//
//  QJMineStarAlertViewController.h
//  QJBox
//
//  Created by wxy on 2022/7/19.
//

#import "QJBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN


typedef NS_ENUM(NSUInteger, QJMineStarAlertViewStyle) {
    QJMineStarAlertViewStyle_collection,
    QJMineStarAlertViewStyle_star,
};

/// 我的空间 被点赞、被收藏展示弹框
@interface QJMineStarAlertViewController : QJBaseViewController

@property (nonatomic, assign) QJMineStarAlertViewStyle style;

@property (nonatomic, copy) NSString *uid;

@property (nonatomic, assign) BOOL isAnother;//是在观看否他人的空间

@end


@interface QJMineStarAlertModel : NSObject


@property (nonatomic, copy) NSString *count;


@property (nonatomic, copy) NSString *name;

@end

NS_ASSUME_NONNULL_END
