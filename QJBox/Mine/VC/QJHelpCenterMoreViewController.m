//
//  QJHelpCenterMoreViewController.m
//  QJBox
//
//  Created by Sun on 2022/8/25.
//

#import "QJHelpCenterMoreViewController.h"
#import "QJMineRequest.h"
#import "QJHelpCenterModel.h"
#import "QJHelpCenterViewCell.h"
#import "QJHelpCenterDetailViewController.h"

@interface QJHelpCenterMoreViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
/**初始数据*/
@property (nonatomic, strong) NSMutableArray *dataArr;
@property (nonatomic, assign) NSInteger page;

@end

@implementation QJHelpCenterMoreViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = self.typeName;
    [self makeTableView];
    self.page = 1;
    [self getMoreList];
    self.tableView.mj_header = [QJRefreshHeader headerWithRefreshingBlock:^{
        self.page = 1;
        [self getMoreList];
    }];
    
    QJRefreshFooter *footerView = [QJRefreshFooter footerWithRefreshingBlock:^{
        [self getMoreList];
    }];
    [footerView setTitle:@"已经到底啦~" forState:MJRefreshStateNoMoreData];
    self.tableView.mj_footer = footerView;
}
- (void)makeTableView {
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_top).offset(NavigationBar_Bottom_Y);
        make.left.right.bottom.equalTo(self.view);
    }];
}


- (NSMutableArray *)dataArr {
    if (!_dataArr) {
        _dataArr = [NSMutableArray arrayWithCapacity:0];
    }
    return _dataArr;
}

- (UITableView *)tableView {
    if (!_tableView) {
        self.tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        self.tableView.backgroundColor = UIColorHex(F5F5F5);
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.tableView.showsVerticalScrollIndicator = NO;
        self.tableView.showsHorizontalScrollIndicator = NO;
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        if (@available(iOS 11.0, *)) {
            self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
    }
    return _tableView;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellHelpCenter = @"cellHelpCenterId";
    QJHelpCenterSearchViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellHelpCenter];
    if (!cell) {
        cell = [[QJHelpCenterSearchViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellHelpCenter];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    QJHelpCenterDetailModel *detailModel = self.dataArr[indexPath.row];
    [cell configureCellWithData:detailModel isDetail:YES];
    MJWeakSelf
    cell.clickCell = ^(QJHelpCenterDetailModel * _Nonnull model) {
        [weakSelf pushDetailView:model];
    };
    return cell;
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 42;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 8)];
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 8;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 0.001)];

    return footerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.001;
}
#pragma mark - 跳转

- (void)pushMoreView:(NSString *)typeName typeId:(NSString *)typeId {
    QJHelpCenterMoreViewController *moreVC = [[QJHelpCenterMoreViewController alloc] init];
    moreVC.typeId = typeId;
    moreVC.typeName = typeName;
    [self.navigationController pushViewController:moreVC animated:YES];
}
- (void)pushDetailView:(QJHelpCenterDetailModel *)model {
    QJHelpCenterDetailViewController *detailVC = [[QJHelpCenterDetailViewController alloc] init];
    detailVC.idStr = model.detailId;
    [self.navigationController pushViewController:detailVC animated:YES];
}
#pragma mark - data
- (void)getMoreList {
    QJMineRequest *startRequest = [[QJMineRequest alloc] init];
    [startRequest requestHelpCenterMoreQuestionType:self.typeId page:self.page];
    [startRequest setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            if (self.page == 1) {
                [self.dataArr removeAllObjects];
                [self.tableView.mj_footer resetNoMoreData];
                [self.tableView.mj_header endRefreshing];
            } else {
                [self.tableView.mj_footer endRefreshing];
            }
            BOOL hasNext = [request.responseJSONObject[@"data"][@"hasNext"] boolValue];
            if (hasNext) {
                self.page += 1;
            } else {
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
            }
            
            NSArray *dataArr = request.responseJSONObject[@"data"][@"records"];
            for (NSDictionary *dic in dataArr) {
                QJHelpCenterDetailModel *model = [QJHelpCenterDetailModel modelWithDictionary:dic];
                [self.dataArr addObject:model];
            }
            [self.tableView reloadData];

        }
        QJEmptyCommonView *emptyView = [QJEmptyCommonView new];
        emptyView.string = @"还没有内容哦~";
        [self.tableView tableViewDisplayWhenHaveNoDataWithView:emptyView ifNecessaryForRowCount:self.dataArr.count];
    }];

    [startRequest setFailureCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        if (self.page == 1) {
            [self.tableView.mj_footer resetNoMoreData];
            [self.tableView.mj_header endRefreshing];
        } else {
            [self.tableView.mj_footer endRefreshing];
        }
        QJEmptyCommonView *emptyView = [QJEmptyCommonView new];
        emptyView.string = @"还没有内容哦~";
        [self.tableView tableViewDisplayWhenHaveNoDataWithView:emptyView ifNecessaryForRowCount:self.dataArr.count];
    }];
}
@end
