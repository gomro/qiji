//
//  QJHelpCenterSearchVC.h
//  QJBox
//
//  Created by Sun on 2022/8/19.
//

#import "QJBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJHelpCenterSearchVC : QJBaseViewController
@property (nonatomic, assign) BOOL isAI; // 是否是ai客服页面进入
@property (nonatomic, copy) NSString *searchStr; // 默认搜索内容
@end

NS_ASSUME_NONNULL_END
