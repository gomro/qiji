//
//  QJMineFansViewController.h
//  QJBox
//
//  Created by macm on 2022/7/19.
//

#import "QJBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJMineFansViewController : QJBaseViewController
@property (nonatomic, copy) NSString *uid;
@property (nonatomic, assign) NSInteger type;//0:关注 1：粉丝

@property (nonatomic, copy) NSString *userName;//名称
@property (nonatomic, copy) NSString *userIcon;//头像
@end

NS_ASSUME_NONNULL_END
