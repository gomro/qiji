//
//  QJHelpCenterDetailViewController.m
//  QJBox
//
//  Created by Sun on 2022/8/25.
//

#import "QJHelpCenterDetailViewController.h"
#import "QJMineRequest.h"

@interface QJHelpCenterDetailViewController ()<WKUIDelegate,WKNavigationDelegate>
/**web*/
@property (nonatomic, strong) WKWebView *webView;
/**webView高度*/
@property (nonatomic, assign) CGFloat height;
/**底部视图*/
@property (nonatomic, strong) UIView *footerView;
/**底部视图高度*/
@property (nonatomic, assign) CGFloat footerHeight;
/**数据*/
@property (nonatomic, strong) QJHelpCenterDetailModel *dataModel;

@property (nonatomic, strong) UIImageView *scaleImage;
@end

@implementation QJHelpCenterDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.footerHeight = 48 + Bottom_iPhoneX_SPACE;

    [self makeConstraints];
    [self getDetailData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (self.scaleImage) {
        [self imageAnimationClick:self.scaleImage];
    }
}

- (void)getDetailData {
    QJMineRequest *startRequest = [[QJMineRequest alloc] init];
    [startRequest requestHelpCenterQuestionDetailTypeID:kCheckStringNil(self.idStr)];
    [startRequest setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            NSDictionary *dataDic = request.responseJSONObject[@"data"];
            QJHelpCenterDetailModel *model = [QJHelpCenterDetailModel modelWithDictionary:dataDic];
            self.dataModel = model;
            self.title = self.dataModel.name;
            [self configureCellWithDataModel:kCheckStringNil(model.content)];
        }
    }];
    [startRequest setFailureCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
  
    }];
}

- (void)makeConstraints {
    [self.view addSubview:self.webView];
    [self.webView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_top).offset(NavigationBar_Bottom_Y);
        make.left.right.equalTo(self.view);
        make.height.mas_equalTo(kScreenHeight - NavigationBar_Bottom_Y - self.footerHeight);
    }];
    
    _footerView = [[UIView alloc]initWithFrame:CGRectMake(0, kScreenHeight + 10, kScreenWidth, self.footerHeight)];
    [self.webView.scrollView addSubview:self.footerView];
    [self setLinkView];
}

- (void)configureCellWithDataModel:(NSString *)htmlContent {

    NSString *headerString = @"<header><meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no'><style>img{max-width:100%, border-radius:20px}</style></header>";
    [self.webView loadHTMLString:[headerString stringByAppendingString:kCheckStringNil(htmlContent)] baseURL:nil];

}

#pragma mark - Lazy Loading
- (WKWebView *)webView{
    if (!_webView) {
        _webView = [[WKWebView alloc] initWithFrame:CGRectZero];
        _webView.navigationDelegate = self;
        _webView.UIDelegate=self;
        [_webView.scrollView addObserver:self forKeyPath:@"contentSize" options:NSKeyValueObservingOptionNew context:nil];
    }
    return _webView;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context{
    CGFloat newHeight = _webView.scrollView.contentSize.height;
    if (self.height && newHeight == self.height) {
        return;
    }
//    self.height = newHeight;
//    _webView.frame=CGRectMake(0, 0, QJScreenWidth, newHeight);
//    [self.webView mas_updateConstraints:^(MASConstraintMaker *make) {
//        make.height.mas_equalTo(newHeight);
//    }];
//    self.webView.height = newHeight;
//
//    CGRect frame = self.footerView.frame;
//
//    frame.origin.y = newHeight;
//    self.footerView.frame = frame;
//    [self.webView.scrollView addSubview:_footerView];
}

#pragma mark WKWebView
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    NSString *js1 = [NSString stringWithFormat:@"document.documentElement.style.webkitUserSelect='none';"];
    NSString *js2 = [NSString stringWithFormat:@"document.documentElement.style.webkitTouchCallout='none';"];
    [self.webView evaluateJavaScript:@"document.body.style.backgroundColor=\"#F5F5F5\"" completionHandler:nil];

    [self.webView evaluateJavaScript:js1 completionHandler:nil];
    [self.webView evaluateJavaScript:js2 completionHandler:nil];
    
    MJWeakSelf
    [webView evaluateJavaScript:@"document.body.scrollHeight"
              completionHandler:^(id result, NSError *_Nullable error) {
       
        CGFloat newHeight = [result floatValue];
        if (newHeight + self.footerHeight >= kScreenHeight - NavigationBar_Bottom_Y) {
            newHeight = kScreenHeight - NavigationBar_Bottom_Y;
            CGRect frame = weakSelf.footerView.frame;
            frame.origin.y = newHeight - self.footerHeight;
            weakSelf.footerView.frame = frame;
            weakSelf.webView.scrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
        } else {
            CGRect frame = weakSelf.footerView.frame;
            frame.origin.y = newHeight;
            weakSelf.footerView.frame = frame;
            weakSelf.webView.scrollView.contentInset = UIEdgeInsetsMake(0, 0, self.footerHeight, 0);
        }
        if ([kCheckStringNil(self.dataModel.linkAddress) isEqualToString:@""]) {
            weakSelf.footerView.hidden = YES;
            weakSelf.webView.scrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
        }
        
        weakSelf.height = newHeight;
        [weakSelf.webView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(newHeight);
        }];
    }];

}

- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(nonnull WKNavigationAction *)navigationAction decisionHandler:(nonnull void (^)(WKNavigationActionPolicy))decisionHandler {
//    NSString *requestString = [navigationAction.request.URL.absoluteString stringByRemovingPercentEncoding];
//    NSArray *components = [requestString componentsSeparatedByString:@":"];
    decisionHandler(WKNavigationActionPolicyAllow);
}

- (UIView *)footerView {
    if (!_footerView) {
        _footerView = [UIView new];
        
    }
    return _footerView;
}

- (void)dealloc{
    [_webView.scrollView removeObserver:self forKeyPath:@"contentSize"];
}

- (void)setLinkView {
    UIView *linkBackView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 48)];
    [self.footerView addSubview:linkBackView];
    
    UILabel *title = [UILabel new];
    [linkBackView addSubview:title];
    title.font = MYFONTALL(FONT_REGULAR, 14);
    title.textColor = UIColorHex(007AFF);
    title.text = @"点击发现更多内容哦";
    [linkBackView addSubview:title];
    [title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(linkBackView);
        make.centerX.equalTo(linkBackView.mas_centerX).offset(-9);
    }];
    
    UIView *line = [UIView new];
    line.backgroundColor = UIColorHex(007AFF);
    [linkBackView addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(title.mas_bottom).offset(-5);
        make.left.equalTo(title.mas_left);
        make.right.equalTo(title.mas_right);
        make.height.mas_equalTo(1);
    }];
    
    UIImageView *image = [UIImageView new];
    image.image = [UIImage imageNamed:@"helpLinkGroup"];
    [linkBackView addSubview:image];
    [image mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(linkBackView);
        make.left.equalTo(title.mas_right).offset(2);
        make.width.height.mas_equalTo(16);
    }];
    self.scaleImage = image;
    [self imageAnimationClick:image];
    
    UIButton *clickButton = [UIButton new];
    [linkBackView addSubview:clickButton];
    [clickButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(title.mas_top);
        make.bottom.equalTo(title.mas_bottom);
        make.left.equalTo(title.mas_left);
        make.right.equalTo(image.mas_right);
    }];
    [clickButton addTarget:self action:@selector(clickLinkView) forControlEvents:UIControlEventTouchUpInside];
}

- (void)clickLinkView {
    if (![kCheckStringNil(self.dataModel.linkAddress) isEqualToString:@""]) {
        QJWKWebViewController *vc = [QJWKWebViewController new];
        vc.url = kCheckStringNil(self.dataModel.linkAddress);
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (void)imageAnimationClick:(UIImageView *)image {
    
    CAKeyframeAnimation* animation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
    animation.duration = 0.6;// 动画时间
    NSMutableArray *values = [NSMutableArray array];
    // 前两个是控制view的大小的；
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.0, 1.0, 1.0)]];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.8, 0.8, 1.0)]];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.0, 1.0, 1.0)]];
    animation.values = values;
    animation.repeatCount =  MAXFLOAT;

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [image.layer addAnimation:animation forKey:nil];
    });
}


@end

