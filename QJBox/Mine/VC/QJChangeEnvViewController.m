//
//  QJChangeEnvViewController.m
//  QJBox
//
//  Created by wxy on 2022/6/6.
//

#import "QJChangeEnvViewController.h"

@interface QJChangeEnvViewController ()<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UITableView *tableView;



@end

@implementation QJChangeEnvViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"配置环境";
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.navBar.mas_bottom);
            make.left.right.equalTo(self.view);
            make.bottom.equalTo(self.view);
    }];
}

 

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight) style:UITableViewStyleGrouped];
        _tableView.dataSource = self;
        _tableView.delegate = self;
    }
    return _tableView;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 2;
    } else if (section == 1 || section == 2){
        return 1;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            cell.textLabel.text = @"测试";
        } else if (indexPath.row == 1) {
            cell.textLabel.text = @"线上";
        }
    } else if (indexPath.section == 1) {
        if (indexPath.row == 0) {
            cell.textLabel.text = @"记得退出重启app";
        }
    } else if (indexPath.section == 2) {
        cell.textLabel.text = @"输入URL链接地址";
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [[tableView cellForRowAtIndexPath:indexPath] setSelected:NO];
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            [QJEnvironmentConfigure shareInstance].currentEnvType = QJEnvironmentTypeDev;
        } else if (indexPath.row == 1) {
            [QJEnvironmentConfigure shareInstance].currentEnvType = QJEnvironmentTypeRelease;
        }
        NSDictionary *dic = [[NSUserDefaults standardUserDefaults] objectForKey:QJ_ENV_CONFIGURATION];
        NSMutableDictionary *envDic = [NSMutableDictionary dictionaryWithDictionary:dic];
        if (envDic) {
            [envDic setValue:@([QJEnvironmentConfigure shareInstance].currentEnvType) forKey:@"envType"];
        } else {
            envDic = [NSMutableDictionary dictionaryWithDictionary:@{@"envType" : @([QJEnvironmentConfigure shareInstance].currentEnvType)}];
        }
        [[NSUserDefaults standardUserDefaults] setValue:envDic forKey:QJ_ENV_CONFIGURATION];
        [[NSUserDefaults standardUserDefaults] synchronize];
        DLog(@"ENV_CONFIGURATION = %@", [[NSUserDefaults standardUserDefaults] objectForKey:QJ_ENV_CONFIGURATION]);
        [self.navigationController popViewControllerAnimated:YES];
        [self removeAllJWTAndUserID];
    } else if (indexPath.section == 2) {
        UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"输入url地址" message:nil preferredStyle:UIAlertControllerStyleAlert];
        [alert addTextFieldWithConfigurationHandler:nil];
        UIAlertAction * confirmAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            if (IsStrEmpty(alert.textFields.firstObject.text)) {
                return;
            }
            //跳转到webview
            
            
        }];
        
        UIAlertAction * cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:confirmAction];
        [alert addAction:cancelAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
}
//切换环境后需要退出上一个用户的登录信息，回到未登录状态
- (void)removeAllJWTAndUserID {
    DLog(@"清除用户信息，回到未登录状态");
}

@end
