//
//  QJProblemFeedbackVC.m
//  QJBox
//
//  Created by wxy on 2022/8/16.
//

#import "QJProblemFeedbackVC.h"
#import "QJProblemDesCell.h"
#import "QJProblemContactInformationCell.h"
#import "QJProblemImageUploadCell.h"
#import "QJCampsiteRequest.h"
#import "QJCommitProblemFeedbackReq.h"
static  int enterCount = 0;
static  int leaveCount = 0;
@interface QJProblemFeedbackVC ()<UITableViewDelegate,UITableViewDataSource>


@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) QJLoginBottomBtn *okBtn;

@property (nonatomic, strong) NSArray *images;

@property (nonatomic, copy) NSString *problemStr;

@property (nonatomic, copy) NSString *qString;

@property (nonatomic, copy) NSString *wString;

@property (nonatomic, strong) dispatch_group_t group;

@property (nonatomic, strong) NSMutableArray *imageUrlPath;

@property (nonatomic, strong) QJCommitProblemFeedbackReq *commitReq;

@end

@implementation QJProblemFeedbackVC

- (void)viewDidLoad {
    [super viewDidLoad];
     
    self.title = @"问题反馈";
    [self makeSubViews];
    
    
    
    
}


#pragma mark -------- UI
- (void)makeSubViews {
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.okBtn];
    self.view.backgroundColor = RGB(244, 244, 244);
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.navBar.mas_bottom).offset(8);
            make.left.right.equalTo(self.view);
            make.bottom.equalTo(self.okBtn.mas_top).offset(-20);
    }];
    [self.okBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view).offset(38);
            make.right.equalTo(self.view).offset(-38);
            make.height.equalTo(@48);
            make.bottom.equalTo(self.view).offset(-Bottom_iPhoneX_SPACE - 10);
    }];
}


#pragma mark ------ getter

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        [_tableView registerClass:[QJProblemDesCell class] forCellReuseIdentifier:@"QJProblemDesCell"];
        [_tableView registerClass:[QJProblemContactInformationCell class] forCellReuseIdentifier:@"QJProblemContactInformationCell"];
        [_tableView registerClass:[QJProblemImageUploadCell class] forCellReuseIdentifier:@"QJProblemImageUploadCell"];
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.backgroundColor = RGB(244, 244, 244);
        _tableView.sectionHeaderHeight = 8;
        
    }
    return _tableView;
}


- (QJLoginBottomBtn *)okBtn {
    if (!_okBtn) {
        _okBtn = [QJLoginBottomBtn new];
        _okBtn.bgBtn.enabled = NO;
        _okBtn.title = @"提交";
        WS(weakSelf)
        _okBtn.btnBlock = ^{
            DLog(@"提交");
            if (!weakSelf.group) {
                weakSelf.group = dispatch_group_create();
            }
            weakSelf.imageUrlPath = [NSMutableArray array];
            for (UIImage *image in weakSelf.images) {
                NSString *path = [NSString stringWithFormat:@"image/%@.jpg", [NSString getRandomFileName]];
                [weakSelf.imageUrlPath safeAddObject:path];
                NSData *data = UIImageJPEGRepresentation(image, 0.5);
                [weakSelf pushData:data objectKey:path];
            }
            
            [weakSelf commite];
        };
    }
    return _okBtn;
}


- (void)commite {
    WS(weakSelf)
    dispatch_group_notify(self.group, dispatch_get_main_queue(), ^{
        DLog(@"提交");
        NSMutableArray *imageUrl = [NSMutableArray array];
        for (NSString *str in self.imageUrlPath) {
            NSString *url = [NSString stringWithFormat:@"/%@",str];
            [imageUrl safeAddObject:url];
        }
        
        weakSelf.commitReq.dic = @{@"picture":imageUrl.copy,@"problemDescribe":self.problemStr?:@"",@"qqNumber":self.qString?:@"",@"wxNumber":self.wString?:@""};
        weakSelf.commitReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
            if (ResponseSuccess) {
                [weakSelf.view makeToast:@"提交成功" duration:3 position:CSToastPositionCenter image:[UIImage imageNamed:@"success_login"]];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [weakSelf.navigationController popViewControllerAnimated:YES];
                });
                
            }else{
                [weakSelf.view makeToast:ResponseMsg];
            }
           
        };
        weakSelf.commitReq.failureCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
            [weakSelf.view makeToast:ResponseFailToastMsg];
        };
        [weakSelf.commitReq start];
    });
}


- (QJCommitProblemFeedbackReq *)commitReq {
    if (!_commitReq) {
        _commitReq = [QJCommitProblemFeedbackReq new];
    }
    return _commitReq;
}

#pragma mark ------- 上传图片
//上传图片和视频
- (void)pushData:(NSData *)data objectKey:(NSString *)objectKey {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        id<OSSCredentialProvider> credential = [[OSSCustomSignerCredentialProvider alloc] initWithImplementedSigner:^NSString * _Nullable(NSString * _Nonnull contentToSign, NSError *__autoreleasing  _Nullable * _Nullable error) {
            OSSTaskCompletionSource * tcs = [OSSTaskCompletionSource taskCompletionSource];
            __block NSString *signToken= @"";
            QJCampsiteRequest *startRequest = [[QJCampsiteRequest alloc] init];
            [startRequest netWorkGetSign:contentToSign];
            [startRequest setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
                if (ResponseSuccess) {
                    NSString *data = request.responseJSONObject[@"data"][@"contentSign"];
                    signToken = data;
                    [tcs setResult:kCheckNil(data)];
                } else {
                    [tcs setError:request.error];
                }
            }];
            
            [tcs.task waitUntilFinished];
            if (tcs.task.error) {
                return nil;
            } else {
                return signToken;
            }
        }];
        OSSClient *client = [[OSSClient alloc] initWithEndpoint: @"https://oss-cn-hangzhou.aliyuncs.com" credentialProvider:credential];
        
        
        OSSPutObjectRequest * put = [OSSPutObjectRequest new];
        put.bucketName = @"sxqj";
        put.objectKey = objectKey;
        put.uploadingData = data;
        // （可选）设置上传进度。
        put.uploadProgress = ^(int64_t bytesSent, int64_t totalByteSent, int64_t totalBytesExpectedToSend) {
            // 指定当前上传长度、当前已经上传总长度、待上传的总长度。
            NSLog(@"%lld, %lld, %lld", bytesSent, totalByteSent, totalBytesExpectedToSend);
        };
        dispatch_group_enter(self.group);
        enterCount++;
        OSSTask * putTask = [client putObject:put];
        
        [putTask waitUntilFinished];
        [putTask continueWithBlock:^id(OSSTask *task) {
            if (!task.error) {
                NSLog(@"upload object success!");
                leaveCount++;
                if (leaveCount <= enterCount) {
                    if (self) {
                        dispatch_group_leave(self.group);
                    }
                    
                }
            } else {
                leaveCount++;
                if (leaveCount <= enterCount) {
                    if (self) {
                        dispatch_group_leave(self.group);
                    }
                    
                }
                NSLog(@"upload object failed, error: %@" , task.error);
            }
            return nil;
        }];
    });
}



#pragma mark ------- UITableViewDelegate, UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return 164*kWScale;
    }else if (indexPath.section == 1) {
        return 148*kWScale;
    }if (indexPath.section == 2) {
        return 176*kWScale;
    }
    return 164*kWScale;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 8*kWScale;
}

 

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    WS(weakSelf)
    if (indexPath.section == 0) {
        QJProblemDesCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QJProblemDesCell" forIndexPath:indexPath];
        cell.editEndBlock = ^(NSString * _Nonnull str) {
            weakSelf.problemStr = str;
            if (str.length > 0) {
                weakSelf.okBtn.bgBtn.enabled = YES;
            }else{
                weakSelf.okBtn.bgBtn.enabled = NO;
            }
        };
        return cell;
    }else if (indexPath.section == 1) {
        QJProblemImageUploadCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QJProblemImageUploadCell" forIndexPath:indexPath];
        cell.selectedImages = ^(NSArray * _Nonnull images) {
            weakSelf.images = images;
        };
        return cell;
    }if (indexPath.section == 2) {
        QJProblemContactInformationCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QJProblemContactInformationCell" forIndexPath:indexPath];
        cell.editFirstBlock = ^(NSString * _Nonnull str) {
            weakSelf.qString = str;
        };
        cell.editSecBlock = ^(NSString * _Nonnull str) {
            weakSelf.wString = str;
        };
        return cell;
    }
    return nil;
    
}






@end
