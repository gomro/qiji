//
//  QJAccountDetailVC.m
//  QJBox
//
//  Created by Sun on 2022/8/19.
//

#import "QJAccountDetailVC.h"
#import "QJAccountDetailViewCell.h"
#import "QJAccountDetailHeaderView.h"
#import "QJMineDatePickerView.h"
#import "BRPickerView.h"
#import "QJMineRequest.h"
#import "QJScoreRequest.h"
#import "QJAccountModel.h"
#import "QJMallShopViewController.h"

@interface QJAccountDetailVC ()<UITableViewDelegate, UITableViewDataSource>
/**余额*/
@property (nonatomic, strong) UILabel *accountSurplus;
/**总余额*/
@property (nonatomic, strong) UILabel *accountTotal;
/**兑换*/
@property (nonatomic, strong) UIButton *exchangeBt;

/**tableView*/
@property (nonatomic, strong) UITableView *tableView;

/**初始数据*/
@property (nonatomic, strong) NSMutableArray *dataArr;
/**section数组*/
@property (nonatomic, strong) NSMutableArray *sectionArr;
/**排序后字典*/
@property (nonatomic, strong) NSMutableDictionary *sortDic;

@property (nonatomic, assign) NSInteger page;

@property (nonatomic, copy) NSString *yearAndMonth;
@end

@implementation QJAccountDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"我的奇迹币";
    self.view.backgroundColor = [UIColor whiteColor];
    self.yearAndMonth = @"";
    [self taskInfoReq];
    [self setHeaderView];
    [self makeConstraintsTable];
    self.page = 1;
    [self getAccountList];
    self.tableView.mj_header = [QJRefreshHeader headerWithRefreshingBlock:^{
        self.page = 1;
        [self taskInfoReq];
        [self getAccountList];
    }];
    self.tableView.mj_footer = [QJRefreshFooter footerWithRefreshingBlock:^{
        [self getAccountList];
    }];
}

#pragma mark - 视图
- (void)setHeaderView {
    UIView *headerView = [UIView new];
    headerView.backgroundColor = UIColorHex(f5f5f5);
    [self.view addSubview:headerView];
    [headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_top).offset(NavigationBar_Bottom_Y);
        make.left.right.equalTo(self.view);
        make.height.mas_equalTo(186*kWScale);
    }];
    
    UIImageView *backImage = [UIImageView new];
    backImage.contentMode = UIViewContentModeScaleAspectFill;
    [headerView addSubview:backImage];
    backImage.image = [UIImage imageNamed:@"accountBgIcon"];
    backImage.userInteractionEnabled = YES;
    backImage.layer.cornerRadius = 8;
    backImage.clipsToBounds = YES;
    [backImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(headerView.mas_top).offset(16);
        make.left.equalTo(headerView.mas_left).offset(16);
        make.right.equalTo(headerView.mas_right).offset(-16);
        make.height.mas_equalTo(116*kWScale);
    }];
    
    [backImage addSubview:self.exchangeBt];
    [self.exchangeBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(backImage);
        make.right.equalTo(backImage.mas_right).offset(-16);
        make.width.mas_equalTo(74*kWScale);
        make.height.mas_equalTo(30*kWScale);
    }];
    
    [backImage addSubview:self.accountSurplus];
    [self.accountSurplus mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.exchangeBt);
        make.left.equalTo(backImage.mas_left).offset(16);
    }];
    
    [backImage addSubview:self.accountTotal];
    [self.accountTotal mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.exchangeBt.mas_bottom).offset(15);
        make.left.equalTo(backImage.mas_left).offset(16);
    }];
    
    UILabel *surplusLb = [UILabel new];
    surplusLb.textColor = UIColorHex(FFFFFF);
    surplusLb.font = MYFONTALL(FONT_REGULAR, 14);
    surplusLb.text = @"账户余额:";
    [backImage addSubview:surplusLb];
    [surplusLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.exchangeBt.mas_top).offset(-4);
        make.left.equalTo(backImage.mas_left).offset(16);
    }];
    
    UILabel *accountLb = [UILabel new];
    accountLb.textColor = UIColorHex(16191C);
    accountLb.font = MYFONTALL(FONT_BOLD, 16);
    accountLb.text = @"账户明细";
    [headerView addSubview:accountLb];
    [accountLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(backImage.mas_bottom).offset(22);
        make.left.equalTo(headerView.mas_left).offset(16);
    }];
    
    UIButton *monthBt = [UIButton new];
    [monthBt setTitle:@"全部" forState:UIControlStateNormal];
    [monthBt setTitleColor:UIColorHex(1F2A4D) forState:UIControlStateNormal];
    monthBt.titleLabel.font = MYFONTALL(FONT_REGULAR, 14);
    monthBt.layer.borderColor = UIColorHex(1F2A4D).CGColor;
    monthBt.layer.borderWidth = 1;
    monthBt.layer.cornerRadius = 13;
    [headerView addSubview:monthBt];
    [monthBt addTarget:self action:@selector(monthSelect) forControlEvents:UIControlEventTouchUpInside];
    [monthBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(accountLb);
        make.right.equalTo(headerView.mas_right).offset(-16);
        make.width.mas_equalTo(60*kWScale);
        make.height.mas_equalTo(25*kWScale);
    }];
    
    self.accountSurplus.text = [NSString stringWithFormat:@"%ld", self.qjcoinCurrent];
    self.accountTotal.text = [NSString stringWithFormat:@"累计获得奇迹币:%ld", self.qjcoinGot];
}

- (void)makeConstraintsTable {
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_top).offset(NavigationBar_Bottom_Y + 186*kWScale);
        make.left.right.bottom.equalTo(self.view);
    }];
}

#pragma mark - lazy loading

- (UILabel *)accountSurplus {
    if (!_accountSurplus) {
        _accountSurplus = [UILabel new];
        _accountSurplus.textColor = UIColorHex(FFFFFF);
        _accountSurplus.font = MYFONTALL(FONT_BOLD, 26);
        _accountSurplus.text = @"";
    }
    return _accountSurplus;
}

- (UILabel *)accountTotal {
    if (!_accountTotal) {
        _accountTotal = [UILabel new];
        _accountTotal.textColor = UIColorHex(FFFFFF);
        _accountTotal.font = MYFONTALL(FONT_REGULAR, 12);
        _accountTotal.text = @"累计获得奇迹币:";
    }
    return _accountTotal;
}

- (UIButton *)exchangeBt {
    if (!_exchangeBt) {
        _exchangeBt = [UIButton new];
        [_exchangeBt setTitle:@"去兑换" forState:UIControlStateNormal];
        [_exchangeBt setTitleColor:UIColorHex(FFFFFF) forState:UIControlStateNormal];
        _exchangeBt.backgroundColor = UIColorHex(FF8125);
        _exchangeBt.titleLabel.font = MYFONTALL(FONT_BOLD, 14);
        _exchangeBt.layer.cornerRadius = 16;
        _exchangeBt.layer.masksToBounds = YES;
        [_exchangeBt addTarget:self action:@selector(pushMall) forControlEvents:UIControlEventTouchUpInside];
    }
    return _exchangeBt;
}

- (UITableView *)tableView {
    if (!_tableView) {
        self.tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        self.tableView.backgroundColor = [UIColor whiteColor];
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.tableView.showsVerticalScrollIndicator = NO;
        self.tableView.showsHorizontalScrollIndicator = NO;
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        if (@available(iOS 11.0, *)) {
            self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
    }
    return _tableView;
}

- (NSMutableArray *)dataArr {
    if (!_dataArr) {
        _dataArr = [NSMutableArray arrayWithCapacity:0];
    }
    return _dataArr;
}

- (NSMutableArray *)sectionArr {
    if (!_sectionArr) {
        _sectionArr = [NSMutableArray arrayWithCapacity:0];
    }
    return _sectionArr;
}

- (NSMutableDictionary *)sortDic {
    if (!_sortDic) {
        _sortDic = [NSMutableDictionary dictionaryWithCapacity:0];
    }
    return _sortDic;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    if(!self.sectionArr.count) {
        return 1;
    }
    return self.sectionArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(!self.sectionArr.count) {
        return 0;
    }
    NSString *time = self.sectionArr[section];
    NSArray *arr = self.sortDic[time];
    return arr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellAccount = @"cellAccountId";
    QJAccountDetailViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellAccount];
    if (!cell) {
        cell = [[QJAccountDetailViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellAccount];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    NSString *time = self.sectionArr[indexPath.section];
    NSArray *arr = self.sortDic[time];
    QJAccountModel *model = arr[indexPath.row];
    [cell configureCellWithData:model];
    return cell;
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60*kWScale;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    QJAccountDetailHeaderView *headerView = [[QJAccountDetailHeaderView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 48*kWScale)];
    
    NSString *now = [NSDate br_stringFromDate:[NSDate date] dateFormat:@"yyyy-MM"];
    NSString *time = @"";
    if(self.sectionArr.count) {
        time = self.sectionArr[section];
    } else {
        if([self.yearAndMonth isEqualToString:@""]) {
            time = now;
        } else {
            time = self.yearAndMonth;
        }
    }
    [headerView configureHeaderWithTitle:time];
    MJWeakSelf
    headerView.clickHeader = ^(NSString * _Nonnull monthTitle) {
        [weakSelf moreMonth:monthTitle];
    };
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 48*kWScale;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 13*kWScale)];
    footerView.backgroundColor = [UIColor whiteColor];
    return footerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 13*kWScale;
}

#pragma mark - 月份选择
- (void)moreMonth:(NSString *)monthTitle {
    BRDatePickerView *datePickerView = [[BRDatePickerView alloc]init];
    datePickerView.pickerMode = BRDatePickerModeYM;
    datePickerView.title = @"";
    
    NSString *registerTime = LSUserDefaultsGET(kQJRegisterTime);
    datePickerView.minDate = [NSDate br_dateFromString:kCheckStringNil(registerTime) dateFormat:@"yyyy-MM-dd HH:mm:ss"];
    datePickerView.maxDate = [NSDate date];
    datePickerView.pickerStyle.doneTextColor = UIColorHex(007AFF);
    datePickerView.pickerStyle.topCornerRadius = 16;
    datePickerView.keyView = self.view; // 将组件 datePickerView 添加到 self.view 上，默认是添加到 keyWindow 上
    MJWeakSelf
    datePickerView.resultBlock = ^(NSDate *selectDate, NSString *selectValue) {
        __typeof(&*weakSelf) strongSelf = weakSelf;
        [strongSelf changeMonth:selectValue];
    };
    
    [datePickerView show];
    datePickerView.selectValue = monthTitle;
}

- (void)changeMonth:(NSString *)monthTitle {
//    if ([self.sectionArr containsObject:monthTitle]) {
//        NSInteger inSection = [self.sectionArr indexOfObject:monthTitle];
//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//            [self.tableView scrollToRow:0 inSection:inSection atScrollPosition:UITableViewScrollPositionTop animated:NO];
//        });
//    } else {
        self.yearAndMonth = kCheckStringNil(monthTitle);
        self.page = 1;
        [self getAccountList];
//    }
}

- (void)monthSelect {
    if ([self.yearAndMonth isEqualToString:@""]) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.tableView scrollToRow:0 inSection:0 atScrollPosition:UITableViewScrollPositionTop animated:NO];
        });
        return;
    }
    self.yearAndMonth = @"";
    self.page = 1;
    [self getAccountList];
}

- (void)pushMall {
    QJMallShopViewController *vc = [[QJMallShopViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - data

- (void)getAccountList {
    QJMineRequest *startRequest = [[QJMineRequest alloc] init];
    [startRequest requestAccountListPage:self.page yearAndMonth:self.yearAndMonth];
    [startRequest setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            NSArray *dataArr = request.responseJSONObject[@"data"][@"records"];
            if (self.page == 1) {
                [self.dataArr removeAllObjects];
                [self.sectionArr removeAllObjects];
                [self.sortDic removeAllObjects];
                [self.tableView.mj_footer resetNoMoreData];
                [self.tableView.mj_header endRefreshing];
            } else {
                [self.tableView.mj_footer endRefreshing];
            }
            BOOL hasNext = [request.responseJSONObject[@"data"][@"hasNext"] boolValue];
            if (hasNext) {
                self.page += 1;
            } else {
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
            }
            NSMutableArray *arr = [NSMutableArray arrayWithCapacity:0];
            for (NSDictionary *dic in dataArr) {
                QJAccountModel *model = [QJAccountModel QJAccountWithDictionary:dic];
                [arr addObject:model];
            }
            [self sortDataWithArr:[arr copy]];

        }
        QJEmptyCommonView *emptyView = [QJEmptyCommonView new];
        emptyView.string = @"没有找到相关明细记录噢~找找其他时间";
        emptyView.emptyImage = @"empty_no_search";
        [self.tableView tableViewDisplayWhenHaveNoDataWithView:emptyView ifNecessaryForRowCount:self.sectionArr.count];
    }];
    [startRequest setFailureCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        if (self.page == 1) {
            [self.tableView.mj_footer resetNoMoreData];
            [self.tableView.mj_header endRefreshing];
        } else {
            [self.tableView.mj_footer endRefreshing];
        }
        QJEmptyCommonView *emptyView = [QJEmptyCommonView new];
        emptyView.string = @"没有找到相关明细记录噢~找找其他时间";
        emptyView.emptyImage = @"empty_no_search";
        [self.tableView tableViewDisplayWhenHaveNoDataWithView:emptyView ifNecessaryForRowCount:self.sectionArr.count];
    }];
}

- (void)sortDataWithArr:(NSArray *)arr {
    for (int i = 0; i < arr.count; i++) {
        QJAccountModel *model = arr[i];
        NSString *timeSection = model.timeSection;
        if (![kCheckStringNil(model.timeSection) isEqualToString:@""]) {
            if (![self.sectionArr containsObject:timeSection]) {
                [self.sectionArr addObject:timeSection];
            }
            NSArray *arr = self.sortDic[kCheckStringNil(timeSection)];
            if (arr.count) {
                NSMutableArray *arrCopy = [NSMutableArray arrayWithArray:arr];
                [arrCopy addObject:model];
                self.sortDic[kCheckStringNil(timeSection)] = [arrCopy copy];
            } else {
                NSMutableArray *arrCopy = [NSMutableArray arrayWithCapacity:0];
                [arrCopy addObject:model];
                self.sortDic[kCheckStringNil(timeSection)] = [arrCopy copy];
            }

        }
    }
    [self.tableView reloadData];
}

- (void)taskInfoReq{
    QJScoreRequest *scoreReq = [[QJScoreRequest alloc] init];
    [scoreReq getUserScoreInfoRequest];
    WS(weakSelf)
    scoreReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            if ([EncodeDicFromDic(request.responseObject, @"data") isKindOfClass:[NSDictionary class]]) {
                NSDictionary *dataDic = EncodeDicFromDic(request.responseObject, @"data");
                NSString *qicoinStr = EncodeStringFromDicDefEmtryValue(dataDic, @"qicoin");;
                NSString *qicoinGotStr = EncodeStringFromDicDefEmtryValue(dataDic, @"qjcoinGot");
                weakSelf.accountSurplus.text = [NSString stringWithFormat:@"%@", qicoinStr];
                weakSelf.accountTotal.text = [NSString stringWithFormat:@"累计获得奇迹币:%@", qicoinGotStr];
            }
        }else{
            NSString *msg = EncodeStringFromDic(request.responseObject, @"message");
            [weakSelf.view makeToast:msg];
        }
    };
    scoreReq.failureCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        [weakSelf.view makeToast:@"数据异常 请稍后再试"];
    };
}


@end
