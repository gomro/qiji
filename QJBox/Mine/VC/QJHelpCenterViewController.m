//
//  QJHelpCenterViewController.m
//  QJBox
//
//  Created by Sun on 2022/8/15.
//

#import "QJHelpCenterViewController.h"
#import "QJHelpCenterViewCell.h"
#import "QJHelpCenterHeaderView.h"
#import "QJProblemFeedbackVC.h"
#import "QJHelpCenterSearchVC.h"
#import "QJMineRequest.h"
#import "QJHelpCenterModel.h"
#import "QJHelpCenterMoreViewController.h"
#import "QJHelpCenterDetailViewController.h"

@interface QJHelpCenterViewController ()<UITableViewDelegate, UITableViewDataSource>
/**搜索*/
@property (nonatomic, strong) UIButton *searchButton;

@property (nonatomic, strong) UITableView *tableView;
/**初始数据*/
@property (nonatomic, strong) NSMutableArray *dataArr;
@property (nonatomic, assign) NSInteger page;

@end

@implementation QJHelpCenterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.page = 1;
    [self setNavView];
    [self makeSearchButton];
    [self makeTableView];
    [self getHomeList];
    self.tableView.mj_header = [QJRefreshHeader headerWithRefreshingBlock:^{
        self.page = 1;
        [self getHomeList];
    }];
    
    QJRefreshFooter *footerView = [QJRefreshFooter footerWithRefreshingBlock:^{
        [self getHomeList];
    }];
    [footerView setTitle:@"已经到底啦~" forState:MJRefreshStateNoMoreData];
    self.tableView.mj_footer = footerView;
}

#pragma mark - 导航相关
- (void)setNavView {
    self.title = @"帮助中心";
    [self.navBar.rightButton setHidden:NO];
    [self.navBar.rightButton setImage:[UIImage imageNamed:@"serviceHelpIcon"] forState:UIControlStateNormal];
    [self.navBar.rightButton addTarget:self action:@selector(serviceView) forControlEvents:UIControlEventTouchUpInside];
}

- (void)serviceView {
    DLog(@"问题反馈");
    
    QJProblemFeedbackVC *vc = [QJProblemFeedbackVC new];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - 搜索
- (void)makeSearchButton {
    [self.view addSubview:self.searchButton];
    CGFloat topHeight = 10+ NavigationBar_Bottom_Y;

    [self.searchButton addTarget:self action:@selector(searchView:) forControlEvents:UIControlEventTouchUpInside];
    [self.searchButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_top).offset(topHeight);
        make.left.equalTo(self.view.mas_left).offset(16);
        make.right.equalTo(self.view.mas_right).offset(-16);
        make.height.mas_equalTo(34*kWScale);
    }];
    UIImageView *image = [UIImageView new];
    image.image = [UIImage imageNamed:@"iconSearch"];
    [self.searchButton addSubview:image];
    [image mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.searchButton.mas_left).offset(8);
        make.centerY.equalTo(self.searchButton);
        make.width.height.mas_equalTo(16*kWScale);
    }];
    
    UILabel *searchLb = [UILabel new];
    searchLb.textColor = UIColorHex(C8CACC);
    searchLb.text = @"搜索你想要解决的问题";
    searchLb.font = MYFONTALL(FONT_REGULAR, 14);
    [self.searchButton addSubview:searchLb];
    [searchLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(image.mas_right).offset(4);
        make.centerY.equalTo(self.searchButton);
    }];
    
}


- (void)searchView:(UIButton *)sender {
    QJHelpCenterSearchVC *search = [[QJHelpCenterSearchVC alloc] init];
    [self.navigationController pushViewController:search animated:YES];
}

- (void)makeTableView {
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.searchButton.mas_bottom).offset(8);
        make.left.right.bottom.equalTo(self.view);
    }];
}

- (UIButton *)searchButton {
    if (!_searchButton) {
        _searchButton = [UIButton new];
        _searchButton.backgroundColor = UIColorHex(F5F5F5);
        _searchButton.layer.cornerRadius = 15;
        _searchButton.layer.masksToBounds = YES;
    }
    return _searchButton;
}

- (NSMutableArray *)dataArr {
    if (!_dataArr) {
        _dataArr = [NSMutableArray arrayWithCapacity:0];
    }
    return _dataArr;
}

- (UITableView *)tableView {
    if (!_tableView) {
        self.tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        self.tableView.backgroundColor = UIColorHex(F5F5F5);
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.tableView.showsVerticalScrollIndicator = NO;
        self.tableView.showsHorizontalScrollIndicator = NO;
        self.tableView.delegate = self;
        self.tableView.dataSource = self;        
        if (@available(iOS 11.0, *)) {
            self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
    }
    return _tableView;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.dataArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    QJHelpCenterModel *model = self.dataArr[section];
    return model.helpCenters.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellHelpCenter = @"cellHelpCenterId";
    QJHelpCenterViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellHelpCenter];
    if (!cell) {
        cell = [[QJHelpCenterViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellHelpCenter];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    QJHelpCenterModel *model = self.dataArr[indexPath.section];
    QJHelpCenterDetailModel *detailModel = model.helpCenters[indexPath.row];
    [cell configureCellWithData:detailModel];
    MJWeakSelf
    cell.clickCell = ^(QJHelpCenterDetailModel * _Nonnull model) {
        [weakSelf pushDetailView:model];
    };
    return cell;
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 38*kWScale;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    QJHelpCenterHeaderView *headerView = [[QJHelpCenterHeaderView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 52*kWScale)];
    QJHelpCenterModel *model = self.dataArr[section];
    [headerView configureHeaderWithData:model];
    MJWeakSelf
    headerView.clickHeader = ^(NSString * _Nonnull typeId, NSString * _Nonnull typeName) {
        [weakSelf pushMoreView:typeName typeId:typeId];
    };
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 52*kWScale;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    QJHelpCenterFooterView *footerView = [[QJHelpCenterFooterView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 16*kWScale)];

    return footerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 16*kWScale;
}

#pragma mark - 跳转

- (void)pushMoreView:(NSString *)typeName typeId:(NSString *)typeId {
    QJHelpCenterMoreViewController *moreVC = [[QJHelpCenterMoreViewController alloc] init];
    moreVC.typeId = typeId;
    moreVC.typeName = typeName;
    [self.navigationController pushViewController:moreVC animated:YES];
}

- (void)pushDetailView:(QJHelpCenterDetailModel *)model {
    QJHelpCenterDetailViewController *detailVC = [[QJHelpCenterDetailViewController alloc] init];
    detailVC.idStr = model.detailId;
    [self.navigationController pushViewController:detailVC animated:YES];
}

#pragma mark - data
- (void)getHomeList {
    QJMineRequest *startRequest = [[QJMineRequest alloc] init];
    [startRequest requestHelpCenterHome];
    [startRequest setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            if (self.page == 1) {
                [self.dataArr removeAllObjects];
                [self.tableView.mj_footer resetNoMoreData];
                [self.tableView.mj_header endRefreshing];
            } else {
                [self.tableView.mj_footer endRefreshing];
            }
            BOOL hasNext = NO;
            if (hasNext) {
                self.page += 1;
            } else {
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
            }
            
            NSArray *dataArr = request.responseJSONObject[@"data"];
            for (NSDictionary *dic in dataArr) {
                QJHelpCenterModel *model = [QJHelpCenterModel modelWithDictionary:dic];
                [self.dataArr addObject:model];
            }
            [self.tableView reloadData];

        }

        QJEmptyCommonView *emptyView = [QJEmptyCommonView new];
        emptyView.string = @"还没有内容哦~";
        [self.tableView tableViewDisplayWhenHaveNoDataWithView:emptyView ifNecessaryForRowCount:self.dataArr.count];
    }];
    [startRequest setFailureCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        if (self.page == 1) {
            [self.tableView.mj_footer resetNoMoreData];
            [self.tableView.mj_header endRefreshing];
        } else {
            [self.tableView.mj_footer endRefreshing];
        }
        QJEmptyCommonView *emptyView = [QJEmptyCommonView new];
        emptyView.string = @"还没有内容哦~";
        [self.tableView tableViewDisplayWhenHaveNoDataWithView:emptyView ifNecessaryForRowCount:self.dataArr.count];
    }];
}
@end
