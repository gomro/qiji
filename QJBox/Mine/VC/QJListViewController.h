//
//  QJListViewController.h
//  QJBox
//
//  Created by Sun on 2022/7/1.
//

#import <UIKit/UIKit.h>
#import "JXCategoryListContainerView.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJListViewController : UIViewController<JXCategoryListContentViewDelegate>

@end

NS_ASSUME_NONNULL_END
