//
//  QJMyCampViewController.h
//  QJBox
//
//  Created by Sun on 2022/6/17.
//

#import "QJBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN




@interface QJMySpaceViewController : QJBaseViewController


@property (nonatomic, copy) NSString *userId;//用户id（如果存在表示进入到别人空间，否则是进入自己的空间）
/* 用户认证状态 */
@property (nonatomic, assign) BOOL verifiedState;

@property (nonatomic, assign) BOOL isSelectedCollocted;//是否选中收藏

@property (nonatomic, assign) BOOL isSelectedCreat;//是否选中创作

@end

NS_ASSUME_NONNULL_END
