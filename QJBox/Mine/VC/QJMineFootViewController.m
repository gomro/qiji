//
//  QJMineFootViewController.m
//  QJBox
//
//  Created by macm on 2022/7/21.
//

#import "QJMineFootViewController.h"
#import "QJMineFootTableViewCell.h"
#import "QJMineRequest.h"
#import "AppDelegate.h"
#import "QJConsultDetailViewController.h"

@interface QJMineFootViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, assign) BOOL isDelete; //是否是删除状态
@property (nonatomic, strong) UIView *bottomView; // 底部删除按钮view
@property (nonatomic, strong) UIButton *selDelButton;
@property (nonatomic, strong) UIButton *allDelButton;
@property (strong, nonatomic) MASConstraint *bottomConstraint;
@property (nonatomic, strong) NSMutableArray *timeArray; // 存放时间，作为section标题，区分有多少section
@property (nonatomic, strong) NSMutableDictionary *dataDic; // 存放数据
@property (nonatomic, assign) NSInteger page;
@property (nonatomic, strong) NSMutableArray *idsArray; // 存放数据id
@end

@implementation QJMineFootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"浏览足迹";
    self.page = 1;

    self.idsArray = [NSMutableArray array];
    self.dataDic = [NSMutableDictionary dictionary];
    self.timeArray = [NSMutableArray array];
    
    [self createView];
    [self sendRequest];
}

- (void)loadData {
    _page = 1;
    [self.timeArray removeAllObjects];
    [self.idsArray removeAllObjects];
    [self.dataDic removeAllObjects];
    [self sendRequest];
}

- (void)loadMoreData {
    _page++;
    [self sendRequest];
}

- (void)sendRequest {

    @weakify(self);
    QJMineRequest *request = [[QJMineRequest alloc] init];
    [request requestUserZoneRecentViewPage:_page completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        @strongify(self);
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        
        if (isSuccess) {
            NSArray *array = [NSArray modelArrayWithClass:[QJMineFootModel class] json:EncodeArrayFromDic(request.responseJSONObject[@"data"], @"records")];
            
            for (QJMineFootModel *model in array) {
                if ([self.timeArray containsObject:model.timeShow]) {
                    [self.dataDic[model.timeShow] addObject:model];
                } else {
                    [self.timeArray addObject:model.timeShow];
                    NSMutableArray *arr = [NSMutableArray arrayWithArray:@[model]];
                    [self.dataDic setObject:arr forKey:model.timeShow];
                }
        
            }
                
            if (self.timeArray.count == 0) {
                [self reloadRightButton];
            }

            [self.tableView reloadData];

            if ([request.responseJSONObject[@"data"][@"hasNext"] integerValue] == 0 || array.count == 0) {
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
            }
        }
        
        [self emptyTableView];

    }];

}

- (void)emptyTableView {
    @weakify(self);
    QJEmptyCommonView *emptyView = [QJEmptyCommonView new];
    emptyView.string = @"您还没有浏览记录哦~";
    emptyView.buttonString = @"去逛逛~";

    emptyView.emptyClick = ^{
        @strongify(self);
        AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        QJAppTabBarController *tab = (QJAppTabBarController *)delegate.window.rootViewController;
        tab.selectedIndex = 3;
        [self.navigationController popViewControllerAnimated:NO];
    };
    [self.tableView tableViewDisplayWhenHaveNoDataWithView:emptyView ifNecessaryForRowCount:self.timeArray.count];

}

- (void)createView {
    [self.navBar.rightButton setHidden:NO];
    [self.navBar.rightButton setTitle:@"清除" forState:UIControlStateNormal];
    [self.navBar.rightButton setTitleColor:UIColorFromRGB(0x007aff) forState:UIControlStateNormal];
    self.navBar.rightButton.titleLabel.font = FontRegular(14);
    self.navBar.rightButton.width = 40*kWScale;
    self.navBar.rightButton.hx_x = kScreenWidth-9-40*kWScale;
    [self.navBar.rightButton addTarget:self action:@selector(rightAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.bottomView];
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        self.bottomConstraint = make.bottom.mas_equalTo(100);
        make.height.mas_equalTo(Bottom_iPhoneX_SPACE+60*kWScale);
    }];
    
    [self.bottomView addSubview:self.selDelButton];
    [self.selDelButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(16);
        make.height.mas_equalTo(34*kWScale);
        make.width.mas_equalTo((kScreenWidth-60)/2);
    }];
    
    [self.bottomView addSubview:self.allDelButton];
    [self.allDelButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-15);
        make.top.mas_equalTo(16);
        make.height.mas_equalTo(34*kWScale);
        make.width.mas_equalTo((kScreenWidth-60)/2);
    }];
    
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.navBar.mas_bottom);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.bottomView.mas_top);
    }];
}

- (void)rightAction {
    self.isDelete = !self.isDelete;
    [self.bottomConstraint uninstall];
    if (self.isDelete) {
        [self.navBar.rightButton setTitle:@"取消" forState:UIControlStateNormal];
        [self.navBar.rightButton setTitleColor:UIColorFromRGB(0x919599) forState:UIControlStateNormal];
        [self.bottomView mas_updateConstraints:^(MASConstraintMaker *make) {
            self.bottomConstraint = make.bottom.mas_equalTo(0);
        }];
    } else {
        [self.navBar.rightButton setTitle:@"清除" forState:UIControlStateNormal];
        [self.navBar.rightButton setTitleColor:UIColorFromRGB(0x007aff) forState:UIControlStateNormal];
        [self.bottomView mas_updateConstraints:^(MASConstraintMaker *make) {
            self.bottomConstraint = make.bottom.mas_equalTo(100);
        }];
    }
    [self.tableView reloadData];
}

// 删除所有数据后刷新清除按钮
- (void)reloadRightButton {
    [self.navBar.rightButton setEnabled:NO];
    [self.navBar.rightButton setTitle:@"清除" forState:UIControlStateNormal];
    [self.navBar.rightButton setTitleColor:UIColorFromRGB(0x919599) forState:UIControlStateNormal];
    [self.bottomView mas_updateConstraints:^(MASConstraintMaker *make) {
        self.bottomConstraint = make.bottom.mas_equalTo(100);
    }];
}

// 删除选中记录
- (void)selDelAction {

    if (self.idsArray.count == 0) {
        return;
    }
    NSString *ids = [self.idsArray componentsJoinedByString:@","];
    [QJAppTool showHUDLoading];
    @weakify(self);
    QJMineRequest *request = [[QJMineRequest alloc] init];
    [request requestUserZoneRecentView_del:ids completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        @strongify(self);
        [QJAppTool hideHUDLoading];
        if (isSuccess) {
            [[QJAppTool getCurrentViewController].view makeToast:@"删除成功" duration:2 position:CSToastPositionCenter image:[UIImage imageNamed:@"success_login"]];

            // 对数据进行删除操作
            for (NSString *time in self.timeArray) {
                NSMutableArray *modelArray = [NSMutableArray array];
                NSArray *array = self.dataDic[time];
                for (QJMineFootModel *model in array) {
                    if (![self.idsArray containsObject:model.idStr]) {
                        [modelArray addObject:model];
                    }
                }
                if (modelArray.count == 0) {
                    [self.timeArray removeObject:time];
                    [self.dataDic removeObjectForKey:time];
                } else {
                    [self.dataDic setObject:modelArray forKey:time];
                }
                
            }

            [self.idsArray removeAllObjects];
            [self.tableView reloadData];
            [self emptyTableView];

            // 删除完数据，更新清除按钮状态
            if (self.timeArray.count == 0) {
                [self reloadRightButton];
            } else {
                [self rightAction];
            }

        }

    }];
    
}

// 删除所有记录
- (void)allDelAction {
    if (self.timeArray.count == 0) {
        return;
    }
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:@"是否清空全部浏览足迹？" preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:nil];
    UIAlertAction *nextAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self sendRequestDelAll];
    }];
    
    [alert addAction:cancelAction];
    [alert addAction:nextAction];

    [self presentViewController:alert animated:YES completion:nil];
    
    
}

- (void)sendRequestDelAll {
    [QJAppTool showHUDLoading];
    @weakify(self);
    QJMineRequest *request = [[QJMineRequest alloc] init];
    [request requestUserZoneRecentView_clear_completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        @strongify(self);
        [QJAppTool hideHUDLoading];
        if (isSuccess) {
            [[QJAppTool getCurrentViewController].view makeToast:@"删除成功" duration:2 position:CSToastPositionCenter image:[UIImage imageNamed:@"success_login"]];

            [self.idsArray removeAllObjects];
            [self.timeArray removeAllObjects];
            [self.dataDic removeAllObjects];

            [self.tableView reloadData];
            [self reloadRightButton];
            [self emptyTableView];

        }

    }];
}

// 刷新选择
- (void)reloadSelectTableViewSection:(NSInteger)section row:(NSInteger)row {
    
    // 更改数据源
    NSString *keyString = self.timeArray[section];
    NSMutableArray *array = self.dataDic[keyString];
    QJMineFootModel *model = array[row];
    model.isDelete = !model.isDelete;
    array[row] = model;
    [self.dataDic setObject:array forKey:keyString];
    
    // 加入id到删除数组
    if ([self.idsArray containsObject:model.idStr]) {
        [self.idsArray removeObject:model.idStr];
    } else {
        [self.idsArray addObject:model.idStr];
    }
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:section];
    //为了避免重新加载时出现不需要的动画（又名“闪烁”)
    BOOL animationsEnabled = [UIView areAnimationsEnabled];
    [UIView setAnimationsEnabled:NO];
    [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath,nil] withRowAnimation:UITableViewRowAnimationNone];
    [UIView setAnimationsEnabled:animationsEnabled];
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.timeArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSString *keystring = self.timeArray[section];
    NSArray *array = self.dataDic[keystring];

    return array.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    QJMineFootTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QJMineFootTableViewCell"];
    NSString *keystring = self.timeArray[indexPath.section];
    NSArray *array = self.dataDic[keystring];
    if (array.count == indexPath.row+1) {
        cell.isLast = YES;
    } else {
        cell.isLast = NO;
    }

    cell.model = array[indexPath.row];
    cell.isDelete = self.isDelete;
    
    @weakify(self);
    cell.QJMineFootTableViewCellClick = ^{
        @strongify(self);
        [self reloadSelectTableViewSection:indexPath.section row:indexPath.row];
    };
    
    return cell;
    
}

#pragma mark - UITableViewDelegate

//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    return 50*kWScale;
//}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 50)];
    UILabel *timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, kScreenWidth-30, 50)];
    NSString *time = self.timeArray[section];
    timeLabel.text = time;
    timeLabel.textColor = UIColorFromRGB(0x919599);
    timeLabel.font = FontRegular(10);
    [headerView addSubview:timeLabel];
    return headerView;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [UIView new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 50;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *keystring = self.timeArray[indexPath.section];
    NSArray *array = self.dataDic[keystring];
    QJMineFootModel *model = array[indexPath.row];
    QJConsultDetailViewController *detailView = [[QJConsultDetailViewController alloc] init];
    detailView.idStr = model.sourceId;
    detailView.isNews = model.type.integerValue == 1 ? YES : NO;
    [[QJAppTool getCurrentViewController].navigationController pushViewController:detailView animated:YES];
}

- (UITableView *)tableView {
    if (!_tableView) {
        self.tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        self.tableView.backgroundColor = UIColorHex(f8f8f8);
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.tableView.showsVerticalScrollIndicator = NO;
        self.tableView.showsHorizontalScrollIndicator = NO;
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        self.tableView.estimatedRowHeight = 0.01;
        if (@available(iOS 11.0, *)) {
            self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }

        [self.tableView registerClass:[QJMineFootTableViewCell class] forCellReuseIdentifier:@"QJMineFootTableViewCell"];
        self.tableView.mj_header = [QJRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadData)];
        self.tableView.mj_footer = [QJRefreshFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];

    }
    return _tableView;
}

- (UIView *)bottomView {
    if (!_bottomView) {
        _bottomView = [[UIView alloc] init];
        _bottomView.backgroundColor = [UIColor whiteColor];
    }
    return _bottomView;
}

- (UIButton *)selDelButton {
    if (!_selDelButton) {
        _selDelButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_selDelButton setBackgroundColor:[UIColor whiteColor]];
        _selDelButton.layer.cornerRadius = 17;
        _selDelButton.layer.borderWidth = 1;
        _selDelButton.layer.borderColor = UIColorFromRGB(0x000000).CGColor;
        [_selDelButton setTitleColor:UIColorFromRGB(0x000000) forState:UIControlStateNormal];
        _selDelButton.titleLabel.font = FontRegular(14);
        [_selDelButton setTitle:@"删除记录" forState:UIControlStateNormal];
        [_selDelButton addTarget:self action:@selector(selDelAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _selDelButton;
}

- (UIButton *)allDelButton {
    if (!_allDelButton) {
        _allDelButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_allDelButton setBackgroundColor:UIColorFromRGB(0xff9500)];
        _allDelButton.layer.cornerRadius = 17;
        [_allDelButton setTitleColor:UIColorFromRGB(0xfefefe) forState:UIControlStateNormal];
        _allDelButton.titleLabel.font = FontRegular(14);
        [_allDelButton setTitle:@"一键清空" forState:UIControlStateNormal];
        [_allDelButton addTarget:self action:@selector(allDelAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _allDelButton;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
