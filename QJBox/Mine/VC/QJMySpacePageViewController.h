//
//  QJMySpacePageViewController.h
//  QJBox
//
//  Created by Sun on 2022/7/1.
//

#import "QJContentBaseViewController.h"
@protocol QJMySpacePageViewControllerDelegate <NSObject>

- (void)pageScrollViewWillBeginScroll;
- (void)pageScrollViewDidEndedScroll;

@end

@interface QJMySpacePageViewController : QJContentBaseViewController
@property (nonatomic, weak) id<QJMySpacePageViewControllerDelegate> scrollDelegate;
- (void)configTitles:(NSArray *)titles childVCs:(NSArray *)childVCs;

- (void)selectItemView:(NSInteger)index;

@property (nonatomic, assign) NSInteger selecteIndex;//选中下标

@property (nonatomic, strong) NSArray *childVCs;
@end

