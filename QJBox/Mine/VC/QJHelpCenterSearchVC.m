//
//  QJHelpCenterSearchVC.m
//  QJBox
//
//  Created by Sun on 2022/8/19.
//

#import "QJHelpCenterSearchVC.h"
#import "QJHelpCenterViewCell.h"
#import "QJMineRequest.h"
#import "QJHelpCenterModel.h"
#import "QJHelpCenterDetailViewController.h"

@interface QJHelpCenterSearchVC ()<UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) UITextField *searchTf;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *dataSource;
@property (nonatomic, assign) BOOL isSearch;
@end

@implementation QJHelpCenterSearchVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    if (self.isAI) {
        [self getSearchCampsiteList:self.searchStr != nil ? self.searchStr : @""];
    } else {
        [self setNavBarHidden:YES];
        [self subSetUI];
        [self.searchTf becomeFirstResponder];
    }
    
    
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.mas_equalTo(0);
        make.top.equalTo(self.view.mas_top).offset(54 + QJStatusBarHeight);
    }];
}

- (void)subSetUI {
    UIButton *cancleBt = [UIButton new];
    [cancleBt setTitle:@"取消" forState:UIControlStateNormal];
    [cancleBt setTitleColor:UIColorHex(090909) forState:UIControlStateNormal];
    cancleBt.titleLabel.font = MYFONTALL(FONT_REGULAR, 14);
    [cancleBt addTarget:self action:@selector(popView:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:cancleBt];
    
    UIView *backView = [[UIView alloc] init];
    backView.backgroundColor = UIColorHex(F5F5F5);
    backView.layer.cornerRadius = 15;
    backView.layer.masksToBounds = YES;
    [self.view addSubview:backView];
    
    CGFloat topHeight = 10 + QJStatusBarHeight;
    [backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_top).offset(topHeight);
        make.left.equalTo(self.view.mas_left).offset(16);
        make.right.equalTo(cancleBt.mas_left).offset(-16);
        make.height.mas_equalTo(34*kWScale);
    }];
    
    [cancleBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.view.mas_right).offset(-16);
        make.centerY.equalTo(backView);
        make.width.mas_equalTo(30*kWScale);
    }];
    
    UIImageView *image = [UIImageView new];
    image.image = [UIImage imageNamed:@"iconSearch"];
    [backView addSubview:image];
    [image mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(backView.mas_left).offset(8);
        make.centerY.equalTo(backView);
        make.width.height.mas_equalTo(16);
    }];
    
    self.searchTf = [UITextField new];
    self.searchTf.textColor = UIColorHex(474849);
    NSMutableAttributedString *placeholderString = [[NSMutableAttributedString alloc] initWithString:@"搜索你想要解决的问题" attributes:@{NSForegroundColorAttributeName : UIColorHex(C8CACC)}];
    self.searchTf.attributedPlaceholder = placeholderString;
    self.searchTf.font = MYFONTALL(FONT_REGULAR, 14);
    self.searchTf.delegate = self;
    self.searchTf.returnKeyType = UIReturnKeySearch;
    self.searchTf.clearButtonMode = UITextFieldViewModeWhileEditing;
    [backView addSubview:self.searchTf];
    [self.searchTf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(image.mas_right).offset(4);
        make.right.equalTo(backView.mas_right).offset(-4);
        make.centerY.equalTo(backView);
    }];
}

- (void)popView:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - Lazy Loading
- (UITableView *)tableView {
    if (!_tableView) {
        self.tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        self.tableView.backgroundColor = [UIColor whiteColor];
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.tableView.showsVerticalScrollIndicator = NO;
        self.tableView.showsHorizontalScrollIndicator = NO;
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        if (@available(iOS 11.0, *)) {
            self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
    }
    return _tableView;
}

- (NSMutableArray *)dataSource {
    if (!_dataSource) {
        _dataSource = [NSMutableArray arrayWithCapacity:0];
    }
    return _dataSource;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.isSearch) {
        QJEmptyCommonView *emptyView = [QJEmptyCommonView new];
        emptyView.string = @"暂无搜索结果";
        [self.tableView tableViewDisplayWhenHaveNoDataWithView:emptyView ifNecessaryForRowCount:self.dataSource.count];
    }

    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellHelpCenter = @"cellSearchHelpCenterId";
    QJHelpCenterSearchViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellHelpCenter];
    if (!cell) {
        cell = [[QJHelpCenterSearchViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellHelpCenter];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    QJHelpCenterDetailModel *detailModel = self.dataSource[indexPath.row];
    [cell configureCellWithData:detailModel isDetail:NO];
    MJWeakSelf
    cell.clickCell = ^(QJHelpCenterDetailModel * _Nonnull model) {
        [weakSelf pushDetailView:model];
    };
    return cell;
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 42;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 7)];

    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 7;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 0.01)];

    return footerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

- (void)getSearchCampsiteList:(NSString *)keyWord {
    [self.searchTf resignFirstResponder];
    self.isSearch = YES;
    QJMineRequest *startRequest = [[QJMineRequest alloc] init];
    [startRequest requestHelpCenterSearchQuestion:keyWord];
    [startRequest setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            NSArray *dataArr = request.responseJSONObject[@"data"];
            [self.dataSource removeAllObjects];
            for (NSDictionary *dic in dataArr) {
                QJHelpCenterDetailModel *model = [QJHelpCenterDetailModel modelWithDictionary:dic];
                [self.dataSource addObject:model];
            }
            [self.tableView reloadData];

        }
    }];
    [startRequest setFailureCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {

    }];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    NSString *keyWord = textField.text;
    [self getSearchCampsiteList:keyWord];
    return YES;
}

- (void)pushDetailView:(QJHelpCenterDetailModel *)model {
    QJHelpCenterDetailViewController *detailVC = [[QJHelpCenterDetailViewController alloc] init];
    detailVC.idStr = model.detailId;
    [self.navigationController pushViewController:detailVC animated:YES];
}


@end
