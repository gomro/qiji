//
//  QJContentBaseViewController.h
//  QJBox
//
//  Created by Sun on 2022/7/1.
//

#import <UIKit/UIKit.h>
#import "JXCategoryView.h"
#import "JXCategoryListContainerView.h"
#import "QJBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJContentBaseViewController : QJBaseViewController<JXCategoryListContainerViewDelegate>

@property (nonatomic, strong) NSArray *titles;

@property (nonatomic, strong) JXCategoryBaseView *categoryView;

@property (nonatomic, strong) JXCategoryListContainerView *listContainerView;

/**背景图**/
@property (nonatomic, strong) UIImageView *backImage;

@property (nonatomic, assign) BOOL isNeedIndicatorPositionChangeItem;

- (JXCategoryBaseView *)preferredCategoryView;

- (CGFloat)preferredCategoryViewHeight;
@end

NS_ASSUME_NONNULL_END
