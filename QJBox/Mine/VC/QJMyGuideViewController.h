//
//  QJMyGuideViewController.h
//  QJBox
//
//  Created by Sun on 2022/6/17.
//

#import "QJListViewController.h"
#import "GKPageScrollView.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJMyGuideViewController : QJListViewController<GKPageListViewDelegate>

/* 0点赞，1收藏，2创作，3动态 */
@property (nonatomic, copy) NSString *sortType;
@property (nonatomic, copy) NSString *userId;//用户id（如果存在表示进入到别人空间，否则是进入自己的空间）
//是否选中创作唤起弹框
@property (nonatomic, assign) BOOL isSelectedCreat;

//刷新列表
- (void)refreshTableview;
@end

NS_ASSUME_NONNULL_END
