//
//  QJMineStarAlertViewController.m
//  QJBox
//
//  Created by wxy on 2022/7/19.
//

#import "QJMineStarAlertViewController.h"
#import "QJMineUserBeStarCollectionMsgRequest.h"
#import "QJMineUserBeCollectedMsgRequest.h"
@interface QJMineStarAlertViewController ()

/** 暗色背景 */
@property (nonatomic, strong) UIView *bgView;
/** 控件View */
@property (nonatomic, strong) UIView *contentView;

@property (nonatomic, strong) UIImageView *bgImageView;

@property (nonatomic, strong) UIImageView *topImageView;

@property (nonatomic, strong) UILabel *ziLabel;//自2022年07月01日

@property (nonatomic, strong) UILabel *jiaLabel;//加入奇迹盒子20个日子以来

@property (nonatomic, strong) UILabel *ninCLabel;//您共发布创作.

@property (nonatomic, strong) UILabel *ninDLabel;//您共发布动态

@property (nonatomic, strong) UILabel *gongLabel;//共被收藏了

@property (nonatomic, strong) UILabel *qiLabel;//期待你的更多精彩～

@property (nonatomic, strong) UIButton *okBtn;//确定按钮


@property (nonatomic, strong) QJMineUserBeCollectedMsgRequest *beCollectedMsgReq;//查询被收藏信息

@property (nonatomic, strong) QJMineUserBeStarCollectionMsgRequest *beLikeMsgReq;//查询被点赞信息

@property (nonatomic, assign) CGFloat bgHeight;
@end

@implementation QJMineStarAlertViewController


- (instancetype)init {
    if (self = [super init]) {
        self.modalPresentationStyle = UIModalPresentationCustom;
    }
    return self;
}

 

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navBar.hidden = YES;
    if(self.isAnother){
        self.bgHeight = Get375Width(333);
    }else{
        self.bgHeight = Get375Width(405);
    }
    [self setupUI];
    [self startAnimation];
    [self makeData];
    
}

- (void)makeData {
    if (self.style == QJMineStarAlertViewStyle_collection) {
        self.beCollectedMsgReq.userID = self.uid;
        WS(weakSelf)
        self.beCollectedMsgReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
            if (ResponseSuccess) {
                NSDictionary *data = EncodeDicFromDic(request.responseJSONObject, @"data");
                NSArray *activities = [NSArray modelArrayWithClass:[QJMineStarAlertModel class] json:EncodeArrayFromDic(data, @"activities")];
                [weakSelf configerData:activities dic:data];
            }else{
                [weakSelf.view makeToast:ResponseMsg];
            }
            
        };
        self.beCollectedMsgReq.failureCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
            [weakSelf.view makeToast:ResponseFailToastMsg];
            
        };
        [self.beCollectedMsgReq start];
        
    }else if (self.style == QJMineStarAlertViewStyle_star) {
        self.beLikeMsgReq.userID = self.uid;
        WS(weakSelf)
        self.beLikeMsgReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
            if (ResponseSuccess) {
                NSDictionary *data = EncodeDicFromDic(request.responseJSONObject, @"data");
                NSArray *activities = [NSArray modelArrayWithClass:[QJMineStarAlertModel class] json:EncodeArrayFromDic(data, @"activities")];
                [weakSelf configerData:activities dic:data];
            }else{
                [weakSelf.view makeToast:ResponseMsg];
            }
            
        };
        self.beLikeMsgReq.failureCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
            [weakSelf.view makeToast:ResponseFailToastMsg];
            
        };
        [self.beLikeMsgReq start];
        
    }
    
    

    
}


- (void)configerData:(NSArray *)data dic:(NSDictionary *)dic{
   
    NSString *thrid = EncodeStringFromDic(dic, @"count");
    self.ziLabel.text = [NSString stringWithFormat:@"自%@",EncodeStringFromDic(dic, @"joinTime")];
    self.jiaLabel.text = [NSString stringWithFormat:@"加入奇迹盒子%@个日子以来",EncodeStringFromDic(dic, @"joinDays")];
    int i = 0;
    NSString *touStr = @"您共发布";
    if(self.isAnother){
        touStr = @"共发布";
    }else{
        touStr = @"您共发布";
    }
    for (QJMineStarAlertModel *model in data) {
        NSMutableAttributedString *ninCStr = [[NSMutableAttributedString alloc]initWithString:touStr attributes:@{NSFontAttributeName : FontRegular(14),NSForegroundColorAttributeName:kColorWithHexString(@"#16191C")}];
        NSMutableAttributedString *ninCStr1 = [[NSMutableAttributedString alloc]initWithString:model.name?:@"" attributes:@{NSFontAttributeName : FontRegular(14),NSForegroundColorAttributeName:kColorWithHexString(@"#FF9500")}];
        NSMutableAttributedString *ninCStr2 = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@" %@ ",model.count?:@""] attributes:@{NSFontAttributeName : FontSemibold(24),NSForegroundColorAttributeName:kColorWithHexString(@"#16191C")}];
        NSMutableAttributedString *ninCStr3 = [[NSMutableAttributedString alloc]initWithString:@"篇" attributes:@{NSFontAttributeName : FontRegular(14),NSForegroundColorAttributeName:kColorWithHexString(@"#16191C")}];
        
        [ninCStr appendAttributedString:ninCStr1];
        [ninCStr appendAttributedString:ninCStr2];
        [ninCStr appendAttributedString:ninCStr3];
        if (i == 0) {
            self.ninCLabel.attributedText = ninCStr.copy;
        }
        
        if (i == 1) {
            self.ninDLabel.attributedText = ninCStr.copy;
        }
        i++;
        
    }

    NSString *styleStr = @"共被收藏了";
    if (self.style == QJMineStarAlertViewStyle_star) {
        styleStr = @"共被赞了";
    }
    NSMutableAttributedString *gongStr = [[NSMutableAttributedString alloc]initWithString:styleStr attributes:@{NSFontAttributeName : FontRegular(14),NSForegroundColorAttributeName:kColorWithHexString(@"#16191C")}];
    
    NSMutableAttributedString *gongStr1 = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@" %@ ",thrid] attributes:@{NSFontAttributeName : FontSemibold(28),NSForegroundColorAttributeName:kColorWithHexString(@"#FF9500")}];
    NSMutableAttributedString *gongStr2 = [[NSMutableAttributedString alloc]initWithString:@"次" attributes:@{NSFontAttributeName : FontRegular(14),NSForegroundColorAttributeName:kColorWithHexString(@"#16191C")}];
    
    [gongStr appendAttributedString:gongStr1];
    [gongStr appendAttributedString:gongStr2];
    self.gongLabel.attributedText = gongStr.copy;
    
  
    
}

- (void)startAnimation {
    self.bgView.frame = [UIScreen mainScreen].bounds;
    
    CGFloat leftOffset = (kScreenWidth - Get375Width(300))/2.0;
    self.contentView.frame = CGRectMake(leftOffset, ([UIScreen mainScreen].bounds.size.height - self.bgHeight)/2.0 , Get375Width(300), self.bgHeight);
    
    if (self.style == QJMineStarAlertViewStyle_star) {
        
        self.topImageView.image = [UIImage imageNamed:@"qj_star_alert"];
        self.bgImageView.hidden = NO;
    }else{
        self.bgImageView.hidden = YES;
        self.topImageView.image = [UIImage imageNamed:@"qj_souCang_alert"];
    }

}

-(void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    [_okBtn graduateLeftColor:[UIColor colorWithHexString:@"#FDBC69"] ToColor:[UIColor colorWithHexString:@"#E46B2F"] startPoint:CGPointMake(0, 0) endPoint:CGPointMake(0, 1)];
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if(self.isAnother){
        self.bgHeight = Get375Width(333);
    }else{
        self.bgHeight = Get375Width(405);
    }
    CGFloat leftOffset = (kScreenWidth - Get375Width(300))/2.0;
    [UIView animateWithDuration:0.4 delay:0 usingSpringWithDamping:0.95 initialSpringVelocity:0.05 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        
        self.bgView.alpha = 1.0f;
        self.contentView.hidden = NO;
        self.contentView.frame = CGRectMake(leftOffset, ([UIScreen mainScreen].bounds.size.height - self.bgHeight)/2.0 , Get375Width(300), self.bgHeight);
        
    } completion:^(BOOL finished) {
    }];
}

- (void)setupUI
{
    self.view.backgroundColor = [UIColor clearColor];
    
    UIView *bgView = [[UIView alloc] init];
    bgView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6];
    [self.view addSubview:bgView];
    self.bgView = bgView;
 
    self.bgView.alpha = 0.0f;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickBgView)];
    [bgView addGestureRecognizer:tap];
    
    
    UIView *cView = [[UIView alloc] init];
    cView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:cView];
    self.contentView = cView;
    self.contentView.layer.cornerRadius = 8;
    self.contentView.layer.masksToBounds = YES;
    
    [self.contentView addSubview:self.bgImageView];
    [self.contentView addSubview:self.topImageView];
    [self.contentView addSubview:self.ninCLabel];
    [self.contentView addSubview:self.ninDLabel];
    [self.contentView addSubview:self.gongLabel];
   
    [self.contentView addSubview:self.okBtn];
    if(self.isAnother){
        [self.bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(Get375Width(21));
            make.right.equalTo(self.contentView).offset(-(Get375Width(21)));
            make.top.equalTo(self.contentView).offset(-(Get375Width(14)));
            make.height.equalTo(@(Get375Width(148)));
        }];
        
        [self.topImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.contentView);
            make.top.equalTo(self.bgImageView).offset(Get375Width(27));
            make.width.height.equalTo(@(Get375Width(94)));
        }];
        
       
        
        [self.ninCLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.contentView);
            make.top.equalTo(self.topImageView.mas_bottom).offset(Get375Width(37));
            make.height.equalTo(@(Get375Width(22)));
        }];
        
        [self.ninDLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.contentView);
            make.top.equalTo(self.ninCLabel.mas_bottom).offset(Get375Width(12));
            make.height.equalTo(@(Get375Width(22)));
        }];
        
        [self.gongLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.contentView);
            make.top.equalTo(self.ninDLabel.mas_bottom).offset(Get375Width(21));
            make.height.equalTo(@(Get375Width(24)));
        }];
        
        
        
        [self.okBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.contentView);
            make.top.equalTo(self.gongLabel.mas_bottom).offset(Get375Width(30));
            make.width.equalTo(@(Get375Width(100)));
            make.height.equalTo(@(Get375Width(36)));
        }];
    }else{
        [self.contentView addSubview:self.ziLabel];
        [self.contentView addSubview:self.jiaLabel];
        [self.contentView addSubview:self.qiLabel];
        [self.bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(Get375Width(21));
            make.right.equalTo(self.contentView).offset(-(Get375Width(21)));
            make.top.equalTo(self.contentView).offset(-(Get375Width(14)));
            make.height.equalTo(@(Get375Width(148)));
        }];
        
        [self.topImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.contentView);
            make.top.equalTo(self.bgImageView).offset(Get375Width(27));
            make.width.height.equalTo(@(Get375Width(94)));
        }];
        
        [self.ziLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.contentView);
            make.top.equalTo(self.topImageView.mas_bottom).offset(Get375Width(36));
            make.height.equalTo(@(Get375Width(13)));
        }];
        
        [self.jiaLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.contentView);
            make.top.equalTo(self.ziLabel.mas_bottom).offset(Get375Width(10));
            make.height.equalTo(@(Get375Width(14.5)));
        }];
        
        [self.ninCLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.contentView);
            make.top.equalTo(self.jiaLabel.mas_bottom).offset(Get375Width(16));
            make.height.equalTo(@(Get375Width(22)));
        }];
        
        [self.ninDLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.contentView);
            make.top.equalTo(self.ninCLabel.mas_bottom).offset(Get375Width(12));
            make.height.equalTo(@(Get375Width(22)));
        }];
        
        [self.gongLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.contentView);
            make.top.equalTo(self.ninDLabel.mas_bottom).offset(Get375Width(21));
            make.height.equalTo(@(Get375Width(24)));
        }];
        
        [self.qiLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.contentView);
            make.top.equalTo(self.gongLabel.mas_bottom).offset(Get375Width(15));
            make.height.equalTo(@(Get375Width(22)));
        }];
        
        [self.okBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.contentView);
            make.top.equalTo(self.qiLabel.mas_bottom).offset(Get375Width(15));
            make.width.equalTo(@(Get375Width(100)));
            make.height.equalTo(@(Get375Width(36)));
        }];
        
    }
    
 
    
 
    
    
    
}





#pragma mark - 消失
- (void)clickBgView
{
    CGFloat leftOffset = (kScreenWidth - Get375Width(300))/2.0;
    [UIView animateWithDuration:0.4 delay:0 usingSpringWithDamping:0.95 initialSpringVelocity:0.05 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        
        self.bgView.alpha = 0.0f ;
        self.contentView.hidden = YES;
        self.contentView.frame = CGRectMake(leftOffset, ([UIScreen mainScreen].bounds.size.height - self.bgHeight)/2.0 , Get375Width(300), self.bgHeight);
        
    } completion:^(BOOL finished) {
        // 动画Animated必须是NO，不然消失之后，会有0.35s时间，再点击无效
        [self dismissViewControllerAnimated:NO completion:nil];
    }];
}

#pragma mark   ---- getter

- (UIImageView *)bgImageView {
    if (!_bgImageView) {
        _bgImageView = [UIImageView new];
        _bgImageView.image = [UIImage imageNamed:@"signin_bgicon"];
    }
    return _bgImageView;
}

- (UIImageView *)topImageView {
    if (!_topImageView) {
        _topImageView = [UIImageView new];
       
        
    }
    return _topImageView;
}


- (UILabel *)ziLabel {
    if (!_ziLabel) {
        _ziLabel = [UILabel new];
        _ziLabel.font = FontRegular(12);
        _ziLabel.textColor = kColorWithHexString(@"#919599");
        _ziLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _ziLabel;
}

- (UILabel *)jiaLabel {
    if (!_jiaLabel) {
        _jiaLabel = [UILabel new];
        _jiaLabel.font = FontRegular(14);
        _jiaLabel.textColor = kColorWithHexString(@"#919599");
        _jiaLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _jiaLabel;
}


- (UILabel *)ninCLabel {
    if (!_ninCLabel) {
        _ninCLabel = [UILabel new];
        _ninCLabel.font = FontRegular(14);
        _ninCLabel.textColor = kColorWithHexString(@"#16191C");
        _ninCLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _ninCLabel;
}


- (UILabel *)ninDLabel {
    if (!_ninDLabel) {
        _ninDLabel = [UILabel new];
        _ninDLabel.font = FontRegular(14);
        _ninDLabel.textColor = kColorWithHexString(@"#16191C");
        _ninDLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _ninDLabel;
}


- (UILabel *)gongLabel {
    if (!_gongLabel) {
        _gongLabel = [UILabel new];
        _gongLabel.font = FontRegular(14);
        _gongLabel.textColor = kColorWithHexString(@"#16191C");
        _gongLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _gongLabel;
}


- (UILabel *)qiLabel {
    if (!_qiLabel) {
        _qiLabel = [UILabel new];
        _qiLabel.font = FontMedium(12);
        _qiLabel.textColor = kColorWithHexString(@"#919599");
        _qiLabel.textAlignment = NSTextAlignmentCenter;
        _qiLabel.text = @"期待你的更多精彩～";
    }
    return _qiLabel;
}

- (UIButton *)okBtn {
    if (!_okBtn) {
        _okBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_okBtn addTarget:self action:@selector(btnAction) forControlEvents:UIControlEventTouchUpInside];
        [_okBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_okBtn graduateLeftColor:[UIColor colorWithHexString:@"#FDBC69"] ToColor:[UIColor colorWithHexString:@"#E46B2F"] startPoint:CGPointMake(0, 0) endPoint:CGPointMake(0, 1)];
        [_okBtn setTitle:@"确定" forState:UIControlStateNormal];
    }
    return _okBtn;
}

-(QJMineUserBeCollectedMsgRequest *)beCollectedMsgReq {
    if (!_beCollectedMsgReq) {
        _beCollectedMsgReq = [QJMineUserBeCollectedMsgRequest new];
    }
    return _beCollectedMsgReq;
}

- (QJMineUserBeStarCollectionMsgRequest *)beLikeMsgReq {
    if (!_beLikeMsgReq) {
        _beLikeMsgReq = [QJMineUserBeStarCollectionMsgRequest new];
    }
    return _beLikeMsgReq;
}
#pragma mark --  action

- (void)btnAction {
    [self clickBgView];
}


// 这里主动释放一些空间，加速内存的释放，防止有时候消失之后，再点不出来。
- (void)dealloc
{
    NSLog(@"%@ --> dealloc",[self class]);
    [self.bgView removeFromSuperview];
    self.bgView = nil;
    [self.contentView removeFromSuperview];
    self.contentView = nil;
}





@end


@implementation QJMineStarAlertModel

 

@end
