//
//  QJMineAuthentViewController.m
//  QJBox
//
//  Created by macm on 2022/7/21.
//

#import "QJMineAuthentViewController.h"
#import "QJMineRequest.h"

/* model */
#import "QJTaskListModel.h"
/* 弹框公共 */
#import "QJCheckInPopCommonView.h"

@interface QJMineAuthentViewController ()<UITextFieldDelegate>
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UITextField *nameTextField;
@property (nonatomic, strong) UILabel *idCardLabel;
@property (nonatomic, strong) UITextField *idCardTextField;
@property (nonatomic, strong) UIButton *doneButton;
@property (nonatomic, strong) UIImageView *authentImageView;

@end

@implementation QJMineAuthentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"实名认证";
    
    if (self.name.length != 0 && self.idcard.length != 0 ) {
        self.nameTextField.text = self.name;
        self.idCardTextField.text = self.idcard;
        [self.authentImageView setHidden:NO];
        [self.nameTextField setEnabled:NO];
        [self.idCardTextField setEnabled:NO];
        self.nameTextField.textColor = UIColorFromRGB(0x919599);
        self.idCardTextField.textColor = UIColorFromRGB(0x919599);
        [self.doneButton setHidden:YES];
    }
    
    [self createView];
}

- (void)createView {
    [self.view addSubview:self.nameLabel];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(NavigationBar_Bottom_Y+30);
    }];
    
    [self.view addSubview:self.nameTextField];
    [self.nameTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(self.nameLabel.mas_bottom).mas_offset(10);
        make.right.mas_equalTo(-15);
        make.height.mas_equalTo(40*kWScale);
    }];
    
    [self.view addSubview:self.idCardLabel];
    [self.idCardLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(self.nameTextField.mas_bottom).mas_offset(30);
    }];
    
    [self.view addSubview:self.idCardTextField];
    [self.idCardTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(self.idCardLabel.mas_bottom).mas_offset(10);
        make.right.mas_equalTo(-15);
        make.height.mas_equalTo(40*kWScale);
    }];
    
    [self.view addSubview:self.authentImageView];
    [self.authentImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.nameTextField.mas_right);
        make.centerY.mas_equalTo(self.nameTextField);
        make.width.mas_equalTo(73*kWScale);
        make.height.mas_equalTo(68*kWScale);
    }];
    
    [self.view addSubview:self.doneButton];
    [self.doneButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(-(Bottom_iPhoneX_SPACE+10));
        make.left.mas_equalTo(38);
        make.right.mas_equalTo(-38);
        make.height.mas_equalTo(48*kWScale);
    }];

}

- (void)doneAction {
    
    
    @weakify(self);
    QJMineRequest *request = [[QJMineRequest alloc] init];
    [request requestUserVerifyReal_name:self.nameTextField.text idCard:self.idCardTextField.text completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        @strongify(self);
        if (isSuccess) {
           
            NSString *simpleDesc = @"";
            if ([EncodeDicFromDic(request.responseObject, @"data") isKindOfClass:[NSDictionary class]]) {
                NSDictionary *dataDic = EncodeDicFromDic(request.responseObject, @"data");
                simpleDesc = EncodeStringFromDicDefEmtryValue(dataDic, @"simpleDesc");
            }
           
            LSUserDefaultsSET(@1, kQJIsRealName);
            [QJUserManager shareManager].isVerifyState = YES;
            if (self.realNameBlock) {
                [self.navigationController popViewControllerAnimated:NO];
                self.realNameBlock();
                
            }else{
                [self.navigationController popViewControllerAnimated:YES];
            }
            if (![kCheckNil(simpleDesc) isEqualToString:@""]) {
                [[QJAppTool getCurrentViewController].view makeToast:simpleDesc duration:3 position:CSToastPositionCenter image:[UIImage imageNamed:@"success_login"]];
            }else{
                [[QJAppTool getCurrentViewController].view makeToast:@"认证成功" duration:2 position:CSToastPositionCenter image:[UIImage imageNamed:@"success_login"]];
            }
        }
    }];
}

- (void)textFeildChange:(UITextField *)textField {
    if (self.nameTextField.text.length > 0 && self.idCardTextField.text.length == 18) {
        [self.doneButton setEnabled:YES];
        [self.doneButton setBackgroundColor:UIColorFromRGB(0x1f2a4d)];
    } else {
        [self.doneButton setEnabled:NO];
        [self.doneButton setBackgroundColor:UIColorFromRGB(0xc8cacc)];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (range.location > 17) {
        return NO;
    }
    return YES;
}

- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [UILabel new];
        _nameLabel.textColor = [UIColor blackColor];
        _nameLabel.font = FontRegular(16);
        _nameLabel.textColor = UIColorFromRGB(0x16191c);
        _nameLabel.text = @"姓名";
    }
    return _nameLabel;
}

- (UITextField *)nameTextField {
    if (!_nameTextField) {
        _nameTextField = [[UITextField alloc] init];
        _nameTextField.backgroundColor = [UIColor whiteColor];
        _nameTextField.layer.cornerRadius = 3;
        _nameTextField.font = FontRegular(14);
        _nameTextField.textColor = UIColorFromRGB(0x16191c);
        _nameTextField.placeholder = @"请输入您的真实姓名";
        UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 10)];
        _nameTextField.leftView = leftView;
        _nameTextField.leftViewMode = UITextFieldViewModeAlways;
        _nameTextField.delegate = self;
        [_nameTextField addTarget:self action:@selector(textFeildChange:) forControlEvents:UIControlEventEditingChanged];
        [_nameTextField setValue:UIColorFromRGB(0xc8cacc) forKeyPath:@"placeholderLabel.textColor"];

    }
    return _nameTextField;
}

- (UILabel *)idCardLabel {
    if (!_idCardLabel) {
        _idCardLabel = [UILabel new];
        _idCardLabel.textColor = [UIColor blackColor];
        _idCardLabel.font = FontRegular(16);
        _idCardLabel.textColor = UIColorFromRGB(0x16191c);
        _idCardLabel.text = @"身份证号";
    }
    return _idCardLabel;
}

- (UITextField *)idCardTextField {
    if (!_idCardTextField) {
        _idCardTextField = [[UITextField alloc] init];
        _idCardTextField.backgroundColor = [UIColor whiteColor];
        _idCardTextField.layer.cornerRadius = 3;
        _idCardTextField.font = FontRegular(14);
        _idCardTextField.textColor = UIColorFromRGB(0x16191c);
        _idCardTextField.placeholder = @"请输入身份证号";
        UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 10)];
        _idCardTextField.leftView = leftView;
        _idCardTextField.leftViewMode = UITextFieldViewModeAlways;
        _idCardTextField.delegate = self;
        [_idCardTextField addTarget:self action:@selector(textFeildChange:) forControlEvents:UIControlEventEditingChanged];
        [_idCardTextField setValue:UIColorFromRGB(0xc8cacc) forKeyPath:@"placeholderLabel.textColor"];

    }
    return _idCardTextField;
}

- (UIImageView *)authentImageView {
    if (!_authentImageView) {
        _authentImageView = [[UIImageView alloc] init];
        _authentImageView.image = [UIImage imageNamed:@"mine_authent"];
        [_authentImageView setHidden:YES];
    }
    return _authentImageView;
}

- (UIButton *)doneButton {
    if (!_doneButton) {
        _doneButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_doneButton setTitle:@"认证" forState:UIControlStateNormal];
        [_doneButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_doneButton.titleLabel setFont:FontSemibold(16)];
        [_doneButton setBackgroundColor:UIColorFromRGB(0xc8cacc)];
        [_doneButton setEnabled:NO];
        _doneButton.layer.cornerRadius = 4;
        [_doneButton addTarget:self action:@selector(doneAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _doneButton;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
