//
//  QJMineFansViewController.m
//  QJBox
//
//  Created by macm on 2022/7/19.
//

#import "QJMineFansViewController.h"
#import "QJSegmentLineView.h"
#import "QJMineFansPageView.h"
#import "QJMySpaceViewController.h"
#import "QJMineFansModel.h"
@interface QJMineFansViewController ()<UIScrollViewDelegate>

/* 切换视图 */
@property (nonatomic, strong) UIView *segmentView;
@property (nonatomic, strong) QJSegmentLineView *segment;
@property (nonatomic, strong) QJMineFansPageView *focusView;
@property (nonatomic, strong) QJMineFansPageView *fansView;
/* 底部滚动视图 */
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UIImageView *accountImageView;
@property (nonatomic, strong) UILabel *accountLabel;
@property (nonatomic, strong) UIButton *searchButton;
@end

@implementation QJMineFansViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    /* 初始化UI */
    [self initUI];
}

- (void)initUI{
    self.navBar.backgroundColor = [UIColor clearColor];

    [self.navBar addSubview:self.accountImageView];
    [self.accountImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(46);
        make.width.height.mas_equalTo(24*kWScale);
        make.centerY.mas_equalTo(self.navBar.backButton);
    }];
    
    [self.navBar addSubview:self.accountLabel];
    [self.accountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.accountImageView.mas_right).mas_offset(6);
        make.centerY.mas_equalTo(self.accountImageView);
    }];
    
    [self.navBar addSubview:self.searchButton];
    [self.searchButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-20);
        make.width.height.mas_equalTo(30*kWScale);
        make.centerY.mas_equalTo(self.accountImageView);
    }];
    
    [self.view addSubview:self.segmentView];
    [self.segmentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(NavigationBar_Bottom_Y);
        make.left.right.equalTo(self.view);
        make.height.equalTo(@(50*kWScale));
    }];
    [self.segmentView layoutIfNeeded];
    [self.segmentView showCorner:16 rectCorner:UIRectCornerTopLeft | UIRectCornerTopRight];
    
    [self.segmentView addSubview:self.segment];
    [self.segment mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.segmentView);
//        make.left.right.equalTo(self.segmentView);
        make.height.equalTo(@(50*kWScale));
        make.width.equalTo(@(160*kWScale));
        make.centerX.equalTo(self.segmentView);
    }];
    
    [self.view addSubview:self.scrollView];
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.segmentView.mas_bottom);
        make.bottom.equalTo(self.view);
        make.left.right.equalTo(self.view);
    }];
    
    self.focusView = [[QJMineFansPageView alloc] initWithFrame:CGRectMake(0, 0, self.scrollView.width, self.scrollView.height)];
    [self.scrollView addSubview:self.focusView];
    WS(weakSelf)
    self.focusView.didSelectedCell = ^(QJMineFansModel * _Nonnull model) {
        [weakSelf gotoSpaceVC:model.uid];
    };
    [self.focusView reloadPageView:0 uid:self.uid];
    
    self.fansView = [[QJMineFansPageView alloc] initWithFrame:CGRectMake(QJScreenWidth, 0, self.scrollView.width, self.scrollView.height)];
    [self.scrollView addSubview:self.fansView];
    self.fansView.didSelectedCell = ^(QJMineFansModel * _Nonnull model) {
        [weakSelf gotoSpaceVC:model.uid];
    };
    [self.fansView reloadPageView:1 uid:self.uid];
    
    [self.segment setSegmentSelectedIndexAndBlock:self.type];

}

#pragma mark - Lazy Loading

- (UIImageView *)accountImageView {
    if (!_accountImageView) {
        _accountImageView = [UIImageView new];
        _accountImageView.image = [UIImage imageNamed:@"mine_head"];
        _accountImageView.layer.cornerRadius = 12*kWScale;
        _accountImageView.layer.masksToBounds = YES;
    }
    return _accountImageView;
}

- (UILabel *)accountLabel {
    if (!_accountLabel) {
        _accountLabel = [UILabel new];
        _accountLabel.text = @"钱萌萌";
        _accountLabel.font = FontSemibold(16);
        _accountLabel.textColor = UIColorHex(16191c);
    }
    return _accountLabel;
}

- (UIButton *)searchButton {
    if (!_searchButton) {
        _searchButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_searchButton setImage:[UIImage imageNamed:@"mine_fans_search"] forState:UIControlStateNormal];
        [_searchButton setHidden:YES];
    }
    return _searchButton;
}

- (UIView *)segmentView{
    if (!_segmentView) {
        _segmentView = [UIView new];
        _segmentView.backgroundColor = [UIColor whiteColor];
    }
    return _segmentView;
}

- (QJSegmentLineView *)segment{
    if (!_segment) {
        _segment = [[QJSegmentLineView alloc] initWithFrame:CGRectMake(kScreenWidth/2-100, NavigationBar_Bottom_Y, 200*kWScale, 50*kWScale)];
        _segment.backgroundColor = [UIColor whiteColor];
        _segment.segmentWidthStyle = QJSegmentViewWidthStyleEqual;
        _segment.itemWidth = 80*kWScale;
        _segment.font = kFont(14);
        _segment.selectedFont = kboldFont(18);
        _segment.textColor = UIColorFromRGB(0x000000);
        _segment.selectedTextColor = UIColorFromRGB(0x000000);
        _segment.showBottomLine = NO;
        _segment.tag = 10000;
        [_segment setTitleArray:@[@"关注",@"粉丝"]];
        WS(weakSelf)
        [_segment setSegmentedItemSelectedBlock:^(QJSegmentLineView * _Nonnull segment, NSInteger selectedIndex) {
//            _segment.selectedIndex = index;
            [weakSelf.scrollView setContentOffset:CGPointMake(weakSelf.scrollView.frame.size.width * selectedIndex, 0) animated:NO];
        }];
    }
    return _segment;
}

- (UIScrollView *)scrollView{
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, NavigationBar_Bottom_Y + 44*kWScale, self.view.width, self.view.height - 50*kWScale - NavigationBar_Bottom_Y)];
        _scrollView.scrollEnabled = YES;
        _scrollView.pagingEnabled = YES;
        _scrollView.bounces = NO;
        _scrollView.delegate = self;
        _scrollView.showsVerticalScrollIndicator = NO;
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.contentSize = CGSizeMake(self.view.width*2, _scrollView.height);
    }
    return _scrollView;
}

- (void)setUserIcon:(NSString *)userIcon {
    _userIcon = userIcon;
    self.accountImageView.imageURL = [NSURL URLWithString:[userIcon checkImageUrlString]];
    
}

- (void)setUserName:(NSString *)userName {
    _userName = userName;
    self.accountLabel.text = userName;
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    int page = (_scrollView.contentOffset.x+self.view.width/2)/self.view.width;
    [_segment setSegmentSelectedIndex:page];
}
 


#pragma mark -----跳转个人空间

- (void)gotoSpaceVC:(NSString *)uid {
    QJMySpaceViewController *vc = [QJMySpaceViewController new];
    vc.userId = uid;
    [self.navigationController pushViewController:vc animated:YES];
}

@end
