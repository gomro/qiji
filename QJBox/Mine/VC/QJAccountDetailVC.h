//
//  QJAccountDetailVC.h
//  QJBox
//
//  Created by Sun on 2022/8/19.
//

#import "QJBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJAccountDetailVC : QJBaseViewController
/**奇迹币数量---必传*/
@property (nonatomic, assign) NSInteger qjcoinCurrent;
/**奇迹币数量（总获取---必传*/
@property (nonatomic, assign) NSInteger qjcoinGot;

@end

NS_ASSUME_NONNULL_END
