//
//  QJMyGuideViewController.m
//  QJBox
//
//  Created by Sun on 2022/6/17.
//

#import "QJMyGuideViewController.h"
#import "QJCampsiteListViewCell.h"
#import "QJConsultDetailViewController.h"
#import "QJCampsiteRequest.h"
#import "QJNewsCategoryModel.h"
#import "QJSegmentLineView.h"
#import "QJDetailAlertView.h"
#import "TYAlertController.h"
#import "QJDraftEmptyView.h"
#import "QJCommunityChoiceVideoOrImageVC.h"
#import "QJMineAuthentViewController.h"
@interface QJMyGuideViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
/**数据*/
@property (nonatomic, strong) NSMutableArray *dataArr;
@property (nonatomic, copy) void(^listScrollViewDidScroll)(UIScrollView *scrollView);

@property (nonatomic, assign) NSInteger page;

/* 二级筛选segment */
@property (nonatomic, strong) UIView *segmentView;
@property (nonatomic, strong) QJSegmentLineView *segment;
/* 二级筛选数据源 */
@property (nonatomic, strong) NSMutableArray *titleArray;
@property (nonatomic, strong) NSMutableArray *segmentArray;
/* 二级筛选 */
@property (nonatomic, strong) NSString *sortString;
@property (nonatomic, assign) BOOL isFirstReq;
@property (nonatomic, assign) NSInteger currentIndex;

@property (nonatomic, assign) BOOL isHiddenMySelf;//是否隐藏了文章



@end

@implementation QJMyGuideViewController

-(void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.title = @"我的攻略";
    [self loadUI];

    self.isFirstReq = YES;
    self.currentIndex = 0;
    self.titleArray = [NSMutableArray arrayWithCapacity:0];
    self.segmentArray = [NSMutableArray arrayWithCapacity:0];
    
    [self creatTitleArray];
    [self resetSearchSort];
    [self getCategoryList];
    WS(weakSelf)
    self.tableView.mj_header = [QJRefreshHeader headerWithRefreshingBlock:^{
        weakSelf.isFirstReq = YES;
        [weakSelf resetSearchSort];
    }];
    self.tableView.mj_footer = [QJRefreshFooter footerWithRefreshingBlock:^{
        [weakSelf getGuideCampsiteList];
    }];
    
    if (self.isSelectedCreat && [self.sortType isEqualToString:@"2"]) {
        [self publish];
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pulishSuccess) name:QJPublish object:nil];
    
}

//刷新列表
- (void)refreshTableview {
    self.isFirstReq = YES;
    [self resetSearchSort];
    
}

- (void)pulishSuccess {
    dispatch_async(dispatch_get_main_queue(), ^{
        self.segmentView.hidden = NO;
        [self resetSearchSort];
    });
}


#pragma mark - Load UI
- (void)loadUI {
    [self.view addSubview:self.tableView];
    
    [self.segmentView addSubview:self.segment];
    
    [self makeConstraints];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView reloadData];
}

- (void)makeConstraints {
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.equalTo(self.view);
        if(self.userId.length > 0) {
            make.bottom.equalTo(self.view).offset(-90);
        }else{
            make.bottom.equalTo(self.view).offset(-Bottom_SN_iPhoneX_OR_LATER_SPACE);
        }
        
         
    }];
}

#pragma mark - Lazy Loading
- (UITableView *)tableView {
    if (!_tableView) {
        self.tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        self.tableView.backgroundColor = [UIColor whiteColor];
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.tableView.showsVerticalScrollIndicator = NO;
        self.tableView.showsHorizontalScrollIndicator = NO;
        self.tableView.estimatedRowHeight = 160;
        if (@available(iOS 11.0, *)) {
            self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
    }
    return _tableView;
}

- (UIView *)segmentView{
    if (!_segmentView) {
        _segmentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 38*kWScale)];
        _segmentView.backgroundColor = [UIColor whiteColor];
       
    }
    return _segmentView;
}

- (QJSegmentLineView *)segment{
    if (!_segment) {
        _segment = [[QJSegmentLineView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 38*kWScale)];
        _segment.backgroundColor = [UIColor whiteColor];
        _segment.segmentWidthStyle = QJSegmentViewWidthStyleAuto;
//        _segment.itemTextOffset = 5;
        _segment.itemSpace = 16;
        _segment.leftOffset = 15;
        _segment.font = kFont(16);
        _segment.selectedFont = kboldFont(17);
        _segment.textColor = UIColorFromRGB(0x16191C);
        _segment.selectedTextColor = UIColorFromRGB(0xFF9500);
        _segment.showBottomLine = NO;
        _segment.tag = 10000;
        WS(weakSelf)
        [_segment setSegmentedItemSelectedBlock:^(QJSegmentLineView * _Nonnull segment, NSInteger selectedIndex) {
            QJNewsCategoryModel *model = weakSelf.titleArray[selectedIndex];
            weakSelf.sortString = model.code;
            weakSelf.currentIndex = selectedIndex;
            [weakSelf resetSearchSort];
        }];
    }
    return _segment;
}

- (NSMutableArray *)dataArr {
    if (!_dataArr) {
        self.dataArr = [NSMutableArray arrayWithCapacity:0];
    }
    return _dataArr;
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(self.isHiddenMySelf){
        if([self.sortType isEqualToString:@"2"]){//用户创作未开放
            QJDraftEmptyView *emptyView = [QJDraftEmptyView new];
            emptyView.string = @"用户已设置了隐私权限\n创作不可见";
            emptyView.offsetY = 72*kWScale;
            emptyView.imageStr = @"empty_no_authority";
            [self.tableView tableViewDisplayWhenHaveNoDataWithView:emptyView ifNecessaryForRowCount:self.dataArr.count];
        }else
            if([self.sortType isEqualToString:@"3"]){//用户动态未开放
            QJDraftEmptyView *emptyView = [QJDraftEmptyView new];
                emptyView.offsetY = 72*kWScale;
            emptyView.string = @"用户已设置了隐私权限\n动态不可见";
            emptyView.imageStr = @"empty_no_authority";
            [self.tableView tableViewDisplayWhenHaveNoDataWithView:emptyView ifNecessaryForRowCount:self.dataArr.count];
        }
    }else{
        if ([self.sortType isEqualToString:@"2"]) {
            
        WS(weakSelf)
            QJEmptyCommonView *emptyView = [QJEmptyCommonView new];
            if(IsStrEmpty(self.userId)|| [self.userId isEqualToString:[QJUserManager shareManager].userID]){//自己空间
            if([QJUserManager shareManager].isVerifyState){//认证过了
                emptyView.buttonString = @"现在去发布";
                emptyView.string = @"还没有发布资讯噢~";
                emptyView.emptyClick = ^{
                    [weakSelf publish];
                };
            }else{
                emptyView.string = @"暂未得到创作资格噢 ~";
                emptyView.buttonString = @"去实名认证获得博主资格";
                _segmentView.hidden = YES;
                emptyView.emptyClick = ^{
                    [weakSelf publish];
                };
            }
            }else {//他人空间
                emptyView.string = @"暂无数据";
            }
            emptyView.emptyImage = @"empty_no_data";
            emptyView.topSpace = 72*kWScale;
            [tableView tableViewDisplayWhenHaveNoDataWithView:emptyView ifNecessaryForRowCount:self.dataArr.count];
            
 
        }else{
            QJEmptyCommonView *emptyView = [QJEmptyCommonView new];
            if ([self.sortType isEqualToString:@"0"]) {
                emptyView.string = @"还没有任何点赞哦～";
            }else if ([self.sortType isEqualToString:@"1"]) {
                emptyView.string = @"还没有任何收藏哦～";
            }else if ([self.sortType isEqualToString:@"3"]) {
                emptyView.string = @"还没有发布动态哦～";
            }else{
                emptyView.string = @"暂无数据";
            }
            emptyView.emptyImage = @"empty_no_data";
            emptyView.topSpace = 72*kWScale;
            [tableView tableViewDisplayWhenHaveNoDataWithView:emptyView ifNecessaryForRowCount:self.dataArr.count];

        }
    }

    return self.dataArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellMineList = @"cellMineListId";
    QJCampsiteListCellStyle style = QJCampsiteListCellStyleDefault;
    QJCampsiteDetailModel *model = self.dataArr[indexPath.row];
    if (model.pictures.count|| model.coverImage.length > 0) {
        cellMineList = @"cellMineListIdPictures";
        style = QJCampsiteListCellStyleImage;
       
    }
    if (![kCheckStringNil(model.videoAddress) isEqualToString:@""]) {
        cellMineList = @"cellMineListIdVideo";
        style = QJCampsiteListCellStyleVideo;
    }
    QJCampsiteListViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellMineList];
    if (!cell) {
        cell = [[QJCampsiteListViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellMineList cellStyle:style];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    cell.isFromUserSpaceVC = YES;
    WS(weakSelf)
    cell.clickMoreBtnBlock = ^(QJCampsiteDetailModel *model) {
        [weakSelf cancellCollected:model];
    };
    model.sortType = self.sortType;
    [cell configureCellWithData:model];
    return cell;
}

//取消收藏
- (void)cancellCollected:(QJCampsiteDetailModel *)model{
    WS(weakSelf)
    NSString *aStr = model.collected?@"取消收藏":@"收藏";
    NSArray *titles = @[aStr];
    if ([model.userId isEqualToString:[QJUserManager shareManager].userID] && !model.deleted) {
        titles = @[aStr,@"删除"];
    }
    
    if([model.sortType isEqualToString:@"0"] && (model.deleted || model.hidden)){//点赞并且已经删除
        titles = @[@"删除记录"];
    }
    
    QJBottomSheetAlertView *view = [[QJBottomSheetAlertView alloc]initWithTitles:titles];
   __block NSString *titlestring = @"提示";
    view.clickIndex = ^(NSInteger index) {
        DLog(@"点击下标 === %ld",index);
        NSString *tips = [NSString stringWithFormat:@"确定要%@吗?",aStr];
        NSString *okBtnStr = @"确认";
        UIColor *rightColor = UIColorHex(007AFF);
        if (index == 1) {
            okBtnStr = @"删除";
            if([model.sortType isEqualToString:@"2"]){
                titlestring = @"删除资讯";
                tips = @"确认要删除该资讯吗删除后内容不可恢复";
            }else if([model.sortType isEqualToString:@"3"]){
                titlestring = @"删除动态";
                tips = @"确认要删除该动态吗删除后内容不可恢复";
            }else if([model.sortType isEqualToString:@"1"]){//收藏
                titlestring = @"确认删除";
                tips = @"该文章删除后不可恢复是否确认删除?";
            }
            else{
                if([model.type isEqualToString:@"1"]){//资讯
                    titlestring = @"删除资讯";
                    tips = @"确认要删除该资讯吗,删除后内容不可恢复";
                }else if([model.type isEqualToString:@"2"]){//动态
                    titlestring = @"删除动态";
                    tips = @"确认要删除该动态吗删除后内容不可恢复";
                }
                
            }
            
           
            rightColor = kColorWithHexString(@"#FF3B30");
        }else if(index == 0 && ![titles.firstObject isEqualToString:@"删除记录"]){
            QJCampsiteRequest *req = [QJCampsiteRequest new];
            if (index == 0) {
                [req netWorkPutCampSave:model.newsId type:model.type action:model.collected?@"0":@"1"];
                req.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
                    if (ResponseSuccess) {
                        if([aStr isEqualToString:@"取消收藏"]){
                            model.collected = NO;
                            [QJAppTool showSuccessToast:[NSString stringWithFormat:@"%@",aStr]];
                        }else{
                            [QJAppTool showSuccessToast:[NSString stringWithFormat:@"%@成功",aStr]];
                            [weakSelf.tableView.mj_header beginRefreshing];
                        }
                        
                       
                    }else{
                        [QJAppTool showSuccessToast:[NSString stringWithFormat:@"%@失败",aStr]];
                    }
                  
                };
                req.failureCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
                    [QJAppTool showWarningToast:ResponseFailToastMsg];
                };
            }
            return;
        }else if(index == 0 && (model.deleted || model.hidden) && [model.sortType isEqualToString:@"0"]){
            QJCampsiteRequest *startRequest = [[QJCampsiteRequest alloc] init];
            [startRequest netWorkPutCampLike:model.newsId?:@"" type:model.type?:@"" action:@"0"];
            [startRequest setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
                if (ResponseSuccess) {
                    [QJAppTool showToast:@"成功删除记录"];
                    [weakSelf.tableView.mj_header beginRefreshing];
                } else {
                    NSString *code = request.responseJSONObject[@"code"];
                    NSString *message = request.responseJSONObject[@"message"];
                    [[QJAppTool getCurrentViewController].view makeToast:kCheckStringNil(message)];
                
                }
            }];
           
            return;
        }
        QJDetailAlertView *alertView = [[QJDetailAlertView alloc] initWithFrame:CGRectZero titleType:QJDetailAlertViewTitleTypeDefault buttonType:QJDetailAlertViewButtonTypeDefault];
        [alertView showTitleText:titlestring describeText:tips];
        [alertView.leftButton setTitle:@"取消" forState:UIControlStateNormal];
        [alertView.rightButton setTitle:okBtnStr forState:UIControlStateNormal];
        [alertView.rightButton setTitleColor:rightColor forState:UIControlStateNormal];
         
        [alertView returnClick:^(QJDetailAlertViewButtonClickType type) {
            [[QJAppTool getCurrentViewController] dismissViewControllerAnimated:YES completion:nil];
            if (type == QJDetailAlertViewButtonClickTypeRight) {
                QJCampsiteRequest *req = [QJCampsiteRequest new];
                if (index == 0) {
                    [req netWorkPutCampSave:model.newsId type:model.type action:model.collected?@"0":@"1"];
                    req.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
                        if (ResponseSuccess) {
                            if([aStr isEqualToString:@"取消收藏"]){
                                model.collected = NO;
                                [QJAppTool showSuccessToast:[NSString stringWithFormat:@"%@",aStr]];
                            }else{
                                [QJAppTool showSuccessToast:[NSString stringWithFormat:@"%@成功",aStr]];
                                [weakSelf.tableView.mj_header beginRefreshing];
                            }
                         
                        }else{
                            [QJAppTool showSuccessToast:[NSString stringWithFormat:@"%@失败",aStr]];
                        }
                      
                    };
                    req.failureCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
                        [QJAppTool showWarningToast:ResponseFailToastMsg];
                    };
                }else if (index == 1){//删除
                    //删除分删除动态和删除资讯两个接口
                    QJCampsiteRequest *req = [QJCampsiteRequest new];
                   
                    if ([model.type isEqualToString:@"1"]) {//资讯
                        [req netWorkDeleteInfoWithId:model.newsId];
                    
                    }else if ([model.type isEqualToString:@"2"]) {//动态
                        [req netWorkDeleteCampNews:model.newsId];
                        
                    }
                    req.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
                        if (ResponseSuccess) {
                            if([model.sortType isEqualToString:@"1"]){//收藏
                                [QJAppTool showToast:@"已删除"];
                            }else{
                                [QJAppTool showToast:@"删除成功"];
                            }
                           
                            [weakSelf.tableView.mj_header beginRefreshing];
                        }else{
                            [QJAppTool showToast:@"删除失败"];
                        }
                    };
                    
                    req.failureCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
                        [QJAppTool showToast:ResponseFailToastMsg];
                    };
                    
                    if([model.sortType isEqualToString:@"1"]){//收藏
                        QJCampsiteRequest *colloctedReq = [QJCampsiteRequest new];
                        [colloctedReq netWorkPutCampSave:model.newsId type:model.type action:@"0"];
                        colloctedReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
                            if (ResponseSuccess) {
                                [weakSelf.tableView.mj_header beginRefreshing];
                             
                            }
                          
                        };
                        colloctedReq.failureCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
                            [QJAppTool showWarningToast:ResponseFailToastMsg];
                        };
                    }
                    
                    
                }
        
            }
        }];
        TYAlertController *alertController = [TYAlertController alertControllerWithAlertView:alertView preferredStyle:TYAlertControllerStyleAlert];
        [self presentViewController:alertController animated:YES completion:nil];
       
        
    };
    [self presentViewController:view animated:NO completion:nil];
   
}

#pragma mark ----去创作

- (void)publish {
    DLog(@"发布");
    
    QJCommunityChoiceVideoOrImageVC *vc = [QJCommunityChoiceVideoOrImageVC new];
    QJNavigationController *nav = [[QJNavigationController alloc]initWithRootViewController:vc];
    nav.modalPresentationStyle = UIModalPresentationCustom;
    WS(weakSelf)
        if (![QJUserManager shareManager].isVerifyState) {
            DLog(@"没实名认证跳转到实名");
            QJMineAuthentViewController *vc = [QJMineAuthentViewController new];
            vc.realNameBlock = ^{
                [QJUserManager shareManager].isVerifyState = YES;
                [weakSelf.tableView reloadData];
                [weakSelf presentViewController:nav animated:NO completion:nil];
            };
            [weakSelf.navigationController pushViewController:vc animated:YES];
            
        }else{
            [weakSelf presentViewController:nav animated:NO completion:nil];
        }
        
    
   
    
  
    
}



#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (![self.sortType isEqualToString:@"3"]) {
        return self.segmentView;
    }else{
        UIView *view = [UIView new];
        view.backgroundColor = [UIColor whiteColor];
        return view;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (![self.sortType isEqualToString:@"3"]) {
        return 38*kWScale;
    }else{
        return 0.5;
    }
}

 

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    QJCampsiteDetailModel *model = self.dataArr[indexPath.row];
    if(![model.userId isEqualToString:[QJUserManager shareManager].userID] && model.hidden){
        //别人的文章隐藏了不能点击
        return;
    }
    if (model.deleted) {//删除文章不可点击
        
        return;
    }
    QJConsultDetailViewController *detailView = [[QJConsultDetailViewController alloc] init];
    detailView.idStr = model.newsId;
    if ([model.type isEqualToString:@"1"]) {
        detailView.isNews = YES;
        detailView.isHidden = model.hidden;
    }
    [self.navigationController pushViewController:detailView animated:YES];
}

#pragma mark - GKPageListViewDelegate
- (UIScrollView *)listScrollView {
    return self.tableView;
}

- (void)listViewDidScrollCallback:(void (^)(UIScrollView * _Nonnull))callback {
    self.listScrollViewDidScroll = callback;
}
#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    !self.listScrollViewDidScroll ? : self.listScrollViewDidScroll(scrollView);
}

#pragma mark - NetWork
// 重置搜索条件
- (void)resetSearchSort {
    self.page = 1;
    [self getGuideCampsiteList];
}

- (void)getGuideCampsiteList {
    QJCampsiteRequest *startRequest = [[QJCampsiteRequest alloc] init];
    NSString *page = [NSString stringWithFormat:@"%ld", self.page];
    [startRequest netWorkGetGuideContent:self.sortType page:page sort:self.sortString uid:self.userId];
    [startRequest setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            NSArray *dataArr = request.responseJSONObject[@"data"][@"records"];
            if (self.page == 1) {
                [self.dataArr removeAllObjects];
                [self.tableView.mj_footer resetNoMoreData];
                [self.tableView.mj_header endRefreshing];
            } else {
                [self.tableView.mj_footer endRefreshing];
            }
            BOOL hasNext = [request.responseJSONObject[@"data"][@"hasNext"] boolValue];
            if (hasNext) {
                self.page += 1;
            } else {
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
            }
            if (self.isFirstReq == YES && self.currentIndex == 0) {
                self.isFirstReq = NO;
                NSString *totalStaing = request.responseJSONObject[@"data"][@"total"];
                if (![kCheckStringNil(totalStaing) isEqualToString:@"0"]) {
                    
                    if (self.titleArray && self.titleArray.count > 0) {
                        NSString *allCountString = [self.segmentArray safeObjectAtIndex:0];
                        allCountString = [NSString stringWithFormat:@"全部(%@)",totalStaing];
                        [self.segmentArray safeReplaceObjectAtIndex:0 withObject:allCountString];
                    }
                    [self.segment setTitleArray:self.segmentArray];
                }
            }

            for (NSDictionary *dic in dataArr) {
                QJCampsiteDetailModel *model = [QJCampsiteDetailModel modelWithDictionary:dic];
                
                if(model.coverImage.length > 0){
                    NSMutableArray *arr = [NSMutableArray array];
                    [arr safeAddObject:model.coverImage];
                    [arr addObjectsFromArray:model.pictures];
                    model.pictures = arr.copy;
                }
                [self.dataArr addObject:model];
            }
            [self.tableView reloadData];
        }else{
            if([EncodeStringFromDic(request.responseObject, @"code") isEqualToString:@"1001"]){
                self.isHiddenMySelf = YES;
                [self.tableView reloadData];
            }
            [self.tableView.mj_header endRefreshing];
            [self.tableView.mj_footer endRefreshing];
        }
    }];
    
    startRequest.failureCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
    };
    
}

/**
 * @author: zjr
 * @date: 2022-8-5
 * @desc: 获取二级分类
 */
- (void)getCategoryList {
    QJCampsiteRequest *startRequest = [[QJCampsiteRequest alloc] init];
    [startRequest netWorkGetInfoCategory];
    [startRequest setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            NSArray *dataArr = request.responseJSONObject[@"data"];
            for (NSDictionary *dic in dataArr) {
                QJNewsCategoryModel *model = [QJNewsCategoryModel modelWithDictionary:dic];
                if ([self.sortType isEqualToString:@"0"] || [self.sortType isEqualToString:@"1"]) {
                    [self.titleArray insertObject:model atIndex:self.titleArray.count-1];
                    [self.segmentArray insertObject:model.name atIndex:self.segmentArray.count-1];
                }else{
                    [self.titleArray insertObject:model atIndex:self.titleArray.count];
                    [self.segmentArray insertObject:model.name atIndex:self.segmentArray.count];
                }
            }
            [self.segment setTitleArray:self.segmentArray];
        }
    }];
}

- (void)creatTitleArray{
    QJNewsCategoryModel *model = [[QJNewsCategoryModel alloc] init];
    model.name = @"全部";
    model.code = @"";
    [self.titleArray addObject:model];
    [self.segmentArray addObject:model.name];
    
    if ([self.sortType isEqualToString:@"0"] || [self.sortType isEqualToString:@"1"]) {
        QJNewsCategoryModel *dynamicModel = [[QJNewsCategoryModel alloc] init];
        dynamicModel.name = @"动态";
        dynamicModel.code = @"3";
        [self.titleArray addObject:dynamicModel];
        [self.segmentArray addObject:dynamicModel.name];
    }
}
@end
