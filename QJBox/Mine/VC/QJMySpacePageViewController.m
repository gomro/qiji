//
//  QJMySpacePageViewController.m
//  QJBox
//
//  Created by Sun on 2022/7/1.
//

#import "QJMySpacePageViewController.h"
#import "JXCategoryTitleView.h"
#import "QJMineSpaceSegementView.h"
@interface QJMySpacePageViewController ()

@property (nonatomic, strong) QJMineSpaceSegementView *myCategoryView;




@end

@implementation QJMySpacePageViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
  
    
}
- (void)configTitles:(NSArray *)titles childVCs:(NSArray *)childVCs {
    self.titles = titles;
    self.childVCs = childVCs;
    if(!_myCategoryView){
        self.myCategoryView = [[QJMineSpaceSegementView alloc]initWithFrame:CGRectMake(0, 16, kScreenWidth, 20*kWScale) titles:titles];
    }
   
    self.categoryView.defaultSelectedIndex = self.selecteIndex;
    WS(weakSelf)
    self.myCategoryView.clickIndex = ^(NSInteger index) {
        [weakSelf.listContainerView.scrollView setContentOffset:CGPointMake(index*kScreenWidth, 0) animated:YES];
        weakSelf.selecteIndex = index;
    };
    [self.view addSubview:self.myCategoryView];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (self.selecteIndex > 0) {
            self.myCategoryView.selectedIndex = self.selecteIndex;
            [self.listContainerView.scrollView setContentOffset:CGPointMake(self.selecteIndex*kScreenWidth, 0) animated:NO];
        }else{
        self.myCategoryView.selectedIndex = 0;
        }
        
    });

}
- (void)selectItemView:(NSInteger)index {
    self.myCategoryView.selectedIndex = index;
     

}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    self.myCategoryView.frame = CGRectMake(0, 16, kScreenWidth, 20*kWScale);
    self.listContainerView.frame = CGRectMake(0, 43*kWScale, kScreenWidth, kScreenHeight - NavigationBar_Bottom_Y  - 43*kWScale);
    
    [self.view showCorner:16 rectCorner:UIRectCornerTopLeft | UIRectCornerTopRight];
}

- (void)listContainerViewDidScroll:(UIScrollView *)scrollView {
    DLog(@"scrllview:==== contentoffset = %@",NSStringFromCGPoint(scrollView.contentOffset));
    NSInteger yu = fmod((scrollView.contentOffset.x),(kScreenWidth));
    
    if (yu == 0) {
        self.myCategoryView.selectedIndex = (scrollView.contentOffset.x)/(kScreenWidth);
    }
    
}

- (CGFloat)preferredCategoryViewHeight {
    return 43*kWScale;
}
#pragma mark - JXCategoryListContainerViewDelegate
- (id<JXCategoryListContentViewDelegate>)listContainerView:(JXCategoryListContainerView *)listContainerView initListForIndex:(NSInteger)index {
    
    
    return self.childVCs[index];
    
}
- (void)categoryView:(JXCategoryBaseView *)categoryView didSelectedItemAtIndex:(NSInteger)index {
    //侧滑手势处理
    self.navigationController.interactivePopGestureRecognizer.enabled = (index == 0);
    NSDictionary *dic = @{@"index":[NSNumber numberWithUnsignedLong:index]};
    [[NSNotificationCenter defaultCenter] postNotificationName:@"DJRecommendViewNotification" object:nil userInfo:dic];
}


#pragma mark - UIScrollViewDelegate
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    if ([self.scrollDelegate respondsToSelector:@selector(pageScrollViewWillBeginScroll)]) {
        [self.scrollDelegate pageScrollViewWillBeginScroll];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if ([self.scrollDelegate respondsToSelector:@selector(pageScrollViewDidEndedScroll)]) {
        [self.scrollDelegate pageScrollViewDidEndedScroll];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if ([self.scrollDelegate respondsToSelector:@selector(pageScrollViewDidEndedScroll)]) {
        [self.scrollDelegate pageScrollViewDidEndedScroll];
    }
}


@end
