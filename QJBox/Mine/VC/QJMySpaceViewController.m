//
//  QJMyCampViewController.m
//  QJBox
//
//  Created by Sun on 2022/6/17.
//

#import "QJMySpaceViewController.h"
#import "QJCommunityScrollView.h"
#import "QJMySpacePageViewController.h"
#import "QJMyGuideViewController.h"
#import "QJSpaceHeaderView.h"
#import "QJMySpaceRequeset.h"
#import "QJMySpaceModel.h"
#import "QJOtherUserSpaceRequest.h"
#import "QJMineFollowRequest.h"
#import "QJMineStarAlertViewController.h"
#import "QJMineFansViewController.h"
#import "QJMineMessageViewController.h"
#import "QJDetailAlertView.h"
#import "TYAlertController.h"
#import "QJMineInfoViewController.h"
#import "QJMineUserModel.h"
#import "QJMineSetHeadImageViewController.h"
#import "QJMineUserModel.h"
#import "QJReportViewController.h"
#import "QJMineSetSignatureViewController.h"
#import "QJMallConfirmOrderVC.h"

//#define kScreenW [UIScreen mainScreen].bounds.size.width
//#define kScreenH [UIScreen mainScreen].bounds.size.height
//  适配比例
//#define ADAPTATIONRATIO     kScreenW / 375.0f
//#define kCriticalPoint -ADAPTATIONRATIO * 50.0f
#define kCriticalPoint 0

#define kWYHeaderHeight (212*kWScale)


@interface QJMySpaceViewController ()<QJMySpacePageViewControllerDelegate, GKPageScrollViewDelegate>
/**联动scrollView*/
@property (nonatomic, strong) GKPageScrollView *pageScrollView;
/**背景图*/
@property (nonatomic, strong) UIImageView *headerBgImgView;
/**高斯模糊*/
@property (nonatomic, strong) UIVisualEffectView *effectView;

/**顶部视图*/
@property (nonatomic, strong) QJSpaceHeaderView *headerView;
/**底部视图*/
@property (nonatomic, strong) QJMySpacePageViewController *pageVC;
@property (nonatomic, strong) UIView *pageView;
@property (nonatomic, strong) NSArray *titles;
@property (nonatomic, strong) NSArray *childVCs;

@property (nonatomic, strong) QJMySpaceRequeset *mySpaceReq;//我的个人空间


@property (nonatomic, strong) QJOtherUserSpaceRequest *otherSpaceReq;//别人空间


@property (nonatomic, strong) UIView *bottomBtnBgView;

@property (nonatomic, strong) QJLoginBottomBtn *followBtn;//关注按钮

@property (nonatomic, strong) QJMySpaceModel *model;

/**编辑*/
@property (nonatomic, strong) UIButton *editButton;

//更多按钮
@property (nonatomic, strong) UIButton *moreButton;

//消息按钮
@property (nonatomic, strong) UIButton *msgButton;


@property (nonatomic, strong) QJMineFollowRequest *followReq;//关注

@property (nonatomic, strong) UIImageView *navUserImageView;//导航栏头像

@property (nonatomic, strong) UILabel *navUserNameLabel;//导航栏作者名字

@property (nonatomic, assign) BOOL isInitView; // 是否已经初始化分类选择
 

@end

@implementation QJMySpaceViewController
 

- (void)viewDidLoad {
    [super viewDidLoad];
    if([self.userId isEqualToString:[QJUserManager shareManager].userID]){
        //如果是自己
        self.userId = @"";
    }
    self.navBar.backgroundColor = [UIColor clearColor];
    [self setNavBackImage:[UIImage imageNamed:@"backImage_white"] highlighted:[UIImage imageNamed:@"backImage_white"]];
    [self.navBar addSubview:self.navUserImageView];
    [self.navBar addSubview:self.navUserNameLabel];
    
    [self.navUserImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.navBar.backButton.mas_right).offset(6);
        make.centerY.equalTo(self.navBar.backButton);
        make.width.height.equalTo(@(24*kWScale));
    }];
    
    [self.navUserNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.navUserImageView.mas_right).offset(10);
        make.centerY.equalTo(self.navUserImageView);
    }];
    if (!IsStrEmpty(self.userId)) {
        self.navBar.rightBarItems = @[self.moreButton];
        [self queryOtherData];
    }else {
        
        self.navBar.rightBarItems = @[self.msgButton,self.editButton];
        [self queryMyData];
    }
  
    
    
//    [self.view addSubview:self.headerView];
    WS(weakSelf)
    self.headerView.starBlock = ^{
        QJMineStarAlertViewController *controller = [QJMineStarAlertViewController new];
        controller.style = QJMineStarAlertViewStyle_star;
        if (weakSelf.userId.length > 0) {
            controller.uid = weakSelf.userId;
            controller.isAnother = YES;
        }else{
            controller.isAnother = NO;
            controller.uid = weakSelf.model.idStr;
        }
       
        [weakSelf presentViewController:controller animated:NO completion:nil];
    };
    
    self.headerView.updatTableviewOffset = ^(CGFloat height) {
        NSInteger insetTopInt = 0;
        if(height > 0){
            insetTopInt = (294-NavigationBar_Bottom_Y +16)*kWScale;
        }else{
            insetTopInt = (270-NavigationBar_Bottom_Y +26)*kWScale;
        }
//        weakSelf.pageScrollView.contentInsetTop = insetTopInt;
        weakSelf.headerView.frame = CGRectMake(0, 0, kScreenWidth, insetTopInt);
        [weakSelf.pageScrollView.mainTableView reloadData];
        
    };

    self.headerView.fllowBlock = ^{
         //关注
        [weakSelf pushFansView:0];
    };
    
    self.headerView.fansBlock = ^{
         //粉丝
        [weakSelf pushFansView:1];
    };
    
    self.headerView.collectionBlock = ^{
         //被收藏
        QJMineStarAlertViewController *controller = [QJMineStarAlertViewController new];
        if (weakSelf.userId.length > 0) {
            controller.uid = weakSelf.userId;
            controller.isAnother = YES;
        }else{
            controller.isAnother = NO;
            controller.uid = weakSelf.model.idStr;
        }
        controller.style = QJMineStarAlertViewStyle_collection;
        [weakSelf presentViewController:controller animated:NO completion:nil];
    };
    self.headerView.qianMingBlock = ^{
        DLog(@"签名");
        QJMineSetSignatureViewController *vc = [QJMineSetSignatureViewController new];
        QJMineUserModel *userModel = [QJMineUserModel new];
        userModel.coverImage = weakSelf.model.coverImage;
        userModel.backgroundImage = weakSelf.model.backgroundImage;
        userModel.nickname = weakSelf.model.nickname;
        userModel.signature = weakSelf.model.signature;
        userModel.sex = weakSelf.model.sex;
        userModel.birthday = weakSelf.model.birthday;
        vc.model = userModel;
        vc.completeBlock = ^(NSString * string) {
            weakSelf.model.signature = string;
            weakSelf.headerView.model = weakSelf.model;
        };
        [weakSelf.navigationController pushViewController:vc animated:YES];
    };
    
    self.headerView.avatBlock = ^{
        DLog(@"编辑头像");
        
        QJMineSetHeadImageViewController *vc = [[QJMineSetHeadImageViewController alloc] init];
        vc.navString = @"设置头像";
        QJMineUserModel *userModel = [QJMineUserModel new];
        userModel.coverImage = weakSelf.model.coverImage;
        userModel.backgroundImage = weakSelf.model.backgroundImage;
        userModel.nickname = weakSelf.model.nickname;
        userModel.signature = weakSelf.model.signature;
        userModel.sex = weakSelf.model.sex;
        userModel.birthday = weakSelf.model.birthday;
        vc.model = userModel;
        
        vc.completeBlock = ^(NSString * _Nonnull headImage) {
            weakSelf.model.coverImage = headImage;
            weakSelf.headerView.avatImageStr = [headImage checkImageUrlString];
        };
        
        [vc.headImageView setImageWithURL:[NSURL URLWithString:[weakSelf.model.coverImage checkImageUrlString]] placeholder:[UIImage imageNamed:@"mine_head"]];
        [weakSelf.navigationController pushViewController:vc animated:YES];
    };
    
    [self.view addSubview:self.pageScrollView];
    [self.pageScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view).insets(UIEdgeInsetsMake(NavigationBar_Bottom_Y, 0, 0, 0));
    }];
    
    [self.view addSubview:self.headerBgImgView];
    [self.view addSubview:self.effectView];
    [self.view addSubview:self.pageScrollView];

 
    
    if (!IsStrEmpty(self.userId)) {//
        [self.view addSubview:self.bottomBtnBgView];
        [self.bottomBtnBgView addSubview:self.followBtn];
        
        [self.bottomBtnBgView mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.right.equalTo(self.view);
                    make.height.equalTo(@(90*kWScale));
                    make.bottom.equalTo(self.view);
        }];
        
        [self.followBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.equalTo(self.bottomBtnBgView).offset(37.5);
                    make.right.equalTo(self.bottomBtnBgView).offset(-37.5);
                    make.bottom.equalTo(self.bottomBtnBgView).offset(-34);
                    make.height.equalTo(@(40*kWScale));
        }];
        
        [self updateFllowstaus:self.model.stared.boolValue];
        WS(weakSelf)
       __block NSString *showStr = @"";
        self.followBtn.btnBlock = ^{
            NSString *type = @"";
            if (weakSelf.model.stared.boolValue) {
                DLog(@"取消关注");
                QJDetailAlertView *alertView = [[QJDetailAlertView alloc] initWithFrame:CGRectZero titleType:QJDetailAlertViewTitleTypeDefault buttonType:QJDetailAlertViewButtonTypeDefault];
                [alertView showTitleText:@"提示" describeText:@"确定要取消关注吗?"];
                [alertView.leftButton setTitle:@"取消" forState:UIControlStateNormal];
                [alertView.rightButton setTitle:@"确认" forState:UIControlStateNormal];
                [alertView.rightButton setTitleColor:UIColorHex(007AFF) forState:UIControlStateNormal];
                 
                [alertView returnClick:^(QJDetailAlertViewButtonClickType type) {
                    [[QJAppTool getCurrentViewController] dismissViewControllerAnimated:YES completion:nil];
                    if (type == QJDetailAlertViewButtonClickTypeRight) {
                        [weakSelf followReqConfiger:@"0" showStr:@"取消关注成功"];
                    }
                }];
                TYAlertController *alertController = [TYAlertController alertControllerWithAlertView:alertView preferredStyle:TYAlertControllerStyleAlert];
                [[QJAppTool getCurrentViewController] presentViewController:alertController animated:YES completion:nil];
                
                
            }else{
                type = @"1";
                DLog(@"关注");
                showStr = @"关注成功";
                [weakSelf followReqConfiger:type showStr:showStr];
            }

        };
        
        
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeSelectIndex:) name:@"DJRecommendViewNotification" object:nil];
}

- (void)followReqConfiger:(NSString *)type showStr:(NSString *)showStr{
    WS(weakSelf)
    self.followReq.userID = self.userId;
    self.followReq.type = type;
    self.followReq.dic = @{@"type":type,@"id":self.userId};
    self.followReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            weakSelf.model.stared = type;
            [weakSelf updateFllowstaus:type.boolValue];
            [weakSelf.view makeToast:showStr];
        }else{
            [weakSelf.view makeToast:ResponseMsg];
        }
    };
    
    self.followReq.failureCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        [weakSelf.view makeToast:ResponseFailToastMsg];
    };
    [self.followReq start];
}

//更新关注按钮
- (void)updateFllowstaus:(BOOL)status {
    if (status) {
        
        self.followBtn.title = @"已关注";
        self.followBtn.layer.borderColor = [UIColor colorWithHexString:@"#919599"].CGColor;
        self.followBtn.titleColor = kColorWithHexString(@"#919599");
        self.followBtn.layer.borderWidth = 1;
        self.followBtn.color = [UIColor whiteColor];
        _followBtn.hidden = NO;
    }else{
        _followBtn.layer.cornerRadius = 2;
        [_followBtn setTitle:@"+关注"];
        self.followBtn.titleColor = kColorWithHexString(@"#ffffff");
        self.followBtn.layer.borderWidth = 0;
        self.followBtn.color = kColorWithHexString(@"1F2A4D");
        _followBtn.hidden = NO;
    }
}

/// 状态栏颜色
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
}
 
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
}

// 刷新请求，更新个人信息，更新列表
- (void)sendRequest {
    
    if (!IsStrEmpty(self.userId)) {
        [self queryOtherData];
    }else {
        [self queryMyData];
    }

    
    QJMyGuideViewController *vc = [self.childVCs safeObjectAtIndex:self.pageVC.selecteIndex];
    [vc refreshTableview];//刷新列表
}

- (void)queryMyData {
    WS(weakSelf)
    self.mySpaceReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        [weakSelf.pageScrollView.mainTableView.mj_header endRefreshing];
        if (ResponseSuccess) {
            NSDictionary *data = EncodeDicFromDic(request.responseJSONObject, @"data");
            QJMySpaceModel *model = [QJMySpaceModel modelWithJSON:data];
            weakSelf.navUserImageView.imageURL = [NSURL URLWithString:[model.coverImage checkImageUrlString]];
            weakSelf.model = model;
            weakSelf.navUserNameLabel.text = model.nickname;
            weakSelf.headerView.isCustom = NO;
            weakSelf.headerView.model = model;
            weakSelf.headerBgImgView.imageURL = [NSURL URLWithString:[model.backgroundImage checkImageUrlString]];

            weakSelf.verifiedState = model.verifiedState?:[QJUserManager shareManager].isVerifyState;
            if (!weakSelf.isInitView) {
                [weakSelf resetSuperSegment];
                weakSelf.isInitView = YES;
            }
            
        }else{
            [weakSelf.view makeToast:ResponseMsg];
        }
    };
    self.mySpaceReq.failureCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        [weakSelf.view makeToast:ResponseFailToastMsg];
        [weakSelf.pageScrollView.mainTableView.mj_header endRefreshing];
    };
    [self.mySpaceReq start];
}

//查询别人空间
- (void)queryOtherData {
    WS(weakSelf)
    self.otherSpaceReq.userID = self.userId;
    self.otherSpaceReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            [weakSelf.pageScrollView.mainTableView.mj_header endRefreshing];
            NSDictionary *data = EncodeDicFromDic(request.responseJSONObject, @"data");
            QJMySpaceModel *model = [QJMySpaceModel modelWithJSON:data];
            weakSelf.model = model;
            [weakSelf updateFllowstaus:weakSelf.model.stared.boolValue];
            weakSelf.navUserImageView.imageURL = [NSURL URLWithString:[model.coverImage checkImageUrlString]];
            weakSelf.navUserNameLabel.text = model.nickname;
            weakSelf.headerView.isCustom = YES;
            weakSelf.headerView.model = model;
            weakSelf.headerBgImgView.imageURL = [NSURL URLWithString:[model.backgroundImage checkImageUrlString]];

            weakSelf.verifiedState = model.verifiedState;
            [weakSelf resetSuperSegment];
        }else{
            if ([EncodeStringFromDic(request.responseObject, @"code") isEqualToString:@"502"]) {
                [weakSelf.view makeToast:ResponseMsg];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [weakSelf.navigationController popViewControllerAnimated:YES];
                });
            }
            
        }
    };
    self.otherSpaceReq.failureCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        [weakSelf.view makeToast:ResponseFailToastMsg];
        [weakSelf.pageScrollView.mainTableView.mj_header endRefreshing];
    };
    [self.otherSpaceReq start];
}

/**
 * @author: zjr
 * @date: 2022-8-5
 * @desc: 重置一级segment分类
 *  根据实名认证状态（verifiedState）区分，创作
 */
- (void)resetSuperSegment{
    NSMutableArray *titleArray = [NSMutableArray array];
    NSMutableArray *chileVcArray = [NSMutableArray array];
    if (self.verifiedState == YES) {
        //查看他人
        if (!IsStrEmpty(self.userId)) {
            titleArray = @[@"动态", @"创作"].mutableCopy;
        }else {
            //查看自己
            titleArray = @[@"动态", @"创作", @"收藏", @"点赞"].mutableCopy;
        }
    }else{
        //查看他人titleArray
        if (!IsStrEmpty(self.userId)) {
            titleArray = @[@"动态"].mutableCopy;
        }else {
            //查看自己
            titleArray = @[@"动态",@"创作", @"收藏", @"点赞"].mutableCopy;
        }
    }
    
    [titleArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSString *titleString = (NSString *)obj;
        QJMyGuideViewController *guideVC = [[QJMyGuideViewController alloc] init];
        guideVC.userId = self.userId;
        if ([titleString isEqualToString:@"动态"]) {
            guideVC.sortType = @"3";
        }else if ([titleString isEqualToString:@"创作"]) {
            guideVC.sortType = @"2";
        }else if ([titleString isEqualToString:@"收藏"]) {
            guideVC.sortType = @"1";
        }else if ([titleString isEqualToString:@"点赞"]) {
            guideVC.sortType = @"0";
        }
        [chileVcArray safeAddObject:guideVC];
    }];
    
    self.titles = titleArray.copy;
    self.childVCs = chileVcArray.copy;
    if (self.isSelectedCollocted) {
        self.pageVC.selecteIndex = 2;
    }
    if (self.isSelectedCreat) {
        self.pageVC.selecteIndex = 1;
        QJMyGuideViewController *guideVC = [self.childVCs safeObjectAtIndex:1];
        guideVC.isSelectedCreat = YES;
    }
    [self.pageVC configTitles:self.titles childVCs:self.childVCs];
   
    [self.pageScrollView reloadData];
    
}

// 跳转粉丝/关注列表
- (void)pushFansView:(NSInteger)type {
    if(self.userId.length > 0) {//他人空间不允许点击
        [self.view makeToast:@"为保护用户隐私,关注/粉丝列表暂不可见~"];
        return;
    }
    
    if ([QJUserManager shareManager].isLogin) {
        QJMineFansViewController *vc = [[QJMineFansViewController alloc] init];
        vc.uid = self.model.idStr;
        vc.type = type;
        vc.userName = self.model.nickname;
        vc.userIcon = self.model.coverImage;
        [self.navigationController pushViewController:vc animated:YES];
        
    } else {
        [self showLoginVCCompleted:^(BOOL isLogin) {

        }];
    }
}

//消息中心
- (void)messageBtnClick{
    if ([QJUserManager shareManager].isLogin) {
        QJMineMessageViewController *vc = [[QJMineMessageViewController alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
    } else {
        [self showLoginVCCompleted:^(BOOL isLogin) {

        }];
    }
    
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

#pragma mark - GKPageScrollViewDelegate
- (UIView *)headerViewInPageScrollView:(GKPageScrollView *)pageScrollView {
    return self.headerView;
}

- (UIView *)pageViewInPageScrollView:(GKPageScrollView *)pageScrollView {
    return self.pageView;
}

- (NSArray<id<GKPageListViewDelegate>> *)listViewsInPageScrollView:(GKPageScrollView *)pageScrollView {
    return self.childVCs;
}

- (void)mainTableViewDidScroll:(UIScrollView *)scrollView isMainCanScroll:(BOOL)isMainCanScroll {
    CGFloat offsetY = scrollView.contentOffset.y;
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"kScrollContentOffsetNotification" object:nil userInfo:@{@"offsetY": @(offsetY)}];

    self.headerBgImgView.height = kWYHeaderHeight+NavigationBar_Bottom_Y+20-offsetY;
    self.effectView.height = kWYHeaderHeight+NavigationBar_Bottom_Y+20-offsetY;

    if (offsetY >= kWYHeaderHeight) {
        self.effectView.alpha = 1.0f;
    }else {

        // 背景虚化
        // offsetY：0 - kWYHeaderHeight 透明度alpha：0.2-1
        CGFloat alpha = 0.0f;
        if (offsetY <= 0) {
            alpha = 0.0f;
        }else if (offsetY < kWYHeaderHeight) {
            alpha = offsetY / kWYHeaderHeight;
        }else {
            alpha = 1.0f;
        }
        if (alpha < 0.2) {
            alpha = 0.2;
        }
        self.effectView.alpha = alpha;
    }
    
    BOOL show = [self isAlbumNameLabelShowingOn];

    if (show) {
        self.navUserImageView.hidden = YES;
        self.navUserNameLabel.hidden = YES;
        
    }else {
        self.navUserImageView.hidden = NO;
        self.navUserNameLabel.hidden = NO;
    }
}

- (BOOL)isAlbumNameLabelShowingOn {
    UIView *view = self.headerView.nickNameLabel;
    
    // 获取titlelabel在视图上的位置
    CGRect showFrame = [self.view convertRect:view.frame fromView:view.superview];
    
    showFrame.origin.y -= NavigationBar_Bottom_Y;
    
    // 判断是否有重叠部分
    BOOL intersects = CGRectIntersectsRect(self.view.bounds, showFrame);
    
    return !view.isHidden && view.alpha > 0.01 && intersects;
}

#pragma mark - GKWBPageViewControllDelegate
- (void)pageScrollViewWillBeginScroll {
    [self.pageScrollView horizonScrollViewWillBeginScroll];
}

- (void)pageScrollViewDidEndedScroll {
    [self.pageScrollView horizonScrollViewDidEndedScroll];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    [self.bottomBtnBgView viewShadowPathWithColor:[UIColor hx_colorWithHexStr:@"CECECE" alpha:0.25] shadowOpacity:0.3 shadowRadius:6 shadowPathType:QJShadowPathTop shadowPathWidth:6];
}

#pragma mark - 懒加载
- (GKPageScrollView *)pageScrollView {
    if (!_pageScrollView) {
        _pageScrollView = [[GKPageScrollView alloc] initWithDelegate:self];
        _pageScrollView.mainTableView.backgroundColor = [UIColor clearColor];
        _pageScrollView.isControlVerticalIndicator = YES;
        _pageScrollView.isAllowListRefresh = NO;
        _pageScrollView.ceilPointHeight = 0;
        _pageScrollView.mainTableView.mj_header = [QJRefreshWhiteHeader headerWithRefreshingTarget:self refreshingAction:@selector(sendRequest)];

//        self.pageScrollView.isAllowCustomHit = YES;
//        self.pageScrollView.mainTableView.isAllowCustomHit = YES;
//        NSInteger insetTopInt = (270-NavigationBar_Bottom_Y +30)*kWScale;
//        self.pageScrollView.contentInsetTop = insetTopInt;
    }
    return _pageScrollView;
}

- (QJSpaceHeaderView *)headerView {
    if (!_headerView) {
        _headerView = [[QJSpaceHeaderView alloc] init];
    }
    return _headerView;
}

- (UIImageView *)headerBgImgView {
    if (!_headerBgImgView) {
        _headerBgImgView = [UIImageView new];
        _headerBgImgView.contentMode = UIViewContentModeScaleAspectFill;
        _headerBgImgView.clipsToBounds = YES;
        _headerBgImgView.image = [UIImage imageNamed:@"mine_head"];
        _headerBgImgView.frame = CGRectMake(0, 0, kScreenWidth, kWYHeaderHeight+NavigationBar_Bottom_Y+20);
    }
    return _headerBgImgView;
}

- (UIVisualEffectView *)effectView {
    if (!_effectView) {
        UIBlurEffect *effect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
        
        _effectView = [[UIVisualEffectView alloc] initWithEffect:effect];
        _effectView.alpha = 0.2;
        _effectView.frame = CGRectMake(0, 0, kScreenWidth, kWYHeaderHeight+NavigationBar_Bottom_Y+20);

    }
    return _effectView;
}

- (UIButton *)editButton {
    if (!_editButton) {
        self.editButton = [UIButton buttonWithType:UIButtonTypeCustom];
     
        [_editButton setImage:[UIImage imageNamed:@"qj_mineSpace_edit"] forState:UIControlStateNormal];
        [self.editButton addTarget:self action:@selector(editAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _editButton;
}


- (UIButton *)msgButton {
    if (!_msgButton) {
        _msgButton = [UIButton buttonWithType:UIButtonTypeCustom];
     
        [_msgButton setImage:[UIImage imageNamed:@"qj_mySpace_message"] forState:UIControlStateNormal];
        [_msgButton addTarget:self action:@selector(msgAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _msgButton;
}


- (UIButton *)moreButton {
    if (!_moreButton) {
        _moreButton = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [_moreButton setImage:[UIImage imageNamed:@"qj_mySpace_more"] forState:UIControlStateNormal];
        [_moreButton addTarget:self action:@selector(moreAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _moreButton;
}


- (QJMySpacePageViewController *)pageVC {
    if (!_pageVC) {
        self.pageVC = [[QJMySpacePageViewController alloc] init];
        self.pageVC.scrollDelegate = self;
    }
    return _pageVC;
}

- (UIView *)pageView {
    if (!_pageView) {
        [self addChildViewController:self.pageVC];
        [self.pageVC didMoveToParentViewController:self];
        _pageView = self.pageVC.view;
    }
    return _pageView;
}

- (QJMySpaceRequeset *)mySpaceReq {
    if (!_mySpaceReq) {
        _mySpaceReq = [QJMySpaceRequeset new];
    }
    return _mySpaceReq;
}



- (QJLoginBottomBtn *)followBtn {
    if (!_followBtn) {
        _followBtn = [QJLoginBottomBtn new];
        _followBtn.layer.cornerRadius = 2;
        [_followBtn setTitle:@"+关注"];
        _followBtn.hidden = YES;
    }
    return _followBtn;
}

- (UIView *)bottomBtnBgView {
    if (!_bottomBtnBgView) {
        _bottomBtnBgView = [UIView new];
        _bottomBtnBgView.backgroundColor = [UIColor whiteColor];
        
    }
    return _bottomBtnBgView;
}

- (QJOtherUserSpaceRequest *)otherSpaceReq {
    if (!_otherSpaceReq) {
        _otherSpaceReq = [QJOtherUserSpaceRequest new];
        
    }
    return _otherSpaceReq;
}

-(QJMineFollowRequest *)followReq {
    if (!_followReq) {
        _followReq = [QJMineFollowRequest new];
    }
    return _followReq;
}



- (UIImageView *)navUserImageView {
    if (!_navUserImageView) {
        _navUserImageView = [UIImageView new];
        _navUserImageView.hidden = YES;
        _navUserImageView.layer.cornerRadius = 12;
        _navUserImageView.layer.masksToBounds = YES;
    }
    return _navUserImageView;
}

- (UILabel *)navUserNameLabel {
    if (!_navUserNameLabel) {
        _navUserNameLabel = [UILabel new];
        _navUserNameLabel.font = FontSemibold(16);
        _navUserNameLabel.textColor = kColorWithHexString(@"ffffff");
        _navUserNameLabel.textAlignment = NSTextAlignmentLeft;
        _navUserNameLabel.hidden = YES;
    }
    return _navUserNameLabel;
}


#pragma mark ----- action

- (void)editAction:(UIButton *)sender {
    DLog(@"编辑");
    QJMineInfoViewController *vc = [QJMineInfoViewController new];
    QJMineUserModel *model = [QJMineUserModel new];
    model.backgroundImage = self.model.backgroundImage;
    model.birthday = self.model.birthday;
    model.coverImage = self.model.coverImage;
    model.nickname = self.model.nickname;
    model.sex = self.model.sex;
    model.signature = self.model.signature;
    model.verifiedState = self.model.verifiedState;
    
    WS(weakSelf)
    if (model) {
        vc.senddata = ^(QJMineUserModel * _Nonnull model) {
            if (model) {
                weakSelf.model.backgroundImage = model.backgroundImage;
                weakSelf.model.birthday = model.birthday;
                weakSelf.model.coverImage = model.coverImage;
                weakSelf.model.nickname = model.nickname;
                weakSelf.model.sex = model.sex;
                weakSelf.model.signature = model.signature;
                weakSelf.model.verifiedState = model.verifiedState;
                weakSelf.headerView.model = weakSelf.model;
            }
            
        };
        [self.navigationController pushViewController:vc animated:YES];
    }
   
}

- (void)msgAction:(UIButton *)sender {
    DLog(@"消息");
    [self messageBtnClick];
}

- (void)moreAction:(UIButton *)sender {
    DLog(@"更多");
    QJBottomSheetAlertView *view = [[QJBottomSheetAlertView alloc]initWithTitles:@[@"举报"]];
    view.clickIndex = ^(NSInteger index) {
        DLog(@"点击下标 === %ld",index);
        QJReportViewController *vc = [QJReportViewController new];
        vc.userId = self.model.idStr;
        [self.navigationController pushViewController:vc animated:YES];
    };
    [self presentViewController:view animated:NO completion:nil];
}

#pragma mark - bottomView
- (void)changeSelectIndex:(NSNotification *)notification {
    NSDictionary *dic = notification.userInfo;
    NSInteger index = [dic[@"index"] integerValue];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];    
}

@end
