//
//  QJMineAuthentViewController.h
//  QJBox
//
//  Created by macm on 2022/7/21.
//

#import "QJBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJMineAuthentViewController : QJBaseViewController
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *idcard;

@property (nonatomic, copy) void(^realNameBlock)(void);

@end

NS_ASSUME_NONNULL_END
