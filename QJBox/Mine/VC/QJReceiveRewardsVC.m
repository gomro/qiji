//
//  QJReceiveRewardsVC.m
//  QJBox
//
//  Created by wxy on 2022/8/16.
//

#import "QJReceiveRewardsVC.h"
#import "QJAreaServiceInformationCollectionCell.h"
#import "QJTakeRewardRequest.h"
@interface QJReceiveRewardsVC ()<UICollectionViewDelegate,UICollectionViewDataSource>

@property (nonatomic, strong) UIView *productNameBgView;

@property (nonatomic, strong) UIImageView *productImageView;

@property (nonatomic, strong) UILabel *productNameLabel;

@property (nonatomic, strong) UIView *roleBgView;

@property (nonatomic, strong) UILabel *roleLabel;


@property (nonatomic, strong) UIView *collectionBgView;

@property (nonatomic, strong) UILabel *collectionTitleLabe;

@property (nonatomic, strong) UICollectionView *collectionView;

@property (nonatomic, strong) QJLoginBottomBtn *okBtn;

@property (nonatomic, strong) NSArray *array;

@property (nonatomic, strong) QJTakeRewardRequest *takeRewardReq;//领取奖励
@end

@implementation QJReceiveRewardsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"奖励领取";
    self.view.backgroundColor = RGB(244, 244, 244);
    [self makeViews];
    [self configerData];
    
}


#pragma mark  ---- UI
- (void)makeViews {
    [self.view addSubview:self.productNameBgView];
    [self.productNameBgView addSubview:self.productNameLabel];
    [self.productNameBgView addSubview:self.productImageView];
    
    [self.view addSubview:self.roleBgView];
    [self.roleBgView addSubview:self.roleLabel];
    
    [self.view addSubview:self.collectionBgView];
    [self.collectionBgView addSubview:self.collectionView];
    [self.collectionBgView addSubview:self.collectionTitleLabe];
    
    [self.view addSubview:self.okBtn];
    [self.productNameBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@62);
        make.left.equalTo(self.view).offset(16);
        make.right.equalTo(self.view).offset(-16);
        make.top.equalTo(self.navBar.mas_bottom).offset(12);
    }];
    
    [self.productImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.productNameBgView);
        make.left.equalTo(self.productNameBgView).offset(14);
        make.width.height.equalTo(@38);
    }];
    
    [self.productNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.productImageView.mas_right).offset(16);
        make.right.equalTo(self.productNameBgView).offset(-16);
        make.centerY.equalTo(self.productImageView);
        
    }];
    
    [self.roleBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.productNameBgView.mas_bottom).offset(8);
        make.height.left.right.equalTo(self.productNameBgView);
        
    }];
    
    [self.roleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.roleBgView).offset(14);
            make.centerY.equalTo(self.roleBgView);
            make.right.equalTo(self.roleBgView).offset(-14);
    }];
    
    [self.collectionBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.productNameBgView);
        make.top.equalTo(self.roleBgView.mas_bottom).offset(13);
        make.bottom.equalTo(self.collectionView).offset(34);
    }];
    
    [self.collectionTitleLabe mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.collectionBgView).offset(14);
        make.top.equalTo(self.collectionBgView).offset(7);
        make.height.equalTo(@23);
    }];
    
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.collectionBgView);
        make.top.equalTo(self.collectionTitleLabe.mas_bottom).offset(12);
        make.height.equalTo(@80);
    }];
    
    [self.okBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).offset(38);
        make.right.equalTo(self.view).offset(-38);
        make.height.equalTo(@48);
        make.bottom.equalTo(self.view).offset(-Bottom_iPhoneX_SPACE - 10);
    }];
    
}

- (void)configerData {
    self.array = @[@"冰雪复古7月28号一区"];
    [self updateCollectionHeight];
    
    [self.collectionView reloadData];
}


- (void)updateCollectionHeight {
    if (self.array.count > 4) {
        NSInteger count = self.array.count - 4;
        CGFloat i = count % 2;
        count = count / 2;
        CGFloat height = 40 *count;
        if (i > 0) {
            height = height + 40;
        }
        height = 80 + height;
        CGFloat maxHeight = kScreenHeight - NavigationBar_Bottom_Y - Bottom_iPhoneX_SPACE - 58 - 200 - 34 *2;
        if (height > maxHeight) {
            height = maxHeight;
        }
        [self.collectionView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.equalTo(@(height));
        }];
    }
}

#pragma mark ------- getter

- (UIView *)productNameBgView {
    if (!_productNameBgView) {
        _productNameBgView = [UIView new];
        _productNameBgView.backgroundColor = [UIColor whiteColor];
        _productNameBgView.layer.cornerRadius = 6;
    }
    return _productNameBgView;
}

- (UIImageView *)productImageView {
    if (!_productImageView) {
        _productImageView = [UIImageView new];
    }
    return _productImageView;
}

- (UILabel *)productNameLabel {
    if (!_productNameLabel) {
        _productNameLabel = [UILabel new];
        _productNameLabel.font = FontSemibold(16);
        _productNameLabel.textColor = kColorWithHexString(@"000000");
        _productNameLabel.textAlignment = NSTextAlignmentLeft;
        _productNameLabel.numberOfLines = 1;
    }
    return _productNameLabel;
}

-(UIView *)roleBgView {
    if (!_roleBgView) {
        _roleBgView = [UIView new];
        _roleBgView.backgroundColor = [UIColor whiteColor];
        _roleBgView.layer.cornerRadius = 6;
    }
    return _roleBgView;
}


- (UILabel *)roleLabel {
    if (!_roleLabel) {
        _roleLabel = [UILabel new];
        _roleLabel.font = FontMedium(14);
        _roleLabel.textColor = kColorWithHexString(@"#474849");
        _roleLabel.textAlignment = NSTextAlignmentLeft;
        _roleLabel.text = @"角色：";
        _roleLabel.numberOfLines = 1;
    }
    return _roleLabel;
}


- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
        layout.minimumLineSpacing = 8;
        layout.minimumInteritemSpacing = 0;
        layout.sectionInset = UIEdgeInsetsMake(0, 14, 0, 14);
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        [_collectionView registerClass:[QJAreaServiceInformationCollectionCell class] forCellWithReuseIdentifier:@"QJAreaServiceInformationCollectionCell"];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.showsHorizontalScrollIndicator = NO;
        _collectionView.backgroundColor = [UIColor whiteColor];
        
        
    }
    return _collectionView;
}

- (UIView *)collectionBgView {
    if (!_collectionBgView) {
        _collectionBgView = [UIView new];
        _collectionBgView.backgroundColor = [UIColor whiteColor];
        _collectionBgView.layer.cornerRadius = 6;
    }
    return _collectionBgView;
}

- (UILabel *)collectionTitleLabe {
    if (!_collectionTitleLabe) {
        _collectionTitleLabe = [UILabel new];
        _collectionTitleLabe.font = FontMedium(14);
        _collectionTitleLabe.textColor = kColorWithHexString(@"#474849");
        _collectionTitleLabe.text = @"游戏区服";
        _collectionTitleLabe.textAlignment = NSTextAlignmentLeft;
    }
    return _collectionTitleLabe;
}

- (QJLoginBottomBtn *)okBtn {
    if (!_okBtn) {
        _okBtn = [QJLoginBottomBtn new];
        _okBtn.title = @"确认领取";
        _okBtn.bgBtn.enabled = NO;
        _okBtn.btnBlock = ^{
            
        };
        
    }
    return _okBtn;
}

- (QJTakeRewardRequest *)takeRewardReq {
    if (!_takeRewardReq) {
        _takeRewardReq = [QJTakeRewardRequest new];
        
    }
    return _takeRewardReq;
}

#pragma mark  -------- UICollectionViewDelegate,UICollectionViewDataSource


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.array.count;
}


- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    NSString *title = [self.array safeObjectAtIndex:indexPath.item];
    QJAreaServiceInformationCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"QJAreaServiceInformationCollectionCell" forIndexPath:indexPath];
    cell.titleLabel.text = title;
    return cell;
    
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    _okBtn.bgBtn.enabled  = YES;
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    //小于等于四个以四个为一行布局
    CGFloat width = (kScreenWidth - 24 - 11 - 32) / 2;
    
    return CGSizeMake(width, 32);;
    
}

@end
