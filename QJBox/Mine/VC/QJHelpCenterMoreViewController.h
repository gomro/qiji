//
//  QJHelpCenterMoreViewController.h
//  QJBox
//
//  Created by Sun on 2022/8/25.
//

#import "QJBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJHelpCenterMoreViewController : QJBaseViewController
@property (nonatomic, copy) NSString *typeId;
@property (nonatomic, copy) NSString *typeName;

@end

NS_ASSUME_NONNULL_END
