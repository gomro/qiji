//
//  QJMineViewController.m
//  QJBox
//
//  Created by wxy on 2022/6/1.
//

#import "QJMineViewController.h"
#import "QJChangeEnvViewController.h"
#import "QJMineHeaderView.h"
#import "QJMineViewCell.h"
#import "QJMineSetViewController.h"
#import "QJMySpaceViewController.h"
#import "QJMineMessageViewController.h"
#import "QJMineInfoViewController.h"
#import "QJMineMsgUnReadView.h"
#import "QJMineMsgRequest.h"
#import "QJMineRequest.h"
#import "QJMyAccountTableViewCell.h"
#import "QJMineFansViewController.h"
#import "QJMineAuthentViewController.h"
#import "QJMineFootViewController.h"
#import "QJMineServiceViewController.h"
#import "QJHelpCenterViewController.h"
/* 更多任务 */
#import "QJMoreTaskViewController.h"

#import "QJAccountDetailVC.h"
#import "QJMallOrderListViewController.h"
#import "QJMallShopViewController.h"
#import "QJAddressViewController.h"

@interface QJMineViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
/**数据*/
@property (nonatomic, strong) NSMutableArray *dataArr;
@property (nonatomic, strong) QJMineMsgUnReadView *unReadView;
@property (nonatomic, strong) QJMineUserModel *myModel;
@property (nonatomic, strong) UIView *navView;
@end

@implementation QJMineViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self sendRequestUnRead];
    [self sendRequest];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavBarHidden:YES];
    self.view.backgroundColor = UIColorHex(f8f8f8);
    [self loadUI];
}

// 获取未读消息数
- (void)sendRequestUnRead {
    @weakify(self);
    QJMineMsgRequest *request = [[QJMineMsgRequest alloc] init];
    [request requestNotifyUnread_completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        @strongify(self);

        if (isSuccess) {
            NSInteger num = [request.responseJSONObject[@"data"][@"count"] integerValue];
            [self.unReadView reloadUnReadView:num];

        }

    }];
    
}

// 获取我的个人信息
- (void)sendRequest {
    @weakify(self);
    QJMineRequest *request = [[QJMineRequest alloc] init];
    [request requestUserInfoMine_completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        @strongify(self);
        if (isSuccess) {
            self.myModel = [QJMineUserModel modelWithJSON:request.responseJSONObject[@"data"]];
            [QJUserManager shareManager].isVerifyState = self.myModel.verifiedState;
//            LSUserDefaultsSET([NSNumber numberWithBool:self.myModel.verifiedState], kQJIsRealName);
            NSString *userId = kCheckStringNil(self.myModel.idStr);
            LSUserDefaultsSET(userId, kQJUserId);
            NSString *userHeadImage = kCheckStringNil(self.myModel.coverImage);
            LSUserDefaultsSET(userHeadImage, kQJUserHeadImage);
            NSString *registerTime = kCheckStringNil(self.myModel.registerTime);
            LSUserDefaultsSET(registerTime, kQJRegisterTime);
            [self.tableView reloadData];
        }
               
    }];
}

#pragma mark - Load UI
- (void)loadUI {
    
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.top.equalTo(self.view);
    }];

    [self setNavBar];
}

- (void)setNavBar {
    [self.view addSubview:self.navView];
    [self.navView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.view);
        make.height.mas_equalTo(hxNavigationBarHeight);
    }];
    
    /**消息中心*/
    UIButton *messageBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.navView addSubview:messageBtn];
    [messageBtn setImage:[UIImage imageNamed:@"mine_message"] forState:UIControlStateNormal];
    [messageBtn addTarget:self action:@selector(messageBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [messageBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.navView.mas_right).offset(-16);
        make.bottom.equalTo(self.navView.mas_bottom).offset(-9);
        make.width.height.mas_equalTo(25);
    }];
    
    /**未读消息数*/
    [self.navView addSubview:self.unReadView];
    [self.unReadView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(messageBtn.mas_right).mas_offset(-3);
        make.centerY.equalTo(messageBtn.mas_top).mas_offset(3);
        make.height.mas_equalTo(14);
        make.width.mas_greaterThanOrEqualTo(14);
    }];
    
    /**设置*/
    UIButton *setterBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.navView addSubview:setterBtn];
    [setterBtn setImage:[UIImage imageNamed:@"mine_set"] forState:UIControlStateNormal];
    [setterBtn addTarget:self action:@selector(pushSetVCAction) forControlEvents:UIControlEventTouchUpInside];
    [setterBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(messageBtn.mas_left).offset(-12);
        make.bottom.equalTo(self.navView.mas_bottom).offset(-9);
        make.width.height.mas_equalTo(25);
    }];
}

#pragma mark - Lazy Loading

- (QJMineMsgUnReadView *)unReadView {
    if (!_unReadView) {
        _unReadView = [[QJMineMsgUnReadView alloc] init];
        _unReadView.layer.cornerRadius = 7;
        _unReadView.layer.borderColor = [UIColor whiteColor].CGColor;
        _unReadView.layer.borderWidth = 1;
        [_unReadView setHidden:YES];
    }
    return _unReadView;
}

- (UIView *)navView {
    if (!_navView) {
        _navView = [[UIView alloc] init];
    }
    return _navView;
}

- (UITableView *)tableView {
    if (!_tableView) {
        self.tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        self.tableView.backgroundColor = UIColorHex(f8f8f8);
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.tableView.showsVerticalScrollIndicator = NO;
        self.tableView.showsHorizontalScrollIndicator = NO;
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        UIImageView *backImageView = [[UIImageView alloc]initWithFrame:self.view.bounds];
        [backImageView setImage:[UIImage imageNamed:@"mine_tab_bg"]];
        backImageView.contentMode = UIViewContentModeScaleToFill;
        self.tableView.backgroundView = backImageView;
        self.tableView.bounces = NO;
        
        if (@available(iOS 11.0, *)) {
            self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }

        [self.tableView registerClass:[QJMyAccountTableViewCell class] forCellReuseIdentifier:@"QJMyAccountTableViewCell"];
        [self.tableView registerClass:[QJMineViewCell class] forCellReuseIdentifier:@"QJMineViewCell"];
    }
    return _tableView;
}

- (NSMutableArray *)dataArr {
    if (!_dataArr) {
        self.dataArr = [NSMutableArray arrayWithCapacity:0];
    }
    return _dataArr;
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        QJMyAccountTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QJMyAccountTableViewCell"];
        cell.model = self.myModel;
        MJWeakSelf
        cell.clickButton = ^(QJMineViewCellStyle headerType) {
            [weakSelf cellButtonClick:headerType];
        };
        return cell;
    } else {
        QJMineViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QJMineViewCell"];
        MJWeakSelf
        cell.clickButton = ^(QJMineViewCellStyle headerType) {
            [weakSelf cellButtonClick:headerType];
        };
        return cell;
    }
    
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 210*kWScale;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    QJMineHeaderView *headerView = [[QJMineHeaderView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, hxNavigationBarHeight+180*kWScale)];
    headerView.model = self.myModel;
    headerView.clickHeader = ^(QJMineHeaderStyle headerType) {
        switch (headerType) {
            case QJMineHeaderStyleAvatar:
                [self pushSetInfoVCAction];
                break;
            case QJMineHeaderStyleSpace:
                [self pushSpaceView];
                break;
            case QJMineHeaderStyleFans:
                [self pushFansView:1];
                break;
            case QJMineHeaderStyleFollowers:
                [self pushFansView:0];
                break;
            case QJMineHeaderStyleFavorite:
                [self pushFavorite];
                break;

            default:
                break;
        }
    };
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return hxNavigationBarHeight+180*kWScale;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

#pragma mark - PushView
- (void)cellButtonClick:(QJMineViewCellStyle)cellStyle {
    // 我的足迹
    if (cellStyle == QJMineViewCellStyleFoot) {
        QJMineFootViewController *vc = [[QJMineFootViewController alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
    }
    // 实名认证
    if (cellStyle == QJMineViewCellStyleAuth) {
        QJMineAuthentViewController *vc = [[QJMineAuthentViewController alloc] init];
        vc.name = self.myModel.realName;
        vc.idcard = self.myModel.idCard;
        [self.navigationController pushViewController:vc animated:YES];
    }
    // 收货地址
    if (cellStyle == QJMineViewCellStyleAddress) {
        QJAddressViewController *addressVC = [[QJAddressViewController alloc] init];
        addressVC.isFromMine = YES;
        [self.navigationController pushViewController:addressVC animated:YES];
    }
    // 帮助中心
    if (cellStyle == QJMineViewCellStyleHelp) {
        QJHelpCenterViewController *vc = [[QJHelpCenterViewController alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
    }
    // 在线客服
    if (cellStyle == QJMineViewCellStyleKefu) {
        QJMineServiceViewController *vc = [[QJMineServiceViewController alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
    }
    // 我的荣耀
    if (cellStyle == QJMineViewCellStyleHonor) {
        
    }
    // 任务
    if (cellStyle == QJMineViewCellStyleTask) {
        WS(weakSelf)
        [self checkLogin:^(BOOL isLogin) {
            if (isLogin) {
                QJMoreTaskViewController *checkInVC = [[QJMoreTaskViewController alloc] init];
                [weakSelf.navigationController pushViewController:checkInVC animated:YES];
            }
        }];
    }
    // 兑换
    if (cellStyle == QJMineViewCellStyleChange) {
        QJMallShopViewController *vc = [[QJMallShopViewController alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
    }
    // 交易
    if (cellStyle == QJMineViewCellStyleDeal) {
        [[QJAppTool getCurrentViewController].view makeToast:@"敬请期待！"];
    }
    //账户
    if (cellStyle == QJMineViewCellStyleAccount) {
        QJAccountDetailVC *vc = [[QJAccountDetailVC alloc] init];
//        vc.qjcoinCurrent = self.myModel.qjcoinCurrent;
//        vc.qjcoinGot = self.myModel.qjcoinGot;
        [self.navigationController pushViewController:vc animated:YES];
    }
    
}

- (void)messageBtnClick:(UIButton *)btn {
    if ([QJUserManager shareManager].isLogin) {
        QJMineMessageViewController *vc = [[QJMineMessageViewController alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
    } else {
        [self showLoginVCCompleted:^(BOOL isLogin) {

        }];
    }
    
}

- (void)setting:(UIButton *)sender {
    QJChangeEnvViewController *vc = [QJChangeEnvViewController new];
    [self.navigationController pushViewController:vc animated:YES];
}

// 跳转设置个人信息页面
- (void)pushSetInfoVCAction {
    if ([QJUserManager shareManager].isLogin) {
        QJMineInfoViewController *vc = [QJMineInfoViewController new];
        [self.navigationController pushViewController:vc animated:YES];
    } else {
        [self showLoginVCCompleted:^(BOOL isLogin) {

        }];
    }
    
}

// 跳转设置页面
- (void)pushSetVCAction {
    if ([QJUserManager shareManager].isLogin) {
        QJMineSetViewController *vc = [QJMineSetViewController new];
        [self.navigationController pushViewController:vc animated:YES];
        
    } else {
        [self showLoginVCCompleted:^(BOOL isLogin) {

        }];
    }
}

// 跳转个人空间
- (void)pushSpaceView {
    if ([QJUserManager shareManager].isLogin) {
        QJMySpaceViewController *vc = [QJMySpaceViewController new];
        vc.verifiedState = self.myModel.verifiedState;
        [self.navigationController pushViewController:vc animated:YES];
        
    } else {
        [self showLoginVCCompleted:^(BOOL isLogin) {

        }];
    }
    
}

// 跳转个人空间收藏列表
- (void)pushFavorite {
    if ([QJUserManager shareManager].isLogin) {
        QJMySpaceViewController *vc = [QJMySpaceViewController new];
        vc.verifiedState = self.myModel.verifiedState;
        vc.isSelectedCollocted = YES;
        [self.navigationController pushViewController:vc animated:YES];
        
    } else {
        [self showLoginVCCompleted:^(BOOL isLogin) {

        }];
    }
    
}

// 跳转粉丝/关注列表
- (void)pushFansView:(NSInteger)type {
    if ([QJUserManager shareManager].isLogin) {
        QJMineFansViewController *vc = [[QJMineFansViewController alloc] init];
        vc.uid = self.myModel.idStr;
        vc.userName = self.myModel.nickname;
        vc.userIcon = self.myModel.coverImage;
        vc.type = type;
        [self.navigationController pushViewController:vc animated:YES];
        
    } else {
        [self showLoginVCCompleted:^(BOOL isLogin) {

        }];
    }
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (scrollView == self.tableView) {
        /* 导航渐变背景色 */
        UIColor *navColor = [UIColor whiteColor];
        CGFloat alpha = MIN(1, 1 - (NavigationBar_Bottom_Y - self.tableView.contentOffset.y)/NavigationBar_Bottom_Y);
        self.navView.backgroundColor = [navColor colorWithAlphaComponent:alpha];
    }
}

@end
