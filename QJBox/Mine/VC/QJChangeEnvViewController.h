//
//  QJChangeEnvViewController.h
//  QJBox
//
//  Created by wxy on 2022/6/6.
//

#import "QJBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN


///设置环境的控制器  分测试环境和线上环境
@interface QJChangeEnvViewController : QJBaseViewController

@end

NS_ASSUME_NONNULL_END
