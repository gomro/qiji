//
//  QJMineFootTableViewCell.h
//  QJBox
//
//  Created by macm on 2022/7/21.
//

#import <UIKit/UIKit.h>
#import "QJMineFootModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface QJMineFootTableViewCell : UITableViewCell
@property (nonatomic, strong) QJMineFootModel *model;
@property (strong, nonatomic) MASConstraint *bottomConstraint;
@property (nonatomic, assign) BOOL isLast;
@property (nonatomic, assign) BOOL isDelete;
@property (nonatomic, copy) void(^QJMineFootTableViewCellClick)(void);

@end

NS_ASSUME_NONNULL_END
