//
//  QJMineFootTableViewCell.m
//  QJBox
//
//  Created by macm on 2022/7/21.
//

#import "QJMineFootTableViewCell.h"

@interface QJMineFootTableViewCell ()
@property (nonatomic, strong) UIButton *selectButton;
@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) UILabel *fromLabel;
@property (nonatomic, strong) UILabel *contentLabel;
@property (strong, nonatomic) MASConstraint *leftConstraint;
@end

@implementation QJMineFootTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self loadUI];
    }
    return self;
}

#pragma mark - Load UI
- (void)loadUI {
    [self.contentView addSubview:self.bgView];
    [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        self.leftConstraint = make.left.mas_equalTo(55);
        make.top.mas_equalTo(0);
        self.bottomConstraint = make.bottom.mas_equalTo(-10);
        make.right.mas_equalTo(-15);
        make.height.mas_equalTo(40);
    }];
    
    [self.bgView addSubview:self.fromLabel];
    [self.fromLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(8);
        make.top.mas_equalTo(8);
        make.bottom.mas_equalTo(-8);
        make.width.mas_equalTo(65*kWScale);
    }];
    
    [self.bgView addSubview:self.contentLabel];
    [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.fromLabel.mas_right).mas_offset(8);
        make.right.mas_equalTo(-8);
        make.bottom.mas_equalTo(-8);
        make.top.mas_equalTo(8);
    }];
    
    [self.contentView addSubview:self.selectButton];
    [self.selectButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.width.height.mas_equalTo(30*kWScale);
        make.centerY.mas_equalTo(self.bgView);
    }];
}

- (void)setModel:(QJMineFootModel *)model {
    _model = model;
    self.contentLabel.text = _model.title;
    if ([_model.type isEqualToString:@"1"]) {
        _fromLabel.text = @"社区 | 创作";
        _fromLabel.textColor = UIColorFromRGB(0x4193a3);
        _fromLabel.layer.backgroundColor = UIColorFromRGB(0xe5f8ff).CGColor;
    } else {
        _fromLabel.text = @"社区 | 动态";
        _fromLabel.textColor = UIColorFromRGB(0xa34152);
        _fromLabel.layer.backgroundColor = UIColorFromRGB(0xffe5e9).CGColor;
    }
    if (_model.isDelete) {
        [_selectButton setImage:[UIImage imageNamed:@"mine_foot_se"] forState:UIControlStateNormal];
    } else {
        [_selectButton setImage:[UIImage imageNamed:@"mine_foot_no"] forState:UIControlStateNormal];
    }

    if (self.isLast) {
        [self.bottomConstraint uninstall];
        [self.bgView mas_updateConstraints:^(MASConstraintMaker *make) {
            self.bottomConstraint = make.bottom.mas_equalTo(0);
        }];
    } else {
        [self.bottomConstraint uninstall];
        [self.bgView mas_updateConstraints:^(MASConstraintMaker *make) {
            self.bottomConstraint = make.bottom.mas_equalTo(-10);
        }];
    }
}

- (void)setIsDelete:(BOOL)isDelete {
    _isDelete = isDelete;
    [self.leftConstraint uninstall];
    if (_isDelete) {
        [self.selectButton setHidden:NO];
        [self.bgView mas_updateConstraints:^(MASConstraintMaker *make) {
            self.leftConstraint = make.left.mas_equalTo(55);
        }];
    } else {
        [self.selectButton setHidden:YES];
        [self.bgView mas_updateConstraints:^(MASConstraintMaker *make) {
            self.leftConstraint = make.left.mas_equalTo(15);
        }];
    }
}

- (void)selectAction {
    if (self.QJMineFootTableViewCellClick) {
        self.QJMineFootTableViewCellClick();
    }
}

- (UIButton *)selectButton {
    if (!_selectButton) {
        _selectButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_selectButton setImage:[UIImage imageNamed:@"mine_foot_no"] forState:UIControlStateNormal];
        [_selectButton addTarget:self action:@selector(selectAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _selectButton;
}

- (UIView *)bgView {
    if (!_bgView) {
        _bgView = [[UIView alloc] init];
        _bgView.backgroundColor = [UIColor whiteColor];
        _bgView.layer.cornerRadius = 4;
    }
    return _bgView;
}

- (UILabel *)fromLabel {
    if (!_fromLabel) {
        _fromLabel = [UILabel new];
        _fromLabel.text = @"社区|动态";
        _fromLabel.textAlignment = NSTextAlignmentCenter;
        _fromLabel.font = FontRegular(10);
        _fromLabel.textColor = UIColorFromRGB(0xa34152);
        _fromLabel.layer.backgroundColor = UIColorFromRGB(0xffe5e9).CGColor;
        _fromLabel.layer.cornerRadius = 2;
    }
    return _fromLabel;
}

- (UILabel *)contentLabel {
    if (!_contentLabel) {
        _contentLabel = [UILabel new];
        _contentLabel.text = @"20倍经验！20倍大师！567点全职业9点全职业9点全职业9层大师！";
        _contentLabel.font = FontSemibold(12);
        _contentLabel.textColor = UIColorHex(16191c);
    }
    return _contentLabel;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
