//
//  QJProblemContactInformationCell.h
//  QJBox
//
//  Created by wxy on 2022/8/17.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJProblemContactInformationCell : UITableViewCell


@property (nonatomic, copy) void (^editFirstBlock)(NSString *str);

@property (nonatomic, copy) void (^editSecBlock)(NSString *str);

@end

NS_ASSUME_NONNULL_END
