//
//  QJMyAccountTableViewCell.h
//  QJBox
//
//  Created by macm on 2022/7/18.
//

#import <UIKit/UIKit.h>
#import "QJMineUserModel.h"
NS_ASSUME_NONNULL_BEGIN

typedef void (^ClickMineAccountCellItem)(QJMineViewCellStyle headerType);

@interface QJMyAccountTableViewCell : UITableViewCell
@property (nonatomic, strong) QJMineUserModel *model;
/**点击回传*/
@property (nonatomic, copy) ClickMineAccountCellItem clickButton;
@end

NS_ASSUME_NONNULL_END
