//
//  QJMyGuideViewCell.h
//  QJBox
//
//  Created by Sun on 2022/6/17.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJMyGuideViewCell : UITableViewCell
/**数据加载*/
- (void)configureCellWithDataStyle:(NSString *)style;
@end

NS_ASSUME_NONNULL_END
