//
//  QJAccountDetailViewCell.m
//  QJBox
//
//  Created by Sun on 2022/8/22.
//

#import "QJAccountDetailViewCell.h"
@interface QJAccountDetailViewCell ()

/**背景*/
@property (nonatomic, strong) UIView *backView;
/**标题*/
@property (nonatomic, strong) UILabel *titleLabel;
/**时间*/
@property (nonatomic, strong) UILabel *timeLabel;
/**金额*/
@property (nonatomic, strong) UILabel *amountLabel;
@end

@implementation QJAccountDetailViewCell
#pragma mark - init
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self loadUI];
    }
    return self;
}

#pragma mark - Load UI
- (void)loadUI {
    [self.contentView addSubview:self.backView];
    [self.backView addSubview:self.titleLabel];
    [self.backView addSubview:self.timeLabel];
    [self.backView addSubview:self.amountLabel];
    [self makeConstraints];
}

- (void)makeConstraints {
    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top).offset(16);
        make.left.equalTo(self.contentView.mas_left).offset(16);
        make.right.equalTo(self.contentView.mas_right).offset(-16);
        make.height.mas_equalTo(44*kWScale);
    }];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.equalTo(self.backView);
        make.right.equalTo(self.amountLabel.mas_left).offset(-10);
    }];
    
    [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom);
        make.left.bottom.equalTo(self.backView);
    }];
    
    [self.amountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.backView.mas_right).offset(-8);
        make.centerY.equalTo(self.backView);
    }];
    
}

#pragma mark - Lazy Loading

- (UIView *)backView {
    if (!_backView) {
        self.backView = [UIView new];
    }
    return _backView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        self.titleLabel = [UILabel new];
        self.titleLabel.textColor = UIColorHex(000000);
        self.titleLabel.font = MYFONTALL(FONT_REGULAR, 16);
        self.titleLabel.text = @"完成任务-加入营地";
    }
    return _titleLabel;
}

- (UILabel *)timeLabel {
    if (!_timeLabel) {
        self.timeLabel = [UILabel new];
        self.timeLabel.textColor = UIColorHex(919599);
        self.timeLabel.font = MYFONTALL(FONT_REGULAR, 12);
        self.timeLabel.text = @"2022-05-11 05:46";
    }
    return _timeLabel;
}

- (UILabel *)amountLabel {
    if (!_amountLabel) {
        self.amountLabel = [UILabel new];
        self.amountLabel.textColor = UIColorHex(FF9500);
        self.amountLabel.font = MYFONTALL(FONT_REGULAR, 20);
        self.amountLabel.text = @"+30";
    }
    return _amountLabel;
}

- (void)configureCellWithData:(QJAccountModel *)model {
    self.timeLabel.text = kCheckStringNil(model.time);
    if (model.add) {
        self.amountLabel.text = [NSString stringWithFormat:@"+%ld", model.qjcoinChange];
        self.titleLabel.text = [NSString stringWithFormat:@"%@", model.descriptionStr];
    } else {
        self.amountLabel.text = [NSString stringWithFormat:@"-%ld", model.qjcoinChange];
        self.titleLabel.text = [NSString stringWithFormat:@"%@", model.descriptionStr];
    }
}

@end
