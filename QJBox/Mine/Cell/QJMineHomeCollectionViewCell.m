//
//  QJMineHomeCollectionViewCell.m
//  QJBox
//
//  Created by macm on 2022/7/18.
//

#import "QJMineHomeCollectionViewCell.h"

@implementation QJMineHomeCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self createView];
    }
    return self;
}

- (void)createView {
    [self.contentView addSubview:self.headImageView];
    [self.headImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(10);
        make.width.height.mas_equalTo(32*kWScale);
        make.centerX.mas_equalTo(self.contentView);
    }];
    
    [self.contentView addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.headImageView.mas_bottom).mas_offset(5);
        make.centerX.mas_equalTo(self.contentView);
    }];
}

- (UIImageView *)headImageView {
    if (!_headImageView) {
        _headImageView = [UIImageView new];
        _headImageView.image = [UIImage imageNamed:@"mine_message"];
        _headImageView.contentMode = UIViewContentModeScaleAspectFit;

    }
    return _headImageView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.text = @"我的";
        _titleLabel.font = FontRegular(12);
        _titleLabel.textColor = UIColorHex(000000);
    }
    return _titleLabel;
}

@end
