//
//  QJAccountDetailViewCell.h
//  QJBox
//
//  Created by Sun on 2022/8/22.
//

#import <UIKit/UIKit.h>
#import "QJAccountModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJAccountDetailViewCell : UITableViewCell
- (void)configureCellWithData:(QJAccountModel *)model;

@end

NS_ASSUME_NONNULL_END
