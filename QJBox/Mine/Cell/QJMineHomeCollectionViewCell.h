//
//  QJMineHomeCollectionViewCell.h
//  QJBox
//
//  Created by macm on 2022/7/18.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJMineHomeCollectionViewCell : UICollectionViewCell
@property (nonatomic, strong) UIImageView *headImageView;
@property (nonatomic, strong) UILabel *titleLabel;
@end

NS_ASSUME_NONNULL_END
