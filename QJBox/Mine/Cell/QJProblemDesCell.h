//
//  QJProblemDesCell.h
//  QJBox
//
//  Created by wxy on 2022/8/16.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/// 问题描述
@interface QJProblemDesCell : UITableViewCell

@property (nonatomic, copy) void (^editEndBlock)(NSString *str);

@end

NS_ASSUME_NONNULL_END
