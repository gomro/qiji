//
//  QJHelpCenterViewCell.h
//  QJBox
//
//  Created by Sun on 2022/8/15.
//

#import <UIKit/UIKit.h>
#import "QJHelpCenterModel.h"
NS_ASSUME_NONNULL_BEGIN

typedef void (^clickCellItem)(QJHelpCenterDetailModel *model);

@interface QJHelpCenterViewCell : UITableViewCell
- (void)configureCellWithData:(QJHelpCenterDetailModel *)model;
@property (nonatomic, copy) clickCellItem clickCell;

@end

@interface QJHelpCenterSearchViewCell : UITableViewCell
- (void)configureCellWithData:(QJHelpCenterDetailModel *)model isDetail:(BOOL)isDetail;
@property (nonatomic, copy) clickCellItem clickCell;

@end
NS_ASSUME_NONNULL_END
