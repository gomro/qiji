//
//  QJMineFansTableViewCell.h
//  QJBox
//
//  Created by macm on 2022/7/19.
//

#import <UIKit/UIKit.h>
#import "QJMineFansModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJMineFansTableViewCell : UITableViewCell
@property (nonatomic, strong) QJMineFansModel *model;
@end

NS_ASSUME_NONNULL_END
