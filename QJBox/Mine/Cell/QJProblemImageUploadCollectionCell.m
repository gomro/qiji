//
//  QJProblemImageUploadCollectionCell.m
//  QJBox
//
//  Created by wxy on 2022/8/16.
//

#import "QJProblemImageUploadCollectionCell.h"



@implementation QJProblemImageUploadModel





@end

@interface QJProblemImageUploadCollectionCell ()

@property (nonatomic, strong) UIImageView *bgImageView;

@property (nonatomic, strong) UIButton *imagBtn;

@property (nonatomic, strong) UIButton *deleteBtn;



@end



@implementation QJProblemImageUploadCollectionCell

-(instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.contentView.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:self.bgImageView];
        [self.bgImageView addSubview:self.imagBtn];
        [self.contentView addSubview:self.deleteBtn];
        
        [self.bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.height.equalTo(@(80*kWScale));
            make.centerX.equalTo(self.contentView);
            make.bottom.equalTo(self.contentView);
        }];
        
        [self.imagBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.edges.equalTo(self.bgImageView);
        }];
        
        [self.deleteBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.bgImageView.mas_right);
            make.centerY.equalTo(self.bgImageView.mas_top);
            make.width.height.equalTo(@(30*kWScale));
        }];
    }
    return self;
}




#pragma mark ------- getter

- (UIImageView *)bgImageView {
    if (!_bgImageView) {
        _bgImageView = [UIImageView new];
        _bgImageView.image = [UIImage imageNamed:@"qj_uploadImage"];
        _bgImageView.userInteractionEnabled = YES;
    }
    return _bgImageView;
}

- (UIButton *)imagBtn {
    if (!_imagBtn) {
        _imagBtn = [UIButton new];
        [_imagBtn addTarget:self action:@selector(addImageAction) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _imagBtn;
}



- (UIButton *)deleteBtn {
    if (!_deleteBtn) {
        _deleteBtn = [UIButton new];
        [_deleteBtn addTarget:self action:@selector(deleteBtnAction) forControlEvents:UIControlEventTouchUpInside];
        [_deleteBtn setImage:[UIImage imageNamed:@"qj_delete_red_16"] forState:UIControlStateNormal];
        [_deleteBtn setEnlargeEdgeWithTop:20 right:20 bottom:20 left:20];
        
    }
    return _deleteBtn;
}



- (void)setModel:(QJProblemImageUploadModel *)model {
    _model = model;
    self.bgImageView.image = model.image;
    self.deleteBtn.hidden = model.isHiddenDeleteBtn;
}


- (void)deleteBtnAction {
    DLog(@"删除");
    if (self.deleteBlock) {
        self.deleteBlock(self.model);
    }
    
}


- (void)addImageAction {
    if (self.addImageBlock) {
        self.addImageBlock();
    }
}

@end
