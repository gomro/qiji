//
//  QJProblemDesCell.m
//  QJBox
//
//  Created by wxy on 2022/8/16.
//

#import "QJProblemDesCell.h"



@interface QJProblemDesCell ()<UITextViewDelegate>

@property (nonatomic, strong) UILabel *redLabel;

@property (nonatomic, strong) UILabel *topLabel;

@property (nonatomic, strong) UITextView *titleText;

@property (nonatomic, strong) UILabel *titleTextPlaceholder;

@property (nonatomic, strong) UILabel *titleNum;
@end



@implementation QJProblemDesCell


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.contentView.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:self.redLabel];
        [self.contentView addSubview:self.topLabel];
        [self.contentView addSubview:self.titleText];
        [self.titleText addSubview:self.titleTextPlaceholder];
        [self.contentView addSubview:self.titleNum];
        [self.redLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.equalTo(self.contentView).offset(16);
                    make.top.equalTo(self.contentView).offset(14);
                    
        }];
        
        [self.redLabel setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
        
        [self.topLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.equalTo(self.redLabel.mas_right);
                    make.top.equalTo(self.contentView).offset(16);
                    make.right.equalTo(self.contentView).offset(-16);
        }];
        
        [self.titleText mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.equalTo(self.contentView).offset(10);
                    make.right.equalTo(self.contentView).offset(-10);
                    make.top.equalTo(self.redLabel.mas_bottom).offset(3);
                    make.bottom.equalTo(self.titleNum.mas_top);
        }];
        
        [self.titleTextPlaceholder mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.top.left.equalTo(self.titleText).offset(6);
                    make.right.equalTo(self.contentView.mas_right).offset(-16);
        }];
        
        [self.titleNum mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.right.bottom.equalTo(self.contentView).offset(-16);
                    
        }];
    }
    return self;
}


#pragma mark  -- getter

- (UILabel *)redLabel {
    if (!_redLabel) {
        _redLabel = [UILabel new];
        _redLabel.textColor = kColorWithHexString(@"#FF3B30");
        _redLabel.text = @"*";
        _redLabel.font = FontSemibold(16);
    }
    return _redLabel;
}

- (UILabel *)topLabel {
    if (!_topLabel) {
        _topLabel = [UILabel new];
        _topLabel.text = @"问题描述";
        _topLabel.textAlignment = NSTextAlignmentLeft;
        _topLabel.font = FontSemibold(14);
    }
    return _topLabel;
}


- (UITextView *)titleText {
    if (!_titleText) {
        _titleText = [UITextView new];
        _titleText.textColor = UIColorHex(16191C);
        _titleText.font = FontRegular(14);
        _titleText.delegate = self;
        
        
    }
    return _titleText;
}


-(UILabel *)titleTextPlaceholder {
    if (!_titleTextPlaceholder) {
        _titleTextPlaceholder = [UILabel new];
        NSAttributedString *attr = [[NSAttributedString alloc]initWithString:@"请详细描述您遇到的问题并附带对应截图以便盒子提供更好的服务给您！" attributes:@{NSForegroundColorAttributeName:UIColorHex(919599),NSFontAttributeName:FontRegular(14)}];
        
        _titleTextPlaceholder.attributedText = attr;
        _titleTextPlaceholder.numberOfLines = 0;
    }
    return _titleTextPlaceholder;
}

- (UILabel *)titleNum {
    if (!_titleNum) {
        _titleNum = [UILabel new];
        _titleNum.textColor = UIColorHex(919599);
        _titleNum.font = FontRegular(14);
        _titleNum.textAlignment = NSTextAlignmentRight;
        _titleNum.text = @"0/50";
    }
    return _titleNum;
}


#pragma mark -----------UITextViewDelegate
- (void)textViewDidChange:(UITextView *)textView {
    if (textView == self.titleText){
        if (textView.text.length == 0) {
            self.titleTextPlaceholder.hidden = NO;
        }else{
            self.titleTextPlaceholder.hidden = YES;
        }
        if (self.titleText.text.length >= 50) {
            self.titleText.text = [self.titleText.text substringToIndex:50];
            
        }
    }
    if (self.editEndBlock) {
        self.editEndBlock(self.titleText.text);
    }
    
    
    NSInteger maxFontNum = 50;//最大输入限制
    NSString *toBeString = textView.text;
    // 获取键盘输入模式
    NSString *lang = [[UIApplication sharedApplication] textInputMode].primaryLanguage;
    if ([lang isEqualToString:@"zh-Hans"]) { // zh-Hans代表简体中文输入，包括简体拼音，健体五笔，简体手写
        UITextRange *selectedRange = [textView markedTextRange];
        //获取高亮部分
        UITextPosition *position = [textView positionFromPosition:selectedRange.start offset:0];
        // 没有高亮选择的字，则对已输入的文字进行字数统计和限制
        if (!position) {
            if (toBeString.length > maxFontNum) {
                textView.text = [toBeString substringToIndex:maxFontNum];//超出限制则截取最大限制的文本
                if (textView == self.titleText) {
                    self.titleNum.text = @"50/50";
                }
                
            } else {
                if (textView == self.titleText) {
                    self.titleNum.text = [NSString stringWithFormat:@"%ld/50",toBeString.length];
                }
                
            }
        }
    } else {// 中文输入法以外的直接统计
        if (toBeString.length > maxFontNum) {
            textView.text = [toBeString substringToIndex:maxFontNum];
            if (textView == self.titleText) {
                self.titleNum.text = [NSString stringWithFormat:@"%ld/50",maxFontNum];
            }
            
        } else {
            if (textView == self.titleText) {
                self.titleNum.text = [NSString stringWithFormat:@"%ld/%ld",toBeString.length,maxFontNum];
            }
            
        }
    }
    
}

@end
