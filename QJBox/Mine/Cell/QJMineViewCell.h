//
//  QJMineViewCell.h
//  QJBox
//
//  Created by Sun on 2022/6/15.
//

#import <UIKit/UIKit.h>
#import "QJMineUserModel.h"

NS_ASSUME_NONNULL_BEGIN

typedef void (^ClickMineCellItem)(QJMineViewCellStyle headerType);

@interface QJMineViewCell : UITableViewCell

/**点击回传*/
@property (nonatomic, copy) ClickMineCellItem clickButton;

@end

NS_ASSUME_NONNULL_END
