//
//  QJMyGuideViewCell.m
//  QJBox
//
//  Created by Sun on 2022/6/17.
//

#import "QJMyGuideViewCell.h"

@interface QJMyGuideViewCell ()
/**状态*/
@property (nonatomic, strong) UILabel *styleLabel;
/**攻略*/
@property (nonatomic, strong) UILabel *raidersLabel;
/**间隔视图*/
@property (nonatomic, strong) UIView *intervalView;

@end

@implementation QJMyGuideViewCell
#pragma mark - init
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self loadUI];
    }
    return self;
}

#pragma mark - Load UI
- (void)loadUI {
    [self.contentView addSubview:self.styleLabel];
    [self.contentView addSubview:self.raidersLabel];
    [self.contentView addSubview:self.intervalView];
    
    [self makeConstraints];
    self.raidersLabel.text = @"测试攻略\n　　CF手游生化酒店BUG卡进去幽灵抓不到，在生化酒店地图中有个很大的BUG，玩家只要建筑旁就能利用BUG卡出去，之后地图中的幽灵看不到你而你可以开枪打他，这个BUG还是蛮大的。目前这个bug有热心玩家已经提交给了官方，现在大家通过视频可以了解BUG位置，到CF手游不限号版本中再去生化酒店地图中验证一番，到时候就能看出BUG有没有被修复啦。";
}

- (void)makeConstraints {
    [self.styleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView.mas_right).offset(-16);
        make.top.equalTo(self.contentView.mas_top).offset(10);
    }];
    
    [self.raidersLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView.mas_left).offset(16);
        make.right.equalTo(self.contentView.mas_right).offset(-16);
        make.top.equalTo(self.styleLabel.mas_bottom).offset(10);
    }];
    
    [self.intervalView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.raidersLabel.mas_bottom).offset(15);
        make.left.right.bottom.equalTo(self.contentView);
        make.height.mas_equalTo(20);
    }];
    
}

#pragma mark - Lazy Loading
- (UILabel *)styleLabel {
    if (!_styleLabel) {
        self.styleLabel = [UILabel new];
        self.styleLabel.textColor = [UIColor blackColor];
        self.styleLabel.font = kFont(13);
    }
    return _styleLabel;
}

- (UILabel *)raidersLabel {
    if (!_raidersLabel) {
        self.raidersLabel = [UILabel new];
        self.raidersLabel.textColor = UIColorHex(666666);
        self.raidersLabel.font = kFont(15);
        self.raidersLabel.numberOfLines = 0;
    }
    return _raidersLabel;
}

- (UIView *)intervalView {
    if (!_intervalView) {
        self.intervalView = [UIView new];
        self.intervalView.backgroundColor = UIColorHex(F8F8F8);
    }
    return _intervalView;
}

#pragma mark - Configure Data
- (void)configureCellWithDataStyle:(NSString *)title {
    self.styleLabel.text = title;
}


@end
