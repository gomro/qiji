//
//  QJHelpCenterViewCell.m
//  QJBox
//
//  Created by Sun on 2022/8/15.
//

#import "QJHelpCenterViewCell.h"

@interface QJHelpCenterViewCell ()
/**背景*/
@property (nonatomic, strong) UIView *backView;
/**背景按钮*/
@property (nonatomic, strong) UIButton *backButton;
/**标题*/
@property (nonatomic, strong) UILabel *titleLabel;
/**右边图片*/
@property (nonatomic, strong) UIImageView *rightImage;
@property (nonatomic, strong) QJHelpCenterDetailModel *model;
@end

@implementation QJHelpCenterViewCell
#pragma mark - init
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = UIColorHex(F5F5F5);
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self loadUI];
    }
    return self;
}

#pragma mark - Load UI
- (void)loadUI {
    [self.contentView addSubview:self.backView];
    [self.backView addSubview:self.backButton];
    [self.backButton addSubview:self.titleLabel];
    [self.backButton addSubview:self.rightImage];
    [self makeConstraints];
}

- (void)makeConstraints {
    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(self.contentView);
        make.left.equalTo(self.contentView.mas_left).offset(16);
        make.right.equalTo(self.contentView.mas_right).offset(-16);
    }];
    
    [self.backButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.backView.mas_top).offset(4);
        make.left.equalTo(self.backView.mas_left).offset(16);
        make.right.equalTo(self.backView.mas_right).offset(-16);
        make.height.mas_equalTo(34*kWScale);
    }];
    
    [self.rightImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.backButton.mas_right).offset(-16);
        make.centerY.equalTo(self.backButton);
        make.width.height.mas_equalTo(24*kWScale);
    }];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.backButton.mas_left).offset(16);
        make.right.equalTo(self.rightImage.mas_left).offset(-10);
        make.centerY.equalTo(self.backButton);
    }];
    
    
}

#pragma mark - Lazy Loading

- (UIView *)backView {
    if (!_backView) {
        _backView = [UIView new];
        _backView.backgroundColor = [UIColor whiteColor];
    }
    return _backView;
}

- (UIView *)backButton {
    if (!_backButton) {
        _backButton = [UIButton new];
        _backButton.backgroundColor = UIColorHex(F5F5F5);
        [_backButton addTarget:self action:@selector(pushDetail) forControlEvents:UIControlEventTouchUpInside];
    }
    return _backButton;
}

- (UIImageView *)rightImage {
    if (!_rightImage) {
        _rightImage = [[UIImageView alloc] init];
        _rightImage.image = [UIImage imageNamed:@"mine_right"];
    }
    return _rightImage;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        self.titleLabel = [UILabel new];
        self.titleLabel.textColor = UIColorHex(919599);
        self.titleLabel.font = MYFONTALL(FONT_REGULAR, 14);
        self.titleLabel.text = @"违规用户举报举报";
    }
    return _titleLabel;
}

- (void)configureCellWithData:(QJHelpCenterDetailModel *)model {
    self.titleLabel.text = kCheckStringNil(model.name);
    self.model = model;
}

- (void)pushDetail {
    if (self.clickCell) {
        self.clickCell(self.model);
    }
}


@end


@interface QJHelpCenterSearchViewCell ()
/**背景按钮*/
@property (nonatomic, strong) UIButton *backButton;
/**标题*/
@property (nonatomic, strong) UILabel *titleLabel;
/**右边图片*/
@property (nonatomic, strong) UIImageView *rightImage;
@property (nonatomic, strong) QJHelpCenterDetailModel *model;

@end

@implementation QJHelpCenterSearchViewCell
#pragma mark - init
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self loadUI];
    }
    return self;
}

#pragma mark - Load UI
- (void)loadUI {
    [self.contentView addSubview:self.backButton];
    [self.backButton addSubview:self.titleLabel];
    [self.backButton addSubview:self.rightImage];
    [self makeConstraints];
}

- (void)makeConstraints {
    
    [self.backButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top).offset(8);
        make.left.equalTo(self.contentView.mas_left).offset(16);
        make.right.equalTo(self.contentView.mas_right).offset(-16);
        make.height.mas_equalTo(34);
    }];
    
    [self.rightImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.backButton.mas_right).offset(-16);
        make.centerY.equalTo(self.backButton);
        make.width.height.mas_equalTo(24);
    }];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.backButton.mas_left).offset(16);
        make.right.equalTo(self.rightImage.mas_left).offset(-10);
        make.centerY.equalTo(self.backButton);
    }];
    
    
}

#pragma mark - Lazy Loading


- (UIView *)backButton {
    if (!_backButton) {
        _backButton = [UIButton new];
        _backButton.backgroundColor = UIColorHex(F5F5F5);
        [_backButton addTarget:self action:@selector(pushDetail) forControlEvents:UIControlEventTouchUpInside];
    }
    return _backButton;
}

- (UIImageView *)rightImage {
    if (!_rightImage) {
        _rightImage = [[UIImageView alloc] init];
        _rightImage.image = [UIImage imageNamed:@"mine_right"];
    }
    return _rightImage;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        self.titleLabel = [UILabel new];
        self.titleLabel.textColor = UIColorHex(919599);
        self.titleLabel.font = MYFONTALL(FONT_REGULAR, 14);
        self.titleLabel.text = @"违规用户举报举报";
    }
    return _titleLabel;
}

- (void)configureCellWithData:(QJHelpCenterDetailModel *)model isDetail:(BOOL)isDetail {
    if (isDetail) {
        self.contentView.backgroundColor = UIColorHex(F5F5F5);
        self.backButton.backgroundColor = [UIColor whiteColor];

    }
    self.titleLabel.text = kCheckStringNil(model.name);
    self.model = model;

}

- (void)pushDetail {
    if (self.clickCell) {
        self.clickCell(self.model);
    }
}
@end
