//
//  QJProblemImageUploadCollectionCell.h
//  QJBox
//
//  Created by wxy on 2022/8/16.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJProblemImageUploadModel : NSObject

@property (nonatomic, strong) UIImage *image;

@property (nonatomic, assign) BOOL isHiddenDeleteBtn;

@end



@interface QJProblemImageUploadCollectionCell : UICollectionViewCell


@property (nonatomic, copy) void(^deleteBlock)(QJProblemImageUploadModel *model);

@property (nonatomic, copy) void(^addImageBlock)(void);

@property (nonatomic, strong) QJProblemImageUploadModel *model;

@end

NS_ASSUME_NONNULL_END
