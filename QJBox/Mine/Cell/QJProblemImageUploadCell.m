//
//  QJProblemImageUploadCell.m
//  QJBox
//
//  Created by wxy on 2022/8/16.
//

#import "QJProblemImageUploadCell.h"
#import "QJProblemImageUploadCollectionCell.h"
#import "TZImagePickerController.h"
@interface QJProblemImageUploadCell ()<UICollectionViewDelegate,UICollectionViewDataSource>

@property (nonatomic, strong) UICollectionView *collectionView;


@end



@implementation QJProblemImageUploadCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.contentView.backgroundColor = [UIColor whiteColor];
        
        [self.contentView addSubview:self.topLabel];
        [self.contentView addSubview:self.collectionView];
        
        QJProblemImageUploadModel *model = [QJProblemImageUploadModel new];
        model.image = [UIImage imageNamed:@"qj_uploadImage"];
        model.isHiddenDeleteBtn = YES;
        self.array = @[model].mutableCopy;
        [self.topLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(16);
            make.top.equalTo(self.contentView).offset(16);
            make.right.equalTo(self.contentView).offset(-16);
        }];
        
        [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.top.equalTo(self.topLabel.mas_bottom).offset(2);
                    make.height.equalTo(@(96*kWScale));
                    make.left.right.equalTo(self.contentView);
        }];
        
        
    }
    return self;
}


#pragma mark  -- getter


- (UILabel *)topLabel {
    if (!_topLabel) {
        _topLabel = [UILabel new];
        _topLabel.text = @"问题截图(0/3)";
        _topLabel.textAlignment = NSTextAlignmentLeft;
        _topLabel.font = FontSemibold(14);
    }
    return _topLabel;
}

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
        layout.minimumLineSpacing = 0;
        layout.minimumInteritemSpacing = 13;
        layout.sectionInset = UIEdgeInsetsMake(0, 16, 0, 16);
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        [_collectionView registerClass:[QJProblemImageUploadCollectionCell class] forCellWithReuseIdentifier:@"QJProblemImageUploadCollectionCell"];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.showsHorizontalScrollIndicator = NO;
        _collectionView.backgroundColor = [UIColor whiteColor];
        
        
    }
    return _collectionView;
}


- (void)updateLabel {
    int i = 0;
    for (QJProblemImageUploadModel *model in self.array.copy) {
        if (!model.isHiddenDeleteBtn) {
            i++;
        }
    }
    self.topLabel.text = [NSString stringWithFormat:@"问题截图(%d/3)",i];
    
}

#pragma mark  ---------- 选择图片
- (void)choiceImage {
    TZImagePickerController *vc = [[TZImagePickerController alloc]initWithMaxImagesCount:1 delegate:nil];
    vc.allowPickingVideo = NO;
    vc.allowPickingOriginalPhoto = NO;
    vc.didFinishPickingPhotosHandle = ^(NSArray<UIImage *> *photos, NSArray *assets, BOOL isSelectOriginalPhoto) {
        UIImage *image = photos.firstObject;
        if (image) {
            QJProblemImageUploadModel *model = [QJProblemImageUploadModel new];
            model.image = image;
            model.isHiddenDeleteBtn = NO;
            
            [self.array insertObject:model atIndex:(self.array.count - 1)];
            if (self.array.count == 4) {
                [self.array removeObject:self.array.lastObject];
            }
            [self updateLabel];
            [self makeSelectedImages];
            [self.collectionView reloadData];
            
        }
    };
    vc.modalPresentationStyle = UIModalPresentationCustom;
    [[QJAppTool getCurrentViewController] presentViewController:vc animated:YES completion:nil];
    
}


//选中图片回传
- (void)makeSelectedImages {
    NSMutableArray *muArr = [NSMutableArray array];
    for (QJProblemImageUploadModel *imageModel in self.array.copy) {
        if (!imageModel.isHiddenDeleteBtn) {
            [muArr safeAddObject:imageModel.image];
        }
    }
    if (self.selectedImages) {
        self.selectedImages(muArr.copy);
    }
}


#pragma mark  -------- UICollectionViewDelegate,UICollectionViewDataSource


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.array.count;
}


- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    QJProblemImageUploadModel *model = [self.array safeObjectAtIndex:indexPath.item];
    QJProblemImageUploadCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"QJProblemImageUploadCollectionCell" forIndexPath:indexPath];
    cell.deleteBlock = ^(QJProblemImageUploadModel * _Nonnull model) {
        QJProblemImageUploadModel *lastModel = self.array.lastObject;
        if (self.array.count == 3 && !lastModel.isHiddenDeleteBtn) {
            [self.array removeObject:model];
            QJProblemImageUploadModel *holderModel = [QJProblemImageUploadModel new];
            holderModel.image = [UIImage imageNamed:@"qj_uploadImage"];
            holderModel.isHiddenDeleteBtn = YES;
            [self.array safeAddObject:holderModel];
        }else{
            [self.array removeObject:model];
        }
        [self makeSelectedImages];
        [self updateLabel];
        [self.collectionView reloadData];
    };
    
    cell.addImageBlock = ^{
      //添加图片
        if (model.isHiddenDeleteBtn) {
            DLog(@"选择图片");
            [self choiceImage];
        }
        
    };
    cell.model = model;
    return cell;
    
}


 


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return CGSizeMake(96*kWScale, 96*kWScale);;
    
}


@end
