//
//  QJMineViewCell.m
//  QJBox
//
//  Created by Sun on 2022/6/15.
//

#import "QJMineViewCell.h"
#import "QJMineCellButton.h"
#import "QJMineHomeCollectionViewCell.h"
#import "QJMineFlowLayout.h"

@interface QJMineViewCell ()<UICollectionViewDelegate, UICollectionViewDataSource>
/**标题*/
@property (nonatomic, strong) UILabel *titleLabel;
/**背景视图*/
@property (nonatomic, strong) UIView *backView;
@property (nonatomic, strong) UIImageView *backImageView;

@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) NSMutableArray *dataSource;

@end

@implementation QJMineViewCell

#pragma mark - init
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self loadUI];
    }
    return self;
}

#pragma mark - Load UI
- (void)loadUI {
    [self.contentView addSubview:self.backView];
    [self.backView addSubview:self.backImageView];
    [self.backView addSubview:self.titleLabel];
    [self.backView addSubview:self.collectionView];
    [self makeConstraints];
}

- (void)makeConstraints {
    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).offset(16);
        make.left.equalTo(self.mas_left).offset(16);
        make.right.equalTo(self.mas_right).offset(-16);
        make.height.mas_equalTo(120*kWScale);
    }];
    
    [self.backImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.mas_equalTo(-8);
        make.right.bottom.mas_equalTo(8);
    }];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.backView.mas_left).offset(16);
        make.top.equalTo(self.backView.mas_top).offset(10);
    }];
    
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(self.titleLabel.mas_bottom).mas_offset(10);
        make.left.right.mas_equalTo(0);
        make.bottom.mas_equalTo(0);
    }];
    
}

#pragma mark - Lazy Loading

- (UIView *)backView {
    if (!_backView) {
        self.backView = [UIView new];
        self.backView.backgroundColor = [UIColor clearColor];
    }
    return _backView;
}

- (UIImageView *)backImageView {
    if (!_backImageView) {
        _backImageView = [[UIImageView alloc] init];
        _backImageView.image = [UIImage imageNamed:@"mine_more_bg"];
        _backImageView.contentMode = UIViewContentModeScaleToFill;
    }
    return _backImageView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        self.titleLabel = [UILabel new];
        self.titleLabel.textColor = [UIColor blackColor];
        self.titleLabel.font = FontSemibold(14);
        self.titleLabel.text = @"更多";
    }
    return _titleLabel;
}

- (NSMutableArray *)dataSource {
    if (!_dataSource) {
        _dataSource = [NSMutableArray arrayWithArray:@[
                       @{@"text":@"我的足迹",@"img":@"mine_footprint"},
                       @{@"text":@"实名认证",@"img":@"mine_ren"},
                       @{@"text":@"收货地址",@"img":@"mine_address"},
                       @{@"text":@"帮助中心",@"img":@"mine_help"},
                       @{@"text":@"在线客服",@"img":@"mine_kefu"},
//                       @{@"text":@"我的荣耀",@"img":@"mine_honor"}
        ]];
    };
    return _dataSource;
}

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        QJMineFlowLayout *layout = [[QJMineFlowLayout alloc] init];
        layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        layout.itemSize = CGSizeMake((kScreenWidth-32)/5, 60*kWScale);
        layout.minimumLineSpacing = 0;
        layout.minimumInteritemSpacing = 0;
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.backgroundColor = [UIColor clearColor];
        [_collectionView registerClass:[QJMineHomeCollectionViewCell class] forCellWithReuseIdentifier:@"QJMineHomeCollectionViewCell"];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.showsHorizontalScrollIndicator = NO;
        //collectionView的item只剩下一个时自动左对齐
        SEL sel = NSSelectorFromString(@"_setRowAlignmentsOptions:");
        if ([_collectionView.collectionViewLayout respondsToSelector:sel]) {
            ((void(*)(id,SEL,NSDictionary*)) objc_msgSend)(_collectionView.collectionViewLayout,sel, @{@"UIFlowLayoutCommonRowHorizontalAlignmentKey":@(NSTextAlignmentLeft),@"UIFlowLayoutLastRowHorizontalAlignmentKey" : @(NSTextAlignmentLeft),@"UIFlowLayoutRowVerticalAlignmentKey" : @(NSTextAlignmentCenter)});
        }
    }
    return _collectionView;
}

#pragma mark - collection
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataSource.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    QJMineHomeCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"QJMineHomeCollectionViewCell" forIndexPath:indexPath];
    NSDictionary *dic = self.dataSource[indexPath.row];
    cell.headImageView.image = [UIImage imageNamed:dic[@"img"]];
    cell.titleLabel.text = dic[@"text"];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.clickButton) {
        self.clickButton(indexPath.row);
    }
}

@end
