//
//  QJMineFansTableViewCell.m
//  QJBox
//
//  Created by macm on 2022/7/19.
//

#import "QJMineFansTableViewCell.h"

@interface QJMineFansTableViewCell ()
@property (nonatomic, strong) UIImageView *accountImageView;
@property (nonatomic, strong) UILabel *accountLabel;
@property (nonatomic, strong) UILabel *contentLabel;
@property (nonatomic, strong) UIView *lineView;

@end

@implementation QJMineFansTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self loadUI];
    }
    return self;
}

#pragma mark - Load UI
- (void)loadUI {
    [self.contentView addSubview:self.accountImageView];
    [self.accountImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.width.height.mas_equalTo(44*kWScale);
        make.centerY.mas_equalTo(self.contentView);
    }];
    
    [self.contentView addSubview:self.accountLabel];
    [self.accountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.accountImageView.mas_right).mas_offset(6);
        make.top.mas_equalTo(self.accountImageView);
    }];
    
    [self.contentView addSubview:self.contentLabel];
    [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.accountImageView.mas_right).mas_offset(6);
        make.right.mas_equalTo(-15);
        make.bottom.mas_equalTo(self.accountImageView).mas_offset(-3);
    }];
    
    [self.contentView addSubview:self.lineView];
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(60);
        make.right.mas_equalTo(-15);
        make.bottom.mas_equalTo(0);
        make.height.mas_equalTo(1);
    }];
    
    
}

- (void)setModel:(QJMineFansModel *)model {
    _model = model;
    [self.accountImageView setImageWithURL:[NSURL URLWithString:[model.coverImage checkImageUrlString]] placeholder:[UIImage imageNamed:@"mine_head"]];
    self.accountLabel.text = _model.nickname;
    self.contentLabel.text = _model.signature;
}

- (UIImageView *)accountImageView {
    if (!_accountImageView) {
        _accountImageView = [UIImageView new];
        _accountImageView.image = [UIImage imageNamed:@"mine_head"];
        _accountImageView.layer.cornerRadius = 22*kWScale;
        _accountImageView.layer.masksToBounds = YES;
    }
    return _accountImageView;
}

- (UILabel *)accountLabel {
    if (!_accountLabel) {
        _accountLabel = [UILabel new];
        _accountLabel.text = @"钱萌萌";
        _accountLabel.font = FontRegular(14);
        _accountLabel.textColor = UIColorHex(16191c);
    }
    return _accountLabel;
}

- (UILabel *)contentLabel {
    if (!_contentLabel) {
        _contentLabel = [UILabel new];
        _contentLabel.text = @"得他声才也我形高对大那级，行及上亮取戏展总，把亚变！要局死地？不方部拿学合的能产对同城汽该打成现时，究明在小没得山至，任报城给保五的红高不我色行我，证活联，北和足";
        _contentLabel.font = FontRegular(12);
        _contentLabel.textColor = UIColorHex(919599);
    }
    return _contentLabel;
}

- (UIView *)lineView {
    if (!_lineView) {
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor = UIColorFromRGB(0xebedf0);
    }
    return _lineView;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
