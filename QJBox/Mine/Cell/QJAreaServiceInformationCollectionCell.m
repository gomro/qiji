//
//  QJAreaServiceInformationCollectionCell.m
//  QJBox
//
//  Created by wxy on 2022/8/16.
//

#import "QJAreaServiceInformationCollectionCell.h"


@interface QJAreaServiceInformationCollectionCell ()



@end

@implementation QJAreaServiceInformationCollectionCell


- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.contentView.backgroundColor = kColorWithHexString(@"#EBEDF0");
        [self.contentView addSubview:self.titleLabel];
        self.contentView.layer.cornerRadius = 4;
        [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(self.contentView);
            make.left.equalTo(self.contentView).offset(17);
            make.right.equalTo(self.contentView).offset(-17);
            make.height.equalTo(@22);
            
        }];
    }
    return self;
}



 //#F5CE70
- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.font = FontMedium(12);
        _titleLabel.textColor = [UIColor blackColor];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        
    }
    return _titleLabel;
}

@end
