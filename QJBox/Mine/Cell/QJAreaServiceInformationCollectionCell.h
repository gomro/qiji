//
//  QJAreaServiceInformationCollectionCell.h
//  QJBox
//
//  Created by wxy on 2022/8/16.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJAreaServiceInformationCollectionCell : UICollectionViewCell


@property (nonatomic, strong) UILabel *titleLabel;

@end

NS_ASSUME_NONNULL_END
