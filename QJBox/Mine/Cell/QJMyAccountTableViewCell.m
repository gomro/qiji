//
//  QJMyAccountTableViewCell.m
//  QJBox
//
//  Created by macm on 2022/7/18.
//

#import "QJMyAccountTableViewCell.h"

@interface QJMyAccountTableViewCell ()
@property (nonatomic, strong) UIView *accountView;
@property (nonatomic, strong) UIImageView *topImageView;
@property (nonatomic, strong) UIImageView *bottomImageView;
@property (nonatomic, strong) UIImageView *accountImageView;
@property (nonatomic, strong) UILabel *accountLabel;
@property (nonatomic, strong) UIButton *accountButton;

@property (nonatomic, strong) UIImageView *qjbImageView;
@property (nonatomic, strong) UILabel *qjbLabel;
@property (nonatomic, strong) UILabel *qjbNumLabel;

@property (nonatomic, strong) UIButton *taskButton;
@property (nonatomic, strong) UIButton *changeButton;
@property (nonatomic, strong) UIButton *dealButton;

@end

@implementation QJMyAccountTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self loadUI];
    }
    return self;
}

- (void)loadUI {
    [self.contentView addSubview:self.accountView];
    [self.accountView addSubview:self.topImageView];
    [self.accountView addSubview:self.bottomImageView];
    [self.accountView addSubview:self.accountImageView];
    [self.accountView addSubview:self.accountLabel];
    [self.accountView addSubview:self.qjbImageView];
    [self.accountView addSubview:self.qjbLabel];
    [self.accountView addSubview:self.qjbNumLabel];
    [self.accountView addSubview:self.accountButton];

    [self.contentView addSubview:self.taskButton];
    [self.contentView addSubview:self.changeButton];
    [self.contentView addSubview:self.dealButton];

    [self makeConstraints];
}

- (void)makeConstraints {
    [self.accountView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(self.contentView).mas_offset(0);
        make.height.mas_equalTo(130*kWScale);
    }];
    
    [self.topImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(15);
        make.left.mas_equalTo(16);
        make.right.mas_equalTo(-16);
        make.height.mas_equalTo(36*kWScale);
    }];
    
    [self.bottomImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.topImageView.mas_bottom).mas_offset(0);
        make.left.mas_equalTo(8);
        make.right.mas_equalTo(-8);
        make.bottom.mas_equalTo(0);
    }];
    
    [self.accountButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.left.right.equalTo(self.accountView);
    }];
    
    [self.accountImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(35);
        make.centerY.mas_equalTo(self.topImageView);
        make.width.height.mas_equalTo(16*kWScale);
    }];
    
    [self.accountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.accountImageView.mas_right).mas_offset(6);
        make.centerY.mas_equalTo(self.accountImageView);
    }];

    [self.qjbImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(40);
        make.centerY.mas_equalTo(self.bottomImageView).mas_offset(-5);
        make.width.height.mas_equalTo(12*kWScale);
    }];

    [self.qjbLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.qjbImageView.mas_right).mas_offset(6);
        make.centerY.mas_equalTo(self.qjbImageView);
    }];

    [self.qjbNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-50);
        make.centerY.mas_equalTo(self.qjbImageView);
    }];

    NSArray *viewArray = @[self.taskButton, self.changeButton, self.dealButton];
    [viewArray mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedSpacing:10 leadSpacing:16 tailSpacing:16];
    [viewArray mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.contentView);
        make.height.equalTo(@(75*kWScale));
    }];
}

- (void)setModel:(QJMineUserModel *)model {
    _model = model;
    self.qjbNumLabel.text = [NSString stringWithFormat:@"%ld",_model.qjcoinCurrent];
}

- (void)taskAction {
    if (_clickButton) {
        _clickButton(QJMineViewCellStyleTask);
    }
}

- (void)changeAction {
    if (_clickButton) {
        _clickButton(QJMineViewCellStyleChange);
    }
}

- (void)dealAction {
    if (_clickButton) {
        _clickButton(QJMineViewCellStyleDeal);
    }
}

- (void)accountAction {    
    if (_clickButton) {
        _clickButton(QJMineViewCellStyleAccount);
    }
}

#pragma mark - Lazy Loading

- (UIView *)accountView {
    if (!_accountView) {
        _accountView = [[UIView alloc] init];
        _accountView.backgroundColor = [UIColor clearColor];
        _accountView.layer.cornerRadius = 8;
        _accountView.layer.masksToBounds = YES;
    }
    return _accountView;
}

- (UIImageView *)topImageView {
    if (!_topImageView) {
        _topImageView = [UIImageView new];
        _topImageView.image = [UIImage imageNamed:@"mine_acc_head_bg"];
        _topImageView.contentMode = UIViewContentModeScaleToFill;
    }
    return _topImageView;
}

- (UIImageView *)bottomImageView {
    if (!_bottomImageView) {
        _bottomImageView = [UIImageView new];
        _bottomImageView.image = [UIImage imageNamed:@"mine_acc_bg"];
        _bottomImageView.contentMode = UIViewContentModeScaleToFill;
    }
    return _bottomImageView;
}

- (UIButton *)accountButton {
    if (!_accountButton) {
        _accountButton = [UIButton new];
        [_accountButton addTarget:self action:@selector(accountAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _accountButton;
}

- (UIImageView *)accountImageView {
    if (!_accountImageView) {
        _accountImageView = [UIImageView new];
        _accountImageView.image = [UIImage imageNamed:@"mine_wallet"];
    }
    return _accountImageView;
}

- (UILabel *)accountLabel {
    if (!_accountLabel) {
        _accountLabel = [UILabel new];
        _accountLabel.text = @"我的账户";
        _accountLabel.font = FontSemibold(12);
        _accountLabel.textColor = UIColorHex(723318);
    }
    return _accountLabel;
}

- (UIImageView *)qjbImageView {
    if (!_qjbImageView) {
        _qjbImageView = [UIImageView new];
        _qjbImageView.image = [UIImage imageNamed:@"mine_qjb"];
    }
    return _qjbImageView;
}

- (UILabel *)qjbLabel {
    if (!_qjbLabel) {
        _qjbLabel = [UILabel new];
        _qjbLabel.text = @"奇迹币";
        _qjbLabel.font = FontSemibold(12);
        _qjbLabel.textColor = UIColorHex(16191c);
    }
    return _qjbLabel;
}

- (UILabel *)qjbNumLabel {
    if (!_qjbNumLabel) {
        _qjbNumLabel = [UILabel new];
        _qjbNumLabel.text = @"0";
        _qjbNumLabel.font = FontSemibold(14);
        _qjbNumLabel.textColor = UIColorHex(e85838);
        _qjbNumLabel.textAlignment = NSTextAlignmentRight;
    }
    return _qjbNumLabel;
}

- (UIButton *)taskButton {
    if (!_taskButton) {
        _taskButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_taskButton setImage:[UIImage imageNamed:@"mine_task_text"] forState:UIControlStateNormal];
        [_taskButton setBackgroundImage:[UIImage imageNamed:@"mine_task_bg"] forState:UIControlStateNormal];
        [_taskButton addTarget:self action:@selector(taskAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _taskButton;
}

- (UIButton *)changeButton {
    if (!_changeButton) {
        _changeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_changeButton setImage:[UIImage imageNamed:@"mine_change_text"] forState:UIControlStateNormal];
        [_changeButton setBackgroundImage:[UIImage imageNamed:@"mine_change_bg"] forState:UIControlStateNormal];
        [_changeButton addTarget:self action:@selector(changeAction) forControlEvents:UIControlEventTouchUpInside];

    }
    return _changeButton;
}

- (UIButton *)dealButton {
    if (!_dealButton) {
        _dealButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_dealButton setImage:[UIImage imageNamed:@"mine_deal_text"] forState:UIControlStateNormal];
        [_dealButton setBackgroundImage:[UIImage imageNamed:@"mine_deal_bg"] forState:UIControlStateNormal];
        [_dealButton addTarget:self action:@selector(dealAction) forControlEvents:UIControlEventTouchUpInside];

    }
    return _dealButton;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
