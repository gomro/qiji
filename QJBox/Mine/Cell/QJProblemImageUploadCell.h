//
//  QJProblemImageUploadCell.h
//  QJBox
//
//  Created by wxy on 2022/8/16.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/// 问题截图上传
@interface QJProblemImageUploadCell : UITableViewCell


@property (nonatomic, strong) UILabel *topLabel;

@property (nonatomic, strong) NSMutableArray *array;

@property (nonatomic, copy) void(^selectedImages)(NSArray *images);
@end

NS_ASSUME_NONNULL_END
