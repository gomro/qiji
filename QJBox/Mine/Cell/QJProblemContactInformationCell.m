//
//  QJProblemContactInformationCell.m
//  QJBox
//
//  Created by wxy on 2022/8/17.
//

#import "QJProblemContactInformationCell.h"


@interface QJProblemContactInformationCell ()<UITextFieldDelegate>


@property (nonatomic, strong) UILabel *topLabel;

@property (nonatomic, strong) UILabel *firstTitleLabel;

@property (nonatomic, strong) UITextField *firstTextField;

@property (nonatomic, strong) UILabel *secTitleLabel;

@property (nonatomic, strong) UITextField *secTextField;


@end



@implementation QJProblemContactInformationCell

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
 
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
         
        self.contentView.backgroundColor = [UIColor whiteColor];
        
        [self.contentView addSubview:self.topLabel];
        [self.contentView addSubview:self.firstTitleLabel];
        [self.contentView addSubview:self.firstTextField];
        [self.contentView addSubview:self.secTitleLabel];
        [self.contentView addSubview:self.secTextField];
        
        [self.topLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.equalTo(self.contentView).offset(16);
                    make.top.equalTo(self.contentView).offset(16);
                    make.right.equalTo(self.contentView).offset(-16);
        }];
        
        
        [self.firstTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.equalTo(self.contentView).offset(16);
                    make.right.equalTo(self.contentView).offset(-16);
                    make.top.equalTo(self.topLabel.mas_bottom).offset(17);
                    make.height.equalTo(@(14*kWScale));
        }];
        
        [self.firstTextField mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.right.left.equalTo(self.firstTitleLabel);
                    make.top.equalTo(self.firstTitleLabel.mas_bottom).offset(8);
                    make.height.equalTo(@(26*kWScale));
        }];
        
        [self.secTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.right.left.equalTo(self.firstTextField);
                    make.top.equalTo(self.firstTextField.mas_bottom).offset(16);
                    make.height.equalTo(self.firstTitleLabel);
        }];
        
        [self.secTextField mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.right.left.equalTo(self.firstTitleLabel);
                    make.top.equalTo(self.secTitleLabel.mas_bottom).offset(8);
                    make.height.equalTo(@(26*kWScale));
        }];
        
    }
    return self;
}




#pragma  mark  ---------- getter



- (UILabel *)topLabel {
    if (!_topLabel) {
        _topLabel = [UILabel new];
        _topLabel.text = @"联系方式";
        _topLabel.textAlignment = NSTextAlignmentLeft;
        _topLabel.font = FontSemibold(14);
    }
    return _topLabel;
}


- (UITextField *)firstTextField {
    if (!_firstTextField) {
        _firstTextField = [UITextField new];
        _firstTextField.backgroundColor = kColorWithHexString(@"#F5F5F5");
        NSAttributedString *str = [[NSAttributedString alloc]initWithString:@"请输入您QQ号" attributes:@{NSFontAttributeName :FontRegular(14),NSForegroundColorAttributeName:kColorWithHexString(@"#C8CACC")}];
        _firstTextField.attributedPlaceholder = str;
        _firstTextField.font = FontRegular(14);
        _firstTextField.delegate = self;
        _firstTextField.textColor = kColorWithHexString(@"#16191C");
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldDidChangeValue:) name:UITextFieldTextDidChangeNotification object:_firstTextField];
    }
    return _firstTextField;
}


- (UILabel *)firstTitleLabel {
    if (!_firstTitleLabel) {
        _firstTitleLabel = [UILabel new];
        _firstTitleLabel.font = FontSemibold(14);
        _firstTitleLabel.text = @"QQ:";
        _firstTitleLabel.textAlignment = NSTextAlignmentLeft;
        _firstTitleLabel.textColor = [UIColor blackColor];
    }
    return _firstTitleLabel;
}

- (UILabel *)secTitleLabel {
    if (!_secTitleLabel) {
        _secTitleLabel = [UILabel new];
        _secTitleLabel.font = FontSemibold(14);
        _secTitleLabel.text = @"微信:";
        _secTitleLabel.textAlignment = NSTextAlignmentLeft;
        _secTitleLabel.textColor = [UIColor blackColor];
    }
    return _secTitleLabel;
}

- (UITextField *)secTextField {
    if (!_secTextField) {
        _secTextField = [UITextField new];
        _secTextField.backgroundColor = kColorWithHexString(@"#F5F5F5");
        NSAttributedString *str = [[NSAttributedString alloc]initWithString:@"请输入您微信号" attributes:@{NSFontAttributeName :FontRegular(14),NSForegroundColorAttributeName:kColorWithHexString(@"#C8CACC")}];
        _secTextField.attributedPlaceholder = str;
        _secTextField.font = FontRegular(14);
        _secTextField.textColor = kColorWithHexString(@"#16191C");
        _secTextField.delegate = self;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldDidChangeValue:) name:UITextFieldTextDidChangeNotification object:_secTextField];
    }
    return _secTextField;
}

- (void)textFieldDidChangeValue:(NSNotification *)notification {
 
    UITextField *sender = (UITextField *)[notification object];
    if (sender == self.firstTextField) {
        if (self.editFirstBlock) {
            self.editFirstBlock(sender.text);
        }
    }
    if (sender == self.secTextField) {
        if (self.editSecBlock) {
            self.editSecBlock(sender.text);
        }
    }
    
}
@end
