//
//  HZPhotoBrowser.m
//  photobrowser
//
//  Created by huangzhenyu on 15-2-3.
//  Copyright (c) 2015年 huangzhenyu. All rights reserved.
//

#import "HZPhotoBrowser.h"
#import "HZPhotoBrowserView.h"
#import "HZPhotoBrowserConfig.h"

#import <AssetsLibrary/AssetsLibrary.h>

@interface HZPhotoBrowser() <UIAlertViewDelegate>
@property (nonatomic,strong) UITapGestureRecognizer *singleTap;
@property (nonatomic,strong) UITapGestureRecognizer *doubleTap;
@property (nonatomic,strong) UIPanGestureRecognizer *pan;
@property (nonatomic, strong) UILongPressGestureRecognizer *longPress;
@property (nonatomic,strong) UIImageView *tempView;
@property (nonatomic,strong) UIView *coverView;
@property (nonatomic,strong) HZPhotoBrowserView *photoBrowserView;
@property (nonatomic,assign) UIDeviceOrientation orientation;
@property (nonatomic,assign) HZPhotoBrowserStyle photoBrowserStyle;
@end
@implementation HZPhotoBrowser 
{
    UIScrollView *_scrollView;
    BOOL _hasShowedFistView;
    UILabel *_indexLabel;
    UIView *_contentView;
}

#pragma mark recyle
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = HZPhotoBrowserBackgrounColor;
        self.isSavePhoto = NO;
        self.isFullWidthForLandScape = YES;
        self.isNeedLandscape = YES;
        self.imageUrlHeader = @{};
    }
    return self;
}

//当视图移动完成后调用
- (void)didMoveToSuperview
{
    [super didMoveToSuperview];
    
    //处理下标可能越界的bug
    _currentImageIndex = _currentImageIndex < 0 ? 0 : _currentImageIndex;
    NSInteger count = _imageCount - 1;
    if (count > 0) {
        if (_currentImageIndex > count) {
            _currentImageIndex = 0;
        }
    }
    [self setupScrollView];
    [self setupToolbars];
//    [self addGestureRecognizer:self.singleTap];
    [self addGestureRecognizer:self.doubleTap];
    [self addGestureRecognizer:self.pan];
//    [self addGestureRecognizer:self.longPress];
    self.photoBrowserView = _scrollView.subviews[self.currentImageIndex];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGRect rect = self.bounds;
    rect.size.width += HZPhotoBrowserImageViewMargin * 2;
    _scrollView.bounds = rect;
    _scrollView.center = CGPointMake(self.bounds.size.width *0.5, self.bounds.size.height *0.5);
    
    CGFloat y = 0;
    __block CGFloat w = _scrollView.frame.size.width - HZPhotoBrowserImageViewMargin * 2;
    CGFloat h = _scrollView.frame.size.height;
    [_scrollView.subviews enumerateObjectsUsingBlock:^(HZPhotoBrowserView *obj, NSUInteger idx, BOOL *stop) {
        CGFloat x = HZPhotoBrowserImageViewMargin + idx * (HZPhotoBrowserImageViewMargin * 2 + w);
        obj.frame = CGRectMake(x, y, w, h);
    }];
    
    _scrollView.contentSize = CGSizeMake(_scrollView.subviews.count * _scrollView.frame.size.width, _scrollView.frame.size.height);
    _scrollView.contentOffset = CGPointMake(self.currentImageIndex * _scrollView.frame.size.width, 0);
    
    
    if (!_hasShowedFistView) {
        [self showFirstImage];
    }
    _indexLabel.bounds = CGRectMake(0, kScreenHeight -40 -(iPhoneX?34:0), kScreenWidth, 20);
}

- (void)dealloc
{
//    NSLog(@"图片浏览器销毁");
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)setSourceImagesContainerView:(UIView *)sourceImagesContainerView{
    _sourceImagesContainerView = sourceImagesContainerView;
    _imageArray = nil;
    _photoBrowserStyle = HZPhotoBrowserStyleDefault;
}

- (void)setImageArray:(NSArray *)imageArray{
    _imageArray = imageArray;
    _imageCount = imageArray.count;
    _sourceImagesContainerView = nil;
    _photoBrowserStyle = HZPhotoBrowserStyleSimple;
}

#pragma mark getter settter
- (UITapGestureRecognizer *)singleTap{
    if (!_singleTap) {
        _singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(photoClick:)];
        _singleTap.numberOfTapsRequired = 1;
//        _singleTap.numberOfTouchesRequired = 1;
        _singleTap.delaysTouchesBegan = YES;
        //只能有一个手势存在
        [_singleTap requireGestureRecognizerToFail:self.doubleTap];
    }
    return _singleTap;
}

- (UITapGestureRecognizer *)doubleTap
{
    if (!_doubleTap) {
        _doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTap:)];
        _doubleTap.numberOfTapsRequired = 2;
//        _doubleTap.numberOfTouchesRequired = 1;
    }
    return _doubleTap;
}

- (UIPanGestureRecognizer *)pan{
    if (!_pan) {
        _pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(didPan:)];
    }
    return _pan;
}

-(UILongPressGestureRecognizer *)longPress
{
    if (!_longPress) {
        _longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(imglongTapClick:)];
        _longPress.minimumPressDuration = 0.5f;//设置长按 时间
        _longPress.numberOfTouchesRequired = 1;
    }
    return _longPress;
}

- (UIImageView *)tempView{
    if (!_tempView) {
        HZPhotoBrowserView *photoBrowserView = _scrollView.subviews[self.currentImageIndex];
        UIImageView *currentImageView = photoBrowserView.imageview;
        CGFloat tempImageX = currentImageView.frame.origin.x - photoBrowserView.scrollOffset.x;
        CGFloat tempImageY = currentImageView.frame.origin.y - photoBrowserView.scrollOffset.y;
        
        CGFloat tempImageW = photoBrowserView.zoomImageSize.width;
        CGFloat tempImageH = photoBrowserView.zoomImageSize.height;
        UIDeviceOrientation orientation = [UIDevice currentDevice].orientation;
        if (UIDeviceOrientationIsLandscape(orientation)) {//横屏
            
            //处理长图,图片太长会导致旋转动画飞掉
            if (tempImageH > KAppHeight) {
                tempImageH = tempImageH > (tempImageW * 1.5)? (tempImageW * 1.5):tempImageH;
                if (fabs(tempImageY) > tempImageH) {
                    tempImageY = 0;
                }
            }

        }
        
        _tempView = [[UIImageView alloc] init];
        //这边的contentmode要跟 HZPhotoGrop里面的按钮的 contentmode保持一致（防止最后出现闪动的动画）
        _tempView.contentMode = UIViewContentModeScaleAspectFill;
        _tempView.clipsToBounds = YES;
        _tempView.frame = CGRectMake(tempImageX, tempImageY, tempImageW, tempImageH);
        _tempView.image = currentImageView.image;
    }
    return _tempView;
}

//做颜色渐变动画的view，让退出动画更加柔和
- (UIView *)coverView{
    if (!_coverView) {
        _coverView = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        _coverView.backgroundColor = HZPhotoBrowserBackgrounColor;
    }
    return _coverView;
}

- (void)setPhotoBrowserView:(HZPhotoBrowserView *)photoBrowserView{
    _photoBrowserView = photoBrowserView;
    __weak typeof(self) weakSelf = self;
    _photoBrowserView.scrollViewWillEndDragging = ^(CGPoint velocity,CGPoint offset) {
        __strong typeof(weakSelf) strongSelf = weakSelf;
//        NSLog(@"%f  %f",velocity.y,offset.y);
        if (((velocity.y < -2 && offset.y < 0) || offset.y < -100)) {
            [strongSelf hidePhotoBrowser];
            NSLog(@"将要结束拖拽 -- 退出");
        }
    };
}

- (void)setCurrentImageIndex:(int)currentImageIndex{
//    _currentImageIndex = currentImageIndex;
    _currentImageIndex = currentImageIndex < 0 ? 0 : currentImageIndex;
    NSInteger count0 = _imageCount;
    NSInteger count1 = _imageArray.count;
    if (count0 > 0) {
        if (_currentImageIndex > count0) {
            _currentImageIndex = 0;
        }
    }
    if (count1 > 0) {
        if (_currentImageIndex > count1) {
            _currentImageIndex = 0;
        }
    }
}

#pragma mark private methods
- (void)setupToolbars {
    //返回
    UIButton *backBt = [[UIButton alloc] initWithFrame:CGRectMake(10,  20 + Top_iPhoneX_SPACE, 50, 50)];
    [backBt setImage:[UIImage imageNamed:@"photoBackIcon"] forState:UIControlStateNormal];
    [backBt addTarget:self action:@selector(backView:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:backBt];
    
    UIButton *downBt = [[UIButton alloc] initWithFrame:CGRectMake(kScreenWidth - 60,  20 + Top_iPhoneX_SPACE, 50, 50)];
    [downBt setImage:[UIImage imageNamed:@"downPicture"] forState:UIControlStateNormal];
    [downBt addTarget:self action:@selector(downView:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:downBt];
    
    // 1. 序标
    UILabel *indexLabel = [[UILabel alloc] init];
    indexLabel.textAlignment = NSTextAlignmentCenter;
    indexLabel.textColor = [UIColor whiteColor];
    indexLabel.font = MYFONTALL(FONT_REGULAR, 14);
    indexLabel.frame = CGRectMake(0, kScreenHeight - 16 - Bottom_iPhoneX_SPACE, kScreenWidth, 20);
    indexLabel.text = [NSString stringWithFormat:@"1/%ld", (long)self.imageCount];
    _indexLabel = indexLabel;
    [self addSubview:indexLabel];
}

- (void)backView:(UIButton *)sender {
    sender.hidden = YES;
    [self hidePhotoBrowser];
}

- (void)downView:(UIButton *)sender {

        [self judgePhotoAlbumPermission];
}


- (void) setupScrollView
{
    _scrollView = [[UIScrollView alloc] init];
    _scrollView.delegate = self;
    _scrollView.showsHorizontalScrollIndicator = NO;
    _scrollView.showsVerticalScrollIndicator = NO;
    _scrollView.pagingEnabled = YES;
    [self addSubview:_scrollView];
    
    for (int i = 0; i < self.imageCount; i++) {
        HZPhotoBrowserView *view = [[HZPhotoBrowserView alloc] init];
        view.isFullWidthForLandScape = self.isFullWidthForLandScape;
        view.imageview.tag = i;
        [_scrollView addSubview:view];
    }
    [self setupImageOfImageViewForIndex:self.currentImageIndex];
    
}

// 加载图片
- (void)setupImageOfImageViewForIndex:(NSInteger)index
{
    HZPhotoBrowserView *view = _scrollView.subviews[index];
    if (view.beginLoadingImage) return;
    if ([self highQualityImageURLForIndex:index]) {
        view.imageUrlHeader = self.imageUrlHeader;
        [view setImageWithURL:[self highQualityImageURLForIndex:index] placeholderImage:[self placeholderImageForIndex:index]];
    } else {
        view.imageview.image = [self placeholderImageForIndex:index];
    }
    view.beginLoadingImage = YES;
}

- (void)onDeviceOrientationChangeWithObserver
{
    [self onDeviceOrientationChange];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onDeviceOrientationChange) name:UIDeviceOrientationDidChangeNotification object:nil];
}

-(void)onDeviceOrientationChange
{
    if (!self.isNeedLandscape) {
        return;
    }
    
    HZPhotoBrowserView *currentView = _scrollView.subviews[self.currentImageIndex];
    UIDeviceOrientation orientation = [UIDevice currentDevice].orientation;
    self.orientation = orientation;
    if (UIDeviceOrientationIsLandscape(orientation)) {
        if (self.bounds.size.width < self.bounds.size.height) {
            NSLog(@"横屏 还原");
            [currentView.scrollview setZoomScale:1.0 animated:YES];//还原
            [self cancelPrepareForHide];
            [UIView animateWithDuration:kRotateAnimationDuration delay:0.0f options:UIViewAnimationOptionBeginFromCurrentState animations:^{
                self.transform = (orientation==UIDeviceOrientationLandscapeRight)?CGAffineTransformMakeRotation(M_PI*1.5):CGAffineTransformMakeRotation(M_PI/2);
                if (iPhoneX) {
                    self.center = [UIApplication sharedApplication].keyWindow.center;
                    self.bounds = CGRectMake(0, 0,  KAppHeight - kStatusBar_Height - kBottomSafeHeight, kAPPWidth);
                } else {
                    self.bounds = CGRectMake(0, 0, KAppHeight, kAPPWidth);
                }
                [self setNeedsLayout];
                [self layoutIfNeeded];
            } completion:^(BOOL finished) {
            
            }];
        }
    }else if (orientation==UIDeviceOrientationPortrait){
        if (self.bounds.size.width > self.bounds.size.height) {
            NSLog(@"竖屏 还原");
            [currentView.scrollview setZoomScale:1.0 animated:YES];//还原
            [UIView animateWithDuration:kRotateAnimationDuration delay:0.0f options:UIViewAnimationOptionBeginFromCurrentState animations:^{
                self.transform = (orientation==UIDeviceOrientationPortrait)?CGAffineTransformIdentity:CGAffineTransformMakeRotation(M_PI);
                if (iPhoneX) {
                    self.bounds = CGRectMake(0, 0, kAPPWidth, KAppHeight - kStatusBar_Height - kBottomSafeHeight);
                } else {
                    self.bounds = CGRectMake(0, 0, kAPPWidth, KAppHeight);
                }
                
                [self setNeedsLayout];
                [self layoutIfNeeded];
            } completion:^(BOOL finished) {
                
            }];
        }
    }
}

- (void)showFirstImage
{
    if (_photoBrowserStyle == HZPhotoBrowserStyleDefault) {
        UIView *sourceView = self.sourceImagesContainerView.subviews[self.currentImageIndex];
        CGRect rect = [self.sourceImagesContainerView convertRect:sourceView.frame toView:self];
        UIImageView *tempView = [[UIImageView alloc] init];
        tempView.frame = rect;
        tempView.image = [self placeholderImageForIndex:self.currentImageIndex];
        [self addSubview:tempView];
//        tempView.contentMode = UIViewContentModeScaleAspectF;
        
        CGFloat placeImageSizeW = tempView.image.size.width;
        CGFloat placeImageSizeH = tempView.image.size.height;
        if (placeImageSizeW == 0) {
            placeImageSizeW = kScreenWidth;
            placeImageSizeH = kScreenHeight;
        }
        
        CGRect targetTemp;
        CGFloat selfW = self.frame.size.width;
        CGFloat selfH = self.frame.size.height;
        
        CGFloat placeHolderH = (placeImageSizeH * selfW)/placeImageSizeW;
        if (placeHolderH <= selfH) {
            targetTemp = CGRectMake(0, (selfH - placeHolderH) * 0.5 , selfW, placeHolderH);
        } else {//图片高度>屏幕高度
            targetTemp = CGRectMake(0, 0, selfW, placeHolderH);
        }
        //先隐藏scrollview
        _scrollView.hidden = YES;
        _indexLabel.hidden = YES;
        self.userInteractionEnabled = NO;
        [UIView animateWithDuration:HZPhotoBrowserShowImageAnimationDuration animations:^{
            //将点击的临时imageview动画放大到和目标imageview一样大
            tempView.frame = targetTemp;
        } completion:^(BOOL finished) {
            self.userInteractionEnabled = YES;
            //动画完成后，删除临时imageview，让目标imageview显示
            _hasShowedFistView = YES;
            [tempView removeFromSuperview];
            _scrollView.hidden = NO;
            _indexLabel.hidden = NO;
        }];
    } else {
//        HZPhotoBrowserShowImageAnimationDuration
        _photoBrowserView.alpha = 0;
        _contentView.alpha = 0;
        [UIView animateWithDuration:0.2 animations:^{
            //将点击的临时imageview动画放大到和目标imageview一样大
            _photoBrowserView.alpha = 1;
            _contentView.alpha = 1;
        } completion:^(BOOL finished) {
            _hasShowedFistView = YES;
        }];
    }
}

- (UIImage *)placeholderImageForIndex:(NSInteger)index
{
    if (_photoBrowserStyle == HZPhotoBrowserStyleDefault) {
        if ([self.delegate respondsToSelector:@selector(photoBrowser:placeholderImageForIndex:)]) {
            return [self.delegate photoBrowser:self placeholderImageForIndex:index];
        }
    } else {
        return nil;
    }
    
    return nil;
}

- (NSURL *)highQualityImageURLForIndex:(NSInteger)index
{
    if (_photoBrowserStyle == HZPhotoBrowserStyleDefault) {
        if ([self.delegate respondsToSelector:@selector(photoBrowser:highQualityImageURLForIndex:)]) {
            return [self.delegate photoBrowser:self highQualityImageURLForIndex:index];
        }
    } else {
        return [NSURL URLWithString:_imageArray[index]];
    }
    return nil;
}


- (void)hidePhotoBrowser
{
    [self prepareForHide];
    if (self.photoBrowserView.imageview.frame.size.height >= kScreenHeight) {
        _tempView.frame = CGRectMake(0, 0, _tempView.frame.size.width, _tempView.frame.size.height);
    }
    [self hideAnimation];
}

- (void)hideAnimation{
//    HZPhotoBrowserView *photoBrowserView = _scrollView.subviews[self.currentImageIndex];
//    UIImageView *currentImageView = photoBrowserView.imageview;
//    NSUInteger currentIndex = currentImageView.tag;
    CGRect targetTemp;
    UIWindow *window = [UIApplication sharedApplication].keyWindow;

    UIView *sourceView = [self getSourceView];
    if (!sourceView) {
        targetTemp = CGRectMake(window.center.x, window.center.y, 0, 0);
    }
    if (_photoBrowserStyle == HZPhotoBrowserStyleDefault) {
        UIView *sourceView = [self getSourceView];
        targetTemp = [self.sourceImagesContainerView convertRect:sourceView.frame toView:self];
    } else {
        //普通动画效果，图片不会回到原位置，图片渐隐消失
        targetTemp = CGRectMake(window.center.x, window.center.y, 0, 0);
    }
    self.window.windowLevel = UIWindowLevelNormal;//显示状态栏
    
    //横长图动画处理
    CGFloat tempImageW = self.photoBrowserView.zoomImageSize.width;
    CGFloat tempImageH = self.photoBrowserView.zoomImageSize.height;
    if (tempImageW > tempImageH) {
        if (tempImageW /tempImageH >= 2.6) {
            _tempView.frame = CGRectMake(_tempView.frame.origin.x, _tempView.frame.origin.y, targetTemp.size.width, targetTemp.size.height);
            _tempView.image = [self placeholderImageForIndex:self.currentImageIndex];
        }
    }

    [UIView animateWithDuration:HZPhotoBrowserHideImageAnimationDuration animations:^{
        if (_photoBrowserStyle == HZPhotoBrowserStyleDefault) {
            _tempView.transform = CGAffineTransformInvert(self.transform);
            _tempView.frame = targetTemp;
        }
        //普通动画效果，图片不会回到原位置，图片渐隐消失
        else{
            _tempView.alpha = 0;
        }
        _coverView.alpha = 0;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
        [_tempView removeFromSuperview];
        [_contentView removeFromSuperview];
        _tempView = nil;
        _contentView = nil;
        sourceView.hidden = NO;
    }];
}

- (UIView *)getSourceView{
    if (_currentImageIndex <= self.sourceImagesContainerView.subviews.count - 1) {
        UIView *sourceView = self.sourceImagesContainerView.subviews[_currentImageIndex];
        return sourceView;
    }
    return nil;
}

- (void)cancelPrepareForHide{
    [self.coverView removeFromSuperview];
    _indexLabel.hidden = NO;
    if (_tempView) {
        [_tempView removeFromSuperview];
        _tempView = nil;
    }
    _scrollView.hidden = NO;
    self.backgroundColor = HZPhotoBrowserBackgrounColor;
    _contentView.backgroundColor = HZPhotoBrowserBackgrounColor;
}

- (void)prepareForHide{
    [_contentView insertSubview:self.coverView belowSubview:self];
    _indexLabel.hidden = YES;
    [self addSubview:self.tempView];
    _photoBrowserView.hidden = YES;
//    NSLog(@"photoBrowserView -- %@",_photoBrowserView);
    self.backgroundColor = [UIColor clearColor];
    _contentView.backgroundColor = [UIColor clearColor];
    UIView *view = [self getSourceView];
    view.hidden = YES;
}

- (void)bounceToOrigin{
    [UIView animateWithDuration:HZPhotoBrowserHideImageAnimationDuration animations:^{
        self.tempView.transform = CGAffineTransformIdentity;
        _coverView.alpha = 1;
    } completion:^(BOOL finished) {
        _indexLabel.hidden = NO;
        [_tempView removeFromSuperview];
        [_coverView removeFromSuperview];
        _tempView = nil;
        _coverView = nil;
        _photoBrowserView.hidden = NO;
        self.backgroundColor = HZPhotoBrowserBackgrounColor;
        _contentView.backgroundColor = HZPhotoBrowserBackgrounColor;
        UIView *view = [self getSourceView];
        view.hidden = NO;
    }];
}

#pragma mark - scrollview代理方法
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    int index = (scrollView.contentOffset.x + _scrollView.bounds.size.width * 0.5) / _scrollView.bounds.size.width;
    _indexLabel.text = [NSString stringWithFormat:@"%d/%ld", index + 1, (long)self.imageCount];
    long left = index - 1;
    long right = index + 1;
    left = left>0?left : 0;
    right = right>self.imageCount?self.imageCount:right;
    
    for (long i = left; i < right; i++) {
         [self setupImageOfImageViewForIndex:i];
    }
}

//scrollview结束滚动调用
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    int autualIndex = scrollView.contentOffset.x  / _scrollView.bounds.size.width;
    //设置当前下标
    self.currentImageIndex = autualIndex;
    self.photoBrowserView = _scrollView.subviews[self.currentImageIndex];
    
    //将不是当前imageview的缩放全部还原 (这个方法有些冗余，后期可以改进)
    for (HZPhotoBrowserView *view in _scrollView.subviews) {
        if (view.imageview.tag != autualIndex) {
                view.scrollview.zoomScale = 1.0;
        }
    }
    
    if (self.scrollDidScrollBlock) {
        self.scrollDidScrollBlock(autualIndex);
    }
}

-(void)scrollDidScrollBlock:(ScrollDidScrollBlock)block
{
    self.scrollDidScrollBlock = block;
}

-(void)hzPhotoBrowserShareBlock:(HZPhotoBrowserShareBlock)block
{
    self.hzPhotoBrowserShareBlock = block;
}

#pragma mark - tap
#pragma mark 单击
- (void)photoClick:(UITapGestureRecognizer *)recognizer
{
    [self hidePhotoBrowser];
}

#pragma mark 双击
- (void)handleDoubleTap:(UITapGestureRecognizer *)recognizer
{
    HZPhotoBrowserView *view = _scrollView.subviews[self.currentImageIndex];
    CGPoint touchPoint = [recognizer locationInView:self];
    if (view.scrollview.zoomScale <= 1.0) {
        CGFloat scaleX = touchPoint.x + view.scrollview.contentOffset.x;//需要放大的图片的X点
        CGFloat sacleY = touchPoint.y + view.scrollview.contentOffset.y;//需要放大的图片的Y点
        [view.scrollview zoomToRect:CGRectMake(scaleX, sacleY, 10, 10) animated:YES];
    } else {
        [view.scrollview setZoomScale:1.0 animated:YES]; //还原
    }
}

#pragma mark 长按
- (void)didPan:(UIPanGestureRecognizer *)panGesture {
    UIDeviceOrientation orientation = [UIDevice currentDevice].orientation;
    if (UIDeviceOrientationIsLandscape(orientation)) {//横屏不允许拉动图片
        return;
    }
    
    //transPoint : 手指在视图上移动的位置（x,y）向下和向右为正，向上和向左为负。
    //locationPoint ： 手指在视图上的位置（x,y）就是手指在视图本身坐标系的位置。
    //velocity： 手指在视图上移动的速度（x,y）, 正负也是代表方向。
    CGPoint transPoint = [panGesture translationInView:self];
    
    //判断如果竖长图时，不在开头和结尾不执行拖拽收起动画
//    if (self.photoBrowserView.scrollview.contentOffset.y <= 0 || self.photoBrowserView.scrollview.contentOffset.y >= self.photoBrowserView.scrollview.contentSize.height - self.photoBrowserView.scrollview.frame.size.height) {
//        
//    }
//    else{
//        if (transPoint.x <= 0 || transPoint.x >= kScreenWidth) {
//            return;
//        }
//    }
    
//    CGPoint locationPoint = [panGesture locationInView:self];
    CGPoint velocity = [panGesture velocityInView:self];//速度
    
//    NSLog(@"开始拖动");
    switch (panGesture.state) {
        case UIGestureRecognizerStateBegan:
        {
            [self prepareForHide];
            //优化长图点击放大效果
            if (self.photoBrowserView.imageview.frame.size.height > kScreenHeight) {
                _tempView.frame = CGRectMake(0, 0, _tempView.frame.size.width, _tempView.frame.size.height);
            }
        }
            break;
        case UIGestureRecognizerStateChanged:
        {
            _indexLabel.hidden = YES;
            double delt = 1 - fabs(transPoint.y) / self.frame.size.height;
            delt = MAX(delt, 0);
            double s = MAX(delt, 0.5);
            CGAffineTransform translation = CGAffineTransformMakeTranslation(transPoint.x/s, transPoint.y/s);
            CGAffineTransform scale = CGAffineTransformMakeScale(s, s);
            self.tempView.transform = CGAffineTransformConcat(translation, scale);
            self.coverView.alpha = delt;
        }
            break;
        case UIGestureRecognizerStateEnded:
        case UIGestureRecognizerStateCancelled:
        {
            if (fabs(transPoint.y) > 220 || fabs(velocity.y) > 500) {//退出图片浏览器
                [self hideAnimation];
            } else {//回到原来的位置
                [self bounceToOrigin];
            }
        }
            break;
        default:
            break;
    }
}

#pragma mark -  长按保存
-(void)imglongTapClick:(UILongPressGestureRecognizer *)gesture {
    if(gesture.state == UIGestureRecognizerStateBegan) {
//        if (!self.isSavePhoto) {
//            [BaseUtil showToastRemindView:@"作品已被保护" isSuccess:NO];
//            return;
//        }
//        UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
//        UIAlertAction *firstAction = [UIAlertAction actionWithTitle:@"保存图片" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//            [self judgePhotoAlbumPermission];
//        }];
//        [firstAction setValue:[GUtil colorForDarkModelWithName:@"COLOR_A1"] forKey:@"titleTextColor"];
//        UIAlertAction *secondAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
//
//        }];
//        [secondAction setValue:[GUtil colorForDarkModelWithName:@"COLOR_A1"] forKey:@"titleTextColor"];
//        [actionSheet addAction:firstAction];
//        [actionSheet addAction:secondAction];
//        [[SharedInstance sharedInstance].baseNavVC presentViewController:actionSheet animated:YES completion:nil];
    }
}

//判断相册权限
- (void)judgePhotoAlbumPermission {
    if ([PHPhotoLibrary authorizationStatus] == PHAuthorizationStatusNotDetermined) {//先判断用户是否做出权限判断
        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
            if (status == PHAuthorizationStatusAuthorized) {
                [self pushImagePickerController];
            } else {
                dispatch_async(dispatch_get_main_queue(), ^(void) {
                    [self makeToast:@"请先开启相册权限"];
            });
            }
        }];
    } else {//做出之后, 再判断用户是否开启权限
        PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
        if (status == PHAuthorizationStatusRestricted ||
            status == PHAuthorizationStatusDenied) {//无权限
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                [self makeToast:@"请先开启相册权限"];

            });
        } else if (status == PHAuthorizationStatusAuthorized) {
            [self pushImagePickerController];
        }
    }
}

-(void)pushImagePickerController
{
    int index = _scrollView.contentOffset.x / _scrollView.bounds.size.width;
    HZPhotoBrowserView *currentView = _scrollView.subviews[index];
    UIImage *image = currentView.imageview.image;
    [HXPhotoTools savePhotoToCustomAlbumWithName:NULL photo:image];
    [self makeToast:@"保存成功"];

//    UIImage *nImage = [SharedInstance jx_WaterImageWithImage:image];
//    [[TZImageManager manager] savePhotoWithImage:nImage completion:^(PHAsset *asset, NSError *error) {
//        if (!error) {
//            UIAlertView*alert = [[UIAlertView alloc]initWithTitle:@"提示"message:@"成功保存到相册" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
//            [alert show];
//        }
//    }];
}

- (void)savePhotoWithData:(NSData *)data completion:(void (^)(NSError *error))completion {
    [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
        PHAssetResourceCreationOptions *options = [[PHAssetResourceCreationOptions alloc] init];
        options.shouldMoveFile = YES;
        PHAssetCreationRequest *request = [PHAssetCreationRequest creationRequestForAsset];
        [request addResourceWithType:PHAssetResourceTypePhoto data:data options:options];
        request.creationDate = [NSDate date];
    } completionHandler:^(BOOL success, NSError * _Nullable error) {
        dispatch_sync(dispatch_get_main_queue(), ^{
            if (success && completion) {
                completion(nil);
            } else if (error) {
                NSLog(@"保存照片出错:%@",error.localizedDescription);
                if (completion) {
                    completion(error);
                }
            }
        });
    }];
}

#pragma mark public methods
- (void)show
{
    _contentView = [[UIView alloc] init];
    _contentView.backgroundColor = HZPhotoBrowserBackgrounColor;
    
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    _contentView.center = window.center;
    _contentView.bounds = window.bounds;

    self.frame = _contentView.bounds;
//    if (IPHONE_X) {
//        self.frame = CGRectMake(0, kStatusBar_Height,kAPPWidth,KAppHeight - kStatusBar_Height - kBottomSafeHeight);
//    } else {
//        self.frame = _contentView.bounds;
//    }
    window.windowLevel = UIWindowLevelStatusBar+10.0f;//隐藏状态栏
    [_contentView addSubview:self];
    
    [window addSubview:_contentView];
    
    [self performSelector:@selector(onDeviceOrientationChangeWithObserver) withObject:nil afterDelay:HZPhotoBrowserShowImageAnimationDuration + 0.2];
}

@end
