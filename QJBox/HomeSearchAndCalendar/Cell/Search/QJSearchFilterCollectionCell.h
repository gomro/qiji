//
//  QJSearchFilterCollectionCell.h
//  QJBox
//
//  Created by rui on 2022/7/6.
//

#import <UIKit/UIKit.h>

#import "QJSearchSortModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJSearchFilterCollectionCell : UICollectionViewCell

/* 数据源 */
@property (nonatomic,strong) QJSearchTagDetailModel *model;

@end

NS_ASSUME_NONNULL_END
