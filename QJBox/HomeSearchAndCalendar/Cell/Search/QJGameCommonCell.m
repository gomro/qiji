//
//  QJGameCommonCell.m
//  QJBox
//
//  Created by rui on 2022/7/7.
//

#import "QJGameCommonCell.h"
/* 标签 */
#import "QJMineMsgTagView.h"
#import "QJProductPostRequest.h"
#import "QJProductRequest.h"

#import "QJDetailAlertView.h"
#import "TYAlertController.h"

@interface QJGameCommonCell ()

//选中按钮
@property (nonatomic, strong) UIButton *chooseButton;
//icon
@property (nonatomic, strong) UIImageView *iconImageView;
//标题
@property (nonatomic, strong) UILabel *titleLabel;
//在线人数
@property (nonatomic, strong) UILabel *onlineLabel;
//获取按钮
@property (nonatomic, strong) UIButton *getButton;
//删除按钮
@property (nonatomic, strong) UIButton *deleteButton;

//标签
@property (nonatomic, strong) QJMineMsgTagView *tagView;

@end

@implementation QJGameCommonCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        /* 初始化UI */
        [self initUI];
        /* 添加约束 */
        [self makeConstraints];
    }
    return self;
}

/**
 * @author: zjr
 * @date: 2022-7-7
 * @desc: 初始化UI
 */
- (void)initUI{
    self.backgroundColor = [UIColor whiteColor];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.contentView.backgroundColor = [UIColor colorWithHexString:@"0xFFFFFF"];
    [self.contentView addSubview:self.chooseButton];
    [self.contentView addSubview:self.iconImageView];
    [self.contentView addSubview:self.titleLabel];
    [self.contentView addSubview:self.onlineLabel];
    [self.contentView addSubview:self.getButton];
    [self.contentView addSubview:self.deleteButton];
    [self.contentView addSubview:self.tagView];
}

/**
 * @author: zjr
 * @date: 2022-7-7
 * @desc: 添加约束
 */
- (void)makeConstraints{
    self.chooseButton.hidden = YES;
    [self.chooseButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(10+15);
        make.left.equalTo(self.contentView).offset(16);
        make.width.mas_equalTo(44*kWScale);
        make.height.mas_equalTo(44*kWScale);
    }];
    [self.iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(10);
        make.left.equalTo(self.chooseButton.mas_right).offset(16);
        make.width.mas_equalTo(64*kWScale);
        make.height.mas_equalTo(64*kWScale);
    }];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.iconImageView).offset(3);
        make.left.equalTo(self.iconImageView.mas_right).offset(8);
        make.right.equalTo(self.contentView);
        make.height.mas_equalTo(14*kWScale);
    }];
    
    [self.onlineLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).offset(6);
        make.left.equalTo(self.iconImageView.mas_right).offset(8);
        make.height.mas_equalTo(10*kWScale);
    }];
    
    [self.deleteButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.width.mas_equalTo(24*kWScale);
        make.height.mas_equalTo(24*kWScale);
        make.right.equalTo(self.contentView);
    }];
    
    [self.getButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.width.mas_equalTo(58*kWScale);
        make.height.mas_equalTo(26*kWScale);
        make.right.equalTo(self.deleteButton.mas_left).offset(-5);
    }];
    
    [self.tagView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.iconImageView.mas_right).offset(8);
        make.right.equalTo(self.getButton.mas_left).offset(-10);
        make.height.mas_equalTo(17*kWScale);
        make.bottom.equalTo(self.iconImageView.mas_bottom).offset(-2);
    }];
}

#pragma mark - Lazy Loading
- (UIImageView *)iconImageView {
    if (!_iconImageView) {
        _iconImageView = [UIImageView new];
        _iconImageView.layer.cornerRadius = 14;
        _iconImageView.layer.masksToBounds = YES;
        _iconImageView.contentMode = UIViewContentModeScaleAspectFill;
    }
    return _iconImageView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.font = kFont(14);
        _titleLabel.textColor = [UIColor blackColor];
        _titleLabel.text = @"";
    }
    return _titleLabel;
}

- (UILabel *)onlineLabel {
    if (!_onlineLabel) {
        _onlineLabel = [UILabel new];
        _onlineLabel.font = kFont(10);
        _onlineLabel.textColor = UIColorFromRGB(0x919599);
        _onlineLabel.text = @"";
    }
    return _onlineLabel;
}

- (QJMineMsgTagView *)tagView {
    if (!_tagView) {
        _tagView = [[QJMineMsgTagView alloc] init];
        _tagView.userInteractionEnabled = NO;
    }
    return _tagView;
}

- (UIButton *)deleteButton {
    if (!_deleteButton) {
        _deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_deleteButton addTarget:self action:@selector(deleteBtnAction) forControlEvents:UIControlEventTouchUpInside];
        [_deleteButton setImage:[UIImage imageNamed:@"qj_product_moreblack"] forState:UIControlStateNormal];
    }
    return _deleteButton;
}

- (UIButton *)chooseButton {
    if (!_chooseButton) {
        _chooseButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_chooseButton addTarget:self action:@selector(chooseBtnAction) forControlEvents:UIControlEventTouchUpInside];
        [_chooseButton setImage:[UIImage imageNamed:@"qj_myproduct_normal"] forState:UIControlStateNormal];
        [_chooseButton setImage:[UIImage imageNamed:@"qj_myproduct_choose"] forState:UIControlStateSelected];
    }
    return _chooseButton;
}

- (UIButton *)getButton {
    if (!_getButton) {
        _getButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_getButton addTarget:self action:@selector(btnAction) forControlEvents:UIControlEventTouchUpInside];

        _getButton.titleLabel.font = FontSemibold(12);
        [_getButton setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithHexString:@"#ffffff"]] forState:UIControlStateNormal];
        [_getButton setTitle:@"获取" forState:UIControlStateNormal];
        [_getButton setTitleColor:kColorWithHexString(@"#1F2A4D") forState:UIControlStateNormal];
        _getButton.layer.cornerRadius = 4;
        _getButton.layer.borderWidth = 1;
        _getButton.layer.borderColor = kColorWithHexString(@"000000").CGColor;
        _getButton.layer.masksToBounds = YES;
    }
    return _getButton;
}

/**
 * @author: zjr
 * @date: 2022-7-7
 * @desc: 数据赋值
 */
- (void)setModel:(QJSearchGameDetailModel *)model{
    _model = model;
    self.titleLabel.text = kCheckStringNil(_model.name);
    self.onlineLabel.text = [NSString stringWithFormat:@"%@人在线",_model.onlineCount];
    self.iconImageView.imageURL = [NSURL URLWithString:[_model.icon.length > 0 ?_model.icon:_model.thumbnail checkImageUrlString]];
    NSMutableArray *titleArray = [NSMutableArray array];
    NSMutableArray *titleColorArray = [NSMutableArray array];
    NSMutableArray *bgColorArray = [NSMutableArray array];
    [_model.tags enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        QJSearchGameDetailModel *tagModel = (QJSearchGameDetailModel *)obj;
        [titleArray addObject:tagModel.name];
        [titleColorArray addObject:[UIColor hx_colorWithHexStr:tagModel.color]];
        [bgColorArray addObject:[UIColor hx_colorWithHexStr:tagModel.backgroundColor]];
    }];
    [self.tagView reloadTagView:titleArray BgColor:bgColorArray TextColor:titleColorArray Radius:2 RectCorner:UIRectCornerAllCorners];
    [self resetConstraints];
    [self changeButtonTitle];
}

- (void)resetConstraints{
    if ([_model.type isEqualToString:@"1"]) {
        self.deleteButton.hidden = NO;
        [self.getButton mas_updateConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.contentView);
            make.width.mas_equalTo(58*kWScale);
            make.height.mas_equalTo(26*kWScale);
            make.right.equalTo(self.deleteButton.mas_left).offset(-5);
        }];
    }else{
        self.deleteButton.hidden = YES;
        [self.getButton mas_updateConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.contentView);
            make.width.mas_equalTo(58*kWScale);
            make.height.mas_equalTo(26*kWScale);
            make.right.equalTo(self.contentView).offset(-16);
        }];
    }
    
    _chooseButton.selected = _model.isDelete;
    if (_model.isEditing == YES) {
        self.chooseButton.hidden = NO;
        [self.chooseButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.contentView).offset(10+15);
            make.left.equalTo(self.contentView).offset(6);
            make.width.mas_equalTo(44*kWScale);
            make.height.mas_equalTo(44*kWScale);
        }];
        [self.iconImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.contentView).offset(10);
            make.left.equalTo(self.chooseButton.mas_right);
            make.width.mas_equalTo(64*kWScale);
            make.height.mas_equalTo(64*kWScale);
        }];
    } else {
        self.chooseButton.hidden = YES;
        [self.iconImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.contentView).offset(10);
            make.left.equalTo(self.contentView).offset(16);
            make.width.mas_equalTo(64*kWScale);
            make.height.mas_equalTo(64*kWScale);
        }];
    }
}

- (void)chooseBtnAction{
    if (self.chooseClick) {
        self.chooseClick(self.model);
    }
}

- (void)deleteBtnAction{
    QJDetailAlertView *alertView = [[QJDetailAlertView alloc] initWithFrame:CGRectZero titleType:QJDetailAlertViewTitleTypeDefault buttonType:QJDetailAlertViewButtonTypeDefault];
    [alertView showTitleText:@"提示" describeText:@"是否要删除游戏？"];
    [alertView.leftButton setTitle:@"取消" forState:UIControlStateNormal];
    [alertView.leftButton setTitleColor:UIColorFromRGB(0x007AFF) forState:UIControlStateNormal];
    [alertView.rightButton setTitle:@"确认" forState:UIControlStateNormal];
    MJWeakSelf
    [alertView returnClick:^(QJDetailAlertViewButtonClickType type) {
        [[QJAppTool getCurrentViewController] dismissViewControllerAnimated:YES completion:nil];
        if (type == QJDetailAlertViewButtonClickTypeRight) {
            if (weakSelf.btnClick) {
                weakSelf.btnClick(weakSelf.model);
            }
        }
    }];
    TYAlertController *alertController = [TYAlertController alertControllerWithAlertView:alertView preferredStyle:TYAlertControllerStyleAlert];
    [[QJAppTool getCurrentViewController] presentViewController:alertController animated:YES completion:nil];
}

- (void)changeButtonTitle{
    if (_model.disabled) {
        [_getButton setTitle:@"已下架" forState:UIControlStateNormal];
    } else {
        if ([[UIApplication sharedApplication] canOpenURL:
             [NSURL URLWithString:_model.iSch]]) {
            [_getButton setTitle:@"启动" forState:UIControlStateNormal];
        }else{
            [_getButton setTitle:@"获取" forState:UIControlStateNormal];
        }
    }
    //_model.iSch
    
}

#warning TODO 获取游戏
- (void)btnAction{
    DLog(@"去获取");
    if (_model.isEditing == NO) {
        [self addProduct];
    //    _model.iSch
        if ([_model.iPkgMode isEqualToString:@"0"]) {//0-appstore 1-custom
            if (_model.iUrl.length > 0) {
                NSURL *iUrl = [NSURL URLWithString:_model.iUrl];
                if ([[UIApplication sharedApplication] canOpenURL:iUrl]) {
                    [[UIApplication sharedApplication] openURL:iUrl options:@{} completionHandler:nil];
                }
            }
            
        }else if ([_model.iPkgMode isEqualToString:@"1"]) {//0-appstore 1-custom
            if (_model.iCusInsUrl.length > 0) {
                NSURL *iCusInsUrl = [NSURL URLWithString:_model.iCusInsUrl];
                if ([[UIApplication sharedApplication] canOpenURL:iCusInsUrl]) {
                    [[UIApplication sharedApplication] openURL:iCusInsUrl options:@{} completionHandler:nil];
                }
            }
           
        }
//        if ([[UIApplication sharedApplication] canOpenURL:
//             [NSURL URLWithString:_model.iSch]]) {
//            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:_model.iSch] options:@{} completionHandler:^(BOOL success) {
//                DLog(@"是否打开 = %@",success?@"YES":@"NO");
//            }];
//    //        if (@available(iOS 10.0, *)){
//    //            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"weixin://"] options:@{} completionHandler:nil];
//    //        } else {
//    //            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"weixin://"]];
//    //        }
//        }else{
//            DLog(@"无法打开");
//        }
    }
}

- (void)addProduct{
    QJProductPostRequest *productReq = [[QJProductPostRequest alloc] init];
    NSMutableDictionary *muDic = [NSMutableDictionary dictionary];
    [muDic setValue:_model.ID forKey:@"id"];
    productReq.dic = muDic.copy;
    [productReq getAddProductRequest];

//    WS(weakSelf)
//    productReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
//        [QJAppTool hideHUDLoading];
//        if (!ResponseSuccess) {
//            NSString *msg = EncodeStringFromDic(request.responseObject, @"message");
//            [QJAppTool showToast:msg];
//        }
//    };
//    productReq.failureCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
//        [QJAppTool showToast:@"数据异常 请稍后再试"];
//    };
    
    QJProductRequest *productGetReq = [[QJProductRequest alloc] init];
    [productGetReq getProductSignatureRequest:_model.ID];
    productGetReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        
    };
}

@end
