//
//  QJSearchContentTypeCell.m
//  QJBox
//
//  Created by rui on 2022/7/6.
//

#import "QJSearchContentTypeCell.h"

@interface QJSearchContentTypeCell()

@property (nonatomic, strong) UIImageView *headerImageV;
/* 分割线 */
//@property (nonatomic, strong) UIView *line;
@property (nonatomic, strong) UILabel *titleLab;

@end

@implementation QJSearchContentTypeCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]){
        [self initChildView];
    }
    return self;
}

/**
 * @author: zjr
 * @date: 2022-7-6
 * @desc:  UI
 */
- (void)initChildView {
    self.contentView.backgroundColor = [UIColor whiteColor];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    self.headerImageV = [[UIImageView alloc] init];
    self.headerImageV.frame = CGRectMake(27.5, 16, 16, 16);
    self.headerImageV.image = [UIImage imageNamed:@"icon_search"];
    [self.contentView addSubview:self.headerImageV];
    
//    self.line = [[UIView alloc] init];
//    [self.contentView addSubview:self.line];
//    _line.backgroundColor = UIColorFromRGB(0xF2F2F2);
//    _line.frame = CGRectMake(16, 43.5, 7, 0.5);

    /* 标题 */
    self.titleLab = [[UILabel alloc] init];
    [self.contentView addSubview:self.titleLab];
    _titleLab.font = kFont(14);
    _titleLab.textColor = UIColorFromRGB(0x474849);
}

- (void)setModel:(id)model{
    _model = model;

//    _line.width = self.width - 16*2;

    _titleLab.left = 56;
    _titleLab.width = self.width - 56  - 16;

    if (!_searchkey) {
        _searchkey = @"";
    }
    [_titleLab setSingleStr:_model range:[_model rangeOfString:_searchkey] color:UIColorFromRGB(0xFD410D)];
    _titleLab.centerY = self.height/2;
    _headerImageV.centerY = self.height/2;
}

@end
