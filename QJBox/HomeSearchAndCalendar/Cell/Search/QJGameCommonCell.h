//
//  QJGameCommonCell.h
//  QJBox
//
//  Created by rui on 2022/7/7.
//

#import <UIKit/UIKit.h>

/* model */
#import "QJSearchGameListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJGameCommonCell : UITableViewCell

@property (nonatomic, strong) QJSearchGameDetailModel *model;
@property (nonatomic, copy) void(^btnClick)(QJSearchGameDetailModel *model);
@property (nonatomic, copy) void(^chooseClick)(QJSearchGameDetailModel *chooseModel);

@end

NS_ASSUME_NONNULL_END
