//
//  QJSearchFilterCollectionCell.m
//  QJBox
//
//  Created by rui on 2022/7/6.
//

#import "QJSearchFilterCollectionCell.h"

@interface QJSearchFilterCollectionCell ()

/* 标题 */
@property (nonatomic, strong) UILabel *tagLabel;

@end

@implementation QJSearchFilterCollectionCell

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self initChildView];
    }
    return self;
}

/* 初始化view */
- (void)initChildView {
    self.backgroundColor = UIColorFromRGB(0xFFFFFF);
    self.contentView.backgroundColor = UIColorFromRGB(0xF7F7F7);
    self.contentView.layer.cornerRadius  = 13;
    self.contentView.layer.masksToBounds = YES;

    self.tagLabel = [[UILabel alloc] init];
    [self.contentView addSubview:self.tagLabel];
    _tagLabel.font = kFont(12);
    _tagLabel.textColor = UIColorFromRGB(0x636363);//0x333333 0x1C1C1C
    _tagLabel.backgroundColor = [UIColor clearColor];
    _tagLabel.textAlignment = NSTextAlignmentCenter;
}

/* 赋值 */
-(void)setModel:(QJSearchTagDetailModel *)model{
    _model = model;
    
    _tagLabel.frame = CGRectMake(5, 0, self.width - 5*2, self.height);
    _tagLabel.text = _model.name;
    
    if (_model.isSelect) {
        _tagLabel.textColor = UIColorFromRGB(0xFF9500);
        self.contentView.backgroundColor = [UIColor colorWithRed:1 green:0.584 blue:0 alpha:0.1];//UIColorFromRGB(0xFFE5E9);
    }else{
        _tagLabel.textColor = UIColorFromRGB(0x636363);
        self.contentView.backgroundColor = UIColorFromRGB(0xF7F7F7);
    }
}

@end

