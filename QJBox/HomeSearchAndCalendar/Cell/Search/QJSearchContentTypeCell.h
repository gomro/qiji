//
//  QJSearchContentTypeCell.h
//  QJBox
//
//  Created by rui on 2022/7/6.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJSearchContentTypeCell : UITableViewCell

/* 搜索的key */
@property (nonatomic,copy) NSString *searchkey;
/* 字符串类型 */
@property (nonatomic,strong) id model;

@end

NS_ASSUME_NONNULL_END
