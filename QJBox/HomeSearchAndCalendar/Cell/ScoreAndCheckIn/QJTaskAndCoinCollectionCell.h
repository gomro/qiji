//
//  QJTaskAndCoinCollectionCell.h
//  QJBox
//
//  Created by rui on 2022/7/5.
//

#import <UIKit/UIKit.h>

/* model */
#import "QJTaskListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJTaskAndCoinCollectionCell : UICollectionViewCell

@property (nonatomic, strong) QJTaskDetailModel *model;
@property (nonatomic, copy) void (^btnClick)(QJTaskDetailModel *model);
//@property (nonatomic, copy) void (^contentBtnClick)(QJTaskDetailModel *model);

@end

NS_ASSUME_NONNULL_END
