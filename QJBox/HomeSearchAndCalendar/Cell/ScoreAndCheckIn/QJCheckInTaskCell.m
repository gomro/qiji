//
//  QJCheckInTaskCell.m
//  QJBox
//
//  Created by rui on 2022/6/27.
//

#import "QJCheckInTaskCell.h"
/* 倒计时 */
#import "QJCountDown.h"

#import "QJProductDetailViewController.h"
#import "QJSearchGameListModel.h"

@interface QJCheckInTaskCell ()

//白色底图
@property (nonatomic, strong) UIView *whiteBgView;
/* icon */
@property (nonatomic, strong) UIImageView *iconImageView;
//任务标题
@property (nonatomic, strong) UILabel *titleLabel;
//任务名字
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UIButton *detailButton;
//任务描述
@property (nonatomic, strong) UILabel *subLabel;
//任务名额
@property (nonatomic, strong) UILabel *countLabel;
//任务完成按钮
@property (nonatomic, strong) UIButton *statusButton;
//线条
@property (nonatomic, strong) UIView *lineView;
@property (nonatomic, strong) QJCountDown *countDown;

@end

@implementation QJCheckInTaskCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor clearColor];
        self.contentView.backgroundColor = [UIColor clearColor];

        /* 初始化UI */
        [self initUI];
        /* 添加约束 */
        [self makeConstraints];
    }
    return self;
}

/**
 * @author: zjr
 * @date: 2022-6-27
 * @desc: 初始化UI
 */
- (void)initUI{
    [self.contentView addSubview:self.whiteBgView];
    [self.whiteBgView addSubview:self.iconImageView];
    [self.whiteBgView addSubview:self.nameLabel];
    [self.whiteBgView addSubview:self.titleLabel];
    [self.whiteBgView addSubview:self.detailButton];
    [self.whiteBgView addSubview:self.subLabel];
    [self.whiteBgView addSubview:self.countLabel];
    [self.whiteBgView addSubview:self.statusButton];
    [self.whiteBgView addSubview:self.lineView];
}

/**
 * @author: zjr
 * @date: 2022-7-1
 * @desc: 添加约束
 */
- (void)makeConstraints{
    [self.whiteBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(0);
        make.right.equalTo(self.contentView).offset(0);
        make.top.bottom.equalTo(self.contentView);
    }];
    
    [self.iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.whiteBgView).offset(10);
        make.left.equalTo(self.whiteBgView).offset(16);
        make.height.equalTo(@(40*kWScale));
        make.width.equalTo(@(40*kWScale));
    }];
    
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.iconImageView.mas_bottom).offset(4);
        make.left.equalTo(self.whiteBgView).offset(16);
        make.width.equalTo(@(55*kWScale));
        make.height.equalTo(@(15*kWScale));
    }];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.whiteBgView).offset(10);
        make.left.equalTo(self.iconImageView.mas_right).offset(13);
        make.height.equalTo(@(20*kWScale));
    }];
    
    [self.detailButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.bottom.equalTo(self.whiteBgView);
        make.right.equalTo(self.titleLabel.mas_left);
    }];
    
    [self.subLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).offset(8);
        make.left.equalTo(self.iconImageView.mas_right).offset(13);
        make.height.equalTo(@(15*kWScale));
    }];
    
    [self.statusButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.whiteBgView).offset(15);
        make.width.equalTo(@(60*kWScale));
        make.height.equalTo(@(28*kWScale));
        make.right.equalTo(self.whiteBgView).offset(-16);
    }];
    
    [self.countLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.statusButton.mas_bottom).offset(12);
//        make.right.equalTo(self.whiteBgView).offset(-16);
        make.centerX.equalTo(self.statusButton);
        make.height.equalTo(@(15*kWScale));
    }];
    
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.whiteBgView);
        make.right.equalTo(self.whiteBgView).offset(-14);
        make.left.equalTo(self.whiteBgView).offset(14);
        make.height.equalTo(@1);
    }];
    
    self.countDown = [QJCountDown new];
}

#pragma mark - Lazy Loading
- (UIImageView *)iconImageView{
    if (!_iconImageView) {
        _iconImageView = [UIImageView new];
        _iconImageView.layer.cornerRadius = 10;
        _iconImageView.layer.masksToBounds = YES;
        _iconImageView.contentMode = UIViewContentModeScaleAspectFill;

    }
    return _iconImageView;
}

- (UIView *)lineView{
    if (!_lineView) {
        _lineView = [UIView new];
        _lineView.backgroundColor = [UIColor hx_colorWithHexStr:@"0x0C1326" alpha:0.3];
    }
    return _lineView;
}

- (UIView *)whiteBgView {
    if (!_whiteBgView) {
        _whiteBgView = [UIView new];
//        _whiteBgView.backgroundColor = [UIColor whiteColor];
    }
    return _whiteBgView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.font = kFont(14);
        _titleLabel.textColor = UIColorFromRGB(0xFFFFFF);
        _titleLabel.text = @"走完游戏地图";
    }
    return _titleLabel;
}

- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [UILabel new];
        _nameLabel.font = kFont(10);
        _nameLabel.textColor = UIColorFromRGB(0xF7D67D);
        _nameLabel.text = @"荣耀大天使";
    }
    return _nameLabel;
}

- (UILabel *)subLabel {
    if (!_subLabel) {
        _subLabel = [UILabel new];
        _subLabel.font = kFont(12);
        _subLabel.textColor = [UIColor hx_colorWithHexStr:@"0xFFFFFF" alpha:0.8];
        _subLabel.text = @"可获道具：振奋铠甲";
    }
    return _subLabel;
}

- (UILabel *)countLabel {
    if (!_countLabel) {
        _countLabel = [UILabel new];
        _countLabel.font = kFont(10);
        _countLabel.textColor = UIColorFromRGB(0xFFFFFF);
        _countLabel.text = @"名额0/10";
    }
    return _countLabel;
}

- (UIButton *)statusButton {
    if (!_statusButton) {
        _statusButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_statusButton addTarget:self action:@selector(btnAction) forControlEvents:UIControlEventTouchUpInside];
        _statusButton.titleLabel.font = kboldFont(12);
        [_statusButton setBackgroundImage:[UIImage imageNamed:@"qj_taskbutton_bgicon_yellow"] forState:UIControlStateNormal];
        [_statusButton setTitle:@"领取" forState:UIControlStateNormal];
        [_statusButton setTitleColor:UIColorFromRGB(0x4C1C0F) forState:UIControlStateNormal];
    }
    return _statusButton;
}

- (UIButton *)detailButton {
    if (!_detailButton) {
        _detailButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_detailButton addTarget:self action:@selector(detailAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _detailButton;
}


/**
 * @author: zjr
 * @date: 2022-8-19
 * @desc: 数据赋值
 */
- (void)setModel:(QJTaskCenterCommonDetailModel *)model{
//    if (_model != model) {
        _model = model;
        [self.countDown destoryTimer];
        [self.iconImageView setImageWithURL:[NSURL URLWithString:[_model.icon checkImageUrlString]] placeholder:[UIImage imageNamed:@"mine_head"]];
        self.titleLabel.text = _model.name;
        self.nameLabel.text = _model.gameName;
        self.subLabel.text = _model.rewardDesc;
        if ([_model.taskStatus isEqualToString:@"1"]) {
            if (_model.isReceiveTask) {
                [_statusButton setBackgroundImage:[UIImage imageNamed:@"qj_taskbutton_bgicon_gray"] forState:UIControlStateNormal];
                [_statusButton setTitle:@"领取成功" forState:UIControlStateNormal];
            }else{
                [_statusButton setBackgroundImage:[UIImage imageNamed:@"qj_taskbutton_bgicon_yellow"] forState:UIControlStateNormal];
                [_statusButton setTitle:@"领取" forState:UIControlStateNormal];
            }
            self.countLabel.text = [NSString stringWithFormat:@"名额%@/%@",self.model.currentNum,self.model.totalNum];
        }else if ([_model.taskStatus isEqualToString:@"2"]) {
            [_statusButton setBackgroundImage:[UIImage imageNamed:@"qj_taskbutton_bgicon_yellow"] forState:UIControlStateNormal];
            [_statusButton setTitle:@"即将开始" forState:UIControlStateNormal];
            WS(weakSelf)
            [self.countDown countDownWithStratTimeStamp:[self.model.startTimeMillis longLongValue] finishTimeStamp:[self.model.endTimeMillis longLongValue] completeBlock:^(NSInteger day, NSInteger hour, NSInteger minute, NSInteger second) {
                weakSelf.countLabel.text = [NSString stringWithFormat:@"%02ld:%02ld:%02ld", (long)(day*24+hour), (long)minute, (long)second];
            }];
            self.countDown.TimerStopComplete = ^{
                [weakSelf.countDown destoryTimer];
            };
        }else{
            [_statusButton setBackgroundImage:[UIImage imageNamed:@"qj_taskbutton_bgicon_gray"] forState:UIControlStateNormal];
            [_statusButton setTitle:@"已领完" forState:UIControlStateNormal];
            self.countLabel.text = [NSString stringWithFormat:@"名额%@/%@",self.model.currentNum,self.model.totalNum];
        }
//    }
}

- (void)detailAction{
    QJProductDetailViewController *detailVC = [[QJProductDetailViewController alloc] init];
    QJSearchGameDetailModel *detailModel = [QJSearchGameDetailModel new];
    detailModel.ID = self.model.gameId;
    detailVC.model = detailModel;
    [[QJAppTool getCurrentViewController].navigationController pushViewController:detailVC animated:YES];
}

- (void)btnAction{
    DLog(@"去完成");
    if (self.btnClick) {
        self.btnClick(self.model);
    }
}

@end
