//
//  QJTaskAndCoinCollectionCell.m
//  QJBox
//
//  Created by rui on 2022/7/5.
//

#import "QJTaskAndCoinCollectionCell.h"

/* 自定义带图片的view */
#import "QJImageAndLabelView.h"

@interface QJTaskAndCoinCollectionCell ()

/* 白色视图 */
@property (nonatomic, strong) UIView *whiteView;
/* 标题 */
@property (nonatomic, strong) UIImageView *iconImageView;
/* 标题 */
@property (nonatomic, strong) UILabel *titleLabel;
/* 来源 */
@property (nonatomic, strong) QJImageAndLabelView *tagLabel;
//任务按钮
@property (nonatomic, strong) UIButton *taskButton;
@property (nonatomic, strong) UIButton *taskContentButton;

@end

@implementation QJTaskAndCoinCollectionCell

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        /* 初始化UI */
        [self initUI];
        /* 添加约束 */
        [self makeConstraints];
    }
    return self;
}

/**
 * @author: zjr
 * @date: 2022-7-5
 * @desc: 初始化UI
 */
- (void)initUI{
    [self.contentView addSubview:self.whiteView];
    [self.whiteView addSubview:self.iconImageView];
    [self.whiteView addSubview:self.tagLabel];
    [self.whiteView addSubview:self.titleLabel];
    [self.contentView addSubview:self.taskButton];
//    [self.whiteView addSubview:self.taskContentButton];
}

/**
 * @author: zjr
 * @date: 2022-7-5
 * @desc: 添加约束
 */
- (void)makeConstraints{
    [self.whiteView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.contentView);
        make.bottom.equalTo(self.contentView).offset(-5);
    }];
    
//    [self.taskContentButton mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.left.right.equalTo(self.whiteView);
//        make.bottom.equalTo(self.whiteView).offset(-30);
//    }];
    
    [self.taskButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.whiteView).offset(7);
        make.centerX.equalTo(self.whiteView);
        make.height.equalTo(@(23*kWScale));
        make.width.equalTo(@(67*kWScale));
    }];
    
    [self.tagLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.taskButton.mas_top).offset(-13);
        make.left.equalTo(self.whiteView).offset(5);
        make.right.equalTo(self.whiteView).offset(-5);
        make.height.equalTo(@(14*kWScale));
    }];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.tagLabel.mas_top).offset(-7);
        make.left.equalTo(self.whiteView).offset(5);
        make.right.equalTo(self.whiteView).offset(-5);
        make.height.equalTo(@(14*kWScale));
    }];
    
    [self.iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.titleLabel.mas_top).offset(-7);
        make.left.equalTo(self.whiteView).offset(5);
        make.right.equalTo(self.whiteView).offset(-5);
        make.top.equalTo(self.whiteView).offset(5);
    }];
}

/**
 * @author: zjr
 * @date: 2022-7-5
 * @desc: 数据源赋值
 */
- (void)setModel:(QJTaskDetailModel *)model{
    _model = model;
    
    self.whiteView.backgroundColor = _model.backgroundColor;
    _taskButton.backgroundColor = _model.buttonBackgroundColor;
    _titleLabel.text = _model.name;
    if ([_model.rewardEnum isEqualToString:@"EXPERIENCE"]) {
        [_tagLabel setText:[NSString stringWithFormat:@"可获得%ld经验值",_model.rewardCount] withIconImage:[UIImage imageNamed:@"qj_smallicon_experience"] withImageDirectly:QJImageLeft];
    }else{
        [_tagLabel setText:[NSString stringWithFormat:@"可获得%ld币",_model.rewardCount] withIconImage:[UIImage imageNamed:@"qj_smallicon_icoin"] withImageDirectly:QJImageLeft];
    }
    self.iconImageView.imageURL = [NSURL URLWithString:[_model.icon checkImageUrlString]];

    if (_model.isSelect) {
        _taskButton.titleLabel.font = kFont(12);
        _taskButton.layer.cornerRadius = 12;
        [self.taskButton mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.whiteView).offset(7);
            make.centerX.equalTo(self.whiteView);
            make.height.equalTo(@(23*kWScale));
            make.width.equalTo(@(52*kWScale));
        }];
    }else{
        _taskButton.titleLabel.font = kFont(9);
        _taskButton.layer.cornerRadius = 8;
        [self.taskButton mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.whiteView).offset(7);
            make.centerX.equalTo(self.whiteView);
            make.height.equalTo(@(16*kWScale));
            make.width.equalTo(@(48*kWScale));
        }];
    }
    
    if ([model.status isEqualToString:@"RECEIVED"]) {
        //已领取
        [_taskButton setTitle:@"已完成" forState:UIControlStateNormal];
    }else if ([model.status isEqualToString:@"UNCOMPLETED"]) {
        //未完成
        [_taskButton setTitle:@"去完成" forState:UIControlStateNormal];
    }else if ([model.status isEqualToString:@"UNRECEIVED"]) {
        //未领取
        [_taskButton setTitle:@"领取" forState:UIControlStateNormal];
    }else if ([model.status isEqualToString:@"EXPIRED"]) {
        //已过期
        [_taskButton setTitle:@"已过期" forState:UIControlStateNormal];
    }
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    UIView *view = [super hitTest:point withEvent:event];
    if (view == self) {
        return nil;
    }
    return view;
}

#pragma mark - Lazy Loading
- (UIView *)whiteView {
    if (!_whiteView) {
        _whiteView = [UIView new];
        _whiteView.backgroundColor = UIColorFromRGB(0xFFF9E9);
        _whiteView.layer.cornerRadius = 8;
        _whiteView.layer.masksToBounds = YES;
    }
    return _whiteView;
}

- (UIImageView *)iconImageView{
    if (!_iconImageView) {
        _iconImageView = [UIImageView new];
    }
    return _iconImageView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.font = kFont(11);
        _titleLabel.textColor = [UIColor colorWithHexString:@"0x8D3C0F"];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.text = @"";
    }
    return _titleLabel;
}

- (QJImageAndLabelView *)tagLabel {
    if (!_tagLabel) {
        _tagLabel = [[QJImageAndLabelView alloc] initWithImage:nil withTitle:@"" withFont:kFont(9) withtextColor:[UIColor colorWithHexString:@"0x8D3C0F"] withSpace:2 withImageDirectly:QJImageLeft];
//        _tagLabel.font = kFont(9);
//        _tagLabel.textColor = [UIColor colorWithHexString:@"0x8D3C0F"];
//        _tagLabel.textAlignment = NSTextAlignmentCenter;
//        _tagLabel.numberOfLines = 2;
//        _tagLabel.text = @"";
    }
    return _tagLabel;
}

//- (QJImageAndLabelView *)scoreLabel {
//    if (!_scoreLabel) {
//        _scoreLabel = [[QJImageAndLabelView alloc] initWithImage:[UIImage imageNamed:@""] withTitle:@"今日可获10币" withFont:kFont(12) withtextColor:UIColorFromRGB(0x999999) withSpace:5 withImageDirectly:QJImageLeft];
//    }
//    return _scoreLabel;
//}

//- (UIButton *)taskContentButton {
//    if (!_taskContentButton) {
//        _taskContentButton = [UIButton buttonWithType:UIButtonTypeCustom];
//        [_taskContentButton addTarget:self action:@selector(btnContentAction) forControlEvents:UIControlEventTouchUpInside];
//    }
//    return _taskContentButton;
//}

- (UIButton *)taskButton {
    if (!_taskButton) {
        _taskButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_taskButton addTarget:self action:@selector(btnAction) forControlEvents:UIControlEventTouchUpInside];
        _taskButton.titleLabel.font = kFont(9);
        _taskButton.backgroundColor = UIColorFromRGB(0xFFDD87);
        [_taskButton setTitle:@"去完成" forState:UIControlStateNormal];
        [_taskButton setTitleColor:UIColorFromRGB(0xFFFFFF) forState:UIControlStateNormal];
        _taskButton.layer.cornerRadius = 10;
        _taskButton.layer.masksToBounds = YES;

    }
    return _taskButton;
}

/**
 * @author: zjr
 * @date: 2022-7-5
 * @desc: button点击事件
 */
- (void)btnAction{
    DLog(@"去完成");
    if (self.btnClick) {
        self.btnClick(_model);
    }
}

//- (void)btnContentAction{
//    DLog(@"去完成");
//    if (self.contentBtnClick) {
//        self.contentBtnClick(_model);
//    }
//}

@end

