//
//  QJTaskAndCoinCell.h
//  QJBox
//
//  Created by rui on 2022/7/4.
//

#import "QJBaseTableViewCell.h"
/* model */
#import "QJTaskListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJTaskAndCoinCell : QJBaseTableViewCell

@property (nonatomic, strong) QjTaskModel *model;
/* 选择任务block */
@property (nonatomic, copy) void (^btnClick)(QJTaskDetailModel *model);
/* 全部任务block */
@property (nonatomic, copy) void (^moreBtnClick)(void);
@property (nonatomic, copy) void (^contentBtnClick)(QJTaskDetailModel *model);

@end

NS_ASSUME_NONNULL_END
