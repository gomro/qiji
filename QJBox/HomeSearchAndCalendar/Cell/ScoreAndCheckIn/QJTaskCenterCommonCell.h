//
//  QJTaskCenterCommonCell.h
//  QJBox
//
//  Created by rui on 2022/8/15.
//

#import <UIKit/UIKit.h>
#import "QJTaskCenterCommonModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJTaskCenterCommonCell : UITableViewCell

@property (nonatomic, strong) QJTaskCenterCommonDetailModel *model;

@end

NS_ASSUME_NONNULL_END
