//
//  QJCalendarCustomCell.h
//  QJBox
//
//  Created by rui on 2022/7/18.
//

#import <FSCalendar/FSCalendar.h>
/* model */
#import "QJSignedModel.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, SelectionType) {
    SelectionTypeNone,
    SelectionTypeHidden,
    SelectionTypeSingle,
    SelectionTypeLeftBorder,
    SelectionTypeMiddle,
    SelectionTypeRightBorder
};

@interface QJCalendarCustomCell : FSCalendarCell

@property (nonatomic, strong) UIImageView *circleImageView;

@property (nonatomic, weak) CAShapeLayer *selectionLayer;
@property (nonatomic, weak) CAShapeLayer *titleSelectLayer;

@property (nonatomic, strong) UILabel *customTitleLabel;
@property (nonatomic, strong) NSDate *selectedDate;
@property (nonatomic, strong) NSDate *minDate;

@property (nonatomic, strong) UILabel *subCustomTitleLabel;

@property (nonatomic, assign) SelectionType selectionType;

@end

NS_ASSUME_NONNULL_END
