//
//  QJCheckInCalendarCollectionCell.m
//  QJBox
//
//  Created by rui on 2022/6/28.
//

#import "QJCheckInCalendarCollectionCell.h"

@interface QJCheckInCalendarCollectionCell ()

/* 进度view */
@property (nonatomic, strong) UIView *leftView;
@property (nonatomic, strong) UIView *rightView;
/* 日期Label */
@property (nonatomic, strong) UILabel *dateLabel;
@property (nonatomic, strong) UIImageView *checkInImageView;
/* 奖励label */
@property (nonatomic, strong) UILabel *awardLabel;
/* 奖励图标 */
@property (nonatomic, strong) UIImageView *giftImageView;

@end

@implementation QJCheckInCalendarCollectionCell

- (instancetype)initWithFrame:(CGRect)frame {
   if (self = [super initWithFrame:frame]) {
       self.contentView.backgroundColor = UIColorFromRGB(0xFFFFFF);
       /* 初始化UI */
       [self initUI];
       /* 添加约束 */
       [self makeConstraints];
   }
   return self;
}

/**
 * @author: zjr
 * @date: 2022-6-28
 * @desc: 初始化UI
 */
- (void)initUI{
    [self.contentView addSubview:self.dateLabel];
    [self.contentView addSubview:self.checkInImageView];
    [self.contentView addSubview:self.awardLabel];
    [self.contentView addSubview:self.giftImageView];
    [self.contentView addSubview:self.leftView];
    [self.contentView addSubview:self.rightView];
    [self.contentView bringSubviewToFront:self.giftImageView];
}

/**
 * @author: zjr
 * @date: 2022-7-1
 * @desc: 添加约束
 */
- (void)makeConstraints{
    [self.giftImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.contentView);
        make.height.equalTo(@30);
        make.width.equalTo(@30);
        make.top.equalTo(self.contentView).offset(20);
    }];
    
    [self.leftView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView);
        make.height.equalTo(@2);
        make.width.equalTo(@(self.contentView.width/2));
//        make.centerY.equalTo(self.giftImageView);
        make.top.equalTo(self.contentView).offset(35);
    }];
    
    [self.rightView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerY.equalTo(self.giftImageView);
        make.top.equalTo(self.contentView).offset(35);
        make.right.equalTo(self.contentView);
        make.height.equalTo(@2);
        make.width.equalTo(@(self.contentView.width/2));
    }];
    
    [self.checkInImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.contentView);
        make.height.equalTo(@10);
        make.width.equalTo(@11);
        make.bottom.equalTo(self.contentView).offset(-10);
    }];
    
    [self.dateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.contentView);
        make.height.equalTo(@10);
        make.left.right.equalTo(self.contentView);
        make.bottom.equalTo(self.contentView).offset(-10);
    }];
    
    [self.awardLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.contentView);
        make.height.equalTo(@11);
        make.width.equalTo(@25);
        make.bottom.equalTo(self.dateLabel.mas_top).offset(-5);
    }];
}

#pragma mark - Lazy Loading
- (UILabel *)awardLabel {
    if (!_awardLabel) {
        _awardLabel = [UILabel new];
        _awardLabel.font = kFont(8);
        _awardLabel.textColor = [UIColor colorWithHexString:@"0xFFFFFF"];
        _awardLabel.backgroundColor = UIColorFromRGB(0xFFD59F);
        _awardLabel.textAlignment = NSTextAlignmentCenter;
        _awardLabel.text = @"+6";
        _awardLabel.layer.cornerRadius = 5.5;
        _awardLabel.layer.masksToBounds = YES;
    }
    return _awardLabel;
}

- (UIView *)leftView {
    if (!_leftView) {
        _leftView = [UIView new];
        _leftView.backgroundColor = [UIColor colorWithHexString:@"0xEDA94D"];
    }
    return _leftView;
}

- (UIView *)rightView {
    if (!_rightView) {
        _rightView = [UIView new];
        _rightView.backgroundColor = [UIColor colorWithHexString:@"0xEDA94D"];
    }
    return _rightView;
}

- (UILabel *)dateLabel {
    if (!_dateLabel) {
        _dateLabel = [UILabel new];
        _dateLabel.font = kFont(8);
        _dateLabel.textColor = [UIColor colorWithHexString:@"0xEDA94D"];
        _dateLabel.textAlignment = NSTextAlignmentCenter;
        _dateLabel.text = @"6-28";
    }
    return _dateLabel;
}

- (UIImageView *)checkInImageView{
    if (!_checkInImageView) {
        _checkInImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"signin_isyes"]];
    }
    return _checkInImageView;
}

- (UIImageView *)giftImageView{
    if (!_giftImageView) {
        _giftImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"sign_icoin"]];
    }
    return _giftImageView;
}

- (void)setModel:(QJSignedItemDetailModel *)model{
    _model = model;
    
    _awardLabel.text = [NSString stringWithFormat:@"+%@",_model.point];
    _dateLabel.text = _model.weekDay;
    _dateLabel.layer.borderColor = [UIColor clearColor].CGColor;
    _dateLabel.layer.borderWidth = 0;
    /*
     haveSigned 是否签到
     supply 是否补签
     bigPresent 是否有大礼包
     
     sign_icoin 未签到 40*40
     sign_icoin_not 已签到
     sign_icoin_repair 补签成功
     sign_icoin_gray 忘签到 30*30
     sign_icoin_giftnoopen 礼物未打开 46*46
     sign_icoin_gift 礼物已经打开  46*53
     */
    
    NSString *today = [_model.today stringByReplacingOccurrencesOfString:@"-" withString:@""];
    NSString *day = [_model.date stringByReplacingOccurrencesOfString:@"-" withString:@""];
    //小于当前日期
    self.checkInImageView.hidden = YES;
    _dateLabel.hidden = NO;

    [self.giftImageView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.contentView);
        make.height.equalTo(@30);
        make.width.equalTo(@30);
        make.top.equalTo(self.contentView).offset(20);
    }];
    
    _awardLabel.backgroundColor = UIColorFromRGB(0xFAE5CA);
    _dateLabel.textColor = UIColorFromRGB(0xFAE5CA);
    if (day.integerValue < today.integerValue) {
        if (_model.haveSigned) {
            self.checkInImageView.hidden = NO;
            _dateLabel.hidden = YES;
            self.giftImageView.image = [UIImage imageNamed:@"sign_icoin_not"];
            if (_model.supply) {
//                self.checkInImageView.hidden = YES;
//                _dateLabel.hidden = NO;
//                _dateLabel.text = @"补签";
                self.giftImageView.image = [UIImage imageNamed:@"sign_icoin_repair"];
            }
        }else if (_model.supply) {
            _dateLabel.text = @"补签";
            self.giftImageView.image = [UIImage imageNamed:@"sign_icoin_repair"];
        }else{
            _dateLabel.text = @"补签";
//            self.giftImageView.image = [UIImage imageNamed:@"sign_icoin_repair"];
            self.giftImageView.image = [UIImage imageNamed:@"sign_icoin_gray"];
        }
    }else if (day.integerValue == today.integerValue) {
        _dateLabel.text = @"今天";
        _dateLabel.hidden = NO;
        self.checkInImageView.hidden = YES;

        if (_model.haveSigned) {
            self.giftImageView.image = [UIImage imageNamed:@"sign_icoin_not"];
        }else{
            self.giftImageView.image = [UIImage imageNamed:@"sign_icoin"];
            [self.giftImageView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.centerX.equalTo(self.contentView);
                make.height.equalTo(@30);
                make.width.equalTo(@30);
                make.top.equalTo(self.contentView).offset(20);
            }];
        }
    }else{
        //大于当前日期
        self.giftImageView.image = [UIImage imageNamed:@"sign_icoin"];
        _awardLabel.backgroundColor = UIColorFromRGB(0xEE6A44);
        _dateLabel.textColor = UIColorFromRGB(0xEE6A44);
        [self.giftImageView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.contentView);
            make.height.equalTo(@30);
            make.width.equalTo(@30);
            make.top.equalTo(self.contentView).offset(20);
        }];
    }
    
    /* 是否显示礼物 */
    if (_model.bigPresent) {
        if (_model.received) {
            [self.giftImageView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.centerX.equalTo(self.contentView);
                make.height.equalTo(@53);
                make.width.equalTo(@46);
                make.top.equalTo(self.contentView).offset(-10);
            }];
            self.giftImageView.image = [UIImage imageNamed:@"sign_icoin_gift"];
        }else{
            [self.giftImageView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.centerX.equalTo(self.contentView);
                make.height.equalTo(@46);
                make.width.equalTo(@46);
                make.top.equalTo(self.contentView).offset(10);
            }];
            self.giftImageView.image = [UIImage imageNamed:@"sign_icoin_giftnoopen"];
        }
    }
    
    _leftView.hidden = NO;
    _rightView.hidden = NO;
    if ([_model.chooseIndexType isEqualToString:@"first"]) {
        _leftView.hidden = YES;
    }
    
    if ([_model.chooseIndexType isEqualToString:@"last"]) {
        _rightView.hidden = YES;
    }
}

@end
