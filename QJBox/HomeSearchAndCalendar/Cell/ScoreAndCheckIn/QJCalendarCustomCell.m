//
//  QJCalendarCustomCell.m
//  QJBox
//
//  Created by rui on 2022/7/18.
//

#import "QJCalendarCustomCell.h"
#import "FSCalendarExtensions.h"

@implementation QJCalendarCustomCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        //sign_icoin_gift
        UIImageView *circleImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"sign_icoin_giftnoopen"]];
//        [self.contentView insertSubview:circleImageView atIndex:0];
        [self.contentView addSubview:circleImageView];
        self.circleImageView = circleImageView;

        self.shapeLayer.hidden = YES;
        self.titleLabel.hidden = YES;
        
        self.customTitleLabel = [UILabel new];
        self.customTitleLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:self.customTitleLabel];
        
        CAShapeLayer *selectionLayer = [[CAShapeLayer alloc] init];
        selectionLayer.fillColor = [UIColor colorWithRed:1 green:0.584 blue:0 alpha:0.31].CGColor;
        selectionLayer.actions = @{@"hidden":[NSNull null]};
        [self.contentView.layer insertSublayer:selectionLayer below:self.customTitleLabel.layer];
        self.selectionLayer = selectionLayer;

        CAShapeLayer *titleSelectLayer = [[CAShapeLayer alloc] init];
        titleSelectLayer.fillColor = [UIColor colorWithRed:1 green:0.584 blue:0 alpha:1].CGColor;
        titleSelectLayer.actions = @{@"hidden":[NSNull null]};
        [self.contentView.layer insertSublayer:titleSelectLayer below:self.customTitleLabel.layer];
        self.titleSelectLayer = titleSelectLayer;
        self.titleSelectLayer.hidden = YES;
        
        self.subCustomTitleLabel = [UILabel new];
        self.subCustomTitleLabel.font = kFont(10);
        self.subCustomTitleLabel.textColor = UIColorFromRGB(0xFF9500);
        self.subCustomTitleLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:self.subCustomTitleLabel];
        [self.subCustomTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.contentView).offset(-5);;
            make.left.equalTo(self.contentView).offset(5);
            make.right.equalTo(self.contentView).offset(-5);
            make.height.equalTo(@(18*kWScale));
        }];
        
        self.backgroundView = [[UIView alloc] initWithFrame:self.bounds];
        self.backgroundView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.1];
    }
    return self;
}

- (void)layoutSubviews{
    [super layoutSubviews];
    
    self.customTitleLabel.frame = self.titleLabel.frame;
    self.customTitleLabel.height = 30*kWScale;
    self.customTitleLabel.text = self.titleLabel.text;
    self.customTitleLabel.font = self.titleLabel.font;

    self.backgroundView.frame = CGRectInset(self.bounds, 1, 1);
    self.circleImageView.frame = self.titleLabel.frame;
    self.circleImageView.height = 30*kWScale;
    self.circleImageView.width = 30*kWScale;
    self.circleImageView.centerX = self.width/2;
    
    self.selectionLayer.frame = CGRectMake(0, 0, self.bounds.size.width, self.customTitleLabel.fs_height);
//    self.selectionLayer.frame = CGRectMake(0, 0, self.customTitleLabel.fs_width, self.customTitleLabel.fs_height);//self.customTitleLabel.bounds;
    self.titleSelectLayer.frame = self.customTitleLabel.bounds;

//    self.customTitleLabel.textColor = self.titleLabel.textColor;
    
//    if (self.selectedDate == self.calendar.selectedDate) {
//        self.titleSelectLayer.hidden = NO;
//    }else{
//        self.titleSelectLayer.hidden = YES;
//    }
    
    if (self.selectionType == SelectionTypeMiddle) {
        self.selectionLayer.path = [UIBezierPath bezierPathWithRect:self.selectionLayer.bounds].CGPath;
    } else if (self.selectionType == SelectionTypeLeftBorder) {
        CGFloat diameter = MIN(self.selectionLayer.fs_height, self.selectionLayer.fs_width);

        self.selectionLayer.path = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(self.contentView.fs_width/2-diameter/2, self.customTitleLabel.fs_height/2-diameter/2, diameter/2+self.bounds.size.width/2, diameter) byRoundingCorners:UIRectCornerTopLeft|UIRectCornerBottomLeft cornerRadii:CGSizeMake(self.selectionLayer.fs_width/2, self.selectionLayer.fs_width/2)].CGPath;

//        self.selectionLayer.path = [UIBezierPath bezierPathWithRoundedRect:self.selectionLayer.bounds byRoundingCorners:UIRectCornerTopLeft|UIRectCornerBottomLeft cornerRadii:CGSizeMake(self.selectionLayer.fs_width/2, self.selectionLayer.fs_width/2)].CGPath;
    } else if (self.selectionType == SelectionTypeRightBorder) {
        CGFloat diameter = MIN(self.selectionLayer.fs_height, self.selectionLayer.fs_width);

        self.selectionLayer.path = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, self.customTitleLabel.fs_height/2-diameter/2, diameter/2+self.bounds.size.width/2, diameter) byRoundingCorners:UIRectCornerTopRight|UIRectCornerBottomRight cornerRadii:CGSizeMake(self.selectionLayer.fs_width/2, self.selectionLayer.fs_width/2)].CGPath;

//        self.selectionLayer.path = [UIBezierPath bezierPathWithRoundedRect:self.selectionLayer.bounds byRoundingCorners:UIRectCornerTopRight|UIRectCornerBottomRight cornerRadii:CGSizeMake(self.selectionLayer.fs_width/2, self.selectionLayer.fs_width/2)].CGPath;
    } else if (self.selectionType == SelectionTypeSingle) {
        CGFloat diameter = MIN(self.selectionLayer.fs_height, self.selectionLayer.fs_width);
        self.selectionLayer.path = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(self.contentView.fs_width/2-diameter/2, self.customTitleLabel.fs_height/2-diameter/2, diameter, diameter)].CGPath;
    }
    
    CGFloat titleDiameter = MIN(self.titleSelectLayer.fs_height, self.titleSelectLayer.fs_width);
    self.titleSelectLayer.path = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(self.contentView.fs_width/2-titleDiameter/2, self.customTitleLabel.fs_height/2-titleDiameter/2, titleDiameter, titleDiameter)].CGPath;

}

- (void)configureAppearance
{
    [super configureAppearance];
    // Override the build-in appearance configuration
//    if (self.isPlaceholder) {
//        self.titleLabel.textColor = UIColorFromRGB(0xFF9500);
//        self.eventIndicator.hidden = YES;
//    }
}

- (void)setSelectionType:(SelectionType)selectionType
{
    if (_selectionType != selectionType) {
        _selectionType = selectionType;
        [self setNeedsLayout];
    }
}

- (void)setMinDate:(NSDate *)minDate{
    if (_minDate != minDate) {
        _minDate = minDate;
    }
}

@end
