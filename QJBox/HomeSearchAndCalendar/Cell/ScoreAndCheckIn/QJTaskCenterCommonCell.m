//
//  QJTaskCenterCommonCell.m
//  QJBox
//
//  Created by rui on 2022/8/15.
//

#import "QJTaskCenterCommonCell.h"

@interface QJTaskCenterCommonCell ()

//白色底图
@property (nonatomic, strong) UIView *whiteBgView;
//任务区服
@property (nonatomic, strong) UILabel *areaTitleLabel;
@property (nonatomic, strong) UILabel *areaSubTitleLabel;
//任务介绍
@property (nonatomic, strong) UILabel *introTitleLabel;
@property (nonatomic, strong) UILabel *introSubTitleLabel;
@property (nonatomic, strong) UIStackView *introView;
//任务说明
@property (nonatomic, strong) UILabel *explainTitleLabel;
@property (nonatomic, strong) UIStackView *explainView;
@property (nonatomic, strong) UILabel *explainSubTitleLabel;
//任务开始时间
@property (nonatomic, strong) UILabel *startTimeTitleLabel;
@property (nonatomic, strong) UILabel *startTimeSubTitleLabel;
//任务结束时间
@property (nonatomic, strong) UILabel *endTimeTitleLabel;
@property (nonatomic, strong) UILabel *endTimeSubTitleLabel;
//任务状态
@property (nonatomic, strong) UILabel *statusLabel;

@end

@implementation QJTaskCenterCommonCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor clearColor];
        self.contentView.backgroundColor = [UIColor clearColor];

        /* 初始化UI */
        [self initUI];
        /* 添加约束 */
        [self makeConstraints];
    }
    return self;
}

/**
 * @author: zjr
 * @date: 2022-6-27
 * @desc: 初始化UI
 */
- (void)initUI{
    [self.contentView addSubview:self.whiteBgView];
    [self.whiteBgView addSubview:self.areaTitleLabel];
    [self.whiteBgView addSubview:self.areaSubTitleLabel];
    [self.whiteBgView addSubview:self.introTitleLabel];
    
    [self.whiteBgView addSubview:self.introView];
    [self.whiteBgView addSubview:self.explainView];
    
//    [self.whiteBgView addSubview:self.introSubTitleLabel];
    [self.whiteBgView addSubview:self.explainTitleLabel];
//    [self.whiteBgView addSubview:self.explainSubTitleLabel];
    [self.whiteBgView addSubview:self.startTimeTitleLabel];
    [self.whiteBgView addSubview:self.startTimeSubTitleLabel];
    [self.whiteBgView addSubview:self.endTimeTitleLabel];
    [self.whiteBgView addSubview:self.endTimeSubTitleLabel];
    [self.whiteBgView addSubview:self.statusLabel];
}

/**
 * @author: zjr
 * @date: 2022-7-1
 * @desc: 添加约束
 */
- (void)makeConstraints{
    [self.whiteBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(16);
        make.right.equalTo(self.contentView).offset(-16);
        make.top.bottom.equalTo(self.contentView);
    }];
    
    [self.areaSubTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.whiteBgView).offset(10);
        make.left.equalTo(self.whiteBgView).offset(68);
        make.right.equalTo(self.whiteBgView).offset(-22);
    }];
    
    [self.areaTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.areaSubTitleLabel.mas_top);
        make.left.equalTo(self.whiteBgView).offset(12);
    }];
    
    [self.introView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.areaSubTitleLabel.mas_bottom).offset(10);
        make.left.equalTo(self.whiteBgView).offset(68);
        make.right.equalTo(self.whiteBgView).offset(-22);
    }];
    
//    [self.introSubTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(self.areaSubTitleLabel.mas_bottom).offset(10);
//        make.left.equalTo(self.whiteBgView).offset(68);
//        make.right.equalTo(self.whiteBgView).offset(-22);
//    }];
    
    [self.introTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.introView.mas_top);
        make.left.equalTo(self.whiteBgView).offset(12);
    }];
    
    [self.explainView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.introView.mas_bottom).offset(10);
        make.left.equalTo(self.whiteBgView).offset(68);
        make.right.equalTo(self.whiteBgView).offset(-22);
    }];
    
//    [self.explainSubTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(self.introSubTitleLabel.mas_bottom).offset(10);
//        make.left.equalTo(self.whiteBgView).offset(68);
//        make.right.equalTo(self.whiteBgView).offset(-22);
//    }];
    
    [self.explainTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.explainView.mas_top);
        make.left.equalTo(self.whiteBgView).offset(12);
    }];
    
    [self.startTimeSubTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.explainView.mas_bottom).offset(10);
        make.left.equalTo(self.whiteBgView).offset(68);
        make.right.equalTo(self.whiteBgView).offset(-22);
    }];
    
    [self.startTimeTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.startTimeSubTitleLabel.mas_top);
        make.left.equalTo(self.whiteBgView).offset(12);
    }];
    
    [self.endTimeSubTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.startTimeSubTitleLabel.mas_bottom).offset(10);
        make.left.equalTo(self.whiteBgView).offset(68);
        make.right.equalTo(self.whiteBgView).offset(-22);
    }];
    
    [self.endTimeTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.endTimeSubTitleLabel.mas_top);
        make.left.equalTo(self.whiteBgView).offset(12);
    }];
    
    [self.statusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.whiteBgView).offset(-22);
        make.height.equalTo(@(15*kWScale));
        make.top.equalTo(self.endTimeSubTitleLabel.mas_bottom).offset(20);
        make.bottom.equalTo(self.whiteBgView).offset(-22);
    }];
}

#pragma mark - Lazy Loading
- (UIView *)whiteBgView {
    if (!_whiteBgView) {
        _whiteBgView = [UIView new];
        _whiteBgView.backgroundColor = [UIColor whiteColor];
    }
    return _whiteBgView;
}

- (UIStackView *)introView{
    if (!_introView) {
        _introView = [UIStackView new];
        _introView.axis = UILayoutConstraintAxisVertical;
        _introView.distribution = UIStackViewDistributionFill;
        _introView.spacing = 10;
    }
    return _introView;
}

- (UIStackView *)explainView{
    if (!_explainView) {
        _explainView = [UIStackView new];
        _explainView.axis = UILayoutConstraintAxisVertical;
        _explainView.distribution = UIStackViewDistributionFill;
        _explainView.spacing = 10;
    }
    return _explainView;
}

- (UILabel *)areaTitleLabel {
    if (!_areaTitleLabel) {
        _areaTitleLabel = [UILabel new];
        _areaTitleLabel.font = kFont(12);
        _areaTitleLabel.textColor = UIColorFromRGB(0x000000);
        _areaTitleLabel.text = @"任务区服";
    }
    return _areaTitleLabel;
}

- (UILabel *)areaSubTitleLabel {
    if (!_areaSubTitleLabel) {
        _areaSubTitleLabel = [UILabel new];
        _areaSubTitleLabel.font = kFont(12);
        _areaSubTitleLabel.textColor = UIColorFromRGB(0x474849);
        _areaSubTitleLabel.text = @"攻速冰雪 7月25号区";
    }
    return _areaSubTitleLabel;
}

- (UILabel *)introTitleLabel {
    if (!_introTitleLabel) {
        _introTitleLabel = [UILabel new];
        _introTitleLabel.font = kFont(12);
        _introTitleLabel.textColor = UIColorFromRGB(0x000000);
        _introTitleLabel.text = @"任务介绍";
    }
    return _introTitleLabel;
}

- (UILabel *)introSubTitleLabel {
    if (!_introSubTitleLabel) {
        _introSubTitleLabel = [UILabel new];
        _introSubTitleLabel.font = kFont(12);
        _introSubTitleLabel.textColor = UIColorFromRGB(0x474849);
        _introSubTitleLabel.text = @"攻速冰雪 打倒10个小boss";
    }
    return _introSubTitleLabel;
}

- (UILabel *)explainTitleLabel {
    if (!_explainTitleLabel) {
        _explainTitleLabel = [UILabel new];
        _explainTitleLabel.font = kFont(12);
        _explainTitleLabel.textColor = UIColorFromRGB(0x000000);
        _explainTitleLabel.text = @"任务说明";
    }
    return _explainTitleLabel;
}

- (UILabel *)explainSubTitleLabel {
    if (!_explainSubTitleLabel) {
        _explainSubTitleLabel = [UILabel new];
        _explainSubTitleLabel.font = kFont(12);
        _explainSubTitleLabel.textColor = UIColorFromRGB(0x474849);
        _explainSubTitleLabel.numberOfLines = 0;
        _explainSubTitleLabel.text = @"达到任务要求后需退出游戏才可领取奖励,游戏账号必须和盒子号一致 否则任务无法完成";
    }
    return _explainSubTitleLabel;
}

- (UILabel *)startTimeTitleLabel {
    if (!_startTimeTitleLabel) {
        _startTimeTitleLabel = [UILabel new];
        _startTimeTitleLabel.font = kFont(12);
        _startTimeTitleLabel.textColor = UIColorFromRGB(0x000000);
        _startTimeTitleLabel.text = @"开始时间";
    }
    return _startTimeTitleLabel;
}

- (UILabel *)startTimeSubTitleLabel {
    if (!_startTimeSubTitleLabel) {
        _startTimeSubTitleLabel = [UILabel new];
        _startTimeSubTitleLabel.font = kFont(12);
        _startTimeSubTitleLabel.textColor = UIColorFromRGB(0x474849);
        _startTimeSubTitleLabel.text = @"2022.07.30 19:00";
    }
    return _startTimeSubTitleLabel;
}

- (UILabel *)endTimeTitleLabel {
    if (!_endTimeTitleLabel) {
        _endTimeTitleLabel = [UILabel new];
        _endTimeTitleLabel.font = kFont(12);
        _endTimeTitleLabel.textColor = UIColorFromRGB(0x000000);
        _endTimeTitleLabel.text = @"结束时间";
    }
    return _endTimeTitleLabel;
}

- (UILabel *)endTimeSubTitleLabel {
    if (!_endTimeSubTitleLabel) {
        _endTimeSubTitleLabel = [UILabel new];
        _endTimeSubTitleLabel.font = kFont(12);
        _endTimeSubTitleLabel.textColor = UIColorFromRGB(0x474849);
        _endTimeSubTitleLabel.text = @"2022.07.31 19:00";
    }
    return _endTimeSubTitleLabel;
}

- (UILabel *)statusLabel {
    if (!_statusLabel) {
        _statusLabel = [UILabel new];
        _statusLabel.font = kFont(12);
        _statusLabel.textColor = UIColorFromRGB(0x4C1C0F);
        _statusLabel.text = @"已开始";
    }
    return _statusLabel;
}

/**
 * @author: zjr
 * @date: 2022-6-30
 * @desc: 数据赋值
 */
- (void)setModel:(QJTaskCenterCommonDetailModel *)model{
    if (_model != model) {
        _model = model;
        [self.introView removeAllSubviews];
        [self.explainView removeAllSubviews];
        self.areaSubTitleLabel.text = _model.gameServerName;
        self.startTimeSubTitleLabel.text = _model.startTime;
        self.endTimeSubTitleLabel.text = _model.endTime;
        for (int i = 0; i < _model.rulesDesc.count; i ++) {
            UILabel *testLabel = [UILabel new];
            testLabel.font = kFont(12);
            testLabel.textColor = UIColorFromRGB(0x474849);
            testLabel.numberOfLines = 0;
            testLabel.text = [_model.rulesDesc safeObjectAtIndex:i];
            [self.introView addArrangedSubview:testLabel];
        }
        
        for (int i = 0; i < _model.taskDesc.count; i ++) {
            UILabel *testLabel = [UILabel new];
            testLabel.font = kFont(12);
            testLabel.textColor = UIColorFromRGB(0x474849);
            testLabel.numberOfLines = 0;
            testLabel.text = [_model.taskDesc safeObjectAtIndex:i];
            [self.explainView addArrangedSubview:testLabel];
        }
        
        _statusLabel.textColor = UIColorFromRGB(0x4C1C0F);
        if ([_model.status isEqualToString:@"0"]) {
            _statusLabel.text = @"已开始";
        }else if ([_model.status isEqualToString:@"1"] && [model.receive isEqualToString:@"0"]) {
            _statusLabel.text = @"已完成";
        }else if ([_model.status isEqualToString:@"1"] && [model.receive isEqualToString:@"1"]) {
            _statusLabel.text = @"已抢光";
        }else{
            _statusLabel.textColor = UIColorFromRGB(0xA4A4A4);
            _statusLabel.text = @"已结束";
        }
    }
}

- (void)btnAction{
    DLog(@"去完成");
}

@end
