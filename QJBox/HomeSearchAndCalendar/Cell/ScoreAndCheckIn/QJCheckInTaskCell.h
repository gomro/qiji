//
//  QJCheckInTaskCell.h
//  QJBox
//
//  Created by rui on 2022/6/27.
//

#import "QJBaseTableViewCell.h"
/* 任务中心公共model */
#import "QJTaskCenterCommonModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJCheckInTaskCell : QJBaseTableViewCell

@property (nonatomic, strong) QJTaskCenterCommonDetailModel *model;
@property (nonatomic, copy) void (^btnClick)(QJTaskCenterCommonDetailModel *model);

@end

NS_ASSUME_NONNULL_END
