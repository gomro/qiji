//
//  QJTaskAndCoinCell.m
//  QJBox
//
//  Created by rui on 2022/7/4.
//

#import "QJTaskAndCoinCell.h"

/* 倒计时 */
//#import "QJCountDown.h"
/* 自定义Cell */
#import "QJTaskAndCoinCollectionCell.h"
/* 进度条 */
#import "QJPrograssView.h"
/* 任务view */
#import "QJTaskCommonView.h"
/* 自定义segmentview */
#import "QJSegmentImageView.h"

@interface QJTaskAndCoinCell ()<UIScrollViewDelegate>

//白色底图
@property (nonatomic, strong) UIView *whiteBgView;
@property (nonatomic, strong) UIImageView *titleImageView;
@property (nonatomic, strong) UIImageView *leftImageView;
@property (nonatomic, strong) UIImageView *rightImageView;
/* 标题 */
@property (nonatomic, strong) UILabel *titleLabel;
/* 副标题 */
@property (nonatomic, strong) UILabel *subTitleLabel;
//全部任务按钮
@property (nonatomic, strong) UIButton *allTaskButton;
//任务数量进度条
@property (nonatomic, strong) UIView *taskPrograssView;
@property (nonatomic, strong) UIImageView *taskIconImageView;
@property (nonatomic, strong) UILabel *taskLabel;
/* 标题 */
@property (nonatomic, strong) UIView *titleView;
@property (nonatomic, strong) QJSegmentImageView *segment;

/* 底部滚动视图 */
@property (nonatomic, strong) UIScrollView *scrollView;

@end

@implementation QJTaskAndCoinCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        /* 初始化UI */
        [self initUI];
        /* 添加约束 */
        [self makeConstraints];
    }
    return self;
}

/**
 * @author: zjr
 * @date: 2022-7-4
 * @desc: 初始化UI
 */
- (void)initUI{
    self.backgroundColor = [UIColor clearColor];
    self.contentView.backgroundColor = [UIColor clearColor];//UIColorFromRGB(0x5DC9A5);//[UIColor colorWithHexString:@"0xF5F5F5"];
    [self.contentView addSubview:self.whiteBgView];
    [self.contentView addSubview:self.titleImageView];
    [self.contentView addSubview:self.leftImageView];
    [self.contentView addSubview:self.rightImageView];
    
    [self.titleImageView addSubview:self.titleLabel];
    [self.titleImageView addSubview:self.subTitleLabel];
    
//    [self.whiteBgView addSubview:self.allTaskButton];
    [self.whiteBgView addSubview:self.taskPrograssView];
    [self.taskPrograssView addSubview:self.taskIconImageView];
    [self.taskPrograssView addSubview:self.taskLabel];
    [self.whiteBgView addSubview:self.scrollView];
    [self.whiteBgView addSubview:self.titleView];
    [self.titleView addSubview:self.segment];
}

/**
 * @author: zjr
 * @date: 2022-7-4
 * @desc: 添加约束
 */
- (void)makeConstraints{
    [self.titleImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.contentView);
        make.top.equalTo(self.contentView).offset(15);
    }];
    
    [self.leftImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(25);
        make.top.equalTo(self.contentView).offset(15);
    }];
    
    [self.rightImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView).offset(-45);
        make.top.equalTo(self.contentView).offset(15);
    }];
    
    [self.whiteBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(15);
        make.right.equalTo(self.contentView).offset(-15);
        make.top.equalTo(self.contentView).offset(51);
        make.bottom.equalTo(self.contentView);
    }];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleImageView).offset(13);
        make.centerX.equalTo(self.titleImageView);
        make.left.equalTo(self.titleImageView).offset(5);
        make.right.equalTo(self.titleImageView).offset(-5);
        make.height.equalTo(@(20*kWScale));
    }];
    
    [self.subTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom);
        make.centerX.equalTo(self.titleImageView);
        make.left.equalTo(self.titleImageView).offset(5);
        make.right.equalTo(self.titleImageView).offset(-5);
        make.height.equalTo(@(20*kWScale));
    }];
    
    [self.taskPrograssView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.whiteBgView).offset(42);
        make.height.equalTo(@(25*kWScale));
        make.left.equalTo(self.whiteBgView).offset(10);
        make.right.equalTo(self.whiteBgView).offset(-10);
    }];
    
    [self.taskIconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.taskPrograssView).offset(2.5);
        make.height.equalTo(@(20*kWScale));
        make.width.equalTo(@(20*kWScale));
        make.left.equalTo(self.taskPrograssView).offset(8);
    }];
    
    [self.taskLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.taskPrograssView).offset(2.5);
        make.height.equalTo(@(20*kWScale));
        make.left.equalTo(self.taskIconImageView.mas_right).offset(8);
        make.right.equalTo(self.taskPrograssView).offset(-10);
    }];
    
//    [self.allTaskButton mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(self.taskPrograssView.mas_bottom).offset(12);
//        make.width.equalTo(@90);
//        make.height.equalTo(@25);
//        make.right.equalTo(self.whiteBgView).offset(-10);
//    }];
    
    [self.titleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.taskPrograssView.mas_bottom);
        make.left.equalTo(self.whiteBgView);
        make.right.equalTo(self.whiteBgView).offset(-10);
        make.height.equalTo(@(40*kWScale));
    }];
    
    [self.segment mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleView);
        make.left.right.equalTo(self.titleView);
        make.height.equalTo(@(44*kWScale));
    }];
    
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.whiteBgView);
        make.top.equalTo(self.titleView.mas_bottom);
        make.height.equalTo(@(175*kWScale));
    }];
}

#pragma mark - Lazy Loading
- (UIView *)whiteBgView {
    if (!_whiteBgView) {
        _whiteBgView = [UIView new];
        _whiteBgView.backgroundColor = [UIColor whiteColor];
    }
    return _whiteBgView;
}

- (UIImageView *)titleImageView{
    if (!_titleImageView) {
        _titleImageView = [UIImageView new];
        _titleImageView.image = [UIImage imageNamed:@"sign_icoin_bg"];
    }
    return _titleImageView;
}

- (UIImageView *)leftImageView{
    if (!_leftImageView) {
        _leftImageView = [UIImageView new];
        _leftImageView.image = [UIImage imageNamed:@"icoin_lefticon"];
    }
    return _leftImageView;
}

- (UIImageView *)rightImageView{
    if (!_rightImageView) {
        _rightImageView = [UIImageView new];
        _rightImageView.image = [UIImage imageNamed:@"icoin_righticon"];
    }
    return _rightImageView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.font = [UIFont fontWithName:@"DFPZongYiW7" size:16*kWScale];
        _titleLabel.textColor = UIColorFromRGB(0x8D3C0F);
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.text = @"赚币乐园";
    }
    return _titleLabel;
}

- (UILabel *)subTitleLabel {
    if (!_subTitleLabel) {
        _subTitleLabel = [UILabel new];
        _subTitleLabel.font = kFont(11);
        _subTitleLabel.textColor = UIColorFromRGB(0x7A482E);
        _subTitleLabel.textAlignment = NSTextAlignmentCenter;
        _subTitleLabel.text = @"赚币乐园";
    }
    return _subTitleLabel;
}

- (UIButton *)allTaskButton {
    if (!_allTaskButton) {
        _allTaskButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_allTaskButton addTarget:self action:@selector(btnAction) forControlEvents:UIControlEventTouchUpInside];
        _allTaskButton.titleLabel.font = kFont(12);
        _allTaskButton.backgroundColor = UIColorFromRGB(0x8D3C0F);
        [_allTaskButton setTitle:@"全部任务" forState:UIControlStateNormal];
        [_allTaskButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _allTaskButton.layer.cornerRadius = 10;
        _allTaskButton.layer.masksToBounds = YES;

    }
    return _allTaskButton;
}

- (UIView *)taskPrograssView {
    if (!_taskPrograssView) {
        _taskPrograssView = [UIView new];
        _taskPrograssView.backgroundColor = [UIColor colorWithRed:0.942 green:0.738 blue:0.651 alpha:0.26];
        _taskPrograssView.layer.cornerRadius = 4;
        _taskPrograssView.layer.masksToBounds = YES;
    }
    return _taskPrograssView;
}

- (UIImageView *)taskIconImageView{
    if (!_taskIconImageView) {
        _taskIconImageView = [UIImageView new];
        _taskIconImageView.image = [UIImage imageNamed:@"task_icon"];
    }
    return _taskIconImageView;
}

- (UILabel *)taskLabel {
    if (!_taskLabel) {
        _taskLabel = [UILabel new];
        _taskLabel.font = kFont(12);
        _taskLabel.textColor = UIColorFromRGB(0x8D3C0F);
    }
    return _taskLabel;
}

- (UIView *)titleView {
    if (!_titleView) {
        _titleView = [UIView new];
        _titleView.backgroundColor = [UIColor whiteColor];//UIColorFromRGB(0xF7F7F7);
    }
    return _titleView;
}

- (QJSegmentImageView *)segment{
    if (!_segment) {
        _segment = [[QJSegmentImageView alloc] initWithFrame:CGRectMake(0, 0, QJScreenWidth, 44*kWScale)];
        _segment.itemSpace = 12;
        _segment.leftOffset = 15;
        _segment.font = kFont(14);
        _segment.selectedFont = kFont(16);
        _segment.textColor = UIColorFromRGB(0x707070);
        _segment.selectedTextColor = UIColorFromRGB(0x413012);
        _segment.showBottomLine = NO;
        _segment.backgroundColor = UIColorFromRGB(0xFFFFFF);
        [_segment setTitleArray:@[@"目标任务",@"周期任务",]];
        WS(weakSelf)
        _segment.segmentedSelectedItemBlock = ^(NSInteger selectedIndex) {
            [weakSelf selectItemIndex:selectedIndex];
        };
    }
    return _segment;
}

- (UIScrollView *)scrollView{
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.width, 190*kWScale)];
        _scrollView.scrollEnabled = NO;
        _scrollView.pagingEnabled = YES;
        _scrollView.bounces = NO;
        _scrollView.delegate = self;
        _scrollView.showsVerticalScrollIndicator = NO;
        _scrollView.showsHorizontalScrollIndicator = NO;
    }
    return _scrollView;
}

/**
 * @author: zjr
 * @date: 2022-7-4
 * @desc: 数据赋值
 */
- (void)setModel:(QjTaskModel *)model {
    _model = model;
    [self.whiteBgView layoutIfNeeded];
    [self.whiteBgView showCorner:4 rectCorner:UIRectCornerAllCorners];
    
    if (_model.indexPathRow == 0) {
        [self.leftImageView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(25);
            make.top.equalTo(self.contentView).offset(15);
        }];
        
        [self.rightImageView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.contentView).offset(-25);
            make.top.equalTo(self.contentView).offset(5);
        }];
    }else{
        [self.leftImageView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(25);
            make.top.equalTo(self.contentView).offset(15);
        }];
        
        [self.rightImageView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.contentView).offset(-25);
            make.top.equalTo(self.contentView).offset(15);
        }];
    }
    
    NSString *taskCountStr = [NSString stringWithFormat:@"今日还有%ld/%ld个任务可做，逾期不候，快去完成",_model.unCompletedCount,_model.totalCount];

    if ([_model.rewardEnum isEqualToString:@"EXPERIENCE"]) {
        _titleLabel.text = @"经验小屋";
        _subTitleLabel.text = @"快速升级 获取权益";
        
        _titleImageView.image = [UIImage imageNamed:@"sign_experience_bg"];
        _leftImageView.image = [UIImage imageNamed:@"experience_lefticon"];
        _rightImageView.image = [UIImage imageNamed:@"experience_righticon"];
    }else{
        _titleLabel.text = @"赚币乐园";
        _subTitleLabel.text = @"官方发布 有币不赚?";
        
        _titleImageView.image = [UIImage imageNamed:@"sign_icoin_bg"];
        _leftImageView.image = [UIImage imageNamed:@"icoin_lefticon"];
        _rightImageView.image = [UIImage imageNamed:@"icoin_righticon"];
    }
//    _taskLabel.text = taskCountStr;
    [_taskLabel setSingleStr:taskCountStr range:[taskCountStr rangeOfString:[NSString stringWithFormat:@"%ld/%ld",_model.unCompletedCount,_model.totalCount]] color:UIColorFromRGB(0xEE853F)];

    if (_model.taskGroups.count > 0) {
        NSMutableArray *titleArray = [NSMutableArray array];
        for (int i = 0; i < _model.taskGroups.count; i ++) {
            QJTaskCommonView *taskCommonView = [[QJTaskCommonView alloc] initWithFrame:CGRectMake(self.scrollView.width*i, 0, self.scrollView.width, self.scrollView.height)];
            QJTaskDetailModel *detailModel = [_model.taskGroups safeObjectAtIndex:i];
            [titleArray safeAddObject:detailModel.groupName];
            detailModel.rewardEnum = _model.rewardEnum;
            taskCommonView.model = detailModel;
            [self.scrollView addSubview:taskCommonView];
            WS(weakSelf)
            taskCommonView.btnClick = ^(QJTaskDetailModel * _Nonnull model) {
                if (weakSelf.btnClick) {
                    weakSelf.btnClick(model);
                }
            };
            taskCommonView.moreBtnClick = ^{
                [weakSelf btnAction];
            };
            taskCommonView.contentBtnClick = ^(QJTaskDetailModel * _Nonnull model) {
                if (weakSelf.contentBtnClick) {
                    weakSelf.contentBtnClick(model);
                }
            };
        }
        [_segment setTitleArray:titleArray];
        _scrollView.contentSize = CGSizeMake(_scrollView.width*_model.taskGroups.count, _scrollView.height);
    }
}

- (void)btnAction{
    if (self.moreBtnClick) {
        self.moreBtnClick();
    }
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    int page = (_scrollView.contentOffset.x+_scrollView.width/2)/_scrollView.width;
    [_segment setSegmentCurIndex:page];
//    [_segment setSegmentSelectedIndex:page];
}

- (void)selectItemIndex:(NSInteger)index {
//    if (index > self.childViews.count-1) {
//       index = self.childViews.count-1;
//    }
    
    if (index > 2) {
       index = 2;
    }
    
    [self.scrollView setContentOffset:CGPointMake(_scrollView.width * index, 0) animated:NO];
//    [[self.childViews yxk_objectAtIndex:index] searchWithType:self.type];
}

@end
