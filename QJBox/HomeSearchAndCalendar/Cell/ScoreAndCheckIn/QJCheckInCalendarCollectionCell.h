//
//  QJCheckInCalendarCollectionCell.h
//  QJBox
//
//  Created by rui on 2022/6/28.
//

#import <UIKit/UIKit.h>
/* model */
#import "QJSignedModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJCheckInCalendarCollectionCell : UICollectionViewCell

/* 数据源 */
@property (nonatomic, strong) QJSignedItemDetailModel *model;

@end

NS_ASSUME_NONNULL_END
