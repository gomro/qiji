//
//  QJSignedModel.m
//  QJBox
//
//  Created by rui on 2022/7/14.
//

#import "QJSignedModel.h"

@implementation QJSignedModel

+ (NSDictionary *)modelContainerPropertyGenericClass{
    return @{
             @"days" : [QJSignedItemDetailModel class],
             };
}

@end

@implementation QJSignedItemDetailModel

@end

