//
//  QJTaskListModel.h
//  QJBox
//
//  Created by rui on 2022/7/15.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@class QjTaskModel;
@class QJTaskDetailModel;

@interface QJTaskListModel : NSObject

@property (nonatomic, strong) QjTaskModel *qjcoinTask;

@property (nonatomic, strong) QjTaskModel *experienceTask;

@end

@interface QjTaskModel : NSObject

@property (nonatomic, strong) NSMutableArray <QJTaskDetailModel *>*taskGroups;

/* 索引 */
@property (nonatomic, assign) NSInteger indexPathRow;
/* 任务类型 EXPERIENCE-经验 QJCOIN-奇迹币 */
@property (nonatomic, copy) NSString *rewardEnum;
/* 未完成任务数 */
@property (nonatomic, assign) NSInteger unCompletedCount;
@property (nonatomic, assign) NSInteger totalCount;

@end

@interface QJTaskDetailModel : NSObject

/* 任务分类-周期任务/每日任务等 */
@property (nonatomic, copy) NSString *groupName;
/* 任务类型 EXPERIENCE-经验 QJCOIN-奇迹币 */
@property (nonatomic, copy) NSString *rewardEnum;

@property (nonatomic, strong) NSMutableArray <QJTaskDetailModel *>*userTaskDetails;
/* 任务跳转类型 1-加入营地，2-发送一条动态，3-成为攻略博主，4-浏览营地一分钟，5-更换默认头像，6-实名认证，7-连续7天签到，8-每日签到，9-完成新人礼 */
@property (nonatomic, assign) NSInteger code;
/* 任务ID */
@property (nonatomic, copy) NSString *ID;
/* 任务状态 UNCOMPLETED-未完成，UNRECEIVED-未领取，RECEIVED-已领取，EXPIRED-已过期 */
@property (nonatomic, copy) NSString *status;
/* 任务奖励 */
@property (nonatomic, assign) NSInteger rewardCount;
/* 任务名称 */
@property (nonatomic, copy) NSString *name;
/* 任务描述 */
@property (nonatomic, copy) NSString *desc;
@property (nonatomic, copy) NSString *detailDesc;
@property (nonatomic, copy) NSString *simpleDesc;

/* 任务icon */
@property (nonatomic, copy) NSString *icon;
/* 任务来源 */
@property (nonatomic, copy) NSString *publisher;

@property (nonatomic, assign) BOOL isSelect;
@property (nonatomic, strong) UIColor *backgroundColor;
@property (nonatomic, strong) UIColor *buttonBackgroundColor;

@end

NS_ASSUME_NONNULL_END
