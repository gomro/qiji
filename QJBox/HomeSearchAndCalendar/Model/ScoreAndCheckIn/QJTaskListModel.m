//
//  QJTaskListModel.m
//  QJBox
//
//  Created by rui on 2022/7/15.
//

#import "QJTaskListModel.h"

@implementation QJTaskListModel

@end

@implementation QjTaskModel

+ (NSDictionary *)modelContainerPropertyGenericClass{
    return @{
             @"taskGroups" : [QJTaskDetailModel class],
             };
}

@end

@implementation QJTaskDetailModel

+ (NSDictionary *)modelCustomPropertyMapper{
    return @{@"ID" : @[@"id"]};
}

+ (NSDictionary *)modelContainerPropertyGenericClass{
    return @{
             @"userTaskDetails" : [QJTaskDetailModel class],
             };
}

@end
