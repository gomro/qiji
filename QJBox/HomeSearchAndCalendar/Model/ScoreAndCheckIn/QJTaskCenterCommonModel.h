//
//  QJTaskCenterCommonModel.h
//  QJBox
//
//  Created by rui on 2022/8/19.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@class QJTaskCenterCommonDetailModel;

@interface QJTaskCenterCommonModel : NSObject

/* 数据源数组 */
@property (nonatomic, strong) NSMutableArray<QJTaskCenterCommonDetailModel *> *records;
/* 总条数 */
@property (nonatomic, strong) NSNumber *total;
/* 分页 */
@property (nonatomic, strong) NSNumber *page;
/* 当前页 */
@property (nonatomic, strong) NSNumber *current;
/* 是否能分页 */
@property (nonatomic, assign) BOOL hasNext;
/* 是否需要加载更多 */
@property (assign, nonatomic) BOOL willLoadMore;
/* 分页 */
- (NSDictionary *)toParams;
/* 解析model */
-(void)configObj:(QJTaskCenterCommonModel *)model;

@end

@interface QJTaskCenterCommonDetailModel : NSObject

/* 审核状态 */
@property (nonatomic, strong) NSString *checkState;
/* 失效/下架原因 */
@property (nonatomic, strong) NSString *checkReason;
/* 任务id */
@property (nonatomic, strong) NSString *ID;
/* 审核时间 */
@property (nonatomic, strong) NSString *checkTime;
/* 审核ID */
@property (nonatomic, strong) NSString *checkerId;
/* 创建时间 */
@property (nonatomic, strong) NSString *createTime;
/* 创建ID */
@property (nonatomic, strong) NSString *createrId;
/* 任务剩余数量 */
@property (nonatomic, strong) NSString *currentNum;
/* 结束时间 */
@property (nonatomic, strong) NSString *endTime;
/* 结束时间戳 */
@property (nonatomic, strong) NSString *endTimeMillis;
/* 游戏ID */
@property (nonatomic, strong) NSString *gameId;
/* 游戏Icon */
@property (nonatomic, strong) NSString *icon;
/* 游戏名称 */
@property (nonatomic, strong) NSString *gameName;
/* 游戏区服名称 */
@property (nonatomic, strong) NSString *gameServerName;
/* 游戏区服ID  */
@property (nonatomic, strong) NSString *gameServerId;
/* 任务名称  */
@property (nonatomic, strong) NSString *name;
/* 奇迹币数值  */
@property (nonatomic, strong) NSString *qjcoinNum;
/* 奖励类型  */
@property (nonatomic, strong) NSString *rewardType;
@property (nonatomic, strong) NSString *rewardDesc;
/* 开始时间  */
@property (nonatomic, strong) NSString *startTime;
/* 开始时间戳  */
@property (nonatomic, strong) NSString *startTimeMillis;
/* 任务说明数组 */
@property (nonatomic, strong) NSMutableArray *taskDesc;
/* 任务介绍数组 */
@property (nonatomic, strong) NSMutableArray *rulesDesc;
@property (nonatomic, strong) NSMutableArray *taskRules;
/* 任务状态 1:领取 2：即将开始 3：已领完) */
@property (nonatomic, strong) NSString *taskStatus;
//0:已领取 1:已完成，2:已失效
@property (nonatomic, strong) NSString *status;
//是否已领取奖励 （0:否 1:是）
@property (nonatomic, strong) NSString *receive;
/* 任务总数量 */
@property (nonatomic, strong) NSString *totalNum;
/* 是否展开 */
@property (nonatomic, assign) BOOL isChoosesOpen;
/* 是否领取 */
@property (nonatomic, assign) BOOL isReceiveTask;
/* 页面类型 */
@property (nonatomic, strong) NSString *type;
/* model索引 */
@property (nonatomic, assign) NSInteger modelIndex;

@end

NS_ASSUME_NONNULL_END

