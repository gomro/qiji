//
//  QJTaskCenterCommonModel.m
//  QJBox
//
//  Created by rui on 2022/8/19.
//

#import "QJTaskCenterCommonModel.h"

@implementation QJTaskCenterCommonModel

- (instancetype)init{
    self = [super init];
    if (self) {
        _page = [NSNumber numberWithInteger:1];
    }
    return self;
}

+ (NSDictionary *)modelContainerPropertyGenericClass{
    return @{@"records" : [QJTaskCenterCommonDetailModel class],
             };
}

- (NSDictionary *)toParams{
    NSDictionary *dict;
    if(_willLoadMore){
        _page=[NSNumber numberWithInteger:_page.integerValue+1];
    }else{
        _page=[NSNumber numberWithInteger:1];
    }
    dict = @{@"page" : _page,
             @"size" : @"10",
    };
    return dict;
}

-(void)configObj:(QJTaskCenterCommonModel *)model{
    self.total=model.total;
    self.current=model.current;
    self.hasNext=model.hasNext;
    
    if (_willLoadMore) {
        [self.records addObjectsFromArray:model.records];
    }else{
        self.records = [NSMutableArray arrayWithArray:model.records];
    }
}

@end

@implementation QJTaskCenterCommonDetailModel

+ (NSDictionary *)modelCustomPropertyMapper{
    return @{@"ID" : @[@"id"]};
}

+ (NSDictionary *)modelContainerPropertyGenericClass{
    return @{
             @"taskDesc" : [NSString class],
             @"rulesDesc" : [NSString class],
             };
}

@end
