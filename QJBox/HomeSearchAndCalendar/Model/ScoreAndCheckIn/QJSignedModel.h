//
//  QJSignedModel.h
//  QJBox
//
//  Created by rui on 2022/7/14.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@class QJSignedItemDetailModel;

@interface QJSignedModel : NSObject

/* 7天签到情况 */
@property (nonatomic, strong) NSMutableArray <QJSignedItemDetailModel *>*days;
/* 连续签到天数 */
@property (nonatomic, strong) NSString *signedDays;
/* 当前日期 */
@property (nonatomic, strong) NSString *today;
/* 注册时间 */
@property (nonatomic, strong) NSString *birthday;
/* 当前月 */
@property (nonatomic, strong) NSString *month;
/* 奇迹币数量 */
@property (nonatomic, strong) NSString *qicoin;
/* 奇迹币数量(总获取) */
@property (nonatomic, assign) NSString *qjcoinGot;
/* 剩余补签兑换次数 */
@property (nonatomic, strong) NSString *remainSignSupplyCount;
/* 可使用补签次数 */
@property (nonatomic, strong) NSString *unUsedSupplySignCount;
/* 今日是否签到 */
@property (nonatomic, assign) BOOL todaySigned;

@property (nonatomic, assign) BOOL isLastOrNextMonth;

@end

@interface QJSignedItemDetailModel : NSObject

@property (nonatomic, strong) NSString *chooseIndexType;
/* 当前日期 */
@property (nonatomic, strong) NSString *today;
/* 注册时间 */
@property (nonatomic, strong) NSString *birthday;
/* 排序 */
@property (nonatomic, strong) NSString *sort;

/* 日期 */
@property (nonatomic, strong) NSString *date;
@property (nonatomic, strong) NSString *weekDay;
/* 积分数 */
@property (nonatomic, strong) NSString *point;
/* 是否已经签到 */
@property (nonatomic, assign) BOOL haveSigned;
/* 是否补签 */
@property (nonatomic, assign) BOOL supply;
/* 是否有大礼包 */
@property (nonatomic, assign) BOOL bigPresent;
/* 是否打开大礼包 */
@property (nonatomic, assign) BOOL received;

//@property (nonatomic, assign) BOOL isSelectd;

@end

NS_ASSUME_NONNULL_END
