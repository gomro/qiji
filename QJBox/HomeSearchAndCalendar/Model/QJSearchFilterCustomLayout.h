//
//  QJSearchFilterCustomLayout.h
//  QJBox
//
//  Created by rui on 2022/7/6.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, FlowAlignment) {
    FlowAlignmentJustyfied,
    FlowAlignmentLeft,
    FlowAlignmentCenter,
    FlowAlignmentRight
};

@interface QJSearchFilterCustomLayout : UICollectionViewFlowLayout

/* layout对齐方式 */
@property (nonatomic, assign) FlowAlignment alignment;

@end

NS_ASSUME_NONNULL_END
