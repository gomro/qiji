//
//  QJSearchGameListModel.h
//  QJBox
//
//  Created by rui on 2022/7/7.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@class QJSearchGameDetailModel;

@interface QJSearchGameListModel : NSObject

/* 数据源数组 */
@property (nonatomic, strong) NSMutableArray<QJSearchGameDetailModel *> *records;
/* 总条数 */
@property (nonatomic, strong) NSNumber *total;
/* 分页 */
@property (nonatomic, strong) NSNumber *page;
/* 当前页 */
@property (nonatomic, strong) NSNumber *current;
/* 是否能分页 */
@property (nonatomic, assign) BOOL hasNext;
/* 是否需要加载更多 */
@property (assign, nonatomic) BOOL willLoadMore;
/* 分页 */
- (NSDictionary *)toParams;
/* 解析model */
-(void)configObj:(QJSearchGameListModel *)model;

@end

@interface QJSearchGameDetailModel : NSObject

/* 游戏icon */
@property (nonatomic, strong) NSString *icon;
/* 游戏id */
@property (nonatomic, strong) NSString *ID;
/* 游戏Appstore链接 */
@property (nonatomic, strong) NSString *iUrl;
/* 游戏自定义安装链接 */
@property (nonatomic, strong) NSString *iCusInsUrl;
/* 游戏安装模式 0-appstore 1-custom */
@property (nonatomic, strong) NSString *iPkgMode;
/* 游戏大小 */
@property (nonatomic, strong) NSString *iPkgSize;
/* 游戏Schema */
@property (nonatomic, strong) NSString *iSch;
/* 游戏名字 */
@property (nonatomic, strong) NSString *name;
/* 是否下架 */
@property (nonatomic, assign) BOOL disabled;
/* 游戏标签文字颜色 */
@property (nonatomic, strong) NSString *color;
/* 游戏标签背景颜色 */
@property (nonatomic, strong) NSString *backgroundColor;
/* 游戏在线人数 */
@property (nonatomic, strong) NSString *onlineCount;
/* 游戏标签 */
@property (nonatomic, strong) NSMutableArray *tags;
/* 游戏缩略图 */
@property (nonatomic, strong) NSString *thumbnail;
/* 开服时间 */
@property (nonatomic, strong) NSString *serverTime;
@property (nonatomic, assign) BOOL serverOpen;

/* 类型区分- 1=我的游戏列表(带删除功能) */
@property (nonatomic, strong) NSString *type;

/* 游戏详情字段 */
@property (nonatomic, strong) NSMutableArray *bannerImg;
@property (nonatomic, strong) NSString *boardRank;//推荐排名
@property (nonatomic, strong) NSString *startCount;//下载量
@property (nonatomic, strong) NSString *startTime;//开服时间
@property (nonatomic, strong) NSString *point;//评分
@property (nonatomic, strong) NSString *gameInfo;//游戏内容
@property (nonatomic, strong) NSString *intro;//游戏简介

@property (nonatomic, strong) NSString *openTime;//开服时间
/* 开服名数组 */
@property (nonatomic, strong) NSMutableArray *serverName;

@property (nonatomic, assign) NSInteger indexRow;

/* 标签字体和背景颜色 */
@property (nonatomic, strong) UIColor *textColor;
@property (nonatomic, strong) UIColor *bgColor;
@property (nonatomic, assign) BOOL isChoosesOpen;
@property (nonatomic, assign) BOOL isShowLastOpen;
@property (nonatomic, assign) BOOL isShowNextOpen;

/* 多选删除 */
@property (nonatomic, assign) BOOL isEditing;
@property (nonatomic, assign) BOOL isDelete;

@end

NS_ASSUME_NONNULL_END
