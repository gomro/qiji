//
//  QJSearchSortModel.h
//  QJBox
//
//  Created by rui on 2022/7/5.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@class QJSearchTagDetailModel;

@interface QJSearchSortModel : NSObject
/**
 * 标记数据来源
 *  0 ：游戏（默认）
 *  1 ：资讯
 */
@property (nonatomic, assign) NSInteger type;

@property (nonatomic, copy) NSString *title;

/* 最外层数组，用于分区显示 */
@property (nonatomic, strong) NSMutableArray <QJSearchTagDetailModel *>*types;
/* 搜索索引数据源 */
@property (nonatomic, strong) NSMutableArray <QJSearchTagDetailModel *>*data;
/* 是否选中框 */
@property (nonatomic, assign) BOOL hasSelect;//选中状态

@end

@interface QJSearchTagDetailModel : NSObject

/* 搜索关键词 */
@property (nonatomic, copy) NSString *keyword;

@property (nonatomic, strong) NSMutableArray <QJSearchTagDetailModel *>*list;

/* 搜索标签 */
@property (nonatomic, strong) NSMutableArray <QJSearchTagDetailModel *>*filterOptions;
/* 更多下载，综合排序数组 */
@property (nonatomic, strong) NSMutableArray <QJSearchTagDetailModel *>*orderOptions;
/* 标签对应的数组 */
@property (nonatomic, strong) NSMutableArray <QJSearchTagDetailModel *>*values;
/* 标签对应的查询ID */
@property (nonatomic, copy) NSString *ID;
/* 是否选中 */
@property (nonatomic, assign) BOOL isSelect;
/* 是否允许多选 */
@property (nonatomic, assign) BOOL multiple;
/* 名字 */
@property (nonatomic, copy) NSString *name;
/* 类型区分 0-热门搜索，1-全部筛选 */
@property (nonatomic, strong) NSString *typeString;
/* 间隔判断 */
@property (nonatomic, assign) NSInteger section;

@end

NS_ASSUME_NONNULL_END
