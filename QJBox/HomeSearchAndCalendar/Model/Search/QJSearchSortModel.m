//
//  QJSearchSortModel.m
//  QJBox
//
//  Created by rui on 2022/7/5.
//

#import "QJSearchSortModel.h"

@implementation QJSearchSortModel

+ (NSDictionary *)modelContainerPropertyGenericClass{
    return @{
             @"types" : [QJSearchTagDetailModel class],
             @"data" : [QJSearchTagDetailModel class],
             };
}

@end

@implementation QJSearchTagDetailModel

+ (NSDictionary *)modelCustomPropertyMapper{
    return @{@"ID" : @[@"id"]};
}

+ (NSDictionary *)modelContainerPropertyGenericClass{
    return @{
             @"list" : [QJSearchTagDetailModel class],
             @"filterOptions" : [QJSearchTagDetailModel class],
             @"orderOptions" : [QJSearchTagDetailModel class],
             @"values" : [QJSearchTagDetailModel class],
             };
}

@end
