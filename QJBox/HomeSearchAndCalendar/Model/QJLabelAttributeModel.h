//
//  QJLabelAttributeModel.h
//  QJBox
//
//  Created by rui on 2022/7/6.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJLabelAttributeModel : NSObject

@property(nonatomic, strong) UIFont      *font;
@property(nonatomic, strong) UIColor     *textColor;
@property(nonatomic, assign) NSRange     range;

@end

NS_ASSUME_NONNULL_END
