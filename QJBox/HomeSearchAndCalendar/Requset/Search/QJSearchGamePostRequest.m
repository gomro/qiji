//
//  QJSearchGamePostRequest.m
//  QJBox
//
//  Created by rui on 2022/7/7.
//

#import "QJSearchGamePostRequest.h"

@implementation QJSearchGamePostRequest

/**
 * @author: zjr
 * @date: 2022-7-6
 * @desc:搜索游戏
 * Post请求
 */
- (void)getSearchGameRequest{
    self.urlStr = @"/game/search";
    [self start];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

- (YTKRequestSerializerType)requestSerializerType {
    return YTKRequestSerializerTypeJSON;
}

@end
