//
//  QJSearchGamePostRequest.h
//  QJBox
//
//  Created by rui on 2022/7/7.
//

#import "QJBaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJSearchGamePostRequest : QJBaseRequest

/**
 * @author: zjr
 * @date: 2022-7-6
 * @desc:搜索游戏
 * Post请求
 * keyword-搜索关键字
 * option-游戏标签
 * order-排序
 */
- (void)getSearchGameRequest;

@end

NS_ASSUME_NONNULL_END
