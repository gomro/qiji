//
//  QJSearchGameRequest.h
//  QJBox
//
//  Created by rui on 2022/7/6.
//

#import "QJBaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJSearchGameRequest : QJBaseRequest

/**
 * @author: zjr
 * @date: 2022-7-6
 * @desc:获取热搜词
 * GET请求
 */
- (void)getSearchHotkeywordsRequest;

/**
 * @author: zjr
 * @date: 2022-7-6
 * @desc:获取过滤选项-游戏标签
 * GET请求
 */
- (void)getSearchOptionsRequest;

/**
 * @author: zjr
 * @date: 2022-7-6
 * @desc:获取搜索建议
 * GET请求
 * keyword-搜索关键字
 */
- (void)getSearchSuggestRequest;

    
@end

NS_ASSUME_NONNULL_END
