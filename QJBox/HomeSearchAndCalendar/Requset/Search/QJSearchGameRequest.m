//
//  QJSearchGameRequest.m
//  QJBox
//
//  Created by rui on 2022/7/6.
//

#import "QJSearchGameRequest.h"

@implementation QJSearchGameRequest

/**
 * @author: zjr
 * @date: 2022-7-6
 * @desc:获取热搜词
 * GET请求
 */
- (void)getSearchHotkeywordsRequest{
    self.urlStr = @"/game/search/hotkeywords";
    [self start];
}

/**
 * @author: zjr
 * @date: 2022-7-6
 * @desc:获取过滤选项-游戏标签
 * GET请求
 */
- (void)getSearchOptionsRequest{
    self.urlStr = @"/game/search/options";
    [self start];
}

/**
 * @author: zjr
 * @date: 2022-7-6
 * @desc:获取搜索建议
 * GET请求
 */
- (void)getSearchSuggestRequest{
    self.urlStr = @"/game/search/suggest";
    [self start];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

- (YTKRequestSerializerType)requestSerializerType {
    return YTKRequestSerializerTypeHTTP;
}

@end
