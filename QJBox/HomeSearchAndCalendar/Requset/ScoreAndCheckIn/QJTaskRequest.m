//
//  QJTaskRequest.m
//  QJBox
//
//  Created by rui on 2022/7/15.
//

#import "QJTaskRequest.h"

@implementation QJTaskRequest

/**
 * @author: zjr
 * @date: 2022-8-19
 * @desc:我的任务
 * GET请求
 */
- (void)getMyTaskListRequest{
    self.urlStr = @"/gameTask/mine/page";
    self.requestType = YTKRequestMethodGET;
    self.serializerType = YTKRequestSerializerTypeHTTP;
    [self start];
}

/**
 * @author: zjr
 * @date: 2022-8-19
 * @desc:任务大厅
 * GET请求
 */
- (void)getTaskCenterListRequest{
    self.urlStr = @"/gameTask/page";
    self.requestType = YTKRequestMethodGET;
    self.serializerType = YTKRequestSerializerTypeHTTP;
    [self start];
}

/**
 * @author: zjr
 * @date: 2022-8-19
 * @desc:领取任务
 * GET请求
 */
- (void)getTaskReceiveRequest{
    self.urlStr = @"/gameTask/receive";
    self.requestType = YTKRequestMethodPOST;
    self.serializerType = YTKRequestSerializerTypeJSON;
    [self start];
}

/**
 * @author: zjr
 * @date: 2022-8-19
 * @desc:领取奖励
 * POST请求
 */
- (void)getTaskRewardReceiveRequest{
    self.urlStr = @"/gameTask/reward/receive";
    self.requestType = YTKRequestMethodPOST;
    self.serializerType = YTKRequestSerializerTypeJSON;
    [self start];
}

/**
 * @author: zjr
 * @date: 2022-8-25
 * @desc:核对任务
 * POST请求
 */
- (void)getTaskcheckTaskRequest{
    self.urlStr = @"/gameTask/checkTask";
    self.requestType = YTKRequestMethodPOST;
    self.serializerType = YTKRequestSerializerTypeJSON;
    [self start];
}

/**
 * @author: zjr
 * @date: 2022-8-25
 * @desc:消息已读
 * GET请求
 */
- (void)getTaskRemindRequest{
    self.urlStr = @"/gameTask/mine/remind";
    self.requestType = YTKRequestMethodGET;
    self.serializerType = YTKRequestSerializerTypeHTTP;
    [self start];
}

@end
