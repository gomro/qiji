//
//  QJScoreRequest.m
//  QJBox
//
//  Created by rui on 2022/7/1.
//

#import "QJScoreRequest.h"

@implementation QJScoreRequest

/**
 * @author: zjr
 * @date: 2022-7-1
 * @desc:获取用户积分信息
 * GET请求
 */
- (void)getUserScoreInfoRequest{
    self.urlStr = @"/task";
    self.serializerType = YTKRequestSerializerTypeHTTP;
    [self start];
}

/**
 * @author: zjr
 * @date: 2022-7-15
 * @desc:奇迹币变动记录
 * GET请求
 */
- (void)getIconRecoredRequest{
    self.urlStr = @"/task/qicoin/records";
    self.serializerType = YTKRequestSerializerTypeHTTP;
    [self start];
}

/**
 * @author: zjr
 * @date: 2022-7-15
 * @desc:全部任务
 * GET请求
 */
- (void)getTaskListRequest{
    self.urlStr = @"/task/taskPanel";
    self.serializerType = YTKRequestSerializerTypeJSON;
    [self start];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

//- (YTKRequestSerializerType)requestSerializerType {
//    return YTKRequestSerializerTypeHTTP;
//}

@end
