//
//  QJScoreRequest.h
//  QJBox
//
//  Created by rui on 2022/7/1.
//

#import "QJBaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJScoreRequest : QJBaseRequest

/**
 * @author: zjr
 * @date: 2022-7-1
 * @desc:获取用户积分信息
 * GET请求
 */
- (void)getUserScoreInfoRequest;

/**
 * @author: zjr
 * @date: 2022-7-15
 * @desc:奇迹币变动记录
 * GET请求
 */
- (void)getIconRecoredRequest;

/**
 * @author: zjr
 * @date: 2022-7-15
 * @desc:全部任务
 * GET请求
 */
- (void)getTaskListRequest;

@end

NS_ASSUME_NONNULL_END
