//
//  QJTaskRequest.h
//  QJBox
//
//  Created by rui on 2022/7/15.
//

#import "QJBaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJTaskRequest : QJBaseRequest

/**
 * @author: zjr
 * @date: 2022-8-19
 * @desc:我的任务
 * GET请求
 */
- (void)getMyTaskListRequest;

/**
 * @author: zjr
 * @date: 2022-8-19
 * @desc:任务大厅
 * GET请求
 */
- (void)getTaskCenterListRequest;

/**
 * @author: zjr
 * @date: 2022-8-19
 * @desc:领取任务
 * GET请求
 */
- (void)getTaskReceiveRequest;

/**
 * @author: zjr
 * @date: 2022-8-19
 * @desc:领取奖励
 * POST请求
 */
- (void)getTaskRewardReceiveRequest;

/**
 * @author: zjr
 * @date: 2022-8-25
 * @desc:核对任务
 * POST请求
 */
- (void)getTaskcheckTaskRequest;

/**
 * @author: zjr
 * @date: 2022-8-25
 * @desc:消息已读
 * GET请求
 */
- (void)getTaskRemindRequest;

@end

NS_ASSUME_NONNULL_END
