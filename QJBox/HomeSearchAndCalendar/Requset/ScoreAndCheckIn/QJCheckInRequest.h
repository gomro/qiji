//
//  QJCheckInRequest.h
//  QJBox
//
//  Created by rui on 2022/7/1.
//

#import "QJBaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJCheckInRequest : QJBaseRequest

/**
 * @author: zjr
 * @date: 2022-7-1
 * @desc:获取APP分享内容
 * GET请求
 */
- (void)getAPPShareInfoRequest;

/**
 * @author: zjr
 * @date: 2022-7-1
 * @desc:获取签到首页7日内签到情况
 * GET请求
 */
- (void)getCheckInSevenDaysRequest;

/**
 * @author: zjr
 * @date: 2022-7-1
 * @desc:获取签到日历一个月的签到情况
 * Get请求
 */
- (void)getCheckInMonthRequest;

@end

NS_ASSUME_NONNULL_END
