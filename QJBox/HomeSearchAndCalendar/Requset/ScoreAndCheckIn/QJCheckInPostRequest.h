//
//  QJCheckInPostRequest.h
//  QJBox
//
//  Created by rui on 2022/7/7.
//

#import "QJBaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJCheckInPostRequest : QJBaseRequest

/**
 * @author: zjr
 * @date: 2022-7-7
 * @desc:每日签到
 * Post请求
 */
- (void)getEveryDayCheckInRequest;

/**
 * @author: zjr
 * @date: 2022-7-1
 * @desc:补签
 * Post请求
 */
- (void)getRepairCheckInRequest;

/**
 * @author: zjr
 * @date: 2022-7-18
 * @desc:兑换补签次数
 * Post请求
 */
- (void)getSupplySignRequest;

/**
 * @author: zjr
 * @date: 2022-8-30
 * @desc:完成任务
 * Post请求
 */
- (void)getTaskCompleteRequest:(NSString *)taskID;
    
@end

NS_ASSUME_NONNULL_END
