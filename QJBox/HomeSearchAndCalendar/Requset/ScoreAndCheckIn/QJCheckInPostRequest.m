//
//  QJCheckInPostRequest.m
//  QJBox
//
//  Created by rui on 2022/7/7.
//

#import "QJCheckInPostRequest.h"

@implementation QJCheckInPostRequest

/* 补签 Post请求 */
- (void)getRepairCheckInRequest{
    self.urlStr = @"/userSign/supplySign";
    [self start];
}

/* 兑换补签次数 */
- (void)getSupplySignRequest{
    self.urlStr = @"/userSign/exchangeSupplySign";
    [self start];
}

/* 每日签到 Post请求 */
- (void)getEveryDayCheckInRequest{
    self.urlStr = @"/userSign/dailySign";
    [self start];
}

/* 完成任务 Post请求 */
- (void)getTaskCompleteRequest:(NSString *)taskID{
    self.urlStr = [NSString stringWithFormat:@"/task/%@/complete",taskID];
    [self start];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

- (YTKRequestSerializerType)requestSerializerType {
    return YTKRequestSerializerTypeJSON;
}

@end
