//
//  QJScorePutRequset.m
//  QJBox
//
//  Created by rui on 2022/7/15.
//

#import "QJScorePutRequset.h"

@implementation QJScorePutRequset

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPUT;
}

- (YTKRequestSerializerType)requestSerializerType {
    return YTKRequestSerializerTypeHTTP;
}

@end
