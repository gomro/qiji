//
//  QJCheckInRequest.m
//  QJBox
//
//  Created by rui on 2022/7/1.
//

#import "QJCheckInRequest.h"

@implementation QJCheckInRequest

/* 获取APP分享内容 Get请求 */
- (void)getAPPShareInfoRequest{
    self.urlStr = @"/userSign/appShare";
    [self start];
}

/* 获取签到首页7日内签到情况 Get请求 */
- (void)getCheckInSevenDaysRequest{
    self.urlStr = @"/userSign/continueSignInfo";
    [self start];
}

/* 获取签到日历一个月的签到情况 Get请求 */
- (void)getCheckInMonthRequest{
    self.urlStr = @"/userSign/monthlySignInfo";
    [self start];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

- (YTKRequestSerializerType)requestSerializerType {
    return YTKRequestSerializerTypeHTTP;
}

@end
