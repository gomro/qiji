//
//  QJCheckInHeaderView.h
//  QJBox
//
//  Created by rui on 2022/6/27.
//

#import <UIKit/UIKit.h>
/* model */
#import "QJSignedModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJCheckInHeaderView : UIView

/**
 * @author: zjr
 * @date: 2022-6-30
 * @desc: button点击事件回调
 * list-跳转积分明细 shop-跳转积分商城 calendar-跳转日历签到详情 checkIn-签到弹框
 */
@property (nonatomic, copy) void (^btnClick)(NSString *type);
/* 数据源 */
@property (nonatomic, strong) QJSignedModel *model;

- (void)updateQicoin:(QJSignedModel *)model;
- (void)updateQJiconinAnimation:(CGFloat)from andTo:(CGFloat)to;

@end

NS_ASSUME_NONNULL_END
