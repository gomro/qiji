//
//  QJEmptyCommonView.h
//  QJBox
//
//  Created by rui on 2022/9/27.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^EmptyClick)(void);

@interface QJEmptyCommonView : UIView

// 文字部分
@property (nonatomic, strong) UIColor *textColor;
@property (nonatomic, strong) NSString *string;

// 按钮部分
@property (nonatomic, strong) UIColor *buttonBgColor;
@property (nonatomic, strong) UIColor *buttonTextColor;
@property (nonatomic, strong) NSString *buttonString;

//缺省图标
@property (nonatomic, strong) NSString *emptyImage;
//距离顶部距离
@property (nonatomic, assign) NSInteger topSpace;
//按钮圆角
@property (nonatomic, assign) NSInteger buttonRadius;

//按钮距离底部距离
@property (nonatomic, assign) NSInteger buttonBottomOffset;

//按钮回调事件
@property (nonatomic, copy) EmptyClick emptyClick;

@end

NS_ASSUME_NONNULL_END
