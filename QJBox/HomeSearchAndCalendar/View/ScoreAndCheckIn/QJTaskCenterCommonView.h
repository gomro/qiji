//
//  QJTaskCenterCommonView.h
//  QJBox
//
//  Created by rui on 2022/8/15.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJTaskCenterCommonView : UIView

/* 类型区分 -已领取 -完成 -失效 */
@property (nonatomic, copy) NSString *type;
/* 请求 */
- (void)refreshFirst;

@end

NS_ASSUME_NONNULL_END
