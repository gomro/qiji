//
//  QJEmptyBlockView.m
//  QJBox
//
//  Created by macm on 2022/8/4.
//

#import "QJEmptyBlockView.h"

@implementation QJEmptyBlockView
- (instancetype)initWithMessage:(NSString *)message image:(UIImage *)image title:(NSString *)title titleColor:(NSString *)titleColor BGColor:(NSString *)bgColor {
    if (self = [super init]) {
        self.frame = CGRectMake(0, 0, QJScreenWidth, QJScreenHeight-NavigationBar_Bottom_Y);
        [self creatUIWithMessage:message image:image title:title titleColor:titleColor BGColor:bgColor];
        self.backgroundColor = UIColorFromRGB(0Xffffff);
    }
    return self;
}

- (void)creatUIWithMessage:(NSString *)message image:(UIImage *)image title:(NSString *)title titleColor:(NSString *)titleColor BGColor:(NSString *)bgColor{
    UIImageView *alertView = [[UIImageView alloc] init];
    [self addSubview:alertView];
    [alertView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self);
        make.top.mas_equalTo(80);
    }];
    if (image) {
        alertView.image = image;
    } else {
        alertView.image = [UIImage imageNamed:@"icon_empty_no_data"];
    }
    UILabel *messageLabel = [[UILabel alloc] init];
    messageLabel.textAlignment = NSTextAlignmentCenter;
    messageLabel.font = kFont(12);
    messageLabel.numberOfLines = 0;
    [self addSubview:messageLabel];
    [messageLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.top.equalTo(alertView.mas_bottom).offset(40);
    }];
    messageLabel.textColor = [UIColor colorWithHexString:@"0x333333"];
    messageLabel.text = message;
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setTitleColor:titleColor.length > 0?[UIColor colorWithHexString:titleColor]:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn setBackgroundColor:bgColor.length > 0?[UIColor colorWithHexString:bgColor]:kColorBtnBg];
    [btn.titleLabel setFont:FontSemibold(12)];
    btn.layer.cornerRadius = 14;
    [btn setContentEdgeInsets:UIEdgeInsetsMake(0, 15, 0, 15)];
    [self addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(28);
        make.top.equalTo(messageLabel.mas_bottom).mas_offset(16);
        make.centerX.equalTo(self);
    }];
    [btn addTarget:self action:@selector(btnClick) forControlEvents:UIControlEventTouchUpInside];
}


- (instancetype)initWithMessage:(NSString *)message image:(UIImage *)image title:(NSString *)title {
    if (self = [super init]) {
        self.frame = CGRectMake(0, 0, QJScreenWidth, QJScreenHeight-NavigationBar_Bottom_Y);
        [self creatUIWithMessage:message image:image title:title];
        self.backgroundColor = UIColorFromRGB(0Xffffff);
    }
    return self;
}

- (void)creatUIWithMessage:(NSString *)message image:(UIImage *)image title:(NSString *)title {
    UIImageView *alertView = [[UIImageView alloc] init];
    [self addSubview:alertView];
    [alertView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self);
        make.top.mas_equalTo(80);
    }];
    if (image) {
        alertView.image = image;
    } else {
        alertView.image = [UIImage imageNamed:@"icon_empty_no_data"];
    }
    UILabel *messageLabel = [[UILabel alloc] init];
    messageLabel.textAlignment = NSTextAlignmentCenter;
    messageLabel.font = kFont(12);
    messageLabel.numberOfLines = 0;
    [self addSubview:messageLabel];
    [messageLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.top.equalTo(alertView.mas_bottom).offset(40);
    }];
    messageLabel.textColor = [UIColor colorWithHexString:@"0x333333"];
    messageLabel.text = message;
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn setBackgroundColor:kColorBtnBg];
    [btn.titleLabel setFont:FontSemibold(12)];
    btn.layer.cornerRadius = 14;
    [btn setContentEdgeInsets:UIEdgeInsetsMake(0, 15, 0, 15)];
    [self addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(28);
        make.top.equalTo(messageLabel.mas_bottom).mas_offset(16);
        make.centerX.equalTo(self);
    }];
    [btn addTarget:self action:@selector(btnClick) forControlEvents:UIControlEventTouchUpInside];
}

- (void)btnClick {
    if (self.emptyClick) {
        self.emptyClick();
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
