//
//  QJCheckInPopCommonView.m
//  QJBox
//
//  Created by rui on 2022/7/14.
//

#import "QJCheckInPopCommonView.h"
#import "QJTaskListModel.h"
#import "QJTaskCenterCommonModel.h"

@interface QJCheckInPopCommonView ()

@property (nonatomic, strong) UIImageView *imgView;
@property (nonatomic, strong) UILabel *tipLab;
@property (nonatomic, strong) UILabel *tapLab;

@end

@implementation QJCheckInPopCommonView

-(instancetype)initWithFrame:(CGRect)frame
{
    self=[super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        /* 初始化默认配置 */
        [self initParams];
    }
    return self;
}

- (void)initParams{
    self.popViewStyle = QJPopContentViewStyleDefault;
}

- (void)setModel:(id)model{
    _model = model;
//    if (_model) {
        [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        [self buildingUI];
//    }
}

- (void)buildingUI{
    /* item宽度 */
    switch (self.popViewStyle) {
        case QJPopContentViewStyleDefault:{
            [self popContentView];
        }
            break;
        case QJPopContentViewStyleUNCOMPLETED:{
            [self popExperienceContentView];
        }
            break;
        case QJPopContentViewStyleUNRECEIVED:{
            [self popQJCoinContentView];
        }
            break;
        case QJPopContentViewStyleRECEIVED:{
            [self popTaskContentCommonView];
        }
            break;
        case QJPopContentViewStyleTaskRECEIVED:{
            [self popTaskContentTaskCommonView];
        }
            break;
        case QJPopContentViewStyleTaskMyRECEIVED:{
            [self popTaskContentMyTaskCommonView];
        }
            break;
        default:
            break;
    }
}

/**
 * @author: zjr
 * @date: 2022-6-30
 * @desc: 默认弹框样式
 */
- (void)popContentView{
    self.backgroundColor = [UIColor whiteColor];
    self.layer.cornerRadius = 8;
    
    UIImageView *bgIconImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"signin_bgicon"]];
    [self addSubview:bgIconImageView];
    [bgIconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self);
        make.centerX.equalTo(self);
    }];
    
    UIImageView *iconImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
    iconImageView.image = [UIImage imageNamed:@"signin_calendar"];
    [self addSubview:iconImageView];
    [iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(44);
        make.centerX.equalTo(self);
        make.height.equalTo(@(78*kWScale));
        make.width.equalTo(@(78*kWScale));
    }];

    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    titleLabel.font = kboldFont(16);
    titleLabel.textColor = UIColorFromRGB(0x474849);
    titleLabel.text = @"您已连续签到0天！";
    if ([_model isKindOfClass:[NSDictionary class]]) {
        titleLabel.text = [NSString stringWithFormat:@"您已连续签到%@天！",EncodeStringFromDicDefEmtryValue(_model, @"continueSignedCount")];
    }
    [self addSubview:titleLabel];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(iconImageView.mas_bottom).offset(35);
        make.centerX.equalTo(self);
    }];
    
    UILabel *subTitleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    subTitleLabel.font = kFont(12);
    subTitleLabel.textColor = UIColorFromRGB(0xFF8121);
    subTitleLabel.text = @"可获得0 奇迹币！";
    [self addSubview:subTitleLabel];
    [subTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(titleLabel.mas_bottom).offset(5);
        make.centerX.equalTo(self);
    }];
    if ([_model isKindOfClass:[NSDictionary class]]) {
        [subTitleLabel setSingleStr:[NSString stringWithFormat:@"可获得%@ 奇迹币！",EncodeStringFromDicDefEmtryValue(_model, @"qjCoinGet")] range:[[NSString stringWithFormat:@"可获得%@ 奇迹币！",EncodeStringFromDicDefEmtryValue(_model, @"qjCoinGet")] rangeOfString:EncodeStringFromDicDefEmtryValue(_model, @"qjCoinGet")] font:kboldFont(20) color:UIColorFromRGB(0xFF8121) numberOfLines:1];
    }
    
    UIButton *doneButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [doneButton addTarget:self action:@selector(btnAction:) forControlEvents:UIControlEventTouchUpInside];
    doneButton.tag = 1111;
    doneButton.titleLabel.font = kFont(14);
    doneButton.backgroundColor = UIColorFromRGB(0x1F2A4D);
    [doneButton setTitle:@"确定" forState:UIControlStateNormal];
    [doneButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    doneButton.layer.cornerRadius = 4;
    doneButton.layer.masksToBounds = YES;
    [self addSubview:doneButton];
    [doneButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self).offset(-30);
        make.width.equalTo(@(100*kWScale));
        make.height.equalTo(@(36*kWScale));
        make.centerX.equalTo(self);
    }];

    [doneButton layoutIfNeeded];
    [doneButton graduateLeftColor:UIColorFromRGB(0xFDBC69) ToColor:UIColorFromRGB(0xE46B2F) startPoint:CGPointMake(0.5, 0) endPoint:CGPointMake(0.5, 1)];
}

/**
 * @author: zjr
 * @date: 2022-8-22
 * @desc: 奇迹币奖励领取弹框
 */
- (void)popQJCoinContentView{
    self.backgroundColor = [UIColor whiteColor];
    self.layer.cornerRadius = 8;
    
    UIImageView *bgIconImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"signin_bgicon"]];
    [self addSubview:bgIconImageView];
    [bgIconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self);
        make.centerX.equalTo(self);
    }];
    
    UIImageView *iconImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
    [self addSubview:iconImageView];
    [iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(44);
        make.centerX.equalTo(self);
        make.height.equalTo(@(78*kWScale));
        make.width.equalTo(@(78*kWScale));
    }];

    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    titleLabel.font = kboldFont(16);
    titleLabel.textColor = UIColorFromRGB(0x474849);
    titleLabel.text = @"您已连续签到0天！";
    if ([_model isKindOfClass:[QJTaskDetailModel class]]) {
        QJTaskDetailModel *detailModel = (QJTaskDetailModel *)_model;
        if (detailModel.code == 7) {
            iconImageView.image = [UIImage imageNamed:@"signin_calendar"];
        } else {
            iconImageView.image = [UIImage imageNamed:@"signin_continuous"];
        }
        titleLabel.text = [NSString stringWithFormat:@"%@",detailModel.name];
    }
    [self addSubview:titleLabel];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(iconImageView.mas_bottom).offset(35);
        make.centerX.equalTo(self);
    }];
    
    UILabel *subTitleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    subTitleLabel.font = kFont(12);
    subTitleLabel.textColor = UIColorFromRGB(0x919599);
    subTitleLabel.text = @"可获得0 奇迹币！";
    subTitleLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:subTitleLabel];
    [subTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(titleLabel.mas_bottom).offset(12);
        make.centerX.equalTo(self);
    }];
    
    if ([_model isKindOfClass:[QJTaskDetailModel class]]) {
        QJTaskDetailModel *detailModel = (QJTaskDetailModel *)_model;
        NSString *contentString = detailModel.detailDesc?detailModel.detailDesc:detailModel.simpleDesc;
        [subTitleLabel setSingleStr:contentString range:[contentString rangeOfString:[NSString stringWithFormat:@"%ld",detailModel.rewardCount]] font:kboldFont(20) color:UIColorFromRGB(0xFF8121) numberOfLines:0];
    }
    
    UIButton *doneButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [doneButton addTarget:self action:@selector(btnAction:) forControlEvents:UIControlEventTouchUpInside];
    doneButton.tag = 1111;
    doneButton.titleLabel.font = kFont(14);
    doneButton.backgroundColor = UIColorFromRGB(0x1F2A4D);
    [doneButton setTitle:@"领取" forState:UIControlStateNormal];
    [doneButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    doneButton.layer.cornerRadius = 4;
    doneButton.layer.masksToBounds = YES;
    [self addSubview:doneButton];
    [doneButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self).offset(-30);
        make.width.equalTo(@(100*kWScale));
        make.height.equalTo(@(36*kWScale));
        make.centerX.equalTo(self);
    }];

    [doneButton layoutIfNeeded];
    [doneButton graduateLeftColor:UIColorFromRGB(0xFDBC69) ToColor:UIColorFromRGB(0xE46B2F) startPoint:CGPointMake(0.5, 0) endPoint:CGPointMake(0.5, 1)];
}

/**
 * @author: zjr
 * @date: 2022-8-22
 * @desc: 任务简介弹框
 */
- (void)popExperienceContentView{
    self.backgroundColor = [UIColor whiteColor];
    self.layer.cornerRadius = 8;
    
    UIImageView *bgIconImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"signin_bgicon"]];
    [self addSubview:bgIconImageView];
    [bgIconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self);
        make.centerX.equalTo(self);
    }];
    
    UIImageView *iconImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
    iconImageView.image = [UIImage imageNamed:@"signin_calendar"];
    [self addSubview:iconImageView];
    [iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(44);
        make.centerX.equalTo(self);
        make.height.equalTo(@(78*kWScale));
        make.width.equalTo(@(78*kWScale));
    }];

    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    titleLabel.font = kboldFont(16);
    titleLabel.textColor = UIColorFromRGB(0x474849);
    titleLabel.text = @"您已连续签到0天！";
    if ([_model isKindOfClass:[QJTaskDetailModel class]]) {
        QJTaskDetailModel *detailModel = (QJTaskDetailModel *)_model;
        titleLabel.text = [NSString stringWithFormat:@"%@",detailModel.name];
    }
    [self addSubview:titleLabel];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(iconImageView.mas_bottom).offset(35);
        make.height.equalTo(@(20*kWScale));
        make.centerX.equalTo(self);
    }];
    
    UILabel *subTitleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    subTitleLabel.font = kFont(12);
    subTitleLabel.textColor = UIColorFromRGB(0x919599);
    subTitleLabel.text = @"50经验值已发放成功";
    [self addSubview:subTitleLabel];
    [subTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(titleLabel.mas_bottom).offset(12);
//        make.bottom.equalTo(self).offset(-76);
        make.centerX.equalTo(self);
    }];
    
    if ([_model isKindOfClass:[QJTaskDetailModel class]]) {
        QJTaskDetailModel *detailModel = (QJTaskDetailModel *)_model;
        NSString *contentString = detailModel.detailDesc?detailModel.detailDesc:detailModel.simpleDesc;
        [subTitleLabel setSingleStr:contentString range:[contentString rangeOfString:[NSString stringWithFormat:@"%ld",detailModel.rewardCount]] font:kboldFont(20) color:UIColorFromRGB(0xFF8121) numberOfLines:0];
//        subTitleLabel.text = detailModel.detailDesc?detailModel.detailDesc:detailModel.simpleDesc;
    }
    
    UIButton *doneButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [doneButton addTarget:self action:@selector(btnAction:) forControlEvents:UIControlEventTouchUpInside];
    doneButton.tag = 1111;
    doneButton.titleLabel.font = kFont(14);
    doneButton.backgroundColor = UIColorFromRGB(0x1F2A4D);
    [doneButton setTitle:@"去完成" forState:UIControlStateNormal];
    [doneButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    doneButton.layer.cornerRadius = 4;
    doneButton.layer.masksToBounds = YES;
    [self addSubview:doneButton];
    [doneButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self).offset(-30);
        make.width.equalTo(@(100*kWScale));
        make.height.equalTo(@(36*kWScale));
        make.centerX.equalTo(self);
    }];

    [doneButton layoutIfNeeded];
    [doneButton graduateLeftColor:UIColorFromRGB(0xFDBC69) ToColor:UIColorFromRGB(0xE46B2F) startPoint:CGPointMake(0.5, 0) endPoint:CGPointMake(0.5, 1)];
}

/**
 * @author: zjr
 * @date: 2022-8-22
 * @desc: 奇迹币奖励内容弹框
 */
- (void)popTaskContentCommonView{
    self.backgroundColor = [UIColor whiteColor];
    self.layer.cornerRadius = 8;
    
    UIImageView *bgIconImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"signin_bgicon"]];
    [self addSubview:bgIconImageView];
    [bgIconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self);
        make.centerX.equalTo(self);
    }];
    
    UIImageView *iconImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
    iconImageView.image = [UIImage imageNamed:@"signin_get"];
    [self addSubview:iconImageView];
    [iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(44);
        make.centerX.equalTo(self);
        make.height.equalTo(@(78*kWScale));
        make.width.equalTo(@(78*kWScale));
    }];

    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    titleLabel.font = kboldFont(16);
    titleLabel.textColor = UIColorFromRGB(0x474849);
    titleLabel.text = @"您已连续签到0天！";
    if ([_model isKindOfClass:[QJTaskDetailModel class]]) {
        QJTaskDetailModel *detailModel = (QJTaskDetailModel *)_model;
        titleLabel.text = [NSString stringWithFormat:@"%@",detailModel.name];
    }
    [self addSubview:titleLabel];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(self).offset(140);
        make.top.equalTo(iconImageView.mas_bottom).offset(35);
        make.height.equalTo(@(20*kWScale));
        make.centerX.equalTo(self);
    }];

    UILabel *subTitleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    subTitleLabel.font = kFont(12);
    subTitleLabel.textColor = UIColorFromRGB(0x919599);
    subTitleLabel.text = @"可获得0 奇迹币！";
    [self addSubview:subTitleLabel];
    [subTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(titleLabel.mas_bottom).offset(5);
        make.bottom.equalTo(self).offset(-30);
        make.centerX.equalTo(self);
    }];

    if ([_model isKindOfClass:[QJTaskDetailModel class]]) {
        QJTaskDetailModel *detailModel = (QJTaskDetailModel *)_model;
        NSString *contentString = detailModel.detailDesc?detailModel.detailDesc:detailModel.simpleDesc;
        [subTitleLabel setSingleStr:contentString range:[contentString rangeOfString:[NSString stringWithFormat:@"%ld",detailModel.rewardCount]] font:kboldFont(20) color:UIColorFromRGB(0xFF8121) numberOfLines:1];
    }
}

/**
 * @author: zjr
 * @date: 2022-8-22
 * @desc: 奇迹币内容弹框
 */
- (void)popTaskContentTaskCommonView{
    self.backgroundColor = [UIColor whiteColor];
    self.layer.cornerRadius = 8;
    
    QJTaskCenterCommonDetailModel *detailModel;
    if ([_model isKindOfClass:[QJTaskCenterCommonDetailModel class]]) {
        detailModel = (QJTaskCenterCommonDetailModel *)_model;
    }
    
    UIImageView *bgIconImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"signin_bgicon"]];
    [self addSubview:bgIconImageView];
    [bgIconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self);
        make.centerX.equalTo(self);
    }];
    
    UIImageView *iconImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
    if ([detailModel.taskStatus isEqualToString:@"1"]) {
        iconImageView.image = [UIImage imageNamed:@"signin_calendar"];
    }else if ([detailModel.taskStatus isEqualToString:@"2"]) {
        iconImageView.image = [UIImage imageNamed:@"signin_calendar"];
    }else{
        iconImageView.image = [UIImage imageNamed:@"signin_calendar_gray"];
    }
    [self addSubview:iconImageView];
    [iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(44);
        make.centerX.equalTo(self);
        make.height.equalTo(@(78*kWScale));
        make.width.equalTo(@(78*kWScale));
    }];

    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    titleLabel.font = kboldFont(16);
    titleLabel.textColor = UIColorFromRGB(0x474849);
    if ([detailModel.taskStatus isEqualToString:@"1"]) {
        titleLabel.text = @"任务领取成功";
    }else if ([detailModel.taskStatus isEqualToString:@"2"]) {
        titleLabel.text = @"任务还未开始";
    }else{
        titleLabel.text = @"任务已领完";
    }
    [self addSubview:titleLabel];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(iconImageView.mas_bottom).offset(35);
        make.centerX.equalTo(self);
    }];
    
    UILabel *subTitleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    subTitleLabel.font = kFont(12);
    subTitleLabel.textColor = UIColorFromRGB(0x919599);
    subTitleLabel.text = @"";
    [self addSubview:subTitleLabel];
    [subTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(titleLabel.mas_bottom).offset(5);
        make.left.equalTo(self).offset(10);
        make.right.equalTo(self).offset(-10);
        make.centerX.equalTo(self);
    }];
    
    NSString *subString = @"";//[NSString stringWithFormat:@"%@-%@任务已被您领取成功，您可在我的任务重复查看该任务",detailModel.gameName,detailModel.name];
    if ([detailModel.taskStatus isEqualToString:@"3"]) {
        subString = [NSString stringWithFormat:@"%@-%@任务已被领完，看看别的任务吧",detailModel.gameName,detailModel.name];
    }else if ([detailModel.taskStatus isEqualToString:@"1"]) {
        subString = [NSString stringWithFormat:@"%@-%@任务已被您领取成功，您可在我的任务重复查看该任务",detailModel.gameName,detailModel.name];
    }else{
        subString = [NSString stringWithFormat:@"%@-%@任务还没开始，看看别的任务吧",detailModel.gameName,detailModel.name];
    }
    [subTitleLabel setSingleStr:subString range:[subString rangeOfString:[NSString stringWithFormat:@"%@",detailModel.name]] font:kboldFont(20) color:UIColorFromRGB(0xFF8121) numberOfLines:0];
    
    UIButton *doneButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [doneButton addTarget:self action:@selector(btnAction:) forControlEvents:UIControlEventTouchUpInside];
    doneButton.tag = 1111;
    doneButton.titleLabel.font = kFont(14);
    doneButton.backgroundColor = UIColorFromRGB(0x1F2A4D);
    if ([detailModel.taskStatus isEqualToString:@"1"]) {
        doneButton.tag = 2222;
        [doneButton setTitle:@"去完成" forState:UIControlStateNormal];
    }else if ([detailModel.taskStatus isEqualToString:@"2"]) {
        [doneButton setTitle:@"确认" forState:UIControlStateNormal];
    }else{
        [doneButton setTitle:@"确认" forState:UIControlStateNormal];
    }
    [doneButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    doneButton.layer.cornerRadius = 4;
    doneButton.layer.masksToBounds = YES;
    [self addSubview:doneButton];
//    [doneButton mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.bottom.equalTo(self).offset(-30);
//        make.width.equalTo(@100);
//        make.height.equalTo(@36);
//        make.centerX.equalTo(self);
//    }];
    
    UILabel *fromLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    fromLabel.font = kFont(12);
    fromLabel.textColor = UIColorFromRGB(0x919599);
    fromLabel.textAlignment = NSTextAlignmentCenter;
    fromLabel.text = [NSString stringWithFormat:@"发布自：%@",detailModel.gameName];
    [self addSubview:fromLabel];
    [fromLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(doneButton.mas_top).offset(-10);
        make.left.equalTo(self).offset(10);
        make.right.equalTo(self).offset(-10);
        make.centerX.equalTo(self);
    }];
    
    UIButton *lookButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [lookButton addTarget:self action:@selector(btnAction:) forControlEvents:UIControlEventTouchUpInside];
    lookButton.tag = 3333;
    lookButton.titleLabel.font = kFont(14);
    lookButton.backgroundColor = UIColorFromRGB(0xFFFFFF);
    [lookButton setTitle:@"查看任务" forState:UIControlStateNormal];
    [lookButton setTitleColor:UIColorFromRGB(0x626262) forState:UIControlStateNormal];
    [self addSubview:lookButton];
    [lookButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self).offset(-30);
        make.width.equalTo(@(100*kWScale));
        make.height.equalTo(@(36*kWScale));
        make.left.equalTo(self).offset(40);
    }];
    
    if ([detailModel.taskStatus isEqualToString:@"1"]) {
        lookButton.hidden = NO;
        [doneButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self).offset(-30);
            make.width.equalTo(@(100*kWScale));
            make.height.equalTo(@(36*kWScale));
            make.right.equalTo(self).offset(-40);
        }];
    }else{
        lookButton.hidden = YES;
        [doneButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self).offset(-30);
            make.width.equalTo(@(100*kWScale));
            make.height.equalTo(@(36*kWScale));
            make.centerX.equalTo(self);
        }];
    }
    [doneButton layoutIfNeeded];
    [doneButton graduateLeftColor:UIColorFromRGB(0xFDBC69) ToColor:UIColorFromRGB(0xE46B2F) startPoint:CGPointMake(0.5, 0) endPoint:CGPointMake(0.5, 1)];
}

/**
 * @author: zjr
 * @date: 2022-8-22
 * @desc: 任务中心相关弹框
 */
- (void)popTaskContentMyTaskCommonView{
    self.backgroundColor = [UIColor whiteColor];
    self.layer.cornerRadius = 8;
    
    QJTaskCenterCommonDetailModel *detailModel;
    if ([_model isKindOfClass:[QJTaskCenterCommonDetailModel class]]) {
        detailModel = (QJTaskCenterCommonDetailModel *)_model;
    }
    
    UIImageView *bgIconImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"signin_bgicon"]];
    [self addSubview:bgIconImageView];
    [bgIconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self);
        make.centerX.equalTo(self);
    }];
    
    UIImageView *iconImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
    if ([detailModel.status isEqualToString:@"1"]) {
        iconImageView.image = [UIImage imageNamed:@"signin_calendar"];
    }else if ([detailModel.status isEqualToString:@"0"]) {
        iconImageView.image = [UIImage imageNamed:@"signin_calendar"];
    }else{
        iconImageView.image = [UIImage imageNamed:@"signin_calendar_gray"];
    }
    [self addSubview:iconImageView];
    [iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(44);
        make.centerX.equalTo(self);
        make.height.equalTo(@(78*kWScale));
        make.width.equalTo(@(78*kWScale));
    }];

    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    titleLabel.font = kboldFont(16);
    titleLabel.textColor = UIColorFromRGB(0x474849);
    if ([detailModel.status isEqualToString:@"1"]) {
        titleLabel.text = @"奖励领取成功";
    }else if ([detailModel.taskStatus isEqualToString:@"0"]) {
        titleLabel.text = @"任务领取成功";
    }else{
        titleLabel.text = @"任务已失效";
    }
    [self addSubview:titleLabel];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(iconImageView.mas_bottom).offset(35);
        make.centerX.equalTo(self);
    }];
    
    UILabel *subTitleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    subTitleLabel.font = kFont(12);
    subTitleLabel.textColor = UIColorFromRGB(0x919599);
    subTitleLabel.text = @"";
    subTitleLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:subTitleLabel];
    [subTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(titleLabel.mas_bottom).offset(5);
        make.left.equalTo(self).offset(10);
        make.right.equalTo(self).offset(-10);
        make.centerX.equalTo(self);
    }];
    
    NSString *subString = @"";
    if ([detailModel.status isEqualToString:@"1"] && [detailModel.receive isEqualToString:@"0"]) {
        subString = [NSString stringWithFormat:@"%@",detailModel.rewardDesc];
    }else if ([detailModel.status isEqualToString:@"1"] && [detailModel.receive isEqualToString:@"1"]) {
        subString = [NSString stringWithFormat:@"任务奖励已被您领取成功，快去看看其他任务吧"];
    }else if ([detailModel.status isEqualToString:@"0"]) {
        subString = [NSString stringWithFormat:@"%@-%@任务已被您领取成功，您可在我的任务重复查看该任务",detailModel.gameName,detailModel.name];
    }else{
        subString = [NSString stringWithFormat:@"任务已结束，领取的任务已失效"];
    }
    [subTitleLabel setSingleStr:subString range:[subString rangeOfString:[NSString stringWithFormat:@"%@",detailModel.name]] font:kboldFont(20) color:UIColorFromRGB(0xFF8121) numberOfLines:0];
    subTitleLabel.textAlignment = NSTextAlignmentCenter;
    
    UIButton *doneButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [doneButton addTarget:self action:@selector(btnAction:) forControlEvents:UIControlEventTouchUpInside];
    doneButton.tag = 1111;
    doneButton.titleLabel.font = kFont(14);
    doneButton.backgroundColor = UIColorFromRGB(0x1F2A4D);
    if ([detailModel.status isEqualToString:@"1"]) {
        doneButton.tag = 3333;
        [doneButton setTitle:@"查看" forState:UIControlStateNormal];
    }else if ([detailModel.status isEqualToString:@"0"]) {
        [doneButton setTitle:@"确定" forState:UIControlStateNormal];
    }else{
        [doneButton setTitle:@"确定" forState:UIControlStateNormal];
    }
    [doneButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    doneButton.layer.cornerRadius = 4;
    doneButton.layer.masksToBounds = YES;
    [self addSubview:doneButton];
//    [doneButton mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.bottom.equalTo(self).offset(-30);
//        make.width.equalTo(@100);
//        make.height.equalTo(@36);
//        make.centerX.equalTo(self);
//    }];
    
    UIButton *lookButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [lookButton addTarget:self action:@selector(btnAction:) forControlEvents:UIControlEventTouchUpInside];
    lookButton.tag = 2222;
    lookButton.titleLabel.font = kFont(14);
    lookButton.backgroundColor = UIColorFromRGB(0xFFFFFF);
    [lookButton setTitle:@"确定" forState:UIControlStateNormal];
    [lookButton setTitleColor:UIColorFromRGB(0x626262) forState:UIControlStateNormal];
    [self addSubview:lookButton];
    [lookButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self).offset(-30);
        make.width.equalTo(@(100*kWScale));
        make.height.equalTo(@(36*kWScale));
        make.left.equalTo(self).offset(40);
    }];
    
    if ([detailModel.status isEqualToString:@"1"]) {
        lookButton.hidden = NO;
        [doneButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self).offset(-30);
            make.width.equalTo(@(100*kWScale));
            make.height.equalTo(@(36*kWScale));
            make.right.equalTo(self).offset(-40);
        }];
    }else{
        lookButton.hidden = YES;
        [doneButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self).offset(-30);
            make.width.equalTo(@(100*kWScale));
            make.height.equalTo(@(36*kWScale));
            make.centerX.equalTo(self);
        }];
    }
    [doneButton layoutIfNeeded];
    [doneButton graduateLeftColor:UIColorFromRGB(0xFDBC69) ToColor:UIColorFromRGB(0xE46B2F) startPoint:CGPointMake(0.5, 0) endPoint:CGPointMake(0.5, 1)];
}

- (void)btnAction:(UIButton *)button{
    if (button.tag == 1111) {
        if (self.directionBlock) {
            self.directionBlock(ButtonClickTypeClose);
        }
    }else if (button.tag == 2222) {
        if (self.directionBlock) {
            self.directionBlock(ButtonClickTypeGoDone);
        }
    }else if (button.tag == 3333) {
        if (self.directionBlock) {
            self.directionBlock(ButtonClickTypeLookTask);
        }
    }
}

@end
