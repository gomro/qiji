//
//  QJEmptyCommonView.m
//  QJBox
//
//  Created by rui on 2022/9/27.
//

#import "QJEmptyCommonView.h"

@interface QJEmptyCommonView ()

@property (nonatomic, strong) UIImageView *topImageView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIButton *titleButton;

@end

@implementation QJEmptyCommonView

- (instancetype)initWithFrame:(CGRect)frame {
    if(self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor clearColor];
        
        [self addSubview:self.topImageView];
        [self addSubview:self.titleLabel];
        [self addSubview:self.titleButton];
        [self.topImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self);
            make.height.width.equalTo(@(Get375Width(240)));
            make.top.equalTo(self).offset(Get375Width(72));
        }];
        [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self);
            make.height.mas_equalTo(28);
            make.top.equalTo(self.topImageView.mas_bottom).offset(-Get375Width(65));
        }];
        [self.titleButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self);
            make.height.mas_equalTo(Get375Width(28));
            make.top.equalTo(self.topImageView.mas_bottom).offset(-Get375Width(28));
            
        }];
    }
    return self;
}

#pragma mark ---------- getter && setter
- (UIImageView *)topImageView {
    if(!_topImageView) {
        _topImageView = [UIImageView new];
        _topImageView.image = [UIImage imageNamed:@"empty_no_data"];
    }
    return _topImageView;
}

- (UIButton *)titleButton {
    if(!_titleButton) {
        _titleButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _titleButton.titleLabel.font = FontRegular(12);
        [_titleButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_titleButton addTarget:self action:@selector(btnClick) forControlEvents:UIControlEventTouchUpInside];
        _titleButton.backgroundColor = UIColorFromRGB(0x1f2a4d);
        _titleButton.layer.cornerRadius = 14;
        [_titleButton setContentEdgeInsets:UIEdgeInsetsMake(0, 15, 0, 15)];
        [_titleButton setHidden:YES];
    }
    return _titleButton;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.font = FontRegular(12);
        _titleLabel.textColor = UIColorFromRGB(0x1f2a4d);
        _titleLabel.text = @"暂无数据";
    }
    return _titleLabel;
}

#pragma mark ---------- 属性赋值
// 文字部分
- (void)setString:(NSString *)string {
    _string = string;
    _titleLabel.text = string;
}
- (void)setTextColor:(UIColor *)textColor{
    _textColor = textColor;
    _titleLabel.textColor = textColor;
}

// 按钮部分
- (void)setButtonBgColor:(UIColor *)buttonBgColor{
    _buttonBgColor = buttonBgColor;
    [_titleButton setBackgroundColor:_buttonBgColor];
}

- (void)setButtonRadius:(NSInteger)buttonRadius{
    _buttonRadius = buttonRadius;
    _titleButton.layer.cornerRadius = _buttonRadius;
}

- (void)setButtonString:(NSString *)buttonString {
    _buttonString = buttonString;
    [self.titleButton setHidden:NO];
    [self.titleButton setTitle:buttonString forState:UIControlStateNormal];
}

- (void)setButtonTextColor:(UIColor *)buttonTextColor {
    _buttonTextColor = buttonTextColor;
    [_titleButton setTitleColor:buttonTextColor forState:UIControlStateNormal];
}

- (void)setEmptyImage:(NSString *)emptyImage{
    _emptyImage = emptyImage;
    self.topImageView.image = [UIImage imageNamed:_emptyImage];
}

- (void)setTopSpace:(NSInteger)topSpace{
    _topSpace = topSpace;
    [self.topImageView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.centerX.equalTo(self);
                make.height.width.equalTo(@(Get375Width(240)));
                make.top.equalTo(self).offset(_topSpace);
    }];
}

- (void)setButtonBottomOffset:(NSInteger)buttonBottomOffset {
    _buttonBottomOffset = buttonBottomOffset;
    [self.titleButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.height.mas_equalTo(Get375Width(28));
        make.bottom.equalTo(self.topImageView).offset(-buttonBottomOffset);
        
    }];
}

- (void)btnClick {
    if (self.emptyClick) {
        self.emptyClick();
    }
}

@end
