//
//  QJTaskCommonView.h
//  QJBox
//
//  Created by rui on 2022/7/12.
//

#import <UIKit/UIKit.h>
/* model */
#import "QJTaskListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJTaskCommonView : UIView

@property (nonatomic, strong) QJTaskDetailModel *model;

@property (nonatomic, copy) void (^btnClick)(QJTaskDetailModel *model);
@property (nonatomic, copy) void (^contentBtnClick)(QJTaskDetailModel *model);
@property (nonatomic, copy) void (^moreBtnClick)(void);

@end

NS_ASSUME_NONNULL_END
