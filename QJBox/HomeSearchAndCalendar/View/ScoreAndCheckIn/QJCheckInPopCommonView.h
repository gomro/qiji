//
//  QJCheckInPopCommonView.h
//  QJBox
//
//  Created by rui on 2022/7/14.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/* item宽度 */
typedef NS_ENUM(NSUInteger, QJPopContentViewStyle) {
    QJPopContentViewStyleDefault = 0, //默认
    QJPopContentViewStyleUNCOMPLETED = 1,//去完成  领取经验
    QJPopContentViewStyleUNRECEIVED = 2,//未领取  领取奇迹币
    QJPopContentViewStyleRECEIVED = 3,//已经领取  任务介绍
    QJPopContentViewStyleTaskRECEIVED = 4,//领取任务
    QJPopContentViewStyleTaskMyRECEIVED = 5,//领取任务
};

typedef NS_ENUM(NSInteger, ButtonClickType) {
    ButtonClickTypeClose = 0,    // 关闭
    ButtonClickTypeSure = 1,     // 确认
    ButtonClickTypeGoDone = 2,   // 去完成
    ButtonClickTypeLookTask = 3,   // 查看任务
};

typedef void(^ButtonTypeBlock)(ButtonClickType direction);

@interface QJCheckInPopCommonView : UIView

@property (nonatomic, strong) id model;

@property (nonatomic, assign) QJPopContentViewStyle popViewStyle;
//按钮点击block, 回调方向
@property (nonatomic, copy) ButtonTypeBlock directionBlock;

@end

NS_ASSUME_NONNULL_END
