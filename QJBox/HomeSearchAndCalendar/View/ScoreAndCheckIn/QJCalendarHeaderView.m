//
//  QJCalendarHeaderView.m
//  QJBox
//
//  Created by rui on 2022/7/18.
//

#import "QJCalendarHeaderView.h"
/* 日历控件 */
#import "FSCalendar.h"
#import "QJCalendarCustomCell.h"
#import "UIColor+Common.h"

@interface QJCalendarHeaderView ()<FSCalendarDataSource,FSCalendarDelegate,FSCalendarDelegateAppearance>

/* 日历view */
@property (nonatomic, strong) FSCalendar *calendar;
@property (nonatomic, strong) NSCalendar *gregorian;
/* 日历背景view */
@property (nonatomic, strong) UIView *calendarView;
/* 补签button */
@property (nonatomic, strong) UIButton *repairButton;
/* 选中日期 */
@property (nonatomic, strong) NSString *chooseDayStr;
/* 签到最小日期 */
@property (nonatomic, strong) NSString *chooseMinDateString;
//补签次数
@property (nonatomic, strong) UILabel *repairCountLabel;
//补签icon
@property (nonatomic, strong) UILabel *repairIconLabel;
//日历数据-打卡数据（未参与周期内的签到）
@property (nonatomic, strong) NSMutableArray *signedDateArray;
/* 日历数据-大礼包数据 */
@property (nonatomic, strong) NSMutableArray *giftDateArray;
/* 日历数据-样式绘制（左右半圆） */
@property (nonatomic, strong) NSMutableArray *leftDateArray;
@property (nonatomic, strong) NSMutableArray *rightDateArray;
/* 日历数据-样式绘制（默认样式） */
@property (nonatomic, strong) NSMutableArray *middleDateArray;
/* 日历数据-补签 */
@property (nonatomic, strong) NSMutableArray *repairDateArray;
/* 注册时间 */
@property (nonatomic, strong) NSString *birthday;

/* 时间转换格式 */
@property (nonatomic, strong) NSDateFormatter *dateFormatter2;

@end

@implementation QJCalendarHeaderView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        /* 添加日历 */
        [self addCalendar];
    }
    return self;
}

/**
 * @author: zjr
 * @date: 2022-7-18
 * @desc: 初始化日历
 */
- (void)addCalendar{
    self.backgroundColor = [UIColor whiteColor];
    self.layer.cornerRadius = 8;
    self.layer.masksToBounds = YES;
    
    self.signedDateArray = [NSMutableArray array];
    self.giftDateArray = [NSMutableArray array];
    self.leftDateArray = [NSMutableArray array];
    self.rightDateArray = [NSMutableArray array];
    self.middleDateArray = [NSMutableArray array];
    self.repairDateArray = [NSMutableArray array];
    
    self.gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    self.dateFormatter2 = [[NSDateFormatter alloc] init];
    self.dateFormatter2.dateFormat = @"yyyy-MM-dd";

    self.calendarView = [[UIView alloc] initWithFrame:CGRectZero];
    self.calendarView.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.calendarView];
    [self.calendarView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.left.right.mas_equalTo(self);
    }];
    
    self.calendar = [[FSCalendar alloc] initWithFrame:CGRectMake(0, 0, QJScreenWidth, 400*kWScale)];
    [self.calendarView addSubview:self.calendar];
    [_calendar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.calendarView);
        make.left.right.mas_equalTo(self.calendarView);
        make.height.mas_equalTo(400*kWScale);
    }];
    _calendar.backgroundColor = [UIColor clearColor];
    _calendar.dataSource = self;
    _calendar.delegate = self;
    _calendar.locale = [NSLocale localeWithLocaleIdentifier:@"zh-CN"];
    _calendar.firstWeekday = 1;
    _calendar.placeholderType = FSCalendarPlaceholderTypeNone;
    _calendar.appearance.todayColor = UIColorFromRGB(0xFFFFFF);
    _calendar.appearance.headerMinimumDissolvedAlpha = 0;
    _calendar.appearance.caseOptions = FSCalendarCaseOptionsWeekdayUsesSingleUpperCase|FSCalendarCaseOptionsHeaderUsesUpperCase;
    _calendar.appearance.headerDateFormat = @"yyyy年MM月";
    _calendar.appearance.weekdayTextColor = [UIColor colorWithRed:0.235 green:0.235 blue:0.263 alpha:0.3];
    _calendar.appearance.headerTitleColor =  UIColorFromRGB(0x000000);
    _calendar.appearance.titlePlaceholderColor = UIColorFromRGB(0x919599);
    _calendar.scrollEnabled = NO;
    [self.calendar selectDate:[NSDate date] scrollToDate:YES];
    [self.calendar registerClass:[QJCalendarCustomCell class] forCellReuseIdentifier:@"customCell"];

    //创建点击跳转显示上一月和下一月button
    UIButton *previousButton = [UIButton buttonWithType:UIButtonTypeCustom];
    previousButton.frame = CGRectMake(25, 13, 24*kWScale, 24*kWScale);
    [previousButton setImage:[UIImage imageNamed:@"icon_calendar_arrowleft"] forState:UIControlStateNormal];
    [previousButton addTarget:self action:@selector(previousClicked:) forControlEvents:UIControlEventTouchUpInside];
    [_calendar addSubview:previousButton];

    UIButton *nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
    nextButton.frame = CGRectMake(_calendar.width -(24*kWScale)*2 - 25, 10, 24*kWScale, 24*kWScale);
    [nextButton setImage:[UIImage imageNamed:@"icon_calendar_arrowright"] forState:UIControlStateNormal];
    [nextButton addTarget:self action:@selector(nextClicked:) forControlEvents:UIControlEventTouchUpInside];
    [_calendar addSubview:nextButton];
    
    self.repairButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.repairButton.titleLabel.font = kFont(13);
    self.repairButton.backgroundColor = [UIColor blackColor];
    [self.repairButton setTitle:@"补签" forState:UIControlStateNormal];
    [self.repairButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.repairButton addTarget:self action:@selector(repairBtnAction) forControlEvents:UIControlEventTouchUpInside];
    self.repairButton.layer.cornerRadius = 20*kWScale;
    self.repairButton.layer.masksToBounds = YES;
    [self.calendarView addSubview:self.repairButton];
    [self.repairButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.calendar.mas_bottom);
        make.width.equalTo(@(220*kWScale));
        make.height.equalTo(@(40*kWScale));
        make.centerX.equalTo(self.calendarView);
    }];
    [self.repairButton layoutIfNeeded];
    [self.repairButton graduateLeftColor:UIColorFromRGB(0xEE8737) ToColor:UIColorFromRGB(0xEA5C44) startPoint:CGPointMake(0.5, 0) endPoint:CGPointMake(0.5, 1)];
    
    _repairIconLabel = [UILabel new];
    _repairIconLabel.font = kFont(12);
    _repairIconLabel.textColor = UIColorFromRGB(0xED753D);
    _repairIconLabel.text = @"补";
    _repairIconLabel.textAlignment = NSTextAlignmentCenter;
    _repairIconLabel.backgroundColor = UIColorFromRGB(0xFFE9C9);
    _repairIconLabel.layer.cornerRadius = 6;
    [self.calendarView addSubview:self.repairIconLabel];
    [self.repairIconLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.calendarView.mas_bottom).offset(-20);
        make.right.equalTo(self.calendarView).offset(-15);
        make.width.equalTo(@(46*kWScale));
        make.height.equalTo(@(26*kWScale));
    }];
    
    _repairCountLabel = [UILabel new];
    _repairCountLabel.font = kFont(12);
    _repairCountLabel.textColor = UIColorFromRGB(0x919599);
    _repairCountLabel.text = @"补签卡：0张";
    _repairCountLabel.textAlignment = NSTextAlignmentRight;
    [self.calendarView addSubview:self.repairCountLabel];
    [self.repairCountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.repairIconLabel);
        make.right.equalTo(self.repairIconLabel.mas_left).offset(-10);
    }];
}

/**
 * @author: zjr
 * @date: 2022-6-28
 * @desc: button点击事件
 */
//上一月按钮点击事件
- (void)previousClicked:(id)sender {
    NSDate *currentMonth = self.calendar.currentPage;
    NSDate *previousMonth = [self.gregorian dateByAddingUnit:NSCalendarUnitMonth value:-1 toDate:currentMonth options:0];
    [self.calendar setCurrentPage:previousMonth animated:YES];
//    NSString *minMinthString = [NSString getStringForDate:previousMonth format:@"MM"];
    [self resetRepairButton];
    
    if (self.leftMonthClick) {
        self.leftMonthClick([NSString getStringForDate:previousMonth format:@"yyyy-MM-dd"]);
    }
}

//下一月按钮点击事件
- (void)nextClicked:(id)sender {
    NSDate *currentMonth = self.calendar.currentPage;
    NSDate *nextMonth = [self.gregorian dateByAddingUnit:NSCalendarUnitMonth value:1 toDate:currentMonth options:0];
//    NSString *minMinthString = [NSString getStringForDate:nextMonth format:@"MM"];
    [self resetRepairButton];
    
    [self.calendar setCurrentPage:nextMonth animated:YES];
    if (self.rightMonthClick) {
        self.rightMonthClick([NSString getStringForDate:nextMonth format:@"yyyy-MM-dd"]);
    }
}

/* 补签 */
- (void)repairBtnAction{
    if (self.btnClick) {
        self.btnClick(self.chooseDayStr);
    }
}

#pragma mark - calendardelegate
- (void)calendar:(FSCalendar *)calendar boundingRectWillChange:(CGRect)bounds animated:(BOOL)animated{
    [_calendar mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(CGRectGetHeight(bounds));
    }];
    [self layoutIfNeeded];
}

- (FSCalendarCell *)calendar:(FSCalendar *)calendar cellForDate:(NSDate *)date atMonthPosition:(FSCalendarMonthPosition)monthPosition
{
    QJCalendarCustomCell *cell = [calendar dequeueReusableCellWithIdentifier:@"customCell" forDate:date atMonthPosition:monthPosition];
    return cell;
}

- (void)calendar:(FSCalendar *)calendar willDisplayCell:(FSCalendarCell *)cell forDate:(NSDate *)date atMonthPosition: (FSCalendarMonthPosition)monthPosition
{
    [self configureCell:cell forDate:date atMonthPosition:monthPosition];
}

- (BOOL)calendar:(FSCalendar *)calendar shouldSelectDate:(NSDate *)date atMonthPosition:(FSCalendarMonthPosition)monthPosition
{
    return monthPosition == FSCalendarMonthPositionCurrent;
}

- (BOOL)calendar:(FSCalendar *)calendar shouldDeselectDate:(NSDate *)date atMonthPosition:(FSCalendarMonthPosition)monthPosition
{
    return monthPosition == FSCalendarMonthPositionCurrent;
}

- (void)calendar:(FSCalendar *)calendar didSelectDate:(NSDate *)date atMonthPosition:(FSCalendarMonthPosition)monthPosition
{
    self.chooseDayStr = [self.dateFormatter2 stringFromDate:date];
    
    NSDate *currentDate = [NSString getDateForString:self.chooseDayStr format:@"yyyy-MM-dd"];
    NSDate *todayDate = [NSString getDateForString:_model.today format:@"yyyy-MM-dd"];

    if (date == todayDate) {
        self.repairButton.userInteractionEnabled = NO;
        [self.repairButton graduateLeftColor:[UIColor colorWithHexString:@"0xF6BEA6"andAlpha:1] ToColor:[UIColor colorWithHexString:@"0xF6BEA6"andAlpha:1] startPoint:CGPointMake(0.5, 0) endPoint:CGPointMake(0.5, 1)];
    }else{
        if ([self.repairDateArray containsObject:currentDate]) {
            self.repairButton.userInteractionEnabled = YES;
            [self.repairButton graduateLeftColor:UIColorFromRGB(0xEE8737) ToColor:UIColorFromRGB(0xEA5C44) startPoint:CGPointMake(0.5, 0) endPoint:CGPointMake(0.5, 1)];
        }else{
            self.repairButton.userInteractionEnabled = NO;
            [self.repairButton graduateLeftColor:[UIColor colorWithHexString:@"0xF6BEA6"andAlpha:1] ToColor:[UIColor colorWithHexString:@"0xF6BEA6"andAlpha:1] startPoint:CGPointMake(0.5, 0) endPoint:CGPointMake(0.5, 1)];
        }
    }

    [self configureVisibleCells];
}

- (void)calendar:(FSCalendar *)calendar didDeselectDate:(NSDate *)date atMonthPosition:(FSCalendarMonthPosition)monthPosition
{
    [self configureVisibleCells];
}

- (NSArray<UIColor *> *)calendar:(FSCalendar *)calendar appearance:(FSCalendarAppearance *)appearance eventDefaultColorsForDate:(NSDate *)date
{
    if ([self.gregorian isDateInToday:date]) {
        return @[[UIColor orangeColor]];
    }
    return @[appearance.eventDefaultColor];
}

#pragma mark - Private methods

- (void)configureVisibleCells
{
    [self.calendar.visibleCells enumerateObjectsUsingBlock:^(__kindof FSCalendarCell * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSDate *date = [self.calendar dateForCell:obj];
        FSCalendarMonthPosition position = [self.calendar monthPositionForCell:obj];
        [self configureCell:obj forDate:date atMonthPosition:position];
    }];
}

- (void)configureCell:(FSCalendarCell *)cell forDate:(NSDate *)date atMonthPosition:(FSCalendarMonthPosition)monthPosition
{
    QJCalendarCustomCell *diyCell = (QJCalendarCustomCell *)cell;
    diyCell.customTitleLabel.hidden = NO;
    diyCell.subCustomTitleLabel.text = @"";
    diyCell.subCustomTitleLabel.textColor = UIColorFromRGB(0xFF9500);
    diyCell.subCustomTitleLabel.layer.cornerRadius = 4;
    diyCell.subCustomTitleLabel.layer.masksToBounds = YES;
    diyCell.subCustomTitleLabel.layer.borderColor = UIColorFromRGB(0xFFFFFF).CGColor;
    diyCell.subCustomTitleLabel.layer.borderWidth = 0;
    diyCell.selectedDate = date;
    [diyCell.contentView.layer insertSublayer:diyCell.titleSelectLayer below:diyCell.customTitleLabel.layer];
    // 打卡数据连线
    diyCell.selectionLayer.hidden = NO;
    diyCell.circleImageView.hidden = YES;
    
    NSString *key = [self.dateFormatter2 stringFromDate:date];
    key = [key stringByReplacingOccurrencesOfString:@"-" withString:@""];
    NSInteger currentDatr = [[self.model.today stringByReplacingOccurrencesOfString:@"-" withString:@""] integerValue];
    if (key.integerValue < currentDatr) {
        diyCell.customTitleLabel.textColor = UIColorFromRGB(0x919599);
    }else{
        diyCell.customTitleLabel.textColor = UIColorFromRGB(0x000000);
    }
    if (monthPosition == FSCalendarMonthPositionCurrent) {
        SelectionType selectionType = SelectionTypeNone;
        if ([self.leftDateArray containsObject:date]) {
            selectionType = SelectionTypeLeftBorder;
            diyCell.subCustomTitleLabel.text = @"已签";
            diyCell.customTitleLabel.textColor = UIColorFromRGB(0xFF9500);
        }
        if ([self.middleDateArray containsObject:date]) {
            selectionType = SelectionTypeMiddle;
            diyCell.subCustomTitleLabel.text = @"已签";
            diyCell.customTitleLabel.textColor = UIColorFromRGB(0xFF9500);
        }
        if ([self.rightDateArray containsObject:date]) {
            selectionType = SelectionTypeRightBorder;
            diyCell.subCustomTitleLabel.text = @"已签";
            diyCell.customTitleLabel.textColor = UIColorFromRGB(0xFF9500);
        }
        if ([self.signedDateArray containsObject:date]) {
            selectionType = SelectionTypeSingle;
            diyCell.subCustomTitleLabel.text = @"已签";
            diyCell.customTitleLabel.textColor = UIColorFromRGB(0xFF9500);
        }
        if ([self.repairDateArray containsObject:date]) {
            selectionType = SelectionTypeNone;
            diyCell.subCustomTitleLabel.text = @"去补签";
            diyCell.subCustomTitleLabel.layer.borderColor = UIColorFromRGB(0xFF9500).CGColor;
            diyCell.subCustomTitleLabel.layer.borderWidth = 1;
            diyCell.customTitleLabel.textColor = UIColorFromRGB(0x919599);
        }
        if ([self.giftDateArray containsObject:date]) {
            diyCell.customTitleLabel.hidden = YES;
            diyCell.circleImageView.hidden = NO;
            [diyCell.contentView.layer insertSublayer:diyCell.titleSelectLayer below:diyCell.circleImageView.layer];
        }
        if (self.calendar.selectedDate == date) {
            diyCell.customTitleLabel.textColor = UIColorFromRGB(0xFFFFFF);
        }
        
        if ([NSString getDateForString:self.model.birthday format:@"yyyy-MM-dd"] == date) {
            diyCell.subCustomTitleLabel.text = @"诞生日";
            diyCell.subCustomTitleLabel.textColor = UIColorFromRGB(0x474849);
            diyCell.subCustomTitleLabel.layer.borderColor = UIColorFromRGB(0xFFFFFF).CGColor;
            diyCell.subCustomTitleLabel.layer.borderWidth = 0;
        }
        
        if (selectionType == SelectionTypeNone) {
            diyCell.selectionLayer.hidden = YES;
            
            if (date == self.calendar.selectedDate) {
                diyCell.titleSelectLayer.hidden = NO;
            }else{
                diyCell.titleSelectLayer.hidden = YES;
            }
            return;
        }
        diyCell.selectionLayer.hidden = NO;
        diyCell.selectionType = selectionType;
        
        if (date == self.calendar.selectedDate) {
            diyCell.titleSelectLayer.hidden = NO;
        }else{
            diyCell.titleSelectLayer.hidden = YES;
        }
    } else {
        diyCell.selectionLayer.hidden = YES;
    }
}

/**
 * @author: zjr
 * @date: 2022-6-29
 * @desc: 小于当前日期的颜色
 */
- (nullable UIColor *)calendar:(FSCalendar *)calendar appearance:(FSCalendarAppearance *)appearance titleDefaultColorForDate:(NSDate *)date{
    NSString *key = [self.dateFormatter2 stringFromDate:date];
    key = [key stringByReplacingOccurrencesOfString:@"-" withString:@""];
    NSInteger currentDatr = [[self.model.today stringByReplacingOccurrencesOfString:@"-" withString:@""] integerValue];
    if (key.integerValue < currentDatr) {
        return UIColorFromRGB(0x919599);
    }
    return UIColorFromRGB(0x000000);
}

- (void)updateDateModel:(QJSignedModel *)model{
    [self updateCalendar];
    [self compairDate];
}

- (void)setModel:(QJSignedModel *)model{
    _model = model;
    _repairCountLabel.text = [NSString stringWithFormat:@"补签卡：%@张",_model.unUsedSupplySignCount];
    
    [self compairDate];
}

- (void)compairDate{
    DLog(@"self.chooseDayStr = %@",self.chooseDayStr);
    NSDate *currentDate = [NSString getDateForString:self.chooseDayStr format:@"yyyy-MM-dd"];
    if ([self.repairDateArray containsObject:currentDate]) {
        self.repairButton.userInteractionEnabled = YES;
        [self.repairButton graduateLeftColor:UIColorFromRGB(0xEE8737) ToColor:UIColorFromRGB(0xEA5C44) startPoint:CGPointMake(0.5, 0) endPoint:CGPointMake(0.5, 1)];
    }else{
        self.repairButton.userInteractionEnabled = NO;
        [self.repairButton graduateLeftColor:[UIColor colorWithHexString:@"0xF6BEA6"andAlpha:1] ToColor:[UIColor colorWithHexString:@"0xF6BEA6"andAlpha:1] startPoint:CGPointMake(0.5, 0) endPoint:CGPointMake(0.5, 1)];
    }
}

- (void)resetRepairButton{
    self.repairButton.userInteractionEnabled = NO;
    [self.repairButton graduateLeftColor:[UIColor colorWithHexString:@"0xF6BEA6"andAlpha:1] ToColor:[UIColor colorWithHexString:@"0xF6BEA6"andAlpha:1] startPoint:CGPointMake(0.5, 0) endPoint:CGPointMake(0.5, 1)];
}

- (void)updateCalendar{
    [self resetDateModel];
    [self.calendar reloadData];
}

- (void)resetDateModel{
    [self.signedDateArray removeAllObjects];
    [self.leftDateArray removeAllObjects];
    [self.rightDateArray removeAllObjects];
    [self.middleDateArray removeAllObjects];
    [self.giftDateArray removeAllObjects];
    [self.repairDateArray removeAllObjects];
    
    if (_model.days.count > 0) {
        NSDate *currentMonth = [NSString getDateForString:_model.today format:@"yyyy-MM-dd"];
        NSDate *minDate = [self.gregorian dateByAddingUnit:NSCalendarUnitMonth value:-2 toDate:currentMonth options:0];
        NSString *minDayString = [NSString getStringForDate:minDate format:@"yyyy-MM-dd"];
        NSInteger minDateInt = [[minDayString stringByReplacingOccurrencesOfString:@"-" withString:@""] integerValue];
        NSInteger todayDateInt = [[_model.today stringByReplacingOccurrencesOfString:@"-" withString:@""] integerValue];
        NSInteger birthdayDateInt = [[_model.birthday stringByReplacingOccurrencesOfString:@"-" withString:@""] integerValue];
        if (minDateInt < birthdayDateInt) {
            minDateInt = birthdayDateInt;
        }
        [_model.days enumerateObjectsUsingBlock:^(QJSignedItemDetailModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if (obj.haveSigned) {
                if ([obj.sort isEqualToString:@"0"]) {
                    [self.signedDateArray addObject:[NSString getDateForString:obj.date format:@"yyyy-MM-dd"]];
                }else if ([obj.sort isEqualToString:@"1"]) {
                    [self.leftDateArray addObject:[NSString getDateForString:obj.date format:@"yyyy-MM-dd"]];
                }else if ([obj.sort isEqualToString:@"7"]) {
                    [self.rightDateArray addObject:[NSString getDateForString:obj.date format:@"yyyy-MM-dd"]];
                }else {
                    [self.middleDateArray addObject:[NSString getDateForString:obj.date format:@"yyyy-MM-dd"]];
                }
            }else{
                NSInteger currentDate = [[obj.date stringByReplacingOccurrencesOfString:@"-" withString:@""] integerValue];
                
                if ((currentDate > minDateInt) && (currentDate < todayDateInt)) {
                    [self.repairDateArray addObject:[NSString getDateForString:obj.date format:@"yyyy-MM-dd"]];
                }
            }
            
            if (obj.bigPresent) {
                [self.giftDateArray addObject:[NSString getDateForString:obj.date format:@"yyyy-MM-dd"]];
            }
        }];
    }
}

@end
