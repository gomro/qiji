//
//  QJCalendarHeaderView.h
//  QJBox
//
//  Created by rui on 2022/7/18.
//

#import <UIKit/UIKit.h>
/* model */
#import "QJSignedModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJCalendarHeaderView : UIView

/* 数据源 */
@property (nonatomic, strong) QJSignedModel *model;
- (void)updateDateModel:(QJSignedModel *)model;

@property (nonatomic, copy) void (^btnClick)(NSString *model);

@property (nonatomic, copy) void (^leftMonthClick)(NSString *month);
@property (nonatomic, copy) void (^rightMonthClick)(NSString *month);

@end

NS_ASSUME_NONNULL_END
