//
//  QJScreenPopVIew.h
//  QJBox
//
//  Created by rui on 2022/6/28.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, QJScreenPopViewPopType) {
    QJScreenPopViewPopTypeDefault,//默认从中间弹出
    QJScreenPopViewPopTypePopFromBottom,//从底部弹出
};

NS_ASSUME_NONNULL_BEGIN

@interface QJScreenPopVIew : UIView

//点击背景是否消失
@property (nonatomic, assign) BOOL dismissWhenClicked;
//中间视图Y轴方向上偏移量默认为0 在showInView 方法调用前设置
@property (nonatomic, assign) CGFloat centerViewOffsetY;
//点击背景消失时的回调
@property (nonatomic, strong) void(^tapDismissBlock)(void);
//弹出方式
@property (nonatomic, assign) QJScreenPopViewPopType popViewPopType;

/**
 创建显示弹窗

 @param view 中间显示的内容View
 @return 弹窗控件
 */
- (instancetype)initWithCenterView:(UIView *)view;

/**
 显示的方法

 @param aView 弹窗的父视图
 */
- (void)showInView:(UIView *)aView;

/**
 消失的方法

 @param block 弹窗消失的回调
 */
- (void)dismissWithBlock:(void(^ __nullable)(void))block;

@end

NS_ASSUME_NONNULL_END
