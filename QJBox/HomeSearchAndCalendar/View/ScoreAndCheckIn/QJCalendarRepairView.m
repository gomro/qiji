//
//  QJCalendarRepairView.m
//  QJBox
//
//  Created by rui on 2022/7/18.
//

#import "QJCalendarRepairView.h"

@interface QJCalendarRepairView ()

//白色底图
@property (nonatomic, strong) UIView *whiteBgView;
//标题
@property (nonatomic, strong) UILabel *repairTitleLabel;
//补签次数
@property (nonatomic, strong) UILabel *repairCountLabel;
//封面图
@property (nonatomic, strong) UIImageView *iconImageView;
//标题
@property (nonatomic, strong) UILabel *titleLabel;
//描述
@property (nonatomic, strong) UILabel *subTitleLabel;
//剩余次数
@property (nonatomic, strong) UIButton *countButton;

@end

@implementation QJCalendarRepairView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        /* 初始化UI */
        [self initUI];
        /* 添加约束 */
        [self makeConstraints];
    }
    return self;
}

/**
 * @author: zjr
 * @date: 2022-6-29
 * @desc: 初始化UI
 */
- (void)initUI{
    self.backgroundColor = [UIColor colorWithHexString:@"0xF7F7F7"];
    [self addSubview:self.whiteBgView];
    [self.whiteBgView addSubview:self.repairTitleLabel];
    [self.whiteBgView addSubview:self.repairCountLabel];
    [self.whiteBgView addSubview:self.iconImageView];
    [self.whiteBgView addSubview:self.titleLabel];
    [self.whiteBgView addSubview:self.subTitleLabel];
    [self.whiteBgView addSubview:self.countButton];
}

/**
 * @author: zjr
 * @date: 2022-7-1
 * @desc: 添加约束
 */
- (void)makeConstraints{
    [self.whiteBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(15);
        make.right.equalTo(self).offset(-15);
        make.top.bottom.equalTo(self).offset(0);
    }];
    
    [self.repairTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.whiteBgView).offset(17);
        make.left.equalTo(self.whiteBgView).offset(17);
        make.height.equalTo(@(20*kWScale));
    }];
    
    [self.repairCountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.whiteBgView).offset(15);
        make.right.equalTo(self.whiteBgView).offset(-15);
        make.height.equalTo(@(20*kWScale));
    }];
    
    [self.iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.repairTitleLabel.mas_bottom).offset(10);
        make.left.equalTo(self.whiteBgView).offset(15);
        make.height.equalTo(@(40*kWScale));
        make.width.equalTo(@(40*kWScale));
    }];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.iconImageView);
        make.left.equalTo(self.iconImageView.mas_right).offset(10);
        make.height.equalTo(@(25*kWScale));
    }];
    
    [self.subTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.iconImageView).offset(5);
        make.left.equalTo(self.iconImageView.mas_right).offset(10);
        make.height.equalTo(@(20*kWScale));
    }];
    
    [self.countButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.iconImageView);
        make.width.equalTo(@(82*kWScale));
        make.height.equalTo(@(31*kWScale));
        make.right.equalTo(self.whiteBgView).offset(-15);
    }];
}

#pragma mark - Lazy Loading
- (UIView *)whiteBgView {
    if (!_whiteBgView) {
        _whiteBgView = [UIView new];
        _whiteBgView.backgroundColor = [UIColor whiteColor];
        _whiteBgView.layer.cornerRadius = 8;
    }
    return _whiteBgView;
}

- (UILabel *)repairTitleLabel {
    if (!_repairTitleLabel) {
        _repairTitleLabel = [UILabel new];
        _repairTitleLabel.font = kFont(16);
        _repairTitleLabel.textColor = [UIColor blackColor];
        _repairTitleLabel.text = @"获取补签卡";
    }
    return _repairTitleLabel;
}

- (UILabel *)repairCountLabel {
    if (!_repairCountLabel) {
        _repairCountLabel = [UILabel new];
        _repairCountLabel.font = kFont(12);
        _repairCountLabel.textColor = UIColorFromRGB(0x919599);
        _repairCountLabel.text = @"剩余 0/3次";
        _repairCountLabel.textAlignment = NSTextAlignmentRight;
    }
    return _repairCountLabel;
}

- (UIImageView *)iconImageView{
    if (!_iconImageView) {
        _iconImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"sign_icoincount_repair"]];
    }
    return _iconImageView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.font = kFont(16);
        _titleLabel.textColor = [UIColor blackColor];
        _titleLabel.text = @"奇迹币兑换";
    }
    return _titleLabel;
}

- (UILabel *)subTitleLabel {
    if (!_subTitleLabel) {
        _subTitleLabel = [UILabel new];
        _subTitleLabel.font = kFont(12);
        _subTitleLabel.textColor = [UIColor blackColor];
        _subTitleLabel.text = @"使用奇迹币兑换补签卡";
    }
    return _subTitleLabel;
}

- (UIButton *)countButton {
    if (!_countButton) {
        _countButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_countButton addTarget:self action:@selector(btnAction) forControlEvents:UIControlEventTouchUpInside];
        _countButton.titleLabel.font = kboldFont(14);
        _countButton.backgroundColor = UIColorFromRGB(0xFFE9C9);
        [_countButton setTitle:@"5币兑换" forState:UIControlStateNormal];
        [_countButton setTitleColor:UIColorFromRGB(0xFF9500) forState:UIControlStateNormal];
        _countButton.layer.cornerRadius = 6;
        _countButton.layer.masksToBounds = YES;
        
    }
    return _countButton;
}

/**
 * @author: zjr
 * @date: 2022-6-30
 * @desc: 数据赋值
 */
- (void)setModel:(QJSignedModel *)model{
    _model = model;
    _repairCountLabel.text = [NSString stringWithFormat:@"剩余 %@/3次",_model.remainSignSupplyCount];
}

- (void)btnAction{
    DLog(@"去兑换");
    if ([_model.remainSignSupplyCount isEqualToString:@"0"]) {
        [QJAppTool showToast:@"奇迹币兑换补签次数为0"];
    }else{
        if (self.btnClick) {
            self.btnClick(_model);
        }
    }
}

@end
