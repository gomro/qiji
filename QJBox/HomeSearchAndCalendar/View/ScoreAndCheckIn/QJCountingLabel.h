#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, QJLabelCountingMethod) {
    QJLabelCountingMethodEaseInOut,
    QJLabelCountingMethodEaseIn,
    QJLabelCountingMethodEaseOut,
    QJLabelCountingMethodLinear,
    QJLabelCountingMethodEaseInBounce,
    QJLabelCountingMethodEaseOutBounce
};

typedef NSString* (^QJCountingLabelFormatBlock)(CGFloat value);
typedef NSAttributedString* (^QJCountingLabelAttributedFormatBlock)(CGFloat value);

@interface QJCountingLabel : UILabel

@property (nonatomic, strong) NSString *format;
@property (nonatomic, assign) QJLabelCountingMethod method;
@property (nonatomic, assign) NSTimeInterval animationDuration;

@property (nonatomic, copy) QJCountingLabelFormatBlock formatBlock;
@property (nonatomic, copy) QJCountingLabelAttributedFormatBlock attributedFormatBlock;
@property (nonatomic, copy) void (^completionBlock)(void);

-(void)countFrom:(CGFloat)startValue to:(CGFloat)endValue;
-(void)countFrom:(CGFloat)startValue to:(CGFloat)endValue withDuration:(NSTimeInterval)duration;

-(void)countFromCurrentValueTo:(CGFloat)endValue;
-(void)countFromCurrentValueTo:(CGFloat)endValue withDuration:(NSTimeInterval)duration;

-(void)countFromZeroTo:(CGFloat)endValue;
-(void)countFromZeroTo:(CGFloat)endValue withDuration:(NSTimeInterval)duration;

- (CGFloat)currentValue;

@end

