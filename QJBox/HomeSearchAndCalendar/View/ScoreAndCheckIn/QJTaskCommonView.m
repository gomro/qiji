//
//  QJTaskCommonView.m
//  QJBox
//
//  Created by rui on 2022/7/12.
//

#import "QJTaskCommonView.h"
/* 自定义Cell */
#import "QJTaskAndCoinCollectionCell.h"
/* 弹框View */
#import "QJScreenPopVIew.h"
/* 弹框公共 */
#import "QJCheckInPopCommonView.h"
#import "QJMoreDataScrollView.h"

@interface QJTaskCommonView ()<UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource>{
    NSMutableArray *_colorArray;
    NSMutableArray *_buttonColorArray;
    CGFloat _distance;
    CGFloat _maxDistance;
}

/* collection */
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) QJMoreDataScrollView *moreView;

@end

@implementation QJTaskCommonView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        _colorArray = [NSMutableArray array];
        [_colorArray addObject:UIColorFromRGB(0xF8EDDA)];
        [_colorArray addObject:UIColorFromRGB(0xE8EBFC)];
        [_colorArray addObject:UIColorFromRGB(0xEEF5FA)];
        
        _buttonColorArray = [NSMutableArray array];
        [_buttonColorArray addObject:UIColorFromRGB(0xE07B42)];
        [_buttonColorArray addObject:UIColorFromRGB(0x7E92FF)];
        [_buttonColorArray addObject:UIColorFromRGB(0x92D1FF)];
        
        _maxDistance = 50;
        _distance = 0;

        /* 初始化UI */
        [self initUI];
        /* 添加约束 */
        [self makeConstraints];
    }
    return self;
}

/**
 * @author: zjr
 * @date: 2022-7-12
 * @desc: 初始化UI
 */
- (void)initUI{
    self.backgroundColor = [UIColor whiteColor];
    QJMoreDataScrollView *moreView = [[QJMoreDataScrollView alloc] initWithFrame:CGRectMake(0, 0, 0, 190*kWScale)];
    moreView.frame = CGRectMake(0,  0, QJScreenWidth-2*15, 190*kWScale);
    moreView.backgroundColor = [UIColor clearColor];
    [self addSubview:moreView];
    self.moreView = moreView;

    [self addSubview:self.collectionView];
}

/**
 * @author: zjr
 * @date: 2022-7-12
 * @desc: 添加约束
 */
- (void)makeConstraints{
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
//        .insets(UIEdgeInsetsMake(15, 15, 15, 15));
        make.edges.mas_equalTo(self);
    }];
}

#pragma mark - Lazy Loading
- (UICollectionView *)collectionView{
    if (!_collectionView) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        layout.minimumLineSpacing = 10;
        layout.minimumInteritemSpacing = 10;
        layout.sectionInset = UIEdgeInsetsMake(15, 15, 15, 15);
        layout.itemSize = CGSizeMake(116*kWScale, 130*kWScale);
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.backgroundColor = [UIColor clearColor];//UIColorFromRGB(0xFFFFFF);
        _collectionView.showsHorizontalScrollIndicator = NO;
        [_collectionView registerClass:[QJTaskAndCoinCollectionCell class] forCellWithReuseIdentifier:@"QJTaskAndCoinCollectionCell"];
    }
    return _collectionView;
}

/**
 * @author: zjr
 * @date: 2022-7-4
 * @desc: 数据赋值
 */
- (void)setModel:(QJTaskDetailModel *)model{
    _model = model;
    if (self.model.userTaskDetails.count > 0) {
        QJTaskDetailModel *model = [self.model.userTaskDetails firstObject];
        model.isSelect = YES;
        for (int i = 0; i < self.model.userTaskDetails.count; i ++) {
            QJTaskDetailModel *dataModel = [self.model.userTaskDetails safeObjectAtIndex:i];
            dataModel.backgroundColor = [_colorArray safeObjectAtIndex:(i % 3)];
            dataModel.buttonBackgroundColor = [_buttonColorArray safeObjectAtIndex:(i % 3)];
        }
    }
    [self.collectionView reloadData];
}

#pragma mark  -------- UICollectionViewDelegate,UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    QJEmptyCommonView *emptyView = [QJEmptyCommonView new];
    emptyView.string = @"任务都做完啦 明天再来看看～";
    emptyView.topSpace = 100*kWScale;
    [collectionView collectionViewDisplayWhenHaveNoDataWithView:emptyView ifNecessaryForRowCount:self.model.userTaskDetails.count];
    return self.model.userTaskDetails.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    QJTaskDetailModel *model = [self.model.userTaskDetails safeObjectAtIndex:indexPath.item];
    QJTaskAndCoinCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"QJTaskAndCoinCollectionCell" forIndexPath:indexPath];
    model.rewardEnum = _model.rewardEnum;
    cell.model = model;
    WS(weakSelf)
    cell.btnClick = ^(QJTaskDetailModel * _Nonnull model) {
        for (QJTaskDetailModel *dataModel in weakSelf.model.userTaskDetails) {
            dataModel.isSelect = NO;
        }
        model.isSelect = YES;
        [UIView performWithoutAnimation:^{
            [weakSelf.collectionView reloadData];
            [weakSelf.collectionView layoutIfNeeded];
        }];
        if (weakSelf.btnClick) {
            weakSelf.btnClick(model);
        }
    };
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    QJTaskDetailModel *model = [self.model.userTaskDetails safeObjectAtIndex:indexPath.item];
    if (model.isSelect) {
        return CGSizeMake(110*kWScale, 145*kWScale);
    }else{
        return CGSizeMake(100*kWScale, 132*kWScale);
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    QJTaskDetailModel *model = [self.model.userTaskDetails safeObjectAtIndex:indexPath.item];
    for (QJTaskDetailModel *dataModel in self.model.userTaskDetails) {
        dataModel.isSelect = NO;
    }
    model.isSelect = YES;
    [UIView performWithoutAnimation:^{
        [self.collectionView reloadData];
        [self.collectionView layoutIfNeeded];
    }];
    if (self.contentBtnClick) {
        self.contentBtnClick(model);
    }
}

//- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
//    CGFloat inset = scrollView.contentOffset.x+QJScreenWidth-30-scrollView.contentSize.width;
//    _distance = inset;
//    if (inset > _maxDistance) {
//        inset = _maxDistance;
//    }
//    if (_distance >= _maxDistance) {
//        _distance = _maxDistance;
//    }
//}
//
//- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset{
//    if (targetContentOffset->x == 0) return;
//    if (self.model.taskDetails.count > 3) {
//        if (_distance == _maxDistance) {
//            [self makeToast:@"加载更多"];
//        }
//    }
//}

#pragma mark 拖拽的操作

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat inset = scrollView.contentOffset.x + QJScreenWidth-2*16 - scrollView.contentSize.width ;
    _distance = inset;
    if (inset > _maxDistance) {
        inset = _maxDistance;
    }
    if (_distance >= _maxDistance) {
        _distance = _maxDistance;
    }
    _moreView.curveInset = inset;
}

#pragma mark 松开手的操作

-(void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset{
    if (targetContentOffset->x == 0) return;
//    if (self.model.taskDetails.count > 3) {
//
//    }
    if (_distance == _maxDistance) {
        if (self.moreBtnClick) {
            self.moreBtnClick();
        }
    }
}
@end
