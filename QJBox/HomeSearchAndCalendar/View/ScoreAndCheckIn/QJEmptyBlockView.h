//
//  QJEmptyBlockView.h
//  QJBox
//
//  Created by macm on 2022/8/4.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void(^EmptyClick)(void);

@interface QJEmptyBlockView : UIView
/**
 * @author: zjr
 * @date: 2022-6-28
 * @desc:message-显示文案，image-显示图片
 */
- (instancetype)initWithMessage:(NSString *)message image:(UIImage *)image title:(NSString *)title;

- (instancetype)initWithMessage:(NSString *)message image:(UIImage *)image title:(NSString *)title titleColor:(NSString *)titleColor BGColor:(NSString *)bgColor;

@property (nonatomic, copy) EmptyClick emptyClick;

@end

NS_ASSUME_NONNULL_END
