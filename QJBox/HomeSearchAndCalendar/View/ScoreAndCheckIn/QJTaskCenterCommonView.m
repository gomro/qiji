//
//  QJTaskCenterCommonView.m
//  QJBox
//
//  Created by rui on 2022/8/15.
//

#import "QJTaskCenterCommonView.h"
#import "QJProductDetailViewController.h"
#import "QJReceiveRewardsVC.h"
/* 任务中心公共cell */
#import "QJTaskCenterCommonCell.h"
/* 任务中心公共model */
#import "QJTaskCenterCommonModel.h"
#import "QJSearchGameListModel.h"
/* 任务中心请求 */
#import "QJTaskRequest.h"
/* 倒计时 */
#import "QJCountDown.h"
/* 弹框View */
#import "QJScreenPopVIew.h"
/* 弹框公共 */
#import "QJCheckInPopCommonView.h"

/**
 * @author: zjr
 * @date: 2022-8-15
 * @desc: 自定义headerview
 */
@interface QJTaskCustomHeaderView : UIView

//白色底图
@property (nonatomic, strong) UIView *whiteBgView;
/* icon */
@property (nonatomic, strong) UIImageView *iconImageView;
//任务标题
@property (nonatomic, strong) UILabel *titleLabel;
//任务名字
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UIButton *detailButton;

//任务描述
@property (nonatomic, strong) UILabel *subLabel;
//任务名额
@property (nonatomic, strong) UILabel *countLabel;
//任务完成按钮
@property (nonatomic, strong) UIButton *statusButton;
@property (nonatomic, strong) QJCountDown *countDown;

@property (nonatomic, strong) QJTaskCenterCommonDetailModel *model;
@property (nonatomic, copy) void (^btnClick)(QJTaskCenterCommonDetailModel *model);

@end

@implementation QJTaskCustomHeaderView

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        /* 初始化UI */
        [self initUI];
        /* 添加约束 */
        [self makeConstraints];
    }
    return self;
}

/**
 * @author: zjr
 * @date: 2022-6-27
 * @desc: 初始化UI
 */
- (void)initUI{
    [self addSubview:self.whiteBgView];
    [self.whiteBgView addSubview:self.iconImageView];
    [self.whiteBgView addSubview:self.nameLabel];
    [self.whiteBgView addSubview:self.titleLabel];
    [self.whiteBgView addSubview:self.detailButton];
    [self.whiteBgView addSubview:self.subLabel];
    [self.whiteBgView addSubview:self.countLabel];
    [self.whiteBgView addSubview:self.statusButton];
}

/**
 * @author: zjr
 * @date: 2022-7-1
 * @desc: 添加约束
 */
- (void)makeConstraints{
    [self.whiteBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(16);
        make.right.equalTo(self).offset(-16);
        make.top.bottom.equalTo(self);
    }];
    
    [self.iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.whiteBgView).offset(10);
        make.left.equalTo(self.whiteBgView).offset(16);
        make.height.equalTo(@(40*kWScale));
        make.width.equalTo(@(40*kWScale));
    }];
    
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.iconImageView.mas_bottom).offset(4);
        make.left.equalTo(self.whiteBgView).offset(16);
        make.width.equalTo(@(55*kWScale));
        make.height.equalTo(@(15*kWScale));
    }];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.whiteBgView).offset(10);
        make.left.equalTo(self.iconImageView.mas_right).offset(13);
        make.height.equalTo(@(20*kWScale));
    }];
    
    [self.detailButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.bottom.equalTo(self.whiteBgView);
        make.right.equalTo(self.titleLabel.mas_left);
    }];
    
    [self.subLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).offset(8);
        make.left.equalTo(self.iconImageView.mas_right).offset(13);
        make.height.equalTo(@(15*kWScale));
    }];
    
    [self.statusButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.whiteBgView).offset(15);
        make.width.equalTo(@(60*kWScale));
        make.height.equalTo(@(28*kWScale));
        make.right.equalTo(self.whiteBgView).offset(-16);
    }];
    
    [self.countLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.statusButton.mas_bottom).offset(12);
        make.centerX.equalTo(self.statusButton);
//        make.right.equalTo(self.whiteBgView).offset(-16);
        make.height.equalTo(@(15*kWScale));
        make.bottom.equalTo(self.whiteBgView).offset(-5);
    }];
    self.countDown = [QJCountDown new];
}

/**
 * @author: zjr
 * @date: 2022-8-19
 * @desc: 数据赋值
 */
- (void)setModel:(QJTaskCenterCommonDetailModel *)model{
    if (_model != model) {
        _model = model;
        [self.countDown destoryTimer];
        [self.iconImageView setImageWithURL:[NSURL URLWithString:[_model.icon checkImageUrlString]] placeholder:[UIImage imageNamed:@"mine_head"]];
        self.titleLabel.text = _model.name;
        self.nameLabel.text = _model.gameName;
        self.subLabel.text = _model.rewardDesc;
        if ([_model.status isEqualToString:@"0"]) {
            [_statusButton setBackgroundImage:[UIImage imageNamed:@"qj_taskbutton_bgicon_yellow"] forState:UIControlStateNormal];
            [_statusButton setTitle:@"去完成" forState:UIControlStateNormal];
            WS(weakSelf)
            [self.countDown countDownWithStratTimeStamp:[self.model.startTimeMillis longLongValue] finishTimeStamp:[self.model.endTimeMillis longLongValue] completeBlock:^(NSInteger day, NSInteger hour, NSInteger minute, NSInteger second) {
                weakSelf.countLabel.text = [NSString stringWithFormat:@"%02ld:%02ld:%02ld", (long)(day*24+hour), (long)minute, (long)second];
            }];
            self.countDown.TimerStopComplete = ^{
                [weakSelf.countDown destoryTimer];
            };
        }else if ([_model.status isEqualToString:@"1"] && [_model.receive isEqualToString:@"0"]) {
            self.countLabel.text = @"";
            [_statusButton setBackgroundImage:[UIImage imageNamed:@"qj_taskbutton_bgicon_yellow"] forState:UIControlStateNormal];
            [_statusButton setTitle:@"领取奖励" forState:UIControlStateNormal];
        }else if ([_model.status isEqualToString:@"1"] && [_model.receive isEqualToString:@"1"]) {
            self.countLabel.text = @"";
            [_statusButton setBackgroundImage:[UIImage imageNamed:@"qj_taskbutton_bgicon_gray"] forState:UIControlStateNormal];
            [_statusButton setTitle:@"已领完" forState:UIControlStateNormal];
        }else{
            self.countLabel.text = @"";
            self.countLabel.text = _model.checkReason;
            [_statusButton setBackgroundImage:[UIImage imageNamed:@"qj_taskbutton_bgicon_gray"] forState:UIControlStateNormal];
            [_statusButton setTitle:@"已失效" forState:UIControlStateNormal];
        }
    }
}

#pragma mark - Lazy Loading
- (UIImageView *)iconImageView{
    if (!_iconImageView) {
        _iconImageView = [UIImageView new];
        _iconImageView.layer.cornerRadius = 10;
        _iconImageView.layer.masksToBounds = YES;
        _iconImageView.contentMode = UIViewContentModeScaleAspectFill;

    }
    return _iconImageView;
}

- (UIView *)whiteBgView {
    if (!_whiteBgView) {
        _whiteBgView = [UIView new];
        _whiteBgView.backgroundColor = [UIColor whiteColor];
    }
    return _whiteBgView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.font = kFont(14);
        _titleLabel.textColor = UIColorFromRGB(0x000000);
        _titleLabel.text = @"走完游戏地图";
    }
    return _titleLabel;
}

- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [UILabel new];
        _nameLabel.font = kFont(10);
        _nameLabel.textColor = UIColorFromRGB(0x000000);
        _nameLabel.text = @"荣耀大天使";
    }
    return _nameLabel;
}

- (UILabel *)subLabel {
    if (!_subLabel) {
        _subLabel = [UILabel new];
        _subLabel.font = kFont(12);
        _subLabel.textColor = [UIColor hx_colorWithHexStr:@"0x000000" alpha:0.8];
        _subLabel.text = @"可获道具：振奋铠甲";
    }
    return _subLabel;
}

- (UILabel *)countLabel {
    if (!_countLabel) {
        _countLabel = [UILabel new];
        _countLabel.font = kFont(10);
        _countLabel.textColor = UIColorFromRGB(0x000000);
        _countLabel.text = @"名额0/10";
    }
    return _countLabel;
}

- (UIButton *)statusButton {
    if (!_statusButton) {
        _statusButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_statusButton addTarget:self action:@selector(btnAction) forControlEvents:UIControlEventTouchUpInside];
        _statusButton.titleLabel.font = kboldFont(12);
        [_statusButton setBackgroundImage:[UIImage imageNamed:@"qj_taskbutton_bgicon_yellow"] forState:UIControlStateNormal];
        [_statusButton setTitle:@"领取" forState:UIControlStateNormal];
        [_statusButton setTitleColor:UIColorFromRGB(0x4C1C0F) forState:UIControlStateNormal];
    }
    return _statusButton;
}

- (UIButton *)detailButton {
    if (!_detailButton) {
        _detailButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_detailButton addTarget:self action:@selector(detailAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _detailButton;
}

- (void)detailAction{
    QJProductDetailViewController *detailVC = [[QJProductDetailViewController alloc] init];
    QJSearchGameDetailModel *detailModel = [QJSearchGameDetailModel new];
    detailModel.ID = self.model.gameId;
    detailVC.model = detailModel;
    [[QJAppTool getCurrentViewController].navigationController pushViewController:detailVC animated:YES];
}

- (void)btnAction{
    if (self.btnClick) {
        self.btnClick(self.model);
    }
}

@end

@interface QJTaskCenterCommonView()<UITableViewDelegate,UITableViewDataSource>

/* table */
@property (nonatomic, strong) UITableView *tableView;
/* model */
@property (nonatomic, strong) QJTaskCenterCommonModel *pageModel;

@end

@implementation QJTaskCenterCommonView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.pageModel = [[QJTaskCenterCommonModel alloc] init];
        /* 初始化View */
        [self initChildView];
    }
    return self;
}

/**
 * @author: zjr
 * @date: 2022-8-15
 * @desc: 加载view/约束
 */
- (void)initChildView {
//    self.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
}

#pragma mark --Lazy Loading
-(UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = [UIColor clearColor];
        [_tableView registerClass:[QJTaskCenterCommonCell class] forCellReuseIdentifier:@"QJTaskCenterCommonCell"];
        // 自动计算行高模式
        _tableView.estimatedRowHeight = 76;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        if (@available(iOS 11.0, *)) {
            _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
        if (@available(iOS 15.0, *)) {
            _tableView.sectionHeaderTopPadding = 0;
        }
        _tableView.mj_header = [QJRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(refreshFirst)];
        _tableView.mj_footer = [QJRefreshFooter footerWithRefreshingTarget:self refreshingAction:@selector(refreshMore)];
    }
    return _tableView;
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _pageModel.records.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    QJTaskCenterCommonDetailModel *model = [_pageModel.records safeObjectAtIndex:section];
    if (model.isChoosesOpen) {
        return 1;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    QJTaskCenterCommonCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QJTaskCenterCommonCell" forIndexPath:indexPath];
    QJTaskCenterCommonDetailModel *model = [_pageModel.records safeObjectAtIndex:indexPath.section];
    model.type = self.type;
    cell.model = model;
    return cell;
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 76*kWScale;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 8;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    QJTaskCustomHeaderView *headerView = [[QJTaskCustomHeaderView alloc] initWithFrame:CGRectMake(16, 0, QJScreenWidth-32, 76*kWScale)];
    QJTaskCenterCommonDetailModel *model = [_pageModel.records safeObjectAtIndex:section];
    model.type = self.type;
    headerView.model = model;
    WS(weakSelf)
    headerView.btnClick = ^(QJTaskCenterCommonDetailModel *model) {
        [weakSelf pushDetail:model];
    };
    headerView.tag = section + 11111;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickHeaderView:)];
    [headerView addGestureRecognizer:tap];
    return headerView;
}

- (void)clickHeaderView:(UITapGestureRecognizer *)sender {
    QJTaskCenterCommonDetailModel *model = [_pageModel.records safeObjectAtIndex:sender.view.tag-11111];
    model.isChoosesOpen = !model.isChoosesOpen;
    [self.tableView reloadSection:sender.view.tag-11111 withRowAnimation:UITableViewRowAnimationFade];
    if (model.isChoosesOpen) {
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:sender.view.tag-11111] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    }
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, QJScreenWidth, 8)];
    footerView.backgroundColor = UIColorFromRGB(0xF5F5F5);
    return footerView;
}

- (void)pushDetail:(QJTaskCenterCommonDetailModel *)model{
    QJTaskRequest *productReq = [[QJTaskRequest alloc] init];
    NSMutableDictionary *muDic = [NSMutableDictionary dictionary];
    [muDic setValue:model.ID forKey:@"id"];
    productReq.dic = muDic.copy;
    [productReq getTaskcheckTaskRequest];
    
    WS(weakSelf)
    productReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            [weakSelf checkTaskStatus:model];
        }else{
            model.status = @"2";
            [weakSelf checkTaskStatus:model];
        }
    };
    productReq.failureCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        [QJAppTool showToast:@"数据异常 请稍后再试"];
    };
}

#pragma mark - 请求方法
/**
 * @author: zjr
 * @date: 2022-8-2
 * @desc: 请求方法
 */
- (void)refreshFirst{
    _pageModel.willLoadMore = NO;
    [self sendRequest];
}

- (void)refreshMore{
    if(_pageModel.hasNext){
        _pageModel.willLoadMore=YES;
        [self sendRequest];
    }else{
        [self.tableView.mj_footer endRefreshing];
        [self.tableView.mj_footer endRefreshingWithNoMoreData];
    }
}

- (void)sendRequest {
    QJTaskRequest *productReq = [[QJTaskRequest alloc] init];
    NSMutableDictionary *muDic = [NSMutableDictionary dictionaryWithDictionary:_pageModel.toParams];
    [muDic setValue:@"20" forKey:@"size"];
    if ([self.type isEqual:@"receive"]) {
        [muDic setValue:@"0" forKey:@"status"];
    }else if ([self.type isEqual:@"done"]) {
        [muDic setValue:@"1" forKey:@"status"];
    }else{
        [muDic setValue:@"2" forKey:@"status"];
    }
    productReq.dic = muDic.copy;
    [productReq getMyTaskListRequest];
    
    WS(weakSelf)
    productReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            [weakSelf.pageModel configObj:[QJTaskCenterCommonModel modelWithDictionary:EncodeDicFromDic(request.responseObject, @"data")]];
        }else{
            NSString *msg = EncodeStringFromDic(request.responseObject, @"message");
            [QJAppTool showToast:msg];
        }
        [weakSelf.tableView reloadData];
        [weakSelf.tableView.mj_header endRefreshing];
        [weakSelf.tableView.mj_footer endRefreshing];
        QJEmptyCommonView *emptyView = [QJEmptyCommonView new];
        emptyView.string = @"暂无内容";
        emptyView.topSpace = 100*kWScale;
        [weakSelf.tableView tableViewDisplayWhenHaveNoDataWithView:emptyView ifNecessaryForRowCount:weakSelf.pageModel.records.count];
        DLog(@"tableView1 = %f",weakSelf.tableView.contentSize.height);
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            DLog(@"tableView2 = %f",weakSelf.tableView.contentSize.height);
        });
    };

    productReq.failureCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        [weakSelf.tableView reloadData];
        [weakSelf.tableView.mj_header endRefreshing];
        [weakSelf.tableView.mj_footer endRefreshing];
        [QJAppTool showToast:@"数据异常 请稍后再试"];
        QJEmptyCommonView *emptyView = [QJEmptyCommonView new];
        emptyView.string = @"暂无内容";
        emptyView.topSpace = 100*kWScale;
        [weakSelf.tableView tableViewDisplayWhenHaveNoDataWithView:emptyView ifNecessaryForRowCount:weakSelf.pageModel.records.count];
    };
}

- (void)receiveRewardReq:(QJTaskCenterCommonDetailModel *)model{
    QJTaskRequest *productReq = [[QJTaskRequest alloc] init];
    NSMutableDictionary *muDic = [NSMutableDictionary dictionary];
    [muDic setValue:model.ID forKey:@"id"];
    productReq.dic = muDic.copy;
    [productReq getTaskRewardReceiveRequest];
    
    WS(weakSelf)
    productReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            [weakSelf initTaskPopView:model];
            [weakSelf refreshFirst];
        }else{
            NSString *msg = EncodeStringFromDic(request.responseObject, @"message");
            [QJAppTool showToast:msg];
        }
    };
    productReq.failureCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        [QJAppTool showToast:@"数据异常 请稍后再试"];
    };
}

- (void)checkTaskStatus:(QJTaskCenterCommonDetailModel *)model{
    if ([model.status isEqualToString:@"0"]) {
        DLog(@"去完成");
        QJProductDetailViewController *detailVC = [[QJProductDetailViewController alloc] init];
        QJSearchGameDetailModel *detailModel = [QJSearchGameDetailModel new];
        detailModel.ID = model.gameId;
        detailVC.model = detailModel;
        [[QJAppTool getCurrentViewController].navigationController pushViewController:detailVC animated:YES];
    }else if ([model.status isEqualToString:@"1"] && [model.receive isEqualToString:@"0"]) {
        DLog(@"领取奖励");
        if ([model.rewardType isEqualToString:@"0"]) {
            [self receiveRewardReq:model];
        }else{
            QJReceiveRewardsVC *rewardVC = [[QJReceiveRewardsVC alloc] init];
            [[QJAppTool getCurrentViewController].navigationController pushViewController:rewardVC animated:YES];
        }
    }else if ([model.status isEqualToString:@"1"] && [model.receive isEqualToString:@"1"]) {
        DLog(@"已领完");
        [self initTaskPopView:model];
    }else{
        DLog(@"失效");
        [self initTaskPopView:model];
    }
}

/**
 * @author: zjr
 * @date: 2022-7-5
 * @desc: 领取任务奖励
 */
- (void)initTaskPopView:(QJTaskCenterCommonDetailModel *)model{
    QJCheckInPopCommonView *contentView = [[QJCheckInPopCommonView alloc] initWithFrame:CGRectMake(0, 0, 300, 344)];
    contentView.popViewStyle = QJPopContentViewStyleTaskMyRECEIVED;
    contentView.model = model;
    contentView.directionBlock = ^(ButtonClickType direction) {
        [self sendRequest];
        [self btnAction];
        if (direction == ButtonClickTypeLookTask) {
            QJProductDetailViewController *detailVC = [[QJProductDetailViewController alloc] init];
            QJSearchGameDetailModel *detailModel = [QJSearchGameDetailModel new];
            detailModel.ID = model.gameId;
            detailVC.model = detailModel;
            [[QJAppTool getCurrentViewController].navigationController pushViewController:detailVC animated:YES];
        }
    };
    QJScreenPopVIew *popView = [[QJScreenPopVIew alloc] initWithCenterView:contentView];
    popView.dismissWhenClicked = YES;
    popView.popViewPopType = QJScreenPopViewPopTypeDefault;
    popView.tag = 100000;
    [popView showInView:[UIApplication sharedApplication].keyWindow];
}

- (void)btnAction{
    QJScreenPopVIew *popView = [[UIApplication sharedApplication].keyWindow viewWithTag:100000];
    [popView dismissWithBlock:nil];
}

@end
