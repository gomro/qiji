//
//  QJCheckInHeaderView.m
//  QJBox
//
//  Created by rui on 2022/6/27.
//

#import "QJCheckInHeaderView.h"

#import "QJCountingLabel.h"
#import "QJCheckInCalendarCollectionCell.h"
#import "QJCheckInPostRequest.h"

@interface QJCheckInHeaderView ()<UICollectionViewDelegate,UICollectionViewDataSource>

/* 背景图片 */
@property (nonatomic, strong) UIImageView *circleBgImageView;
/* 积分背景view */
@property (nonatomic, strong) UIView *scoreBgView;
/* 积分标题 */
@property (nonatomic, strong) UILabel *scoreTitleLabel;
/* 积分 */
@property (nonatomic, strong) QJCountingLabel *scoreLabel;
/* 积分管理Button */
@property (nonatomic, strong) UIButton *scoreListButton;
@property (nonatomic, strong) UIView *scoreWhiteView;
@property (nonatomic, strong) UIImageView *scoreYellowView;
@property (nonatomic, strong) UIImageView *scoreIconImageView;
@property (nonatomic, strong) UILabel *icoinLabel;
/* 领取奇迹币Button */
@property (nonatomic, strong) UIButton *getCoinButton;
/* 日历背景view */
@property (nonatomic, strong) UIView *calendarBgView;
/* 签到title */
@property (nonatomic, strong) UILabel *checkInTitleLabel;
@property (nonatomic, strong) UILabel *checkInSubTitleLabel;
/* 签到日历button */
@property (nonatomic, strong) UIButton *calendarButton;
@property (nonatomic, strong) UIButton *monthButton;
/* 签到collection */
@property (nonatomic, strong) UICollectionView *calendarCollectionView;
/* 签到Button */
@property (nonatomic, strong) UIButton *checkInButton;

@end

@implementation QJCheckInHeaderView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        /* 初始化UI */
        [self initUI];
        /* 添加约束 */
        [self makeConstraints];
    }
    return self;
}

/**
 * @author: zjr
 * @date: 2022-6-27
 * @desc: 初始化UI
 */
- (void)initUI{
    self.backgroundColor = [UIColor clearColor];//UIColorFromRGB(0x5DC9A5);
    [self addSubview:self.circleBgImageView];

    [self addSubview:self.scoreBgView];
    [self.scoreBgView addSubview:self.scoreTitleLabel];
    [self.scoreBgView addSubview:self.scoreLabel];
    //金币管理
    [self.scoreBgView addSubview:self.scoreWhiteView];
    [self.scoreBgView addSubview:self.scoreYellowView];
    [self.scoreBgView addSubview:self.scoreIconImageView];
    [self.scoreBgView addSubview:self.icoinLabel];
    [self.scoreBgView addSubview:self.scoreListButton];
    //一键领币
    [self.scoreBgView addSubview:self.getCoinButton];
    
    [self addSubview:self.calendarBgView];
    [self bringSubviewToFront:self.calendarBgView];
    [self.calendarBgView addSubview:self.checkInTitleLabel];
    [self.calendarBgView addSubview:self.checkInSubTitleLabel];
    [self.calendarBgView addSubview:self.calendarButton];
    [self.calendarBgView addSubview:self.monthButton];
    [self.calendarBgView addSubview:self.calendarCollectionView];
    [self.calendarBgView addSubview:self.checkInButton];
}

/**
 * @author: zjr
 * @date: 2022-7-1
 * @desc: 添加约束
 */
- (void)makeConstraints{
    [self.circleBgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self);
        make.height.equalTo(@(392*kWScale));
    }];
    
    [self.scoreBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(NavigationBar_Bottom_Y);
        make.left.right.equalTo(self);
        make.height.equalTo(@(200*kWScale));
    }];
    
    [self.scoreTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.scoreBgView).offset(6);
        make.left.equalTo(self.scoreBgView).offset(30);
        make.right.equalTo(self.scoreBgView).offset(-16);
        make.height.equalTo(@(20*kWScale));
    }];
    
    [self.scoreLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.scoreTitleLabel.mas_bottom);
        make.left.equalTo(self.scoreBgView).offset(30);
        make.height.equalTo(@(40*kWScale));
    }];
    
    [self.getCoinButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.scoreBgView).offset(30);
        make.top.equalTo(self.scoreLabel.mas_bottom).offset(10);
        make.height.equalTo(@(30*kWScale));
        make.width.equalTo(@(85*kWScale));
    }];
    
    [self.scoreWhiteView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.scoreBgView).offset(-27);
        make.centerY.equalTo(self.getCoinButton).offset(-10);
        make.height.equalTo(@(40*kWScale));
        make.width.equalTo(@(40*kWScale));
    }];

    [self.scoreYellowView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.scoreWhiteView);
        make.centerY.equalTo(self.scoreWhiteView);//.offset(-3);
        make.height.equalTo(@(34*kWScale));
        make.width.equalTo(@(34*kWScale));
    }];

    [self.scoreIconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.scoreWhiteView);
        make.centerY.equalTo(self.scoreWhiteView);//.offset(-2);
        make.height.equalTo(@(30*kWScale));
        make.width.equalTo(@(30*kWScale));
    }];
    
    [self.icoinLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.scoreWhiteView);
        make.top.equalTo(self.scoreWhiteView.mas_bottom).offset(-6);
        make.height.equalTo(@(16*kWScale));
        make.width.equalTo(@(44*kWScale));
    }];
    
    [self.scoreListButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.scoreBgView).offset(-27);
        make.centerY.equalTo(self.getCoinButton);
        make.height.equalTo(@(44*kWScale));
        make.width.equalTo(@(56*kWScale));
    }];

    [self.calendarBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(213*kWScale);
        make.left.equalTo(self).offset(16);
        make.right.equalTo(self).offset(-16);
        make.height.equalTo(@(218*kWScale));
    }];
    
    [self.checkInTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.calendarBgView).offset(15);
        make.left.equalTo(self.calendarBgView).offset(16);
        make.height.equalTo(@(20*kWScale));
    }];
    
    [self.checkInSubTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.checkInTitleLabel.mas_bottom).offset(10);
        make.left.equalTo(self.calendarBgView).offset(16);
        make.height.equalTo(@(15*kWScale));
    }];
    
    [self.monthButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.calendarBgView).offset(21);
        make.right.equalTo(self.calendarBgView);
        make.width.equalTo(@(34*kWScale));
        make.height.equalTo(@(31*kWScale));
    }];
    
    [self.calendarButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.monthButton.mas_left).offset(5);
        make.centerY.equalTo(self.monthButton);
        make.height.equalTo(@(26*kWScale));
    }];
    
    [self.checkInButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.calendarBgView).offset(-20);
        make.right.equalTo(self.calendarBgView).offset(-18);
        make.left.equalTo(self.calendarBgView).offset(25);
        make.height.equalTo(@(40*kWScale));
    }];
    
    [self.calendarCollectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.checkInButton.mas_top);
        make.left.right.equalTo(self.calendarBgView);
        make.height.equalTo(@(95*kWScale));
    }];
    
    [_getCoinButton layoutIfNeeded];
    [_getCoinButton graduateLeftColor:UIColorFromRGB(0xFFC26D) ToColor:UIColorFromRGB(0xDF5D25) startPoint:CGPointMake(0.5, 0) endPoint:CGPointMake(0.5, 1)];
}

#pragma mark - Lazy Loading
- (UIView *)scoreBgView{
    if (!_scoreBgView) {
        _scoreBgView = [UIView new];
//        _scoreBgView.backgroundColor = [UIColor colorWithHexString:@"0xEEEEEE"];
    }
    return _scoreBgView;
}

- (UILabel *)scoreTitleLabel {
    if (!_scoreTitleLabel) {
        _scoreTitleLabel = [UILabel new];
        _scoreTitleLabel.font = kFont(14);
        _scoreTitleLabel.textColor = [UIColor whiteColor];
        _scoreTitleLabel.text = @"奇迹币";
    }
    return _scoreTitleLabel;
}

- (QJCountingLabel *)scoreLabel {
    if (!_scoreLabel) {
        _scoreLabel = [QJCountingLabel new];
        _scoreLabel.font = kboldFont(36);
        _scoreLabel.textColor = [UIColor whiteColor];
        _scoreLabel.text = @"0";
        _scoreLabel.format = @"%d";
    }
    return _scoreLabel;
}

- (UIButton *)scoreListButton {
    if (!_scoreListButton) {
        _scoreListButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _scoreListButton.titleLabel.font = kFont(13);
        [_scoreListButton setTitleColor:[UIColor colorWithHexString:@"0xFFFFFF"] forState:UIControlStateNormal];
        [_scoreListButton addTarget:self action:@selector(scoreBtnClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _scoreListButton;
}

- (UIView *)scoreWhiteView {
    if (!_scoreWhiteView) {
        _scoreWhiteView = [UIView new];
        _scoreWhiteView.backgroundColor = [UIColor whiteColor];
        _scoreWhiteView.layer.cornerRadius = 20*kWScale;
        _scoreWhiteView.layer.masksToBounds = YES;
    }
    return _scoreWhiteView;
}

- (UIImageView *)scoreYellowView {
    if (!_scoreYellowView) {
        _scoreYellowView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@""]];
        _scoreYellowView.backgroundColor = UIColorFromRGB(0xF8CA56);
        _scoreYellowView.layer.cornerRadius = 17*kWScale;
        _scoreYellowView.layer.masksToBounds = YES;
    }
    return _scoreYellowView;
}

- (UIImageView *)scoreIconImageView {
    if (!_scoreIconImageView) {
        _scoreIconImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"sign_icoin"]];//icoin_bgicon
    }
    return _scoreIconImageView;
}

- (UILabel *)icoinLabel {
    if (!_icoinLabel) {
        _icoinLabel = [UILabel new];
        _icoinLabel.font = kFont(9);
        _icoinLabel.textColor = UIColorFromRGB(0xBA890C);
        _icoinLabel.text = @"金币管理";
        _icoinLabel.textAlignment = NSTextAlignmentCenter;
        _icoinLabel.backgroundColor = [UIColor whiteColor];
        _icoinLabel.layer.cornerRadius = 8;
        _icoinLabel.layer.masksToBounds = YES;
    }
    return _icoinLabel;
}

- (UIButton *)getCoinButton {
    if (!_getCoinButton) {
        _getCoinButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _getCoinButton.titleLabel.font = kFont(13);
        [_getCoinButton setTitleColor:[UIColor colorWithHexString:@"0xFFFFFF"] forState:UIControlStateNormal];
        [_getCoinButton setTitle:@"一键领币" forState:UIControlStateNormal];
        [_getCoinButton addTarget:self action:@selector(scoreShopBtnClick) forControlEvents:UIControlEventTouchUpInside];
        _getCoinButton.layer.cornerRadius = 15;
        _getCoinButton.layer.masksToBounds = YES;
    }
    return _getCoinButton;
}

- (UIView *)calendarBgView{
    if (!_calendarBgView) {
        _calendarBgView = [UIView new];
        _calendarBgView.backgroundColor = [UIColor whiteColor];
        _calendarBgView.layer.cornerRadius = 8;
        _calendarBgView.layer.masksToBounds = YES;
    }
    return _calendarBgView;
}

- (UIImageView *)circleBgImageView{
    if (!_circleBgImageView) {
        _circleBgImageView = [UIImageView new];
        _circleBgImageView.image = [UIImage imageNamed:@"taskandicon_bg"];
    }
    return _circleBgImageView;
}

- (UILabel *)checkInTitleLabel {
    if (!_checkInTitleLabel) {
        _checkInTitleLabel = [UILabel new];
        _checkInTitleLabel.font = kFont(16);
        _checkInTitleLabel.textColor = [UIColor colorWithHexString:@"0x413012"];
        _checkInTitleLabel.text = @"已连续签到0天";
    }
    return _checkInTitleLabel;
}

- (UILabel *)checkInSubTitleLabel {
    if (!_checkInSubTitleLabel) {
        _checkInSubTitleLabel = [UILabel new];
        _checkInSubTitleLabel.font = kFont(14);
        _checkInSubTitleLabel.textColor = [UIColor colorWithHexString:@"0x413012"];
        _checkInSubTitleLabel.text = @"签到每满7天有惊喜噢！";
    }
    return _checkInSubTitleLabel;
}

- (UIButton *)monthButton {
    if (!_monthButton) {
        _monthButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_monthButton addTarget:self action:@selector(calanderBtnClick) forControlEvents:UIControlEventTouchUpInside];
        _monthButton.titleLabel.font = kboldFont(15);
        [_monthButton setTitle:@"" forState:UIControlStateNormal];
        [_monthButton setTitleColor:[UIColor colorWithHexString:@"0xEE6C44"] forState:UIControlStateNormal];
        [_monthButton setBackgroundImage:[UIImage imageNamed:@"calendar_monthbg"] forState:UIControlStateNormal];
    }
    return _monthButton;
}

- (UIButton *)calendarButton {
    if (!_calendarButton) {
        _calendarButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _calendarButton.titleLabel.font = kFont(11);
        [_calendarButton setTitleColor:[UIColor colorWithHexString:@"0xFFFFFF"] forState:UIControlStateNormal];
        [_calendarButton setTitle:@"查看签到日历" forState:UIControlStateNormal];
        [_calendarButton setBackgroundImage:[UIImage imageNamed:@"calendar_lookbg"] forState:UIControlStateNormal];
        [_calendarButton addTarget:self action:@selector(calanderBtnClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _calendarButton;
}

- (UICollectionView *)calendarCollectionView{
    if (!_calendarCollectionView) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
//        layout.itemSize = CGSizeMake((QJScreenWidth-16*2)/7.0, 95);
        layout.minimumLineSpacing = 0;
        layout.minimumInteritemSpacing = 0;
        _calendarCollectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, QJScreenWidth-16*2, 95) collectionViewLayout:layout];
        [_calendarCollectionView registerClass:[QJCheckInCalendarCollectionCell class] forCellWithReuseIdentifier:@"QJCheckInCalendarCollectionCell"];
        _calendarCollectionView.delegate = self;
        _calendarCollectionView.dataSource = self;
        _calendarCollectionView.backgroundColor = [UIColor clearColor];
        _calendarCollectionView.showsVerticalScrollIndicator = NO;
        _calendarCollectionView.showsHorizontalScrollIndicator = NO;
    }
    return _calendarCollectionView;
}

- (UIButton *)checkInButton {
    if (!_checkInButton) {
        _checkInButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _checkInButton.titleLabel.font = kFont(16);
        [_checkInButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_checkInButton setTitle:@"今日已签到" forState:UIControlStateNormal];
        _checkInButton.backgroundColor = [UIColor blackColor];
        [_checkInButton addTarget:self action:@selector(checkInBtnClick) forControlEvents:UIControlEventTouchUpInside];
        _checkInButton.layer.cornerRadius = 20;
        _checkInButton.layer.masksToBounds = YES;
    }
    return _checkInButton;
}

#pragma mark  -------- UICollectionViewDelegate,UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.model.days.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    QJCheckInCalendarCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"QJCheckInCalendarCollectionCell" forIndexPath:indexPath];
    QJSignedItemDetailModel *model = [self.model.days safeObjectAtIndex:indexPath.item];
    model.today = self.model.today;
    if (indexPath.item == 0) {
        model.chooseIndexType = @"first";
    }else if (indexPath.item == self.model.days.count-1) {
        model.chooseIndexType = @"last";
    }
    cell.model = model;
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger num = 7;
    CGFloat width = CGRectGetWidth(collectionView.bounds)/num;
    CGFloat height = 95.0;
    if(indexPath.row == 0){
        //第一列的width比其他列稍大一些，消除item之间的间隙
        CGFloat realWidth = CGRectGetWidth(collectionView.bounds) - floor(width) * (num - 1);
        return CGSizeMake(realWidth, height);
    }else{
        return CGSizeMake(floor(width), height);
    }
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsZero;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 0;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 0;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    QJSignedItemDetailModel *model = [self.model.days safeObjectAtIndex:indexPath.item];
    if (model.haveSigned == YES) {
        return;
    }
//    [self resetButton:model];
    DLog(@"没有签到没有签到");
}

/**
 * @author: zjr
 * @date: 2022-6-27
 * @desc:  button点击事件
 */
- (void)scoreBtnClick{
    DLog(@"积分管理");
    if (self.btnClick) {
        self.btnClick(@"list");
    }
}

- (void)scoreShopBtnClick{
    DLog(@"积分商城");
    if (self.btnClick) {
        self.btnClick(@"shop");
    }
}

- (void)calanderBtnClick{
    DLog(@"跳转日历页面");
    if (self.btnClick) {
        self.btnClick(@"calendar");
    }
}

- (void)checkInBtnClick{
    DLog(@"打卡签到");
    if (self.btnClick) {
        self.btnClick(@"checkIn");
    }
}

/**
 * @author: zjr
 * @date: 2022-7-13
 * @desc: 数据源赋值
 */
- (void)updateQicoin:(QJSignedModel *)model{
    if (IsStrEmpty(_model.qicoin)) {
        _scoreLabel.text = @"0";
    }else{
        [_scoreLabel countFrom:0 to:_model.qicoin.floatValue withDuration:1];
    }
}

- (void)updateQJiconinAnimation:(CGFloat)from andTo:(CGFloat)to{
    [_scoreLabel countFrom:from to:to withDuration:1];
}

- (void)resetButton:(QJSignedItemDetailModel *)model{
    if (model.haveSigned == YES) {
        [_checkInButton setTitle:@"今日已签到" forState:UIControlStateNormal];
        _checkInButton.userInteractionEnabled = NO;
        [_checkInButton graduateLeftColor:UIColorFromRGB(0xFAD5CA) ToColor:UIColorFromRGB(0xFAD5CA)];
    } else {
        [_checkInButton setTitle:@"签到" forState:UIControlStateNormal];
        _checkInButton.userInteractionEnabled = YES;
        [_checkInButton graduateLeftColor:UIColorFromRGB(0xE89051) ToColor:UIColorFromRGB(0xE05951) startPoint:CGPointMake(0.5, 0) endPoint:CGPointMake(0.5, 1)];
    }
}

- (void)setModel:(QJSignedModel *)model{
    _model = model;
    [_checkInButton layoutIfNeeded];
    
    [_monthButton setTitle:_model.month forState:UIControlStateNormal];
    
    if (_model.todaySigned) {
        [_checkInButton setTitle:@"今日已签到" forState:UIControlStateNormal];
        _checkInButton.userInteractionEnabled = NO;
        [_checkInButton graduateLeftColor:UIColorFromRGB(0xFAD5CA) ToColor:UIColorFromRGB(0xFAD5CA)];
    }else{
        [_checkInButton setTitle:@"签到" forState:UIControlStateNormal];
        _checkInButton.userInteractionEnabled = YES;
        [_checkInButton graduateLeftColor:UIColorFromRGB(0xE89051) ToColor:UIColorFromRGB(0xE05951) startPoint:CGPointMake(0.5, 0) endPoint:CGPointMake(0.5, 1)];
    }
    
    if (!IsStrEmpty(_model.signedDays)) {
        NSString *signedDaysStr = [NSString stringWithFormat:@"已连续签到%@天",_model.signedDays];
        [_checkInTitleLabel setSingleStr:signedDaysStr range:[signedDaysStr rangeOfString:[NSString stringWithFormat:@"%@天",_model.signedDays]] font:kboldFont(16) color:UIColorFromRGB(0xE76742) numberOfLines:1];
    }

    [_calendarCollectionView reloadData];
}

//补签
- (void)repair:(NSString *)dateString{
    NSMutableDictionary *muDic = [NSMutableDictionary dictionary];
    [muDic setValue:dateString forKey:@"signDate"];
    QJCheckInPostRequest *checkinReq = [[QJCheckInPostRequest alloc] init];
    checkinReq.dic = muDic.copy;
    [checkinReq getRepairCheckInRequest];
    WS(weakSelf)
    checkinReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
//            [weakSelf initRequest];
        }else{
            NSString *msg = EncodeStringFromDic(request.responseObject, @"message");
            [weakSelf makeToast:msg];
        }
    };
}

@end
