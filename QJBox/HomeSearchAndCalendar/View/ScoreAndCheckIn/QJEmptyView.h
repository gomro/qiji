//
//  QJEmptyView.h
//  QJBox
//
//  Created by rui on 2022/6/28.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJEmptyView : UIView

/**
 * @author: zjr
 * @date: 2022-6-28
 * @desc:message-显示文案，image-显示图片
 */
- (instancetype)initWithMessage:(NSString *)message image:(UIImage *)image;

@end

NS_ASSUME_NONNULL_END
