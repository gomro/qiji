//
//  QJCalendarRepairView.h
//  QJBox
//
//  Created by rui on 2022/7/18.
//

#import <UIKit/UIKit.h>
/* model */
#import "QJSignedModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJCalendarRepairView : UIView

/* 数据源 */
@property (nonatomic, strong) QJSignedModel *model;

@property (nonatomic, copy) void (^btnClick)(QJSignedModel *model);

@end

NS_ASSUME_NONNULL_END
