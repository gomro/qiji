//
//  QJEmptyView.m
//  QJBox
//
//  Created by rui on 2022/6/28.
//

#import "QJEmptyView.h"

@implementation QJEmptyView

- (instancetype)initWithMessage:(NSString *)message image:(UIImage *)image {
    if (self = [super init]) {
        self.frame = CGRectMake(0, 0, QJScreenWidth, QJScreenHeight-NavigationBar_Bottom_Y);
        [self creatUIWithMessage:message image:image];
        self.backgroundColor = UIColorFromRGB(0Xffffff);
    }
    return self;
}

- (void)creatUIWithMessage:(NSString *)message image:(UIImage *)image {
    UIImageView *alertView = [[UIImageView alloc] init];
    [self addSubview:alertView];
    [alertView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self);
//        make.top.mas_equalTo(80);
        make.centerY.equalTo(self).offset(-40);
       
    }];
    if (image) {
        alertView.image = image;
    } else {
        alertView.image = [UIImage imageNamed:@"empty_no_data"];
    }
    UILabel *messageLabel = [[UILabel alloc] init];
    messageLabel.textAlignment = NSTextAlignmentCenter;
    messageLabel.font = kFont(12);
    messageLabel.numberOfLines = 0;
    [self addSubview:messageLabel];
    [messageLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.top.equalTo(alertView.mas_bottom).offset(40);
    }];
    messageLabel.textColor = [UIColor colorWithHexString:@"0x333333"];
    messageLabel.text = message;
}

@end
