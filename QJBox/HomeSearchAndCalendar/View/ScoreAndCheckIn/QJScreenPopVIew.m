//
//  QJScreenPopVIew.m
//  QJBox
//
//  Created by rui on 2022/6/28.
//

#import "QJScreenPopVIew.h"

@interface QJScreenPopVIew ()

/* 展示视图 */
@property (nonatomic, strong) UIView *centerView;
/* 背景底层button */
@property (nonatomic, strong) UIButton *bgButton;

@end

@implementation QJScreenPopVIew

- (instancetype)initWithCenterView:(UIView *)view  {
    if (self = [super init]) {
        self.bgButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.bgButton.frame = self.frame;
        [self.bgButton addTarget:self action:@selector(tapClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.bgButton];
        self.centerView = view;
    }
    return self;
}

/* button点击事件 */
- (void)tapClick:(UIButton *)sender {
    if (_dismissWhenClicked) {
        [self dismissWithBlock:^{
            if (self.tapDismissBlock) {
                self.tapDismissBlock();
            }
        }];
    }
}

/* 视图显示 */
- (void)showInView:(UIView *)aView {
    self.frame = aView.bounds;
    self.bgButton.frame = self.frame;
    [self addSubview:self.centerView];
    [aView addSubview:self];
    if (_popViewPopType != QJScreenPopViewPopTypeDefault) {
        self.centerView.top = aView.height;
        self.centerView.centerX = aView.centerX;
        [UIView animateWithDuration:0.25 animations:^{
            self.centerView.center = CGPointMake(aView.center.x, (aView.center.y - aView.frame.origin.y + self.centerViewOffsetY));
        } completion:^(BOOL finished) {
            self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
        }];
    } else {
        self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
        self.centerView.center = CGPointMake(aView.center.x, (aView.center.y - aView.frame.origin.y + self.centerViewOffsetY));
        self.centerView.transform = CGAffineTransformMakeScale(0, 0);
        [UIView animateWithDuration:0.25 animations:^{
            self.centerView.transform = CGAffineTransformIdentity;
        }];
    }
}

#pragma mark - Setter
- (void)setCenterViewOffsetY:(CGFloat)centerViewOffsetY {
    if (_centerViewOffsetY != centerViewOffsetY) {
        _centerViewOffsetY = centerViewOffsetY;
    }
}

- (void)setPopViewPopType:(QJScreenPopViewPopType)popViewPopType {
    _popViewPopType = popViewPopType;
}

- (void)setDismissWhenClicked:(BOOL)dismissWhenClicked {
    _dismissWhenClicked = dismissWhenClicked;
}

/* 隐藏页面回调函数 */
- (void)dismissWithBlock:(void (^)(void))block {
    if (_popViewPopType != QJScreenPopViewPopTypeDefault) {
        [UIView animateWithDuration:0.25 animations:^{
            self.centerView.top = self.height;
        } completion:^(BOOL finished) {
            [self.centerView removeFromSuperview];
            self.centerView = nil;
            [self removeFromSuperview];
            if (block) {
                block();
            }
        }];
    } else {
        [UIView animateWithDuration:0.25 animations:^{
            self.centerView.transform = CGAffineTransformMakeScale(0.0001,0.0001);
            self.backgroundColor = [UIColor clearColor];
        } completion:^(BOOL finished) {
            [self.centerView removeFromSuperview];
            self.centerView = nil;
            [self removeFromSuperview];
            if (block) {
                block();
            }
        }];
    }
}

@end
