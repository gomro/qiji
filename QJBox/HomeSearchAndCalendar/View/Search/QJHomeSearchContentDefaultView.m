//
//  QJHomeSearchContentDefaultView.m
//  QJBox
//
//  Created by rui on 2022/7/6.
//

#import "QJHomeSearchContentDefaultView.h"
/* 自定义layout */
#import "QJSearchFilterCustomLayout.h"
/* 自定义collectioncell */
#import "QJSearchFilterCollectionCell.h"
/* 自定义headerview */
#import "QJSearchFilterHeaderView.h"
/* 历史搜索记录 */
#import "QJHomeSearchHistoryHeaderView.h"

@interface QJHomeSearchContentDefaultView ()<UICollectionViewDelegate,UICollectionViewDataSource>

@property (nonatomic,strong) UICollectionView *collectionView;
/* 数据源 */
@property (nonatomic,strong) QJSearchSortModel *filterModel;
@property (nonatomic,strong) QJHomeSearchHistoryHeaderView *headerView;

@end

@implementation QJHomeSearchContentDefaultView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self initChildView];
    }
    return self;
}

/* 初始化view */
- (void)initChildView {
    self.backgroundColor = [UIColor whiteColor];

    QJSearchFilterCustomLayout *layout = [QJSearchFilterCustomLayout new];
    layout.alignment = FlowAlignmentLeft;
    layout.estimatedItemSize = CGSizeMake(80, 30);
    layout.sectionInset = UIEdgeInsetsMake(0, 15, 0, 15);
    layout.headerReferenceSize = CGSizeMake(self.width-30, 40);
    self.collectionView = [[UICollectionView alloc] initWithFrame:self.bounds collectionViewLayout:layout];
    _collectionView.backgroundColor = [UIColor clearColor];
    _collectionView.dataSource = self;
    _collectionView.delegate = self;
    _collectionView.showsVerticalScrollIndicator = NO;
    [_collectionView registerClass:[QJSearchFilterCollectionCell class] forCellWithReuseIdentifier:@"QJSearchFilterCollectionCell"];
    [_collectionView registerClass:[QJSearchFilterHeaderView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"QJSearchFilterHeaderView"];
    [self addSubview:self.collectionView];
    
    self.headerView = [[QJHomeSearchHistoryHeaderView alloc] initWithFrame:CGRectMake(0, 0, QJScreenWidth, 200)];
    [self.collectionView addSubview:self.headerView];
    self.headerView.hidden = YES;
    WS(weakSelf)
    self.headerView.heightBlock = ^(CGFloat height) {
        weakSelf.headerView.hidden = NO;
        weakSelf.headerView.height = height;
        [weakSelf updateHeader];
    };
    self.headerView.cellSelectBlock = ^(id  _Nonnull item) {
        if (weakSelf.cellSelectBlock) {
            weakSelf.cellSelectBlock(item);
        }
    };
    self.headerView.deleteBlock = ^{
        weakSelf.headerView.height = 0;
        weakSelf.headerView.hidden = YES;
        [weakSelf updateHeader];
    };
}

/* 赋值 */
- (void)showFilterWithModel:(QJSearchSortModel *)filterModel{
    self.filterModel = [QJSearchSortModel new];
    self.filterModel = filterModel;
    [self.collectionView reloadData];
}

- (void)showFilterHistoryWithModel:(QJSearchSortModel *)filterModel{
    if (filterModel.types.count > 0) {
        [self.headerView showFilterWithModel:filterModel];
    }else{
        self.headerView.height = 0;
        self.headerView.hidden = YES;
        [self updateHeader];
    }
}

#pragma mark - collectionViewDelegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return self.filterModel.types.count;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    QJSearchTagDetailModel *item = [self.filterModel.types safeObjectAtIndex:section];
    QJEmptyCommonView *emptyView = [QJEmptyCommonView new];
    emptyView.topSpace = 100;
    [collectionView collectionViewDisplayWhenHaveNoDataWithView:emptyView ifNecessaryForRowCount:item.list.count];
    return item.list.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    QJSearchFilterCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"QJSearchFilterCollectionCell" forIndexPath:indexPath];
    QJSearchTagDetailModel *item = [self.filterModel.types safeObjectAtIndex:indexPath.section];
    QJSearchTagDetailModel *model = [item.list safeObjectAtIndex:indexPath.row];
    cell.model = model;
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    QJSearchTagDetailModel *item = [self.filterModel.types safeObjectAtIndex:indexPath.section];
    QJSearchTagDetailModel *model = [item.list safeObjectAtIndex:indexPath.row];

    CGFloat width = [model.name getWidthWithFont:kFont(13) withHeight:26]+30;
    if (width > self.width) {
        width = self.width;
    }else{
        width = [model.name getWidthWithFont:kFont(13) withHeight:26] + 30;
    }
    
    return CGSizeMake(width, 32);
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    UICollectionReusableView *reusableView = nil;
    // 区头
    if (kind == UICollectionElementKindSectionHeader) {
        QJSearchFilterHeaderView *headerView = (QJSearchFilterHeaderView *)[collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"QJSearchFilterHeaderView" forIndexPath:indexPath];
        QJSearchTagDetailModel *item = [self.filterModel.types safeObjectAtIndex:indexPath.section];
        item.typeString = @"0";
        item.section = indexPath.section;
        headerView.model = item;
        reusableView = headerView;
    }
    return reusableView;
}

// 设置区头尺寸高度
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    CGSize size = CGSizeMake(_collectionView.width, 40);
    return size;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    QJSearchTagDetailModel *item = [self.filterModel.types safeObjectAtIndex:indexPath.section];
    QJSearchTagDetailModel *model = [item.list safeObjectAtIndex:indexPath.row];
    if (self.cellSelectBlock) {
        self.cellSelectBlock(model);
    }
}

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    if (self.scrollViewWillBeginDraggingBlock) {
        self.scrollViewWillBeginDraggingBlock();
    }
}

/* 更新历史记录view高度 */
- (void)updateHeader {
    CGFloat headerHeight = self.headerView.height;
    _headerView.frame = CGRectMake(0, -headerHeight, QJScreenWidth, headerHeight);
    //设置滚动范围偏移200
    _collectionView.scrollIndicatorInsets = UIEdgeInsetsMake(headerHeight, 0, 0, 0);
    //设置内容范围偏移200
    _collectionView.contentInset = UIEdgeInsetsMake(headerHeight, 0, 0, 0);
    [self.collectionView reloadData];
}


@end
