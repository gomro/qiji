//
//  QJSearchSortView.m
//  QJBox
//
//  Created by rui on 2022/7/5.
//

#import "QJSearchSortView.h"

/**
 * @author: zjr
 * @date: 2022-7-7
 * @desc: 自定义cell
 */
@interface QJSearchSortCell : UITableViewCell

/* 标题 */
@property (nonatomic,strong) UILabel *titleLab;
/* 数据源 */
@property (nonatomic,strong) QJSearchTagDetailModel *model;
/* 分割线 */
@property (nonatomic,strong) UIView *line;

@end

@implementation QJSearchSortCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]){
        [self initChildView];
    }
    return self;
}

/* 初始化 */
- (void)initChildView {
    self.contentView.backgroundColor = [UIColor whiteColor];
    self.selectionStyle = UITableViewCellSelectionStyleNone;

    self.titleLab = [[UILabel alloc] initWithFrame:self.bounds];
    [self.contentView addSubview:self.titleLab];
    _titleLab.font = kFont(14);
    _titleLab.textColor = UIColorFromRGB(0x000000);
    _titleLab.textAlignment = NSTextAlignmentLeft;
    [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.contentView).insets(UIEdgeInsetsMake(0, 25, 0, 5));
    }];
    
    self.line = [[UIView alloc] init];
    [self.contentView addSubview:self.line];
    _line.backgroundColor = UIColorFromRGB(0xe5e5e5);
    [_line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.contentView).offset(16);
        make.right.mas_equalTo(self.contentView).offset(-16);
        make.bottom.equalTo(self.contentView);
        make.height.mas_equalTo(0.5);
    }];
}

/* 赋值 */
-(void)setModel:(QJSearchTagDetailModel *)model{
    _model = model;
    _titleLab.text = _model.name;
    if (_model.isSelect) {
        _titleLab.textColor = UIColorFromRGB(0xFF9500);
    }else{
        _titleLab.textColor = UIColorFromRGB(0x000000);
    }
}

@end

@interface QJSearchSortView()<UITableViewDelegate,UITableViewDataSource>

/* 底部背景button */
@property (nonatomic, strong) UIButton *bgView;
/* 数据table */
@property (nonatomic,strong) UITableView *tableView;
/* 白色箭头图片 */
@property (nonatomic,strong) UIImageView *bgImgView;

@end

@implementation QJSearchSortView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self initChildView];
    }
    return self;
}

/* 初始化 */
- (void)initChildView {
    self.bgView = [UIButton buttonWithType:UIButtonTypeCustom];
    _bgView.frame =  self.bounds;
    _bgView.backgroundColor = [UIColor colorWithWhite:0.f alpha:0.7];
    [_bgView addTarget:self action:@selector(removeView) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.bgView];
    
    /* 图片 */
//    self.bgImgView = [[UIImageView alloc] initWithFrame:self.bounds];
//    [self addSubview:self.bgImgView];
//    _bgImgView.contentMode = UIViewContentModeScaleAspectFill;
    [self.bgView addSubview:self.tableView];
    [self.tableView layoutIfNeeded];
    [self.tableView showCorner:8 rectCorner:UIRectCornerBottomLeft | UIRectCornerBottomRight];
}

#pragma mark - Lazy Loading
-(UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:self.bounds style:UITableViewStylePlain];
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = [UIColor whiteColor];
        [_tableView registerClass:[QJSearchSortCell class] forCellReuseIdentifier:@"QJSearchSortCell"];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone; //设置分割线的分割
//        _tableView.separatorColor = UIColorFromRGB(0xF2F2F2); //设置分割线的颜色
//        [_tableView setSeparatorInset:UIEdgeInsetsMake(0, 15, 0, 0)]; //设置分割线的边Insets

        if (@available(iOS 11.0, *)) {
            _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
            _tableView.estimatedRowHeight = 0;
            _tableView.estimatedSectionFooterHeight = 0;
            _tableView.estimatedSectionHeaderHeight = 0;
        }
    }
    return _tableView;
}

#pragma mark - tableviewdelegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _sortTags.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50*kWScale;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    QJSearchSortCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QJSearchSortCell" forIndexPath:indexPath];
    cell.model = [_sortTags safeObjectAtIndex:indexPath.row];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self removeView];
    if (self.itemSelectBlock) {
        self.itemSelectBlock([_sortTags safeObjectAtIndex:indexPath.row]);
    }
}

/* 数据源赋值 */
- (void)setSortTags:(NSMutableArray *)sortTags {
    _sortTags = sortTags;
    if (!_sortTags) {
        return;
    }
    if (_sortTags.count > 0) {
        _tableView.height = _sortTags.count*(50*kWScale);
        [_tableView reloadData];
        
//        _bgImgView.height = _sortTags.count*40 + 10 + 8;
//        _bgImgView.image = [[UIImage imageNamed:@"icon_searchgame"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 0, 8, 0) resizingMode:UIImageResizingModeStretch];;
//        self.height = _tableView.height;
        [self.tableView layoutIfNeeded];
        [self.tableView showCorner:8 rectCorner:UIRectCornerBottomLeft | UIRectCornerBottomRight];
    }
}

/* 移除view */
- (void)removeView{
    self.hidden = YES;
}

@end
