//
//  QJHomeSearchContentView.h
//  QJBox
//
//  Created by rui on 2022/7/6.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJHomeSearchContentView : UIView

/* 点击回调 */
@property (nonatomic, copy) void (^cellSelectBlock)(id item);
/* 滑动事件回调 */
@property (nonatomic, copy) void (^scrollViewWillBeginDraggingBlock)(void);
/* 赋值---搜索的key */
- (void)searchWithKey:(NSString *)key;

@end

NS_ASSUME_NONNULL_END
