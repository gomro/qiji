//
//  QJHomeSearchHistoryHeaderView.h
//  QJBox
//
//  Created by rui on 2022/7/7.
//

#import <UIKit/UIKit.h>

#import "QJSearchSortModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJHomeSearchHistoryHeaderView : UIView

/* 点击回调 */
@property (nonatomic,copy) void (^cellSelectBlock)(id item);
/* 删除回调 */
@property (nonatomic,copy) void (^deleteBlock)(void);
/* view高度计算完成回调 */
@property (nonatomic,copy) void (^heightBlock)(CGFloat height);
/* 赋值 */
- (void)showFilterWithModel:(QJSearchSortModel *)filterModel;

@end

NS_ASSUME_NONNULL_END
