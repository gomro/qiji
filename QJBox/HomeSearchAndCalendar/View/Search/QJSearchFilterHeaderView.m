//
//  QJSearchFilterHeaderView.m
//  QJBox
//
//  Created by rui on 2022/7/6.
//

#import "QJSearchFilterHeaderView.h"

@interface QJSearchFilterHeaderView ()

@property (nonatomic, strong) UIView *lineView;
/* 标题 */
@property (nonatomic, strong) UILabel *titleLab;
//icon
@property (nonatomic, strong) UIImageView *iconImageView;

@end

@implementation QJSearchFilterHeaderView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self initChildView];
    }
    return self;
}

- (void)initChildView {
    self.backgroundColor = [UIColor whiteColor];
    
    _iconImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"search_hottag"]];
    [self addSubview:_iconImageView];
    _iconImageView.frame = CGRectMake(15, 12, 16, 16);
    _iconImageView.hidden = YES;
    
    self.titleLab = [[UILabel alloc] initWithFrame:CGRectMake(15, 20, self.width - 15*2, 30)];
    [self addSubview:self.titleLab];
    self.titleLab.backgroundColor = [UIColor whiteColor];
    _titleLab.font = kFont(12);
    _titleLab.textColor = UIColorFromRGB(0x000000);
    
    self.lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, QJScreenWidth, 10)];
    self.lineView.backgroundColor = UIColorFromRGB(0xF7F7F7);
    [self addSubview:self.lineView];
}

- (void)setModel:(QJSearchTagDetailModel *)model{
    _model = model;
    self.titleLab.text = _model.name;
    if ([_model.typeString isEqualToString:@"1"]) {
        _titleLab.font = kFont(12);
        _iconImageView.hidden = YES;
        if (_model.section == 0) {
            self.lineView.hidden = YES;
            self.titleLab.frame = CGRectMake(15, 0, self.width - 15*2, 30);
        }else{
            self.lineView.hidden = NO;
            self.titleLab.frame = CGRectMake(15, 20, self.width - 15*2, 30);
        }
    }else{
        _iconImageView.hidden = NO;
        self.lineView.hidden = YES;
        _titleLab.font = kFont(14);
        self.titleLab.frame = CGRectMake(40, 0, self.width - 50, 40);
    }
}

@end

