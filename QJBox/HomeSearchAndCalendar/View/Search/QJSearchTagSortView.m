//
//  QJSearchTagSortView.m
//  QJBox
//
//  Created by rui on 2022/7/5.
//

#import "QJSearchTagSortView.h"

/* 自定义layout */
#import "QJSearchFilterCustomLayout.h"
/* 自定义collectioncell */
#import "QJSearchFilterCollectionCell.h"
/* 自定义header */
#import "QJSearchFilterHeaderView.h"

@interface QJSearchTagSortView()<UICollectionViewDelegate,UICollectionViewDataSource>

/* 底部背景button */
@property (nonatomic, strong) UIButton *bgView;
/* 白色内容背景view */
@property (nonatomic, strong) UIView *whiteView;
/* 标题 */
@property (nonatomic, strong) UILabel *titleLabel;
/* 标签collection */
@property (nonatomic, strong) UICollectionView *collectionView;
/* 底部按钮背景view */
@property (nonatomic, strong) UIView *reSetOrDoneView;
/* 重置按钮 */
@property (nonatomic, strong) UIButton *resetBtn;
/* 完成按钮 */
@property (nonatomic, strong) UIButton *doneBtn;
/* 数据源 */
@property (nonatomic, strong) QJSearchSortModel *filterModel;

@end

@implementation QJSearchTagSortView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self initChildView];
    }
    return self;
}

/* 初始化view */
- (void)initChildView {
    self.bgView = [UIButton buttonWithType:UIButtonTypeCustom];
    _bgView.frame =  self.bounds;
    _bgView.backgroundColor = [UIColor colorWithWhite:0.f alpha:0.7];
    [_bgView addTarget:self action:@selector(hideBgView) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.bgView];
    
    self.whiteView = [[UIView alloc] initWithFrame:CGRectMake(84, 0, _bgView.width - 84, _bgView.height)];
    [_bgView addSubview:self.whiteView];
    _whiteView.backgroundColor = [UIColor whiteColor];
    
    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 64, _bgView.width - 84, 64)];
    self.titleLabel.backgroundColor = [UIColor whiteColor];
    self.titleLabel.textColor = UIColorFromRGB(0x000000);
    self.titleLabel.font = kFont(16);
    self.titleLabel.text = @"全部筛选";
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    [_whiteView addSubview:self.titleLabel];
    
    QJSearchFilterCustomLayout *layout = [QJSearchFilterCustomLayout new];
    layout.alignment = FlowAlignmentLeft;
    layout.minimumLineSpacing = 12;
    layout.minimumInteritemSpacing = 10;
    layout.estimatedItemSize = CGSizeMake(80*kWScale, 30*kWScale);
    layout.sectionInset = UIEdgeInsetsMake(15, 15, 15, 15);
    layout.headerReferenceSize = CGSizeMake(_whiteView.width-30, 50);
    layout.footerReferenceSize = CGSizeMake(_whiteView.width-30, 10);
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 64 + 64, _whiteView.width, _whiteView.height - 84 - 64 - 64) collectionViewLayout:layout];
    [_whiteView addSubview:self.collectionView];
    _collectionView.backgroundColor = [UIColor whiteColor];
    _collectionView.dataSource = self;
    _collectionView.delegate = self;
    _collectionView.showsVerticalScrollIndicator = NO;
    [_collectionView registerClass:[QJSearchFilterCollectionCell class] forCellWithReuseIdentifier:@"QJSearchFilterCollectionCell"];
    [_collectionView registerClass:[QJSearchFilterHeaderView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"QJSearchFilterHeaderView"];
    [_collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"QJSearchFilterFooterView"];
    if (@available(iOS 11.0, *)) {
        _collectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }

    self.reSetOrDoneView = [[UIView alloc] initWithFrame:CGRectMake(0, _whiteView.height - 84, _whiteView.width, 84)];
    [_whiteView addSubview:self.reSetOrDoneView];
    _reSetOrDoneView.backgroundColor = [UIColor whiteColor];
    
//    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _reSetOrDoneView.width, 1)];
//    [_reSetOrDoneView addSubview:line];
//    line.backgroundColor = UIColorFromRGB(0xeeeeee);
    
    UIButton *resetBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_reSetOrDoneView addSubview:resetBtn];
    [resetBtn setTitle:@"重置" forState:UIControlStateNormal];
    resetBtn.titleLabel.font = kFont(14);
    [resetBtn setTitleColor:UIColorFromRGB(0x101010) forState:UIControlStateNormal];
    [resetBtn addTarget:self action:@selector(resetClick) forControlEvents:UIControlEventTouchUpInside];
    resetBtn.frame = CGRectMake(18, 20, 88, 34);
    resetBtn.backgroundColor = UIColorFromRGB(0xFFFFFF);
    resetBtn.layer.borderWidth = 1;
    resetBtn.layer.borderColor = [UIColor colorWithRed:0.57 green:0.585 blue:0.6 alpha:1].CGColor;
    resetBtn.layer.cornerRadius = 15;
    resetBtn.layer.masksToBounds = YES;
    self.resetBtn = resetBtn;
    
    UIButton *downBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_reSetOrDoneView addSubview:downBtn];
    [downBtn setTitle:@"确认" forState:UIControlStateNormal];
    downBtn.titleLabel.font = kFont(14);
    [downBtn setTitleColor:UIColorFromRGB(0xFFFFFF) forState:UIControlStateNormal];
    [downBtn addTarget:self action:@selector(downClick) forControlEvents:UIControlEventTouchUpInside];
    downBtn.frame = CGRectMake(resetBtn.right+12, 20, _reSetOrDoneView.width - resetBtn.right - 30, 34);
    downBtn.backgroundColor = UIColorFromRGB(0xFF9500);
    downBtn.layer.cornerRadius = 15;
    downBtn.layer.masksToBounds = YES;
    self.doneBtn = downBtn;
    
    self.resetBtn.backgroundColor = UIColorFromRGB(0xE6E6E6);
    self.resetBtn.layer.borderWidth = 0;
    self.resetBtn.layer.borderColor = [UIColor clearColor].CGColor;

    self.doneBtn.backgroundColor = UIColorFromRGB(0xE6E6E6);
    [self.doneBtn setTitleColor:UIColorFromRGB(0x101010) forState:UIControlStateNormal];
    
    _whiteView.left = QJScreenWidth;
    self.hidden = YES;
}

#pragma mark - collectionViewDelegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return self.filterModel.types.count;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    QJSearchTagDetailModel *item = [self.filterModel.types safeObjectAtIndex:section];
    return item.values.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    QJSearchFilterCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"QJSearchFilterCollectionCell" forIndexPath:indexPath];
    QJSearchTagDetailModel *item = [self.filterModel.types safeObjectAtIndex:indexPath.section];
    QJSearchTagDetailModel *model = [item.values safeObjectAtIndex:indexPath.row];
    cell.model = model;
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [self updateItemWithIndex:indexPath];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    QJSearchTagDetailModel *item = [self.filterModel.types safeObjectAtIndex:indexPath.section];
    QJSearchTagDetailModel *model = [item.values safeObjectAtIndex:indexPath.row];

    CGFloat width = [model.name getWidthWithFont:kFont(13) withHeight:26*kWScale]+30;
    if (width > (_bgView.width - 84)) {
        width = (_bgView.width - 84);
    }else{
        width = [model.name getWidthWithFont:kFont(13) withHeight:26*kWScale] + 30;
    }
    
    return CGSizeMake(width, 26*kWScale);
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{

    UICollectionReusableView *reusableView = nil;
    // 区头
    if (kind == UICollectionElementKindSectionHeader) {
        QJSearchFilterHeaderView *headerView = (QJSearchFilterHeaderView *)[collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"QJSearchFilterHeaderView" forIndexPath:indexPath];
        QJSearchTagDetailModel *item = [self.filterModel.types safeObjectAtIndex:indexPath.section];
        item.typeString = @"1";
        item.section = indexPath.section;
        headerView.model = item;
        reusableView = headerView;
    }else if (kind == UICollectionElementKindSectionFooter) {
        UICollectionReusableView *footerview = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"QJSearchFilterFooterView" forIndexPath:indexPath];
        footerview.backgroundColor = [UIColor whiteColor];
        reusableView = footerview;
    }
    return reusableView;
}

// 设置区头尺寸高度
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    CGSize size = CGSizeMake(_collectionView.width, 40);
    if (self.filterModel.types.count > 0) {
        if (section == 0) {
            size = CGSizeMake(_collectionView.width, 40);
        }else{
            size = CGSizeMake(_collectionView.width, 50);
        }
    }
    return size;
}

#pragma mark - 隐藏显示控制
- (void)hideBgView {
    [self resetClick];
    [self menuViewHide];
}

- (void)menuViewHide{
    [UIView animateWithDuration:0.5 animations:^{
        self->_whiteView.left = QJScreenWidth;
    } completion:^(BOOL finished) {
        self.hidden = YES;
    }];
}

- (void)menuViewShow{
    [UIView animateWithDuration:0.5 animations:^{
        self->_whiteView.left = 84;
        self.hidden = NO;
    }];
}

/**
 * @author: zjr
 * @date: 2022-7-6
 * @desc: button点击事件
 */
/* 重置 */
- (void)resetClick {
    [self.filterModel.types enumerateObjectsUsingBlock:^(QJSearchTagDetailModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [obj.values enumerateObjectsUsingBlock:^(QJSearchTagDetailModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            obj.isSelect = NO;
        }];
    }];
    [self.collectionView  reloadData];
}

/* 完成 */
- (void)downClick {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [self.filterModel.types enumerateObjectsUsingBlock:^(QJSearchTagDetailModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSMutableArray *arr = [NSMutableArray array];
        [obj.values enumerateObjectsUsingBlock:^(QJSearchTagDetailModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if (obj.isSelect) {
                self.filterModel.hasSelect = YES;
                [arr safeAddObject:obj.ID];
            }
        }];
        [dic setValue:arr forKey:obj.ID];
    }];
    
    [self menuViewHide];
    if (self.filterSelectBlock) {
        self.filterSelectBlock(dic, self.filterModel);
    }
}

/* 赋值 */
- (void)showFilterWithModel:(QJSearchSortModel *)filterModel{
    self.filterModel = [QJSearchSortModel new];
    self.filterModel = filterModel;
    [self.collectionView reloadData];
    [self menuViewShow];
}

- (void)showChooseFilterWithModel:(NSMutableDictionary *)chooseDic{
    [[chooseDic allKeys] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSString *ID = (NSString *)obj;
        [self.filterModel.types enumerateObjectsUsingBlock:^(QJSearchTagDetailModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            QJSearchTagDetailModel *model = (QJSearchTagDetailModel *)obj;
            if ([obj.ID isEqualToString:ID]) {
                NSMutableArray *arr = [chooseDic valueForKey:ID];
                [arr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    NSString *chooseID = (NSString *)obj;
                    [model.values enumerateObjectsUsingBlock:^(QJSearchTagDetailModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                        if ([obj.ID isEqualToString:chooseID]) {
                            obj.isSelect = YES;
                        }
                    }];
                }];
            }
        }];
    }];
    __block BOOL isChoose = NO;
    [[chooseDic allValues] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSMutableArray *temp = (NSMutableArray *)obj;
        if (temp.count > 0) {
            isChoose = YES;
        }
    }];
    if (isChoose) {
        self.resetBtn.backgroundColor = UIColorFromRGB(0xFFFFFF);
        self.resetBtn.layer.borderWidth = 1;
        self.resetBtn.layer.borderColor = [UIColor colorWithRed:0.57 green:0.585 blue:0.6 alpha:1].CGColor;

        self.doneBtn.backgroundColor = UIColorFromRGB(0xFF9500);
        [self.doneBtn setTitleColor:UIColorFromRGB(0xFFFFFF) forState:UIControlStateNormal];
    }else{
        self.resetBtn.backgroundColor = UIColorFromRGB(0xE6E6E6);
        self.resetBtn.layer.borderWidth = 0;
        self.resetBtn.layer.borderColor = [UIColor clearColor].CGColor;

        self.doneBtn.backgroundColor = UIColorFromRGB(0xE6E6E6);
        [self.doneBtn setTitleColor:UIColorFromRGB(0x101010) forState:UIControlStateNormal];
    }
    [self.collectionView reloadData];
}

/**
 * @author: zjr
 * @date: 2022-7-6
 * @desc: 更新选中状态
 */
- (void)updateItemWithIndex:(NSIndexPath *)indexPath {
    QJSearchTagDetailModel *item = [self.filterModel.types safeObjectAtIndex:indexPath.section];
    if (item.multiple) {
        QJSearchTagDetailModel *model = [item.values safeObjectAtIndex:indexPath.row];
        model.isSelect = !model.isSelect;
    }else{
        [item.values enumerateObjectsUsingBlock:^(QJSearchTagDetailModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if (idx == indexPath.row) {
                obj.isSelect = !obj.isSelect;
            }else{
                obj.isSelect = NO;
            }
        }];
    }
    
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [self.filterModel.types enumerateObjectsUsingBlock:^(QJSearchTagDetailModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSMutableArray *arr = [NSMutableArray array];
        [obj.values enumerateObjectsUsingBlock:^(QJSearchTagDetailModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if (obj.isSelect) {
                self.filterModel.hasSelect = YES;
                [arr safeAddObject:obj.ID];
            }
        }];
        [dic setValue:arr forKey:obj.ID];
    }];
    __block BOOL isChoose = NO;
    [[dic allValues] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSMutableArray *temp = (NSMutableArray *)obj;
        if (temp.count > 0) {
            isChoose = YES;
        }
    }];
    if (isChoose) {
        self.resetBtn.backgroundColor = UIColorFromRGB(0xFFFFFF);
        self.resetBtn.layer.borderWidth = 1;
        self.resetBtn.layer.borderColor = [UIColor colorWithRed:0.57 green:0.585 blue:0.6 alpha:1].CGColor;

        self.doneBtn.backgroundColor = UIColorFromRGB(0xFF9500);
        [self.doneBtn setTitleColor:UIColorFromRGB(0xFFFFFF) forState:UIControlStateNormal];
    }else{
        self.resetBtn.backgroundColor = UIColorFromRGB(0xE6E6E6);
        self.resetBtn.layer.borderWidth = 0;
        self.resetBtn.layer.borderColor = [UIColor clearColor].CGColor;

        self.doneBtn.backgroundColor = UIColorFromRGB(0xE6E6E6);
        [self.doneBtn setTitleColor:UIColorFromRGB(0x101010) forState:UIControlStateNormal];
    }

    [self.collectionView  reloadData];
}

@end

