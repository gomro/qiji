//
//  QJSearchSortView.h
//  QJBox
//
//  Created by rui on 2022/7/5.
//

#import <UIKit/UIKit.h>

#import "QJSearchSortModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJSearchSortView : UIView

/**
 * @author: zjr
 * @date: 2022-7-7
 * @desc: 更多下载，综合排序-对应数据源
 * 数组内model为QJSearchTagDetailModel
 */
@property (nonatomic, strong) NSMutableArray *sortTags;

/**
 * @author: zjr
 * @date: 2022-7-7
 * @desc: 更多下载，综合排序-点击回调
 * 返回值为QJSearchTagDetailModel类型
 */
@property (nonatomic, copy) void (^itemSelectBlock)(QJSearchTagDetailModel *model);

/* 移除view */
- (void)removeView;

@end

NS_ASSUME_NONNULL_END
