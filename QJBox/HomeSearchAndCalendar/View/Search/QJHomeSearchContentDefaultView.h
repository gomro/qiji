//
//  QJHomeSearchContentDefaultView.h
//  QJBox
//
//  Created by rui on 2022/7/6.
//

#import <UIKit/UIKit.h>

#import "QJSearchSortModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJHomeSearchContentDefaultView : UIView

/* 点击回调 */
@property (nonatomic,copy) void (^cellSelectBlock)(id item);
/* 滑动事件回调 */
@property (nonatomic,copy) void (^scrollViewWillBeginDraggingBlock)(void);

/* 赋值 */
- (void)showFilterWithModel:(QJSearchSortModel *)filterModel;
/* 赋值---历史搜索记录 */
- (void)showFilterHistoryWithModel:(QJSearchSortModel *)filterModel;

@end

NS_ASSUME_NONNULL_END
