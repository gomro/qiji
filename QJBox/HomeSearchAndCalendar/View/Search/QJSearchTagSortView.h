//
//  QJSearchTagSortView.h
//  QJBox
//
//  Created by rui on 2022/7/5.
//

#import <UIKit/UIKit.h>

#import "QJSearchSortModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJSearchTagSortView : UIView

/* 选中完成后回调事件 */
@property (nonatomic, copy) void (^filterSelectBlock)(NSMutableDictionary *dic,QJSearchSortModel *filterModel);
/* 赋值 */
- (void)showFilterWithModel:(QJSearchSortModel *)filterModel;
- (void)showChooseFilterWithModel:(NSMutableDictionary *)chooseDic;

@end

NS_ASSUME_NONNULL_END
