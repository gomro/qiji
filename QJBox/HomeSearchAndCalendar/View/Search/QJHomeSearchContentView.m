//
//  QJHomeSearchContentView.m
//  QJBox
//
//  Created by rui on 2022/7/6.
//

#import "QJHomeSearchContentView.h"
/* 自定义cell */
#import "QJSearchContentTypeCell.h"
/* 请求 */
#import "QJSearchGameRequest.h"
/* model */
#import "QJSearchSortModel.h"

@interface QJHomeSearchContentView()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, copy) NSString *key;
/* 请求方法 */
@property (nonatomic, strong) QJSearchGameRequest *searchSuggestReq;
@property (nonatomic, strong) QJSearchSortModel        *searchModel;

@end

@implementation QJHomeSearchContentView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.searchModel = [QJSearchSortModel new];
        [self initChildView];
    }
    return self;
}

- (void)initChildView {
    self.backgroundColor = [UIColor whiteColor];
    
    [self addSubview:self.tableView];
}

#pragma mark - UITableViewdelegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    QJEmptyCommonView *emptyView = [QJEmptyCommonView new];
    emptyView.string = @"暂无搜索结果";
    emptyView.emptyImage = @"empty_no_search";
    emptyView.topSpace = 100;
    [tableView tableViewDisplayWhenHaveNoDataWithView:emptyView ifNecessaryForRowCount:self.searchModel.data.count];
    return self.searchModel.data.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    QJSearchTagDetailModel *item = [self.searchModel.data safeObjectAtIndex:indexPath.row];
    QJSearchContentTypeCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QJSearchContentTypeCell" forIndexPath:indexPath];
    cell.searchkey = self.key;
    cell.model = item.keyword;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    QJSearchTagDetailModel *item = [self.searchModel.data safeObjectAtIndex:indexPath.row];
    if (self.cellSelectBlock) {
        self.cellSelectBlock(item);
    }
}

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    if (self.scrollViewWillBeginDraggingBlock) {
        self.scrollViewWillBeginDraggingBlock();
    }
}

#pragma mark - 搜索key传值并请求
- (void)searchWithKey:(NSString *)key{
    self.key = key;
    [self sendRequest];
}

- (void)sendRequest{
    NSMutableDictionary *muDic = [NSMutableDictionary new];
    [muDic setValue:self.key forKey:@"keyword"];
    self.searchSuggestReq.dic = muDic.copy;
    [self.searchSuggestReq getSearchSuggestRequest];
    WS(weakSelf)
    self.searchSuggestReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        DLog(@"request = %@",request.responseJSONObject);
        if (ResponseSuccess) {
            if ([EncodeArrayFromDic(request.responseObject, @"data") isKindOfClass:[NSArray class]]) {
                weakSelf.searchModel.data = [NSArray modelArrayWithClass:[QJSearchTagDetailModel class] json:EncodeArrayFromDic(request.responseObject, @"data")].mutableCopy;
            }
        }else{
            NSString *msg = EncodeStringFromDic(request.responseObject, @"message");
            [weakSelf makeToast:msg];
        }
        [weakSelf.tableView reloadData];
    };
    self.searchSuggestReq.failureCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        [weakSelf makeToast:@"数据异常 请稍后再试"];
        [weakSelf.tableView reloadData];
    };
}

#pragma mark - lazy loading
-(UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.width , self.height) style:UITableViewStylePlain];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = [UIColor whiteColor];
        [_tableView registerClass:[QJSearchContentTypeCell class] forCellReuseIdentifier:@"QJSearchContentTypeCell"];
        if (@available(iOS 11.0, *)) {
            _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
            _tableView.estimatedRowHeight = 0;
            _tableView.estimatedSectionFooterHeight = 0;
            _tableView.estimatedSectionHeaderHeight = 0;
        }
    }
    return _tableView;
}

-(QJSearchGameRequest *)searchSuggestReq {
    if (!_searchSuggestReq) {
        _searchSuggestReq = [QJSearchGameRequest new];
    }
    return _searchSuggestReq;
}

@end
