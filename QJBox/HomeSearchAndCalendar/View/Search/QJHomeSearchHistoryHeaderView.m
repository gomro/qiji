//
//  QJHomeSearchHistoryHeaderView.m
//  QJBox
//
//  Created by rui on 2022/7/7.
//

#import "QJHomeSearchHistoryHeaderView.h"

/* 自定义layout */
#import "QJSearchFilterCustomLayout.h"
/* 自定义collectioncell */
#import "QJSearchFilterCollectionCell.h"

@interface QJHomeSearchHistoryHeaderView ()<UICollectionViewDelegate,UICollectionViewDataSource>
/* 历史记录 */
@property (nonatomic,strong) UICollectionView *collectionView;
/* 数据源 */
@property (nonatomic,strong) QJSearchSortModel *filterModel;
//icon
@property (nonatomic, strong) UIImageView *iconImageView;
//标题
@property (nonatomic, strong) UILabel *titleLabel;
//删除按钮
@property (nonatomic, strong) UIButton *deleteButton;

@end

@implementation QJHomeSearchHistoryHeaderView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self initChildView];
    }
    return self;
}

/* 初始化view */
- (void)initChildView {
    self.backgroundColor = [UIColor whiteColor];

    _iconImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"search_history"]];
    [self addSubview:_iconImageView];
    [self.iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(12);
        make.left.equalTo(self).offset(15);
        make.width.equalTo(@16);
        make.height.equalTo(@16);
    }];
    
    _titleLabel = [UILabel new];
    _titleLabel.font = kFont(14);
    _titleLabel.textColor = UIColorFromRGB(0x000000);
    _titleLabel.text = @"最近搜索";
    [self addSubview:_titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self);
        make.left.equalTo(self).offset(40);
        make.height.equalTo(@40);
    }];
    
    self.deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.deleteButton addTarget:self action:@selector(btnAction) forControlEvents:UIControlEventTouchUpInside];
    self.deleteButton.titleLabel.font = kFont(14);
    [self.deleteButton setImage:[UIImage imageNamed:@"search_delete"] forState:UIControlStateNormal];
    [self.deleteButton setTitle:@"清空" forState:UIControlStateNormal];
    [self.deleteButton setTitleColor:UIColorFromRGB(0x333333) forState:UIControlStateNormal];
    [self addSubview:self.deleteButton];
    [self.deleteButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self);
        make.right.equalTo(self).offset(-15);
        make.height.equalTo(@40);
//        make.width.equalTo(@40);
    }];
    [self.deleteButton layoutWithStatus:QJLayoutStatusNormal andMargin:5];
    
    QJSearchFilterCustomLayout *layout = [QJSearchFilterCustomLayout new];
    layout.alignment = FlowAlignmentLeft;
    layout.estimatedItemSize = CGSizeMake(80, 30);
    layout.sectionInset = UIEdgeInsetsMake(0, 15, 0, 15);
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 40, QJScreenWidth, 50) collectionViewLayout:layout];
    _collectionView.backgroundColor = [UIColor clearColor];
    _collectionView.dataSource = self;
    _collectionView.delegate = self;
    _collectionView.showsVerticalScrollIndicator = NO;
    [_collectionView registerClass:[QJSearchFilterCollectionCell class] forCellWithReuseIdentifier:@"QJSearchFilterCollectionCell"];
    [self addSubview:self.collectionView];
}

/* 赋值 */
- (void)showFilterWithModel:(QJSearchSortModel *)filterModel{
    self.filterModel = [QJSearchSortModel new];
    self.filterModel = filterModel;
    [self.collectionView reloadData];
    [self.collectionView layoutIfNeeded];
    self.collectionView.height = self.collectionView.contentSize.height;
    self.height = self.collectionView.height+50;
    if (self.heightBlock) {
        self.heightBlock(self.collectionView.height+50);
    }
}

#pragma mark - collectionViewDelegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return self.filterModel.types.count;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    QJSearchTagDetailModel *item = [self.filterModel.types safeObjectAtIndex:section];
    return item.list.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    QJSearchFilterCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"QJSearchFilterCollectionCell" forIndexPath:indexPath];
    QJSearchTagDetailModel *item = [self.filterModel.types safeObjectAtIndex:indexPath.section];
    QJSearchTagDetailModel *model = [item.list safeObjectAtIndex:indexPath.row];
    cell.model = model;
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    QJSearchTagDetailModel *item = [self.filterModel.types safeObjectAtIndex:indexPath.section];
    QJSearchTagDetailModel *model = [item.list safeObjectAtIndex:indexPath.row];

    CGFloat width = [model.name getWidthWithFont:kFont(13) withHeight:32]+30;
    if (width > self.width) {
        width = self.width;
    }else{
        width = [model.name getWidthWithFont:kFont(13) withHeight:32] + 30;
    }
    
    return CGSizeMake(width, 32);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    QJSearchTagDetailModel *item = [self.filterModel.types safeObjectAtIndex:indexPath.section];
    QJSearchTagDetailModel *model = [item.list safeObjectAtIndex:indexPath.row];
    if (self.cellSelectBlock) {
        self.cellSelectBlock(model);
    }
}

/**
 * @author: zjr
 * @date: 2022-7-7
 * @desc: 点击事件
 */
- (void)btnAction{
    if (self.filterModel.type == 1) {
        [[NSUserDefaults standardUserDefaults] setValue:[NSMutableArray array] forKey:@"newsHistorySearch"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    } else {
        [[NSUserDefaults standardUserDefaults] setValue:[NSMutableArray array] forKey:@"historySearch"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }

    if (self.deleteBlock) {
        self.deleteBlock();
    }
}

@end
