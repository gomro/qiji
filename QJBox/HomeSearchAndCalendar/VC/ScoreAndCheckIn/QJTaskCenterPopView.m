//
//  QJTaskCenterPopView.m
//  QJBox
//
//  Created by rui on 2022/8/16.
//

#import "QJTaskCenterPopView.h"
/* 倒计时 */
#import "QJCountDown.h"

@interface QJTaskCenterPopView ()

@property (nonatomic, strong) UIView *contentView;
@property (nonatomic, strong) UIView *lineView;
@property (nonatomic, strong) UIButton *closeButton;

@property (nonatomic, strong) UIView *whiteBgView;
/* icon */
@property (nonatomic, strong) UIImageView *iconImageView;
//任务标题
@property (nonatomic, strong) UILabel *titleLabel;
//任务名字
@property (nonatomic, strong) UILabel *nameLabel;
//任务描述
@property (nonatomic, strong) UILabel *subLabel;
//任务名额
@property (nonatomic, strong) UILabel *countLabel;
//任务完成按钮
@property (nonatomic, strong) UIButton *statusButton;
//任务区服
@property (nonatomic, strong) UILabel *areaTitleLabel;
@property (nonatomic, strong) UILabel *areaSubTitleLabel;
//任务介绍
@property (nonatomic, strong) UILabel *introTitleLabel;
@property (nonatomic, strong) UIStackView *introView;
//任务说明
@property (nonatomic, strong) UILabel *explainTitleLabel;
@property (nonatomic, strong) UIStackView *explainView;
//任务开始时间
@property (nonatomic, strong) UILabel *startTimeTitleLabel;
@property (nonatomic, strong) UILabel *startTimeSubTitleLabel;
//任务结束时间
@property (nonatomic, strong) UILabel *endTimeTitleLabel;
@property (nonatomic, strong) UILabel *endTimeSubTitleLabel;
//任务状态
@property (nonatomic, strong) UILabel *statusLabel;
@property (nonatomic, strong) QJCountDown *countDown;

@end

@implementation QJTaskCenterPopView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithWhite:0 alpha:0.4];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hidAnimation)];
        [self addGestureRecognizer:tap];
        
        /* 初始化UI */
        [self initUI];
        self.countDown = [QJCountDown new];

        [self creatContentView];
        [self makeConstraints];
        [self changeFrame];
    }
    return self;
}

- (void)initUI{
    self.contentView = [[UIView alloc] initWithFrame:CGRectMake(16, 0, QJScreenWidth-32, 462*kWScale)];
    self.contentView.centerY = self.height/2;
    [self addSubview:self.contentView];
    [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(16);
        make.right.equalTo(self).offset(-16);
        make.centerY.equalTo(self);
        make.height.equalTo(@(462*kWScale));
    }];
    
    UIImageView *lineImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"qj_taskcentericon_pop_linebg"]];
    lineImageView.frame = CGRectMake(0, 0, QJScreenWidth-32, 10);
    [self.contentView addSubview:lineImageView];
    [lineImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@(10*kWScale));
        make.width.equalTo(@(QJScreenWidth-32));
        make.top.equalTo(self.contentView);
        make.left.equalTo(self.contentView);
    }];
    
    UIImageView *bottomLineImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"qj_taskcentericon_pop_linebg"]];
    bottomLineImageView.frame = CGRectMake(0, 0, QJScreenWidth-32, 10);
    [self.contentView addSubview:bottomLineImageView];
    [bottomLineImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@(10*kWScale));
        make.width.equalTo(@(QJScreenWidth-32));
        make.bottom.equalTo(self.contentView);
        make.left.equalTo(self.contentView);
    }];
    
    UIImageView *bgImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"qj_taskcentericon_pop_bg"]];
    bgImageView.frame = CGRectMake(0, 0, QJScreenWidth-32, 10);
    [self.contentView addSubview:bgImageView];
    [bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(10);
        make.left.equalTo(self.contentView).offset(10);
        make.right.equalTo(self.contentView).offset(-10);
        make.bottom.equalTo(self.contentView).offset(-10);
    }];

    UIImageView *bgImageView1 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"qj_taskcentericon_pop_alphabg"]];
    bgImageView1.frame = CGRectMake(0, 0, QJScreenWidth-32, 10);
    [self.contentView addSubview:bgImageView1];
    [bgImageView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(10);
        make.left.equalTo(self.contentView).offset(10);
        make.right.equalTo(self.contentView).offset(-10);
        make.bottom.equalTo(self.contentView).offset(-10);
    }];
    
    UIImageView *bgImageView2 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"qj_taskcentericon_pop_borderbg"]];
    bgImageView2.frame = CGRectMake(0, 0, QJScreenWidth-32, 10);
    [self.contentView addSubview:bgImageView2];
    [bgImageView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(14);
        make.left.equalTo(self.contentView).offset(14);
        make.right.equalTo(self.contentView).offset(-14);
        make.bottom.equalTo(self.contentView).offset(-14);
    }];
    
    self.lineView = [[UIView alloc] initWithFrame:CGRectZero];
    self.lineView.backgroundColor = UIColorFromRGB(0xA6BBD6);
    [self.contentView addSubview:self.lineView];
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_bottom);
        make.centerX.equalTo(self.contentView);
        make.width.equalTo(@(2*kWScale));
        make.height.equalTo(@(50*kWScale));
    }];
    
    self.closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.closeButton addTarget:self action:@selector(hidAnimation) forControlEvents:UIControlEventTouchUpInside];
    self.closeButton.frame = CGRectMake(0, 0, 44*kWScale, 44*kWScale);
    self.closeButton.top = self.contentView.bottom + 30;
    self.closeButton.centerX = QJScreenWidth/2;
    [self.closeButton setImage:[UIImage imageNamed:@"qj_taskcentericon_pop_close"] forState:UIControlStateNormal];
//    self.closeButton.hidden = YES;
    [self addSubview:self.closeButton];
    [self.closeButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.lineView.mas_bottom).offset(-15);
        make.centerX.equalTo(self.contentView);
        make.width.equalTo(@(44*kWScale));
        make.height.equalTo(@(44*kWScale));
    }];
}

- (void)creatContentView{
    [self.contentView addSubview:self.whiteBgView];
    [self.whiteBgView addSubview:self.iconImageView];
    [self.whiteBgView addSubview:self.nameLabel];
    [self.whiteBgView addSubview:self.titleLabel];
    [self.whiteBgView addSubview:self.subLabel];
    [self.whiteBgView addSubview:self.countLabel];
    [self.whiteBgView addSubview:self.statusButton];
    
    [self.whiteBgView addSubview:self.areaTitleLabel];
    [self.whiteBgView addSubview:self.areaSubTitleLabel];

    [self.whiteBgView addSubview:self.introView];
    [self.whiteBgView addSubview:self.explainView];

    [self.whiteBgView addSubview:self.introTitleLabel];
    [self.whiteBgView addSubview:self.explainTitleLabel];
    [self.whiteBgView addSubview:self.startTimeTitleLabel];
    [self.whiteBgView addSubview:self.startTimeSubTitleLabel];
    [self.whiteBgView addSubview:self.endTimeTitleLabel];
    [self.whiteBgView addSubview:self.endTimeSubTitleLabel];
    [self.whiteBgView addSubview:self.statusLabel];
}

/**
 * @author: zjr
 * @date: 2022-7-1
 * @desc: 添加约束
 */
- (void)makeConstraints{
    [self.whiteBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(16);
        make.right.equalTo(self.contentView).offset(-16);
        make.top.equalTo(self.contentView).offset(10);
        make.bottom.equalTo(self.contentView).offset(-10);
    }];

    [self.iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.whiteBgView).offset(10);
        make.left.equalTo(self.whiteBgView).offset(16);
        make.height.equalTo(@(40*kWScale));
        make.width.equalTo(@(40*kWScale));
    }];
    
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.iconImageView.mas_bottom).offset(4);
        make.left.equalTo(self.whiteBgView).offset(16);
        make.height.equalTo(@(15*kWScale));
    }];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.whiteBgView).offset(10);
        make.left.equalTo(self.iconImageView.mas_right).offset(13);
        make.height.equalTo(@(20*kWScale));
    }];
    
    [self.subLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).offset(8);
        make.left.equalTo(self.iconImageView.mas_right).offset(13);
        make.height.equalTo(@(15*kWScale));
    }];
    
    [self.statusButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.whiteBgView).offset(15);
        make.width.equalTo(@(60*kWScale));
        make.height.equalTo(@(28*kWScale));
        make.right.equalTo(self.whiteBgView).offset(-16);
    }];
    
    [self.countLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.statusButton.mas_bottom).offset(12);
        make.right.equalTo(self.whiteBgView).offset(-16);
        make.height.equalTo(@(15*kWScale));
    }];
    
    [self.areaSubTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.nameLabel.mas_bottom).offset(20);
        make.left.equalTo(self.whiteBgView).offset(68);
        make.right.equalTo(self.whiteBgView).offset(-22);
    }];
    
    [self.areaTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.areaSubTitleLabel.mas_top);
        make.left.equalTo(self.whiteBgView).offset(12);
    }];
    
    [self.introView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.areaSubTitleLabel.mas_bottom).offset(10);
        make.left.equalTo(self.whiteBgView).offset(68);
        make.right.equalTo(self.whiteBgView).offset(-22);
    }];
    
    [self.introTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.introView.mas_top);
        make.left.equalTo(self.whiteBgView).offset(12);
    }];
    
    [self.explainView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.introView.mas_bottom).offset(10);
        make.left.equalTo(self.whiteBgView).offset(68);
        make.right.equalTo(self.whiteBgView).offset(-22);
    }];
    
    [self.explainTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.explainView.mas_top);
        make.left.equalTo(self.whiteBgView).offset(12);
    }];
    
    [self.startTimeSubTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.explainView.mas_bottom).offset(10);
        make.left.equalTo(self.whiteBgView).offset(68);
        make.right.equalTo(self.whiteBgView).offset(-22);
    }];
    
    [self.startTimeTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.startTimeSubTitleLabel.mas_top);
        make.left.equalTo(self.whiteBgView).offset(12);
    }];
    
    [self.endTimeSubTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.startTimeSubTitleLabel.mas_bottom).offset(10);
        make.left.equalTo(self.whiteBgView).offset(68);
        make.right.equalTo(self.whiteBgView).offset(-22);
    }];
    
    [self.endTimeTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.endTimeSubTitleLabel.mas_top);
        make.left.equalTo(self.whiteBgView).offset(12);
    }];
    
    [self.statusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.whiteBgView).offset(-22);
//        make.height.equalTo(@15);
//        #warning TODO 约束问题
//        make.top.equalTo(self.endTimeTitleLabel.mas_bottom).offset(80);
        make.bottom.equalTo(self.whiteBgView).offset(-22);
    }];
}

- (void)setAdjustingFrame:(CGRect)adjustingFrame{
    _adjustingFrame = adjustingFrame;
    self.contentView.frame = _adjustingFrame;
    [self.contentView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@(_adjustingFrame.size.height));
    }];
}


- (void)changeAlpha:(NSInteger)alpha{
//    self.contentView.alpha = alpha;
//    self.lineView.alpha = alpha;
//    self.closeButton.alpha = alpha;
//    self.whiteBgView.alpha = alpha;
    
    self.iconImageView.alpha = alpha;
    self.titleLabel.alpha = alpha;
    self.nameLabel.alpha = alpha;
    self.subLabel.alpha = alpha;
    self.countLabel.alpha = alpha;
    self.statusButton.alpha = alpha;
    self.areaTitleLabel.alpha = alpha;
    self.introView.alpha = alpha;
    self.explainView.alpha = alpha;
    self.areaSubTitleLabel.alpha = alpha;
    self.introTitleLabel.alpha = alpha;
    self.explainTitleLabel.alpha = alpha;
    self.startTimeTitleLabel.alpha = alpha;
    self.endTimeTitleLabel.alpha = alpha;
    self.startTimeSubTitleLabel.alpha = alpha;
    self.endTimeSubTitleLabel.alpha = alpha;
    self.statusLabel.alpha = alpha;
}

- (void)changeFrame{
    [self layoutIfNeeded];
    [self.contentView layoutIfNeeded];
    [self.lineView layoutIfNeeded];
    [self.closeButton layoutIfNeeded];
    [self.whiteBgView layoutIfNeeded];
    [self.iconImageView layoutIfNeeded];
    [self.titleLabel layoutIfNeeded];
    [self.nameLabel layoutIfNeeded];
    [self.subLabel layoutIfNeeded];
    [self.countLabel layoutIfNeeded];
    [self.statusButton layoutIfNeeded];
    [self.areaTitleLabel layoutIfNeeded];
    [self.introView layoutIfNeeded];
    [self.explainView layoutIfNeeded];
    [self.areaSubTitleLabel layoutIfNeeded];
    [self.introTitleLabel layoutIfNeeded];
    [self.explainTitleLabel layoutIfNeeded];
    [self.startTimeTitleLabel layoutIfNeeded];
    [self.startTimeSubTitleLabel layoutIfNeeded];
    [self.endTimeTitleLabel layoutIfNeeded];
    [self.endTimeSubTitleLabel layoutIfNeeded];
    [self.statusLabel layoutIfNeeded];
}

- (void)setModel:(QJTaskCenterCommonDetailModel *)model{
    if (_model != model) {
        _model = model;
        [self.countDown destoryTimer];
        [self.iconImageView setImageWithURL:[NSURL URLWithString:[_model.icon checkImageUrlString]] placeholder:[UIImage imageNamed:@"mine_head"]];
        self.titleLabel.text = _model.name;
        self.nameLabel.text = _model.gameName;
        self.subLabel.text = _model.rewardDesc;
        if ([_model.taskStatus isEqualToString:@"1"]) {
            [_statusButton setBackgroundImage:[UIImage imageNamed:@"qj_taskbutton_bgicon_yellow"] forState:UIControlStateNormal];
            [_statusButton setTitle:@"领取" forState:UIControlStateNormal];
            self.countLabel.text = [NSString stringWithFormat:@"名额%@/%@",self.model.currentNum,self.model.totalNum];
            _statusLabel.text = @"已开始";
        }else if ([_model.taskStatus isEqualToString:@"2"]) {
            [_statusButton setBackgroundImage:[UIImage imageNamed:@"qj_taskbutton_bgicon_yellow"] forState:UIControlStateNormal];
            [_statusButton setTitle:@"即将开始" forState:UIControlStateNormal];
            WS(weakSelf)
            [self.countDown countDownWithStratTimeStamp:[self.model.startTimeMillis longLongValue] finishTimeStamp:[self.model.endTimeMillis longLongValue] completeBlock:^(NSInteger day, NSInteger hour, NSInteger minute, NSInteger second) {
                weakSelf.countLabel.text = [NSString stringWithFormat:@"%02ld:%02ld:%02ld", (long)(day*24+hour), (long)minute, (long)second];
            }];
            self.countDown.TimerStopComplete = ^{
                weakSelf.statusLabel.text = @"已开始";
                [weakSelf.countDown destoryTimer];
            };
            _statusLabel.text = @"未开始";
        }else{
            [_statusButton setBackgroundImage:[UIImage imageNamed:@"qj_taskbutton_bgicon_gray"] forState:UIControlStateNormal];
            [_statusButton setTitle:@"已领完" forState:UIControlStateNormal];
            self.countLabel.text = [NSString stringWithFormat:@"名额%@/%@",self.model.currentNum,self.model.totalNum];
            _statusLabel.text = @"已抢光";
        }
        
        [self.introView removeAllSubviews];
        [self.explainView removeAllSubviews];
        self.areaSubTitleLabel.text = _model.gameServerName;
        self.startTimeSubTitleLabel.text = _model.startTime;
        self.endTimeSubTitleLabel.text = _model.endTime;
        for (int i = 0; i < _model.rulesDesc.count; i ++) {
            UILabel *testLabel = [UILabel new];
            testLabel.font = kFont(12);
            testLabel.textColor = [UIColor hx_colorWithHexStr:@"0xFFFFFF" alpha:0.8];
            testLabel.numberOfLines = 0;
            testLabel.text = [_model.rulesDesc safeObjectAtIndex:i];
            [self.introView addArrangedSubview:testLabel];
        }
        
        for (int i = 0; i < _model.taskDesc.count; i ++) {
            UILabel *testLabel = [UILabel new];
            testLabel.font = kFont(12);
            testLabel.textColor = [UIColor hx_colorWithHexStr:@"0xFFFFFF" alpha:0.8];
            testLabel.numberOfLines = 0;
            testLabel.text = [_model.taskDesc safeObjectAtIndex:i];
            [self.explainView addArrangedSubview:testLabel];
        }
        [self changeFrame];
    }
}

- (void)showAnimation{
    self.alpha = 0;
    [self changeAlpha:0];
    [self changeFrame];
    [self setNeedsUpdateConstraints];
    [UIView animateWithDuration:0.5 animations:^{
        self.alpha = 1;
        [self.contentView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.equalTo(@462);
        }];
        [self changeFrame];
    } completion:^(BOOL finished) {
        [self changeFrame];
    }];
    [UIView animateWithDuration:0.2 animations:^{
        [self changeAlpha:1];
    }];
}

- (void)hidAnimation{
    [self changeFrame];
    [self setNeedsUpdateConstraints];
    [UIView animateWithDuration:0.2 animations:^{
        [self changeAlpha:0];
    }];
    [UIView animateWithDuration:0.5 animations:^{
        self.alpha = 0;
        [self.contentView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.equalTo(@(self.adjustingFrame.size.height));
        }];
        [self changeFrame];
    } completion:^(BOOL finished) {
        [self changeFrame];
//        self.hidden = YES;
        self.alpha = 0;
    }];
}

#pragma mark - Lazy Loading
- (UIImageView *)iconImageView{
    if (!_iconImageView) {
        _iconImageView = [UIImageView new];
        _iconImageView.layer.cornerRadius = 10;
        _iconImageView.layer.masksToBounds = YES;
        _iconImageView.contentMode = UIViewContentModeScaleAspectFill;

    }
    return _iconImageView;
}

- (UIView *)whiteBgView {
    if (!_whiteBgView) {
        _whiteBgView = [UIView new];
        _whiteBgView.backgroundColor = [UIColor clearColor];
    }
    return _whiteBgView;
}

- (UIStackView *)introView{
    if (!_introView) {
        _introView = [UIStackView new];
        _introView.axis = UILayoutConstraintAxisVertical;
        _introView.distribution = UIStackViewDistributionFill;
        _introView.spacing = 10;
    }
    return _introView;
}

- (UIStackView *)explainView{
    if (!_explainView) {
        _explainView = [UIStackView new];
        _explainView.axis = UILayoutConstraintAxisVertical;
        _explainView.distribution = UIStackViewDistributionFill;
        _explainView.spacing = 10;
    }
    return _explainView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.font = kFont(14);
        _titleLabel.textColor = UIColorFromRGB(0xFFFFFF);
        _titleLabel.text = @"走完游戏地图";
    }
    return _titleLabel;
}

- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [UILabel new];
        _nameLabel.font = kFont(10);
        _nameLabel.textColor = UIColorFromRGB(0xF7D67D);
        _nameLabel.text = @"荣耀大天使";
    }
    return _nameLabel;
}

- (UILabel *)subLabel {
    if (!_subLabel) {
        _subLabel = [UILabel new];
        _subLabel.font = kFont(12);
        _subLabel.textColor = [UIColor hx_colorWithHexStr:@"0xFFFFFF" alpha:0.8];
        _subLabel.text = @"可获道具：振奋铠甲";
    }
    return _subLabel;
}

- (UILabel *)countLabel {
    if (!_countLabel) {
        _countLabel = [UILabel new];
        _countLabel.font = kFont(10);
        _countLabel.textColor = UIColorFromRGB(0xFFFFFF);
        _countLabel.text = @"名额0/10";
    }
    return _countLabel;
}

- (UIButton *)statusButton {
    if (!_statusButton) {
        _statusButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_statusButton addTarget:self action:@selector(btnAction) forControlEvents:UIControlEventTouchUpInside];
        _statusButton.titleLabel.font = kboldFont(12);
        [_statusButton setBackgroundImage:[UIImage imageNamed:@"qj_taskbutton_bgicon_yellow"] forState:UIControlStateNormal];
        [_statusButton setBackgroundImage:[UIImage imageNamed:@"qj_taskbutton_bgicon_gray"] forState:UIControlStateNormal];
        [_statusButton setTitle:@"领取" forState:UIControlStateNormal];
        [_statusButton setTitleColor:UIColorFromRGB(0x4C1C0F) forState:UIControlStateNormal];
    }
    return _statusButton;
}

- (UILabel *)areaTitleLabel {
    if (!_areaTitleLabel) {
        _areaTitleLabel = [UILabel new];
        _areaTitleLabel.font = kFont(12);
        _areaTitleLabel.textColor = UIColorFromRGB(0xFFFFFF);
        _areaTitleLabel.text = @"任务区服";
    }
    return _areaTitleLabel;
}

- (UILabel *)areaSubTitleLabel {
    if (!_areaSubTitleLabel) {
        _areaSubTitleLabel = [UILabel new];
        _areaSubTitleLabel.font = kFont(12);
        _areaSubTitleLabel.textColor = [UIColor hx_colorWithHexStr:@"0xFFFFFF" alpha:0.8];
        _areaSubTitleLabel.text = @"攻速冰雪 7月25号区";
    }
    return _areaSubTitleLabel;
}

- (UILabel *)introTitleLabel {
    if (!_introTitleLabel) {
        _introTitleLabel = [UILabel new];
        _introTitleLabel.font = kFont(12);
        _introTitleLabel.textColor = UIColorFromRGB(0xFFFFFF);
        _introTitleLabel.text = @"任务介绍";
    }
    return _introTitleLabel;
}

- (UILabel *)explainTitleLabel {
    if (!_explainTitleLabel) {
        _explainTitleLabel = [UILabel new];
        _explainTitleLabel.font = kFont(12);
        _explainTitleLabel.textColor = UIColorFromRGB(0xFFFFFF);
        _explainTitleLabel.text = @"任务说明";
    }
    return _explainTitleLabel;
}

- (UILabel *)startTimeTitleLabel {
    if (!_startTimeTitleLabel) {
        _startTimeTitleLabel = [UILabel new];
        _startTimeTitleLabel.font = kFont(12);
        _startTimeTitleLabel.textColor = UIColorFromRGB(0xFFFFFF);
        _startTimeTitleLabel.text = @"开始时间";
    }
    return _startTimeTitleLabel;
}

- (UILabel *)startTimeSubTitleLabel {
    if (!_startTimeSubTitleLabel) {
        _startTimeSubTitleLabel = [UILabel new];
        _startTimeSubTitleLabel.font = kFont(12);
        _startTimeSubTitleLabel.textColor = [UIColor hx_colorWithHexStr:@"0xFFFFFF" alpha:0.8];
        _startTimeSubTitleLabel.text = @"2022.07.30 19:00";
    }
    return _startTimeSubTitleLabel;
}

- (UILabel *)endTimeTitleLabel {
    if (!_endTimeTitleLabel) {
        _endTimeTitleLabel = [UILabel new];
        _endTimeTitleLabel.font = kFont(12);
        _endTimeTitleLabel.textColor = UIColorFromRGB(0xFFFFFF);
        _endTimeTitleLabel.text = @"结束时间";
    }
    return _endTimeTitleLabel;
}

- (UILabel *)endTimeSubTitleLabel {
    if (!_endTimeSubTitleLabel) {
        _endTimeSubTitleLabel = [UILabel new];
        _endTimeSubTitleLabel.font = kFont(12);
        _endTimeSubTitleLabel.textColor = [UIColor hx_colorWithHexStr:@"0xFFFFFF" alpha:0.8];
        _endTimeSubTitleLabel.text = @"2022.07.31 19:00";
    }
    return _endTimeSubTitleLabel;
}

- (UILabel *)statusLabel {
    if (!_statusLabel) {
        _statusLabel = [UILabel new];
        _statusLabel.font = kFont(12);
        _statusLabel.textColor = UIColorFromRGB(0xECB16C);
        _statusLabel.text = @"已开始";
    }
    return _statusLabel;
}

- (void)btnAction{
    [self hidAnimation];
    if (self.btnClick) {
        self.btnClick(self.model);
    }
}

@end
