//
//  QJMoreTaskViewController.m
//  QJBox
//
//  Created by rui on 2022/6/28.
//

#import "QJMoreTaskViewController.h"
#import "QJMyTaskViewController.h"
#import "QJProductDetailViewController.h"
/* 弹框View */
#import "QJScreenPopVIew.h"
/* 弹框公共 */
#import "QJCheckInPopCommonView.h"

/* 自定义任务cell */
#import "QJCheckInTaskCell.h"
#import "QJTaskCenterPopView.h"
/* 任务中心公共model */
#import "QJTaskCenterCommonModel.h"
#import "QJSearchGameListModel.h"

/* 任务中心请求 */
#import "QJTaskRequest.h"

@interface QJMoreTaskViewController ()<UITableViewDelegate, UITableViewDataSource>

/* 自定义导航View */
@property (nonatomic, strong) UIView *navView;
/* 返回按钮 */
@property (nonatomic, strong) UIButton *backButton;
@property (nonatomic, strong) UIButton *myTaskButton;
@property (nonatomic, strong) UIView *redView;
/* 标题 */
@property (nonatomic, strong) UILabel *titleLabel;
/* tableView */
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UITableView *tableView;
//数据源
@property (nonatomic, strong) NSMutableArray *dataArr;
/* model */
@property (nonatomic, strong) QJTaskCenterCommonModel *pageModel;
@property (nonatomic, assign) BOOL isFlag;

@property (nonatomic, strong) QJTaskCenterPopView *popView;

@end

@implementation QJMoreTaskViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    /* 调整自定义导航视图层次 */
    [self.view bringSubviewToFront:self.navView];
}

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.title = @"任务大厅";
    self.navBar.hidden = YES;
    /* 加载自定义导航 */
    [self initNav];
    /* 初始化UI */
    [self initUI];
    /* 请求方法实现 */
    self.isFlag = NO;
    self.pageModel = [[QJTaskCenterCommonModel alloc] init];
    [self sendRequest];
    [self getTaskRemind];
    
    QJTaskCenterPopView *popView = [[QJTaskCenterPopView alloc] initWithFrame:CGRectMake(0, 0, QJScreenWidth, QJScreenHeight)];
    [self.view addSubview:popView];
    self.popView = popView;
    self.popView.alpha = 0;
    WS(weakSelf)
    self.popView.btnClick = ^(QJTaskCenterCommonDetailModel * _Nonnull model) {
        if ([model.taskStatus isEqualToString:@"1"]) {
            [weakSelf.pageModel.records enumerateObjectsUsingBlock:^(QJTaskCenterCommonDetailModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if ([obj.ID isEqualToString:model.ID]) {
                    obj.isReceiveTask = YES;
                }
            }];
            [weakSelf receiveRequest:model];
        }else{
            [weakSelf initTaskPopView:model];
        }
    };
}

/**
 * @author: zjr
 * @date: 2022-8-15
 * @desc: 自定义导航
 */
- (void)initNav{
    UIImageView *bgImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"qj_taskcenter_bg"]];
    bgImageView.frame = CGRectMake(0, 0, QJScreenWidth, 200*kWScale);
    [self.view addSubview:bgImageView];
    [bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@(200*kWScale));
        make.top.left.right.equalTo(self.view);
    }];
    
    self.view.backgroundColor = UIColorFromRGB(0x000000);
    [bgImageView graduateLeftColor:[UIColor clearColor] ToColor:UIColorFromRGB(0x000000) startPoint:CGPointMake(0.5, 0) endPoint:CGPointMake(0.5, 1)];

    self.navView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, QJScreenWidth, NavigationBar_Bottom_Y)];
    self.navView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.navView];
    
    self.backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.backButton setImage:[UIImage imageNamed:@"backImage_white"] forState:UIControlStateNormal];
    self.backButton.frame = CGRectMake(9, [QJDeviceConstant top_iPhoneX_SPACE]+27, 30, 30);
    [self.backButton addTarget:self action:@selector(backBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [self.navView addSubview:self.backButton];
    
    self.myTaskButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.myTaskButton setImage:[UIImage imageNamed:@"qj_taskcenter_icon"] forState:UIControlStateNormal];
    self.myTaskButton.titleLabel.font = kFont(12);
    [self.myTaskButton setTitle:@"我的任务" forState:UIControlStateNormal];
    [self.myTaskButton setTitleColor:UIColorFromRGB(0xFFFFFF) forState:UIControlStateNormal];
    self.myTaskButton.frame = CGRectMake(QJScreenWidth - 60 - 16, [QJDeviceConstant top_iPhoneX_SPACE]+20, 60, 50);
    [self.myTaskButton layoutWithStatus:QJLayoutStatusImageTop andMargin:5];
    [self.myTaskButton addTarget:self action:@selector(taskBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [self.navView addSubview:self.myTaskButton];
    
    [self.navView addSubview:self.redView];
    [self.redView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.myTaskButton.mas_right).offset(-15);
        make.top.equalTo(self.myTaskButton.mas_top).offset(10);
        make.width.height.equalTo(@4);
    }];
    
    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 30)];
    self.titleLabel.centerY = self.backButton.centerY;
    self.titleLabel.centerX = QJScreenWidth/2;
    self.titleLabel.text = @"任务大厅";
    self.titleLabel.font = kboldFont(17);
    self.titleLabel.textColor = UIColorFromRGB(0xFFFFFF);
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.navView addSubview:self.titleLabel];
}

- (UIView *)redView {
    if (!_redView) {
        _redView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 4, 4)];
        _redView.layer.cornerRadius = 2;
        _redView.layer.masksToBounds = YES;
        _redView.backgroundColor = [UIColor redColor];
    }
    return _redView;
}


/**
 * @author: zjr
 * @date: 2022-6-28
 * @desc: 初始化UI
 */
- (void)initUI{
    [self.view addSubview:self.scrollView];
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(NavigationBar_Bottom_Y);
        make.left.right.bottom.equalTo(self.view);
    }];
    
    //qj_taskcenter_bg0
    UIImageView *bgIconImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"qj_taskcentericon_bg"]];
    bgIconImageView.frame = CGRectMake(0, 0, QJScreenWidth, 262*kWScale);
    [self.scrollView addSubview:bgIconImageView];
    [bgIconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@(262*kWScale));
        make.width.equalTo(@(QJScreenWidth));
        make.top.equalTo(self.scrollView).offset(10);
    }];
    
    UIImageView *bgImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"qj_taskcentercontent_bg"]];
    bgImageView.frame = CGRectMake(16, 0, QJScreenWidth-32, 200*kWScale);
    [self.scrollView addSubview:bgImageView];
    [bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@(QJScreenHeight - NavigationBar_Bottom_Y-10));
        make.width.equalTo(@(QJScreenWidth - 32));
        make.top.equalTo(self.scrollView).offset(10);
        make.left.equalTo(self.scrollView).offset(16);
        make.right.equalTo(self.scrollView).offset(-16);
    }];
    
    UIImageView *bgTopImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"qj_taskcenter_bg0"]];
    bgTopImageView.frame = CGRectMake(0, 0, QJScreenWidth, 40);
    [self.scrollView addSubview:bgTopImageView];
    [bgTopImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@(40*kWScale));
        make.width.equalTo(@(QJScreenWidth));
        make.top.equalTo(self.scrollView);
    }];

    [self.scrollView addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.left.right.bottom.equalTo(self.scrollView);
        make.top.equalTo(self.scrollView).offset(10);
        make.left.equalTo(self.scrollView).offset(16);
        make.right.equalTo(self.scrollView).offset(-16);
        make.height.equalTo(@(QJScreenHeight - NavigationBar_Bottom_Y-20));
        make.width.equalTo(@(QJScreenWidth - 32));
    }];
}

/**
 * @author: zjr
 * @date: 2022-6-28
 * @desc: button点击事件
 */
- (void)backBtnAction{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)taskBtnAction{
    QJMyTaskViewController *myTaskVC = [[QJMyTaskViewController alloc] init];
    [self.navigationController pushViewController:myTaskVC animated:YES];
}

#pragma mark - Lazy Loading
- (UIScrollView *)scrollView {
    if (!_scrollView) {
        CGFloat scrollW = self.view.width;
        CGFloat scrollH = QJScreenHeight - NavigationBar_Bottom_Y;
        
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, scrollW, scrollH)];
        _scrollView.backgroundColor = [UIColor clearColor];
        _scrollView.pagingEnabled = YES;
        _scrollView.directionalLockEnabled = YES;
        _scrollView.bounces = YES;
        _scrollView.delegate = self;
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.showsHorizontalScrollIndicator = NO;
//        _scrollView.mj_header = [QJRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(refreshData)];
//        _scrollView.mj_footer = [QJRefreshFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreHomeData)];
    }
    return _scrollView;
}

- (UITableView *)tableView {
    if (!_tableView) {
        self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, NavigationBar_Bottom_Y, QJScreenWidth, self.view.height-NavigationBar_Bottom_Y) style:UITableViewStyleGrouped];
        self.tableView.backgroundColor = [UIColor clearColor];
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.tableView.showsVerticalScrollIndicator = NO;
        self.tableView.showsHorizontalScrollIndicator = NO;
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        [self.tableView registerClass:[QJCheckInTaskCell class] forCellReuseIdentifier:@"QJCheckInTaskCell"];
        if (@available(iOS 11.0, *)) {
            self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
        _tableView.mj_header = [QJRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(refreshData)];
        _tableView.mj_footer = [QJRefreshFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreHomeData)];
    }
    return _tableView;
}

- (NSMutableArray *)dataArr {
    if (!_dataArr) {
        self.dataArr = [NSMutableArray arrayWithCapacity:0];
    }
    return _dataArr;
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    QJEmptyCommonView *emptyView = [QJEmptyCommonView new];
    emptyView.string = @"暂无内容";
    emptyView.textColor = [UIColor whiteColor];
    emptyView.emptyImage = @"empty_no_data";
    emptyView.topSpace = 100*kWScale;
    emptyView.backgroundColor = [UIColor clearColor];
    [tableView tableViewDisplayWhenHaveNoDataWithView:emptyView ifNecessaryForRowCount:self.pageModel.records.count];

    return _pageModel.records.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    QJCheckInTaskCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QJCheckInTaskCell" forIndexPath:indexPath];
    QJTaskCenterCommonDetailModel *cellModel = [_pageModel.records safeObjectAtIndex:indexPath.row];
    cellModel.modelIndex = indexPath.row;
    cell.model = cellModel;
    WS(weakSelf)
    cell.btnClick = ^(QJTaskCenterCommonDetailModel * _Nonnull model) {
        if ([model.taskStatus isEqualToString:@"1"]) {
            [weakSelf.pageModel.records enumerateObjectsUsingBlock:^(QJTaskCenterCommonDetailModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if ([obj.ID isEqualToString:model.ID]) {
                    obj.isReceiveTask = YES;
                }
            }];
            [weakSelf receiveRequest:model];
        }else{
            [weakSelf initTaskPopView:model];
        }
    };
    return cell;
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 76*kWScale;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    QJCheckInTaskCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    CGRect chooseFrame;
    chooseFrame = cell.frame;
    chooseFrame.size.width = QJScreenWidth - 32;
    chooseFrame.size.height = 76*kWScale;
    chooseFrame.origin.x = 16;
    chooseFrame.origin.y = QJScreenHeight/2;
    self.popView.adjustingFrame = chooseFrame;
    self.popView.model = [_pageModel.records safeObjectAtIndex:indexPath.row];
    [self.popView showAnimation];
}

//- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
//    UIView *headView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, QJScreenWidth, 40)];
//    headView.backgroundColor = [UIColor redColor];
//    return headView;
//}
//
//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
//    return 40;
//}

#pragma mark - 请求方法
- (void)refreshData {
    self.isFlag = !self.isFlag;
    _pageModel.willLoadMore = NO;
    [self sendRequest];
}

- (void)loadMoreHomeData {
    DLog(@"加载更多");
    if(_pageModel.hasNext){
        _pageModel.willLoadMore=YES;
        [self sendRequest];
    }else{
        [self.tableView.mj_footer endRefreshing];
        [self.tableView.mj_footer endRefreshingWithNoMoreData];
    }
}

- (void)sendRequest{
    DLog(@"请求方法实现");
    QJTaskRequest *productReq = [[QJTaskRequest alloc] init];
    NSMutableDictionary *muDic = [NSMutableDictionary dictionaryWithDictionary:_pageModel.toParams];
    [muDic setValue:@"10" forKey:@"size"];
    [muDic setValue:[NSNumber numberWithBool:self.isFlag] forKey:@"flag"];
    productReq.dic = muDic.copy;
    [productReq getTaskCenterListRequest];
    
    WS(weakSelf)
    productReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            [weakSelf.pageModel configObj:[QJTaskCenterCommonModel modelWithDictionary:EncodeDicFromDic(request.responseObject, @"data")]];
        }else{
            NSString *msg = EncodeStringFromDic(request.responseObject, @"message");
            [weakSelf.view makeToast:msg];
        }
        [weakSelf.tableView reloadData];
        [weakSelf.tableView.mj_header endRefreshing];
        [weakSelf.tableView.mj_footer endRefreshing];
    };
    productReq.failureCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        [weakSelf.tableView reloadData];
        [weakSelf.tableView.mj_header endRefreshing];
        [weakSelf.tableView.mj_footer endRefreshing];
        [weakSelf.view makeToast:@"数据异常 请稍后再试"];
    };
}

- (void)receiveRequest:(QJTaskCenterCommonDetailModel *)model{
    QJTaskRequest *productReq = [[QJTaskRequest alloc] init];
    NSMutableDictionary *muDic = [NSMutableDictionary dictionary];
    [muDic setValue:model.ID forKey:@"id"];
    productReq.dic = muDic.copy;
    [productReq getTaskReceiveRequest];
    
    WS(weakSelf)
    productReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            [weakSelf.tableView reloadData];
            [weakSelf initTaskPopView:model];
        }else{
            NSString *msg = EncodeStringFromDic(request.responseObject, @"message");
            [weakSelf.view makeToast:msg];
        }
    };
    productReq.failureCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        [weakSelf.view makeToast:@"数据异常 请稍后再试"];
    };
}

- (void)getTaskRemind{
    QJTaskRequest *productReq = [[QJTaskRequest alloc] init];
    [productReq getTaskRemindRequest];
    
    WS(weakSelf)
    productReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            if ([EncodeDicFromDic(request.responseObject, @"data") isKindOfClass:[NSDictionary class]]) {
                NSDictionary *dataDic = EncodeDicFromDic(request.responseObject, @"data");
                NSString *allRemindStr = EncodeStringFromDicDefEmtryValue(dataDic, @"allRemind");
                if ([allRemindStr isEqualToString:@"1"]) {
                    weakSelf.redView.hidden = NO;
                }else{
                    weakSelf.redView.hidden = YES;
                }
//                NSString *qicoinGotStr = EncodeStringFromDicDefEmtryValue(dataDic, @"readRemind");
//                NSString *qicoinGotStr = EncodeStringFromDicDefEmtryValue(dataDic, @"rewardRemind");
                
            }
        }
    };
}

/**
 * @author: zjr
 * @date: 2022-7-5
 * @desc: 领取任务奖励
 */
- (void)initTaskPopView:(QJTaskCenterCommonDetailModel *)model{
    QJCheckInPopCommonView *contentView = [[QJCheckInPopCommonView alloc] initWithFrame:CGRectMake(0, 0, 300*kWScale, 344*kWScale)];
    if ([model.status isEqualToString:@"UNRECEIVED"]) {
        contentView.popViewStyle = QJPopContentViewStyleRECEIVED;
    }else{
        contentView.popViewStyle = QJPopContentViewStyleTaskRECEIVED;
    }
    contentView.model = model;
    contentView.directionBlock = ^(ButtonClickType direction) {
//        if ([model.taskStatus isEqualToString:@"1"]) {
//            [self.pageModel.records safeRemoveObjectAtIndex:model.modelIndex];
//            [self.tableView reloadData];
//        }
//        [self sendRequest];
        [self btnAction];
        if (direction == ButtonClickTypeLookTask) {
            QJMyTaskViewController *myTaskVC = [[QJMyTaskViewController alloc] init];
            [self.navigationController pushViewController:myTaskVC animated:YES];
        }else if (direction == ButtonClickTypeGoDone) {
            QJProductDetailViewController *detailVC = [[QJProductDetailViewController alloc] init];
            QJSearchGameDetailModel *detailModel = [QJSearchGameDetailModel new];
            detailModel.ID = model.gameId;
            detailVC.model = detailModel;
            [self.navigationController pushViewController:detailVC animated:YES];
        }
    };
    QJScreenPopVIew *popView = [[QJScreenPopVIew alloc] initWithCenterView:contentView];
    popView.dismissWhenClicked = YES;
    popView.popViewPopType = QJScreenPopViewPopTypeDefault;
    popView.tag = 100000;
    popView.tapDismissBlock = ^{
//        if ([model.taskStatus isEqualToString:@"1"]) {
//            [self.pageModel.records safeRemoveObjectAtIndex:model.modelIndex];
//            [self.tableView reloadData];
//        }
    };
    [popView showInView:self.view];
}

- (void)btnAction{
    QJScreenPopVIew *popView = [self.view viewWithTag:100000];
    [popView dismissWithBlock:nil];
}

@end
