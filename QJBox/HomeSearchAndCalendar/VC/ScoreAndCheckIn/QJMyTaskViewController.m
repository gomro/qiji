//
//  QJMyTaskViewController.m
//  QJBox
//
//  Created by rui on 2022/8/15.
//

#import "QJMyTaskViewController.h"
#import "QJSegmentLineView.h"
#import "QJTaskCenterCommonView.h"
#import "QJTaskRequest.h"

@interface QJMyTaskViewController ()<UIScrollViewDelegate>

@property (nonatomic, strong) QJSegmentLineView *segment;
@property (nonatomic, strong) UIScrollView      *scrollView;
@property (nonatomic, strong) NSMutableArray    *childVCs;

@end

@implementation QJMyTaskViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorFromRGB(0xF5F5F5);
    self.title = @"我的任务";
    /* 初始化UI */
    [self initUI];
    [self getTaskRemind];
}

/**
 * @author: zjr
 * @date: 2022-8-15
 * @desc: 初始化UI
 */
- (void)initUI{
    [self.view addSubview:self.segment];
    [self.view addSubview:self.scrollView];
    [self.segment mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(NavigationBar_Bottom_Y);
        make.left.right.equalTo(self.view);
        make.height.equalTo(@(53*kWScale));
    }];
    
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(NavigationBar_Bottom_Y+53*kWScale);
        make.left.bottom.right.equalTo(self.view);
    }];
    
    for (int i = 0; i < 3 ; i ++) {
        QJTaskCenterCommonView *view = [[QJTaskCenterCommonView alloc] initWithFrame:CGRectMake(0, 0, QJScreenWidth, QJScreenHeight - 53 - NavigationBar_Bottom_Y)];
        if (i == 0) {
            view.type = @"receive";
        }else if (i == 1) {
            view.type = @"done";
        }else {
            view.type = @"disable";
        }
        [view refreshFirst];
        [self.childVCs addObject:view];
    }
    [self.childVCs enumerateObjectsUsingBlock:^(UIView *vc, NSUInteger idx, BOOL * _Nonnull stop) {
        [self.scrollView addSubview:vc];
        vc.frame = CGRectMake(idx * QJScreenWidth, 0, QJScreenWidth, QJScreenHeight - 53*kWScale - NavigationBar_Bottom_Y);
    }];
    _scrollView.contentSize = CGSizeMake(self.childVCs.count * QJScreenWidth, 0);
    
    [_segment showRedCount:NO];
}

- (QJSegmentLineView *)segment {
    if (!_segment) {
        _segment = [[QJSegmentLineView alloc] initWithFrame:CGRectMake(0, NavigationBar_Bottom_Y, QJScreenWidth, 53*kWScale)];
        _segment.backgroundColor = UIColorFromRGB(0xF5F5F5);
        _segment.segmentWidthStyle = QJSegmentViewWidthStyleEqual;
        _segment.itemWidth = QJScreenWidth/3;
        _segment.font = kFont(16);
        _segment.selectedFont = kboldFont(18);
        _segment.textColor = UIColorFromRGB(0x16191C);
        _segment.selectedTextColor = UIColorFromRGB(0x16191C);
        _segment.showBottomLine = NO;
        _segment.tag = 10000;
        [_segment setTitleArray:@[@"已领取",@"已完成",@"已失效",]];
        WS(weakSelf)
        [_segment setSegmentedItemSelectedBlock:^(QJSegmentLineView * _Nonnull segment, NSInteger selectedIndex) {
            [weakSelf.scrollView setContentOffset:CGPointMake(QJScreenWidth * selectedIndex, 0) animated:NO];
        }];
    }
    return _segment;
}

- (UIScrollView *)scrollView {
    if (!_scrollView) {
        CGFloat scrollW = self.view.width;
        CGFloat scrollH = QJScreenHeight - 53*kWScale - NavigationBar_Bottom_Y;
        
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 53*kWScale, scrollW, scrollH)];
        _scrollView.backgroundColor = UIColorFromRGB(0xF5F5F5);
        _scrollView.pagingEnabled = YES;
        _scrollView.directionalLockEnabled = YES;
//        _scrollView.scrollEnabled = NO;
        _scrollView.bounces = NO;
        _scrollView.delegate = self;
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.showsHorizontalScrollIndicator = NO;
    }
    return _scrollView;
}

- (NSMutableArray *)childVCs {
    if (!_childVCs) {
        _childVCs = [NSMutableArray array];
    }
    return _childVCs;
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    int page = (_scrollView.contentOffset.x+self.view.width/2.0) / self.view.width;
    [_segment setSegmentSelectedIndex:page];
}

- (void)getTaskRemind{
    QJTaskRequest *productReq = [[QJTaskRequest alloc] init];
    [productReq getTaskRemindRequest];
    
    WS(weakSelf)
    productReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            if ([EncodeDicFromDic(request.responseObject, @"data") isKindOfClass:[NSDictionary class]]) {
                NSDictionary *dataDic = EncodeDicFromDic(request.responseObject, @"data");
//                NSString *allRemindStr = EncodeStringFromDicDefEmtryValue(dataDic, @"allRemind");
                NSString *rewardRemindStr = EncodeStringFromDicDefEmtryValue(dataDic, @"rewardRemind");
                if ([rewardRemindStr isEqualToString:@"1"]) {
                    [weakSelf.segment showRedCount:NO];
                }else{
                    [weakSelf.segment showRedCount:YES];
                }
//                NSString *qicoinGotStr = EncodeStringFromDicDefEmtryValue(dataDic, @"readRemind");
//                NSString *qicoinGotStr = EncodeStringFromDicDefEmtryValue(dataDic, @"rewardRemind");
                
            }
        }
    };
}

@end
