//
//  QJTaskCenterPopView.h
//  QJBox
//
//  Created by rui on 2022/8/16.
//

#import <UIKit/UIKit.h>
/* 任务中心公共model */
#import "QJTaskCenterCommonModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJTaskCenterPopView : UIView

@property (nonatomic, strong) QJTaskCenterCommonDetailModel *model;
@property (nonatomic, assign) CGRect adjustingFrame;
@property (nonatomic, copy) void (^btnClick)(QJTaskCenterCommonDetailModel *model);

- (void)showAnimation;
- (void)hidAnimation;

@end

NS_ASSUME_NONNULL_END
