//
//  QJCheckInCalendarDetailViewController.h
//  QJBox
//
//  Created by rui on 2022/6/28.
//

#import "QJBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJCheckInCalendarDetailViewController : QJBaseViewController

//数据源
@property (nonatomic, strong) NSString *month;

@end

NS_ASSUME_NONNULL_END
