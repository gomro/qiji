//
//  QJCheckInCalendarDetailViewController.m
//  QJBox
//
//  Created by rui on 2022/6/28.
//

#import "QJCheckInCalendarDetailViewController.h"

/* 日历控件 */
#import "QJCalendarHeaderView.h"
/* 补签 */
#import "QJCalendarRepairView.h"
/* 补签弹框 */
//#import "QJAlertView.h"
/* model */
#import "QJTaskListModel.h"
#import "QJSignedModel.h"
//积分信息
#import "QJScoreRequest.h"
#import "QJCheckInRequest.h"
#import "QJCheckInPostRequest.h"

@interface QJCheckInCalendarDetailViewController ()<UIScrollViewDelegate>

/* 日历背景图片 */
@property (strong, nonatomic) UIImageView *bgImageView;
/* 自定义导航View */
@property (nonatomic, strong) UIView *navView;
/* 返回按钮 */
@property (nonatomic, strong) UIButton *backButton;
/* 标题 */
@property (nonatomic, strong) UILabel *titleLabel;
/* 日历view */
@property (nonatomic, strong) QJCalendarHeaderView *headerView;
/* 补签view */
@property (nonatomic, strong) QJCalendarRepairView *repairView;
/* scrollView */
@property (nonatomic, strong) UIScrollView *scrollView;
/* model */
@property (nonatomic, strong) QJSignedModel *signedModel;

@end

@implementation QJCheckInCalendarDetailViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    /* 调整自定义导航视图层次 */
    [self.view bringSubviewToFront:self.navView];
}

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.title = @"签到日历";
    self.navBar.hidden = YES;
    self.signedModel = [QJSignedModel new];
    self.signedModel.days = [NSMutableArray array];
    [NSTimeZone setDefaultTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    /* 加载自定义导航 */
    [self initNav];
    /* 初始化UI */
    [self initUI];
    /* 请求方法 */
    [self initRequest];
    
    if (@available(iOS 11.0, *)) {
        _scrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
}

//- (void)Test{
//    QJAlertView *alertView = [[QJAlertView alloc] initWithTitle:@"补签失败" message:@"您的补签次数为0 您可通过分享好友或者积分兑换补签次数奥～" leftButtonTitle:@"好的" rightButtonTitle:@"去获取" buttonClick:^(ButtonDirection direction) {
//        if (direction == ButtonDirectionRight) {
//            DLog(@"去获取");
//        }
//    }];
//    [alertView show];
//}

/**
 * @author: zjr
 * @date: 2022-7-18
 * @desc: 自定义导航
 */
- (void)initNav{
    self.view.backgroundColor = UIColorFromRGB(0xF5F5F5);
    self.navView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, QJScreenWidth, NavigationBar_Bottom_Y)];
    self.navView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.navView];
    
    self.backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.backButton setImage:[UIImage imageNamed:@"backimage_black"] forState:UIControlStateNormal];
    self.backButton.frame = CGRectMake(9, [QJDeviceConstant top_iPhoneX_SPACE]+27, 30, 30);
    [self.backButton addTarget:self action:@selector(backBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [self.navView addSubview:self.backButton];
    
    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 30)];
    self.titleLabel.centerY = self.backButton.centerY;
    self.titleLabel.centerX = QJScreenWidth/2;
    self.titleLabel.text = @"签到日历";
    self.titleLabel.font = kboldFont(17);
    self.titleLabel.textColor = UIColorFromRGB(0x000000);
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.navView addSubview:self.titleLabel];
}

/**
 * @author: zjr
 * @date: 2022-6-28
 * @desc: 初始化UI
 */
- (void)initUI{
    [self.view addSubview:self.scrollView];

    _scrollView.contentSize = CGSizeMake(_scrollView.width, QJScreenHeight);

    [self.scrollView addSubview:self.bgImageView];

    self.bgImageView.image = [UIImage imageNamed:@"calendar_monthbg_07"];
    
    self.headerView = [[QJCalendarHeaderView alloc] initWithFrame:CGRectMake(15, 200*kWScale, QJScreenWidth-30, (448+80)*kWScale)];
    [self.scrollView addSubview:self.headerView];

    self.repairView = [[QJCalendarRepairView alloc] initWithFrame:CGRectMake(0, self.headerView.bottom+12, QJScreenWidth, 105*kWScale)];
    [self.scrollView addSubview:self.repairView];
    
    CGFloat height = (200+448+80+105+12+15)*kWScale;
    if (height > QJScreenHeight) {
        _scrollView.contentSize = CGSizeMake(_scrollView.width, height);
    }else{
        _scrollView.contentSize = CGSizeMake(_scrollView.width, QJScreenHeight+10);
    }
    
    /* 兑换次数 */
    WS(weakSelf)
    self.headerView.btnClick = ^(NSString * _Nonnull model) {
        [weakSelf repair:model];
    };
    /* 切换背景图片 */
    self.headerView.leftMonthClick = ^(NSString * _Nonnull month) {
//        [self.scrollView setContentOffset:CGPointMake(0, self.headerView.bottom - NavigationBar_Bottom_Y) animated:YES];
//        return;
        NSString *currentMonth = [NSString getStringForDateString:month byFormat:@"yyyy-MM-dd" toFormat:@"MM"];
        weakSelf.month = [NSString getStringForDateString:month byFormat:@"yyyy-MM-dd" toFormat:@"yyyy-MM"];
        weakSelf.bgImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"calendar_monthbg_%@",currentMonth]];
        weakSelf.signedModel.isLastOrNextMonth = YES;
        [weakSelf monthDataReq];
    };
    self.headerView.rightMonthClick = ^(NSString * _Nonnull month) {
        NSString *currentMonth = [NSString getStringForDateString:month byFormat:@"yyyy-MM-dd" toFormat:@"MM"];
        weakSelf.month = [NSString getStringForDateString:month byFormat:@"yyyy-MM-dd" toFormat:@"yyyy-MM"];
        weakSelf.bgImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"calendar_monthbg_%@",currentMonth]];
        weakSelf.signedModel.isLastOrNextMonth = YES;
        [weakSelf monthDataReq];
    };
    
    self.repairView.btnClick = ^(QJSignedModel * _Nonnull model) {
        [weakSelf repairCount];
    };
}

/**
 * @author: zjr
 * @date: 2022-6-28
 * @desc: button点击事件
 */
- (void)backBtnAction{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Lazy Loading
- (UIScrollView *)scrollView{
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.width, self.view.height)];
        _scrollView.backgroundColor = UIColorFromRGB(0xF5F5F5);
        _scrollView.scrollEnabled = YES;
        _scrollView.pagingEnabled = NO;
        _scrollView.bounces = YES;
        _scrollView.delegate = self;
        _scrollView.showsVerticalScrollIndicator = NO;
        _scrollView.showsHorizontalScrollIndicator = NO;
    }
    return _scrollView;
}

- (UIImageView *)bgImageView{
    if (!_bgImageView) {
        _bgImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, QJScreenWidth, 223*kWScale)];
        _bgImageView.userInteractionEnabled = YES;
    }
    return _bgImageView;
}

/*
 ------------------自定义alertview
 //    cell.btnClick = ^(QJTaskDetailModel * _Nonnull model) {
 //        if ([model.title isEqualToString:@"分享好友"]) {
 //            [[QJShareManager sharedManager] sharWithShareModel:[QJShareModel shareModelFactoryWithData:nil]];
 //        }else{
 //            QJAlertView *alertView = [[QJAlertView alloc] initWithTitle:@"补签失败" message:@"您的补签次数为0 您可通过分享好友或者积分兑换补签次数奥～" leftButtonTitle:@"好的" rightButtonTitle:@"去获取" buttonClick:^(ButtonDirection direction) {
 //                if (direction == ButtonDirectionRight) {
 //                    DLog(@"去获取");
 //                }
 //            }];
 //            [alertView show];
 //        }

 */
#pragma mark - 请求方法
- (void)initRequest{
    DLog(@"请求方法实现");
    self.signedModel.isLastOrNextMonth = NO;
    [self taskInfoReq];
    [self monthDataReq];
}

/**
 * @author: zjr
 * @date: 2022-7-18
 * @desc: 积分信息
 */
- (void)taskInfoReq{
    QJScoreRequest *scoreReq = [[QJScoreRequest alloc] init];
    [scoreReq getUserScoreInfoRequest];
    WS(weakSelf)
    scoreReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            if ([EncodeDicFromDic(request.responseObject, @"data") isKindOfClass:[NSDictionary class]]) {
                NSDictionary *dataDic = EncodeDicFromDic(request.responseObject, @"data");
                weakSelf.signedModel.qicoin = EncodeStringFromDicDefEmtryValue(dataDic, @"qicoin");
                weakSelf.signedModel.qjcoinGot = EncodeStringFromDicDefEmtryValue(dataDic, @"qjcoinGot");
                weakSelf.signedModel.remainSignSupplyCount = EncodeStringFromDicDefEmtryValue(dataDic, @"remainSignSupplyCount");
                weakSelf.signedModel.unUsedSupplySignCount = EncodeStringFromDicDefEmtryValue(dataDic, @"unUsedSupplySignCount");
                weakSelf.signedModel.birthday = EncodeStringFromDicDefEmtryValue(dataDic, @"birthday");
                weakSelf.signedModel.today = EncodeStringFromDicDefEmtryValue(dataDic, @"today");
                weakSelf.headerView.model = weakSelf.signedModel;
                weakSelf.repairView.model = weakSelf.signedModel;
            }
        }else{
            NSString *msg = EncodeStringFromDic(request.responseObject, @"message");
            [weakSelf.view makeToast:msg];
        }
    };
    scoreReq.failureCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        [weakSelf.view makeToast:@"数据异常 请稍后再试"];
    };
}

/**
 * @author: zjr
 * @date: 2022-7-18
 * @desc: 兑换补签次数
 */
- (void)repairCount{
    if (![self.signedModel.qicoin isEqualToString:@"0"]) {
        if (self.signedModel.qicoin.integerValue < 5) {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"补签失败" message:@"您的奇迹币不足，扣款失败，是否获取？" preferredStyle:UIAlertControllerStyleAlert];
            [alertController addAction:([UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                
            }])];
            [alertController addAction:([UIAlertAction actionWithTitle:@"去获取" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self.navigationController popViewControllerAnimated:YES];
            }])];
            
            [self presentViewController:alertController animated:YES completion:nil];
        }else{
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"奇迹币补签" message:@"确认用5奇迹币兑换一次补签机会？" preferredStyle:UIAlertControllerStyleAlert];
            [alertController addAction:([UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                
            }])];
            [alertController addAction:([UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                QJCheckInPostRequest *checkinReq = [[QJCheckInPostRequest alloc] init];
                [checkinReq getSupplySignRequest];
                WS(weakSelf)
                checkinReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
                    if (ResponseSuccess) {
                        [weakSelf.view makeToast:@"兑换成功，补签次数+1~"];
                        [weakSelf taskInfoReq];
                    }else{
                        NSString *msg = EncodeStringFromDic(request.responseObject, @"message");
                        [weakSelf.view makeToast:msg];
                    }
                };
                checkinReq.failureCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
                    [weakSelf.view makeToast:@"数据异常 请稍后再试"];
                };
            }])];
            
            [self presentViewController:alertController animated:YES completion:nil];
        }
    }
}

/**
 * @author: zjr
 * @date: 2022-7-18
 * @desc: 补签
 */
- (void)repair:(NSString *)dateString{
    if ([self.signedModel.unUsedSupplySignCount isEqualToString:@"0"]) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"补签失败" message:@"您的补签次数为0，您可使用5奇迹币兑换补签卡噢～" preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:([UIAlertAction actionWithTitle:@"不兑换" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }])];
        [alertController addAction:([UIAlertAction actionWithTitle:@"兑换" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self.scrollView setContentOffset:CGPointMake(0, self.headerView.bottom - NavigationBar_Bottom_Y) animated:YES];
        }])];

        [self presentViewController:alertController animated:YES completion:nil];
        return;
    }
    NSMutableDictionary *muDic = [NSMutableDictionary dictionary];
    [muDic setValue:dateString forKey:@"signDate"];
    QJCheckInPostRequest *checkinReq = [[QJCheckInPostRequest alloc] init];
    checkinReq.dic = muDic.copy;
    [checkinReq getRepairCheckInRequest];
    WS(weakSelf)
    checkinReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            [weakSelf initRequest];
        }else{
            NSString *msg = EncodeStringFromDic(request.responseObject, @"message");
            [weakSelf.view makeToast:msg];
        }
    };
    checkinReq.failureCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        [weakSelf.view makeToast:@"数据异常 请稍后再试"];
    };
}

- (void)monthDataReq{
    NSMutableDictionary *muDic = [NSMutableDictionary dictionary];
    [muDic setValue:self.month forKey:@"date"];

    QJCheckInRequest *checkinReq = [[QJCheckInRequest alloc] init];
    checkinReq.dic = muDic.copy;
    [checkinReq getCheckInMonthRequest];
    WS(weakSelf)
    checkinReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            if ([EncodeArrayFromDic(request.responseObject, @"data") isKindOfClass:[NSArray class]]) {
                weakSelf.signedModel.days = [NSArray modelArrayWithClass:[QJSignedItemDetailModel class] json:EncodeArrayFromDic(request.responseObject, @"data")].mutableCopy;
                [weakSelf.headerView updateDateModel:weakSelf.signedModel];
            }
        }else{
            NSString *msg = EncodeStringFromDic(request.responseObject, @"message");
            [weakSelf.view makeToast:msg];
        }
    };
    checkinReq.failureCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        [weakSelf.view makeToast:@"数据异常 请稍后再试"];
    };
}

#pragma mark - scrollview
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    /* 导航渐变背景色 */
    UIColor *navColor = [UIColor whiteColor];
    CGFloat alpha = MIN(1, 1 - (NavigationBar_Bottom_Y - scrollView.contentOffset.y)/NavigationBar_Bottom_Y);
    self.navView.backgroundColor = [navColor colorWithAlphaComponent:alpha];
    self.titleLabel.textColor = [UIColor colorWithWhite:0 alpha:alpha];
}
@end
