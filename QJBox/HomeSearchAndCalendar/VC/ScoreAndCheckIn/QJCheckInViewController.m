//
//  QJCheckInViewController.m
//  QJBox
//
//  Created by rui on 2022/6/27.
//

#import "QJCheckInViewController.h"

/* 表头视图 */
#import "QJCheckInHeaderView.h"
/* 自定义任务cell */
#import "QJTaskAndCoinCell.h"
/* 积分明细 */
#import "QJAccountDetailVC.h"
/* 积分商城 */
#import "QJMallShopViewController.h"
/* 签到日历 */
#import "QJCheckInCalendarDetailViewController.h"
/* 更多任务 */
#import "QJMoreTaskViewController.h"
/* 弹框View */
#import "QJScreenPopVIew.h"
/* model */
#import "QJTaskListModel.h"
/* 弹框公共 */
#import "QJCheckInPopCommonView.h"
/* 请求 */
//积分信息
#import "QJScoreRequest.h"
//签到相关
#import "QJCheckInRequest.h"
#import "QJScorePutRequset.h"
#import "QJCheckInPostRequest.h"
/* model */
#import "QJSignedModel.h"

//任务跳转页面
#import "QJMineAuthentViewController.h"
#import "QJMySpaceViewController.h"
#import "QJMineInfoViewController.h"
#import "QJCommunityViewController.h"
#import "QJEditCampsiteViewController.h"
#import "AppDelegate.h"

@interface QJCheckInViewController ()<UITableViewDelegate, UITableViewDataSource>

/* 自定义导航View */
@property (nonatomic, strong) UIView *navView;
/* 返回按钮 */
@property (nonatomic, strong) UIButton *backButton;
/* 标题 */
@property (nonatomic, strong) UILabel *titleLabel;
//contentTable
@property (nonatomic, strong) UITableView *tableView;
//数据源
@property (nonatomic, strong) NSMutableArray *dataArr;
/* 表头视图 */
@property (nonatomic, strong) QJCheckInHeaderView *headerView;
/* model */
@property (nonatomic, strong) QJSignedModel *signedModel;
@property (nonatomic, strong) QJTaskListModel *taskListModel;
@property (nonatomic, strong) NSString *qicoin;
@property (nonatomic, strong) NSString *qjcoinGot;

@end

@implementation QJCheckInViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    /* 调整自定义导航视图层次 */
    [self.view bringSubviewToFront:self.navView];
//    [self initRequest];
    [QJAppTool shareManager].taskID = @"";
}

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.title = @"奇迹有礼";
    self.navBar.hidden = YES;
    self.signedModel = [QJSignedModel new];
    self.taskListModel = [QJTaskListModel new];
    /* 加载自定义导航 */
    [self initNav];
    /* 初始化UI */
    [self initUI];
    /* 请求方法实现 */
    [self initRequest];
}

/**
 * @author: zjr
 * @date: 2022-7-13
 * @desc: 自定义导航
 */
- (void)initNav{
    self.view.backgroundColor = UIColorFromRGB(0xF5F5F5);
    self.navView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, QJScreenWidth, NavigationBar_Bottom_Y)];
    self.navView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.navView];
    
    self.backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.backButton setImage:[UIImage imageNamed:@"backImage_white"] forState:UIControlStateNormal];
    self.backButton.frame = CGRectMake(9, [QJDeviceConstant top_iPhoneX_SPACE]+27, 30, 30);
    [self.backButton addTarget:self action:@selector(backBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [self.navView addSubview:self.backButton];
    
    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 30)];
    self.titleLabel.centerY = self.backButton.centerY;
    self.titleLabel.centerX = QJScreenWidth/2;
    self.titleLabel.text = @"奇迹有礼";
    self.titleLabel.font = kboldFont(16);
    self.titleLabel.textColor = UIColorFromRGB(0xFFFFFF);
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.navView addSubview:self.titleLabel];
}

/**
 * @author: zjr
 * @date: 2022-6-27
 * @desc: 初始化UI
 */
- (void)initUI{
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(self.view).offset(NavigationBar_Bottom_Y);
        make.top.left.bottom.right.equalTo(self.view);
//        make.bottom.equalTo(self.view).offset(-Bottom_iPhoneX_SPACE);
    }];
    self.headerView = [[QJCheckInHeaderView alloc] initWithFrame:CGRectMake(0, 0, QJScreenWidth, (392+40+15)*kWScale)];
    WS(weakSelf)
    self.headerView.btnClick = ^(NSString * _Nonnull type) {
        if ([type isEqualToString:@"list"]) {
            QJAccountDetailVC *scoreDetail = [[QJAccountDetailVC alloc] init];
//            scoreDetail.qjcoinCurrent = weakSelf.qicoin.integerValue;
//            scoreDetail.qjcoinGot = weakSelf.qjcoinGot.integerValue;
            [weakSelf.navigationController pushViewController:scoreDetail animated:YES];
        }else if ([type isEqualToString:@"shop"]) {
            [weakSelf getRewardAll];
        }else if ([type isEqualToString:@"calendar"]) {
            QJCheckInCalendarDetailViewController *scoreShopVC = [[QJCheckInCalendarDetailViewController alloc] init];
            scoreShopVC.month = [NSString getStringForDateString:weakSelf.signedModel.today byFormat:@"yyyy-MM-dd" toFormat:@"yyyy-MM"];
            [weakSelf.navigationController pushViewController:scoreShopVC animated:YES];
        }else if ([type isEqualToString:@"checkIn"]) {
            [weakSelf dailySignReq];
        }
    };
    self.tableView.tableHeaderView = self.headerView;
}

#pragma mark - Lazy Loading
- (UITableView *)tableView {
    if (!_tableView) {
        self.tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        self.tableView.backgroundColor = UIColorFromRGB(0x3FB268);//[UIColor colorWithHexString:@"0xF7F7F7"];
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.tableView.showsVerticalScrollIndicator = NO;
        self.tableView.showsHorizontalScrollIndicator = NO;
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        [self.tableView registerClass:[QJTaskAndCoinCell class] forCellReuseIdentifier:@"kQJTaskAndCoinCell"];
        if (@available(iOS 11.0, *)) {
            self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
    }
    return _tableView;
}

- (NSMutableArray *)dataArr {
    if (!_dataArr) {
        self.dataArr = [NSMutableArray arrayWithCapacity:0];
    }
    return _dataArr;
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.dataArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    QjTaskModel *dataModel = [self.dataArr safeObjectAtIndex:indexPath.section];
    QJTaskAndCoinCell *cell = [tableView dequeueReusableCellWithIdentifier:@"kQJTaskAndCoinCell" forIndexPath:indexPath];
    dataModel.indexPathRow = indexPath.section;
    cell.model = dataModel;
    WS(weakSelf)
    //button点击事件
    cell.btnClick = ^(QJTaskDetailModel * _Nonnull model) {
        if (model.code == 4) {
            [QJAppTool shareManager].taskID = model.ID;
        }
//#warning TODO 任务领取
        if ([model.status isEqualToString:@"UNCOMPLETED"]) {
            [weakSelf taskModelClickAction:model];
        } else {
            [weakSelf initTaskPopView:model];
        }
    };
    cell.moreBtnClick = ^{
        [weakSelf moreBtnClick];
    };
    //内容点击事件
    cell.contentBtnClick = ^(QJTaskDetailModel * _Nonnull model) {
        if (model.code == 4) {
            [QJAppTool shareManager].taskID = model.ID;
        }
        [weakSelf initTaskPopView:model];
    };
    return cell;
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 335*kWScale;
}

/**
 * @author: zjr
 * @date: 2022-6-28
 * @desc: button点击事件
 */
- (void)moreBtnClick{
    QJMoreTaskViewController *moreTaskVC = [[QJMoreTaskViewController alloc] init];
    [self.navigationController pushViewController:moreTaskVC animated:YES];
}

- (void)backBtnAction{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)btnAction{
    QJScreenPopVIew *popView = [self.view viewWithTag:100000];
    QJScreenPopVIew *popDefaultView = [self.view viewWithTag:200000];
    [popView dismissWithBlock:nil];
    [popDefaultView dismissWithBlock:nil];
}

- (void)doneBtnAction{
    QJScreenPopVIew *popView = [self.view viewWithTag:100000];
    QJScreenPopVIew *popDefaultView = [self.view viewWithTag:200000];
    [popView dismissWithBlock:nil];
    [popDefaultView dismissWithBlock:nil];
}

/**
 * @author: zjr
 * @date: 2022-6-28
 * @desc: 每日弹框
 */
- (void)initPopView:(NSDictionary *)dataDic{
    QJCheckInPopCommonView *contentView = [[QJCheckInPopCommonView alloc] initWithFrame:CGRectMake(0, 0, 300*kWScale, 285*kWScale)];
    contentView.popViewStyle = QJPopContentViewStyleDefault;
    contentView.model = dataDic;
    contentView.directionBlock = ^(ButtonClickType direction) {
        if (direction == ButtonClickTypeClose || direction == ButtonClickTypeSure) {
            [self btnAction];
        }else{
            [self doneBtnAction];
        }
    };
    
    QJScreenPopVIew *popView = [[QJScreenPopVIew alloc] initWithCenterView:contentView];
    popView.dismissWhenClicked = YES;
    popView.popViewPopType = QJScreenPopViewPopTypeDefault;
    popView.tag = 200000;
    [popView showInView:self.view];
}

/**
 * @author: zjr
 * @date: 2022-7-5
 * @desc: 领取任务奖励
 */
- (void)initTaskPopView:(QJTaskDetailModel *)model{
    QJCheckInPopCommonView *contentView = [[QJCheckInPopCommonView alloc] initWithFrame:CGRectMake(0, 0, 300*kWScale, model.code == 7?344*kWScale:285*kWScale)];
    if ([model.status isEqualToString:@"UNRECEIVED"]) {
        contentView.popViewStyle = QJPopContentViewStyleUNRECEIVED;
    }else if ([model.status isEqualToString:@"RECEIVED"]) {
        contentView.popViewStyle = QJPopContentViewStyleRECEIVED;
    }else{
        contentView.popViewStyle = QJPopContentViewStyleUNCOMPLETED;
    }
    
    contentView.model = model;
    contentView.directionBlock = ^(ButtonClickType direction) {
        [self btnAction];
        [self taskModelClickAction:model];
    };
    QJScreenPopVIew *popView = [[QJScreenPopVIew alloc] initWithCenterView:contentView];
    popView.dismissWhenClicked = YES;
    popView.popViewPopType = QJScreenPopViewPopTypeDefault;
    popView.tag = 100000;
    [popView showInView:self.view];
}

- (void)taskModelClickAction:(QJTaskDetailModel *)model{
    if ([model.status isEqualToString:@"UNRECEIVED"]) {
        QJScorePutRequset *checkinReq = [[QJScorePutRequset alloc] init];
        checkinReq.urlStr = [NSString stringWithFormat:@"/task/%@/reward",model.ID];
        checkinReq.requestType = YTKRequestMethodPUT;
        checkinReq.serializerType = YTKRequestSerializerTypeJSON;
        [checkinReq start];
        
        WS(weakSelf)
        checkinReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
            if (ResponseSuccess) {
                [weakSelf.view makeToast:@"领取成功"];
                [weakSelf initRequest];
            }else{
                NSString *msg = EncodeStringFromDic(request.responseObject, @"message");
                [weakSelf.view makeToast:msg];
            }
        };
        checkinReq.failureCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
            [weakSelf.view makeToast:@"数据异常 请稍后再试"];
        };
    }else if ([model.status isEqualToString:@"RECEIVED"]) {
        [self initTaskPopView:model];
    }else{
        DLog(@"跳转不同的页面");
        [self pushViewControllerForType:model.code];
    }
}

- (void)pushViewControllerForType:(NSInteger )code{
    /* 任务跳转类型 1-加入营地，2-发送一条动态，3-成为攻略博主，4-浏览营地一分钟，5-更换默认头像，6-实名认证，7-连续7天签到，8-每日签到，9-完成新人礼 */
    switch (code) {
        case 1:
        {
//            [self pushCommunityIsCountDown:NO andSelectRow:1];
            QJEditCampsiteViewController *editCampsiteVC = [[QJEditCampsiteViewController alloc] init];
            editCampsiteVC.campsiteStyle = QJCampsiteViewStyleEdit;
            editCampsiteVC.isFromQJHome = YES;
            [self.navigationController pushViewController:editCampsiteVC animated:YES];
        }
            break;
        case 2:
        {
            [self pushCommunityIsCountDown:NO andSelectRow:1];
        }
            break;
        case 3:
        {
            if (![QJUserManager shareManager].isVerifyState) {
                QJMineAuthentViewController *authentVC = [[QJMineAuthentViewController alloc] init];
                authentVC.realNameBlock = ^{
                    [self allTaskReq];
                    QJMySpaceViewController *mineInfoVC = [[QJMySpaceViewController alloc] init];
                    mineInfoVC.isSelectedCreat = YES;
                    [self.navigationController pushViewController:mineInfoVC animated:YES];
                };
                [self.navigationController pushViewController:authentVC animated:YES];
            }else{
                QJMySpaceViewController *mineInfoVC = [[QJMySpaceViewController alloc] init];
                mineInfoVC.isSelectedCreat = YES;
                [self.navigationController pushViewController:mineInfoVC animated:YES];
            }
        }
            break;
        case 4:
        {
            [self pushCommunityIsCountDown:YES andSelectRow:1];
        }
            break;
        case 5:
        {
            QJMineInfoViewController *mineInfoVC = [[QJMineInfoViewController alloc] init];
//            QJMySpaceViewController *mineInfoVC = [[QJMySpaceViewController alloc] init];
            [self.navigationController pushViewController:mineInfoVC animated:YES];
        }
            break;
        case 6:
        {
            if (![QJUserManager shareManager].isVerifyState) {
                QJMineAuthentViewController *authentVC = [[QJMineAuthentViewController alloc] init];
                authentVC.realNameBlock = ^{
                    [self allTaskReq];
                };
                [self.navigationController pushViewController:authentVC animated:YES];
            }else{
                [self.view makeToast:@"已经实名认证过了"];
            }
        }
            break;
        case 7:
        {
            QJCheckInCalendarDetailViewController *scoreShopVC = [[QJCheckInCalendarDetailViewController alloc] init];
            scoreShopVC.month = [NSString getStringForDateString:self.signedModel.today byFormat:@"yyyy-MM-dd" toFormat:@"yyyy-MM"];
            [self.navigationController pushViewController:scoreShopVC animated:YES];
//            [self.view makeToast:@"连续7天签到"];
        }
            break;
        case 8:
        {
            [self.view makeToast:@"每日签到"];
        }
            break;
        case 9:
        {
            [self.view makeToast:@"完成新人礼"];
        }
            break;
        default:
            break;
    }
}

- (void)pushCommunityIsCountDown:(BOOL)isCountDown andSelectRow:(NSInteger )selectRow{
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    QJAppTabBarController *tab = (QJAppTabBarController *)delegate.window.rootViewController;
    QJNavigationController *navController = [tab.viewControllers safeObjectAtIndex:3];
    QJCommunityViewController *communityVC = (QJCommunityViewController *)navController.topViewController;
    communityVC.communitySelectRow = selectRow;
    communityVC.isCountDownTask = isCountDown;
    tab.selectedIndex = 3;
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (scrollView == self.tableView) {
        /* 导航渐变背景色 */
        UIColor *navColor = [UIColor whiteColor];
        CGFloat alpha = MIN(1, 1 - (NavigationBar_Bottom_Y - self.tableView.contentOffset.y)/NavigationBar_Bottom_Y);
        self.navView.backgroundColor = [navColor colorWithAlphaComponent:alpha];
        if (alpha >= 1) {
            self.titleLabel.textColor = [UIColor blackColor];
            [self.backButton setImage:[UIImage imageNamed:@"backimage_black"] forState:UIControlStateNormal];
        }else{
            self.titleLabel.textColor = [UIColor whiteColor];
            [self.backButton setImage:[UIImage imageNamed:@"backImage_white"] forState:UIControlStateNormal];
        }
    }
}

#pragma mark - 请求方法
- (void)initRequest{
    DLog(@"请求方法实现");
    /* 7日签到情况 */
    [self checkInSevenDaysReq];
    /* 积分信息接口 */
    [self taskInfoReq];
    /* 全部任务接口 */
    [self allTaskReq];
    
    /*
     间隔白色背景上下30
     任务 160高度
     白色背景高度=16+20+10+25+160+40
     300
     */
    [self.tableView reloadData];    
}

/**
 * @author: zjr
 * @date: 2022-7-15
 * @desc: 获取7天打卡情况
 */
- (void)checkInSevenDaysReq{
    QJCheckInRequest *checkinReq = [[QJCheckInRequest alloc] init];

    [checkinReq getCheckInSevenDaysRequest];
    WS(weakSelf)
    checkinReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            if ([EncodeDicFromDic(request.responseObject, @"data") isKindOfClass:[NSDictionary class]]) {
                weakSelf.signedModel = [QJSignedModel modelWithDictionary:EncodeDicFromDic(request.responseObject, @"data")];
                weakSelf.headerView.model = weakSelf.signedModel;
                if (weakSelf.signedModel.todaySigned == NO) {
                    [self dailySignReq];
                }
            }
        }else{
            NSString *msg = EncodeStringFromDic(request.responseObject, @"message");
            [weakSelf.view makeToast:msg];
        }
    };
    checkinReq.failureCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        [weakSelf.view makeToast:@"数据异常 请稍后再试"];
    };
}

/**
 * @author: zjr
 * @date: 2022-7-15
 * @desc: 当日打卡
 */
- (void)dailySignReq{
    QJCheckInPostRequest *checkInPostReq = [[QJCheckInPostRequest alloc] init];
    [checkInPostReq getEveryDayCheckInRequest];
    WS(weakSelf)
    checkInPostReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            if ([EncodeDicFromDic(request.responseObject, @"data") isKindOfClass:[NSDictionary class]]) {
                NSDictionary *dataDic = EncodeDicFromDic(request.responseObject, @"data");
                [weakSelf initRequest];
                [weakSelf initPopView:dataDic];
            }
        }else{
            NSString *msg = EncodeStringFromDic(request.responseObject, @"message");
            [weakSelf.view makeToast:msg];
        }
    };
    checkInPostReq.failureCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        [weakSelf.view makeToast:@"数据异常 请稍后再试"];
    };
}

/**
 * @author: zjr
 * @date: 2022-7-15
 * @desc: 积分信息
 */
- (void)taskInfoReq{
    QJScoreRequest *scoreReq = [[QJScoreRequest alloc] init];
    [scoreReq getUserScoreInfoRequest];
    WS(weakSelf)
    scoreReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            if ([EncodeDicFromDic(request.responseObject, @"data") isKindOfClass:[NSDictionary class]]) {
                NSDictionary *dataDic = EncodeDicFromDic(request.responseObject, @"data");
                weakSelf.signedModel.qicoin = EncodeStringFromDicDefEmtryValue(dataDic, @"qicoin");
                weakSelf.signedModel.qjcoinGot = EncodeStringFromDicDefEmtryValue(dataDic, @"qjcoinGot");
                weakSelf.qicoin = weakSelf.signedModel.qicoin;
                weakSelf.qjcoinGot = weakSelf.signedModel.qjcoinGot;
                weakSelf.signedModel.remainSignSupplyCount = EncodeStringFromDicDefEmtryValue(dataDic, @"remainSignSupplyCount");
                weakSelf.signedModel.unUsedSupplySignCount = EncodeStringFromDicDefEmtryValue(dataDic, @"unUsedSupplySignCount");
                weakSelf.headerView.model = weakSelf.signedModel;
                [weakSelf.headerView updateQicoin:weakSelf.signedModel];
            }
        }else{
            NSString *msg = EncodeStringFromDic(request.responseObject, @"message");
            [weakSelf.view makeToast:msg];
        }
    };
    scoreReq.failureCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        [weakSelf.view makeToast:@"数据异常 请稍后再试"];
    };
}

/**
 * @author: zjr
 * @date: 2022-7-15
 * @desc: 全部任务
 */
- (void)allTaskReq{
    [self.dataArr removeAllObjects];
    QJScoreRequest *scoreReq = [[QJScoreRequest alloc] init];
    [scoreReq getTaskListRequest];
    WS(weakSelf)
    scoreReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            if ([EncodeDicFromDic(request.responseObject, @"data") isKindOfClass:[NSDictionary class]]) {
                weakSelf.taskListModel = [QJTaskListModel modelWithDictionary:EncodeDicFromDic(request.responseObject, @"data")];
                if (weakSelf.taskListModel.qjcoinTask) {
                    [weakSelf.dataArr addObject:weakSelf.taskListModel.qjcoinTask];
                }
                if (weakSelf.taskListModel.experienceTask) {
                    [weakSelf.dataArr addObject:weakSelf.taskListModel.experienceTask];
                }
            }
            [weakSelf.tableView reloadData];
        }else{
            NSString *msg = EncodeStringFromDic(request.responseObject, @"message");
            [weakSelf.view makeToast:msg];
        }
    };
    scoreReq.failureCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        [weakSelf.view makeToast:@"数据异常 请稍后再试"];
    };
}

/**
 * @author: zjr
 * @date: 2022-7-15
 * @desc: 一键领币
 */
- (void)getRewardAll{
    QJScorePutRequset *checkinReq = [[QJScorePutRequset alloc] init];
    checkinReq.urlStr = @"/task/reward_all";
    checkinReq.requestType = YTKRequestMethodPUT;
    checkinReq.serializerType = YTKRequestSerializerTypeJSON;
    [checkinReq start];
    
    WS(weakSelf)
    checkinReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            if ([EncodeDicFromDic(request.responseObject, @"data") isKindOfClass:[NSDictionary class]]) {
                NSDictionary *dataDic = EncodeDicFromDic(request.responseObject, @"data");
                NSString *qjconinAfterStr = EncodeStringFromDicDefEmtryValue(dataDic, @"qjconinAfter");
                NSString *qjconinBefore = EncodeStringFromDicDefEmtryValue(dataDic, @"qjconinBefore");
                NSString *qjconinGet = EncodeStringFromDicDefEmtryValue(dataDic, @"qjconinGet");
                if ([qjconinGet isEqualToString:@"0"]) {
                    [self.view makeToast:@"暂无可领取的奇迹币"];
                }else{
                    [weakSelf allTaskReq];
                }
                [weakSelf.headerView updateQJiconinAnimation:qjconinBefore.floatValue andTo:qjconinAfterStr.floatValue];
            }
        }else{
            NSString *msg = EncodeStringFromDic(request.responseObject, @"message");
            [weakSelf.view makeToast:msg];
        }
    };
    checkinReq.failureCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        [weakSelf.view makeToast:@"数据异常 请稍后再试"];
    };
}

@end
