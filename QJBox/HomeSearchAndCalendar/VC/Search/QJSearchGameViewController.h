//
//  QJSearchGameViewController.h
//  QJBox
//
//  Created by rui on 2022/7/5.
//

#import "QJBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJSearchGameViewController : QJBaseViewController

@property (nonatomic, assign) BOOL isFromGameCenter;

@end

NS_ASSUME_NONNULL_END
