//
//  QJSearchResultViewController.m
//  QJBox
//
//  Created by rui on 2022/7/5.
//

#import "QJSearchResultViewController.h"
/* 排序搜索 */
#import "QJSearchSortView.h"
#import "QJSearchTagSortView.h"
/* 搜索索引 */
#import "QJHomeSearchContentView.h"
/* model */
#import "QJSearchSortModel.h"
#import "QJSearchGameListModel.h"
/* 自定义任务cell */
#import "QJGameCommonCell.h"
/* 请求 */
#import "QJSearchGameRequest.h"
#import "QJSearchGamePostRequest.h"
/* 游戏详情 */
#import "QJProductDetailViewController.h"
#import "AppDelegate.h"

@interface QJSearchResultViewController ()<UITextFieldDelegate,UITableViewDelegate, UITableViewDataSource>

/* 自定义导航View */
@property (nonatomic, strong) UIView *navView;
/* 返回按钮 */
@property (nonatomic, strong) UIButton *backButton;
/* 标题 */
@property (nonatomic, strong) UILabel *titleLabel;
/* 搜索 */
@property (nonatomic, strong) UITextField *searchTF;
/* 筛选父视图 */
@property (nonatomic, strong) UIView *filterView;
/* 整体筛选 */
@property (nonatomic, strong) UIButton *sortButton;
/* 总数量 */
@property (nonatomic, strong) UILabel *countLabel;
/* 标签筛选 */
@property (nonatomic, strong) UIButton *tagSortButton;
/* 筛选view */
@property (nonatomic,strong) QJSearchSortView *sortView;
/* 标签筛选view */
@property (nonatomic, strong) QJSearchTagSortView *sortTagView;
//数据源
@property (nonatomic, strong) NSMutableArray *gameDataArr;
@property (nonatomic, strong) QJSearchGameListModel *pageModel;
/* 游戏标签数据 */
@property (nonatomic, strong) QJSearchTagDetailModel *searchOptionsModel;
@property (nonatomic, strong) QJSearchSortModel *dataModel;
/* 搜索索引view */
@property (nonatomic, strong) QJHomeSearchContentView *searchContenView;
/* 用户输入次数，用来控制延迟搜索请求 */
@property (nonatomic, assign) NSInteger inputCount;
/* tableView */
@property (nonatomic, strong) UITableView *tableView;
/* 请求方法 */
@property (nonatomic, strong) QJSearchGameRequest *searchSuggestReq;
@property (nonatomic, strong) QJSearchGamePostRequest *searchGameReq;
/* 搜索游戏请求key */
@property (nonatomic, strong) NSString *orderKey;
@property (nonatomic, strong) NSDictionary *optionDic;

@end

@implementation QJSearchResultViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navBar.hidden = YES;
    self.searchOptionsModel = [QJSearchTagDetailModel new];
    self.pageModel = [QJSearchGameListModel new];
    /* 初始化导航 */
    [self initNav];
    /* 初始化UI */
    [self initUI];
    [self initSortView];
    /* 请求方法实现 */
    /* 获取标签 */
    [self sendSearchOptionRequest];
    /* 获取搜索数据 */
    [self sendRequest];
}

/**
 * @author: zjr
 * @date: 2022-7-5
 * @desc: 初始化UI
 */
- (void)initUI{
    [self.filterView addSubview:self.sortButton];
    [self.filterView addSubview:self.tagSortButton];
    [self.filterView addSubview:self.countLabel];
    
    [self.sortButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.filterView).offset(15);
        make.top.equalTo(self.filterView).offset(7);
        make.height.mas_equalTo(30);
    }];
    [_sortButton layoutWithStatus:QJLayoutStatusImageRight andMargin:5];

    [self.countLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.sortButton.mas_right).offset(20);
        make.centerY.equalTo(self.sortButton);
        make.height.mas_equalTo(30);
    }];
    
    [self.tagSortButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.filterView).offset(7);
        make.right.equalTo(self.filterView).offset(-15);
        make.height.mas_equalTo(30);
    }];
    [_tagSortButton layoutWithStatus:QJLayoutStatusImageRight andMargin:5];

    /* 添加tableview */
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(NavigationBar_Bottom_Y+44);
        make.left.bottom.right.equalTo(self.view);
    }];
    
    [self.view addSubview:self.searchContenView];
    
    /* 搜索view回调 */
    WS(weakSelf)
    _searchContenView.scrollViewWillBeginDraggingBlock = ^{
        [weakSelf.searchTF resignFirstResponder];
    };
    _searchContenView.cellSelectBlock = ^(id  _Nonnull item) {
        DLog(@"点击cell");
        QJSearchTagDetailModel *model = (QJSearchTagDetailModel *)item;
        weakSelf.searchKey = model.keyword;
        weakSelf.searchTF.text = model.keyword;
        [weakSelf refreshData];
        weakSelf.searchContenView.hidden = YES;
    };
}

/**
 * @author: zjr
 * @date: 2022-7-5
 * @desc: 初始化导航
 */
- (void)initNav{
    self.view.backgroundColor = UIColorFromRGB(0xFFFFFF);
    self.navView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, QJScreenWidth, NavigationBar_Bottom_Y+44)];
    self.navView.backgroundColor = UIColorFromRGB(0xFFFFFF);
    [self.view addSubview:self.navView];
    
    /* 搜搜输入框 */
    self.searchTF = [[UITextField alloc] initWithFrame:CGRectMake(15, NavigationBar_Bottom_Y+7, QJScreenWidth-30, 30)];
    [self.navView addSubview:self.searchTF];
    [self.searchTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.navView).offset(15);
        make.top.equalTo(self.navView).offset([QJDeviceConstant sysStatusBarHeight]+4);
        make.bottom.equalTo(self.navView).offset(-49);
        make.right.equalTo(self.navView).offset(-74);
    }];
    [self.searchTF layoutIfNeeded];
    self.searchTF.layer.cornerRadius = self.searchTF.height/2;
    
    self.searchTF.backgroundColor = [UIColor colorWithRed:0.842 green:0.842 blue:0.842 alpha:0.19];
    self.searchTF.textColor = UIColorFromRGB(0x474849);
    self.searchTF.font = kFont(14);
    NSMutableAttributedString *placeholderString = [[NSMutableAttributedString alloc] initWithString:@"输入关键词搜索游戏" attributes:@{NSForegroundColorAttributeName : UIColorFromRGB(0xC8CACC), NSFontAttributeName : kFont(14)}];
    self.searchTF.attributedPlaceholder = placeholderString;
    if (self.searchKey) {
        self.searchTF.text = self.searchKey;
    }

    UIView *searchView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 34, 30)];
    UIImageView *searchImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_search"]];
    searchImage.frame = CGRectMake(14, 7, 16, 16);
    [searchView addSubview:searchImage];
    self.searchTF.leftView = searchView;
    self.searchTF.leftViewMode = UITextFieldViewModeAlways;
    self.searchTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.searchTF.delegate = self;
    [self.searchTF addTarget:self action:@selector(textFeildChange:) forControlEvents:UIControlEventEditingChanged];
    self.searchTF.returnKeyType = UIReturnKeySearch;

    UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.navView addSubview:cancelBtn];
    [cancelBtn setTitle:@"取消" forState:UIControlStateNormal];
    cancelBtn.titleLabel.font = kFont(14);
    [cancelBtn setTitleColor:UIColorFromRGB(0x333333) forState:UIControlStateNormal];
    [cancelBtn addTarget:self action:@selector(backBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.searchTF.mas_right);
        make.right.mas_equalTo(self.navView);
        make.bottom.equalTo(self.navView).offset(-50);
        make.height.mas_equalTo(30);
    }];
    
    self.filterView = [[UIView alloc] initWithFrame:CGRectMake(0, NavigationBar_Bottom_Y, QJScreenWidth, 44)];
    [self.navView addSubview:self.filterView];
}

/**
 * @author: zjr
 * @date: 2022-7-5
 * @desc: button点击事件
 */
- (void)backBtnAction{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)sortBtnClick{
    if (self.sortView.hidden == YES) {
        self.sortView.hidden = NO;
        [self.view bringSubviewToFront:self.sortView];
    }else{
        self.sortView.hidden = YES;
    }
}

- (void)tagSortBtnClick{
    self.sortView.hidden = YES;
    if (self.sortTagView) {
        [[UIApplication sharedApplication].keyWindow addSubview:_sortTagView];
        WS(weakSelf)
        _sortTagView.filterSelectBlock = ^(NSMutableDictionary * _Nonnull dic, QJSearchSortModel * _Nonnull filterModel) {
            DLog(@"选中的dic = %@",dic);
            weakSelf.optionDic = dic;
            [weakSelf resetSortTagButton];
            [weakSelf refreshData];
        };
    }
    [_sortTagView showFilterWithModel:self.dataModel];
    [_sortTagView showChooseFilterWithModel:[self.optionDic mutableCopy]];
}

#pragma mark - Lazy Loading
- (UIButton *)sortButton {
    if (!_sortButton) {
        _sortButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _sortButton.titleLabel.font = kFont(14);
        [_sortButton setTitleColor:[UIColor colorWithHexString:@"0x000000"] forState:UIControlStateNormal];
        [_sortButton setTitle:@"综合排序" forState:UIControlStateNormal];
        [_sortButton addTarget:self action:@selector(sortBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [_sortButton setImage:[UIImage imageNamed:@"iconDownOne"] forState:UIControlStateNormal];
        [_sortButton layoutWithStatus:QJLayoutStatusImageRight andMargin:5];
    }
    return _sortButton;
}

- (UIButton *)tagSortButton {
    if (!_tagSortButton) {
        _tagSortButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _tagSortButton.titleLabel.font = kFont(14);
        [_tagSortButton setTitleColor:[UIColor colorWithHexString:@"0x919599"] forState:UIControlStateNormal];
        [_tagSortButton setTitleColor:[UIColor colorWithHexString:@"0xFF9500"] forState:UIControlStateSelected];
        [_tagSortButton setTitle:@"筛选" forState:UIControlStateNormal];
        [_tagSortButton addTarget:self action:@selector(tagSortBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [_tagSortButton setImage:[UIImage imageNamed:@"search_sorttag"] forState:UIControlStateNormal];
        [_tagSortButton setImage:[UIImage imageNamed:@"search_sorttag_select"] forState:UIControlStateSelected];
        [_tagSortButton layoutWithStatus:QJLayoutStatusImageRight andMargin:5];
    }
    return _tagSortButton;
}

- (UILabel *)countLabel{
    if (!_countLabel) {
        _countLabel = [UILabel new];
        _countLabel.font = kFont(12);
        _countLabel.textColor = [UIColor colorWithHexString:@"0x919599"];
    }
    return _countLabel;
}

- (QJSearchTagSortView *)sortTagView{
    if (!_sortTagView) {
        _sortTagView = [[QJSearchTagSortView alloc] initWithFrame:CGRectMake(0, 0, QJScreenWidth, QJScreenHeight)];
    }
    return _sortTagView;
}

- (NSMutableArray *)gameDataArr {
    if (!_gameDataArr) {
        self.gameDataArr = [NSMutableArray arrayWithCapacity:0];
    }
    return _gameDataArr;
}

-(QJHomeSearchContentView *)searchContenView{
    if (!_searchContenView) {
        _searchContenView = [[QJHomeSearchContentView alloc] initWithFrame:CGRectMake(0, NavigationBar_Bottom_Y, QJScreenWidth, QJScreenHeight - (NavigationBar_Bottom_Y))];
        _searchContenView.hidden = YES;
    }
    return _searchContenView;
}

-(QJSearchGameRequest *)searchSuggestReq {
    if (!_searchSuggestReq) {
        _searchSuggestReq = [QJSearchGameRequest new];
    }
    return _searchSuggestReq;
}

- (QJSearchGamePostRequest *)searchGameReq{
    if (!_searchGameReq) {
        _searchGameReq = [QJSearchGamePostRequest new];
    }
    return _searchGameReq;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _tableView.backgroundColor = [UIColor colorWithHexString:@"0xFFFFFF"];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.showsHorizontalScrollIndicator = NO;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        [_tableView registerClass:[QJGameCommonCell class] forCellReuseIdentifier:@"QJGameCommonCell"];
        if (@available(iOS 11.0, *)) {
            _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
        _tableView.mj_header = [QJRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(refreshData)];
        _tableView.mj_footer = [QJRefreshFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreHomeData)];
    }
    return _tableView;
}

- (void)initSortView{
    if (!_sortView) {
        _sortView = [[QJSearchSortView alloc] initWithFrame:CGRectMake(0, 0, QJScreenWidth, QJScreenHeight-NavigationBar_Bottom_Y)];
        WS(weakSelf)
        _sortView.itemSelectBlock = ^(QJSearchTagDetailModel * _Nonnull model) {
            weakSelf.orderKey = model.ID;
            [weakSelf.searchOptionsModel.orderOptions enumerateObjectsUsingBlock:^(QJSearchTagDetailModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                obj.isSelect = NO;
            }];
            model.isSelect = YES;
            [weakSelf.sortButton setTitle:model.name forState:UIControlStateNormal];
            weakSelf.sortView.sortTags = weakSelf.searchOptionsModel.orderOptions;
            [weakSelf refreshData];
        };
    }
    
    [self.view addSubview:self.sortView];
    self.sortView.hidden = YES;
    
    [self.sortView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.sortButton.mas_bottom).offset(-5);
        make.bottom.equalTo(self.view);
        make.left.right.equalTo(self.view);
    }];
}

#pragma mark -- UITextFieldDelegate
- (void)textFeildChange:(UITextField *)textFeild {
    if (textFeild.markedTextRange == nil) {
        NSString *toBeString = textFeild.text;
        NSInteger kMaxLength = 10;
        if (toBeString.length > kMaxLength) {
            textFeild.text = [toBeString substringToIndex:kMaxLength];
        }
        if (self.searchKey.hash == textFeild.text.hash) {
            return;
        }
        self.searchKey = textFeild.text;
        
        self.inputCount ++;
        [self performSelector:@selector(requestKeyWorld:) withObject:@(self.inputCount) afterDelay:0.6f];
    }
}

- (void)requestKeyWorld:(NSNumber *)count {
    if (self.inputCount == [count integerValue]) {
        //执行网络请求
        if (self.searchKey.length > 0) {
            NSCharacterSet *set = [NSCharacterSet whitespaceAndNewlineCharacterSet];
            NSString *trimedString = [self.searchKey stringByTrimmingCharactersInSet:set];
            if ([trimedString length] == 0) {
                _searchContenView.hidden = YES;
                return;
            }
            [_searchContenView searchWithKey:self.searchKey];
            _searchContenView.hidden = NO;
        }else{
            _searchContenView.hidden = YES;
        }
    }
}

- (BOOL)textFieldShouldClear:(UITextField *)textField{
    _searchContenView.hidden = YES;
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    NSString * text = textField.text;
    self.searchKey = textField.text;
    NSCharacterSet *set = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    NSString *trimedString = [text stringByTrimmingCharactersInSet:set];

    [self refreshData];
    _searchContenView.hidden = YES;

    return YES;
}

#pragma mark - UITableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    QJEmptyCommonView *emptyView = [QJEmptyCommonView new];
    emptyView.string = @"暂无搜索结果";
    emptyView.emptyImage = @"empty_no_search";
//    emptyView.buttonTextColor = UIColorFromRGB(0x919599);
//    emptyView.buttonBgColor = UIColorFromRGB(0xF8F8F8);
    emptyView.buttonString = @"查看所有游戏";
    emptyView.topSpace = 100;
//    emptyView.buttonRadius = 5;
    emptyView.emptyClick = ^{
        [self goToGameCenter];
    };
    [tableView tableViewDisplayWhenHaveNoDataWithView:emptyView ifNecessaryForRowCount:self.pageModel.records.count];
    return self.pageModel.records.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    QJSearchGameDetailModel *model = [self.pageModel.records safeObjectAtIndex:indexPath.row];
    QJGameCommonCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QJGameCommonCell" forIndexPath:indexPath];
    cell.model = model;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 84;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.001;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (self.pageModel.hasNext == NO && self.pageModel.records.count > 0) {
        return 68;
    }
    return 0.001;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return [UIView new];
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if (self.pageModel.hasNext == NO && self.pageModel.records.count > 0 && self.isFromGameCenter == NO) {
        UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, QJScreenWidth, 64)];
        footerView.backgroundColor = [UIColor whiteColor];
        
        UIButton *lookAllButton = [UIButton buttonWithType:UIButtonTypeCustom];
//        lookAllButton.backgroundColor = UIColorFromRGB(0xF8F8F8);
        [lookAllButton setTitleColor:UIColorFromRGB(0x1F2A4D) forState:UIControlStateNormal];
        [lookAllButton setTitle:@"查看所有游戏" forState:UIControlStateNormal];
//        lookAllButton.layer.cornerRadius = 8;
        lookAllButton.titleLabel.font = kFont(14);
        [lookAllButton addTarget:self action:@selector(goToGameCenter) forControlEvents:UIControlEventTouchUpInside];
        [footerView addSubview:lookAllButton];
        [lookAllButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(footerView);
            make.top.equalTo(footerView).offset(25);
            make.width.equalTo(@175);
            make.height.equalTo(@38);
        }];
        
        return footerView;
    }
    return nil;
}

- (void)goToGameCenter{
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    QJAppTabBarController *tab = (QJAppTabBarController *)delegate.window.rootViewController;
    tab.selectedIndex = 1;
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    QJSearchGameDetailModel *model = [self.pageModel.records safeObjectAtIndex:indexPath.row];
    QJProductDetailViewController *detailVC = [[QJProductDetailViewController alloc] init];
    detailVC.model = model;
    [self.navigationController pushViewController:detailVC animated:YES];
}

#pragma mark - 请求方法
- (void)refreshData {
    _pageModel.willLoadMore = NO;
    [self sendRequest];
}

- (void)loadMoreHomeData {
    if(_pageModel.hasNext){
        _pageModel.willLoadMore=YES;
        [self sendRequest];
    }else{
        [self.tableView.mj_footer endRefreshing];
        [self.tableView.mj_footer endRefreshingWithNoMoreData];
    }
}

/**
 * @author: zjr
 * @date: 2022-7-7
 * @desc: 搜索游戏
 */
- (void)sendRequest{
    NSMutableDictionary *muDic = [NSMutableDictionary dictionaryWithDictionary:_pageModel.toParams];
    [muDic setValue:self.searchKey forKey:@"keyword"];
    [muDic setValue:self.orderKey forKey:@"order"];
    [muDic setValue:self.optionDic forKey:@"option"];
    self.searchGameReq.dic = muDic.copy;
    [self.searchGameReq getSearchGameRequest];
    WS(weakSelf)
    self.searchGameReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            [weakSelf.pageModel configObj:[QJSearchGameListModel modelWithDictionary:EncodeDicFromDic(request.responseObject, @"data")]];
            weakSelf.countLabel.text = [NSString stringWithFormat:@"共%@个结果",weakSelf.pageModel.total];
        }else{
            NSString *msg = EncodeStringFromDic(request.responseObject, @"message");
            [weakSelf.view makeToast:msg];
        }
        [weakSelf.tableView reloadData];
        [weakSelf.tableView.mj_header endRefreshing];
        [weakSelf.tableView.mj_footer endRefreshing];
    };
    self.searchGameReq.failureCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        [weakSelf.view makeToast:@"数据异常 请稍后再试"];
        [weakSelf.tableView.mj_header endRefreshing];
        [weakSelf.tableView.mj_footer endRefreshing];
        [weakSelf.tableView reloadData];
    };
}

/**
 * @author: zjr
 * @date: 2022-7-7
 * @desc: 获取游戏标签，筛选类型数据
 */
- (void)sendSearchOptionRequest{
    self.dataModel = [QJSearchSortModel new];

    [self.searchSuggestReq getSearchOptionsRequest];
    WS(weakSelf)
    self.searchSuggestReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            if ([EncodeDicFromDic(request.responseObject, @"data") isKindOfClass:[NSDictionary class]]) {
                weakSelf.searchOptionsModel = [QJSearchTagDetailModel modelWithDictionary:EncodeDicFromDic(request.responseObject, @"data")];
                /* 默认选中第一个 */
                QJSearchTagDetailModel *model = [weakSelf.searchOptionsModel.orderOptions firstObject];
                model.isSelect = YES;
                
                /* 排序赋值 */
                if (weakSelf.searchOptionsModel.orderOptions) {
                    weakSelf.sortView.sortTags = weakSelf.searchOptionsModel.orderOptions;
                }else{
                    QJSearchTagDetailModel *model = [QJSearchTagDetailModel new];
                    model.name = @"综合排序";
                    model.isSelect = YES;
                    
                    QJSearchTagDetailModel *model1 = [QJSearchTagDetailModel new];
                    model1.name = @"下载最多";
                    model1.isSelect = NO;
                    weakSelf.sortView.sortTags = @[model,model1].mutableCopy;
                }
                
                weakSelf.dataModel.types = weakSelf.searchOptionsModel.filterOptions;
            }
        }else{
            NSString *msg = EncodeStringFromDic(request.responseObject, @"message");
            [weakSelf.view makeToast:msg];
        }
        
    };
    self.searchSuggestReq.failureCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        [weakSelf.view makeToast:@"数据异常 请稍后再试"];
    };
}

- (void)resetSortTagButton {
    __block BOOL isChoose = NO;
    [[self.optionDic allValues] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSMutableArray *temp = (NSMutableArray *)obj;
        if (temp.count > 0) {
            isChoose = YES;
        }
    }];
    if (isChoose) {
        self.tagSortButton.selected = YES;
    }else{
        self.tagSortButton.selected = NO;
    }
}

- (void)dealloc{
    [self.sortView removeFromSuperview];
    [self.sortTagView removeFromSuperview];
}

#pragma mark - UIScrollView
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    self.sortView.hidden = YES;
}

@end
