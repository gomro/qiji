//
//  QJSearchGameViewController.m
//  QJBox
//
//  Created by rui on 2022/7/5.
//

#import "QJSearchGameViewController.h"
/* 搜索索引 */
#import "QJHomeSearchContentView.h"
/* 默认view */
#import "QJHomeSearchContentDefaultView.h"
/* 搜索结果页 */
#import "QJSearchResultViewController.h"
/* 请求 */
#import "QJSearchGameRequest.h"

@interface QJSearchGameViewController ()<UITextFieldDelegate>{
    BOOL _isFirstLoad;
    BOOL _isAnimation;
}

/* 自定义导航View */
@property (nonatomic, strong) UIView *navView;
/* 搜索 */
@property (nonatomic, strong) UIView *searchView;
@property (nonatomic, strong) UITextField *searchTF;
@property (nonatomic, strong) UIButton *cancelBtn;
/* 搜索key */
@property (nonatomic, copy) NSString *searchKey;
/* 搜索索引view */
@property (nonatomic, strong) QJHomeSearchContentView *searchContenView;
/* mornview */
@property (nonatomic, strong) QJHomeSearchContentDefaultView *searchDeaultView;
/* 用户输入次数，用来控制延迟搜索请求 */
@property (nonatomic, assign) NSInteger inputCount;
/* 请求方法 */
@property (nonatomic, strong) QJSearchGameRequest *searchSuggestReq;
@property (nonatomic, strong) QJSearchSortModel *searchHotKeyModel;

@end

@implementation QJSearchGameViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self initHistoryData];
    [self.searchTF becomeFirstResponder];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.searchTF resignFirstResponder];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navBar.hidden = YES;
    _isFirstLoad = NO;
    _isAnimation = NO;
    self.searchHotKeyModel = [QJSearchSortModel new];
    /* 初始化导航 */
    [self initNav];
    /* 初始化UI */
    [self initUI];
    /* 加载搜索记录 */
    [self sendRequest];
}

/**
 * @author: zjr
 * @date: 2022-7-5
 * @desc: 初始化UI
 */
- (void)initUI{
    [self.view addSubview:self.searchDeaultView];
    [self.view addSubview:self.searchContenView];
    
    /* 默认页面-首页事件回调 */
    WS(weakSelf)
    _searchDeaultView.scrollViewWillBeginDraggingBlock = ^{
        [weakSelf.searchTF resignFirstResponder];
    };
    _searchDeaultView.cellSelectBlock = ^(id  _Nonnull item) {
        QJSearchTagDetailModel *model = (QJSearchTagDetailModel *)item;
        QJSearchResultViewController *searchResultVC = [[QJSearchResultViewController alloc] init];
        searchResultVC.searchKey = model.name;
        searchResultVC.isFromGameCenter = self.isFromGameCenter;
        [weakSelf.navigationController pushViewController:searchResultVC animated:YES];
    };
    
    /* 搜索索引页面-事件回调 */
    _searchContenView.scrollViewWillBeginDraggingBlock = ^{
        [weakSelf.searchTF resignFirstResponder];
    };
    _searchContenView.cellSelectBlock = ^(id  _Nonnull item) {
        QJSearchTagDetailModel *model = (QJSearchTagDetailModel *)item;
        QJSearchResultViewController *searchResultVC = [[QJSearchResultViewController alloc] init];
        searchResultVC.searchKey = model.keyword;
        searchResultVC.isFromGameCenter = self.isFromGameCenter;
        [weakSelf.navigationController pushViewController:searchResultVC animated:YES];
    };
    
    /* 调整坐标 */
    [self resetNavHeight];
}

/**
 * @author: zjr
 * @date: 2022-7-5
 * @desc: 初始化导航
 */
- (void)initNav{
    self.view.backgroundColor = UIColorFromRGB(0xFFFFFF);
    self.navView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, QJScreenWidth, NavigationBar_Bottom_Y)];
    self.navView.backgroundColor = UIColorFromRGB(0xFFFFFF);
    [self.view addSubview:self.navView];
    
    [self.navView addSubview:self.searchView];
    
    /* searchView */
    self.searchTF = [[UITextField alloc] initWithFrame:CGRectMake(15, NavigationBar_Bottom_Y+15, QJScreenWidth-30, 32)];
    [self.searchView addSubview:self.searchTF];
    [self.searchTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.searchView).offset(15);
        make.top.equalTo(self.searchView).offset(4);
        make.bottom.equalTo(self.searchView).offset(-5);
        make.right.equalTo(self.searchView).offset(-74);
    }];
    [self.searchTF layoutIfNeeded];
    self.searchTF.layer.cornerRadius = self.searchTF.height/2;
    
    self.searchTF.backgroundColor = [UIColor colorWithRed:0.842 green:0.842 blue:0.842 alpha:0.19];
    self.searchTF.textColor = UIColorFromRGB(0x474849);
    self.searchTF.font = kFont(14);

    NSMutableAttributedString *placeholderString = [[NSMutableAttributedString alloc] initWithString:@"输入关键词搜索游戏" attributes:@{NSForegroundColorAttributeName : UIColorFromRGB(0xC8CACC), NSFontAttributeName : kFont(14)}];
    self.searchTF.attributedPlaceholder = placeholderString;

    UIView *searchIconView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 34, 30)];
    UIImageView *searchImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_search"]];
    searchImage.frame = CGRectMake(14, 7, 16, 16);
    [searchIconView addSubview:searchImage];
    self.searchTF.leftView = searchIconView;
    self.searchTF.leftViewMode = UITextFieldViewModeAlways;
    self.searchTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.searchTF.delegate = self;
    [self.searchTF addTarget:self action:@selector(textFeildChange:) forControlEvents:UIControlEventEditingChanged];
    self.searchTF.returnKeyType = UIReturnKeySearch;
    
    self.cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.searchView addSubview:self.cancelBtn];
    [self.cancelBtn setTitle:@"取消" forState:UIControlStateNormal];
    self.cancelBtn.titleLabel.font = kFont(14);
    [self.cancelBtn setTitleColor:UIColorFromRGB(0x101010) forState:UIControlStateNormal];
    [self.cancelBtn addTarget:self action:@selector(backBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [self.cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.searchTF.mas_right);
        make.right.mas_equalTo(self.searchView);
        make.bottom.equalTo(self.searchView).offset(-6);
        make.height.mas_equalTo(30);
    }];
}

/**
 * @author: zjr
 * @date: 2022-7-5
 * @desc: button点击事件
 */
- (void)backBtnAction{
    [self resignBtnAction];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)resignBtnAction{
    self.searchTF.text = @"";
    _searchContenView.hidden = YES;
    [self.searchTF resignFirstResponder];
}

#pragma mark -- UITextFieldDelegate
- (void)textFeildChange:(UITextField *)textFeild {
    if (textFeild.markedTextRange == nil) {
        NSString *toBeString = textFeild.text;
        NSInteger kMaxLength = 10;
        if (toBeString.length > kMaxLength) {
            textFeild.text = [toBeString substringToIndex:kMaxLength];
        }
        if (self.searchKey.hash == textFeild.text.hash) {
            return;
        }
        self.searchKey = textFeild.text;
        
        self.inputCount ++;
        [self performSelector:@selector(requestKeyWorld:) withObject:@(self.inputCount) afterDelay:0.6f];
    }
}

- (void)requestKeyWorld:(NSNumber *)count {
    if (self.inputCount == [count integerValue]) {
        //执行网络请求
        if (self.searchKey.length > 0) {
            NSCharacterSet *set = [NSCharacterSet whitespaceAndNewlineCharacterSet];
            NSString *trimedString = [self.searchKey stringByTrimmingCharactersInSet:set];
            if ([trimedString length] == 0) {
                _searchContenView.hidden = YES;
                return;
            }
            [_searchContenView searchWithKey:self.searchKey];
            _searchContenView.hidden = NO;
        }else{
            _searchContenView.hidden = YES;
        }
    }
}

- (BOOL)textFieldShouldClear:(UITextField *)textField{
    _searchContenView.hidden = YES;
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    NSString * text = textField.text;
    self.searchKey = textField.text;
    NSCharacterSet *set = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    NSString *trimedString = [text stringByTrimmingCharactersInSet:set];
//    if ([trimedString length] == 0) {
//        [self.view makeToast:@"请输入内容"];
//        return NO;
//    }
        
    QJSearchResultViewController *searchResultVC = [[QJSearchResultViewController alloc] init];
    searchResultVC.searchKey = self.searchKey;
    searchResultVC.isFromGameCenter = self.isFromGameCenter;
    [self.navigationController pushViewController:searchResultVC animated:YES];
        
//    if (self.searchKey.length > 0) {
//        [_searchContenView searchWithKey:self.searchKey];
//        _searchContenView.hidden = NO;
//    }else{
//        _searchContenView.hidden = YES;
//    }

    if (trimedString.length > 0) {
        NSArray *historyArray = [[NSUserDefaults standardUserDefaults] valueForKey:@"historySearch"];
        NSMutableArray *historyArrayNew = [NSMutableArray array];
        if (!historyArray) {
            NSMutableArray *listArray = [NSMutableArray array];
            [listArray safeAddObject:text];
            historyArrayNew = listArray;
        }else{
            NSMutableArray *listArray = [historyArray mutableCopy];
            if (listArray.count > 10) {
                [listArray removeFirstObject];
            }
            [listArray safeAddObject:text];
            for (NSString *text in listArray) {
                if (![historyArrayNew containsObject:text]) {
                    [historyArrayNew safeAddObject:text];
                }
            }
        }
        
        [[NSUserDefaults standardUserDefaults] setValue:historyArrayNew forKey:@"historySearch"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }

    return YES;
}

#pragma mark -------- Lazy Loading
-(QJHomeSearchContentView *)searchContenView{
    if (!_searchContenView) {
        _searchContenView = [[QJHomeSearchContentView alloc] initWithFrame:CGRectMake(0, NavigationBar_Bottom_Y, QJScreenWidth, QJScreenHeight - (NavigationBar_Bottom_Y))];
        _searchContenView.hidden = YES;
    }
    return _searchContenView;
}

-(QJHomeSearchContentDefaultView *)searchDeaultView{
    if (!_searchDeaultView) {
        _searchDeaultView = [[QJHomeSearchContentDefaultView alloc] initWithFrame:CGRectMake(0, NavigationBar_Bottom_Y, QJScreenWidth, QJScreenHeight - (NavigationBar_Bottom_Y))];
    }
    return _searchDeaultView;
}

- (UIView *)searchView{
    if (!_searchView) {
        _searchView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, QJScreenWidth, 44)];
        _searchView.backgroundColor = [UIColor whiteColor];
    }
    return _searchView;
}

-(QJSearchGameRequest *)searchSuggestReq {
    if (!_searchSuggestReq) {
        _searchSuggestReq = [QJSearchGameRequest new];
    }
    return _searchSuggestReq;
}
 
#pragma mark -- Request
/**
 * @author: zjr
 * @date: 2022-7-7
 * @desc: 获取搜索首页历史标签数据
 */
- (void)initHistoryData{
    NSMutableArray *modelHeaderArray = [NSMutableArray array];
    NSMutableArray *historyArray = [[NSUserDefaults standardUserDefaults] valueForKey:@"historySearch"];
    if (historyArray && historyArray.count > 0) {
        QJSearchTagDetailModel *model = [QJSearchTagDetailModel new];
        model.name = @"最近搜索";
        model.multiple = YES;
        model.list = [NSMutableArray array];
        NSMutableArray *listArray = [historyArray mutableCopy];
        for (NSString *text in listArray) {
            QJSearchTagDetailModel *listModel = [QJSearchTagDetailModel new];
            listModel.name = text;
            [model.list safeAddObject:listModel];
        }
        [modelHeaderArray safeAddObject:model];
    }
        
    QJSearchSortModel *dataHeaderModel = [QJSearchSortModel new];
    dataHeaderModel.types = modelHeaderArray;
    [_searchDeaultView showFilterHistoryWithModel:dataHeaderModel];
}

/**
 * @author: zjr
 * @date: 2022-7-7
 * @desc: 获取搜索首页热门标签数据
 */
- (void)sendRequest{
    [self.searchSuggestReq getSearchHotkeywordsRequest];
    WS(weakSelf)
    self.searchSuggestReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            if ([EncodeArrayFromDic(request.responseObject, @"data") isKindOfClass:[NSArray class]]) {
                weakSelf.searchHotKeyModel.data = [NSArray modelArrayWithClass:[QJSearchTagDetailModel class] json:EncodeArrayFromDic(request.responseObject, @"data")].mutableCopy;
            }
        }else{
            NSString *msg = EncodeStringFromDic(request.responseObject, @"message");
            [weakSelf.view makeToast:msg];
        }
        
        [weakSelf resetHotKeyModel];
    };
    self.searchSuggestReq.failureCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        [weakSelf.view makeToast:@"数据异常 请稍后再试"];
    };
}

/**
 * @author: zjr
 * @date: 2022-7-7
 * @desc: 重置model数据格式，后续可更改
 */
- (void)resetHotKeyModel{
    QJSearchTagDetailModel *listModel = [QJSearchTagDetailModel new];
    listModel.name = @"热门搜索";
    listModel.multiple = YES;
    listModel.list = [NSMutableArray array];
    for (QJSearchTagDetailModel *model in self.searchHotKeyModel.data) {
        QJSearchTagDetailModel *detailModel = [QJSearchTagDetailModel new];
        detailModel.name = model.keyword;
        [listModel.list safeAddObject:detailModel];
    }
    
    NSMutableArray *modelArray = [NSMutableArray array];
    [modelArray safeAddObject:listModel];
    QJSearchSortModel *dataHeaderModel = [QJSearchSortModel new];
    dataHeaderModel.types = modelArray;
    [_searchDeaultView showFilterWithModel:dataHeaderModel];
}

/**
 * @author: zjr
 * @date: 2022-7-14
 * @desc: 导航坐标动画调整
 */
- (void)resetNavHeight{
    self.navView.height = NavigationBar_Bottom_Y;
    self.searchView.top = [QJDeviceConstant sysStatusBarHeight];

    [self.searchTF mas_updateConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.searchView).offset(15);
        make.top.equalTo(self.searchView).offset(4);
        make.bottom.equalTo(self.searchView).offset(-5);
        make.right.equalTo(self.searchView).offset(-74);
    }];
    
    self.cancelBtn.hidden = NO;

    self.searchDeaultView.frame = CGRectMake(0, NavigationBar_Bottom_Y, QJScreenWidth, QJScreenHeight - (NavigationBar_Bottom_Y));
    self.searchContenView.frame = CGRectMake(0, NavigationBar_Bottom_Y, QJScreenWidth, QJScreenHeight - (NavigationBar_Bottom_Y));
}

@end
