//
//  QJMineSetSignatureViewController.m
//  QJBox
//
//  Created by macm on 2022/7/22.
//

#import "QJMineSetSignatureViewController.h"
#import "QJMineSetRequest.h"

@interface QJMineSetSignatureViewController ()<UITextViewDelegate>
@property (nonatomic, strong) UITextView *textView;
@property (nonatomic, strong) UILabel *numLabel;
@property (nonatomic, strong) UILabel *placeholderLabel;
@end

@implementation QJMineSetSignatureViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"设置个性签名";
    [self createView];
}

- (void)createView {
    [self.navBar.rightButton setHidden:NO];
    self.navBar.rightButton.hx_x = kScreenWidth - 60;
    self.navBar.rightButton.width = 54;
    self.navBar.rightButton.height = 24;
    [self.navBar.rightButton setTitle:@"完成" forState:UIControlStateNormal];
    [self.navBar.rightButton.titleLabel setFont:FontRegular(12)];
    [self.navBar.rightButton addTarget:self action:@selector(rightAction) forControlEvents:UIControlEventTouchUpInside];
    [self.navBar.rightButton setTitleColor:UIColorFromRGB(0x007aff) forState:UIControlStateNormal];
    self.navBar.backgroundColor = [UIColor clearColor];

    [self.view addSubview:self.textView];
    [self.textView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(NavigationBar_Bottom_Y+8);
        make.left.right.mas_equalTo(0);
        make.height.mas_equalTo(158);
    }];
    
    [self.view addSubview:self.numLabel];
    [self.numLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.textView.mas_right).mas_offset(-10);
        make.bottom.mas_equalTo(self.textView.mas_bottom).mas_offset(-10);
    }];
    
    [self.textView addSubview:self.placeholderLabel];
    [self.placeholderLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(10);
        make.left.mas_equalTo(20);

    }];
    
    self.textView.text = self.model.signature;
    [self.placeholderLabel setHidden:self.model.signature.length > 0 ? YES : NO];
    self.numLabel.text = [NSString stringWithFormat:@"%ld/30",self.textView.text.length];
}

- (void)rightAction {
    self.model.signature = self.textView.text.length == 0 ? @"" : self.textView.text;
    [QJAppTool showHUDLoading];
    @weakify(self);
    QJMineSetRequest *request = [[QJMineSetRequest alloc] init];
    [request requestUserUpdateBackgroundImage:self.model.backgroundImage birthday:self.model.birthday coverImage:self.model.coverImage nickname:self.model.nickname sex:self.model.sex signature:self.model.signature completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        [QJAppTool hideHUDLoading];
        @strongify(self);
        if (isSuccess) {
            if (self.completeBlock) {
                self.completeBlock(self.textView.text);
            }
            [self.navigationController popViewControllerAnimated:YES];
        }
            
    }];
    
    
}

- (void)textViewDidChangeSelection:(UITextView *)textView {
    if ([textView.text isEqualToString:@""]){
        [self.placeholderLabel setHidden:NO];
    }else{
        [self.placeholderLabel setHidden:YES];
    }
//    NSRange textRange = [textView selectedRange];
//    [textView setText:[self disable_emoji:[textView text]]];
//    [textView setSelectedRange:textRange];
}

-(void)textViewDidChange:(UITextView *)textView {
    UITextRange *selectedRange = [textView markedTextRange];
    //获取高亮部分
    UITextPosition *pos = [textView positionFromPosition:selectedRange.start offset:0];
   //如果在变化中是高亮部分在变，就不要计算字符了
    if (selectedRange && pos) {
        return;
    }
    if (textView.text.length > 30) {
        textView.text = [textView.text substringToIndex:30];
    }

    self.numLabel.text = [NSString stringWithFormat:@"%ld/30",textView.text.length];
    
}

//禁止输入表情
//- (NSString *)disable_emoji:(NSString *)text {
//    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"[^\\u0020-\\u007E\\u00A0-\\u00BE\\u2E80-\\uA4CF\\uF900-\\uFAFF\\uFE30-\\uFE4F\\uFF00-\\uFFEF\\u0080-\\u009F\\u2000-\\u201f\r\n]" options:NSRegularExpressionCaseInsensitive error:nil];
//    NSString *modifiedString = [regex stringByReplacingMatchesInString:text
//                                                               options:0
//                                                                 range:NSMakeRange(0, [text length])
//                                                          withTemplate:@""];
//    return modifiedString;
//}

- (UITextView *)textView {
    if (!_textView) {
        self.textView = [[UITextView alloc] initWithFrame:CGRectMake(20, 100, kScreenWidth - 40, 200)];
        self.textView.backgroundColor = [UIColor whiteColor];
        self.textView.textContainerInset = UIEdgeInsetsMake(10, 10, 10, 10);
        self.textView.textColor = UIColorFromRGB(0x16191c);
        self.textView.font = FontRegular(14);
        self.textView.delegate = self;
    }
    return _textView;
}

- (UILabel *)numLabel {
    if (!_numLabel) {
        _numLabel = [[UILabel alloc] init];
        _numLabel.textColor = UIColorFromRGB(0x474849);
        _numLabel.font = FontRegular(14);
        _numLabel.text = @"0/30";
    }
    return _numLabel;
}

- (UILabel *)placeholderLabel {
    if (!_placeholderLabel) {
        _placeholderLabel = [[UILabel alloc] init];
        _placeholderLabel.text = @"请输入个性签名";
        _placeholderLabel.textColor = UIColorFromRGB(0x919599);
        _placeholderLabel.font = FontRegular(14);
    }
    return _placeholderLabel;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
