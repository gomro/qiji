//
//  QJMineCameraViewController.h
//  QJBox
//
//  Created by macm on 2022/7/26.
//

#import <UIKit/UIKit.h>
#import "QJMineImageViewCropViewController.h"

NS_ASSUME_NONNULL_BEGIN
typedef void(^UserImageBlock)(UIImage *image);

@interface QJMineCameraViewController : UIViewController
@property (copy, nonatomic) UserImageBlock imageBlock;
@property (nonatomic, assign) QJImageViewCropType crop;
@end

NS_ASSUME_NONNULL_END
