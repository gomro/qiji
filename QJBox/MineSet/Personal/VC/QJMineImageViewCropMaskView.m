//
//  QJMineImageViewCropMaskView.m
//  QJBox
//
//  Created by macm on 2022/7/26.
//

#import "QJMineImageViewCropMaskView.h"
@interface QJMineImageViewCropMaskView ()

@property (nonatomic, strong) UIView *rectView;

@end
@implementation QJMineImageViewCropMaskView
- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor clearColor];
        _rectView = [[UIView alloc] init];
        _rectView.clipsToBounds = YES;
        _rectView.layer.borderColor = [UIColor whiteColor].CGColor;
        _rectView.layer.borderWidth = 2;
        [self addSubview:_rectView];
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];

    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextAddRect(context, self.maskRect);
    CGContextAddRect(context, rect);
    [[UIColor colorWithRed:0 green:0 blue:0 alpha:0.4] setFill];
    CGContextDrawPath(context, kCGPathEOFill);
}

- (void)setMaskRect:(CGRect)rect
{
    if (!CGRectEqualToRect(_maskRect, rect)) {
        _maskRect = rect;
        _rectView.frame = rect;
        [self setNeedsDisplay];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
