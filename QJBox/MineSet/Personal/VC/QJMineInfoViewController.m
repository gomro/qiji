//
//  QJMineInfoViewController.m
//  QJBox
//
//  Created by macm on 2022/6/14.
//

#import "QJMineInfoViewController.h"
#import "QJChooseLocationPicker.h"
#import "QJMineDatePickerView.h"
#import "QJMineSetSignatureViewController.h"
#import "QJMineSetTextTableViewCell.h"
#import "QJMineSetNameViewController.h"
#import "QJMineSetInfoTableViewCell.h"
#import "QJMineSetHeadImageViewController.h"
#import "QJMineSetRequest.h"

@interface QJMineInfoViewController ()<UITableViewDataSource, UITableViewDelegate,ChooseLocationPickerDelegate, ChooseLocationDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic ,strong) QJChooseLocationPicker *choosePicker;
@property (nonatomic, copy) NSString *dateString;
@property (nonatomic, strong) QJMineUserModel *model;

@end

@implementation QJMineInfoViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self sendRequest];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    if (self.senddata) {
        
        self.senddata(self.model);
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"个人信息";
    
    self.navBar.backgroundColor = [UIColor clearColor];

    [self.view addSubview:self.tableView];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.navBar.mas_bottom);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view);
    }];
    
}

- (void)sendRequest {
    [QJAppTool showHUDLoading];
    @weakify(self);
    QJMineSetRequest *request = [[QJMineSetRequest alloc] init];
    [request requestUserInfo_completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        @strongify(self);
        [QJAppTool hideHUDLoading];
        if (isSuccess) {
            self.model = [QJMineUserModel modelWithJSON:request.responseJSONObject[@"data"]];
            [self.tableView reloadData];
        }
               
    }];
}

/** 保存用户信息
 *  type: 0: 性别 1：生日
 *  param；参数
 */
- (void)sendRequestSave:(NSInteger)type param:(NSString *)param {
    NSInteger sex = type == 0 ? param.integerValue : self.model.sex;
    NSString *birthday = type == 1 ? param : self.model.birthday;
    [QJAppTool showHUDLoading];
    @weakify(self);
    QJMineSetRequest *request = [[QJMineSetRequest alloc] init];
    [request requestUserUpdateBackgroundImage:self.model.backgroundImage birthday:birthday coverImage:self.model.coverImage nickname:self.model.nickname sex:sex signature:self.model.signature completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        [QJAppTool hideHUDLoading];
        @strongify(self);
        if (isSuccess) {
            if (type == 0) {
                self.model.sex = sex;
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:3];
                QJMineSetTextTableViewCell *cell = [self.tableView cellForRowAtIndexPath: indexPath];
                [self reloadGender:cell];
            } else {
                self.model.birthday = birthday;
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:4];
                QJMineSetTextTableViewCell *cell = [self.tableView cellForRowAtIndexPath: indexPath];
                cell.contentLabel.text = self.model.birthday;
                cell.contentLabel.textColor = UIColorFromRGB(0x000000);
            }
            
        }
            
    }];
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight) style:UITableViewStyleGrouped];
        _tableView.backgroundColor = UIColorFromRGB(0xf5f5f5);
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.dataSource = self;
        _tableView.delegate = self;
        [_tableView registerClass:[QJMineSetTextTableViewCell class] forCellReuseIdentifier:@"cell"];
        [_tableView registerClass:[QJMineSetInfoTableViewCell class] forCellReuseIdentifier:@"headCell"];
        
    }
    return _tableView;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 6;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 48*kWScale;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 8;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [[UIView alloc] init];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [[UIView alloc] init];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    QJMineSetTextTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    cell.titleLabel.textColor = kColorGray;
    cell.contentLabel.textColor = [UIColor blackColor];
    cell.rigntImageView.image = [UIImage imageNamed:@"mine_right_black"];
    cell.contentLabel.font = FontRegular(14);
    
    if (indexPath.section == 0) {
        [self reloadHeadImageView:cell];
        return cell;
    } else if (indexPath.section == 1) {
        cell.titleLabel.text = @"昵称";
        cell.contentLabel.text = self.model.nickname;
    } else if (indexPath.section == 2) {
        cell.titleLabel.text = @"个性签名";
        if (self.model.signature.length == 0) {
            cell.contentLabel.text = @"有趣的个性签名可以吸引更多人哦~";
            cell.contentLabel.textColor = UIColorFromRGB(0xc8cacc);
            [cell.contWidthConstraint uninstall];
            [cell.contentLabel mas_updateConstraints:^(MASConstraintMaker *make) {
                cell.contWidthConstraint = make.width.mas_lessThanOrEqualTo(kScreenWidth);
            }];
        } else {
            cell.contentLabel.text = self.model.signature;
            cell.contentLabel.textColor = UIColorFromRGB(0x000000);
        }
        
    } else if (indexPath.section == 3) {
        cell.titleLabel.text = @"性别";
        [self reloadGender:cell];
        
        return cell;
    } else if (indexPath.section == 4) {
        cell.titleLabel.text = @"生日";
        if (self.model.birthday.length == 0) {
            cell.contentLabel.text = @"(选填)";
            cell.contentLabel.textColor = UIColorFromRGB(0xc8cacc);
        } else {
            cell.contentLabel.text = self.model.birthday;
            cell.contentLabel.textColor = UIColorFromRGB(0x000000);
        }
    } else if (indexPath.section == 5) {
        cell.titleLabel.text = @"背景图";
        cell.contentLabel.text = @"上传主页背景图";
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [[tableView cellForRowAtIndexPath:indexPath] setSelected:NO];
    if (indexPath.section == 0) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
//        QJMineSetTextTableViewCell *cell = [self.tableView cellForRowAtIndexPath: indexPath];
        QJMineSetHeadImageViewController *vc = [[QJMineSetHeadImageViewController alloc] init];
        vc.navString = @"设置头像";
        vc.model = self.model;
//        @weakify(self);
//        vc.completeBlock = ^(NSString * _Nonnull headImage) {
//            @strongify(self);
//            self.model.coverImage = headImage;
//            [self reloadHeadImageView:cell];
//        };
        [vc.headImageView setImageWithURL:[NSURL URLWithString:[_model.coverImage checkImageUrlString]] placeholder:[UIImage imageNamed:@"mine_head"]];
        [self.navigationController pushViewController:vc animated:YES];
    }
    if (indexPath.section == 1) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:1];
//        QJMineSetTextTableViewCell *cell = [self.tableView cellForRowAtIndexPath: indexPath];
        
        QJMineSetNameViewController *vc = [[QJMineSetNameViewController alloc] init];
        vc.model = self.model;
        
//        @weakify(self);
//        vc.completeBlock = ^(NSString * string) {
//            @strongify(self);
//            self.model.nickname = string;
//            cell.contentLabel.text = string;
//        };
        [self.navigationController pushViewController:vc animated:YES];
    }
    if (indexPath.section == 2) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:2];
//        QJMineSetTextTableViewCell *cell = [self.tableView cellForRowAtIndexPath: indexPath];
        
        QJMineSetSignatureViewController *vc = [[QJMineSetSignatureViewController alloc] init];
        vc.model = self.model;
        
//        @weakify(self);
//        vc.completeBlock = ^(NSString * string) {
//            @strongify(self);
//            self.model.signature = string;
//            if (self.model.signature.length == 0) {
//                cell.contentLabel.text = @"有趣的个性签名可以吸引更多人哦~";
//                cell.contentLabel.textColor = UIColorFromRGB(0xc8cacc);
//            } else {
//                cell.contentLabel.text = self.model.signature;
//                cell.contentLabel.textColor = UIColorFromRGB(0x000000);
//            }
//        };
        [self.navigationController pushViewController:vc animated:YES];
    }
    if (indexPath.section == 3) {
        @weakify(self);
        QJBottomSheetAlertView *view = [[QJBottomSheetAlertView alloc]initWithTitles:@[@"男",@"女",@"不展示"]];
        view.clickIndex = ^(NSInteger index) {
            @strongify(self);
            if (index == 0) {
                [self sendRequestSave:0 param:[NSString stringWithFormat:@"%d",1]];
            } else if (index == 1) {
                [self sendRequestSave:0 param:[NSString stringWithFormat:@"%d",2]];
            } else {
                [self sendRequestSave:0 param:[NSString stringWithFormat:@"%d",0]];
            }
            
        };
        [self presentViewController:view animated:NO completion:nil];
            
    }
    if (indexPath.section == 4) {
        NSString *dateString = self.model.birthday;
        if (dateString == nil || dateString.length == 0) {
            NSDate *date = [NSDate date];
            NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
            fmt.dateFormat = @"yyyy-MM-dd";
            dateString = [fmt stringFromDate:date];
        }
        
        @weakify(self);
        QJMineDatePickerView *datepicker = [[QJMineDatePickerView alloc] initWithDateStyle:DateStyleShowYearMonthDay scrollToDate:[NSDate dateWithString:dateString format:@"yyyy-MM-dd"] CompleteBlock:^(NSDate *selectDate) {
            @strongify(self);
            NSString *dateString = [selectDate stringWithFormat:@"yyyy-MM-dd"];
            NSLog(@"选择的日期：%@",dateString);
            [self sendRequestSave:1 param:dateString];

            
        }];
        [datepicker show];
    }
//        if (indexPath.row == 5) {
//            [[QJChooseLocationAnimation shareAnimation] showDownPopView:self.choosePicker];
//        }
    if (indexPath.section == 5) {
        QJMineSetHeadImageViewController *vc = [[QJMineSetHeadImageViewController alloc] init];
        vc.navString = @"设置背景图";
        vc.model = self.model;
        [vc.headImageView setImageWithURL:[NSURL URLWithString:[_model.backgroundImage checkImageUrlString]] placeholder:[UIImage imageNamed:@"mine_head"]];
//        @weakify(self);
//        vc.completeBlock = ^(NSString * _Nonnull headImage) {
//            @strongify(self);
//            self.model.backgroundImage = headImage;
//        };
        [self.navigationController pushViewController:vc animated:YES];
    }
    
}

// 赋值头像
- (void)reloadHeadImageView:(QJMineSetTextTableViewCell *)cell {
    cell.titleLabel.text = @"头像";
    UIImageView *avaterImageView = [[UIImageView alloc] init];
    [avaterImageView setImageWithURL:[NSURL URLWithString:[_model.coverImage checkImageUrlString]] placeholder:[UIImage imageNamed:@"mine_head"]];
    avaterImageView.size = CGSizeMake(24, 24);
    avaterImageView.layer.cornerRadius = 12;
    avaterImageView.layer.masksToBounds = YES;
    NSMutableAttributedString *attachText = [NSMutableAttributedString attachmentStringWithContent:avaterImageView contentMode:UIViewContentModeScaleAspectFill attachmentSize:CGSizeMake(24, 24) alignToFont:FontRegular(14) alignment:YYTextVerticalAlignmentCenter];
    cell.contentLabel.attributedText = attachText;
}

// 赋值性别
- (void)reloadGender:(QJMineSetTextTableViewCell *)cell {
    NSString *content = @"(选填)";
    if (_model.sex == 0) {
        NSMutableAttributedString *contentString = [[NSMutableAttributedString alloc] initWithString:content attributes:@{NSFontAttributeName : FontRegular(14), NSForegroundColorAttributeName : UIColorFromRGB(0xc8cacc)}];
        cell.contentLabel.attributedText = contentString;
    } else {
        UIImageView *avaterImageView = [[UIImageView alloc] init];
        avaterImageView.contentMode = UIViewContentModeScaleAspectFit;
        avaterImageView.image = self.model.sex == 1 ? [UIImage imageNamed:@"mine_boy"] : [UIImage imageNamed:@"mine_girl"];
        avaterImageView.size = CGSizeMake(20, 16);
        NSMutableAttributedString *attachText = [NSMutableAttributedString attachmentStringWithContent:avaterImageView contentMode:UIViewContentModeScaleAspectFill attachmentSize:CGSizeMake(20, 16) alignToFont:FontRegular(14) alignment:YYTextVerticalAlignmentCenter];
        
        content = self.model.sex == 1 ? @"男" : @"女";
        
        UIColor *textColor = self.model.sex == 1 ? UIColorFromRGB(0x007aef) : UIColorFromRGB(0xf44d94);
        
        NSMutableAttributedString *contentString = [[NSMutableAttributedString alloc] initWithString:content attributes:@{NSFontAttributeName : FontRegular(14), NSForegroundColorAttributeName : textColor}];
        [attachText appendAttributedString:contentString];
        
        cell.contentLabel.attributedText = attachText;
    }
}

#pragma mark - ChooseLocationPickerDelegate

- (void)dismissChoosPicker {
    [[QJChooseLocationAnimation shareAnimation] dismissDownPopView];
}

- (void)confirmSelectedAreaResultWithProvince:(NSString *)provinceTitle city:(NSString *)cityTitle district:(NSString *)districtTitle {
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:5 inSection:0];
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath: indexPath];
    cell.textLabel.text = [NSString stringWithFormat:@"地址：%@ %@ %@",provinceTitle,cityTitle,districtTitle];

}

- (QJChooseLocationPicker *)choosePicker {
    if (!_choosePicker) {
        _choosePicker = [[QJChooseLocationPicker alloc] initWithFrame:CGRectMake(0, [UIScreen mainScreen].bounds.size.height, self.view.frame.size.width, 260)];
        _choosePicker.titleFont = kFont(15);
        _choosePicker.confirmBtnTitleColor = [UIColor blueColor];
        _choosePicker.pickViewDelegate = self;
        [_choosePicker parseAndShowDefaultData];
    }
    return _choosePicker;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
