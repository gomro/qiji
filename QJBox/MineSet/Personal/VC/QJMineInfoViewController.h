//
//  QJMineInfoViewController.h
//  QJBox
//
//  Created by macm on 2022/6/14.
//

#import "QJBaseViewController.h"
#import "QJMineUserModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface QJMineInfoViewController : QJBaseViewController

@property (nonatomic, copy) void(^senddata)(QJMineUserModel *model);
@end

NS_ASSUME_NONNULL_END
