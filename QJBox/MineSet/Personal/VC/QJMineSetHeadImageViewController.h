//
//  QJMineSetHeadImageViewController.h
//  QJBox
//
//  Created by macm on 2022/7/25.
//

#import "QJBaseViewController.h"
#import "QJMineUserModel.h"
NS_ASSUME_NONNULL_BEGIN
typedef void(^CompleteBlock)(NSString *);

@interface QJMineSetHeadImageViewController : QJBaseViewController
@property (nonatomic, strong) UIImageView *headImageView;
@property (nonatomic, copy) NSString *navString; //根据标题判断页面
@property (nonatomic, strong) QJMineUserModel *model;
@property (nonatomic, copy) CompleteBlock completeBlock;
@end

NS_ASSUME_NONNULL_END
