//
//  QJMineSetSignatureViewController.h
//  QJBox
//
//  Created by macm on 2022/7/22.
//

#import "QJBaseViewController.h"
#import "QJMineUserModel.h"
NS_ASSUME_NONNULL_BEGIN
typedef void(^CompleteBlock)(NSString *);

@interface QJMineSetSignatureViewController : QJBaseViewController
@property (nonatomic, copy) CompleteBlock completeBlock;
@property (nonatomic, strong) QJMineUserModel *model;
@end

NS_ASSUME_NONNULL_END
