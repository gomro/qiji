//
//  QJMineImageViewCropViewController.h
//  QJBox
//
//  Created by macm on 2022/7/26.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/**
 *  裁剪比例
 */
typedef enum {
    QJImageViewCrop_1_1 = 0, //1:1
    QJImageViewCrop_3_4, // 3:4
    
} QJImageViewCropType;

@class QJMineImageViewCropViewController;

@protocol QJMineImageViewCropControllerDelegate <NSObject>

// 裁剪完图片回调
- (void)avatarCropController:(QJMineImageViewCropViewController *)cropController didFinishCropWithImage:(UIImage *)image;

@end

@interface QJMineImageViewCropViewController : UIViewController

@property (nonatomic, weak) id<QJMineImageViewCropControllerDelegate>delegate;

/**
 *  裁剪区域  默认 屏幕宽度显示屏幕中心位置
 */
@property (nonatomic, assign) CGRect cropRect;

/**
 *  最大缩放比例  默认2
 */
@property (nonatomic, assign) CGFloat maxScale;

/**
 *  初始化方法
 *
 *  @param image 待裁剪图片
 *  @param crop 裁剪比例
 *  @return ZKRAccountAvatarCropController
 */
- (instancetype)initWithImage:(UIImage *)image crop:(QJImageViewCropType)crop;

@end

NS_ASSUME_NONNULL_END

