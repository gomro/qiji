//
//  QJMineImageViewCropMaskView.h
//  QJBox
//
//  Created by macm on 2022/7/26.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJMineImageViewCropMaskView : UIView
@property (nonatomic, assign) CGRect maskRect;

- (void)setMaskRect:(CGRect)rect;
@end

NS_ASSUME_NONNULL_END
