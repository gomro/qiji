//
//  QJMineSetNameViewController.m
//  QJBox
//
//  Created by macm on 2022/7/22.
//

#import "QJMineSetNameViewController.h"
#import "QJMineSetRequest.h"

@interface QJMineSetNameViewController () <UITextFieldDelegate>
@property (nonatomic, strong) UITextField *nameTextField;
@property (nonatomic, strong) UILabel *tipLabel;
@end

@implementation QJMineSetNameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"设置昵称";
    [self createView];
}

- (void)createView {
    [self.navBar.rightButton setHidden:NO];
    self.navBar.rightButton.hx_x = kScreenWidth - 60;
    self.navBar.rightButton.width = 54;
    self.navBar.rightButton.height = 24;
    [self.navBar.rightButton setTitle:@"完成" forState:UIControlStateNormal];
    [self.navBar.rightButton.titleLabel setFont:FontRegular(12)];
    [self.navBar.rightButton addTarget:self action:@selector(rightAction) forControlEvents:UIControlEventTouchUpInside];
    self.navBar.backgroundColor = [UIColor clearColor];

    [self.view addSubview:self.nameTextField];
    [self.nameTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(NavigationBar_Bottom_Y+8);
        make.left.right.mas_equalTo(0);
        make.height.mas_equalTo(48);
    }];
    
    [self.view addSubview:self.tipLabel];
    [self.tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(13);
        make.top.mas_equalTo(self.nameTextField.mas_bottom).mas_offset(8);
    }];
    
    self.nameTextField.text = self.model.nickname;
    [self reloadRightButton];
    
}

- (void)rightAction {
    if (self.nameTextField.text.length == 0) {
        return;
    }
    self.model.nickname = self.nameTextField.text;
    [QJAppTool showHUDLoading];
    @weakify(self);
    QJMineSetRequest *request = [[QJMineSetRequest alloc] init];
    [request requestUserUpdateBackgroundImage:self.model.backgroundImage birthday:self.model.birthday coverImage:self.model.coverImage nickname:self.model.nickname sex:self.model.sex signature:self.model.signature completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        [QJAppTool hideHUDLoading];
        @strongify(self);
        if (isSuccess) {
            if (self.completeBlock) {
                self.completeBlock(self.nameTextField.text);
            }
            [self.navigationController popViewControllerAnimated:YES];
        }
        
    }];
}

- (void)textFeildChange:(UITextField *)textField {

    UITextInputMode *currentInputMode = textField.textInputMode;
    NSString *lang = [currentInputMode primaryLanguage]; // 键盘输入模式
    if ([lang isEqualToString:@"zh-Hans"]) { // 简体中文输入，包括简体拼音，健体五笔，简体手写
        UITextRange *selectedRange = [textField markedTextRange];
        //获取高亮部分
        UITextPosition *position = [textField positionFromPosition:selectedRange.start offset:0];
        // 没有高亮选择的字，则对已输入的文字进行字数统计和限制
        if (!position) {
            if (textField.text.length > 7) {
                textField.text = [textField.text substringToIndex:7];
            }
        }
        // 有高亮选择的字符串，则暂不对文字进行统计和限制
        else{
            
        }
    }
    // 中文输入法以外的直接对其统计限制即可，不考虑其他语种情况
    else{
        if (textField.text.length > 7) {
            textField.text = [textField.text substringToIndex:7];
        }
    }
        
    [self reloadRightButton];
 
}

- (void)reloadRightButton {
    if (self.nameTextField.text.length > 0) {
        [self.tipLabel setHidden:YES];
        if ([NSString hasEmoji:self.nameTextField.text] || [NSString isContainsTwoEmoji:self.nameTextField.text]){
            [self.navBar.rightButton setEnabled:NO];
            [self.navBar.rightButton setTitleColor:UIColorFromRGB(0x919599) forState:UIControlStateNormal];
        } else {
            [self.navBar.rightButton setEnabled:YES];
            [self.navBar.rightButton setTitleColor:UIColorFromRGB(0x007aff) forState:UIControlStateNormal];
        }
    } else {
        [self.tipLabel setHidden:NO];
        [self.navBar.rightButton setEnabled:NO];
        [self.navBar.rightButton setTitleColor:UIColorFromRGB(0x919599) forState:UIControlStateNormal];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    // 隐藏表情键盘
    if ([[textField.textInputMode primaryLanguage] isEqualToString:@"emoji"]) {
        return NO;
    }
    //判断键盘是不是九宫格键盘
    if ([NSString isNineKeyBoard:string] ){
        return YES;
    }else{
        if ([NSString hasEmoji:string] || [NSString isContainsTwoEmoji:string]){
            return NO;
        }
    }

    return YES;
}

- (UITextField *)nameTextField {
    if (!_nameTextField) {
        _nameTextField = [[UITextField alloc] init];
        _nameTextField.backgroundColor = [UIColor whiteColor];
        _nameTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
        _nameTextField.placeholder = @"请输入昵称";
        _nameTextField.font = FontRegular(14);
        _nameTextField.textColor = UIColorFromRGB(0x474849);
        UIView *left = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
        _nameTextField.leftView = left;
        _nameTextField.leftViewMode = UITextFieldViewModeAlways;
        _nameTextField.delegate = self;
        [_nameTextField addTarget:self action:@selector(textFeildChange:) forControlEvents:UIControlEventEditingChanged];
    }
    return _nameTextField;
}

- (UILabel *)tipLabel {
    if (!_tipLabel) {
        _tipLabel = [[UILabel alloc] init];
        _tipLabel.textColor = UIColorFromRGB(0x919599);
        _tipLabel.font = FontRegular(12);
        _tipLabel.text = @"仅支持汉字、字母、数字、或三种组合";
    }
    return _tipLabel;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
