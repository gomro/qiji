//
//  QJMineImageViewCropViewController.m
//  QJBox
//
//  Created by macm on 2022/7/26.
//

#import "QJMineImageViewCropViewController.h"
#import "QJMineImageViewCropMaskView.h"

@interface QJMineImageViewCropViewController ()<UIScrollViewDelegate>

@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) QJMineImageViewCropMaskView *maskView;
@property (nonatomic, strong) UIView *buttonView;
@property (nonatomic, strong) UIButton *cancelButton;
@property (nonatomic, strong) UIButton *backButton;
@property (nonatomic, strong) UIButton *cropButton;
@property (nonatomic, strong) UIImage *image; // 待裁剪的图片

@property (nonatomic, assign) QJImageViewCropType crop;

@end
@implementation QJMineImageViewCropViewController

/**
 *  初始化方法
 *
 *  @param image 待裁剪图片
 *  @param crop 裁剪比例
 *  @return ZKRAccountAvatarCropController
 */
- (instancetype)initWithImage:(UIImage *)image crop:(QJImageViewCropType)crop {
    self = [super init];
    if (self) {
        _image = image;
        _crop = crop;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.view.backgroundColor = [UIColor blackColor];

    CGRect bounds = self.view.bounds;
    CGFloat currentWidth = bounds.size.width;
    CGFloat currentHeight = bounds.size.height;

    CGFloat cropHeight = 0;
    switch (self.crop) {
        case QJImageViewCrop_1_1:
            cropHeight = currentWidth;
            break;
        case QJImageViewCrop_3_4:
            cropHeight = currentWidth/4*3;
            break;
        default:
            break;
    }
    
    _maxScale = 2.0f;
    _cropRect = CGRectMake(0, (currentHeight/2) - (cropHeight/2), currentWidth, cropHeight);

    if (_image) {
        _image = [self fixOrientation:_image];
    }

    [self initSubviews];
}

- (CGRect)imageViewRectWithImage:(UIImage *)image {
    CGRect bounds = self.view.bounds;
    CGFloat currentWidth = bounds.size.width;

    CGFloat width = 0;
    CGFloat height = 0;

    width = currentWidth;
    height = image.size.height / image.size.width * width;
    if (height < self.cropRect.size.height) {
        height = self.cropRect.size.height;
        width = image.size.width / image.size.height * height;
    }

    return CGRectMake(0, 0, width, height);
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];

    [self layoutSubViews];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    self.navigationController.navigationBar.hidden = YES;
    [_maskView setMaskRect:self.cropRect];
    [self refreshScrollView];
}

- (void)initSubviews {
    _scrollView = [[UIScrollView alloc] init];
    _scrollView.delegate = self;
    _scrollView.alwaysBounceVertical = YES;
    _scrollView.alwaysBounceHorizontal = YES;
    _scrollView.showsVerticalScrollIndicator = NO;
    _scrollView.showsHorizontalScrollIndicator = NO;
    [self.view addSubview:_scrollView];

    _maskView = [[QJMineImageViewCropMaskView alloc] init];
    _maskView.userInteractionEnabled = NO;
    [self.view addSubview:_maskView];

    _buttonView = [[UIView alloc] init];
    _buttonView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:_buttonView];

    _cropButton = [[UIButton alloc] init];
    [_cropButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_cropButton setBackgroundColor:UIColorFromRGB(0xff9500)];
    _cropButton.layer.cornerRadius = 4;
    [_cropButton.titleLabel setFont:FontSemibold(12)];
    [_cropButton setTitle:@"确定" forState:UIControlStateNormal];
    [_cropButton addTarget:self action:@selector(cropImageAction) forControlEvents:UIControlEventTouchUpInside];
    [_buttonView addSubview:_cropButton];

    _cancelButton = [[UIButton alloc] init];
    [_cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_cancelButton.titleLabel setFont:FontSemibold(12)];
    [_cancelButton setTitle:@"取消" forState:UIControlStateNormal];
    [_cancelButton addTarget:self action:@selector(cancelAction) forControlEvents:UIControlEventTouchUpInside];
    [_buttonView addSubview:_cancelButton];
    
    _backButton = [[UIButton alloc] init];
    [_backButton setImage:[UIImage imageNamed:@"mine_back_w"] forState:UIControlStateNormal];
    [_backButton addTarget:self action:@selector(cancelAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_backButton];

    [self layoutSubViews];
}

- (void)layoutSubViews {
    CGRect bounds = self.view.bounds;
//    CGFloat currentWidth = bounds.size.width;
//    CGFloat currentHeight = bounds.size.height;

//    _cropRect = CGRectMake(0, (currentHeight - currentWidth) / 2, currentWidth, currentWidth/2);

    _scrollView.frame = bounds;

    if (!_imageView) {
        _imageView = [[UIImageView alloc] initWithImage:_image];
        _imageView.frame = [self imageViewRectWithImage:_image];
        [_scrollView addSubview:_imageView];
    } else {
        _imageView.frame = [self imageViewRectWithImage:_image];
        _imageView.image = _image;
    }

    _scrollView.contentSize = _imageView.frame.size;
    CGRect scrollViewFrame = _scrollView.frame;
    _maskView.frame = scrollViewFrame;
    [_buttonView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.height.mas_equalTo(Bottom_iPhoneX_SPACE + 50);
        make.bottom.mas_equalTo(0);
    }];
    [_cropButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-15);
        make.top.mas_equalTo(10);
        make.width.mas_equalTo(54);
        make.height.mas_equalTo(24);
    }];
    [_cancelButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(10);
        make.width.mas_equalTo(54);
        make.height.mas_equalTo(24);
    }];

    _backButton.frame = CGRectMake(15, NavigationBar_Bottom_Y-40, 30, 30);
}

- (void)cropImageAction {
    [self dismissViewControllerAnimated:NO completion:^{
        [self cropImage];
    }];
}

- (void)cancelAction {
    
    [self dismissViewControllerAnimated:NO completion:nil];
}

#pragma mark - 裁剪图片
- (void)cropImage {
    // 计算缩放比例
    CGFloat scale = _imageView.image.size.height / _imageView.frame.size.height;
    CGFloat imageScale = _imageView.image.scale;

    CGFloat width = self.cropRect.size.width * scale * imageScale;
    CGFloat height = self.cropRect.size.height * scale * imageScale;
    CGFloat x = (self.cropRect.origin.x + _scrollView.contentOffset.x) * scale * imageScale;
    CGFloat y = (self.cropRect.origin.y + _scrollView.contentOffset.y) * scale * imageScale;

    // 设置裁剪图片的区域
    CGRect rect = CGRectMake(x, y, width, height);

    CGImageRef imageRef = CGImageCreateWithImageInRect(self.imageView.image.CGImage, rect);
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // 截取区域图片
        UIImage *image = [UIImage imageWithCGImage:imageRef];
        CGImageRelease(imageRef);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([self.delegate respondsToSelector:@selector(avatarCropController:didFinishCropWithImage:)]) {
                [self.delegate avatarCropController:self didFinishCropWithImage:image];
            }
        });
    });
}

#pragma mark - UIScrollViewDelegate 返回缩放的view
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return _imageView;
}

#pragma mark - 处理scrollView的最小缩放比例 和 滚动范围
- (void)refreshScrollView {
    CGFloat top = self.cropRect.origin.y;

    CGFloat minScale = 0.f;
    if (_imageView.image.size.height > _imageView.image.size.width) {
        minScale = self.cropRect.size.width / _imageView.bounds.size.width;
    } else {
//        minScale = self.cropRect.size.height / _imageView.bounds.size.height;
        minScale = 1;
    }
    CGFloat bottom = self.cropRect.origin.y;
    
    top = self.cropRect.origin.y - [QJDeviceConstant sysStatusBarHeight];
    bottom = bottom - Bottom_iPhoneX_SPACE;

    _scrollView.maximumZoomScale = self.maxScale;
    _scrollView.minimumZoomScale = minScale;
    _scrollView.contentInset = UIEdgeInsetsMake(top, 0, bottom, 0);

    [self scrollToCenter];
}

#pragma mark - 滚动图片到中间位置
- (void)scrollToCenter {
    CGRect bounds = self.view.bounds;
    CGFloat currentWidth = bounds.size.width;
    CGFloat currentHeight = bounds.size.height;

    CGFloat x = (_imageView.frame.size.width - currentWidth) / 2;
    CGFloat y = (_imageView.frame.size.height - currentHeight) / 2;
    y = (_imageView.frame.size.height - currentHeight) / 2  + [QJDeviceConstant sysStatusBarHeight];
    _scrollView.contentOffset = CGPointMake(x, y);
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark -- 图片旋转
- (UIImage *)fixOrientation:(UIImage *)aImage {
    // 图片为正向
    if (aImage.imageOrientation == UIImageOrientationUp) {
        return aImage;
    }

    CGAffineTransform transform = CGAffineTransformIdentity;

    // 判断当前旋转方向，取最后的修正transform
    switch (aImage.imageOrientation) {
        case UIImageOrientationDown:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, aImage.size.width, aImage.size.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;

        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
            transform = CGAffineTransformTranslate(transform, aImage.size.width, 0);
            transform = CGAffineTransformRotate(transform, M_PI_2);
            break;

        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, 0, aImage.size.height);
            transform = CGAffineTransformRotate(transform, -M_PI_2);
            break;
        default:
            break;
    }

    switch (aImage.imageOrientation) {
        case UIImageOrientationUpMirrored:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, aImage.size.width, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;

        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, aImage.size.height, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
        default:
            break;
    }

    CGContextRef ctx = CGBitmapContextCreate(NULL, aImage.size.width, aImage.size.height,
                                             CGImageGetBitsPerComponent(aImage.CGImage), 0,
                                             CGImageGetColorSpace(aImage.CGImage),
                                             CGImageGetBitmapInfo(aImage.CGImage));
    CGContextConcatCTM(ctx, transform);
    switch (aImage.imageOrientation) {
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            CGContextDrawImage(ctx, CGRectMake(0, 0, aImage.size.height, aImage.size.width), aImage.CGImage);
            break;
        default:
            CGContextDrawImage(ctx, CGRectMake(0, 0, aImage.size.width, aImage.size.height), aImage.CGImage);
            break;
    }

    CGImageRef cgimg = CGBitmapContextCreateImage(ctx);
    UIImage *img = [UIImage imageWithCGImage:cgimg];
    CGContextRelease(ctx);
    CGImageRelease(cgimg);
    return img;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
