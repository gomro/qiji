//
//  QJMineSetHeadImageViewController.m
//  QJBox
//
//  Created by macm on 2022/7/25.
//

#import "QJMineSetHeadImageViewController.h"
#import "QJMineBottomAlertView.h"
#import "QJMineCameraViewController.h"
#import "QJMineSetRequest.h"
#import "QJCampsiteRequest.h"

/* model */
#import "QJTaskListModel.h"
/* 弹框公共 */
#import "QJCheckInPopCommonView.h"

@interface QJMineSetHeadImageViewController ()<UINavigationControllerDelegate,UIImagePickerControllerDelegate,QJMineImageViewCropControllerDelegate>
@property (nonatomic, assign) QJImageViewCropType crop; // 裁剪比例
@end

@implementation QJMineSetHeadImageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = self.navString;
    self.view.backgroundColor = [UIColor blackColor];
    if ([self.navString isEqualToString:@"设置背景图"]) {
        self.crop = QJImageViewCrop_3_4;
    } else {
        self.crop = QJImageViewCrop_1_1;
    }
    [self createView];
    
}

- (void)moreAction {
    @weakify(self);
    QJMineBottomAlertView *view = [[QJMineBottomAlertView alloc] initWithTitles:@[@"拍照",@"相册"]];
    view.clickIndex = ^(NSInteger index) {
        @strongify(self);
        
        if (index == 0) {
            QJMineCameraViewController *vc  = [[QJMineCameraViewController alloc] init];
            vc.crop = self.crop;
            vc.modalPresentationStyle = UIModalPresentationCustom;
            vc.imageBlock = ^(UIImage * _Nonnull image) {
                @strongify(self);
                self.headImageView.image = image;
                [self pushImageData:image objectKey:[NSString stringWithFormat:@"images/%@.jpg",[QJAppTool getRandomString]]];
            };
            [self presentViewController:vc animated:YES completion:nil];

        } else {
            UIImagePickerController *imagePicker = [[UIImagePickerController alloc]init];
            [imagePicker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
            [imagePicker setAllowsEditing:NO];
            [imagePicker setDelegate:self];
            [self presentViewController:imagePicker animated:YES completion:nil];
        }
        
        
    };
    [view show];

}

#pragma mark - UIImagePicker代理方法
#pragma mark 照片选择完成的代理方法，照片信息保存在info参数中

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    NSLog(@"info %@", info);
    // 获取编辑后的照片
//    UIImage *image = info[@"UIImagePickerControllerEditedImage"];
    UIImage *image = info[@"UIImagePickerControllerOriginalImage"];

    // 关闭照片选择器
    // 注意：使用照片选择器选择的图片，只是保存在内存中
    // 如果需要再次使用，选择照片后，需要做保存处理
    [self dismissViewControllerAnimated:NO completion:^{
        QJMineImageViewCropViewController *vc = [[QJMineImageViewCropViewController alloc] initWithImage:image crop:self.crop];
        vc.modalPresentationStyle = UIModalPresentationCustom;
        vc.delegate = self;
        [self presentViewController:vc animated:YES completion:nil];
    }];
    
}

// 裁剪完图片回调
- (void)avatarCropController:(QJMineImageViewCropViewController *)cropController didFinishCropWithImage:(UIImage *)image {
    self.headImageView.image = image;
    [self pushImageData:image objectKey:[NSString stringWithFormat:@"images/%@.jpg",[QJAppTool getRandomString]]];
}

// 图片上传阿里云
- (void)pushImageData:(UIImage *)currentImage objectKey:(NSString *)objectKey {
    [QJAppTool showHUDLoading];

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        id<OSSCredentialProvider> credential = [[OSSCustomSignerCredentialProvider alloc] initWithImplementedSigner:^NSString * _Nullable(NSString * _Nonnull contentToSign, NSError *__autoreleasing  _Nullable * _Nullable error) {
            OSSTaskCompletionSource * tcs = [OSSTaskCompletionSource taskCompletionSource];
            __block NSString *signToken= @"";
            QJCampsiteRequest *startRequest = [[QJCampsiteRequest alloc] init];
            [startRequest netWorkGetSign:contentToSign];
            [startRequest setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
                if (ResponseSuccess) {
                    NSLog(@"加载完成");

                    NSString *data = request.responseJSONObject[@"data"][@"contentSign"];
                    signToken = data;
                    [tcs setResult:kCheckNil(data)];
                } else {
                    [tcs setError:request.error];
                }
            }];
            
            [tcs.task waitUntilFinished];
            if (tcs.task.error) {
                return nil;
            } else {
                return signToken;
            }
        }];
        OSSClient *client = [[OSSClient alloc] initWithEndpoint: @"https://oss-cn-hangzhou.aliyuncs.com" credentialProvider:credential];
                
        NSData *imageData = UIImageJPEGRepresentation(currentImage, 0.5);
        OSSPutObjectRequest * put = [OSSPutObjectRequest new];
        put.bucketName = @"sxqj";
        put.objectKey = objectKey;
        put.uploadingData = imageData;
        // （可选）设置上传进度。
        put.uploadProgress = ^(int64_t bytesSent, int64_t totalByteSent, int64_t totalBytesExpectedToSend) {
            // 指定当前上传长度、当前已经上传总长度、待上传的总长度。
            NSLog(@"%lld, %lld, %lld", bytesSent, totalByteSent, totalBytesExpectedToSend);
        };
        OSSTask * putTask = [client putObject:put];
        [putTask waitUntilFinished];
        [putTask continueWithBlock:^id(OSSTask *task) {
            if (!task.error) {
                NSLog(@"upload object success!");
                [self sendRequest:[NSString stringWithFormat:@"/%@",objectKey]];
            } else {
                [QJAppTool hideHUDLoading];
                NSLog(@"upload object failed, error: %@" , task.error);
            }
            return nil;
        }];
    });
}

// 保存个人信息
- (void)sendRequest:(NSString *)headImage {
    
    NSString *cover = self.model.coverImage;
    NSString *background = self.model.backgroundImage;
    
    if ([self.navString isEqualToString:@"设置背景图"]) {
        background = headImage;
    } else {
        cover = headImage;
    }
    @weakify(self);
    QJMineSetRequest *request = [[QJMineSetRequest alloc] init];
    [request requestUserUpdateBackgroundImage:background birthday:self.model.birthday coverImage:cover nickname:self.model.nickname sex:self.model.sex signature:self.model.signature completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        [QJAppTool hideHUDLoading];
        @strongify(self);
        if (isSuccess) {
            NSString *simpleDesc = @"";
            if ([EncodeDicFromDic(request.responseObject, @"data") isKindOfClass:[NSDictionary class]]) {
                NSDictionary *dataDic = EncodeDicFromDic(request.responseObject, @"data");
                simpleDesc = EncodeStringFromDicDefEmtryValue(dataDic, @"simpleDesc");
            }
            if (self.completeBlock) {
                self.completeBlock(headImage);
            }
            if ([self.navString isEqualToString:@"设置背景图"]) {
                [[QJAppTool getCurrentViewController].view makeToast:@"背景图上传成功" duration:2 position:CSToastPositionCenter image:[UIImage imageNamed:@"success_login"]];
            }else{
                [[QJAppTool getCurrentViewController].view makeToast:@"头像上传成功" duration:2 position:CSToastPositionCenter image:[UIImage imageNamed:@"success_login"]];
            }
//            [self.navigationController popViewControllerAnimated:YES];
            if (![kCheckNil(simpleDesc) isEqualToString:@""]) {
                [[QJAppTool getCurrentViewController].view makeToast:simpleDesc duration:2 position:CSToastPositionCenter];
            }
        }
            
    }];
}

- (void)createView {
    self.navBar.titleLabel.textColor = [UIColor whiteColor];
    self.navBar.backgroundColor = [UIColor blackColor];
    [self.navBar.backButton setImage:[UIImage imageNamed:@"mine_back_w"] forState:UIControlStateNormal];
    
    [self.navBar.rightButton setHidden:NO];
    [self.navBar.rightButton setImage:[UIImage imageNamed:@"mine_more_w"] forState:UIControlStateNormal];
    [self.navBar.rightButton addTarget:self action:@selector(moreAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.headImageView];
    [self.headImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(kScreenWidth);
        make.center.mas_equalTo(self.view);
    }];
}

- (UIImageView *)headImageView {
    if (!_headImageView) {
        _headImageView = [[UIImageView alloc] init];
        _headImageView.contentMode = UIViewContentModeScaleAspectFit;
    }
    return _headImageView;
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
