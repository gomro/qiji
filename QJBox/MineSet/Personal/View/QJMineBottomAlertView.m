//
//  QJMineBottomAlertView.m
//  QJBox
//
//  Created by macm on 2022/7/26.
//

#import "QJMineBottomAlertView.h"

#define btnTag  1000
@interface QJMineBottomAlertView ()

/** 控件View */
@property (nonatomic, strong) UIView *contentView;

@property (nonatomic, strong) NSArray *titles;//标题

@property (nonatomic, assign) CGFloat totalHeight;
@end

@implementation QJMineBottomAlertView

- (instancetype)initWithTitles:(NSArray *)titles {
    if (self = [super init]) {
        self.frame = [UIScreen mainScreen].bounds;
        self.backgroundColor = RGBAlpha(0, 0, 0, 0.4);
        
        self.titles = titles;
        self.totalHeight = self.titles.count*52 + 8 + (self.titles.count-1)*1 + Bottom_SN_iPhoneX_OR_LATER_SPACE + 52;
        [self setupUI];
    }
    return self;
}

- (void)setupUI {
    
    UIView *cView = [[UIView alloc] init];
    cView.backgroundColor = [UIColor whiteColor];
    [self addSubview:cView];
    self.contentView = cView;
   
    self.contentView.layer.masksToBounds = YES;
     
    self.contentView.frame = CGRectMake(0, kScreenHeight, kScreenWidth, self.totalHeight);
    [self.contentView showCorner:16 rectCorner:UIRectCornerTopLeft | UIRectCornerTopRight];

    int i = 0;
    for (NSString *text in self.titles) {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.tag = btnTag + i;
        [btn addTarget:self action:@selector(btnAction:) forControlEvents:UIControlEventTouchUpInside];
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        btn.titleLabel.font = FontRegular(14);
        [btn setTitle:text forState:UIControlStateNormal];
        
        [self.contentView addSubview:btn];
        
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.top.equalTo(self.contentView).offset(i*53);
                    make.left.right.equalTo(self.contentView);
                    make.height.equalTo(@52);
        }];
        
        i++;
    }
     
    
    for (int n = 0; n < self.titles.count - 1; n++) {
        UIView *line = [UIView new];
        line.backgroundColor = kColorWithHexString(@"#EBEDF0");
        [self.contentView addSubview:line];
        
        UIButton *btn = [self.contentView viewWithTag:(btnTag + n)];
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.top.equalTo(btn.mas_bottom);
                    make.height.equalTo(@1);
                    make.left.right.equalTo(self.contentView);
        }];
        
        
        
         
    }
    
    UIButton *cancalBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [cancalBtn addTarget:self action:@selector(cancelBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [cancalBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    cancalBtn.titleLabel.font = FontRegular(14);
    [cancalBtn setTitle:@"取消" forState:UIControlStateNormal];
    [self.contentView addSubview:cancalBtn];
    [cancalBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self);
            make.height.equalTo(@52);
            make.bottom.equalTo(self.contentView).offset(-Bottom_SN_iPhoneX_OR_LATER_SPACE);
    }];
    
    UIView *lineView = [UIView new];
    lineView.backgroundColor = kColorWithHexString(@"#F9F9F9");
    [self.contentView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(cancalBtn.mas_top);
            make.height.equalTo(@8);
            make.left.right.equalTo(self.contentView);
    }];
    
    
    
}

#pragma mark --  action

- (void)btnAction:(UIButton *)sender {
    if (self.clickIndex) {
        self.clickIndex(sender.tag - btnTag);
    }
    [self fadeOut];
    
}

- (void)cancelBtnAction {
    [self fadeOut];
}

- (void)show {
    UIWindow *keywindow = [[UIApplication sharedApplication] keyWindow];
    [keywindow addSubview:self];
    [self fadeIn];
}

- (void)fadeIn {
//    self.transform = CGAffineTransformMakeScale(1.3, 1.3);
    self.alpha = 0;
    [UIView animateWithDuration:0.20 animations:^{
        self.alpha = 1;
//        self.transform = CGAffineTransformMakeScale(1, 1);
        self.contentView.frame = CGRectMake(0, [UIScreen mainScreen].bounds.size.height - self.totalHeight , kScreenWidth, self.totalHeight);

    }];
}

- (void)fadeOut {

    [UIView animateWithDuration:0.35 animations:^{
//        self.transform = CGAffineTransformMakeScale(1.3, 1.3);
        self.alpha = 0.0;
        self.contentView.frame = CGRectMake(0,[UIScreen mainScreen].bounds.size.height, kScreenWidth, self.totalHeight);
    } completion:^(BOOL finished) {
        if (finished) {
            [self removeFromSuperview];
        }
    }];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];

    UIView *view = [touch view];

    if (view == self) {
        [self fadeOut];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
