//
//  QJMineSetInfoTableViewCell.m
//  QJBox
//
//  Created by macm on 2022/7/22.
//

#import "QJMineSetInfoTableViewCell.h"

@implementation QJMineSetInfoTableViewCell

- (void)setupUI {
    [super setupUI];
    [self.contentView addSubview:self.headImageView];
    [self.headImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.contentLabel.mas_left).mas_offset(0);
        make.width.height.mas_equalTo(24*kWScale);
        make.centerY.mas_equalTo(self.rigntImageView);
    }];
    
}

- (UIImageView *)headImageView {
    if (!_headImageView) {
        _headImageView = [[UIImageView alloc] init];
        _headImageView.layer.cornerRadius = 12;
        _headImageView.layer.masksToBounds = YES;
    }
    return _headImageView;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
