//
//  QJMineSetTextTableViewCell.m
//  QJBox
//
//  Created by macm on 2022/7/19.
//

#import "QJMineSetTextTableViewCell.h"

@interface QJMineSetTextTableViewCell ()
@property (nonatomic, strong) UIView *lineView;
@end

@implementation QJMineSetTextTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor whiteColor];

        [self setupUI];
    }
    return self;
}

- (void)setupUI {
    [self.contentView addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.centerY.mas_equalTo(self.contentView);
        make.width.mas_greaterThanOrEqualTo(30*kWScale);
    }];
    
    [self.contentView addSubview:self.leftLabel];
    [self.leftLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.titleLabel.mas_right).mas_offset(8);
        make.centerY.mas_equalTo(self.contentView);
    }];
    
    [self.contentView addSubview:self.contentLabel];
    [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        self.contWidthConstraint = make.width.mas_lessThanOrEqualTo(kScreenWidth/2-45);
        make.right.mas_equalTo(-45);
        make.centerY.mas_equalTo(self.contentView);
    }];
    
    [self.contentView addSubview:self.rigntImageView];
    [self.rigntImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-15);
        make.centerY.mas_equalTo(self.contentView);
    }];
    
    [self.contentView addSubview:self.lineView];
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.bottom.mas_equalTo(0);
        make.height.mas_equalTo(1);
    }];
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.textColor = UIColorFromRGB(0x000000);
        _titleLabel.font = FontRegular(14);
    }
    return _titleLabel;
}

- (UILabel *)leftLabel {
    if (!_leftLabel) {
        _leftLabel = [[UILabel alloc] init];
        _leftLabel.textColor = UIColorFromRGB(0x919599);
        _leftLabel.font = FontRegular(14);
    }
    return _leftLabel;
}

- (YYLabel *)contentLabel {
    if (!_contentLabel) {
        _contentLabel = [[YYLabel alloc] init];
        _contentLabel.textColor = UIColorFromRGB(0x919599);
        _contentLabel.font = FontRegular(14);
        _contentLabel.textAlignment = NSTextAlignmentRight;
    }
    return _contentLabel;
}

- (UIImageView *)rigntImageView {
    if (!_rigntImageView) {
        _rigntImageView = [[UIImageView alloc] init];
        _rigntImageView.image = [UIImage imageNamed:@"mine_right"];
    }
    return _rigntImageView;
}

- (UIView *)lineView {
    if (!_lineView) {
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor = UIColorFromRGB(0xf9f9f9);
    }
    return _lineView;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
