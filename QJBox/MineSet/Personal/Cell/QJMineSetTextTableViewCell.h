//
//  QJMineSetTextTableViewCell.h
//  QJBox
//
//  Created by macm on 2022/7/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJMineSetTextTableViewCell : UITableViewCell
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *leftLabel;
@property (nonatomic, strong) YYLabel *contentLabel;
@property (strong, nonatomic) MASConstraint *contWidthConstraint;
@property (nonatomic, strong) UIImageView *rigntImageView;
- (void)setupUI;
@end

NS_ASSUME_NONNULL_END
