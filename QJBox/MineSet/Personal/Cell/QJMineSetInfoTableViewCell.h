//
//  QJMineSetInfoTableViewCell.h
//  QJBox
//
//  Created by macm on 2022/7/22.
//

#import "QJMineSetTextTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJMineSetInfoTableViewCell : QJMineSetTextTableViewCell
@property (nonatomic, strong) UIImageView *headImageView;

@end

NS_ASSUME_NONNULL_END
