//
//  QJMineSetRequest.m
//  QJBox
//
//  Created by macm on 2022/6/28.
//

#import "QJMineSetRequest.h"

@implementation QJMineSetRequest

- (void)sendRequest_completion:(RequestBlock)completion {
    [self start];
    
    [self setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        if (completion) {
            completion(ResponseSuccess, request);
        }
        if (!ResponseSuccess && ![EncodeStringFromDic(request.responseObject, @"code") isEqualToString:@"501"]) {//
            [[QJAppTool getCurrentViewController].view makeToast:EncodeStringFromDic(request.responseObject, @"message") duration:2 position:CSToastPositionCenter image:[UIImage imageNamed:@"tips_toast"]];
        }
    }];
    
    [self setFailureCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        if (completion) {
            completion(NO, request);
        }
        [[QJAppTool getCurrentViewController].view makeToast:@"网络请求失败" duration:2 position:CSToastPositionCenter image:[UIImage imageNamed:@"tips_toast"]];
    }];
}

/**退出登录*/
- (void)requestUserLogout {
    self.urlStr = @"/user/logout";
    self.requestType = YTKRequestMethodPOST;
    [self start];
}

/**用户协议*/
- (void)requestPrivacyProtocol {
    self.urlStr = @"/privacy/protocol";
    self.requestType = YTKRequestMethodGET;
    self.serializerType = YTKRequestSerializerTypeJSON;
    [self start];
}

/**隐私政策*/
- (void)requestPrivacyPolicy {
    self.urlStr = @"/privacy/policy";
    self.requestType = YTKRequestMethodGET;
    [self start];
}

/**获取我的-设置-个人信息*/
- (void)requestUserInfo_completion:(RequestBlock)completion {
    self.urlStr = @"/user/info";
    self.requestType = YTKRequestMethodGET;
    
    [self sendRequest_completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        if (completion) {
            completion(isSuccess, request);
        }
    }];
    
}

/**保存我的-设置-个人信息
 * backgroundImage    背景图
 * birthday    生日 yyyy-MM-dd
 * coverImage    头像
 * nickname    昵称
 * sex    性别: 未设置=0, 男=1, 女=2
 * signature    个性签名
 */
- (void)requestUserUpdateBackgroundImage:(NSString *)backgroundImage birthday:(NSString *)birthday coverImage:(NSString *)coverImage nickname:(NSString *)nickname sex:(NSInteger)sex signature:(NSString *)signature completion:(RequestBlock)completion {
    self.dic = @{
        @"backgroundImage":backgroundImage == nil ? @"" : backgroundImage,
        @"birthday":birthday == nil ? [NSNull null] : birthday,
        @"coverImage":coverImage == nil ? @"" : coverImage,
        @"nickname":nickname == nil ? [NSNull null] : nickname,
        @"sex":@(sex),
        @"signature":signature == nil || signature.length == 0 ? @"" : signature,
    };
    self.urlStr = @"/user/update";
    self.requestType = YTKRequestMethodPOST;
    self.serializerType = YTKRequestSerializerTypeJSON;

    [self sendRequest_completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        if (completion) {
            completion(isSuccess, request);
        }
    }];
    
}

/**获取验证码
 * mobile    手机号
 */
- (void)requestUserVerifyMobile:(NSString *)mobile type:(NSString *)type completion:(RequestBlock)completion {
    self.dic = @{
        @"mobile":mobile,
        @"type":type,
    };
    self.urlStr = @"/user/verify/code";
    self.requestType = YTKRequestMethodGET;
    
    [self sendRequest_completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        if (completion) {
            completion(isSuccess, request);
        }
    }];
    
}

/**验证验证码
 * code    验证码
 * mobile    手机号
 */
- (void)requestUserVerifyCode:(NSString *)code Mobile:(NSString *)mobile type:(NSString *)type completion:(RequestBlock)completion {
    self.dic = @{
        @"code":code,
        @"mobile":mobile,
        @"type":type,
    };
    self.urlStr = @"/user/verify/codeVerify";
    self.requestType = YTKRequestMethodGET;
    
    [self sendRequest_completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        if (completion) {
            completion(isSuccess, request);
        }
    }];
    
}

/**修改手机号
 * code    验证码
 * mobile    手机号
 * newCode    新手机号验证码
 * newMobile    新手机号
 */
- (void)requestUserUpdateCode:(NSString *)code mobile:(NSString *)mobile newCode:(NSString *)newCode newMobile:(NSString *)newMobile completion:(RequestBlock)completion {
    self.dic = @{
        @"code":code,
        @"mobile":mobile,
        @"newCode":newCode,
        @"newMobile":newMobile,
    };
    self.urlStr = @"/user/updatePhone";
    self.requestType = YTKRequestMethodPOST;
    self.serializerType = YTKRequestSerializerTypeJSON;

    [self sendRequest_completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        if (completion) {
            completion(isSuccess, request);
        }
    }];
    
}

/**设置密码
 * code    验证码
 * mobile    手机号
 * checkedPassword    确认密码
 * newPassword    新密码
 */
- (void)requestUserSetpwdCode:(NSString *)code Mobile:(NSString *)mobile checkedPassword:(NSString *)checkedPassword newPassword:(NSString *)newPassword completion:(RequestBlock)completion {
    self.dic = @{
        @"code":code,
        @"mobile":mobile,
        @"checkedPassword":checkedPassword,
        @"newPassword":newPassword,
    };
    self.urlStr = @"/user/setpwd";
    self.requestType = YTKRequestMethodPUT;
    self.serializerType = YTKRequestSerializerTypeJSON;

    [self sendRequest_completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        if (completion) {
            completion(isSuccess, request);
        }
    }];
    
}

/**注销
 * code    验证码
 * mobile    手机号
 */
- (void)requestUserCancelCode:(NSString *)code Mobile:(NSString *)mobile completion:(RequestBlock)completion {
    self.dic = @{
        @"code":code,
        @"mobile":mobile,
    };
    self.urlStr = @"/user/cancel";
    self.requestType = YTKRequestMethodPOST;
    self.serializerType = YTKRequestSerializerTypeJSON;

    [self sendRequest_completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        if (completion) {
            completion(isSuccess, request);
        }
    }];
    
}

/**修改我的-设置-动态隐私设置
 * campNewsOpen    动态是否可查看
 * informationOpen    创作是否可查看
 */
- (void)requestUserPrivacyUpdateCampNewsOpen:(BOOL)campNewsOpen informationOpen:(BOOL)informationOpen completion:(RequestBlock)completion {
    self.dic = @{
        @"campNewsOpen":@(campNewsOpen),
        @"informationOpen":@(informationOpen),
    };
    self.urlStr = @"/user/privacy/update";
    self.requestType = YTKRequestMethodPOST;
    self.serializerType = YTKRequestSerializerTypeJSON;

    [self sendRequest_completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        if (completion) {
            completion(isSuccess, request);
        }
    }];
    
}

/**
 * 获取关于我们配置信息
 */
- (void)requestConfigAboutUs_completion:(RequestBlock)completion {

    self.urlStr = @"/config/aboutUs";
    self.requestType = YTKRequestMethodGET;
    
    [self sendRequest_completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        if (completion) {
            completion(isSuccess, request);
        }
    }];
}

/**
 * 获取关于我们配置信息
 */
- (void)requestConfigVersion_completion:(RequestBlock)completion {
    self.dic = @{
        @"type":@"1",
        @"version":[QJDeviceConstant appVersion],
    };
    self.urlStr = @"/config/version";
    self.requestType = YTKRequestMethodPOST;
    self.serializerType = YTKRequestSerializerTypeJSON;
    
    [self sendRequest_completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        if (completion) {
            completion(isSuccess, request);
        }
    }];
}

@end
