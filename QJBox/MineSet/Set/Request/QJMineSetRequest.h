//
//  QJMineSetRequest.h
//  QJBox
//
//  Created by macm on 2022/6/28.
//

#import "QJBaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

typedef void(^RequestBlock)(BOOL isSuccess,YTKBaseRequest * _Nonnull request);

@interface QJMineSetRequest : QJBaseRequest

/**退出登录*/
- (void)requestUserLogout;

/**用户协议*/
- (void)requestPrivacyProtocol;

/**隐私政策*/
- (void)requestPrivacyPolicy;

/**获取我的-设置-个人信息*/
- (void)requestUserInfo_completion:(RequestBlock)completion;

/**保存我的-设置-个人信息
 * backgroundImage    背景图
 * birthday    生日 yyyy-MM-dd
 * coverImage    头像
 * nickname    昵称
 * sex    性别: 未设置=0, 男=1, 女=2
 * signature    个性签名
 */
- (void)requestUserUpdateBackgroundImage:(NSString *)backgroundImage birthday:(NSString *)birthday coverImage:(NSString *)coverImage nickname:(NSString *)nickname sex:(NSInteger)sex signature:(NSString *)signature completion:(RequestBlock)completion;

/**获取验证码
 * mobile    手机号
 */
- (void)requestUserVerifyMobile:(NSString *)mobile type:(NSString *)type completion:(RequestBlock)completion;

/**验证验证码
 * code    验证码
 * mobile    手机号
 */
- (void)requestUserVerifyCode:(NSString *)code Mobile:(NSString *)mobile type:(NSString *)type completion:(RequestBlock)completion;
/**修改手机号
 * code    验证码
 * mobile    手机号
 * newCode    新手机号验证码
 * newMobile    新手机号
 */
- (void)requestUserUpdateCode:(NSString *)code mobile:(NSString *)mobile newCode:(NSString *)newCode newMobile:(NSString *)newMobile completion:(RequestBlock)completion;

/**设置密码
 * code    验证码
 * mobile    手机号
 * checkedPassword    确认密码
 * newPassword    新密码
 */
- (void)requestUserSetpwdCode:(NSString *)code Mobile:(NSString *)mobile checkedPassword:(NSString *)checkedPassword newPassword:(NSString *)newPassword completion:(RequestBlock)completion;

/**注销
 * code    验证码
 * mobile    手机号
 */
- (void)requestUserCancelCode:(NSString *)code Mobile:(NSString *)mobile completion:(RequestBlock)completion;

/**修改我的-设置-动态隐私设置
 * campNewsOpen    动态是否可查看
 * informationOpen    创作是否可查看
 */
- (void)requestUserPrivacyUpdateCampNewsOpen:(BOOL)campNewsOpen informationOpen:(BOOL)informationOpen completion:(RequestBlock)completion;

/**
 * 获取关于我们配置信息
 */
- (void)requestConfigAboutUs_completion:(RequestBlock)completion;

- (void)requestConfigVersion_completion:(RequestBlock)completion;

@end

NS_ASSUME_NONNULL_END
