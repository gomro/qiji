//
//  QJMineSetNewVersionView.h
//  QJBox
//
//  Created by macm on 2022/9/23.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJMineSetNewVersionView : UIView
- (instancetype)initVersion:(NSString *)version content:(NSString *)content upType:(NSString *)upType;
- (void)show;
@end

NS_ASSUME_NONNULL_END
