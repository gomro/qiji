//
//  QJMineSetNewVersionView.m
//  QJBox
//
//  Created by macm on 2022/9/23.
//

#import "QJMineSetNewVersionView.h"
#import "QJMineSetVersionTableViewCell.h"

@interface QJMineSetNewVersionView ()<UITableViewDataSource, UITableViewDelegate>

/** 控件View */
@property (nonatomic, strong) UIView *contentView;
@property (nonatomic, strong) UIImageView *bgImageView;
@property (nonatomic, strong) UIView *versionView;
@property (nonatomic, strong) UILabel *versionLabel;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UIButton *doneButton;
@property (nonatomic, strong) UIButton *cancelButton;
@property (nonatomic, copy) NSString *content;
@property (nonatomic, copy) NSString *upType;
@end

@implementation QJMineSetNewVersionView

- (instancetype)initVersion:(NSString *)version content:(NSString *)content upType:(NSString *)upType {
    if (self = [super init]) {
        self.frame = [UIScreen mainScreen].bounds;
        self.backgroundColor = RGBAlpha(0, 0, 0, 0.4);

        self.versionLabel.text = version;
        self.content = content;
        self.upType = upType;

        [self setupUI];
        
    }
    return self;
}

- (void)setupUI {
    [self addSubview:self.contentView];
    [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(270);
        make.height.mas_equalTo(375);
        make.centerX.mas_equalTo(self);
        make.centerY.mas_equalTo(self).mas_offset(-40);

    }];
    
    [self.contentView addSubview:self.bgImageView];
    [self.bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.top.mas_equalTo(0);
    }];
    
    [self.contentView addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(105);
        make.centerX.mas_equalTo(self.contentView);
    }];
    
    [self.contentView addSubview:self.versionView];
    [self.versionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.titleLabel.mas_top).mas_offset(7);
        make.left.mas_equalTo(self.titleLabel.mas_right).mas_offset(-3);
        make.height.mas_equalTo(20);
    }];
    
    [self.versionView addSubview:self.versionLabel];
    [self.versionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(2);
        make.bottom.mas_equalTo(-2);
        make.left.mas_equalTo(10);
        make.right.mas_equalTo(-10);
    }];
    
    [self.contentView addSubview:self.tableView];
    [self.contentView addSubview:self.doneButton];

    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(175);
        make.left.mas_equalTo(25);
        make.right.mas_equalTo(-25);
        make.height.mas_equalTo(105);
    }];
    
    if ([self.upType isEqualToString:@"1"]) {
        [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(175);
            make.left.mas_equalTo(25);
            make.right.mas_equalTo(-25);
            make.height.mas_equalTo(143);
        }];
        
        [self.doneButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(48);
            make.right.mas_equalTo(-48);
            make.bottom.mas_equalTo(-15);
            make.height.mas_equalTo(32);
        }];
    } else {
        [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(175);
            make.left.mas_equalTo(25);
            make.right.mas_equalTo(-25);
            make.height.mas_equalTo(105);
        }];
        
        [self.doneButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(48);
            make.right.mas_equalTo(-48);
            make.bottom.mas_equalTo(-53);
            make.height.mas_equalTo(32);
        }];
        
        [self.contentView addSubview:self.cancelButton];
        [self.cancelButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(48);
            make.right.mas_equalTo(-48);
            make.bottom.mas_equalTo(-15);
            make.height.mas_equalTo(32);
        }];
    }
    
}

- (void)doneAction {
    NSURL *url = [NSURL URLWithString:@"itms-apps://itunes.apple.com/app/id1142110895"];
    [[UIApplication sharedApplication]openURL:url options:@{UIApplicationOpenURLOptionsSourceApplicationKey:@YES} completionHandler:^(BOOL success) {
        if (success) {
            NSLog(@"10以后可以跳转url");
        }else{
            NSLog(@"10以后不可以跳转url");
        }
    }];
}

- (void)cancelAction {
    [self fadeOut];
}

- (void)show {
    UIWindow *keywindow = [[UIApplication sharedApplication] keyWindow];
    [keywindow addSubview:self];
    [self fadeIn];
}

- (void)fadeIn {
    self.alpha = 0;
    [UIView animateWithDuration:0.20 animations:^{
        self.alpha = 1;

    }];
}

- (void)fadeOut {

    [UIView animateWithDuration:0.35 animations:^{
        self.alpha = 0.0;
    } completion:^(BOOL finished) {
        if (finished) {
            [self removeFromSuperview];
        }
    }];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];

    UIView *view = [touch view];

    if (view == self) {
        if (![self.upType isEqualToString:@"1"]) {
            [self fadeOut];
        }
    }
}

- (UIView *)contentView {
    if (!_contentView) {
        _contentView = [[UIView alloc] init];
        _contentView.backgroundColor = [UIColor clearColor];
    }
    return _contentView;
}

- (UIImageView *)bgImageView {
    if (!_bgImageView) {
        _bgImageView = [[UIImageView alloc] init];
        _bgImageView.image = [UIImage imageNamed:@"mine_set_ver"];
    }
    return _bgImageView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.textColor = [UIColor whiteColor];
        _titleLabel.text = @"发现新版本";
        [_titleLabel setFont:FontRegular(20)];
    }
    return _titleLabel;
}

- (UIView *)versionView {
    if (!_versionView) {
        _versionView = [[UIView alloc] init];
        _versionView.backgroundColor = [UIColor whiteColor];
        _versionView.layer.cornerRadius = 10;
    }
    return _versionView;
}

- (UILabel *)versionLabel {
    if (!_versionLabel) {
        _versionLabel = [[UILabel alloc] init];
        _versionLabel.textColor = UIColorFromRGB(0x1c1c1c);
        [_versionLabel setFont:FontRegular(12)];
    }
    return _versionLabel;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] init];
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableView registerClass:[QJMineSetVersionTableViewCell class] forCellReuseIdentifier:@"cell"];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.estimatedRowHeight = 1;
    }
    return _tableView;
}

- (UIButton *)doneButton {
    if (!_doneButton) {
        _doneButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_doneButton setTitle:@"立即更新" forState:UIControlStateNormal];
        [_doneButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_doneButton.titleLabel setFont:FontRegular(16)];
        [_doneButton setBackgroundColor:UIColorFromRGB(0x1f2a4d)];
        _doneButton.layer.cornerRadius = 16;
        [_doneButton addTarget:self action:@selector(doneAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _doneButton;
}

- (UIButton *)cancelButton {
    if (!_cancelButton) {
        _cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_cancelButton setTitle:@"稍后再说" forState:UIControlStateNormal];
        [_cancelButton setTitleColor:UIColorFromRGB(0x1f2a4d) forState:UIControlStateNormal];
        [_cancelButton.titleLabel setFont:FontRegular(16)];
        [_cancelButton setBackgroundColor:[UIColor clearColor]];
        [_cancelButton addTarget:self action:@selector(cancelAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _cancelButton;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    QJMineSetVersionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    cell.contentLabel.text = self.content;
    return cell;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
