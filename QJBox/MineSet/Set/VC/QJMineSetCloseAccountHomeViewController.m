//
//  QJMineSetCloseAccountHomeViewController.m
//  QJBox
//
//  Created by macm on 2022/8/3.
//

#import "QJMineSetCloseAccountHomeViewController.h"
#import "QJMineSetChangePhoneNewViewController.h"

@interface QJMineSetCloseAccountHomeViewController ()
@property (nonatomic, strong) UIButton *doneButton;
@property (nonatomic, strong) UIButton *checkBtn;
@property (nonatomic, strong) YYLabel *userAgreementLabel;
@end

@implementation QJMineSetCloseAccountHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"注销账号";
    self.navBar.backgroundColor = [UIColor clearColor];

    [self createView];
}

- (void)doneAction {
    QJMineSetChangePhoneNewViewController *vc = [[QJMineSetChangePhoneNewViewController alloc] init];
    vc.phone = self.phone;
    vc.navStyle = QJSetPhoneCloseAccount;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)createView {
    UIImageView *headImageView = [[UIImageView alloc] init];
    headImageView.image = [UIImage imageNamed:@"mine_logo"];
    [self.view addSubview:headImageView];
    [headImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(NavigationBar_Bottom_Y+25);
        make.centerX.mas_equalTo(self.view);
        make.width.height.mas_equalTo(120);
    }];
    
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.text = @"申请注销的注意事项";
    titleLabel.textColor = UIColorFromRGB(0x000000);
    titleLabel.font = FontSemibold(16);
    [self.view addSubview:titleLabel];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(headImageView.mas_bottom).mas_offset(25);
        make.left.mas_equalTo(15);
    }];
    
    UILabel *textLabel = [[UILabel alloc] init];
    textLabel.text = @"请在安全与常用网络设备环境下工作，并保证您15天内无安全信息修改/注销申请等操作。\n\n如账号下有未使用虚拟货币和付费服务，注销成功后将随账号一同被清空，请谨慎操作。\n\n请您仔细阅读《注销协议》账号注销完成后无法恢复，请谨慎选择并操作。";
    textLabel.textColor = UIColorFromRGB(0x000000);
    textLabel.font = FontRegular(14);
    textLabel.numberOfLines = 0;
    textLabel.textAlignment = NSTextAlignmentLeft;
    [self.view addSubview:textLabel];
    [textLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(titleLabel.mas_bottom).mas_offset(6);
        make.left.mas_equalTo(15);
        make.right.mas_equalTo(-15);
    }];

    [self.view addSubview:self.doneButton];
    
    [self.view addSubview:self.checkBtn];
    [self.checkBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.doneButton);
        make.bottom.mas_equalTo(self.doneButton.mas_top).mas_offset(-10);
        make.width.height.mas_equalTo(25);
    }];
    
    [self.view addSubview:self.userAgreementLabel];
    [self.userAgreementLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.checkBtn.mas_right).mas_offset(0);
        make.centerY.mas_equalTo(self.checkBtn);
    }];
    
}

- (void)btnAction:(UIButton *)sender {
    sender.selected = !sender.selected;
    if (sender.selected) {
        [self.doneButton setEnabled:YES];
        [self.doneButton setBackgroundColor:kColorBtnBg];
    } else {
        [self.doneButton setEnabled:NO];
        [_doneButton setBackgroundColor:UIColorFromRGB(0xc8cacc)];
    }
}

- (UIButton *)checkBtn {
    if (!_checkBtn) {
        _checkBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_checkBtn setImage:[UIImage imageNamed:@"unSelectedAgreementIcon"] forState:UIControlStateNormal];
        [_checkBtn setImage:[UIImage imageNamed:@"selectedAgreementIcon"] forState:UIControlStateSelected];
        [_checkBtn setEnlargeEdgeWithTop:10 right:10 bottom:10 left:10];
        [_checkBtn addTarget:self action:@selector(btnAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _checkBtn;
}


- (YYLabel *)userAgreementLabel {
    if (!_userAgreementLabel) {
        WS(weakSelf)
        _userAgreementLabel = [[YYLabel alloc]init];
        _userAgreementLabel.textAlignment = NSTextAlignmentLeft;
        NSString *str = @"已阅读并同意《注销协议》";
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc]initWithString:str];
        attributedString.color = [UIColor colorWithHexString:@"#919599"];
        attributedString.font = kFont(11);
        NSRange firstRange = [str rangeOfString:@"《注销协议》"];
        [attributedString setTextHighlightRange:firstRange color:[UIColor colorWithHexString:@"#000000"] backgroundColor:[UIColor clearColor] tapAction:^(UIView * _Nonnull containerView, NSAttributedString * _Nonnull text, NSRange range, CGRect rect) {
            QJWKWebViewController *vc = [QJWKWebViewController new];
            vc.navTitle = @"注销协议";
            vc.url = [NSString stringWithFormat:@"%@/privacy/cancel",[QJEnvironmentConfigure shareInstance].baseURL];
            [weakSelf.navigationController pushViewController:vc animated:YES];
        }];
        
        _userAgreementLabel.attributedText = attributedString;
        _userAgreementLabel.numberOfLines = 0;
    }
    return _userAgreementLabel;
}

- (UIButton *)doneButton {
    if (!_doneButton) {
        _doneButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _doneButton.frame = CGRectMake(35, kScreenHeight-Bottom_iPhoneX_SPACE-20-48, kScreenWidth-70, 48);
        [_doneButton setTitle:@"申请注销" forState:UIControlStateNormal];
        [_doneButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_doneButton setBackgroundColor:UIColorFromRGB(0xc8cacc)];
        [_doneButton.titleLabel setFont:FontSemibold(16)];
        _doneButton.layer.cornerRadius = 4;
        [_doneButton addTarget:self action:@selector(doneAction) forControlEvents:UIControlEventTouchUpInside];
        [_doneButton setEnabled:NO];
    }
    return _doneButton;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
