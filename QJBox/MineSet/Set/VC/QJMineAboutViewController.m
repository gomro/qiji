//
//  QJMineAboutViewController.m
//  QJBox
//
//  Created by macm on 2022/6/14.
//

#import "QJMineAboutViewController.h"
#import "QJMineSetRequest.h"
#import "QJMineSetAboutModel.h"
#import "QJMineSetNewVersionView.h"
#import "QJMineSetVersionModel.h"

@interface QJMineAboutViewController ()
@property (nonatomic, strong) UIImageView *logoImageView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *textLabel;
@property (nonatomic, strong) YYLabel *verLabel;
@property (nonatomic, strong) UIButton *telButton;
@property (nonatomic, strong) QJMineSetAboutModel *model;
@property (nonatomic, strong) QJMineSetVersionModel *vModel;

@end

@implementation QJMineAboutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.navBar.backgroundColor = [UIColor clearColor];

    self.title = @"关于盒子";
    [self createView];
    [self sendRequest];
}

- (void)sendRequest {
    [QJAppTool showHUDLoading];
    @weakify(self);
    QJMineSetRequest *request = [[QJMineSetRequest alloc] init];
    [request requestConfigAboutUs_completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        @strongify(self);
        [QJAppTool hideHUDLoading];
        if (isSuccess) {
            self.model = [QJMineSetAboutModel modelWithJSON:request.responseJSONObject[@"data"]];
            [self.logoImageView setImageWithURL:[NSURL URLWithString:[self.model.logo checkImageUrlString]] placeholder:[UIImage imageNamed:@"mine_head"]];
            self.titleLabel.text = self.model.name;
            self.textLabel.text = self.model.desc;
            [self.telButton setTitle:[NSString stringWithFormat:@" 联系我们%@",self.model.servicePhone] forState:UIControlStateNormal];
        }
               
    }];
}

- (void)sendRequestVersion {
    [QJAppTool showHUDLoading];
    @weakify(self);
    QJMineSetRequest *request = [[QJMineSetRequest alloc] init];
    [request requestConfigVersion_completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        @strongify(self);
        [QJAppTool hideHUDLoading];
        if (isSuccess) {
            self.vModel = [QJMineSetVersionModel modelWithJSON:request.responseJSONObject[@"data"]];
            
            if (!self.vModel.isNew) {
                QJMineSetNewVersionView *vc = [[QJMineSetNewVersionView alloc] initVersion:self.vModel.version content:self.vModel.updateDesc upType:self.vModel.updateType];
                [vc show];
            } else {
                [QJAppTool showWarningToast:@"您当前是最新版本"];
            }
            
            
        }
               
    }];
}

- (void)telAction {
    NSMutableString *str = [[NSMutableString alloc] initWithFormat:@"telprompt://%@",self.model.servicePhone];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str] options:@{} completionHandler:^(BOOL success) {
        
    }];
}

- (void)createView {
    [self.view addSubview:self.logoImageView];
    [self.logoImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(NavigationBar_Bottom_Y+70);
        make.width.height.mas_equalTo(54*kWScale);
        make.centerX.mas_equalTo(self.view);
    }];
    
    [self.view addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.logoImageView.mas_bottom).mas_offset(5);
        make.centerX.mas_equalTo(self.view);
    }];
    
    [self.view addSubview:self.textLabel];
    [self.textLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.titleLabel.mas_bottom).mas_offset(20);
        make.left.mas_equalTo(15);
        make.right.mas_equalTo(-15);
    }];
    
    [self.view addSubview:self.telButton];
    [self.telButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.height.mas_equalTo(30*kWScale);
        make.bottom.mas_equalTo(-Bottom_iPhoneX_SPACE-20);
    }];
    [self.view addSubview:self.verLabel];
    [self.verLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.telButton.mas_top).mas_offset(-5);
        make.centerX.mas_equalTo(self.view);
    }];
    
}

- (UIImageView *)logoImageView {
    if (!_logoImageView) {
        _logoImageView = [[UIImageView alloc] init];
        _logoImageView.image = [UIImage imageNamed:@"mine_logo"];
    }
    return _logoImageView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.textColor = [UIColor blackColor];
        _titleLabel.text = @" ";
        _titleLabel.font = FontSemibold(20);
    }
    return _titleLabel;
}

- (UILabel *)textLabel {
    if (!_textLabel) {
        _textLabel = [[UILabel alloc] init];
        _textLabel.textColor = [UIColor blackColor];
        _textLabel.text = @"      ";
        _textLabel.font = FontRegular(14);
        _textLabel.numberOfLines = 0;
    }
    return _textLabel;
}

- (UIButton *)telButton {
    if (!_telButton) {
        _telButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_telButton setTitle:@"  " forState:UIControlStateNormal];
        [_telButton setTitleColor:UIColorFromRGB(0x919599) forState:UIControlStateNormal];
        [_telButton setImage:[UIImage imageNamed:@"mine_phone"] forState:UIControlStateNormal];
        _telButton.titleLabel.font = FontRegular(12);
        [_telButton addTarget:self action:@selector(telAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _telButton;
}

- (YYLabel *)verLabel {
    if (!_verLabel) {
        _verLabel = [[YYLabel alloc] init];
        NSString *string = [NSString stringWithFormat:@"当前版本%@  检测最新版本",[QJDeviceConstant appVersion]];
        NSMutableAttributedString *text  = [[NSMutableAttributedString alloc] initWithString:string];
        text.font = FontRegular(14);
        text.color = [UIColor blackColor];
        [text setTextHighlightRange:NSMakeRange(string.length-6, 6) color:[UIColor colorWithHexString:@"0x007aff"] backgroundColor:[UIColor clearColor] tapAction:^(UIView * _Nonnull containerView, NSAttributedString * _Nonnull text, NSRange range, CGRect rect) {

            [self sendRequestVersion];
            
        }];
        _verLabel.preferredMaxLayoutWidth = kScreenWidth; //设置最大的宽度
        _verLabel.attributedText = text;  //设置富文本
    }
    return _verLabel;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
