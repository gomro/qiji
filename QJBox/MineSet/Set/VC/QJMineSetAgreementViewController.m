//
//  QJMineSetAgreementViewController.m
//  QJBox
//
//  Created by macm on 2022/7/21.
//

#import "QJMineSetAgreementViewController.h"
#import "QJMineSetRequest.h"
#import "QJMineSetTextTableViewCell.h"

@interface QJMineSetAgreementViewController ()<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UITableView *tableView;

@end

@implementation QJMineSetAgreementViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"用户协议与隐私政策";
    self.navBar.backgroundColor = [UIColor clearColor];

    [self.view addSubview:self.tableView];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.navBar.mas_bottom);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view);
    }];
}

// 用户协议
- (void)sendRequestProtocol {
//    [self.view showHUDIndicator];
    @weakify(self);
    QJMineSetRequest *request = [[QJMineSetRequest alloc] init];
    [request requestPrivacyProtocol];
    [request setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
//        [self.view hideHUDIndicator];
        @strongify(self);
        DLog(@"-=-=-==-%@",request.responseJSONObject[@"code"]);
        if (ResponseSuccess) {
            NSString *htmlUrl= request.responseJSONObject[@"data"][@"url"];
            QJWKWebViewController *vc = [QJWKWebViewController new];
            vc.url = htmlUrl;
            [self.navigationController pushViewController:vc animated:YES];
        }
    }];

    [request setFailureCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {

        
    }];
}

// 隐私政策
- (void)sendRequestPolicy {
//    [self.view showHUDIndicator];
    @weakify(self);
    QJMineSetRequest *request = [[QJMineSetRequest alloc] init];
    [request requestPrivacyPolicy];
    [request setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
//        [self.view hideHUDIndicator];
        @strongify(self);
        DLog(@"-=-=-==-%@",request.responseJSONObject[@"code"]);
        if (ResponseSuccess) {
            NSString *htmlUrl= request.responseJSONObject[@"data"][@"url"];
            QJWKWebViewController *vc = [QJWKWebViewController new];
            vc.url = htmlUrl;
            [self.navigationController pushViewController:vc animated:YES];
        }
    }];

    [request setFailureCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {

        
    }];
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight) style:UITableViewStyleGrouped];
        _tableView.backgroundColor = UIColorFromRGB(0xf5f5f5);
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.dataSource = self;
        _tableView.delegate = self;
    }
    return _tableView;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 48*kWScale;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 8;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [[UIView alloc] init];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [[UIView alloc] init];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"cell";
    QJMineSetTextTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[QJMineSetTextTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    if (indexPath.section == 0) {
        cell.titleLabel.text = @"用户协议";
    } else {
        cell.titleLabel.text = @"隐私政策";
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [[tableView cellForRowAtIndexPath:indexPath] setSelected:NO];
    if (indexPath.section == 0) {
//        [self sendRequestProtocol];
        QJWKWebViewController *vc = [QJWKWebViewController new];
        vc.url = [QJUserManager shareManager].userProtocol;
        vc.navTitle = @"用户协议";
        [self.navigationController pushViewController:vc animated:YES];
    }
    if (indexPath.section == 1) {
//        [self sendRequestPolicy];
        QJWKWebViewController *vc = [QJWKWebViewController new];
        vc.url = [QJUserManager shareManager].userPolicy;
        vc.navTitle = @"隐私政策";
        [self.navigationController pushViewController:vc animated:YES];
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
