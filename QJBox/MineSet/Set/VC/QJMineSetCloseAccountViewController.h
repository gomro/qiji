//
//  QJMineSetCloseAccountViewController.h
//  QJBox
//
//  Created by macm on 2022/7/27.
//

#import "QJBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJMineSetCloseAccountViewController : QJBaseViewController
@property (nonatomic, copy) NSString *phone;
@property (nonatomic, copy) NSString *code;
@end

NS_ASSUME_NONNULL_END
