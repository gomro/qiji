//
//  QJMineSetNormalViewController.m
//  QJBox
//
//  Created by macm on 2022/7/21.
//

#import "QJMineSetNormalViewController.h"
#import "QJMineSetTextTableViewCell.h"

@interface QJMineSetNormalViewController ()<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UITableView *tableView;

@end

@implementation QJMineSetNormalViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"通用设置";
    self.navBar.backgroundColor = [UIColor clearColor];

    [self.view addSubview:self.tableView];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.navBar.mas_bottom);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view);
    }];
}

- (void)cleanAction {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:@"你确定要清除缓存吗？" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *nextAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self cleanCache];
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:nextAction];
    [alert addAction:cancelAction];
    [self presentViewController:alert animated:YES completion:nil];

}

// 获取缓存大小
- (NSString *)getCacheString {

    YYImageCache *cache = [YYWebImageManager sharedManager].cache;
    NSInteger discCache = cache.diskCache.totalCost;
//    NSInteger memoryCache = cache.memoryCache.totalCost;
       
    return [self fileSizeWithInteger:discCache];

}

// 清除缓存大小
- (void)cleanCache {
    // 获取缓存管理类
    YYImageCache *cache = [YYWebImageManager sharedManager].cache;
    
    // 清空磁盘缓存
    [cache.diskCache removeAllObjects];
//    [cache.memoryCache removeAllObjects];

    [self.tableView reloadData];
//    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
//    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath: indexPath];
//    cell.textLabel.text = [NSString stringWithFormat:@"清除缓存    0M"];
//    cell.contentLabel.text = [self getCacheString];
//     清空磁盘缓存，带进度回调
//    [cache.diskCache removeAllObjectsWithProgressBlock:^(int removedCount, int totalCount) {
//    // progress
//    } endBlock:^(BOOL error) {
//    // end
//    }];
}


// 根据数据计算出大小
- (NSString *)fileSizeWithInteger:(NSInteger)size{
    // 1K = 1024dB, 1M = 1024K,1G = 1024M
//    if (size < 1024) {// 小于1k
//        return [NSString stringWithFormat:@"%ldB",(long)size];
//    }else if (size < 1024 * 1024){// 小于1m
//        CGFloat aFloat = size / 1024.0;
//        return [NSString stringWithFormat:@"%.0fK",aFloat];
//    }else if (size < 1024 * 1024 * 1024){// 小于1G
    CGFloat aFloat = size / 1024.0 / 1024.0;
    if (aFloat == 0) {
        return [NSString stringWithFormat:@"(0.0MB)"];
    } else {
        return [NSString stringWithFormat:@"%.1fMB",aFloat];
    }
        
//    }else{
//        CGFloat aFloat = size / 1024.0 / 1024.0 / 1024.0;
//        return [NSString stringWithFormat:@"%.2fG",aFloat];
//    }
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight) style:UITableViewStyleGrouped];
        _tableView.backgroundColor = UIColorFromRGB(0xf5f5f5);
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableView registerClass:[QJMineSetTextTableViewCell class] forCellReuseIdentifier:@"cell"];
        _tableView.dataSource = self;
        _tableView.delegate = self;
    }
    return _tableView;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 48*kWScale;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [[UIView alloc] init];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [[UIView alloc] init];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    QJMineSetTextTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    cell.titleLabel.text = @"清除缓存";
    cell.contentLabel.text = [self getCacheString];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [[tableView cellForRowAtIndexPath:indexPath] setSelected:NO];
    [self cleanAction];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
