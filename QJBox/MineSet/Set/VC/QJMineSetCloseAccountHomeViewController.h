//
//  QJMineSetCloseAccountHomeViewController.h
//  QJBox
//
//  Created by macm on 2022/8/3.
//

#import "QJBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJMineSetCloseAccountHomeViewController : QJBaseViewController
@property (nonatomic, copy) NSString *phone;
@end

NS_ASSUME_NONNULL_END
