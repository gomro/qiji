//
//  QJMineSetActivityViewController.m
//  QJBox
//
//  Created by macm on 2022/7/22.
//

#import "QJMineSetActivityViewController.h"
#import "QJMineSetTextTableViewCell.h"
#import "QJMineSetRequest.h"

@interface QJMineSetActivityViewController ()<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UIButton *doneButton;
@property (nonatomic, assign) BOOL campNewsOpen;
@property (nonatomic, assign) BOOL informationOpen;

@end

@implementation QJMineSetActivityViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"动态隐私设置";
    self.navBar.backgroundColor = [UIColor clearColor];

    self.campNewsOpen = self.model.campNewsOpen;
    self.informationOpen = self.model.informationOpen;
    
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.navBar.mas_bottom);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view);
    }];
    
    [self.view addSubview:self.doneButton];
    [self.doneButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(-(Bottom_iPhoneX_SPACE+10));
        make.left.mas_equalTo(38);
        make.right.mas_equalTo(-38);
        make.height.mas_equalTo(48);
    }];
}

- (void)doneAction {
    [QJAppTool showHUDLoading];
    QJMineSetRequest *request = [[QJMineSetRequest alloc] init];
    @weakify(self);
    [request requestUserPrivacyUpdateCampNewsOpen:self.campNewsOpen informationOpen:self.informationOpen completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        [QJAppTool hideHUDLoading];
        @strongify(self);
        if (isSuccess) {
            self.model.campNewsOpen = self.campNewsOpen;
            self.model.informationOpen = self.informationOpen;
            [[QJAppTool getCurrentViewController].view makeToast:@"修改成功" duration:2 position:CSToastPositionCenter image:[UIImage imageNamed:@"success_login"]];
        }

    }];
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight) style:UITableViewStyleGrouped];
        _tableView.backgroundColor = UIColorFromRGB(0xf5f5f5);
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.dataSource = self;
        _tableView.delegate = self;
    }
    return _tableView;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 100;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [[UIView alloc] init];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 100)];
    label.text = @"勾选后您的粉丝可在个人主页看到相关信息";
    label.textColor = UIColorFromRGB(0x919599);
    label.font = FontRegular(12);
    label.textAlignment = NSTextAlignmentCenter;
    return label;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"cell";
    QJMineSetTextTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[QJMineSetTextTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    if (indexPath.row == 0) {
        cell.titleLabel.text = @"动态";
        [cell.rigntImageView setImage:self.campNewsOpen ? [UIImage imageNamed:@"mine_act_sel"] : [UIImage imageNamed:@"mine_act_nor"]];
    } else {
        cell.titleLabel.text = @"创作";
        [cell.rigntImageView setImage:self.informationOpen ? [UIImage imageNamed:@"mine_act_sel"] : [UIImage imageNamed:@"mine_act_nor"]];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [[tableView cellForRowAtIndexPath:indexPath] setSelected:NO];
    if (indexPath.row == 0) {
        self.campNewsOpen = !self.campNewsOpen;
    }
    if (indexPath.row == 1) {
        self.informationOpen = !self.informationOpen;
    }
    [self.tableView reloadData];
}

- (UIButton *)doneButton {
    if (!_doneButton) {
        _doneButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_doneButton setTitle:@"确认" forState:UIControlStateNormal];
        [_doneButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_doneButton.titleLabel setFont:FontSemibold(16)];
        [_doneButton setBackgroundColor:kColorBtnBg];
        _doneButton.layer.cornerRadius = 4;
        [_doneButton addTarget:self action:@selector(doneAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _doneButton;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
