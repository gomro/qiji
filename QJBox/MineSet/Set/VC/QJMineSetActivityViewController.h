//
//  QJMineSetActivityViewController.h
//  QJBox
//
//  Created by macm on 2022/7/22.
//

#import "QJBaseViewController.h"
#import "QJMineUserModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface QJMineSetActivityViewController : QJBaseViewController
@property (nonatomic, strong) QJMineUserModel *model;
@end

NS_ASSUME_NONNULL_END
