//
//  QJMineSetCloseAccountViewController.m
//  QJBox
//
//  Created by macm on 2022/7/27.
//

#import "QJMineSetCloseAccountViewController.h"
#import "QJMineSetRequest.h"

@interface QJMineSetCloseAccountViewController ()
@property (nonatomic, strong) UIButton *doneButton;
@end

@implementation QJMineSetCloseAccountViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"注销确认";
    self.navBar.backgroundColor = [UIColor clearColor];

    [self createView];
    
    [self startTimer];
}

- (void)doneAction {
    [QJAppTool showHUDLoading];
    @weakify(self);
    QJMineSetRequest *request = [[QJMineSetRequest alloc] init];
    [request requestUserCancelCode:self.code Mobile:self.phone completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        [QJAppTool hideHUDLoading];
        @strongify(self);
        if (isSuccess) {
            LSUserDefaultsSET(@"", kQJUserId);
            LSUserDefaultsSET(@"",kQJToken);//退出登录
            LSUserDefaultsSET(@0, kQJIsRealName);
            LSUserDefaultsSET(@"", kQJUserId);
            [QJUserManager shareManager].isLogin = NO;
            [QJUserManager shareManager].isVerifyState = NO;
            [QJUserManager shareManager].userID = @"";
            [QJUserManager shareManager].isNetLogout = NO;//退出登录时操作
            QJAppTabBarController *tabController = [[QJAppTabBarController alloc] init];
            [UIApplication sharedApplication].delegate.window.rootViewController = tabController;
            [tabController checkLogin:^(BOOL isLogin) {
               
                
            }];

        }

    }];

}

- (void)startTimer {
    __weak typeof (self) weakSelf=self;
    __block NSInteger time = 3;
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
    dispatch_source_set_timer(timer, DISPATCH_TIME_NOW, 1.0 * NSEC_PER_SEC, 0 * NSEC_PER_SEC);
    dispatch_source_set_event_handler(timer, ^{
        if (time < 1) {
            dispatch_source_cancel(timer);
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf.doneButton setEnabled:YES];
                [weakSelf.doneButton setTitle:@"确认注销" forState:UIControlStateNormal];
                [weakSelf.doneButton setBackgroundColor:kColorBtnBg];
            });
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf.doneButton setEnabled:NO];
                [weakSelf.doneButton setTitle:[NSString stringWithFormat:@"确认注销（%ld）",time] forState:UIControlStateNormal];
                [weakSelf.doneButton setBackgroundColor:UIColorFromRGB(0x919599)];
            });
            time --;
        }
    });
    dispatch_resume(timer);
}

- (void)createView {
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.text = @"请仔细阅读再确认";
    titleLabel.textColor = UIColorFromRGB(0x000000);
    titleLabel.font = FontSemibold(16);
    [self.view addSubview:titleLabel];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(NavigationBar_Bottom_Y+25);
        make.left.mas_equalTo(15);
    }];
    
    UILabel *textLabel = [[UILabel alloc] init];
    textLabel.text = @"1、我们将在7日内处理您的注销申请，期间若出现重新登录该账号，注销申请将被自动注销。\n\n2、注销成功后，有未使用的虚拟货币或付费服务，注销成功将随账号一同被清空。\n\n3、如您有什么疑问，可以在盒子内联系平台，获取帮助。";
    textLabel.textColor = UIColorFromRGB(0x000000);
    textLabel.font = FontRegular(14);
    textLabel.numberOfLines = 0;
    textLabel.textAlignment = NSTextAlignmentLeft;
    [self.view addSubview:textLabel];
    [textLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(titleLabel.mas_bottom).mas_offset(6);
        make.left.mas_equalTo(15);
        make.right.mas_equalTo(-15);
    }];
    
    [self.view addSubview:self.doneButton];
    
}

- (UIButton *)doneButton {
    if (!_doneButton) {
        _doneButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _doneButton.frame = CGRectMake(35, kScreenHeight-Bottom_iPhoneX_SPACE-20-48, kScreenWidth-70, 48);
        [_doneButton setTitle:@"确认注销" forState:UIControlStateNormal];
        [_doneButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_doneButton setBackgroundColor:UIColorFromRGB(0x919599)];
        [_doneButton.titleLabel setFont:FontSemibold(16)];
        _doneButton.layer.cornerRadius = 4;
        [_doneButton addTarget:self action:@selector(doneAction) forControlEvents:UIControlEventTouchUpInside];
        [_doneButton setEnabled:NO];
    }
    return _doneButton;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
