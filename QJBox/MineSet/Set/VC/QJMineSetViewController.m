//
//  QJMineSetViewController.m
//  QJBox
//
//  Created by macm on 2022/6/14.
//

#import "QJMineSetViewController.h"
#import "QJMineAboutViewController.h"
#import "QJMineInfoViewController.h"
#import "QJMineNotifyViewController.h"
#import "QJMineSafeViewController.h"
#import "QJMineFeedBackViewController.h"
#import "QJMineSetPrivacyViewController.h"
#import "QJMineSetRequest.h"
#import "QJAlertView.h"
#import "QJMineSetTextTableViewCell.h"
#import "QJMineSetNormalViewController.h"
#import "QJMineSetAgreementViewController.h"
#import "QJMineSetRequest.h"

@interface QJMineSetViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UIView *bottomView;
@property (nonatomic, strong) UIButton *logoutButton;
@property (nonatomic, strong) QJMineUserModel *model;
@end

@implementation QJMineSetViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"设置";
    
    [self createView];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self sendRequest];
}

- (void)createView {
    self.navBar.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.tableView];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.navBar.mas_bottom);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view);
    }];
    
    [self.view addSubview:self.bottomView];
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.left.right.mas_equalTo(0);
        make.height.mas_equalTo(Bottom_iPhoneX_SPACE+48*kWScale);
    }];
    
    [self.bottomView addSubview:self.logoutButton];
    [self.logoutButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(0);
        make.height.mas_equalTo(48*kWScale);
    }];
}

- (void)sendRequest {
    @weakify(self);
    QJMineSetRequest *request = [[QJMineSetRequest alloc] init];
    [request requestUserInfo_completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        @strongify(self);
        if (isSuccess) {
            self.model = [QJMineUserModel modelWithJSON:request.responseJSONObject[@"data"]];
        }
               
    }];
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight) style:UITableViewStyleGrouped];
        _tableView.backgroundColor = UIColorFromRGB(0xf5f5f5);
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableView registerClass:[QJMineSetTextTableViewCell class] forCellReuseIdentifier:@"cell"];
        _tableView.dataSource = self;
        _tableView.delegate = self;
    }
    return _tableView;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 2) {
        return 3;
    }
    if (section == 3) {
        return 2;
    }
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 48*kWScale;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [[UIView alloc] init];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [[UIView alloc] init];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    QJMineSetTextTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (indexPath.section == 0) {
        cell.titleLabel.text = @"账户信息";
    } else if (indexPath.section == 1) {
        cell.titleLabel.text = @"安全与绑定";
    } else if (indexPath.section == 2) {
        if (indexPath.row == 0) {
            cell.titleLabel.text = @"推送设置";
        } else if (indexPath.row == 1) {
            cell.titleLabel.text = @"通用设置";
        } else {
            cell.titleLabel.text = @"隐私设置";
        }
    } else if (indexPath.section == 3) {
        if (indexPath.row == 0) {
            cell.titleLabel.text = @"关于我们";
            cell.contentLabel.text = [NSString stringWithFormat:@"当前版本 %@",[QJDeviceConstant appVersion]];
        } else {
            cell.titleLabel.text = @"用户协议与隐私政策";
        }
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [[tableView cellForRowAtIndexPath:indexPath] setSelected:NO];
    if (indexPath.section == 0) {
        QJMineInfoViewController *vc = [QJMineInfoViewController new];
        [self.navigationController pushViewController:vc animated:YES];
    }
    if (indexPath.section == 1) {
        QJMineSafeViewController *vc = [QJMineSafeViewController new];
        vc.model = self.model;
        [self.navigationController pushViewController:vc animated:YES];
    }
    if (indexPath.section == 2) {
        if (indexPath.row == 0) {
            QJMineNotifyViewController *vc = [QJMineNotifyViewController new];
            [self.navigationController pushViewController:vc animated:YES];
        } else if (indexPath.row == 1) {
            QJMineSetNormalViewController *vc = [QJMineSetNormalViewController new];
            [self.navigationController pushViewController:vc animated:YES];
        } else if (indexPath.row == 2) {
            QJMineSetPrivacyViewController *vc = [QJMineSetPrivacyViewController new];
            vc.model = self.model;
            [self.navigationController pushViewController:vc animated:YES];
        }
    }
    if (indexPath.section == 3) {
        if (indexPath.row == 0) {
            QJMineAboutViewController *vc = [QJMineAboutViewController new];
            [self.navigationController pushViewController:vc animated:YES];
        } else {
            QJMineSetAgreementViewController *vc = [QJMineSetAgreementViewController new];
            [self.navigationController pushViewController:vc animated:YES];
        }
        
    }
}

// 退出登录
- (void)logoutAction {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:@"您确定要退出当前账号吗" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *nextAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self sendRequestLogout];
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDestructive handler:nil];
    
    [alert addAction:cancelAction];
    [alert addAction:nextAction];
    [self presentViewController:alert animated:YES completion:nil];
    [cancelAction setValue:UIColorFromRGB(0x919599) forKey:@"titleTextColor"];

}

- (void)sendRequestLogout {
    WS(weakSelf)
    QJMineSetRequest *setRequest = [[QJMineSetRequest alloc] init];
    [setRequest requestUserLogout];
    [setRequest setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        DLog(@"-=-=-==-%@",request.responseJSONObject[@"code"]);
        if ([EncodeStringFromDic(request.responseJSONObject, @"code") isEqualToString:@"0"]) {
            LSUserDefaultsSET(@"", kQJUserId);
            LSUserDefaultsSET(@"",kQJToken);//退出登录
            LSUserDefaultsSET(@0, kQJIsRealName);
            LSUserDefaultsSET(@"", kQJUserId);
            [QJUserManager shareManager].isLogin = NO;
            [QJUserManager shareManager].isVerifyState = NO;
            [QJUserManager shareManager].userID = @"";
            QJAppTabBarController *tabController = [[QJAppTabBarController alloc] init];
            [UIApplication sharedApplication].delegate.window.rootViewController = tabController;
            [tabController checkLogin:^(BOOL isLogin) {
               
                
            }];
        }
    }];
}

- (UIView *)bottomView {
    if (!_bottomView) {
        _bottomView = [[UIView alloc] init];
        _bottomView.backgroundColor = [UIColor whiteColor];
    }
    return _bottomView;
}

-(UIButton *)logoutButton {
    if (!_logoutButton) {
        _logoutButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_logoutButton setTitle:@"退出登录" forState:UIControlStateNormal];
        [_logoutButton setBackgroundColor:[UIColor whiteColor]];
        [_logoutButton setTitleColor:UIColorFromRGB(0xff3b30) forState:UIControlStateNormal];
        _logoutButton.titleLabel.font = FontRegular(14);
        [_logoutButton addTarget:self action:@selector(logoutAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _logoutButton;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
