//
//  QJMineSafeViewController.m
//  QJBox
//
//  Created by macm on 2022/6/14.
//

#import "QJMineSafeViewController.h"
#import "QJMineSetTextTableViewCell.h"
#import "QJMineSetChangePhoneViewController.h"
#import "QJMineSetChangePhoneNewViewController.h"
#import "QJMineSetCloseAccountHomeViewController.h"

@interface QJMineSafeViewController ()<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UITableView *tableView;

@end

@implementation QJMineSafeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"安全与绑定";
    
    self.navBar.backgroundColor = [UIColor clearColor];

    [self.view addSubview:self.tableView];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.navBar.mas_bottom);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view);
    }];
    
    // 修改手机成功后刷新显示
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeAction:) name:@"SetChangePhone" object:nil];
    
}

- (void)changeAction:(NSNotification *)obj {
    NSString *phone = [NSString stringWithFormat:@"%@",obj.object];
    self.model.mobile = phone;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    QJMineSetTextTableViewCell *cell = [self.tableView cellForRowAtIndexPath: indexPath];
    cell.leftLabel.text = self.model.mobile;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight) style:UITableViewStyleGrouped];
        _tableView.backgroundColor = UIColorFromRGB(0xf5f5f5);
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.dataSource = self;
        _tableView.delegate = self;
    }
    return _tableView;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 6;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44*kWScale;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 8;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [[UIView alloc] init];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [[UIView alloc] init];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"cell";
    QJMineSetTextTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[QJMineSetTextTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    if (indexPath.section == 0) {
        cell.titleLabel.text = @"手机号";
        cell.leftLabel.text = self.model.mobile;
        cell.contentLabel.text = @"更换绑定";
    } else if (indexPath.section == 1) {
        cell.titleLabel.text = @"微信号";
        cell.leftLabel.text = self.model.wxName;
        [cell.rigntImageView setHidden:YES];
    } else if (indexPath.section == 2) {
        cell.titleLabel.text = @"Apple ID";
        cell.leftLabel.text = self.model.appleName;
        [cell.rigntImageView setHidden:YES];
    } else if (indexPath.section == 3) {
        cell.titleLabel.text = @"QQ";
        cell.leftLabel.text = self.model.qqName;
        [cell.rigntImageView setHidden:YES];
    } else if (indexPath.section == 4) {
        cell.titleLabel.text = @"设置密码";
        cell.contentLabel.text = @"去设置";
    } else if (indexPath.section == 5) {
        cell.titleLabel.text = @"永久注销账号";
        cell.contentLabel.text = @"注销";
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [[tableView cellForRowAtIndexPath:indexPath] setSelected:NO];
    
    if (indexPath.section == 0) {
        QJMineSetChangePhoneViewController *vc = [[QJMineSetChangePhoneViewController alloc] init];
        vc.phone = self.model.mobile;
        [self.navigationController pushViewController:vc animated:YES];
    }
    if (indexPath.section == 4) {
        QJMineSetChangePhoneNewViewController *vc = [[QJMineSetChangePhoneNewViewController alloc] init];
        vc.navStyle = QJSetPhonePassword;
        vc.phone = self.model.mobile;
        [self.navigationController pushViewController:vc animated:YES];
    }
    if (indexPath.section == 5) {
        QJMineSetCloseAccountHomeViewController *vc = [[QJMineSetCloseAccountHomeViewController alloc] init];
        vc.phone = self.model.mobile;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
