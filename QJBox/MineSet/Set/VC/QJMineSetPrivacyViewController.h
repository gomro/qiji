//
//  QJMineSetPrivacyViewController.h
//  QJBox
//
//  Created by macm on 2022/6/24.
//

#import "QJBaseViewController.h"
#import "QJMineUserModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface QJMineSetPrivacyViewController : QJBaseViewController
@property (nonatomic, strong) QJMineUserModel *model;
@end

NS_ASSUME_NONNULL_END
