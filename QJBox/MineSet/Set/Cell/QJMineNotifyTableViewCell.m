//
//  QJMineNotifyTableViewCell.m
//  QJBox
//
//  Created by macm on 2022/6/16.
//

#import "QJMineNotifyTableViewCell.h"
#import "QJMineMsgSetRequest.h"

@interface QJMineNotifyTableViewCell ()
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UISwitch *setSwitch;

@end

@implementation QJMineNotifyTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor clearColor];
        [self setupUI];
    }
    return self;
}

- (void)setupUI {
    [self.contentView addSubview:self.titleLabel];
    [self.contentView addSubview:self.setSwitch];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.centerY.mas_equalTo(self.contentView);
    }];
    
    [self.setSwitch mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-15);
        make.centerY.mas_equalTo(self.contentView);
    }];
    
    UIView *lineView = [[UIView alloc] init];
    lineView.backgroundColor = UIColorFromRGB(0xf9f9f9);
    [self.contentView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(-1);
        make.left.right.equalTo(self.contentView);
        make.height.mas_equalTo(1);
    }];
}

- (void)setModel:(QJMineMsgSetModel *)model {
    _model = model;
    self.titleLabel.text = _model.name;
    [self.setSwitch setOn:_model.status];
}

- (void)switchAction:(UISwitch *)sender {
    [QJAppTool showHUDLoading];
    QJMineMsgSetRequest *request = [[QJMineMsgSetRequest alloc] init];
    request.param = @[@{@"id":[NSNumber numberWithString:_model.idStr], @"status":[NSNumber numberWithBool:sender.isOn]}];
    [request requestNotifySetting];
    [request setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        [QJAppTool hideHUDLoading];
        if (ResponseSuccess) {
            NSLog(@"更改成功");
        } else {
            [[QJAppTool getCurrentViewController].view makeToast:EncodeStringFromDic(request.responseObject, @"message")];
            [sender setOn:!sender.isOn];
        }
    }];
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.textColor = [UIColor blackColor];
        _titleLabel.text = @"评论";
        _titleLabel.font = FontMedium(14);
    }
    return _titleLabel;
}

- (UISwitch *)setSwitch {
    if (!_setSwitch) {
        _setSwitch = [[UISwitch alloc] init];
//        _setSwitch.transform = CGAffineTransformMakeScale(0.7, 0.7);
        [_setSwitch addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];
    }
    return _setSwitch;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
