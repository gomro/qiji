//
//  QJMineSetVersionTableViewCell.h
//  QJBox
//
//  Created by macm on 2022/9/23.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJMineSetVersionTableViewCell : UITableViewCell
@property (nonatomic, strong) UILabel *contentLabel;

@end

NS_ASSUME_NONNULL_END
