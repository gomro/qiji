//
//  QJMineNotifyTableViewCell.h
//  QJBox
//
//  Created by macm on 2022/6/16.
//

#import "QJBaseTableViewCell.h"
#import "QJMineMsgSetModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJMineNotifyTableViewCell : QJBaseTableViewCell
@property (nonatomic, strong) QJMineMsgSetModel *model;
@end

NS_ASSUME_NONNULL_END
