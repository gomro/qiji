//
//  QJMineSetVersionTableViewCell.m
//  QJBox
//
//  Created by macm on 2022/9/23.
//

#import "QJMineSetVersionTableViewCell.h"

@interface QJMineSetVersionTableViewCell ()
//@property (nonatomic, strong) UIView *leftView;

@end

@implementation QJMineSetVersionTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor clearColor];
        [self setupUI];
    }
    return self;
}

- (void)setupUI {
//    [self.contentView addSubview:self.leftView];
    [self.contentView addSubview:self.contentLabel];
    
//    [self.leftView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(10);
//        make.top.mas_equalTo(5);
//        make.width.height.mas_equalTo(5);
//    }];
    
    [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-10);
        make.left.mas_equalTo(10);
        make.top.mas_equalTo(5);
        make.bottom.mas_equalTo(-5);
        make.height.mas_greaterThanOrEqualTo(100);
    }];
    
}

//- (UIView *)leftView {
//    if (!_leftView) {
//        _leftView = [[UIView alloc] init];
//        _leftView.backgroundColor = UIColorFromRGB(0x1c1c1c);
//        _leftView.layer.cornerRadius = 2.5;
//    }
//    return _leftView;
//}

- (UILabel *)contentLabel {
    if (!_contentLabel) {
        _contentLabel = [[UILabel alloc] init];
        _contentLabel.textColor = UIColorFromRGB(0x1c1c1c);
        _contentLabel.font = FontRegular(14);
        _contentLabel.numberOfLines = 0;
        _contentLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _contentLabel;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
