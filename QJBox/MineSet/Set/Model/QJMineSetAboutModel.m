//
//  QJMineSetAboutModel.m
//  QJBox
//
//  Created by macm on 2022/9/5.
//

#import "QJMineSetAboutModel.h"

@implementation QJMineSetAboutModel
+ (NSDictionary *)modelCustomPropertyMapper{
    return @{@"desc":@"description"};
}
@end
