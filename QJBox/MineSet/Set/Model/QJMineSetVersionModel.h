//
//  QJMineSetVersionModel.h
//  QJBox
//
//  Created by macm on 2022/9/23.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJMineSetVersionModel : NSObject
@property (nonatomic, copy) NSString *androidApk; //    android更新包地址    string
@property (nonatomic, assign) BOOL isNew; //    是否为最新版本    boolean
@property (nonatomic, copy) NSString *name; //    项目名称    string
@property (nonatomic, copy) NSString *type; //    系统类型 （0，android，1:ios）,可用值:ANDROID,IOS    string
@property (nonatomic, copy) NSString *updateDesc; //    更新提示语    string
@property (nonatomic, copy) NSString *updateType; //    更新类型 （0，提示更新 1，强制更新）,可用值:FORCE,UN_FORCE    string
@property (nonatomic, copy) NSString *version; //    版本号    string
@end

NS_ASSUME_NONNULL_END
