//
//  QJMineSetAboutModel.h
//  QJBox
//
//  Created by macm on 2022/9/5.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJMineSetAboutModel : NSObject
@property (nonatomic, copy) NSString *desc; //    平台描述    string
@property (nonatomic, copy) NSString *logo; //    平台LOGO    string
@property (nonatomic, copy) NSString *name; //    平台名称    string
@property (nonatomic, copy) NSString *servicePhone; //    客服电话    string
@end

NS_ASSUME_NONNULL_END
