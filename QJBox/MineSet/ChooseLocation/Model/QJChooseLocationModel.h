//
//  QJChooseLocationModel.h
//  QJBox
//
//  Created by macm on 2022/6/15.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJChooseLocationModel : NSObject
@property (nonatomic ,copy) NSString *name;

@property (nonatomic ,strong) NSArray <QJChooseLocationModel*>*models;

@property (nonatomic ,copy) NSString *code;

@property (nonatomic ,assign) NSInteger tag;

/// for cell
@property (nonatomic ,assign) BOOL isSelected;
@end

NS_ASSUME_NONNULL_END
