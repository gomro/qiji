//
//  QJChooseLocationDataTool.h
//  QJBox
//
//  Created by macm on 2022/6/15.
//

#import <Foundation/Foundation.h>
#import "QJChooseLocationModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJChooseLocationDataTool : NSObject

+ (void)parseContainCodeDataFromJson:(void(^)(BOOL finish, NSArray <QJChooseLocationModel *>* locationCLModels))complete;
+ (void)parseNoCodeDataFromJson:(void(^)(BOOL finish, NSArray <QJChooseLocationModel *>* locationCLModels))complete;
+ (NSBundle *)resourceBundle;
+ (NSString *)getPathFromZSChooseLocationBundleForResource:(NSString *)name ofType:(NSString *)ext inDirectory:(NSString *)subpath;
@end

NS_ASSUME_NONNULL_END
