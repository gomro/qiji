//
//  QJMineMsgSetModel.m
//  QJBox
//
//  Created by macm on 2022/7/11.
//

#import "QJMineMsgSetModel.h"

@implementation QJMineMsgSetModel

+ (NSDictionary *)modelCustomPropertyMapper{
    return @{@"idStr":@"id"};
}

@end
