//
//  QJMineMsgSetModel.h
//  QJBox
//
//  Created by macm on 2022/7/11.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJMineMsgSetModel : NSObject
@property (nonatomic, copy) NSString *idStr;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, assign) BOOL status;
@end

NS_ASSUME_NONNULL_END
