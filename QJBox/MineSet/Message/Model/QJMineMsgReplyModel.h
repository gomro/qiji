//
//  QJMineMsgReplyModel.h
//  QJBox
//
//  Created by macm on 2022/7/7.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJMineMsgReplyTargetModel : NSObject
@property (nonatomic, copy) NSString *content; //    评论内容    string
@property (nonatomic, copy) NSString *coverImage; //    评论人头像    string
@property (nonatomic, copy) NSString *createTime; //    评论时间    string
@property (nonatomic, assign) BOOL deleted; //    评论是否已被删除    boolean
@property (nonatomic, copy) NSString *idStr; //        string
@property (nonatomic, copy) NSString *nickname; //    评论人昵称    string
@property (nonatomic, copy) NSString *uid; //    评论人uid    string
@end

@interface QJMineMsgReplyModel : NSObject
@property (nonatomic, assign) BOOL isMineReply; //是否是我的回复
@property (nonatomic, assign) BOOL isShowAll; //是否展开评论
@property (nonatomic, assign) BOOL isShowReplyAll; //是否展开我的回复

@property (nonatomic, copy) NSString *content;//content    回复内容    string
@property (nonatomic, copy) NSString *coverImage;//coverImage    头像    string
@property (nonatomic, copy) NSString *createTime;//createTime    时间    string
@property (nonatomic, copy) NSString *idStr;//id    评论id    string
@property (nonatomic, copy) NSString *nickname;//nickname    昵称    string
@property (nonatomic, copy) NSString *sourceReplyCoverImage;//sourceReplyCoverImage    回复头像    string
@property (nonatomic, copy) NSString *sourceReplyNickname;//sourceReplyNickname    回复昵称    string
@property (nonatomic, copy) NSString *sourceReplyUid;//sourceReplyUid    回复评论id    string
@property (nonatomic, copy) NSString *sourceDescription;//sourceDescription    来源描述    string
@property (nonatomic, copy) NSString *sourceId;//sourceId    来源id    string
@property (nonatomic, copy) NSString *sourceReplyContent;//sourceReplyContent    上级回复的内容    string
@property (nonatomic, assign) BOOL sourceReplyDeleted;//sourceReplyDeleted    上级评论是否已被删除    boolean
@property (nonatomic, copy) NSString *sourceReplyTime;//sourceReplyTime    上级回复的时间    string
@property (nonatomic, strong) NSArray *sourceTags;//sourceTags    来源标签    array    string
@property (nonatomic, assign) NSInteger type;//type    来源类型: 动态=2    integer
@property (nonatomic, copy) NSString *uid;//uid    用户id    string
@property (nonatomic, strong) NSDictionary *targetPage; //    跳转目标页面路由信息    App页面路由    App页面路由
@property (nonatomic, copy) NSString *targetType; //    回复目标类型: 主要内容=1, 评论=2,可用值:COMMENT,MAIN_CONTENT    string
@property (nonatomic, strong) QJMineMsgReplyTargetModel *targetComment; 

@end

NS_ASSUME_NONNULL_END
