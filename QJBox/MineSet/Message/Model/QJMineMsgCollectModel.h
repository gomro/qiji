//
//  QJMineMsgCollectModel.h
//  QJBox
//
//  Created by macm on 2022/7/13.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

//点赞 关注 收藏
@interface QJMineMsgCollectModel : NSObject
@property (nonatomic, copy) NSString *coverImage;
@property (nonatomic, copy) NSString *createTime;
@property (nonatomic, copy) NSString *idStr;
@property (nonatomic, copy) NSString *nickname;
@property (nonatomic, copy) NSString *sourceDescription;
@property (nonatomic, copy) NSString *sourceId;
@property (nonatomic, copy) NSString *sourceReplyContent;
@property (nonatomic, strong) NSArray *sourceTags;
@property (nonatomic, assign) NSInteger type;
@property (nonatomic, copy) NSString *uid;
@end

NS_ASSUME_NONNULL_END
