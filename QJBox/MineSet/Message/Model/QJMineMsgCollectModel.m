//
//  QJMineMsgCollectModel.m
//  QJBox
//
//  Created by macm on 2022/7/13.
//

#import "QJMineMsgCollectModel.h"

@implementation QJMineMsgCollectModel
+ (NSDictionary *)modelCustomPropertyMapper{
    return @{@"idStr":@"id"};
}
@end
