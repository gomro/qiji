//
//  QJMineMsgSystemModel.h
//  QJBox
//
//  Created by macm on 2022/7/12.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJMineMsgSystemModel : NSObject
@property (nonatomic, copy) NSString *avatar;
@property (nonatomic, copy) NSString *descriptionStr;
@property (nonatomic, copy) NSString *idStr;
@property (nonatomic, copy) NSString *target;
@property (nonatomic, copy) NSString *createTime;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, strong) NSDictionary *targetPage;
@end

NS_ASSUME_NONNULL_END
