//
//  QJMineMsgSystemModel.m
//  QJBox
//
//  Created by macm on 2022/7/12.
//

#import "QJMineMsgSystemModel.h"

@implementation QJMineMsgSystemModel
+ (NSDictionary *)modelCustomPropertyMapper{
    return @{@"idStr":@"id",@"descriptionStr":@"description"};
}
@end
