//
//  QJMineMsgCenterModel.h
//  QJBox
//
//  Created by macm on 2022/7/11.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

// 获取消息中心汇总信息
@interface QJMineMsgCenterModel : NSObject

@property (nonatomic, copy) NSString *lastTime;
@property (nonatomic, copy) NSString *lastDescription;
@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSString *typeName;
@property (nonatomic, assign) NSInteger unreadCount;

@end

NS_ASSUME_NONNULL_END
