//
//  QJMineMsgOfficialModel.h
//  QJBox
//
//  Created by macm on 2022/7/12.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJMineMsgOfficialModel : NSObject
@property (nonatomic, copy) NSString *avatar;
@property (nonatomic, copy) NSString *coverImg;
@property (nonatomic, copy) NSString *idStr;
@property (nonatomic, copy) NSString *nickName;
@property (nonatomic, copy) NSString *publishTime;
@property (nonatomic, copy) NSString *title;

@end

NS_ASSUME_NONNULL_END
