//
//  QJMineMsgOfficialTableViewCell.h
//  QJBox
//
//  Created by macm on 2022/7/5.
//

#import <UIKit/UIKit.h>
#import "QJMineMsgOfficialModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJMineMsgOfficialTableViewCell : UITableViewCell
@property (nonatomic, strong) QJMineMsgOfficialModel *model;
@end

NS_ASSUME_NONNULL_END
