//
//  QJMineMsgMyReplyTableViewCell.m
//  QJBox
//
//  Created by macm on 2022/7/8.
//

#import "QJMineMsgMyReplyTableViewCell.h"
#import "QJMineMsgTagView.h"
#import "QJMySpaceViewController.h"

@interface QJMineMsgMyReplyTableViewCell ()
@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) UIImageView *headImageView;
//@property (nonatomic, strong) UIImageView *bloggerImage;
@property (nonatomic, strong) UILabel *timeLabel;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) YYLabel *contentLabel;
@property (nonatomic, strong) UILabel *isDelLabel;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) QJMineMsgTagView *tagView;
@property (nonatomic, strong) UILabel *sourceContentLabel;
@property (strong, nonatomic) MASConstraint *topConstraint;

@end

@implementation QJMineMsgMyReplyTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor clearColor];
        [self setupUI];
    }
    return self;
}

- (void)setupUI {
    [self.contentView addSubview:self.bgView];
    [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            
        make.top.mas_equalTo(14);
        make.left.mas_equalTo(14);
        make.right.mas_equalTo(-14);
        make.bottom.mas_equalTo(0);
    }];
    
    [self.bgView addSubview:self.headImageView];
    [self.headImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(14);
        make.left.mas_equalTo(14);
        make.width.height.mas_equalTo(40*kWScale);
        
    }];
    
//    [self.bgView addSubview:self.bloggerImage];
//    [self.bloggerImage mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.bottom.mas_equalTo(self.headImageView.mas_bottom).mas_offset(0);
//        make.right.equalTo(self.headImageView.mas_right).offset(0);
//        make.width.mas_equalTo(12*kWScale);
//        make.height.mas_equalTo(14*kWScale);
//    }];
    
    [self.bgView addSubview:self.nameLabel];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.headImageView.mas_top).mas_offset(0);
        make.left.mas_equalTo(self.headImageView.mas_right).mas_offset(12);
        make.width.mas_lessThanOrEqualTo(200*kWScale);
    }];

    [self.bgView addSubview:self.timeLabel];
    [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.nameLabel);
        make.top.mas_equalTo(self.nameLabel.mas_bottom).mas_offset(5);
    }];
    
    [self.bgView addSubview:self.contentLabel];
    [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {

        make.top.mas_equalTo(self.headImageView.mas_bottom).mas_offset(10);
        make.right.mas_equalTo(-14);
        make.left.mas_equalTo(14);
    }];
    
    [self.bgView addSubview:self.sourceContentLabel];
    [self.sourceContentLabel mas_makeConstraints:^(MASConstraintMaker *make) {

        make.top.mas_equalTo(self.contentLabel.mas_bottom).mas_offset(5);
        make.left.mas_equalTo(14);
        make.right.mas_equalTo(-14);
    }];
    
    [self.bgView addSubview:self.isDelLabel];
    
    [self.bgView addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        self.topConstraint = make.top.mas_equalTo(self.contentLabel.mas_bottom).mas_offset(14);
        make.right.mas_equalTo(-14);
        make.left.mas_equalTo(14);
        make.height.mas_equalTo(37*kWScale);
        
    }];
    
    [self.isDelLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.titleLabel.mas_top).mas_offset(10*kWScale);
        make.right.mas_equalTo(-14);
        make.left.mas_equalTo(14);
        make.height.mas_equalTo(30*kWScale);
    }];
    
    [self.bgView addSubview:self.tagView];
    [self.tagView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(self.titleLabel.mas_bottom).mas_offset(10);
        make.left.mas_equalTo(14);
        make.right.mas_equalTo(-54);
        make.height.mas_equalTo(17*kWScale);
        make.bottom.mas_equalTo(-14);
    }];
    
}

- (void)setModel:(QJMineMsgReplyModel *)model {
    _model = model;
    
    self.titleLabel.text = [NSString stringWithFormat:@"  |   %@", _model.sourceDescription];
    self.nameLabel.text = _model.nickname;
    [self.headImageView setImageWithURL:[NSURL URLWithString:[model.coverImage checkImageUrlString]] placeholder:[UIImage imageNamed:@"mine_head"]];
    [self setContentLabelNum:_model.content];

    [self.tagView reloadTagView:_model.sourceTags BgColor:nil TextColor:nil Radius:2 RectCorner:UIRectCornerAllCorners];
    
    [self.isDelLabel setHidden:!_model.targetComment.deleted];
    NSInteger deleteHeight = _model.targetComment.deleted ? 20 : 0;
    [self.topConstraint uninstall];
    
    NSString *targetString = @"";
    if ([_model.targetType isEqualToString:@"2"]) {
        targetString = @"回复评论";
        NSString *sourceContent = [NSString stringWithFormat:@"%@：%@",_model.targetComment.nickname,_model.targetComment.content];
        NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:sourceContent];
        NSRange toUserRange = [sourceContent rangeOfString:_model.targetComment.nickname];
        [attrStr setColor:UIColorHex(7e7e7e) range:toUserRange];
        self.sourceContentLabel.attributedText = attrStr;
        [self.sourceContentLabel setHidden:NO];

        [self.titleLabel mas_updateConstraints:^(MASConstraintMaker *make) {
            self.topConstraint = make.top.mas_equalTo(self.sourceContentLabel.mas_bottom).mas_offset(14+deleteHeight);
        }];
        
    } else {
        targetString = @"评价内容";
        [self.sourceContentLabel setHidden:YES];

        [self.titleLabel mas_updateConstraints:^(MASConstraintMaker *make) {
            self.topConstraint = make.top.mas_equalTo(self.contentLabel.mas_bottom).mas_offset(14+deleteHeight);
        }];
        
    }
    self.timeLabel.text = [NSString stringWithFormat:@"%@%@", [QJAppTool formatDayTime:_model.createTime],targetString];
    //    self.bloggerImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"qj_bozhuLevel_%ld",self.model.bloggerLevel]];

    
}

// 设置回复文字内容
- (void)setContentLabelNum:(NSString *)content {

    NSMutableAttributedString *contentString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@", content] attributes:@{NSFontAttributeName : FontMedium(14), NSForegroundColorAttributeName : UIColorFromRGB(0x474849)}];

    self.contentLabel.attributedText = contentString;

    // 如果是展开的状态，Label的行数是0，默认行数是2
    if (_model.isShowReplyAll) {
        self.contentLabel.numberOfLines = 0;
        // 如果是展开的状态，说明文字数量大于两行，文字末尾加上 收起 功能
        [self expandString];
    } else {
        self.contentLabel.numberOfLines = 2;
        // 如果是默认状态，文字可能是多行，更改文字过多时结尾的...,增加点击事件
        [self addSeeMoreButtonInLabel:self.contentLabel];
    }

}

// 多行显示内容的尾部将 ... 改成 查看全部并添加点击事件
- (void)addSeeMoreButtonInLabel:(YYLabel *)label {
    UIFont *font = kFont(14);
 
    NSString *moreString = @" ...全部";
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@", moreString]];
    NSRange expandRange = [text.string rangeOfString:moreString];
    
    [text addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0x007aff) range:expandRange];
    [text addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0x474849) range:NSMakeRange(0, expandRange.location)];

    //添加点击事件
    YYTextHighlight *hi = [YYTextHighlight new];
    [text setTextHighlight:hi range:[text.string rangeOfString:moreString]];
    
    __weak typeof(self) weakSelf = self;
    hi.tapAction = ^(UIView *containerView, NSAttributedString *text, NSRange range, CGRect rect) {
        //点击展开
        if (weakSelf.showAllClick) {
            weakSelf.showAllClick(YES);
        }

    };
    
    text.font = font;
    
    YYLabel *seeMore = [YYLabel new];
    seeMore.attributedText = text;
    [seeMore sizeToFit];
    
    NSAttributedString *truncationToken = [NSAttributedString attachmentStringWithContent:seeMore contentMode:UIViewContentModeCenter attachmentSize:seeMore.frame.size alignToFont:text.font alignment:YYTextVerticalAlignmentTop];
    
    label.truncationToken = truncationToken;
}

- (NSAttributedString *)appendAttriStringWithFont:(UIFont *)font {
    if (!font) {
        font = kFont(14);
    }
    
    NSString *appendText = @" 收起 ";
    NSMutableAttributedString *append = [[NSMutableAttributedString alloc] initWithString:appendText attributes:@{NSFontAttributeName : font, NSForegroundColorAttributeName : UIColorFromRGB(0x007aff)}];
    
    YYTextHighlight *hi = [YYTextHighlight new];
    [append setTextHighlight:hi range:[append.string rangeOfString:appendText]];
    
    __weak typeof(self) weakSelf = self;
    hi.tapAction = ^(UIView *containerView, NSAttributedString *text, NSRange range, CGRect rect) {
        //点击收起
        if (weakSelf.showAllClick) {
            weakSelf.showAllClick(NO);
        }
    };
    
    return append;
}

- (void)expandString {
    NSMutableAttributedString *attri = [_contentLabel.attributedText mutableCopy];
    [attri appendAttributedString:[self appendAttriStringWithFont:attri.font]];
    self.contentLabel.attributedText = attri;
}

- (void)packUpString {
    NSString *appendText = @" 收起 ";
    NSMutableAttributedString *attri = [_contentLabel.attributedText mutableCopy];
    NSRange range = [attri.string rangeOfString:appendText options:NSBackwardsSearch];

    if (range.location != NSNotFound) {
        [attri deleteCharactersInRange:range];
    }

    self.contentLabel.attributedText = attri;
}

- (UIView *)bgView {
    if (!_bgView) {
        _bgView = [[UIView alloc] init];
        _bgView.backgroundColor = [UIColor whiteColor];
        _bgView.layer.cornerRadius = 10;
    }
    return _bgView;
}

- (void)headAction {
    QJMySpaceViewController *vc = [QJMySpaceViewController new];
    if (self.model.isMineReply) {
        vc.userId = self.model.sourceReplyUid;
    } else {
        vc.userId = self.model.uid;
    }
    [[QJAppTool getCurrentViewController].navigationController pushViewController:vc animated:YES];
}

- (UIImageView *)headImageView {
    if (!_headImageView) {
        _headImageView = [[UIImageView alloc] init];
        _headImageView.layer.cornerRadius = 20*kWScale;
        _headImageView.layer.masksToBounds = YES;
        _headImageView.image = [UIImage imageNamed:@"mine_head"];
        _headImageView.contentMode = UIViewContentModeScaleAspectFill;
        _headImageView.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(headAction)];
        [_headImageView addGestureRecognizer:tap];

    }
    return _headImageView;
}

//- (UIImageView *)bloggerImage {
//    if (!_bloggerImage) {
//        self.bloggerImage = [UIImageView new];
//        self.bloggerImage.image = [UIImage imageNamed:@"avatarLevelIcon"];
//    }
//    return _bloggerImage;
//}

- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc] init];
        _nameLabel.text = @"比克大魔王";
        _nameLabel.textColor = UIColorFromRGB(0x2261a9);
        [_nameLabel setFont:FontMedium(14)];
    }
    return _nameLabel;
}

- (UILabel *)timeLabel {
    if (!_timeLabel) {
        _timeLabel = [[UILabel alloc] init];
        _timeLabel.text = @"1小时前";
        _timeLabel.textColor = UIColorFromRGB(0x919599);
        _timeLabel.textAlignment = NSTextAlignmentRight;
        [_timeLabel setFont:FontMedium(11)];
    }
    return _timeLabel;
}

- (YYLabel *)contentLabel {
    if (!_contentLabel) {
        _contentLabel = [[YYLabel alloc] init];
        _contentLabel.backgroundColor = [UIColor clearColor];
        
        //设置最大宽度 ，如果宽度没有设置好，会出现高度计算不准确，不能自动换行的问题
        _contentLabel.preferredMaxLayoutWidth = kScreenWidth-40;
        
//        _contentLabel.userInteractionEnabled = YES;
//        _contentLabel.textVerticalAlignment = YYTextVerticalAlignmentTop;
    }
    return _contentLabel;
}

- (UILabel *)isDelLabel {
    if (!_isDelLabel) {
        _isDelLabel = [[UILabel alloc] init];
        _isDelLabel.text = @"       该评论已删除";
        _isDelLabel.layer.cornerRadius = 5;
        _isDelLabel.backgroundColor = UIColorFromRGB(0xfafafa);
        _isDelLabel.textColor = UIColorFromRGB(0x919599);
        _isDelLabel.font = FontMedium(12);
    }
    return _isDelLabel;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.text = @"  |   大天使之战，有没有来玩的？";
        _titleLabel.textColor = UIColorFromRGB(0x919599);
        _titleLabel.backgroundColor = UIColorFromRGB(0xfafafa);
        [_titleLabel setFont:FontMedium(14)];
        _titleLabel.layer.cornerRadius = 5;
        _titleLabel.layer.masksToBounds = YES;
    }
    return _titleLabel;
}

- (QJMineMsgTagView *)tagView {
    if (!_tagView) {
        _tagView = [[QJMineMsgTagView alloc] init];
    }
    return _tagView;
}

- (UILabel *)sourceContentLabel {
    if (!_sourceContentLabel) {
        _sourceContentLabel = [[UILabel alloc] init];
        _sourceContentLabel.textColor = UIColorFromRGB(0x474849);
        [_sourceContentLabel setFont:FontRegular(14)];
        _sourceContentLabel.numberOfLines = 2;
    }
    return _sourceContentLabel;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
