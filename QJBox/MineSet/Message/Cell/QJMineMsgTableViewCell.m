//
//  QJMineMsgTableViewCell.m
//  QJBox
//
//  Created by macm on 2022/7/5.
//

#import "QJMineMsgTableViewCell.h"
#import "QJMineMsgUnReadView.h"

@interface QJMineMsgTableViewCell ()
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIImageView *headImageView;
@property (nonatomic, strong) UILabel *timeLabel;
@property (nonatomic, strong) UILabel *descLabel;
@property (nonatomic, strong) QJMineMsgUnReadView *unReadView;

@end

@implementation QJMineMsgTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor clearColor];

        [self setupUI];
    }
    return self;
}

- (void)setupUI {
    [self.contentView addSubview:self.headImageView];
    [self.headImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.width.height.mas_equalTo(44*kWScale);
        make.centerY.equalTo(self.contentView);
        
    }];
    
    [self.contentView addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.headImageView.mas_top).mas_offset(0);
        make.right.mas_equalTo(self.contentView).mas_offset(-30);
        make.left.mas_equalTo(self.headImageView.mas_right).mas_offset(10);

    }];

    [self.contentView addSubview:self.timeLabel];
    [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {

        make.top.mas_equalTo(self.headImageView.mas_top).mas_offset(0);
        make.right.mas_equalTo(self.contentView).mas_offset(-15);

    }];
    
    [self.contentView addSubview:self.descLabel];
    [self.descLabel mas_makeConstraints:^(MASConstraintMaker *make) {

        make.bottom.mas_equalTo(self.headImageView.mas_bottom);
        make.right.mas_equalTo(self.contentView).mas_offset(-60);
        make.left.mas_equalTo(self.headImageView.mas_right).mas_offset(10);
    }];
    
    [self.contentView addSubview:self.unReadView];
    [self.unReadView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.right.mas_equalTo(-32);
        make.height.mas_equalTo(14*kWScale);
        make.width.mas_greaterThanOrEqualTo(14*kWScale);
        make.centerY.equalTo(self.descLabel);
    }];
    
    [self.contentView addSubview:self.lineView];
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(0);
        make.left.mas_equalTo(15);
        make.right.mas_equalTo(-15);
        make.height.mas_equalTo(1);
    }];
    
}

- (void)setModel:(QJMineMsgCenterModel *)model {
    _model = model;
    
    if ([_model.type isEqualToString:@"0"]) {
        self.headImageView.image = [UIImage imageNamed:@"mine_msg_guan"];
    } else {
        self.headImageView.image = [UIImage imageNamed:@"mine_msg_system"];
    }
    self.titleLabel.text = _model.typeName;
    self.descLabel.text = _model.lastDescription;
    self.timeLabel.text = [QJAppTool formatDayTime:_model.lastTime];
    [self.unReadView reloadUnReadView:_model.unreadCount];
    
}

- (UIImageView *)headImageView {
    if (!_headImageView) {
        _headImageView = [[UIImageView alloc] init];
    }
    return _headImageView;
}

- (QJMineMsgUnReadView *)unReadView {
    if (!_unReadView) {
        _unReadView = [[QJMineMsgUnReadView alloc] init];
        _unReadView.layer.cornerRadius = 7;
    }
    return _unReadView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.text = @"系统消息";
        _titleLabel.textColor = [UIColor blackColor];
        [_titleLabel setFont:FontMedium(16)];
    }
    return _titleLabel;
}

- (UILabel *)timeLabel {
    if (!_timeLabel) {
        _timeLabel = [[UILabel alloc] init];
        _timeLabel.text = @"1小时前";
        _timeLabel.textColor = UIColorFromRGB(0x474849);
        _timeLabel.textAlignment = NSTextAlignmentRight;
        [_timeLabel setFont:FontRegular(12)];
    }
    return _timeLabel;
}

- (UILabel *)descLabel {
    if (!_descLabel) {
        _descLabel = [[UILabel alloc] init];
        _descLabel.text = @"系统刚刚发布了一条信息";
        _descLabel.textColor = UIColorFromRGB(0x999999);
        [_descLabel setFont:FontRegular(14)];
    }
    return _descLabel;
}

- (UIView *)lineView {
    if (!_lineView) {
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor = UIColorFromRGB(0xf0f0f0);
    }
    return _lineView;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
