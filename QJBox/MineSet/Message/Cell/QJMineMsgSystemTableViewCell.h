//
//  QJMineMsgSystemTableViewCell.h
//  QJBox
//
//  Created by macm on 2022/7/5.
//

#import <UIKit/UIKit.h>
#import "QJMineMsgSystemModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJMineMsgSystemTableViewCell : UITableViewCell
@property (nonatomic, strong) QJMineMsgSystemModel *model;

@end

NS_ASSUME_NONNULL_END
