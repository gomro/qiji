//
//  QJMineMsgTagCollectionViewCell.h
//  QJBox
//
//  Created by macm on 2022/7/11.
//

#import <UIKit/UIKit.h>

static CGFloat cellHeight = 17;

NS_ASSUME_NONNULL_BEGIN

@interface QJMineMsgTagCollectionViewCell : UICollectionViewCell
@property (nonatomic, copy) void (^itemClick)(void);
@property (nonatomic, strong) UILabel *tagLabel;
- (void)setLabelTextColor:(UIColor *)textColor bgColor:(UIColor *)bgColor text:(NSString *)text Radius:(CGFloat)radius RectCorner:(UIRectCorner)rectCorner;

+ (CGSize)getSizeWithText:(NSString*)text;

@end

NS_ASSUME_NONNULL_END


