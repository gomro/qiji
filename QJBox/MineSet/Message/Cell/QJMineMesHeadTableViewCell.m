//
//  QJMineMesHeadTableViewCell.m
//  QJBox
//
//  Created by macm on 2022/7/5.
//

#import "QJMineMesHeadTableViewCell.h"
#import "QJMineMsgHeadView.h"
#import "QJMineMsgReplyViewController.h"
#import "QJMineMsgZanViewController.h"
#import "QJMineMsgFocusViewController.h"
#import "QJMineMsgCenterModel.h"

@interface QJMineMesHeadTableViewCell ()
@property (nonatomic, strong) QJMineMsgHeadView *replyView;
@property (nonatomic, strong) QJMineMsgHeadView *zanView;
@property (nonatomic, strong) QJMineMsgHeadView *focusView;
@property (nonatomic, strong) QJMineMsgHeadView *collectView;
@property (nonatomic, strong) UIView *lineView;
@end

@implementation QJMineMesHeadTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor clearColor];
        [self setupUI];
    }
    return self;
}

- (void)setupUI {
    [self.contentView addSubview:self.replyView];
    [self.replyView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.bottom.mas_equalTo(0);
        make.width.mas_equalTo(kScreenWidth/4);
    }];
    
    [self.contentView addSubview:self.zanView];
    [self.zanView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kScreenWidth/4);
        make.top.bottom.mas_equalTo(0);
        make.width.mas_equalTo(kScreenWidth/4);
    }];
    
    [self.contentView addSubview:self.focusView];
    [self.focusView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kScreenWidth/2);
        make.top.bottom.mas_equalTo(0);
        make.width.mas_equalTo(kScreenWidth/4);
    }];
    
    [self.contentView addSubview:self.collectView];
    [self.collectView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo((kScreenWidth/4)*3);
        make.top.bottom.mas_equalTo(0);
        make.width.mas_equalTo(kScreenWidth/4);
    }];
    
    [self.contentView addSubview:self.lineView];
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
            
        make.bottom.left.right.mas_equalTo(0);
        make.height.mas_equalTo(10*kWScale);
    }];
}

- (void)setDataArray:(NSArray *)dataArray {
    _dataArray = dataArray;
    for (QJMineMsgCenterModel *model in _dataArray) {
        if ([model.type isEqualToString:@"2"]) {
            [self.replyView.unReadView reloadUnReadView:model.unreadCount];
        }
        if ([model.type isEqualToString:@"3"]) {
            [self.zanView.unReadView reloadUnReadView:model.unreadCount];
        }
        if ([model.type isEqualToString:@"4"]) {
            [self.focusView.unReadView reloadUnReadView:model.unreadCount];
        }
        if ([model.type isEqualToString:@"5"]) {
            [self.collectView.unReadView reloadUnReadView:model.unreadCount];
        }
    }
}

- (QJMineMsgHeadView *)replyView {
    if (!_replyView) {
        _replyView = [[QJMineMsgHeadView alloc] init];
        _replyView.titleLabel.text = @"回复我的";
        _replyView.bgImageView.image = [UIImage imageNamed:@"mine_msg_reply"];

        @weakify(self)
        _replyView.msgHeadClick = ^(NSInteger index) {
            @strongify(self);
            QJMineMsgReplyViewController *vc = [[QJMineMsgReplyViewController alloc] init];
            [self.viewController.navigationController pushViewController:vc animated:YES];
            
        };
        
    }
    return _replyView;
}

- (QJMineMsgHeadView *)zanView {
    if (!_zanView) {
        _zanView = [[QJMineMsgHeadView alloc] init];
        _zanView.titleLabel.text = @"赞";
        _zanView.bgImageView.image = [UIImage imageNamed:@"mine_msg_zan"];
        @weakify(self)
        _zanView.msgHeadClick = ^(NSInteger index) {
            @strongify(self);
            QJMineMsgZanViewController *vc = [[QJMineMsgZanViewController alloc] init];
            [self.viewController.navigationController pushViewController:vc animated:YES];
            
        };
    }
    return _zanView;
}

- (QJMineMsgHeadView *)focusView {
    if (!_focusView) {
        _focusView = [[QJMineMsgHeadView alloc] init];
        _focusView.titleLabel.text = @"关注";
        _focusView.bgImageView.image = [UIImage imageNamed:@"mine_msg_focus"];

        @weakify(self)
        _focusView.msgHeadClick = ^(NSInteger index) {
            @strongify(self);
            QJMineMsgFocusViewController *vc = [[QJMineMsgFocusViewController alloc] init];
            [self.viewController.navigationController pushViewController:vc animated:YES];
            
        };
        
    }
    return _focusView;
}

- (QJMineMsgHeadView *)collectView {
    if (!_collectView) {
        _collectView = [[QJMineMsgHeadView alloc] init];
        _collectView.titleLabel.text = @"收藏";
        _collectView.bgImageView.image = [UIImage imageNamed:@"mine_msg_collect"];

        @weakify(self)
        _collectView.msgHeadClick = ^(NSInteger index) {
            @strongify(self);
            QJMineMsgZanViewController *vc = [[QJMineMsgZanViewController alloc] init];
            vc.titleString = @"收藏";
            [self.viewController.navigationController pushViewController:vc animated:YES];
            
        };
    }
    return _collectView;
}

- (UIView *)lineView {
    if (!_lineView) {
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor = UIColorFromRGB(0xf0f0f0);
    }
    return _lineView;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
