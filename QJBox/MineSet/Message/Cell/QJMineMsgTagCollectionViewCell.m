//
//  QJMineMsgTagCollectionViewCell.m
//  QJBox
//
//  Created by macm on 2022/7/11.
//

#import "QJMineMsgTagCollectionViewCell.h"

@interface QJMineMsgTagCollectionViewCell ()


@end

@implementation QJMineMsgTagCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = UIColor.clearColor;
        [self createView];
    }
    return self;
}

#pragma mark - 视图

- (void)createView {
    [self.contentView addSubview:self.tagLabel];
}

- (void)setLabelTextColor:(UIColor *)textColor bgColor:(UIColor *)bgColor text:(NSString *)text Radius:(CGFloat)radius RectCorner:(UIRectCorner)rectCorner {
    self.tagLabel.text = text;
    self.tagLabel.textColor = textColor;
    self.tagLabel.layer.backgroundColor = bgColor.CGColor;
    
    self.tagLabel.frame = CGRectMake(0, 0, [self getTagLabelRect].size.width, [self getTagLabelRect].size.height);
    
    [self.tagLabel showCorner:radius rectCorner:rectCorner];
}

- (UILabel *)tagLabel {
    if (!_tagLabel) {
        _tagLabel = [[UILabel alloc] init];
        _tagLabel.font = FontRegular(10);
        _tagLabel.text = @"天使之战";
        _tagLabel.textAlignment = NSTextAlignmentCenter;
        _tagLabel.textColor = UIColorFromRGB(0xffc78e);
    }
    return _tagLabel;
}

// 获取文字宽高
- (CGRect)getTagLabelRect {
    CGRect rect = [self.tagLabel.text boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, cellHeight) options:NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesFontLeading |NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: self.tagLabel.font} context:nil];
    rect.size.width += 10;
    rect.size.height = cellHeight;
    return rect;
}

#pragma mark — 实现自适应文字宽度的关键步骤:item的layoutAttributes
- (UICollectionViewLayoutAttributes *)preferredLayoutAttributesFittingAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes{
    
    UICollectionViewLayoutAttributes *attributes = [super preferredLayoutAttributesFittingAttributes:layoutAttributes];
    
    attributes.frame = [self getTagLabelRect];
    return attributes;
}

@end


