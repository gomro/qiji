//
//  QJMineMsgOfficialTableViewCell.m
//  QJBox
//
//  Created by macm on 2022/7/5.
//

#import "QJMineMsgOfficialTableViewCell.h"

@interface QJMineMsgOfficialTableViewCell ()
@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) UIImageView *headImageView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *timeLabel;
@property (nonatomic, strong) UIImageView *contentImageView;
@property (nonatomic, strong) UILabel *contentLabel;

@end

@implementation QJMineMsgOfficialTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor clearColor];
        [self setupUI];
    }
    return self;
}

- (void)setupUI {
    
    [self.contentView addSubview:self.bgView];
    [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(0);
        make.left.mas_offset(0);
        make.right.mas_equalTo(0);
        make.top.mas_equalTo(10);
    }];
    
    [self.bgView addSubview:self.headImageView];
    [self.headImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            
        make.top.left.mas_equalTo(10);
        make.width.height.mas_equalTo(18*kWScale);
    }];
    
    [self.bgView addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            
        make.left.equalTo(self.headImageView.mas_right).mas_offset(10);
        make.centerY.equalTo(self.headImageView);
        make.width.mas_lessThanOrEqualTo(200*kWScale);
    }];
    
    [self.bgView addSubview:self.timeLabel];
    [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            
        make.centerY.equalTo(self.headImageView);
        make.right.equalTo(self.bgView).mas_offset(-10);
    }];
    
    [self.bgView addSubview:self.contentImageView];
    [self.contentImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            
        make.top.mas_equalTo(self.headImageView.mas_bottom).mas_offset(10);
        make.left.mas_equalTo(10);
        make.right.mas_equalTo(-10);
        make.height.mas_equalTo(150*kWScale);

    }];
    
    [self.bgView addSubview:self.contentLabel];
    [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.contentImageView.mas_bottom).mas_offset(10);
        make.left.mas_equalTo(10);
        make.right.mas_equalTo(-10);
        make.bottom.mas_equalTo(-10);
    }];
    
    
}

- (void)setModel:(QJMineMsgOfficialModel *)model {
    _model = model;
    
    self.titleLabel.text = _model.nickName;
    self.timeLabel.text = [QJAppTool formatDayTime:_model.publishTime];
    self.contentLabel.text = _model.title;
    [self.headImageView setImageWithURL:[NSURL URLWithString:[model.avatar checkImageUrlString]] placeholder:[UIImage imageNamed:@"mine_head"]];
    [self.contentImageView setImageWithURL:[NSURL URLWithString:[model.coverImg checkImageUrlString]] placeholder:[UIImage imageNamed:@"mine_msg_system_image"]];

}

- (UIView *)bgView {
    if (!_bgView) {
        _bgView = [[UIView alloc] init];
        _bgView.backgroundColor = [UIColor whiteColor];
    }
    return _bgView;
}

- (UIImageView *)headImageView {
    if (!_headImageView) {
        _headImageView = [[UIImageView alloc] init];
        _headImageView.layer.cornerRadius = 9;
        _headImageView.layer.masksToBounds = YES;
        _headImageView.image = [UIImage imageNamed:@"mine_head"];
        _headImageView.contentMode = UIViewContentModeScaleAspectFill;

    }
    return _headImageView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.text = @"盒子官方";
        _titleLabel.textColor = UIColorFromRGB(0x151515);
        [_titleLabel setFont:FontMedium(14)];
    }
    return _titleLabel;
}

- (UILabel *)timeLabel {
    if (!_timeLabel) {
        _timeLabel = [[UILabel alloc] init];
        _timeLabel.text = @"1小时前";
        _timeLabel.textColor = UIColorFromRGB(0x919599);
        _timeLabel.textAlignment = NSTextAlignmentRight;
        [_timeLabel setFont:FontMedium(11)];
    }
    return _timeLabel;
}

- (UIImageView *)contentImageView {
    if (!_contentImageView) {
        _contentImageView = [[UIImageView alloc] init];
        _contentImageView.image = [UIImage imageNamed:@"mine_msg_system_image"];
        _contentImageView.layer.cornerRadius = 6;
        _contentImageView.layer.masksToBounds = YES;
        _contentImageView.contentMode = UIViewContentModeScaleAspectFill;
        
    }
    return _contentImageView;
}

- (UILabel *)contentLabel {
    if (!_contentLabel) {
        _contentLabel = [[UILabel alloc] init];
        _contentLabel.text = @"最新发布一款传奇游戏，免费开放";
        _contentLabel.textColor = UIColorFromRGB(0x000000);
        [_contentLabel setFont:FontSemibold(14)];
    }
    return _contentLabel;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
