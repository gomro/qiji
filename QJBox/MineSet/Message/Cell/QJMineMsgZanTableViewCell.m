//
//  QJMineMsgZanTableViewCell.m
//  QJBox
//
//  Created by macm on 2022/7/5.
//

#import "QJMineMsgZanTableViewCell.h"
#import "QJMineMsgTagView.h"
#import "QJMySpaceViewController.h"

@interface QJMineMsgZanTableViewCell ()
@property (nonatomic, strong) UIView *bgView;

@property (nonatomic, strong) UIImageView *headImageView;
//@property (nonatomic, strong) UIImageView *bloggerImage;
@property (nonatomic, strong) UILabel *timeLabel;
@property (nonatomic, strong) UILabel *nameLabel;

@property (nonatomic, strong) UIView *titleBgView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) QJMineMsgTagView *tagView;

@end

@implementation QJMineMsgZanTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor clearColor];
        [self setupUI];
    }
    return self;
}

- (void)setupUI {
    [self.contentView addSubview:self.bgView];
    [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            
        make.top.mas_equalTo(14);
        make.left.mas_equalTo(14);
        make.right.mas_equalTo(-14);
        make.bottom.mas_equalTo(0);
    }];
    
    [self.bgView addSubview:self.headImageView];
    [self.headImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(14);
        make.left.mas_equalTo(14);
        make.width.height.mas_equalTo(40*kWScale);
        
    }];
    
//    [self.bgView addSubview:self.bloggerImage];
//    [self.bloggerImage mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.bottom.mas_equalTo(self.headImageView.mas_bottom).mas_offset(0);
//        make.right.equalTo(self.headImageView.mas_right).offset(0);
//        make.width.mas_equalTo(12*kWScale);
//        make.height.mas_equalTo(14*kWScale);
//    }];
    
    [self.bgView addSubview:self.nameLabel];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.headImageView.mas_top).mas_offset(0);
        make.left.mas_equalTo(self.headImageView.mas_right).mas_offset(10);
        make.width.mas_lessThanOrEqualTo(200*kWScale);
    }];
    
    [self.bgView addSubview:self.typeLabel];
    [self.typeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.headImageView);
        make.left.mas_equalTo(self.headImageView.mas_right).mas_offset(10);

    }];

    [self.bgView addSubview:self.timeLabel];
    [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {

        make.right.mas_equalTo(-14);
        make.centerY.equalTo(self.nameLabel);
    }];
    
    [self.bgView addSubview:self.titleBgView];
    [self.titleBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(self.headImageView.mas_bottom).mas_offset(10);
        make.left.mas_equalTo(14);
        make.right.mas_equalTo(-14);

    }];
    
    [self.titleBgView addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(8);
        make.right.mas_equalTo(-5);
        make.left.mas_equalTo(5);
        make.bottom.mas_equalTo(-8);

    }];
    
    [self.bgView addSubview:self.tagView];
    [self.tagView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(self.titleBgView.mas_bottom).mas_offset(10);
        make.left.mas_equalTo(14);
        make.right.mas_equalTo(-14);
        make.height.mas_equalTo(17*kWScale);
        make.bottom.mas_equalTo(-14);
    }];
    
}

- (void)setModel:(QJMineMsgCollectModel *)model {
    _model = model;
    [self.headImageView setImageWithURL:[NSURL URLWithString:[model.coverImage checkImageUrlString]] placeholder:[UIImage imageNamed:@"mine_head"]];
    self.nameLabel.text = _model.nickname;
    self.timeLabel.text = [QJAppTool formatDayTime:_model.createTime];
    self.titleLabel.text = [NSString stringWithFormat:@"  |   %@", _model.sourceDescription];

    [self.tagView reloadTagView:_model.sourceTags BgColor:nil TextColor:nil Radius:2 RectCorner:UIRectCornerAllCorners];
    //    self.bloggerImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"qj_bozhuLevel_%ld",self.model.bloggerLevel]];

}

- (void)headAction {
    QJMySpaceViewController *vc = [QJMySpaceViewController new];
    vc.userId = self.model.uid;
    [[QJAppTool getCurrentViewController].navigationController pushViewController:vc animated:YES];
}

- (UIView *)bgView {
    if (!_bgView) {
        _bgView = [[UIView alloc] init];
        _bgView.backgroundColor = [UIColor whiteColor];
        _bgView.layer.cornerRadius = 10;
    }
    return _bgView;
}

- (UIImageView *)headImageView {
    if (!_headImageView) {
        _headImageView = [[UIImageView alloc] init];
        _headImageView.layer.cornerRadius = 20*kWScale;
        _headImageView.layer.masksToBounds = YES;
        _headImageView.image = [UIImage imageNamed:@"mine_head"];
        _headImageView.contentMode = UIViewContentModeScaleAspectFill;
        _headImageView.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(headAction)];
        [_headImageView addGestureRecognizer:tap];

    }
    return _headImageView;
}

//- (UIImageView *)bloggerImage {
//    if (!_bloggerImage) {
//        self.bloggerImage = [UIImageView new];
//        self.bloggerImage.image = [UIImage imageNamed:@"avatarLevelIcon"];
//    }
//    return _bloggerImage;
//}

- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc] init];
        _nameLabel.text = @"比克大魔王";
        _nameLabel.textColor = UIColorFromRGB(0x2261a9);
        [_nameLabel setFont:FontMedium(14)];
    }
    return _nameLabel;
}

- (UILabel *)typeLabel {
    if (!_typeLabel) {
        _typeLabel = [[UILabel alloc] init];
        _typeLabel.text = @"点赞了我";
        _typeLabel.textColor = UIColorFromRGB(0x919599);
        [_typeLabel setFont:FontMedium(11)];
    }
    return _typeLabel;
}

- (UILabel *)timeLabel {
    if (!_timeLabel) {
        _timeLabel = [[UILabel alloc] init];
        _timeLabel.text = @"1小时前";
        _timeLabel.textColor = UIColorFromRGB(0x919599);
        _timeLabel.textAlignment = NSTextAlignmentRight;
        [_timeLabel setFont:FontMedium(11)];
    }
    return _timeLabel;
}

- (UIView *)titleBgView {
    if (!_titleBgView) {
        _titleBgView = [[UIView alloc] init];
        _titleBgView.backgroundColor = UIColorFromRGB(0xfafafa);
        _titleBgView.layer.cornerRadius = 5;
        _titleBgView.layer.masksToBounds = YES;
    }
    return _titleBgView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.text = @"  |   大天使之战，有没有来玩的？";
        _titleLabel.textColor = UIColorFromRGB(0x919599);
        _titleLabel.numberOfLines = 2;
        [_titleLabel setFont:FontMedium(14)];
    }
    return _titleLabel;
}

- (QJMineMsgTagView *)tagView {
    if (!_tagView) {
        _tagView = [[QJMineMsgTagView alloc] init];
    }
    return _tagView;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
