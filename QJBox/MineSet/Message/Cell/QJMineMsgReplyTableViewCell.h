//
//  QJMineMsgReplyTableViewCell.h
//  QJBox
//
//  Created by macm on 2022/7/5.
//

#import <UIKit/UIKit.h>
#import "QJMineMsgReplyModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJMineMsgReplyTableViewCell : UITableViewCell
@property (nonatomic, copy) void(^replyClick)(NSInteger index);
@property (nonatomic, copy) void(^showAllClick)(BOOL isShow);

@property (nonatomic, strong) QJMineMsgReplyModel *model;
@end

NS_ASSUME_NONNULL_END
