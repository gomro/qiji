//
//  QJMineMsgMyReplyTableViewCell.h
//  QJBox
//
//  Created by macm on 2022/7/8.
//

#import <UIKit/UIKit.h>
#import "QJMineMsgReplyModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJMineMsgMyReplyTableViewCell : UITableViewCell
@property (nonatomic, copy) void(^replyClick)(NSInteger index);
@property (nonatomic, copy) void(^showAllClick)(BOOL isShow);
@property (nonatomic, copy) void(^showAllHeadViewClick)(BOOL isShow);

@property (nonatomic, strong) QJMineMsgReplyModel *model;
@end

NS_ASSUME_NONNULL_END
