//
//  QJMineMsgTableViewCell.h
//  QJBox
//
//  Created by macm on 2022/7/5.
//

#import <UIKit/UIKit.h>
#import "QJMineMsgCenterModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface QJMineMsgTableViewCell : UITableViewCell
@property (nonatomic, strong) QJMineMsgCenterModel *model;
@property (nonatomic, strong) UIView *lineView;

@end

NS_ASSUME_NONNULL_END
