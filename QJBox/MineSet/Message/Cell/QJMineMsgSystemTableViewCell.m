//
//  QJMineMsgSystemTableViewCell.m
//  QJBox
//
//  Created by macm on 2022/7/5.
//

#import "QJMineMsgSystemTableViewCell.h"

@interface QJMineMsgSystemTableViewCell ()
@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *timeLabel;
@property (nonatomic, strong) UIImageView *contentImageView;
@property (nonatomic, strong) UILabel *contentLabel;

@end

@implementation QJMineMsgSystemTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor clearColor];
        [self setupUI];
    }
    return self;
}

- (void)setupUI {
    
    [self.contentView addSubview:self.bgView];
    [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(0);
        make.left.mas_offset(10);
        make.right.mas_equalTo(-10);
        make.top.mas_equalTo(10);
    }];
    
    [self.bgView addSubview:self.contentImageView];
    [self.contentImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            
        make.top.mas_equalTo(10);
        make.left.mas_equalTo(10);
        make.width.height.mas_equalTo(50*kWScale);
        make.bottom.mas_equalTo(-10);
    }];
    
    [self.bgView addSubview:self.timeLabel];
    [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(15);
        make.width.mas_equalTo(70*kWScale);
        make.right.equalTo(self.bgView).mas_offset(-10);
    }];
    
    [self.bgView addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.timeLabel);
        make.left.equalTo(self.contentImageView.mas_right).mas_offset(10);
        make.right.equalTo(self.timeLabel.mas_left).mas_offset(-5);
    }];
    
    [self.bgView addSubview:self.contentLabel];
    [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.titleLabel.mas_bottom).mas_offset(6);
        make.left.mas_equalTo(self.contentImageView.mas_right).mas_offset(10);
        make.right.mas_equalTo(-10);
    }];

}

- (void)setModel:(QJMineMsgSystemModel *)model {
    _model = model;
    
    self.titleLabel.text = _model.title;
    self.timeLabel.text = [QJAppTool formatDayTime:_model.createTime];
    self.contentLabel.text = _model.descriptionStr;
    [self.contentImageView setImageWithURL:[NSURL URLWithString:[model.avatar checkImageUrlString]] placeholder:[UIImage imageNamed:@"mine_head"]];

}

- (UIView *)bgView {
    if (!_bgView) {
        _bgView = [[UIView alloc] init];
        _bgView.backgroundColor = [UIColor whiteColor];
    }
    return _bgView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.text = @"盒子官方";
        _titleLabel.textColor = UIColorFromRGB(0x474849);
        [_titleLabel setFont:FontSemibold(16)];
    }
    return _titleLabel;
}

- (UILabel *)timeLabel {
    if (!_timeLabel) {
        _timeLabel = [[UILabel alloc] init];
        _timeLabel.text = @"1小时前";
        _timeLabel.textColor = UIColorFromRGB(0x919599);
        _timeLabel.textAlignment = NSTextAlignmentRight;
        [_timeLabel setFont:FontRegular(12)];
    }
    return _timeLabel;
}

- (UIImageView *)contentImageView {
    if (!_contentImageView) {
        _contentImageView = [[UIImageView alloc] init];
        _contentImageView.layer.cornerRadius = 25*kWScale;
        _contentImageView.layer.masksToBounds = YES;
        _contentImageView.contentMode = UIViewContentModeScaleAspectFill;
    }
    return _contentImageView;
}

- (UILabel *)contentLabel {
    if (!_contentLabel) {
        _contentLabel = [[UILabel alloc] init];
        _contentLabel.text = @"最新发布一款传奇游戏，免费开放";
        _contentLabel.textColor = UIColorFromRGB(0x171717);
        [_contentLabel setFont:FontRegular(14)];
        _contentLabel.numberOfLines = 1;
    }
    return _contentLabel;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
