//
//  QJMineMsgFocusTableViewCell.h
//  QJBox
//
//  Created by macm on 2022/7/5.
//

#import <UIKit/UIKit.h>
#import "QJMineMsgCollectModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJMineMsgFocusTableViewCell : UITableViewCell
@property (nonatomic, strong) QJMineMsgCollectModel *model;

@end

NS_ASSUME_NONNULL_END
