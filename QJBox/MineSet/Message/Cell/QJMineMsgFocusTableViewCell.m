//
//  QJMineMsgFocusTableViewCell.m
//  QJBox
//
//  Created by macm on 2022/7/5.
//

#import "QJMineMsgFocusTableViewCell.h"

@interface QJMineMsgFocusTableViewCell ()
@property (nonatomic, strong) UIView *bgView;

@property (nonatomic, strong) UIImageView *headImageView;
//@property (nonatomic, strong) UIImageView *bloggerImage;
@property (nonatomic, strong) UILabel *timeLabel;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *typeLabel;

@end

@implementation QJMineMsgFocusTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor clearColor];
        [self setupUI];
    }
    return self;
}

- (void)setupUI {
    [self.contentView addSubview:self.bgView];
    [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            
        make.top.mas_equalTo(0);
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.bottom.mas_equalTo(0);
    }];
    
    [self.bgView addSubview:self.headImageView];
    [self.headImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(14);
        make.left.mas_equalTo(14);
        make.width.height.mas_equalTo(40*kWScale);
        make.bottom.mas_equalTo(-14);
        
    }];
    
//    [self.bgView addSubview:self.bloggerImage];
//    [self.bloggerImage mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.bottom.mas_equalTo(self.headImageView.mas_bottom).mas_offset(0);
//        make.right.equalTo(self.headImageView.mas_right).offset(0);
//        make.width.mas_equalTo(12*kWScale);
//        make.height.mas_equalTo(14*kWScale);
//    }];
    
    [self.bgView addSubview:self.nameLabel];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.headImageView);
        make.left.mas_equalTo(self.headImageView.mas_right).mas_offset(10);
        make.width.mas_lessThanOrEqualTo(200*kWScale);
    }];
    
    [self.bgView addSubview:self.typeLabel];
    [self.typeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.headImageView);
        make.left.mas_equalTo(self.headImageView.mas_right).mas_offset(10);

    }];

    [self.bgView addSubview:self.timeLabel];
    [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {

        make.centerY.mas_equalTo(self.bgView);
        make.right.mas_equalTo(-14);

    }];
    
}

- (void)setModel:(QJMineMsgCollectModel *)model {
    _model = model;
    [self.headImageView setImageWithURL:[NSURL URLWithString:[model.coverImage checkImageUrlString]] placeholder:[UIImage imageNamed:@"mine_head"]];
    self.nameLabel.text = _model.nickname;
    self.timeLabel.text = [QJAppTool formatDayTime:_model.createTime];
    //    self.bloggerImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"qj_bozhuLevel_%ld",self.model.bloggerLevel]];

}

- (UIView *)bgView {
    if (!_bgView) {
        _bgView = [[UIView alloc] init];
        _bgView.backgroundColor = [UIColor whiteColor];
        _bgView.layer.cornerRadius = 10*kWScale;
    }
    return _bgView;
}

- (UIImageView *)headImageView {
    if (!_headImageView) {
        _headImageView = [[UIImageView alloc] init];
        _headImageView.layer.cornerRadius = 20*kWScale;
        _headImageView.layer.masksToBounds = YES;
        _headImageView.image = [UIImage imageNamed:@"mine_head"];
        _headImageView.contentMode = UIViewContentModeScaleAspectFill;

    }
    return _headImageView;
}

//- (UIImageView *)bloggerImage {
//    if (!_bloggerImage) {
//        self.bloggerImage = [UIImageView new];
//        self.bloggerImage.image = [UIImage imageNamed:@"avatarLevelIcon"];
//    }
//    return _bloggerImage;
//}

- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc] init];
        _nameLabel.text = @"比克大魔王";
        _nameLabel.textColor = UIColorFromRGB(0x2261a9);
        [_nameLabel setFont:FontMedium(14)];
    }
    return _nameLabel;
}

- (UILabel *)typeLabel {
    if (!_typeLabel) {
        _typeLabel = [[UILabel alloc] init];
        _typeLabel.text = @"关注了我";
        _typeLabel.textColor = UIColorFromRGB(0x919599);
        [_typeLabel setFont:FontMedium(11)];
    }
    return _typeLabel;
}

- (UILabel *)timeLabel {
    if (!_timeLabel) {
        _timeLabel = [[UILabel alloc] init];
        _timeLabel.text = @"1小时前";
        _timeLabel.textColor = UIColorFromRGB(0x919599);
        _timeLabel.textAlignment = NSTextAlignmentRight;
        [_timeLabel setFont:FontMedium(11)];
    }
    return _timeLabel;
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
