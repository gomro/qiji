//
//  QJMineMsgReplyTableViewCell.m
//  QJBox
//
//  Created by macm on 2022/7/5.
//

#import "QJMineMsgReplyTableViewCell.h"
#import "QJMineMsgReplyHeadView.h"

@interface QJMineMsgReplyTableViewCell ()
@property (nonatomic, strong) QJMineMsgReplyHeadView *replyView;

@end

@implementation QJMineMsgReplyTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor clearColor];
        [self setupUI];
    }
    return self;
}

- (void)setupUI {
    [self.contentView addSubview:self.replyView];
    [self.replyView mas_makeConstraints:^(MASConstraintMaker *make) {
            
        make.top.mas_equalTo(14);
        make.left.mas_equalTo(14);
        make.right.mas_equalTo(-14);
        make.bottom.mas_equalTo(0);
    }];
    
}

- (void)setModel:(QJMineMsgReplyModel *)model {
    _model = model;
    
    // 如果是我的回复，隐藏回复按钮，显示我的回复view
//    if (_model.isMineReply) {
//        [self.replyButton setHidden:YES];
//        [self.bottomView setHidden:NO];
//    } else {
//        [self.replyButton setHidden:NO];
//        [self.bottomView setHidden:YES];
//    }

//    [self setContentLabelNum];
    self.replyView.model = self.model;
}

- (QJMineMsgReplyHeadView *)replyView {
    if (!_replyView) {
        _replyView = [[QJMineMsgReplyHeadView alloc] init];
        _replyView.backgroundColor = [UIColor whiteColor];
        _replyView.layer.cornerRadius = 10;
        @weakify(self);
        _replyView.showAllHeadViewClick = ^(BOOL isShow) {
            @strongify(self);
            if (self.showAllClick) {
                self.showAllClick(isShow);
            }
        };
        _replyView.replyHeadViewClick = ^(NSInteger index) {
            @strongify(self);
            if (self.replyClick) {
                self.replyClick(0);
            }
            
        };
    }
    return _replyView;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
