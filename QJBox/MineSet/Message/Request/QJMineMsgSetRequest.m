//
//  QJMineMsgSetRequest.m
//  QJBox
//
//  Created by macm on 2022/7/12.
//

#import "QJMineMsgSetRequest.h"

@implementation QJMineMsgSetRequest

/**修改消息提醒设置信息*/
- (void)requestNotifySetting {
    self.urlStr = @"/notify/settings";
    self.requestType = YTKRequestMethodPUT;
    self.serializerType = YTKRequestSerializerTypeJSON;
    [self start];
}

- (id)requestArgument {
    return self.param;
}

@end
