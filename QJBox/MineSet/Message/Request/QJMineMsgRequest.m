//
//  QJMineMsgRequest.m
//  QJBox
//
//  Created by macm on 2022/7/11.
//

#import "QJMineMsgRequest.h"

@implementation QJMineMsgRequest

- (void)sendRequest_completion:(RequestBlock)completion {
    [self start];
    
    [self setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        if (completion) {
            completion(ResponseSuccess, request);
        }
        if (!ResponseSuccess) {
            [[QJAppTool getCurrentViewController].view makeToast:EncodeStringFromDic(request.responseObject, @"message") duration:2 position:CSToastPositionCenter image:[UIImage imageNamed:@"tips_toast"]];
        }
    }];
    
    [self setFailureCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        if (completion) {
            completion(NO, request);
        }
        [[QJAppTool getCurrentViewController].view makeToast:@"网络请求失败" duration:2 position:CSToastPositionCenter image:[UIImage imageNamed:@"tips_toast"]];
    }];
}

/**获取消息中心汇总信息*/
- (void)requestNotifyCenter_completion:(RequestBlock)completion {
    self.urlStr = @"/notify/center";
    self.requestType = YTKRequestMethodGET;
    
    [self sendRequest_completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        if (completion) {
            completion(isSuccess, request);
        }
    }];
    
}

/**获取消息提醒设置信息*/
- (void)requestNotifySettings_completion:(RequestBlock)completion {
    self.urlStr = @"/notify/settings";
    self.requestType = YTKRequestMethodGET;
    
    [self sendRequest_completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        if (completion) {
            completion(isSuccess, request);
        }
    }];
}

/**获取用户未读消息数量*/
- (void)requestNotifyUnread_completion:(RequestBlock)completion {
    self.urlStr = @"/notify/unread";
    self.requestType = YTKRequestMethodGET;
    
    [self start];
    
    [self setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        if (completion) {
            completion(ResponseSuccess, request);
        }
    }];
    
    [self setFailureCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        if (completion) {
            completion(NO, request);
        }
    }];
}

/**公告列表*/
- (void)requestAnnouncementList:(NSInteger)page completion:(RequestBlock)completion {
    self.dic = @{@"page":@(page), @"size":@(10)};
    self.urlStr = @"/announcement/list";
    self.requestType = YTKRequestMethodGET;
    
    [self sendRequest_completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        if (completion) {
            completion(isSuccess, request);
        }
    }];
}

/**公告详情*/
- (void)requestAnnouncementDetail:(NSString *)aId completion:(RequestBlock)completion {
    self.urlStr = [NSString stringWithFormat:@"/announcement/detail/%@", aId];
    self.requestType = YTKRequestMethodGET;
    
    [self sendRequest_completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        if (completion) {
            completion(isSuccess, request);
        }
    }];
}

/**系统消息列表*/
- (void)requestSysnotifyList:(NSInteger)page completion:(RequestBlock)completion {
    self.dic = @{@"page":@(page), @"size":@(10)};
    self.urlStr = @"/sysnotify/list";
    self.requestType = YTKRequestMethodGET;
    
    [self sendRequest_completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        if (completion) {
            completion(isSuccess, request);
        }
    }];
}

/**收藏我的-消息列表*/
- (void)requestNotifyCollectList:(NSInteger)page completion:(RequestBlock)completion {
    self.dic = @{@"page":@(page), @"size":@(10)};
    self.urlStr = @"/collect/notify/list";
    self.requestType = YTKRequestMethodGET;
    
    [self sendRequest_completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        if (completion) {
            completion(isSuccess, request);
        }
    }];
}

/**收藏我的-删除消息*/
- (void)requestNotifyCollect:(NSString *)msgId completion:(RequestBlock)completion {
    self.urlStr = [NSString stringWithFormat:@"/collect/notify/%@",msgId];
    self.requestType = YTKRequestMethodDELETE;
    
    [self sendRequest_completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        if (completion) {
            completion(isSuccess, request);
        }
    }];
}

/**关注我的-消息列表*/
- (void)requestNotifyFollowList:(NSInteger)page completion:(RequestBlock)completion {
    self.dic = @{@"page":@(page), @"size":@(10)};
    self.urlStr = @"/follow/notify/list";
    self.requestType = YTKRequestMethodGET;
    
    [self sendRequest_completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        if (completion) {
            completion(isSuccess, request);
        }
    }];
}

/**关注我的-删除消息*/
- (void)requestNotifyFollow:(NSString *)msgId completion:(RequestBlock)completion {
    self.urlStr = [NSString stringWithFormat:@"/follow/notify/%@",msgId];
    self.requestType = YTKRequestMethodDELETE;
    
    [self sendRequest_completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        if (completion) {
            completion(isSuccess, request);
        }
    }];
}

/**点赞我的-消息列表*/
- (void)requestNotifyLikeList:(NSInteger)page completion:(RequestBlock)completion {
    self.dic = @{@"page":@(page), @"size":@(10)};
    self.urlStr = @"/like/notify/list";
    self.requestType = YTKRequestMethodGET;
    
    [self sendRequest_completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        if (completion) {
            completion(isSuccess, request);
        }
    }];
}

/**点赞我的-删除消息*/
- (void)requestNotifyLike:(NSString *)msgId completion:(RequestBlock)completion {
    self.urlStr = [NSString stringWithFormat:@"/like/notify/%@",msgId];
    self.requestType = YTKRequestMethodDELETE;
    
    [self sendRequest_completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        if (completion) {
            completion(isSuccess,request);
        }
    }];
}

/**回复我的-消息列表*/
- (void)requestCommentNotifyListIreply:(NSInteger)page completion:(RequestBlock)completion {
    self.dic = @{@"page":@(page), @"size":@(10)};
    self.urlStr = @"/comment/notify/list/replyme";
    self.requestType = YTKRequestMethodGET;
    
    [self sendRequest_completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        if (completion) {
            completion(isSuccess, request);
        }
    }];
}

/**我回复的-消息列表*/
- (void)requestCommentNotifyListReplyme:(NSInteger)page completion:(RequestBlock)completion {
    self.dic = @{@"page":@(page), @"size":@(10)};
    self.urlStr = @"/comment/notify/list/ireply";
    self.requestType = YTKRequestMethodGET;
    
    [self sendRequest_completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        if (completion) {
            completion(isSuccess, request);
        }
    }];
}

/**回复我的-删除消息
 * type    消息类型，我回复的=0, 回复我的=1
 */
- (void)requestCommentNotify:(NSString *)msgId type:(BOOL)type completion:(RequestBlock)completion {
    self.urlStr = [NSString stringWithFormat:@"/comment/notify/%@/%d",msgId,type];
    self.requestType = YTKRequestMethodDELETE;
    
    [self sendRequest_completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        if (completion) {
            completion(isSuccess, request);
        }
    }];
}

/**我回复的-删除消息 */
- (void)requestMyCommentNotify:(NSString *)msgId completion:(RequestBlock)completion {
    self.urlStr = [NSString stringWithFormat:@"/comment/%@",msgId];
    self.requestType = YTKRequestMethodDELETE;
    
    [self sendRequest_completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        if (completion) {
            completion(isSuccess, request);
        }
    }];
}

/**新增评论
 * content    评论内容     string
 * replyCommentId    回复评论id    string
 * sourceId    原记录id， 如动态id、攻略id等     string
 * type    0-游戏，1-攻略，2-动态   string
 */
- (void)requestAddComment:(NSString *)content replyCommentId:(NSString *)replyCommentId sourceId:(NSString *)sourceId levelComment:(NSString *)levelComment type:(NSString *)type completion:(RequestBlock)completion {
    self.dic = @{@"content":content,@"replyCommentId":replyCommentId,@"sourceId":sourceId,@"levelComment":levelComment,@"type":type};
    self.urlStr = @"/comment";
    self.requestType = YTKRequestMethodPOST;
    self.serializerType = YTKRequestSerializerTypeJSON;
    
    [self sendRequest_completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        if (completion) {
            completion(isSuccess, request);
        }
    }];
}
@end
