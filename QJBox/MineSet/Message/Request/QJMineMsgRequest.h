//
//  QJMineMsgRequest.h
//  QJBox
//
//  Created by macm on 2022/7/11.
//

#import "QJBaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

typedef void(^RequestBlock)(BOOL isSuccess,YTKBaseRequest * _Nonnull request);

@interface QJMineMsgRequest : QJBaseRequest

/**获取消息中心汇总信息*/
- (void)requestNotifyCenter_completion:(RequestBlock)completion;

/**获取消息提醒设置信息*/
- (void)requestNotifySettings_completion:(RequestBlock)completion;

/**获取用户未读消息数量*/
- (void)requestNotifyUnread_completion:(RequestBlock)completion;

/**公告列表*/
- (void)requestAnnouncementList:(NSInteger)page completion:(RequestBlock)completion;

/**公告详情*/
- (void)requestAnnouncementDetail:(NSString *)aId completion:(RequestBlock)completion;

/**系统消息列表*/
- (void)requestSysnotifyList:(NSInteger)page completion:(RequestBlock)completion;

/**收藏我的-消息列表*/
- (void)requestNotifyCollectList:(NSInteger)page completion:(RequestBlock)completion;

/**收藏我的-删除消息*/
- (void)requestNotifyCollect:(NSString *)msgId completion:(RequestBlock)completion;

/**关注我的-消息列表*/
- (void)requestNotifyFollowList:(NSInteger)page completion:(RequestBlock)completion;

/**关注我的-删除消息*/
- (void)requestNotifyFollow:(NSString *)msgId completion:(RequestBlock)completion;

/**点赞我的-消息列表*/
- (void)requestNotifyLikeList:(NSInteger)page completion:(RequestBlock)completion;

/**点赞我的-删除消息*/
- (void)requestNotifyLike:(NSString *)msgId completion:(RequestBlock)completion;

/**回复我的-消息列表*/
- (void)requestCommentNotifyListIreply:(NSInteger)page completion:(RequestBlock)completion;

/**我回复的-消息列表*/
- (void)requestCommentNotifyListReplyme:(NSInteger)page completion:(RequestBlock)completion;

/**回复我的-删除消息
 * type    消息类型，我回复的=0, 回复我的=1
 */
- (void)requestCommentNotify:(NSString *)msgId type:(BOOL)type completion:(RequestBlock)completion;
/**我回复的-删除消息 */
- (void)requestMyCommentNotify:(NSString *)msgId completion:(RequestBlock)completion;
/**新增评论
 * content    评论内容     string
 * replyCommentId    回复评论id    string
 * sourceId    原记录id，如动态id、攻略id等     string
 * type    0-游戏，1-攻略，2-动态   string
 */
- (void)requestAddComment:(NSString *)content replyCommentId:(NSString *)replyCommentId sourceId:(NSString *)sourceId levelComment:(NSString *)levelComment type:(NSString *)type completion:(RequestBlock)completion;

@end

NS_ASSUME_NONNULL_END
