//
//  QJMineMsgSetRequest.h
//  QJBox
//
//  Created by macm on 2022/7/12.
//

#import "QJBaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJMineMsgSetRequest : QJBaseRequest

/**修改消息提醒设置信息*/
- (void)requestNotifySetting;

@property (nonatomic, strong) NSArray *param;

@end

NS_ASSUME_NONNULL_END
