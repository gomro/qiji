//
//  QJMineMsgUnReadView.m
//  QJBox
//
//  Created by macm on 2022/7/5.
//

#import "QJMineMsgUnReadView.h"

@interface QJMineMsgUnReadView ()
@property (nonatomic, strong) UILabel *numLabel;

@end

@implementation QJMineMsgUnReadView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = UIColorFromRGB(0xec1c24);
        [self createView];
        [self setHidden:YES];
        
    }
    return self;
}

- (void)createView {
    [self addSubview:self.numLabel];
    [self.numLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            
        make.top.mas_equalTo(3);
        make.bottom.mas_equalTo(-3);
        make.left.mas_equalTo(3);
        make.right.mas_equalTo(-3);
    }];
}

- (void)reloadUnReadView:(NSInteger)num {
    if (num > 0) {
        [self setHidden:NO];
        if (num > 99) {
            self.numLabel.text = [NSString stringWithFormat:@"99+"];
        } else {
            self.numLabel.text = [NSString stringWithFormat:@"%ld", num];
        }
        
    }  else {
        [self setHidden:YES];
    }
}

- (UILabel *)numLabel {
    if (!_numLabel) {
        _numLabel  = [[UILabel alloc] init];
        _numLabel.textColor = [UIColor whiteColor];
        [_numLabel setFont:FontMedium(8)];
        _numLabel.text = @"1";
        _numLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _numLabel;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
