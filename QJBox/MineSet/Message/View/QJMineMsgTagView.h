//
//  QJMineMsgTagView.h
//  QJBox
//
//  Created by macm on 2022/7/11.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJMineMsgTagView : UIView

/**
 * 刷新标签文字和颜色
 * Title:标签文字 （必传）
 * BgColor：标签背景颜色 (可传空)
 * TextColor：标签文字颜色 (可传空)
 * Radius：圆角的数值
 * RectCorner：圆角的方向
 */
- (void)reloadTagView:(NSArray *)title BgColor:(NSArray *__nullable)bgColor TextColor:(NSArray *__nullable)textColor Radius:(CGFloat)radius RectCorner:(UIRectCorner)rectCorner;
@end

NS_ASSUME_NONNULL_END
