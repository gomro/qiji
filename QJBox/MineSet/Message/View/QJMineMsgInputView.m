//
//  QJMineMsgInputView.m
//  QJBox
//
//  Created by macm on 2022/7/6.
//

#import "QJMineMsgInputView.h"
#import "ICChatBoxFaceView.h"

#import "ICMessageConst.h"
#import "XZEmotion.h"
#import "NSString+Extension.h"
#import "UIView+Extension.h"

#import "QJMineMsgRequest.h"

// 表情列表高度
#define FACEHEIGHT 260 + Bottom_SN_iPhoneX_OR_LATER_SPACE

// 文字输入默认高度
#define TEXTHEIGHT 37

@interface QJMineMsgInputView ()<UITextViewDelegate>
@property (nonatomic, strong) UIButton *faceButton;
@property (nonatomic, strong) ICChatBoxFaceView *faceListView;

@property (nonatomic, assign) CGFloat keyBoardHeight;
@property (nonatomic, assign) CGFloat textHieght;

@end

@implementation QJMineMsgInputView

- (instancetype)init {
    self = [super init];
    if (self) {
         
        self.frame = [UIScreen mainScreen].bounds;
        self.backgroundColor = RGBAlpha(0, 0, 0, 0.4);
        
        self.textHieght = TEXTHEIGHT;
        
        [self addNotification];
        
        [self createView];
        
    }
    return self;
}

// 监听通知
- (void)addNotification {
    // 添加对键盘的监控
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(emotionDidSelected:) name:GXEmotionDidSelectNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deleteBtnClicked) name:GXEmotionDidDeleteNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sendMessage) name:GXEmotionDidSendNotification object:nil];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)setModel:(QJMineMsgReplyModel *)model {
    _model = model;
    self.textView.placeholder = [NSString stringWithFormat:@"回复 %@",model.nickname];
}

//选中表情
- (void)emotionDidSelected:(NSNotification *)notifi {
    XZEmotion *emotion = notifi.userInfo[GXSelectEmotionKey];
    if (emotion.code) {
        [self.textView insertText:emotion.code.emoji];
        [self.textView scrollRangeToVisible:NSMakeRange(self.textView.text.length, 0)];
    } else if (emotion.face_name) {
        [self.textView insertText:emotion.face_name];
    }
}

// 删除表情
- (void)deleteBtnClicked {
    [self.textView deleteBackward];
}

//发送消息
- (void)sendMessage {
    
    if (self.textView.text.length == 0 || [[self.textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0) {
        [self makeToast:@"不能回复空白评论"];
        return;
    }
    [self sendRequest];
    
}

- (void)sendRequest {
    [QJAppTool showHUDLoading];
    @weakify(self);
    QJMineMsgRequest *request = [[QJMineMsgRequest alloc] init];
    [request requestAddComment:self.textView.text replyCommentId:_model.idStr sourceId:_model.sourceId levelComment:@"5" type:[NSString stringWithFormat:@"%ld", _model.type] completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        [QJAppTool hideHUDLoading];
        @strongify(self);
        if (isSuccess) {
            [[QJAppTool getCurrentViewController].view makeToast:@"回复评论成功"];
            [self fadeOut];
        }
        
    }];
    
}

//按下发送
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ([text isEqualToString:@"\n"]) {
        
        if ([[self.textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0) {
            [QJAppTool showWarningToast:@"不能发送空白消息"];
        } else {
            [self sendMessage];
        }
        return NO;//这里返回NO，就代表return键值失效，即页面上按下return，不会出现换行，如果为yes，则输入页面会换行
    }
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView {
    UITextRange *selectedRange = [textView markedTextRange];
    NSString * newText = [textView textInRange:selectedRange]; //获取高亮部分
    if(newText.length>0)
        return;
   
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 3;// 字体的行间距
    NSDictionary *attributes = @{NSFontAttributeName:kFont(14),NSParagraphStyleAttributeName:paragraphStyle};
    textView.attributedText = [[NSAttributedString alloc] initWithString:textView.text attributes:attributes];
}

- (void)keyBoardWillShow:(NSNotification *) note {
    [self.faceButton setSelected:NO];
    // 获取用户信息
    NSDictionary *userInfo = [NSDictionary dictionaryWithDictionary:note.userInfo];
    // 获取键盘高度
    CGRect keyBoardBounds  = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    self.keyBoardHeight = keyBoardBounds.size.height;
    // 获取键盘动画时间
    CGFloat animationTime  = [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];

    // 定义好动作
    void (^animation)(void) = ^void(void) {
        self.toolView.transform = CGAffineTransformMakeTranslation(0, - self.keyBoardHeight - self.textHieght - 20);
    };

    if (animationTime > 0) {
        [UIView animateWithDuration:animationTime animations:animation];
    } else {
        animation();
    }
    
}

- (void)keyBoardWillHide:(NSNotification *) note {
//    // 获取用户信息
//    NSDictionary *userInfo = [NSDictionary dictionaryWithDictionary:note.userInfo];
//    // 获取键盘动画时间
//    CGFloat animationTime  = [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
//
//    // 定义好动作
//    void (^animation)(void) = ^void(void) {
//        self.toolView.transform = CGAffineTransformIdentity;
//    };
//
//    if (animationTime > 0) {
//        [UIView animateWithDuration:animationTime animations:animation];
//    } else {
//        animation();
//    }
}

- (void)createView {
    
    [self addSubview:self.toolView];
    
    [self.toolView addSubview:self.textView];
    
    [self.toolView addSubview:self.faceButton];
    
    [self addSubview:self.faceListView];
    
    @weakify(self);
    self.textView.textHeightChangedBlock = ^(NSString *text, CGFloat textHeight) {
        @strongify(self);

        if (textHeight < 37) {
            textHeight = 37;
        }
        self.textHieght = textHeight;
        [self changeTextViewHeight];
    
    };
}

- (void)changeTextViewHeight {
    self.textView.height = self.textHieght;
    self.toolView.transform = CGAffineTransformMakeTranslation(0, - self.keyBoardHeight - self.textHieght - 20);
    self.faceButton.y = self.textHieght - 30 + 10;
}

- (void)faceAction {
    self.faceButton.selected = !self.faceButton.selected;
    if (self.faceButton.selected) {
        [self.textView resignFirstResponder];
        self.keyBoardHeight = FACEHEIGHT;
        
        // 定义好动作
        void (^animation)(void) = ^void(void) {
            self.toolView.transform = CGAffineTransformMakeTranslation(0, - self.keyBoardHeight - self.textHieght - 20);
            self.faceListView.transform = CGAffineTransformMakeTranslation(0, - self.keyBoardHeight);
        };

        [UIView animateWithDuration:0.25 animations:animation];

    } else {
        [self.textView becomeFirstResponder];
        
        // 定义好动作
        void (^animation)(void) = ^void(void) {
            self.faceListView.transform = CGAffineTransformIdentity;
        };

        [UIView animateWithDuration:0.25 animations:animation];
    
    }
}

- (void)show {
    UIWindow *keywindow = [[UIApplication sharedApplication] keyWindow];
    [keywindow addSubview:self];
    [self fadeIn];
}

- (void)fadeIn {
    [self.textView becomeFirstResponder];
//    self.transform = CGAffineTransformMakeScale(1.3, 1.3);
    self.alpha = 0;
    [UIView animateWithDuration:0.20 animations:^{
        self.alpha = 1;
//        self.transform = CGAffineTransformMakeScale(1, 1);
    }];
}

- (void)fadeOut {
    [self endEditing:YES];
    // 清除所有输入数据及状态
//    UITextRange *textRange = [self.textView textRangeFromPosition:self.textView.beginningOfDocument toPosition:self.textView.endOfDocument];
//    [self.textView replaceRange:textRange withText:@""];
//    self.textHieght = TEXTHEIGHT;
//    [self.faceButton setSelected:NO];
//    [self changeTextViewHeight];
    
    [UIView animateWithDuration:0.35 animations:^{
        self.toolView.transform = CGAffineTransformMakeTranslation(0, 1000);
        self.faceListView.transform = CGAffineTransformMakeTranslation(0, 1000);
//        self.transform = CGAffineTransformMakeScale(1.3, 1.3);
        self.alpha = 0.0;
    } completion:^(BOOL finished) {
        if (finished) {
            [self removeFromSuperview];
        }
    }];
}

- (UIView *)toolView {
    if (!_toolView) {
        _toolView = [[UIView alloc] initWithFrame:CGRectMake(0, kScreenHeight, kScreenWidth, 1000)];
        _toolView.backgroundColor = [UIColor whiteColor];
    }
    return _toolView;
}

- (QJMineMsgTextView *)textView {
    if (!_textView) {
        _textView = [[QJMineMsgTextView alloc] initWithFrame:CGRectMake(16, 8, kScreenWidth - 32, 37)];
        _textView.backgroundColor = UIColorFromRGB(0xf5f5f5);
        _textView.delegate = self;
        _textView.font = kFont(14);
//        _textView.placeholder = @"回复 飞翔的奥迪";
        _textView.maxNumberOfLines = 5;
        _textView.returnKeyType = UIReturnKeySend;
    }
    return _textView;
}

- (UIButton *)faceButton {
    if (!_faceButton) {
        _faceButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _faceButton.frame = CGRectMake(kScreenWidth - 50, 10, 30, 30);
        [_faceButton setImage:[UIImage imageNamed:@"ToolViewEmotion"] forState:UIControlStateNormal];
        [_faceButton setImage:[UIImage imageNamed:@"ToolViewKeyboard"] forState:UIControlStateSelected];

        [_faceButton addTarget:self action:@selector(faceAction) forControlEvents:UIControlEventTouchUpInside];
        [_faceButton setHidden:YES];
    }
    return _faceButton;
}

- (ICChatBoxFaceView *)faceListView {
    if (!_faceListView) {
        _faceListView = [[ICChatBoxFaceView alloc] initWithFrame:CGRectMake(0, kScreenHeight, kScreenWidth, FACEHEIGHT - Bottom_SN_iPhoneX_OR_LATER_SPACE)];
    }
    return _faceListView;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];

    UIView *view = [touch view];

    if (view == self) {
        [self fadeOut];
    }
    
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
