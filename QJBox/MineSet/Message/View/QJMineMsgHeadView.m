//
//  QJMineMsgHeadView.m
//  QJBox
//
//  Created by macm on 2022/7/5.
//

#import "QJMineMsgHeadView.h"
#import "UIView+XWAddForRoundedCorner.h"

@interface QJMineMsgHeadView ()

@end

@implementation QJMineMsgHeadView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self createView];
        
    }
    return self;
}

- (void)createView {
    [self addSubview:self.bgImageView];
    [self.bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(35*kWScale);
        make.width.height.mas_equalTo(44*kWScale);
        make.centerX.mas_equalTo(self);
        
    }];
    
    [self addSubview:self.unReadView];
    [self.unReadView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.bgImageView.mas_top).mas_offset(-6);
        make.height.mas_equalTo(14*kWScale);
        make.width.mas_greaterThanOrEqualTo(14*kWScale);
        make.centerX.mas_equalTo(self.bgImageView.mas_right);
        
    }];

    [self addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {

        make.top.mas_equalTo(self.bgImageView.mas_bottom).mas_offset(10);
        make.left.right.mas_equalTo(0);

    }];
}

- (UIImageView *)bgImageView {
    if (!_bgImageView) {
        _bgImageView = [[UIImageView alloc] init];
    }
    return _bgImageView;
}

- (QJMineMsgUnReadView *)unReadView {
    if (!_unReadView) {
        _unReadView = [[QJMineMsgUnReadView alloc] init];
        [_unReadView xw_roundedCornerWithRadius:6 cornerColor:[UIColor whiteColor] corners:UIRectCornerTopLeft | UIRectCornerBottomRight | UIRectCornerTopRight];

    }
    return _unReadView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.text = @"标题";
        _titleLabel.textColor = UIColorFromRGB(0x474849);
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        [_titleLabel setFont:FontMedium(14)];
    }
    return _titleLabel;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    if (self.msgHeadClick) {
        self.msgHeadClick(0);
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
