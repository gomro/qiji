//
//  QJMineMsgTagView.m
//  QJBox
//
//  Created by macm on 2022/7/11.
//

#import "QJMineMsgTagView.h"
#import "QJMineMsgTagCollectionViewCell.h"

@interface QJMineMsgTagView () <UICollectionViewDelegate, UICollectionViewDataSource>
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) NSArray *titleArray;
@property (nonatomic, strong) NSArray *bgColorArray;
@property (nonatomic, strong) NSArray *textColorArray;
@property (nonatomic, assign) CGFloat radius;
@property (nonatomic, assign) UIRectCorner rectCorner;
@end

@implementation QJMineMsgTagView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        
        [self createView];
        
    }
    return self;
}

- (void)createView {
    [self addSubview:self.collectionView];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
            
        make.top.right.left.bottom.mas_equalTo(0);
    }];
}

/**
 * 刷新标签文字和颜色
 * Title:标签文字 （必传）
 * BgColor：标签背景颜色 (可传空)
 * TextColor：标签文字颜色 (可传空)
 * Radius：圆角的数值
 * RectCorner：圆角的方向
 */
- (void)reloadTagView:(NSArray *)title BgColor:(NSArray *__nullable)bgColor TextColor:(NSArray *__nullable)textColor Radius:(CGFloat)radius RectCorner:(UIRectCorner)rectCorner {
    self.titleArray = title;
    self.bgColorArray = bgColor;
    self.textColorArray = textColor;
    self.radius = radius;
    self.rectCorner = rectCorner;

    [self.collectionView reloadData];
}

// 设置标签颜色和文字
- (void)setCellData:(QJMineMsgTagCollectionViewCell *)cell row:(NSInteger)row {
    UIColor *textColor = UIColorFromRGB(0x87c5ff);
    UIColor *bgColor = UIColorFromRGB(0xe8f3fd);
    
    if (self.bgColorArray.count == self.titleArray.count && self.textColorArray.count == self.titleArray.count) {
        textColor = self.textColorArray[row];
        bgColor = self.bgColorArray[row];
    } else {
        // 两个颜色交替显示
        if ((row + 1) % 2 == 1) {
            textColor = UIColorFromRGB(0xffc78e);
            bgColor = UIColorFromRGB(0xfdf7ed);
        }
    }
    
    [cell setLabelTextColor:textColor bgColor:bgColor text:self.titleArray[row] Radius:self.radius RectCorner:self.rectCorner];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.titleArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    QJMineMsgTagCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    [self setCellData:cell row:indexPath.row];
    
    return cell;
}

- (UICollectionView *)collectionView{
    if (!_collectionView) {
        UICollectionViewFlowLayout  *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.minimumLineSpacing = 10;
        layout.minimumInteritemSpacing  = 10;
        layout.scrollDirection =  UICollectionViewScrollDirectionHorizontal;

        // 设置item预估的大小
        layout.estimatedItemSize = CGSizeMake(20, cellHeight);
        
        _collectionView = [[UICollectionView alloc] initWithFrame:self.frame collectionViewLayout:layout];
        _collectionView.backgroundColor = [UIColor clearColor];
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.showsHorizontalScrollIndicator = NO;
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        [_collectionView registerClass:[QJMineMsgTagCollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
        _collectionView.userInteractionEnabled = NO;
        //collectionView的item只剩下一个时自动左对齐
        SEL sel = NSSelectorFromString(@"_setRowAlignmentsOptions:");
        if ([_collectionView.collectionViewLayout respondsToSelector:sel]) {
            ((void(*)(id,SEL,NSDictionary*)) objc_msgSend)(_collectionView.collectionViewLayout,sel, @{@"UIFlowLayoutCommonRowHorizontalAlignmentKey":@(NSTextAlignmentLeft),@"UIFlowLayoutLastRowHorizontalAlignmentKey" : @(NSTextAlignmentLeft),@"UIFlowLayoutRowVerticalAlignmentKey" : @(NSTextAlignmentCenter)});
        }
        
    }
    return _collectionView;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
