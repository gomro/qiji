//
//  QJMineMsgDeleteView.h
//  QJBox
//
//  Created by macm on 2022/7/14.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void(^QJMineMsgDeleteViewBlock)(void);

@interface QJMineMsgDeleteView : UIView
@property (nonatomic, copy) QJMineMsgDeleteViewBlock deleteViewBlock;
- (void)show;
@end

NS_ASSUME_NONNULL_END
