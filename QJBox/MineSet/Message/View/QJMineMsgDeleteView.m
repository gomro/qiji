//
//  QJMineMsgDeleteView.m
//  QJBox
//
//  Created by macm on 2022/7/14.
//

#import "QJMineMsgDeleteView.h"
#import "UIView+XWAddForRoundedCorner.h"

@interface QJMineMsgDeleteView ()
@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) UIButton *cancelButton;
@property (nonatomic, strong) UIView *lineView;
@property (nonatomic, strong) UIButton *deleteButton;

@end

@implementation QJMineMsgDeleteView
- (instancetype)init {
    self = [super init];
    if (self) {
         
        self.frame = [UIScreen mainScreen].bounds;
//        self.backgroundColor = RGBAlpha(0, 0, 0, 0.4);
        self.backgroundColor = [UIColor clearColor];
        
        [self createView];
        
    }
    return self;
}

- (void)createView {
    [self addSubview:self.bgView];
    [self.bgView addSubview:self.cancelButton];
    [self.bgView addSubview:self.lineView];
    [self.bgView addSubview:self.deleteButton];
}

- (void)deleteAction {
    if (self.deleteViewBlock) {
        self.deleteViewBlock();
    }
    [self fadeOut];
}

- (void)show {
    UIWindow *keywindow = [[UIApplication sharedApplication] keyWindow];
    [keywindow addSubview:self];
    [self fadeIn];
}

- (void)fadeIn {
//    self.transform = CGAffineTransformMakeScale(1.3, 1.3);
    self.alpha = 0;
    [UIView animateWithDuration:.35 animations:^{
        self.bgView.transform = CGAffineTransformMakeTranslation(0, - 117 - Bottom_iPhoneX_SPACE);
        self.alpha = 1;
//        self.transform = CGAffineTransformMakeScale(1, 1);
    }];
}

- (void)fadeOut {
    [UIView animateWithDuration:.35 animations:^{
        self.bgView.transform = CGAffineTransformMakeTranslation(0, kScreenHeight);
//        self.transform = CGAffineTransformMakeScale(1.3, 1.3);
        self.alpha = 0.0;
    } completion:^(BOOL finished) {
        if (finished) {
            [self removeFromSuperview];
        }
    }];
}

- (UIView *)bgView {
    if (!_bgView) {
        _bgView = [[UIView alloc] initWithFrame:CGRectMake(0, kScreenHeight, kScreenWidth, 117+Bottom_iPhoneX_SPACE)];
        _bgView.backgroundColor = [UIColor whiteColor];
        self.bgView.layer.cornerRadius = 10;
        self.bgView.layer.shadowColor = [UIColor blackColor].CGColor;//shadowColor阴影颜色
        self.bgView.layer.shadowOffset = CGSizeMake(0,0);//shadowOffset阴影偏移,x向右偏移2，y向下偏移6，默认(0, -3),这个跟shadowRadius配合使用
        self.bgView.layer.shadowOpacity = 0.3;//阴影透明度，默认0
        self.bgView.layer.shadowRadius = 4;//阴影半径，默认3
        
        self.bgView.layer.shadowPath = [UIBezierPath bezierPathWithRoundedRect:self.bgView.bounds byRoundingCorners:UIRectCornerBottomLeft|UIRectCornerBottomRight|UIRectCornerTopLeft|UIRectCornerTopRight cornerRadii:CGSizeMake(self.bgView.layer.cornerRadius, self.bgView.layer.cornerRadius)].CGPath;//参数依次为大小，设置四个角圆角状态，圆角曲度  设置阴影路径可避免离屏渲染
    }
    return _bgView;
}

- (UIButton *)cancelButton {
    if (!_cancelButton) {
        _cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _cancelButton.frame = CGRectMake(0, 60, kScreenWidth, 50);
        [_cancelButton setBackgroundColor:[UIColor clearColor]];
        [_cancelButton setTitle:@"取消" forState:UIControlStateNormal];
        [_cancelButton setTitleColor:UIColorFromRGB(0x16191c) forState:UIControlStateNormal];
        [_cancelButton.titleLabel setFont:FontRegular(14)];
        [_cancelButton addTarget:self action:@selector(fadeOut) forControlEvents:UIControlEventTouchUpInside];
    }
    return _cancelButton;
}

- (UIView *)lineView {
    if (!_lineView) {
        _lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 50, kScreenWidth, 10)];
        _lineView.backgroundColor = UIColorFromRGB(0xf5f5f5);
    }
    return _lineView;
}

- (UIButton *)deleteButton {
    if (!_deleteButton) {
        _deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _deleteButton.frame = CGRectMake(0, 0, kScreenWidth, 50);
        [_deleteButton setBackgroundColor:[UIColor clearColor]];
        [_deleteButton setTitle:@"删除" forState:UIControlStateNormal];
        [_deleteButton setTitleColor:UIColorFromRGB(0xff3b30) forState:UIControlStateNormal];
        [_deleteButton.titleLabel setFont:FontRegular(14)];
        [_deleteButton addTarget:self action:@selector(deleteAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _deleteButton;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];

    UIView *view = [touch view];

    if (view == self) {
        [self fadeOut];
    }
    
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
