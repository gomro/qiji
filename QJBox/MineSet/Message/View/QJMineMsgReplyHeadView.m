//
//  QJMineMsgReplyHeadView.m
//  QJBox
//
//  Created by macm on 2022/7/8.
//

#import "QJMineMsgReplyHeadView.h"
#import "QJMineMsgTagView.h"
#import "QJMySpaceViewController.h"

@interface QJMineMsgReplyHeadView ()
@property (nonatomic, strong) UIImageView *headImageView;
//@property (nonatomic, strong) UIImageView *bloggerImage;
@property (nonatomic, strong) UILabel *timeLabel;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) YYLabel *contentLabel;
@property (nonatomic, strong) UILabel *isDelLabel;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) QJMineMsgTagView *tagView;
@property (nonatomic, strong) UIButton *replyButton;
@property (nonatomic, strong) UIView *lineView;

@end

@implementation QJMineMsgReplyHeadView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        
        [self createView];
        
    }
    return self;
}

- (void)createView {
    [self addSubview:self.headImageView];
    [self.headImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(14);
        make.left.mas_equalTo(14);
        make.width.height.mas_equalTo(40*kWScale);
        
    }];
//    [self addSubview:self.bloggerImage];
//    [self.bloggerImage mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.bottom.mas_equalTo(self.headImageView.mas_bottom).mas_offset(0);
//        make.right.equalTo(self.headImageView.mas_right).offset(0);
//        make.width.mas_equalTo(12*kWScale);
//        make.height.mas_equalTo(14*kWScale);
//    }];
    
    [self addSubview:self.nameLabel];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.headImageView.mas_top).mas_offset(0);
        make.left.mas_equalTo(self.headImageView.mas_right).mas_offset(12);
        make.width.mas_lessThanOrEqualTo(200*kWScale);
    }];

    [self addSubview:self.timeLabel];
    [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {

        make.right.mas_equalTo(-14);
        make.centerY.equalTo(self.nameLabel);

    }];
    
    [self addSubview:self.contentLabel];
    [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {

        make.top.mas_equalTo(self.nameLabel.mas_bottom).mas_offset(0);
        make.right.mas_equalTo(self).mas_offset(-10);
        make.left.mas_equalTo(10);
    }];
    
    [self addSubview:self.isDelLabel];
    [self.isDelLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.nameLabel.mas_bottom).mas_offset(0);
        make.left.mas_equalTo(self.headImageView.mas_right).mas_offset(10);
        make.width.mas_equalTo(91*kWScale);
        make.height.mas_equalTo(20*kWScale);
    }];
    
    [self addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(self.contentLabel.mas_bottom).mas_offset(14);
        make.right.mas_equalTo(self).mas_offset(-14);
        make.left.mas_equalTo(14);
        make.height.mas_equalTo(37*kWScale);
        
    }];
    
    [self addSubview:self.tagView];
    [self.tagView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(self.titleLabel.mas_bottom).mas_offset(10);
        make.left.mas_equalTo(14);
        make.right.mas_equalTo(-54);
        make.height.mas_equalTo(17*kWScale);
        make.bottom.mas_equalTo(-14);
    }];
    
    [self addSubview:self.replyButton];
    [self.replyButton mas_makeConstraints:^(MASConstraintMaker *make) {
            
        make.right.mas_equalTo(-14);
        make.width.mas_equalTo(21);
        make.height.mas_equalTo(18*kWScale);
        make.centerY.equalTo(self.tagView);
    }];
}

- (void)setModel:(QJMineMsgReplyModel *)model {
    _model = model;
    self.titleLabel.text = [NSString stringWithFormat:@"  |   %@", _model.sourceDescription];

    if (_model.isMineReply) {
//        self.nameLabel.text = _model.targetComment.nickname;
        self.nameLabel.text = _model.sourceReplyNickname;

//        [self.headImageView setImageWithURL:[NSURL URLWithString:[model.targetComment.coverImage checkImageUrlString]] placeholder:[UIImage imageNamed:@"mine_head"]];
        [self.headImageView setImageWithURL:[NSURL URLWithString:[model.sourceReplyCoverImage checkImageUrlString]] placeholder:[UIImage imageNamed:@"mine_head"]];

//        self.timeLabel.text = [QJAppTool formatDayTime:_model.targetComment.createTime];
        self.timeLabel.text = [QJAppTool formatDayTime:_model.sourceReplyTime];

        [self.replyButton setHidden:YES];
        
        [self.isDelLabel setHidden:!_model.sourceReplyDeleted];
        if (_model.sourceReplyDeleted) {
            self.contentLabel.text = @"      ";
        } else {
            [self setContentLabelNum:_model.sourceReplyContent];
        }
        
    } else {
        self.nameLabel.text = _model.nickname;
        [self.headImageView setImageWithURL:[NSURL URLWithString:[model.coverImage checkImageUrlString]] placeholder:[UIImage imageNamed:@"mine_head"]];
        self.timeLabel.text = [QJAppTool formatDayTime:_model.createTime];
        [self.replyButton setHidden:NO];
        
        [self.isDelLabel setHidden:YES];
        [self setContentLabelNum:_model.content];
    }

    [self.tagView reloadTagView:_model.sourceTags BgColor:nil TextColor:nil Radius:2 RectCorner:UIRectCornerAllCorners];
//    self.bloggerImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"qj_bozhuLevel_%ld",self.model.bloggerLevel]];

}

// 回复
- (void)replyAction {
    if (self.replyHeadViewClick) {
        self.replyHeadViewClick(0);
    }
}
// 设置回复文字内容
- (void)setContentLabelNum:(NSString *)content {
//    NSString *title = @"《大刀进行曲》是音乐家麦新1937年7月在上海创作的。麦新,上海浦东人，1938年加入中国共产党。这首《大刀进行曲》在中国人民最需要的时候出现需要的时候出现时候出现，极大的激发了中华儿女的爱国热情，在2015年8月26日，国家新闻出版广电总局发布了“我最喜爱的十大抗战歌曲”网络投票结果，《大刀进行曲》入选10首歌曲之一。";
    
    NSMutableAttributedString *contentAttributedString = [[NSMutableAttributedString alloc] initWithString:@"            回复了你：" attributes:@{NSFontAttributeName : FontMedium(14), NSForegroundColorAttributeName : UIColorFromRGB(0x7e7e7e)}];

    NSMutableAttributedString *contentString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@", content] attributes:@{NSFontAttributeName : FontMedium(14), NSForegroundColorAttributeName : UIColorFromRGB(0x474849)}];

    [contentAttributedString appendAttributedString:contentString];
    
    self.contentLabel.attributedText = contentAttributedString;

    // 如果是展开的状态，Label的行数是0，默认行数是2
    if (_model.isShowAll) {
        self.contentLabel.numberOfLines = 0;
        // 如果是展开的状态，说明文字数量大于两行，文字末尾加上 收起 功能
        [self expandString];
    } else {
        self.contentLabel.numberOfLines = 2;
        // 如果是默认状态，文字可能是多行，更改文字过多时结尾的...,增加点击事件
        [self addSeeMoreButtonInLabel:self.contentLabel];
    }

}

// 多行显示内容的尾部将 ... 改成 查看全部并添加点击事件
- (void)addSeeMoreButtonInLabel:(YYLabel *)label {
    UIFont *font = kFont(14);
 
    NSString *moreString = @" ...全部";
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@", moreString]];
    NSRange expandRange = [text.string rangeOfString:moreString];
    
    [text addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0x007aff) range:expandRange];
    [text addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0x474849) range:NSMakeRange(0, expandRange.location)];

    //添加点击事件
    YYTextHighlight *hi = [YYTextHighlight new];
    [text setTextHighlight:hi range:[text.string rangeOfString:moreString]];
    
    __weak typeof(self) weakSelf = self;
    hi.tapAction = ^(UIView *containerView, NSAttributedString *text, NSRange range, CGRect rect) {
        //点击展开
        if (weakSelf.showAllHeadViewClick) {
            weakSelf.showAllHeadViewClick(YES);
        }

    };
    
    text.font = font;
    
    YYLabel *seeMore = [YYLabel new];
    seeMore.attributedText = text;
    [seeMore sizeToFit];
    
    NSAttributedString *truncationToken = [NSAttributedString attachmentStringWithContent:seeMore contentMode:UIViewContentModeCenter attachmentSize:seeMore.frame.size alignToFont:text.font alignment:YYTextVerticalAlignmentTop];
    
    label.truncationToken = truncationToken;
}

- (NSAttributedString *)appendAttriStringWithFont:(UIFont *)font {
    if (!font) {
        font = kFont(14);
    }
//    if ([_contentLabel.attributedText.string containsString:@"收起"]) {
//        return [[NSAttributedString alloc] initWithString:@""];
//    }
    
    NSString *appendText = @" 收起 ";
    NSMutableAttributedString *append = [[NSMutableAttributedString alloc] initWithString:appendText attributes:@{NSFontAttributeName : font, NSForegroundColorAttributeName : UIColorFromRGB(0x007aff)}];
    
    YYTextHighlight *hi = [YYTextHighlight new];
    [append setTextHighlight:hi range:[append.string rangeOfString:appendText]];
    
    __weak typeof(self) weakSelf = self;
    hi.tapAction = ^(UIView *containerView, NSAttributedString *text, NSRange range, CGRect rect) {
        //点击收起
        if (weakSelf.showAllHeadViewClick) {
            weakSelf.showAllHeadViewClick(NO);
        }
    };
    
    return append;
}

- (void)expandString {
    NSMutableAttributedString *attri = [_contentLabel.attributedText mutableCopy];
    [attri appendAttributedString:[self appendAttriStringWithFont:attri.font]];
    self.contentLabel.attributedText = attri;
}

- (void)packUpString {
    NSString *appendText = @" 收起 ";
    NSMutableAttributedString *attri = [_contentLabel.attributedText mutableCopy];
    NSRange range = [attri.string rangeOfString:appendText options:NSBackwardsSearch];

    if (range.location != NSNotFound) {
        [attri deleteCharactersInRange:range];
    }

    _contentLabel.attributedText = attri;
}

- (void)headAction {
    QJMySpaceViewController *vc = [QJMySpaceViewController new];
    if (self.model.isMineReply) {
        vc.userId = self.model.sourceReplyUid;
    } else {
        vc.userId = self.model.uid;
    }
    [[QJAppTool getCurrentViewController].navigationController pushViewController:vc animated:YES];
}

- (UIImageView *)headImageView {
    if (!_headImageView) {
        _headImageView = [[UIImageView alloc] init];
        _headImageView.layer.cornerRadius = 20*kWScale;
        _headImageView.layer.masksToBounds = YES;
        _headImageView.image = [UIImage imageNamed:@"mine_head"];
        _headImageView.contentMode = UIViewContentModeScaleAspectFill;
        _headImageView.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(headAction)];
        [_headImageView addGestureRecognizer:tap];

    }
    return _headImageView;
}

//- (UIImageView *)bloggerImage {
//    if (!_bloggerImage) {
//        self.bloggerImage = [UIImageView new];
//        self.bloggerImage.image = [UIImage imageNamed:@"avatarLevelIcon"];
//    }
//    return _bloggerImage;
//}

- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc] init];
        _nameLabel.text = @"比克大魔王";
        _nameLabel.textColor = UIColorFromRGB(0x2261a9);
        [_nameLabel setFont:FontMedium(14)];
    }
    return _nameLabel;
}

- (UILabel *)timeLabel {
    if (!_timeLabel) {
        _timeLabel = [[UILabel alloc] init];
        _timeLabel.text = @"1小时前";
        _timeLabel.textColor = UIColorFromRGB(0x919599);
        _timeLabel.textAlignment = NSTextAlignmentRight;
        [_timeLabel setFont:FontMedium(11)];
    }
    return _timeLabel;
}

- (YYLabel *)contentLabel {
    if (!_contentLabel) {
        _contentLabel = [[YYLabel alloc] init];
        _contentLabel.backgroundColor = [UIColor clearColor];
        
        //设置最大宽度 ，如果宽度没有设置好，会出现高度计算不准确，不能自动换行的问题
        _contentLabel.preferredMaxLayoutWidth = kScreenWidth-40;
        
//        _contentLabel.userInteractionEnabled = YES;
//        _contentLabel.textVerticalAlignment = YYTextVerticalAlignmentTop;
    }
    return _contentLabel;
}

- (UILabel *)isDelLabel {
    if (!_isDelLabel) {
        _isDelLabel = [[UILabel alloc] init];
        _isDelLabel.text = @"该评论已删除";
        _isDelLabel.layer.cornerRadius = 4;
        _isDelLabel.layer.backgroundColor = UIColorFromRGB(0xefefef).CGColor;
        _isDelLabel.textColor = UIColorFromRGB(0x646464);
        _isDelLabel.textAlignment = NSTextAlignmentCenter;
        _isDelLabel.font = FontMedium(12);
    }
    return _isDelLabel;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.text = @"  |   大天使之战，有没有来玩的？";
        _titleLabel.textColor = UIColorFromRGB(0x919599);
        _titleLabel.backgroundColor = UIColorFromRGB(0xfafafa);
        [_titleLabel setFont:FontMedium(14)];
        _titleLabel.layer.cornerRadius = 5;
        _titleLabel.layer.masksToBounds = YES;
    }
    return _titleLabel;
}

- (QJMineMsgTagView *)tagView {
    if (!_tagView) {
        _tagView = [[QJMineMsgTagView alloc] init];
    }
    return _tagView;
}

- (UIButton *)replyButton {
    if (!_replyButton) {
        _replyButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_replyButton setImage:[UIImage imageNamed:@"mine_comment"] forState:UIControlStateNormal];
        [_replyButton addTarget:self action:@selector(replyAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _replyButton;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
