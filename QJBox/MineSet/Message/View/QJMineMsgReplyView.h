//
//  QJMineMsgReplyView.h
//  QJBox
//
//  Created by macm on 2022/7/5.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJMineMsgReplyView : UIView
// isMineReply: 是否是我的回复
- (instancetype)initWithFrame:(CGRect)frame isMineReply:(BOOL)isMineReply;
@end

NS_ASSUME_NONNULL_END
