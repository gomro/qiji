//
//  QJMineMsgReplyView.m
//  QJBox
//
//  Created by macm on 2022/7/5.
//

#import "QJMineMsgReplyView.h"
#import "QJMineMsgReplyTableViewCell.h"

@interface QJMineMsgReplyView ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, assign) BOOL isMineReply;

@end

@implementation QJMineMsgReplyView

- (instancetype)initWithFrame:(CGRect)frame isMineReply:(BOOL)isMineReply {
    self = [super initWithFrame:frame];
    if (self) {
        self.isMineReply = isMineReply;
        
        self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height) style:UITableViewStylePlain];
        self.tableView.dataSource = self;
        self.tableView.delegate = self;
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.tableView.backgroundColor = UIColorFromRGB(0xF7F7F7);
        self.tableView.showsVerticalScrollIndicator = NO;
        self.tableView.showsHorizontalScrollIndicator = NO;
        self.tableView.estimatedRowHeight = 1;
        [self.tableView registerClass:[QJMineMsgReplyTableViewCell class] forCellReuseIdentifier:@"cell"];
        if (@available(iOS 11.0, *)) {
            self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
        [self addSubview:self.tableView];
    }
    return self;
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    [tableView tableViewDisplayWhenHaveNoDataWithMsg:@"这里什么也没有～" centerImage:[UIImage imageNamed:@"icon_empty_no_data"] ifNecessaryForRowCount:20];
    return 20;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    QJMineMsgReplyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    QJMineMsgReplyModel *model = [[QJMineMsgReplyModel alloc] init];
    model.isMineReply = self.isMineReply;
    
    cell.model = model;
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return self.isMineReply ? 210 : 150;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
