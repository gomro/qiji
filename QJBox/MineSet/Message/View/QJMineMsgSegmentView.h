//
//  QJMineMsgSegmentView.h
//  QJBox
//
//  Created by macm on 2022/7/6.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJMineMsgSegmentView : UIView
@property (nonatomic, copy) void(^segmentClick)(NSInteger index);

@end

NS_ASSUME_NONNULL_END
