//
//  QJMineMsgReplyHeadView.h
//  QJBox
//
//  Created by macm on 2022/7/8.
//

#import <UIKit/UIKit.h>
#import "QJMineMsgReplyModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJMineMsgReplyHeadView : UIView
@property (nonatomic, copy) void(^showAllHeadViewClick)(BOOL isShow);
@property (nonatomic, copy) void(^replyHeadViewClick)(NSInteger index);

@property (nonatomic, strong) QJMineMsgReplyModel *model;

@end

NS_ASSUME_NONNULL_END
