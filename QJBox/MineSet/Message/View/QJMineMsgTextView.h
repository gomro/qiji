//
//  QJMineMsgTextView.h
//  QJBox
//
//  Created by macm on 2022/7/6.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^MsgTextHeightChangedBlock)(NSString *text, CGFloat textHeight);

@interface QJMineMsgTextView : UITextView

@property (nonatomic, copy) MsgTextHeightChangedBlock textHeightChangedBlock;
@property (nonatomic, assign) NSInteger maxNumberOfLines;
@property (nonatomic, assign) NSUInteger cornerRadius;
@property (nonatomic, copy) NSString *placeholder;
@property (nonatomic, strong) UIColor *placeholderColor;
@property (nonatomic, strong) UITextView *placeholderView;

// 当文字改变时应该调用该方法
- (void)textDidChange;

@end

NS_ASSUME_NONNULL_END
