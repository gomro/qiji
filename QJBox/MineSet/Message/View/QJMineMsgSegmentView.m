//
//  QJMineMsgSegmentView.m
//  QJBox
//
//  Created by macm on 2022/7/6.
//

#import "QJMineMsgSegmentView.h"

@interface QJMineMsgSegmentView ()
@property (nonatomic, strong) UIButton *leftButton;
@property (nonatomic, strong) UIButton *rightButton;
//@property (nonatomic, strong) UIView *lineView;

@property (strong, nonatomic) MASConstraint *centerConstraint;

@end

@implementation QJMineMsgSegmentView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self createView];
    }
    return self;
}

- (void)createView {
    [self addSubview:self.leftButton];
    
    [self.leftButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.bottom.mas_equalTo(0);
        make.width.mas_equalTo(75);
    }];
    
    [self addSubview:self.rightButton];
    [self.rightButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.right.bottom.mas_equalTo(0);
        make.width.mas_equalTo(75);
    }];
    
//    [self addSubview:self.lineView];
//    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
//
//        make.top.mas_equalTo(36);
//        make.width.mas_equalTo(24);
//        make.height.mas_equalTo(3);
//        self.centerConstraint = make.centerX.mas_equalTo(self.leftButton);
//    }];
}

- (void)leftAction {
    
    [_leftButton.titleLabel setFont:FontSemibold(16)];
    [_rightButton.titleLabel setFont:FontRegular(14)];
    [_leftButton setTitleColor:UIColorFromRGB(0x474849) forState:UIControlStateNormal];
    [_rightButton setTitleColor:UIColorFromRGB(0x919599) forState:UIControlStateNormal];

//    [self.centerConstraint uninstall];
//    [self.lineView mas_updateConstraints:^(MASConstraintMaker *make) {
//        self.centerConstraint = make.centerX.mas_equalTo(self.leftButton);
//    }];
    
    if (self.segmentClick) {
        self.segmentClick(0);
    }
}

- (void)rightAction {

    [_leftButton.titleLabel setFont:FontRegular(14)];
    [_rightButton.titleLabel setFont:FontSemibold(16)];
    [_leftButton setTitleColor:UIColorFromRGB(0x919599) forState:UIControlStateNormal];
    [_rightButton setTitleColor:UIColorFromRGB(0x474849) forState:UIControlStateNormal];

//    [self.centerConstraint uninstall];
//    [self.lineView mas_updateConstraints:^(MASConstraintMaker *make) {
//        self.centerConstraint = make.centerX.mas_equalTo(self.rightButton);
//    }];
    
    if (self.segmentClick) {
        self.segmentClick(1);
    }
}

- (UIButton *)leftButton {
    if (!_leftButton) {
        _leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_leftButton setTitle:@"评论我的" forState:UIControlStateNormal];
        [_leftButton setTitleColor:UIColorFromRGB(0x474849) forState:UIControlStateNormal];
        [_leftButton addTarget:self action:@selector(leftAction) forControlEvents:UIControlEventTouchUpInside];
        [_leftButton.titleLabel setFont:FontSemibold(16)];
    }
    return _leftButton;
}

- (UIButton *)rightButton {
    if (!_rightButton) {
        _rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_rightButton setTitle:@"我回复的" forState:UIControlStateNormal];
        [_rightButton setTitleColor:UIColorFromRGB(0x919599) forState:UIControlStateNormal];
        [_rightButton addTarget:self action:@selector(rightAction) forControlEvents:UIControlEventTouchUpInside];
        [_rightButton.titleLabel setFont:FontRegular(14)];
    }
    return _rightButton;
}

//- (UIView *)lineView {
//    if (!_lineView) {
//        _lineView = [[UIView alloc] init];
//        _lineView.backgroundColor = UIColorFromRGB(0xffb005);
//    }
//    return _lineView;
//}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
