//
//  QJMineMsgInputView.h
//  QJBox
//
//  Created by macm on 2022/7/6.
//

#import <UIKit/UIKit.h>
#import "QJMineMsgTextView.h"
#import "QJMineMsgReplyModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface QJMineMsgInputView : UIView

/**
 *  输入框
 */
@property (nonatomic, strong) QJMineMsgTextView *textView;

@property (nonatomic, strong) QJMineMsgReplyModel *model;
@property (nonatomic, strong) UIView *toolView;

- (instancetype)init;
- (void)show;
@end

NS_ASSUME_NONNULL_END
