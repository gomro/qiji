//
//  QJMineMsgHeadView.h
//  QJBox
//
//  Created by macm on 2022/7/5.
//

#import <UIKit/UIKit.h>
#import "QJMineMsgUnReadView.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJMineMsgHeadView : UIView
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIImageView *bgImageView;
@property (nonatomic, strong) QJMineMsgUnReadView *unReadView;

@property (nonatomic, copy) void(^msgHeadClick)(NSInteger index);

@end

NS_ASSUME_NONNULL_END
