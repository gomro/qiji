//
//  QJMineMsgUnReadView.h
//  QJBox
//
//  Created by macm on 2022/7/5.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJMineMsgUnReadView : UIView
- (void)reloadUnReadView:(NSInteger)num;
@end

NS_ASSUME_NONNULL_END
