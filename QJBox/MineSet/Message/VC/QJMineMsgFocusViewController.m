//
//  QJMineMsgFocusViewController.m
//  QJBox
//
//  Created by macm on 2022/7/5.
//

#import "QJMineMsgFocusViewController.h"
#import "QJMineMsgFocusTableViewCell.h"
#import "QJMineMsgRequest.h"
#import "QJMineMsgDeleteView.h"
#import "QJMySpaceViewController.h"

@interface QJMineMsgFocusViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, assign) NSInteger page;
// 选中的行
@property (nonatomic, assign) NSInteger selectRow;
@end

@implementation QJMineMsgFocusViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"关注";
    self.view.backgroundColor = UIColorFromRGB(0xF5F5F5);
    self.dataArray = [NSMutableArray array];
    self.page = 1;

    [self createView];
    [self sendRequest];
    [self addLongPress];
}

- (void)loadData {
    _page = 1;
    [self.dataArray removeAllObjects];
    [self sendRequest];
}

- (void)loadMoreData {
    _page++;
    [self sendRequest];
}

- (void)sendRequest {
    @weakify(self);
    QJMineMsgRequest *request = [[QJMineMsgRequest alloc] init];
    [request requestNotifyFollowList:_page completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        @strongify(self);
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        
        if (isSuccess) {
            NSArray *array = [NSArray modelArrayWithClass:[QJMineMsgCollectModel class] json:EncodeArrayFromDic(request.responseJSONObject[@"data"], @"records")];
            [self.dataArray addObjectsFromArray:array];
            [self.tableView reloadData];
            
            if ([request.responseJSONObject[@"data"][@"hasNext"] integerValue] == 0 || array.count == 0) {
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
            }
        }
        [self.tableView tableViewDisplayWhenHaveNoDataWithView:[QJEmptyCommonView new] ifNecessaryForRowCount:self.dataArray.count];

    }];
    
}

- (void)sendRequestDelete {
    [QJAppTool showHUDLoading];
    QJMineMsgCollectModel *model = self.dataArray[self.selectRow];
    @weakify(self);
    QJMineMsgRequest *request = [[QJMineMsgRequest alloc] init];
    [request requestNotifyFollow:model.idStr completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        [QJAppTool hideHUDLoading];
        @strongify(self);
        if (isSuccess) {
            [self.dataArray removeObjectAtIndex:self.selectRow];
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:self.selectRow] withRowAnimation:UITableViewRowAnimationFade];
        }
        
    }];
    
}

//创建长按手势
- (void)addLongPress {
    UILongPressGestureRecognizer *longPressGR = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(longPressGR:)];
    longPressGR.minimumPressDuration = 1;
    [self.tableView addGestureRecognizer:longPressGR];
}

//实现手势对应的功能
-(void)longPressGR:(UILongPressGestureRecognizer *)longPressGR {

    if (longPressGR.state == UIGestureRecognizerStateBegan) {
        CGPoint point = [longPressGR locationInView:self.tableView];
        NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:point];
        self.selectRow = indexPath.row;

        QJMineMsgDeleteView *view = [[QJMineMsgDeleteView alloc] init];
        @weakify(self);
        view.deleteViewBlock = ^{
            @strongify(self);
            [self sendRequestDelete];
        };
        [view show];
    }

    if (longPressGR.state == UIGestureRecognizerStateEnded) {
        
        
    }

}

- (void)createView {

    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(NavigationBar_Bottom_Y);
        make.left.mas_equalTo(15);
        make.right.mas_equalTo(-15);
        make.bottom.mas_equalTo(0);

    }];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.dataArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    QJMineMsgFocusTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.model = self.dataArray[indexPath.section];
    return cell;
}

#pragma mark - UITableViewDelegate
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    return 68;
//}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 14;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [UIView new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [UIView new];
}

//设Cell可编辑
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

//设置删除按钮
-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return  UITableViewCellEditingStyleDelete;
}


//进入编辑（删除）模式
-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    //出现alterView隐藏删除按钮
    [tableView setEditing:NO animated:YES];
    if (editingStyle == UITableViewCellEditingStyleDelete) {
//        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"你确定删除该消息？" preferredStyle:UIAlertControllerStyleAlert];
//        [alertController addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil]];
//        [alertController addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
            [self sendRequestDelete];
//        }]];
//
//        [self presentViewController:alertController animated:YES completion:nil];
    }
}

//修改编辑按钮文字
-(NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
    return @"删除";
}

//设置进入编辑状态时，Cell不会缩进
- (BOOL)tableView: (UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    QJMineMsgCollectModel *model = self.dataArray[indexPath.row];
    QJMySpaceViewController *vc = [QJMySpaceViewController new];
    vc.userId = model.uid;
    [self.navigationController pushViewController:vc animated:YES];
}

- (UITableView *)tableView {
    if (!_tableView) {
        self.tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        self.tableView.dataSource = self;
        self.tableView.delegate = self;
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.tableView.backgroundColor = UIColorFromRGB(0xF5F5F5);
        self.tableView.showsVerticalScrollIndicator = NO;
        self.tableView.showsHorizontalScrollIndicator = NO;
        [self.tableView registerClass:[QJMineMsgFocusTableViewCell class] forCellReuseIdentifier:@"cell"];
        if (@available(iOS 11.0, *)) {
            self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
        self.tableView.mj_header = [QJRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadData)];
        self.tableView.mj_footer = [QJRefreshFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
    }
    return _tableView;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
