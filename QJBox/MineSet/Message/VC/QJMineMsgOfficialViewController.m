//
//  QJMineMsgOfficialViewController.m
//  QJBox
//
//  Created by macm on 2022/7/5.
//

#import "QJMineMsgOfficialViewController.h"
#import "QJMineMsgOfficialTableViewCell.h"
#import "QJMineMsgRequest.h"

@interface QJMineMsgOfficialViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, assign) NSInteger page;

@end

@implementation QJMineMsgOfficialViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"官方消息";
    
    self.dataArray = [NSMutableArray array];
    self.page = 1;
    [self createView];
    [self sendRequest];
}

- (void)loadData {
    _page = 1;
    [self.dataArray removeAllObjects];
    [self sendRequest];
}

- (void)loadMoreData {
    _page++;
    [self sendRequest];
}

- (void)sendRequest {
    @weakify(self);
    QJMineMsgRequest *request = [[QJMineMsgRequest alloc] init];
    [request requestAnnouncementList:_page completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        @strongify(self);
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        
        if (isSuccess) {
            NSArray *array = [NSArray modelArrayWithClass:[QJMineMsgOfficialModel class] json:EncodeArrayFromDic(request.responseJSONObject[@"data"], @"records")];
            [self.dataArray addObjectsFromArray:array];
            [self.tableView reloadData];
            
            if ([request.responseJSONObject[@"data"][@"hasNext"] integerValue] == 0 || array.count == 0) {
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
            }
        }
        [self.tableView tableViewDisplayWhenHaveNoDataWithView:[QJEmptyCommonView new] ifNecessaryForRowCount:self.dataArray.count];

    }];
    
}

- (void)sendRequestDetail:(NSString *)idString {
    [QJAppTool showHUDLoading];
    @weakify(self);
    QJMineMsgRequest *request = [[QJMineMsgRequest alloc] init];
    [request requestAnnouncementDetail:idString completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        [QJAppTool hideHUDLoading];
        @strongify(self);
        if (isSuccess) {
            NSString *htmlString = request.responseJSONObject[@"data"][@"content"];
            QJWKWebViewController *vc = [QJWKWebViewController new];
            vc.navTitle = @"消息详情";
            vc.htmlString = htmlString;
            [self.navigationController pushViewController:vc animated:YES];
        }
        
    }];
    
}

- (void)createView {

    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(NavigationBar_Bottom_Y);
        make.right.left.bottom.mas_equalTo(0);
    }];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    QJMineMsgOfficialTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.model = self.dataArray[indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    QJMineMsgOfficialModel *model = self.dataArray[indexPath.row];
    [self sendRequestDetail:model.idStr];
}

- (UITableView *)tableView {
    if (!_tableView) {
        self.tableView = [[UITableView alloc] init];
        self.tableView.dataSource = self;
        self.tableView.delegate = self;
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.tableView.backgroundColor = UIColorFromRGB(0xF7F7F7);
        self.tableView.showsVerticalScrollIndicator = NO;
        self.tableView.showsHorizontalScrollIndicator = NO;
        self.tableView.estimatedRowHeight = 1;
        [self.tableView registerClass:[QJMineMsgOfficialTableViewCell class] forCellReuseIdentifier:@"cell"];
        if (@available(iOS 11.0, *)) {
            self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
        self.tableView.mj_header = [QJRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadData)];
        self.tableView.mj_footer = [QJRefreshFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];

    }
    return _tableView;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
