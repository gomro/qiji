//
//  QJMineMsgReplyViewController.m
//  QJBox
//
//  Created by macm on 2022/7/5.
//

#import "QJMineMsgReplyViewController.h"
#import "QJMineMsgSegmentView.h"
#import "QJMineMsgInputView.h"
#import "QJMineMsgReplyTableViewCell.h"
#import "QJMineMsgMyReplyTableViewCell.h"
#import "QJMineMsgRequest.h"
#import "QJMineMsgDeleteView.h"
#import "QJConsultDetailViewController.h"

@interface QJMineMsgReplyViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) QJMineMsgSegmentView *segmentView;

@property (nonatomic, assign) BOOL isMineReply;
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, assign) NSInteger page;
// 选中的行
@property (nonatomic, assign) NSInteger selectRow;
@end

@implementation QJMineMsgReplyViewController
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [IQKeyboardManager sharedManager].enableAutoToolbar = NO;
    [IQKeyboardManager sharedManager].shouldResignOnTouchOutside = NO;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [IQKeyboardManager sharedManager].enableAutoToolbar = YES;
    [IQKeyboardManager sharedManager].shouldResignOnTouchOutside = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createView];
    
    self.dataArray = [NSMutableArray array];
    self.page = 1;
    
    [self sendRequest];
    [self addLongPress];

}

- (void)loadData {
    _page = 1;
    [self.dataArray removeAllObjects];
    [self sendRequest];
}

- (void)loadMoreData {
    _page++;
    [self sendRequest];
}

- (void)sendRequest {
    @weakify(self);
    QJMineMsgRequest *request = [[QJMineMsgRequest alloc] init];
    if (self.isMineReply) {
        [request requestCommentNotifyListReplyme:_page completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
            @strongify(self);
            [self.tableView.mj_header endRefreshing];
            [self.tableView.mj_footer endRefreshing];
            if (isSuccess) {
                NSArray *array = [NSArray modelArrayWithClass:[QJMineMsgReplyModel class] json:EncodeArrayFromDic(request.responseJSONObject[@"data"], @"records")];
                [self.dataArray addObjectsFromArray:array];
                [self.tableView reloadData];
                
                if ([request.responseJSONObject[@"data"][@"hasNext"] integerValue] == 0 || array.count == 0) {
                    [self.tableView.mj_footer endRefreshingWithNoMoreData];
                }
            }
            [self.tableView tableViewDisplayWhenHaveNoDataWithView:[QJEmptyCommonView new] ifNecessaryForRowCount:self.dataArray.count];
        }];
    }else {
        [request requestCommentNotifyListIreply:_page completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
            @strongify(self);
            [self.tableView.mj_header endRefreshing];
            [self.tableView.mj_footer endRefreshing];
            if (isSuccess) {
                NSArray *array = [NSArray modelArrayWithClass:[QJMineMsgReplyModel class] json:EncodeArrayFromDic(request.responseJSONObject[@"data"], @"records")];
                [self.dataArray addObjectsFromArray:array];
                [self.tableView reloadData];
                
                if ([request.responseJSONObject[@"data"][@"hasNext"] integerValue] == 0 || array.count == 0) {
                    [self.tableView.mj_footer endRefreshingWithNoMoreData];
                }
            }
            [self.tableView tableViewDisplayWhenHaveNoDataWithView:[QJEmptyCommonView new] ifNecessaryForRowCount:self.dataArray.count];
        }];
    }

}

- (void)sendRequestDelete {
    [QJAppTool showHUDLoading];
    QJMineMsgReplyModel *model = self.dataArray[self.selectRow];
    @weakify(self);
    QJMineMsgRequest *request = [[QJMineMsgRequest alloc] init];
    if (self.isMineReply) {
        [request requestMyCommentNotify:model.idStr completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
            @strongify(self);
            [QJAppTool hideHUDLoading];
            if (isSuccess) {
                [self.dataArray removeObjectAtIndex:self.selectRow];
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.selectRow inSection:0];
                [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            }

        }];
    }else {
        [request requestCommentNotify:model.idStr type:YES completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
            @strongify(self);
            [QJAppTool hideHUDLoading];
            if (isSuccess) {
                [self.dataArray removeObjectAtIndex:self.selectRow];
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.selectRow inSection:0];
                [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            }
        }];
    }
    
}

//创建长按手势
- (void)addLongPress {
    UILongPressGestureRecognizer *longPressGR = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(longPressGR:)];
    longPressGR.minimumPressDuration = 1;
    [self.tableView addGestureRecognizer:longPressGR];
}

//实现手势对应的功能
-(void)longPressGR:(UILongPressGestureRecognizer *)longPressGR {

    if (longPressGR.state == UIGestureRecognizerStateBegan) {
        CGPoint point = [longPressGR locationInView:self.tableView];
        NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:point];
        self.selectRow = indexPath.row;

        QJMineMsgDeleteView *view = [[QJMineMsgDeleteView alloc] init];
        @weakify(self);
        view.deleteViewBlock = ^{
            @strongify(self);
            [self sendRequestDelete];
        };
        [view show];
    }

    if (longPressGR.state == UIGestureRecognizerStateEnded) {
        
        
    }

}

- (void)createView{
    
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(NavigationBar_Bottom_Y);
        make.right.left.bottom.mas_equalTo(0);
    }];
    
    [self.navBarItemView addSubview:self.segmentView];
    [self.segmentView mas_makeConstraints:^(MASConstraintMaker *make) {
            
        make.top.bottom.mas_equalTo(self.navBarItemView);
        make.width.mas_equalTo(160*kWScale);
        make.centerX.mas_equalTo(self.navBarItemView);
    }];
    
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    //我的回复
    if (self.isMineReply) {
        QJMineMsgMyReplyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"myCell" forIndexPath:indexPath];
        
        QJMineMsgReplyModel *model = self.dataArray[indexPath.row];
        
        model.isMineReply = self.isMineReply;
        
        cell.model = model;
        
        cell.replyClick = ^(NSInteger index) {
            QJMineMsgInputView *inputView = [[QJMineMsgInputView alloc] init];
            inputView.model = model;
            [inputView show];
        };
        cell.showAllClick = ^(BOOL isShow) {
            
            QJMineMsgReplyModel *model = self.dataArray[indexPath.row];
            model.isShowReplyAll = isShow;
            self.dataArray[indexPath.row] = model;
            
            [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            
        };
        cell.showAllHeadViewClick = ^(BOOL isShow) {
            QJMineMsgReplyModel *model = self.dataArray[indexPath.row];
            model.isShowAll = isShow;
            self.dataArray[indexPath.row] = model;
            
            [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            
        };
        return cell;
        
    //回复我的
    } else {
        QJMineMsgReplyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
        
        QJMineMsgReplyModel *model = self.dataArray[indexPath.row];
        
        model.isMineReply = self.isMineReply;
        
        cell.model = model;
        
        cell.replyClick = ^(NSInteger index) {
            QJMineMsgInputView *inputView = [[QJMineMsgInputView alloc] init];
            inputView.model = model;
            [inputView show];
        };
        cell.showAllClick = ^(BOOL isShow) {
            
            QJMineMsgReplyModel *model = self.dataArray[indexPath.row];
            model.isShowAll = isShow;
            self.dataArray[indexPath.row] = model;
            
            [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            
        };
        return cell;
    }
    
    //关键的一步,解决不正常显示问题
//     [cell layoutIfNeeded];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    QJMineMsgReplyModel *model = self.dataArray[indexPath.row];
//    [[QJMediator sharedInstance] pushQJMediatorViewController:model.targetPage];
    QJConsultDetailViewController *detailView = [[QJConsultDetailViewController alloc] init];
    detailView.idStr = model.sourceId;
    detailView.isNews = model.type == 1 ? YES : NO;
    [[QJAppTool getCurrentViewController].navigationController pushViewController:detailView animated:YES];
}

#pragma mark - Lazy Loading

- (UITableView *)tableView {
    if (!_tableView) {
        self.tableView = [[UITableView alloc] init];
        self.tableView.dataSource = self;
        self.tableView.delegate = self;
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.tableView.backgroundColor = UIColorFromRGB(0xF5F5F5);
        self.tableView.showsVerticalScrollIndicator = NO;
        self.tableView.showsHorizontalScrollIndicator = NO;
        
        // 预设行高
        self.tableView.estimatedRowHeight = 0.1;
        // 自动计算行高模式
        self.tableView.rowHeight = UITableViewAutomaticDimension;

        [self.tableView registerClass:[QJMineMsgReplyTableViewCell class] forCellReuseIdentifier:@"cell"];
        [self.tableView registerClass:[QJMineMsgMyReplyTableViewCell class] forCellReuseIdentifier:@"myCell"];

        if (@available(iOS 11.0, *)) {
            self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
        self.tableView.mj_header = [QJRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadData)];
        self.tableView.mj_footer = [QJRefreshFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
    }
    return _tableView;
}

- (QJMineMsgSegmentView *)segmentView {
    if (!_segmentView) {
        _segmentView = [[QJMineMsgSegmentView alloc] init];
        @weakify(self);
        _segmentView.segmentClick = ^(NSInteger index) {
            @strongify(self);
            
            self.isMineReply = index == 1 ? YES : NO;
            [self.tableView.mj_header beginRefreshing];
            
        };
    }
    return _segmentView;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
