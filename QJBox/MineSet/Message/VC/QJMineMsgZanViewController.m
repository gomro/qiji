//
//  QJMineMsgZanViewController.m
//  QJBox
//
//  Created by macm on 2022/7/5.
//

#import "QJMineMsgZanViewController.h"
#import "QJMineMsgZanTableViewCell.h"
#import "QJMineMsgRequest.h"
#import "QJMineMsgDeleteView.h"
#import "QJConsultDetailViewController.h"

@interface QJMineMsgZanViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, assign) NSInteger page;
// 选中的行
@property (nonatomic, assign) NSInteger selectRow;
@end

@implementation QJMineMsgZanViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = self.titleString == nil ? @"赞" : self.titleString;
    self.page = 1;
    
    self.dataArray = [NSMutableArray array];
    [self createView];
    [self sendRequest];
    [self addLongPress];
}

- (void)loadData {
    _page = 1;
    [self.dataArray removeAllObjects];
    [self sendRequest];
}

- (void)loadMoreData {
    _page++;
    [self sendRequest];
}

- (void)sendRequest {

    @weakify(self);
    QJMineMsgRequest *request = [[QJMineMsgRequest alloc] init];
    if ([self.titleString isEqualToString:@"收藏"]) {
        [request requestNotifyCollectList:_page completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
            @strongify(self);
            [self.tableView.mj_header endRefreshing];
            [self.tableView.mj_footer endRefreshing];
            
            DLog(@"-=-=-==-%@",request.responseJSONObject[@"code"]);
            if (isSuccess) {
                NSArray *array = [NSArray modelArrayWithClass:[QJMineMsgCollectModel class] json:EncodeArrayFromDic(request.responseJSONObject[@"data"], @"records")];
                [self.dataArray addObjectsFromArray:array];
                [self.tableView reloadData];
                
                if ([request.responseJSONObject[@"data"][@"hasNext"] integerValue] == 0 || array.count == 0) {
                    [self.tableView.mj_footer endRefreshingWithNoMoreData];
                }
            }
            [self.tableView tableViewDisplayWhenHaveNoDataWithView:[QJEmptyCommonView new] ifNecessaryForRowCount:self.dataArray.count];

        }];
    } else {
        [request requestNotifyLikeList:_page completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
            @strongify(self);
            [self.tableView.mj_header endRefreshing];
            [self.tableView.mj_footer endRefreshing];
            
            DLog(@"-=-=-==-%@",request.responseJSONObject[@"code"]);
            if (isSuccess) {
                NSArray *array = [NSArray modelArrayWithClass:[QJMineMsgCollectModel class] json:EncodeArrayFromDic(request.responseJSONObject[@"data"], @"records")];
                [self.dataArray addObjectsFromArray:array];
                [self.tableView reloadData];
                
                if ([request.responseJSONObject[@"data"][@"hasNext"] integerValue] == 0 || array.count == 0) {
                    [self.tableView.mj_footer endRefreshingWithNoMoreData];
                }
            }
            [self.tableView tableViewDisplayWhenHaveNoDataWithView:[QJEmptyCommonView new] ifNecessaryForRowCount:self.dataArray.count];
        }];
    }
    
}

- (void)sendRequestDelete {
    [QJAppTool showHUDLoading];
    QJMineMsgCollectModel *model = self.dataArray[self.selectRow];
    @weakify(self);
    QJMineMsgRequest *request = [[QJMineMsgRequest alloc] init];
    if ([self.titleString isEqualToString:@"收藏"]) {
        [request requestNotifyCollect:model.idStr completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
            [QJAppTool hideHUDLoading];
            @strongify(self);
            if (isSuccess) {
                [self.dataArray removeObjectAtIndex:self.selectRow];
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.selectRow inSection:0];
                [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            }
        }];
    } else {
        [request requestNotifyLike:model.idStr completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
            [QJAppTool hideHUDLoading];
            @strongify(self);
            if (isSuccess) {
                [self.dataArray removeObjectAtIndex:self.selectRow];
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.selectRow inSection:0];
                [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            }
            
        }];
    }

}

//创建长按手势
- (void)addLongPress {
    UILongPressGestureRecognizer *longPressGR = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(longPressGR:)];
    longPressGR.minimumPressDuration = 1;
    [self.tableView addGestureRecognizer:longPressGR];
}

//实现手势对应的功能
-(void)longPressGR:(UILongPressGestureRecognizer *)longPressGR {

    if (longPressGR.state == UIGestureRecognizerStateBegan) {
        CGPoint point = [longPressGR locationInView:self.tableView];
        NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:point];
        self.selectRow = indexPath.row;

        QJMineMsgDeleteView *view = [[QJMineMsgDeleteView alloc] init];
        @weakify(self);
        view.deleteViewBlock = ^{
            @strongify(self);
            [self sendRequestDelete];
        };
        [view show];
    }

    if (longPressGR.state == UIGestureRecognizerStateEnded) {
        
        
    }

}


- (void)createView {

    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(NavigationBar_Bottom_Y);
        make.right.left.bottom.mas_equalTo(0);
    }];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    QJMineMsgZanTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    if ([self.titleString isEqualToString:@"收藏"]) {
        cell.typeLabel.text = @"收藏了我";
    }
    cell.model = self.dataArray[indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    QJMineMsgCollectModel *model = self.dataArray[indexPath.row];
    QJConsultDetailViewController *detailView = [[QJConsultDetailViewController alloc] init];
    detailView.idStr = model.sourceId;
    detailView.isNews = model.type == 1 ? YES : NO;
    [[QJAppTool getCurrentViewController].navigationController pushViewController:detailView animated:YES];
}

- (UITableView *)tableView {
    if (!_tableView) {
        self.tableView = [[UITableView alloc] init];
        self.tableView.dataSource = self;
        self.tableView.delegate = self;
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.tableView.backgroundColor = UIColorFromRGB(0xF5F5F5);
        self.tableView.showsVerticalScrollIndicator = NO;
        self.tableView.showsHorizontalScrollIndicator = NO;
        [self.tableView registerClass:[QJMineMsgZanTableViewCell class] forCellReuseIdentifier:@"cell"];
        if (@available(iOS 11.0, *)) {
            self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
        self.tableView.estimatedRowHeight = 140;
        self.tableView.mj_header = [QJRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadData)];
        self.tableView.mj_footer = [QJRefreshFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
    }
    return _tableView;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
