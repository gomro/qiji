//
//  QJMsgSystemDetailViewController.m
//  QJBox
//
//  Created by macm on 2022/9/8.
//

#import "QJMsgSystemDetailViewController.h"

@interface QJMsgSystemDetailViewController ()

@end

@implementation QJMsgSystemDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = self.titleStr;
    
    UILabel *label = [[UILabel alloc] init];
    label.text = self.content;
    label.textColor = UIColorFromRGB(0x474849);
    label.textAlignment = NSTextAlignmentLeft;
    label.font = FontRegular(14);
    label.numberOfLines = 0;
    [self.view addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(NavigationBar_Bottom_Y+15);
        make.left.mas_equalTo(15);
        make.right.mas_equalTo(-15);
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
