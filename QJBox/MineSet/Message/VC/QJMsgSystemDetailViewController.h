//
//  QJMsgSystemDetailViewController.h
//  QJBox
//
//  Created by macm on 2022/9/8.
//

#import "QJBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJMsgSystemDetailViewController : QJBaseViewController
@property (nonatomic, copy) NSString *titleStr;
@property (nonatomic, copy) NSString *content;
@end

NS_ASSUME_NONNULL_END
