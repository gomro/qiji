//
//  QJMineMessageViewController.m
//  QJBox
//
//  Created by macm on 2022/6/20.
//

#import "QJMineMessageViewController.h"
#import "QJMineMesHeadTableViewCell.h"
#import "QJMineMsgTableViewCell.h"
#import "QJMineNotifyViewController.h"
#import "QJMineMsgReplyViewController.h"
#import "QJMineMsgOfficialViewController.h"
#import "QJMineMsgSystemViewController.h"
#import "QJMineMsgRequest.h"
#import "QJMineMsgCenterModel.h"

@interface QJMineMessageViewController ()<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *dataArray;
@end

@implementation QJMineMessageViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self sendRequest];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"消息中心";
    [self createView];
}

- (void)sendRequest {
    @weakify(self);
    QJMineMsgRequest *request = [[QJMineMsgRequest alloc] init];
    [request requestNotifyCenter_completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        @strongify(self);
        [self.tableView.mj_header endRefreshing];
        if (isSuccess) {
            self.dataArray = [NSArray modelArrayWithClass:[QJMineMsgCenterModel class] json:EncodeArrayFromDic(request.responseJSONObject, @"data")];
            [self.tableView reloadData];
        }
               
    }];
}

- (void)createView {
    [self.navBar.rightButton setHidden:NO];
    [self.navBar.rightButton setImage:[UIImage imageNamed:@"mine_msg_more"] forState:UIControlStateNormal];
    [self.navBar.rightButton addTarget:self action:@selector(rightAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.tableView];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.navBar.mas_bottom);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view);
    }];
}

- (void)rightAction {
    QJMineNotifyViewController *vc = [[QJMineNotifyViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight) style:UITableViewStyleGrouped];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = [UIColor whiteColor];
        [_tableView registerClass:[QJMineMesHeadTableViewCell class] forCellReuseIdentifier:@"headCell"];
        [_tableView registerClass:[QJMineMsgTableViewCell class] forCellReuseIdentifier:@"cell"];

        _tableView.mj_header = [QJRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(sendRequest)];

    }
    return _tableView;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return 3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        return 140*kWScale;
    }
    return 80*kWScale;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [[UIView alloc] init];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [[UIView alloc] init];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {
        QJMineMesHeadTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"headCell"];
        cell.dataArray = self.dataArray;
        return cell;

    } else {
        QJMineMsgTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
        if (indexPath.row == 1) {
            for (QJMineMsgCenterModel *model in self.dataArray) {
                if ([model.type isEqualToString:@"0"]) {
                    cell.model = model;
                }
            }
            [cell.lineView setHidden:NO];
            
        }
        if (indexPath.row == 2) {
            for (QJMineMsgCenterModel *model in self.dataArray) {
                if ([model.type isEqualToString:@"1"]) {
                    cell.model = model;
                }
            }
            [cell.lineView setHidden:YES];
            
        }
        return cell;
        
    }

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [[tableView cellForRowAtIndexPath:indexPath] setSelected:NO];
    if (indexPath.row == 0) {
        QJMineMsgReplyViewController *vc = [[QJMineMsgReplyViewController alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
    }
    if (indexPath.row == 1) {
        QJMineMsgOfficialViewController *vc = [[QJMineMsgOfficialViewController alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
    }
    if (indexPath.row == 2) {
        QJMineMsgSystemViewController *vc = [[QJMineMsgSystemViewController alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
