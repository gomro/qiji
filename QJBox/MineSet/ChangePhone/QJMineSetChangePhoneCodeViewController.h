//
//  QJMineSetChangePhoneCodeViewController.h
//  QJBox
//
//  Created by macm on 2022/7/25.
//

#import "QJBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJMineSetChangePhoneCodeViewController : QJBaseViewController
@property (nonatomic, copy) NSString *phone;
@end

NS_ASSUME_NONNULL_END
