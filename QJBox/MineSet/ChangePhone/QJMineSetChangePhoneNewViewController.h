//
//  QJMineSetChangePhoneNewViewController.h
//  QJBox
//
//  Created by macm on 2022/7/25.
//

#import "QJBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, QJSetPhoneStyle){
    QJSetPhoneChange,       // 更换手机
    QJSetPhonePassword,     // 设置密码
    QJSetPhoneCloseAccount, // 注销账号
};

@interface QJMineSetChangePhoneNewViewController : QJBaseViewController
@property (nonatomic, assign) QJSetPhoneStyle navStyle;
@property (nonatomic, copy) NSString *phone;
@property (nonatomic, copy) NSString *code;
@end

NS_ASSUME_NONNULL_END
