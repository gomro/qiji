//
//  QJMineSetChangePhoneViewController.m
//  QJBox
//
//  Created by macm on 2022/7/25.
//

#import "QJMineSetChangePhoneViewController.h"
#import "QJMineSetChangePhoneCodeViewController.h"
#import "QJMineSetRequest.h"
#import "QJImageVerifyCode.h"
@interface QJMineSetChangePhoneViewController ()<QJImageVerifyCodeDelegate>
@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *phoneLabel;
@property (nonatomic, strong) UIButton *doneButton;
@property (nonatomic, strong) QJImageVerifyCode *imageVerifyCode;
@end

@implementation QJMineSetChangePhoneViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"更换绑定";
    self.navBar.backgroundColor = [UIColor clearColor];

    [self createView];
    
    self.phoneLabel.text = self.phone;
}

- (void)doneAction {
    if (self.phone.length == 0) {
        [self.view makeToast:@"手机号不正确"];
        return;
    }
    [self getCodeFromNet];

}

- (QJImageVerifyCode *)imageVerifyCode {
    if (!_imageVerifyCode) {
        _imageVerifyCode = [QJImageVerifyCode new];
    }
    return _imageVerifyCode;
}

- (void)getCodeFromNet {
    [QJAppTool showHUDLoading];
    @weakify(self);
    QJMineSetRequest *request = [[QJMineSetRequest alloc] init];
    [request requestUserVerifyMobile:self.phone type:@"UPDATE_PHONE" completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        [QJAppTool hideHUDLoading];
        @strongify(self);
        if (isSuccess) {
            QJMineSetChangePhoneCodeViewController *vc = [[QJMineSetChangePhoneCodeViewController alloc] init];
            vc.phone = self.phone;
            [self.navigationController pushViewController:vc animated:YES];
        }else if ([EncodeStringFromDic(request.responseObject, @"code") isEqualToString:@"501"]){//出滑块
            self.imageVerifyCode = [QJImageVerifyCode new];
            self.imageVerifyCode.delegate = self;
            [self.imageVerifyCode showView];
        }

    }];

    
}

- (void)createView {
    [self.view addSubview:self.bgView];
    [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(NavigationBar_Bottom_Y+10);
        make.left.right.mas_equalTo(0);
        make.height.mas_equalTo(48);
    }];
    
    [self.bgView addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.centerY.mas_equalTo(self.bgView);
    }];
    
    [self.bgView addSubview:self.phoneLabel];
    [self.phoneLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-10);
        make.centerY.mas_equalTo(self.bgView);
    }];
    
    [self.view addSubview:self.doneButton];
}

- (UIView *)bgView {
    if (!_bgView) {
        _bgView = [[UIView alloc] init];
        _bgView.backgroundColor = [UIColor whiteColor];
    }
    return _bgView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.text = @"手机号";
        _titleLabel.textColor = UIColorFromRGB(0x919599);
        _titleLabel.font = FontRegular(14);
    }
    return _titleLabel;
}

- (UILabel *)phoneLabel {
    if (!_phoneLabel) {
        _phoneLabel = [[UILabel alloc] init];
        _phoneLabel.textColor = UIColorFromRGB(0x16191c);
        _phoneLabel.font = FontRegular(14);
    }
    return _phoneLabel;
}

- (UIButton *)doneButton {
    if (!_doneButton) {
        _doneButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _doneButton.frame = CGRectMake(35, kScreenHeight-Bottom_iPhoneX_SPACE-20-48, kScreenWidth-70, 48);
        [_doneButton setTitle:@"获取验证码" forState:UIControlStateNormal];
        [_doneButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_doneButton setBackgroundColor:kColorBtnBg];
        [_doneButton.titleLabel setFont:FontSemibold(16)];
        _doneButton.layer.cornerRadius = 4;
        [_doneButton addTarget:self action:@selector(doneAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _doneButton;
}
#pragma mark ---- QJImageVerifyCodeDelegate

/// 完成验证之后的回调
/// @param result 验证结果 BOOL:YES/NO
/// @param validate 二次校验数据，如果验证结果为false，validate返回空
/// @param message 结果描述信息
- (void)QJImageVerifyCodeValidateFinish:(BOOL)result validate:(NSString *)validate message:(NSString *)message {
    DLog(@"收到验证结果的回调:(%d,%@,%@)", result, validate, message);
    if (result) {
        [QJUserManager shareManager].validate = validate;
        [self getCodeFromNet];
      
        
    }
  
}


@end
