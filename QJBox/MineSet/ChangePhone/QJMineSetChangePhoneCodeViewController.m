//
//  QJMineSetChangePhoneCodeViewController.m
//  QJBox
//
//  Created by macm on 2022/7/25.
//

#import "QJMineSetChangePhoneCodeViewController.h"
#import "QJSMSCodeInputView.h"
#import "QJMineSetChangePhoneNewViewController.h"
#import "QJMineSetRequest.h"

@interface QJMineSetChangePhoneCodeViewController ()
@property (nonatomic, strong) QJSMSCodeInputView *smsInputView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIButton *doneButton;
@property (nonatomic, copy) NSString *code;
@end

@implementation QJMineSetChangePhoneCodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"更换绑定";
    self.navBar.backgroundColor = [UIColor clearColor];

    [self createView];
    
    // 添加对键盘的监控
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    [self startTimer];
    
    [self.smsInputView becomeFirstResponder];
}

#pragma mark  ------- 定时器

- (void)startTimer {
    __weak typeof (self) weakSelf=self;
    __block NSInteger time = 60;
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
    dispatch_source_set_timer(timer, DISPATCH_TIME_NOW, 1.0 * NSEC_PER_SEC, 0 * NSEC_PER_SEC);
    dispatch_source_set_event_handler(timer, ^{
        if (time < 1) {
            dispatch_source_cancel(timer);
            
            dispatch_async(dispatch_get_main_queue(), ^{
                weakSelf.doneButton.userInteractionEnabled = YES;
                [weakSelf.doneButton setBackgroundColor:kColorBtnBg];
                [weakSelf.doneButton setTitle:@"重新发送" forState:UIControlStateNormal];
            });
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                weakSelf.doneButton.userInteractionEnabled = NO;
                [weakSelf.doneButton setBackgroundColor:[UIColor colorWithHexString:@"#C8CACC"]];
                [weakSelf.doneButton setTitle:[NSString stringWithFormat:@"重新发送(%lds)",time] forState:UIControlStateNormal];
            });
            time --;
        }
    });
    dispatch_resume(timer);
}

- (void)doneAction {
    [self startTimer];
}

- (void)keyBoardWillShow:(NSNotification *) note {
    // 获取用户信息
    NSDictionary *userInfo = [NSDictionary dictionaryWithDictionary:note.userInfo];
    // 获取键盘高度
    CGRect keyBoardBounds  = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat keyBoardHeight = keyBoardBounds.size.height;
    // 获取键盘动画时间
    CGFloat animationTime  = [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];

    // 定义好动作
    void (^animation)(void) = ^void(void) {
        self.doneButton.transform = CGAffineTransformMakeTranslation(0, -keyBoardHeight);
    };

    if (animationTime > 0) {
        [UIView animateWithDuration:animationTime animations:animation];
    } else {
        animation();
    }
    
}

- (void)keyBoardWillHide:(NSNotification *) note {

    // 获取用户信息
    NSDictionary *userInfo = [NSDictionary dictionaryWithDictionary:note.userInfo];
    // 获取键盘动画时间
    CGFloat animationTime  = [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];

    // 定义好动作
    void (^animation)(void) = ^void(void) {
        self.doneButton.transform = CGAffineTransformIdentity;
    };

    if (animationTime > 0) {
        [UIView animateWithDuration:animationTime animations:animation];
    } else {
        animation();
    }
}

- (void)createView {
    [self.view addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(NavigationBar_Bottom_Y+12);
        make.left.mas_equalTo(35);
    }];
    
    [self.view addSubview:self.smsInputView];
    [self.smsInputView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).offset(43.5);
        make.top.equalTo(self.titleLabel.mas_bottom).offset(10);
        make.height.equalTo(@48);
        make.right.equalTo(self.view).offset(-43.5);
    }];
    
    [self.view addSubview:self.doneButton];
    
}

- (void)sendRequest {
    if (self.code.length == 0) {
        [self.view makeToast:@"验证码不正确"];
        return;
    }
    [QJAppTool showHUDLoading];
    @weakify(self);
    QJMineSetRequest *request = [[QJMineSetRequest alloc] init];
    [request requestUserVerifyCode:self.code Mobile:self.phone type:@"UPDATE_PHONE" completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        [QJAppTool hideHUDLoading];
        @strongify(self);
        if (isSuccess) {
            QJMineSetChangePhoneNewViewController *vc = [[QJMineSetChangePhoneNewViewController alloc] init];
            vc.navStyle = QJSetPhoneChange;
            vc.phone = self.phone;
            vc.code = self.code;
            [self.navigationController pushViewController:vc animated:YES];
            
        }

    }];
}

- (QJSMSCodeInputView *)smsInputView {
    if (!_smsInputView) {
        _smsInputView = [QJSMSCodeInputView new];
        @weakify(self);
        _smsInputView.codeInputFinish = ^(NSString * _Nonnull code) {
            @strongify(self);
            self.code = code;
            DLog(@"验证码=%@",code);
            [self sendRequest];
        };
        
    }
    return _smsInputView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.text = @"请输入验证码";
        _titleLabel.textColor = UIColorFromRGB(0x919599);
        _titleLabel.font = FontRegular(16);
    }
    return _titleLabel;
}

- (UIButton *)doneButton {
    if (!_doneButton) {
        _doneButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _doneButton.frame = CGRectMake(35, kScreenHeight-Bottom_iPhoneX_SPACE-20-48, kScreenWidth-70, 48);
        [_doneButton setTitle:@"获取验证码" forState:UIControlStateNormal];
        [_doneButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_doneButton setBackgroundColor:kColorBtnBg];
        [_doneButton.titleLabel setFont:FontSemibold(16)];
        _doneButton.layer.cornerRadius = 4;
        [_doneButton addTarget:self action:@selector(doneAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _doneButton;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
