//
//  QJMineSetNewPasswordViewController.m
//  QJBox
//
//  Created by macm on 2022/7/25.
//

#import "QJMineSetNewPasswordViewController.h"
#import "QJMineSetRequest.h"

@interface QJMineSetNewPasswordViewController ()<UITextFieldDelegate>
@property (nonatomic, strong) UITextField *passwordTextField;
@property (nonatomic, strong) UITextField *newPasswordTextField;
@property (nonatomic, strong) UIButton *doneButton;
@property (nonatomic, strong) UIButton *passButton;
@property (nonatomic, strong) UIButton *newPassButton;

@end

@implementation QJMineSetNewPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"设置密码";
    [self createView];
    // 添加对键盘的监控
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyBoardWillShow:(NSNotification *) note {
    // 获取用户信息
    NSDictionary *userInfo = [NSDictionary dictionaryWithDictionary:note.userInfo];
    // 获取键盘高度
    CGRect keyBoardBounds  = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat keyBoardHeight = keyBoardBounds.size.height;
    // 获取键盘动画时间
    CGFloat animationTime  = [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];

    // 定义好动作
    void (^animation)(void) = ^void(void) {
        self.doneButton.transform = CGAffineTransformMakeTranslation(0, -keyBoardHeight);
    };

    if (animationTime > 0) {
        [UIView animateWithDuration:animationTime animations:animation];
    } else {
        animation();
    }
    
}

- (void)keyBoardWillHide:(NSNotification *) note {

    // 获取用户信息
    NSDictionary *userInfo = [NSDictionary dictionaryWithDictionary:note.userInfo];
    // 获取键盘动画时间
    CGFloat animationTime  = [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];

    // 定义好动作
    void (^animation)(void) = ^void(void) {
        self.doneButton.transform = CGAffineTransformIdentity;
    };

    if (animationTime > 0) {
        [UIView animateWithDuration:animationTime animations:animation];
    } else {
        animation();
    }
}

- (void)showPassAction:(UIButton *)btn {
    btn.selected = !btn.selected;
    self.passwordTextField.secureTextEntry = !btn.selected;
}

- (void)showNewPassAction:(UIButton *)btn {
    btn.selected = !btn.selected;
    self.newPasswordTextField.secureTextEntry = !btn.selected;
}

- (void)doneAction {
    if (self.passwordTextField.text.length < 6) {
        [self.view makeToast:@"请设置密码最少6位"];
        return;
    }
    if (self.passwordTextField.text.length > 16) {
        [self.view makeToast:@"请设置密码最大16位"];
        return;
    }
    if (![self.passwordTextField.text isEqualToString:self.newPasswordTextField.text]) {
        [self.view makeToast:@"两次密码不一致"];
        return;
    }
    [QJAppTool showHUDLoading];
    @weakify(self);
    QJMineSetRequest *request = [[QJMineSetRequest alloc] init];
    [request requestUserSetpwdCode:self.code Mobile:self.phone checkedPassword:self.passwordTextField.text newPassword:self.newPasswordTextField.text completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        [QJAppTool hideHUDLoading];
        @strongify(self);
        if (isSuccess) {
            [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:2] animated:YES];
            [[QJAppTool getCurrentViewController].view makeToast:@"重置成功 请登录" duration:2 position:CSToastPositionCenter image:[UIImage imageNamed:@"success_login"]];
        }

    }];
    
}

//- (void)textFeildChange:(UITextField *)textField {
//    if (self.passwordTextField.text.length > 0 && self.newPasswordTextField.text.length > 0) {
//        [self.doneButton setEnabled:YES];
//        [self.doneButton setBackgroundColor:kColorBtnBg];
//    } else {
//        [self.doneButton setEnabled:NO];
//        [self.doneButton setBackgroundColor:UIColorFromRGB(0xc8cacc)];
//    }
//}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    // 隐藏表情键盘
    if ([[textField.textInputMode primaryLanguage] isEqualToString:@"emoji"] || ![textField.textInputMode primaryLanguage]) {
        return NO;
    }
    //判断键盘是不是九宫格键盘
    if ([NSString isNineKeyBoard:string] ){
        return YES;
    }else{
        if ([NSString hasEmoji:string] || [NSString isContainsTwoEmoji:string]){
            return NO;
        }
    }
    
    if (range.location > 15) {
        return NO;
    }
    
    // 密文切换不置空
    if (textField.isSecureTextEntry) {
        NSString *secureString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        textField.text = secureString;
        return NO;
    }
    return YES;
}

- (void)createView {
    self.navBar.backgroundColor = [UIColor clearColor];

    [self.view addSubview:self.passwordTextField];
    [self.passwordTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(NavigationBar_Bottom_Y + 8);
        make.left.right.mas_equalTo(0);
        make.height.mas_equalTo(48);
    }];
    
    [self.view addSubview:self.newPasswordTextField];
    [self.newPasswordTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.passwordTextField.mas_bottom).mas_offset(1);
        make.left.right.mas_equalTo(0);
        make.height.mas_equalTo(48);
    }];
    
    [self.view addSubview:self.passButton];
    [self.passButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-15);
        make.centerY.mas_equalTo(self.passwordTextField);
    }];
    
    [self.view addSubview:self.newPassButton];
    [self.newPassButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-15);
        make.centerY.mas_equalTo(self.newPasswordTextField);
    }];
    
    [self.view addSubview:self.doneButton];
}

- (UITextField *)passwordTextField {
    if (!_passwordTextField) {
        _passwordTextField = [UITextField new];
        _passwordTextField.backgroundColor = [UIColor whiteColor];
        UIView *left = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 10)];
        _passwordTextField.leftView = left;
        _passwordTextField.leftViewMode = UITextFieldViewModeAlways;
        _passwordTextField.font = kFont(14);
        NSAttributedString *attStr = [[NSAttributedString alloc]initWithString:@"请设置密码" attributes:@{NSFontAttributeName:FontRegular(14),NSForegroundColorAttributeName:[UIColor colorWithHexString:@"#919599"]}];
        _passwordTextField.attributedPlaceholder = attStr;
        _passwordTextField.delegate = self;
        _passwordTextField.secureTextEntry = YES;
//        [_passwordTextField addTarget:self action:@selector(textFeildChange:) forControlEvents:UIControlEventEditingChanged];

    }
    return _passwordTextField;
}

- (UITextField *)newPasswordTextField {
    if (!_newPasswordTextField) {
        _newPasswordTextField = [UITextField new];
        _newPasswordTextField.backgroundColor = [UIColor whiteColor];
        UIView *left = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 10)];
        _newPasswordTextField.leftView = left;
        _newPasswordTextField.leftViewMode = UITextFieldViewModeAlways;
        _newPasswordTextField.font = kFont(14);
        NSAttributedString *attStr = [[NSAttributedString alloc]initWithString:@"请重新输入密码" attributes:@{NSFontAttributeName:FontRegular(14),NSForegroundColorAttributeName:[UIColor colorWithHexString:@"#919599"]}];
        _newPasswordTextField.attributedPlaceholder = attStr;
        _newPasswordTextField.delegate = self;
        _newPasswordTextField.secureTextEntry = YES;
//        [_newPasswordTextField addTarget:self action:@selector(textFeildChange:) forControlEvents:UIControlEventEditingChanged];

    }
    return _newPasswordTextField;
}

- (UIButton *)doneButton {
    if (!_doneButton) {
        _doneButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _doneButton.frame = CGRectMake(35, kScreenHeight-Bottom_iPhoneX_SPACE-20-48, kScreenWidth-70, 48);
        [_doneButton setTitle:@"确认" forState:UIControlStateNormal];
        [_doneButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//        [_doneButton setBackgroundColor:UIColorFromRGB(0xc8cacc)];
        [_doneButton setBackgroundColor:kColorBtnBg];
        [_doneButton.titleLabel setFont:FontSemibold(16)];
        _doneButton.layer.cornerRadius = 4;
        [_doneButton addTarget:self action:@selector(doneAction) forControlEvents:UIControlEventTouchUpInside];
//        [_doneButton setEnabled:NO];
    }
    return _doneButton;
}

- (UIButton *)passButton {
    if (!_passButton) {
        _passButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_passButton setImage:[UIImage imageNamed:@"preview-closelogin"] forState:UIControlStateNormal];
        [_passButton setImage:[UIImage imageNamed:@"previewlogin"] forState:UIControlStateSelected];
        [_passButton addTarget:self action:@selector(showPassAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _passButton;
}

- (UIButton *)newPassButton {
    if (!_newPassButton) {
        _newPassButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_newPassButton setImage:[UIImage imageNamed:@"preview-closelogin"] forState:UIControlStateNormal];
        [_newPassButton setImage:[UIImage imageNamed:@"previewlogin"] forState:UIControlStateSelected];
        [_newPassButton addTarget:self action:@selector(showNewPassAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _newPassButton;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
