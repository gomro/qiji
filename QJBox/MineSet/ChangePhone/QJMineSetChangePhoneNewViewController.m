//
//  QJMineSetChangePhoneNewViewController.m
//  QJBox
//
//  Created by macm on 2022/7/25.
//

#import "QJMineSetChangePhoneNewViewController.h"
#import "QJMineSetNewPasswordViewController.h"
#import "QJMineSafeViewController.h"
#import "QJMineSetRequest.h"
#import "QJMineSetCloseAccountViewController.h"
#import "QJImageVerifyCode.h"
@interface QJMineSetChangePhoneNewViewController ()<UITextFieldDelegate,QJImageVerifyCodeDelegate>
@property (nonatomic, strong) UITextField *phoneTextField;
@property (nonatomic, strong) UITextField *codeTextField;
@property (nonatomic, strong) UIButton *codeButton;
@property (nonatomic, strong) UIButton *doneButton;
@property (nonatomic, strong) QJImageVerifyCode *imageVerifyCode;
@property (nonatomic, copy) NSString *codeType;
@end

@implementation QJMineSetChangePhoneNewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if (self.navStyle == QJSetPhoneChange) {
        self.title = @"更改绑定";
        self.codeType = @"UPDATE_PHONE";
    }
    if (self.navStyle == QJSetPhonePassword) {
        self.title = @"设置密码";
        self.codeType = @"UPDATE_PASSWORD";
    }
    if (self.navStyle == QJSetPhoneCloseAccount) {
        self.title = @"注销确认";
        self.codeType = @"CANCEL";
    }
    [self createView];
    
    if (self.navStyle == QJSetPhonePassword || self.navStyle == QJSetPhoneCloseAccount) {
        self.phoneTextField.text = self.phone;
        [self.phoneTextField setEnabled:NO];
        [self.doneButton setTitle:@"下一步" forState:UIControlStateNormal];
        self.phoneTextField.textColor = kColorGray;
    }
    
    // 添加对键盘的监控
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyBoardWillShow:(NSNotification *) note {
    // 获取用户信息
    NSDictionary *userInfo = [NSDictionary dictionaryWithDictionary:note.userInfo];
    // 获取键盘高度
    CGRect keyBoardBounds  = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat keyBoardHeight = keyBoardBounds.size.height;
    // 获取键盘动画时间
    CGFloat animationTime  = [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];

    // 定义好动作
    void (^animation)(void) = ^void(void) {
        self.doneButton.transform = CGAffineTransformMakeTranslation(0, -keyBoardHeight);
    };

    if (animationTime > 0) {
        [UIView animateWithDuration:animationTime animations:animation];
    } else {
        animation();
    }
    
}

- (void)keyBoardWillHide:(NSNotification *) note {

    // 获取用户信息
    NSDictionary *userInfo = [NSDictionary dictionaryWithDictionary:note.userInfo];
    // 获取键盘动画时间
    CGFloat animationTime  = [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];

    // 定义好动作
    void (^animation)(void) = ^void(void) {
        self.doneButton.transform = CGAffineTransformIdentity;
    };

    if (animationTime > 0) {
        [UIView animateWithDuration:animationTime animations:animation];
    } else {
        animation();
    }
}

- (void)codeAction {
    if (self.phoneTextField.text.length == 0) {
        [self.view makeToast:@"手机号不正确"];
        return;
    }

    [self getCode];
}


- (void)getCode {
    [QJAppTool showHUDLoading];
    @weakify(self);
    QJMineSetRequest *request = [[QJMineSetRequest alloc] init];
    [request requestUserVerifyMobile:self.phoneTextField.text type:self.codeType completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        [QJAppTool hideHUDLoading];
        @strongify(self);
        if (isSuccess) {
            [self startTimer];

        }else if ([EncodeStringFromDic(request.responseObject, @"code") isEqualToString:@"501"]){//出滑块
            self.imageVerifyCode = [QJImageVerifyCode new];
            self.imageVerifyCode.delegate = self;
            [self.imageVerifyCode showView];
        }

    }];
}

- (void)doneAction {
    if (self.phoneTextField.text.length == 0) {
        [self.view makeToast:@"手机号不正确"];
        return;
    }
    if (self.codeTextField.text.length == 0) {
        [self.view makeToast:@"验证码不正确"];
        return;
    }
    if (self.navStyle == QJSetPhonePassword) {
        [QJAppTool showHUDLoading];
        @weakify(self);
        QJMineSetRequest *request = [[QJMineSetRequest alloc] init];
        [request requestUserVerifyCode:self.codeTextField.text Mobile:self.phone type:self.codeType completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
            [QJAppTool hideHUDLoading];
            @strongify(self);
            if (isSuccess) {
                QJMineSetNewPasswordViewController *vc = [[QJMineSetNewPasswordViewController alloc] init];
                vc.phone = self.phoneTextField.text;
                vc.code = self.codeTextField.text;
                [self.navigationController pushViewController:vc animated:YES];

            }

        }];
        
    }
    if (self.navStyle == QJSetPhoneChange) {
        [QJAppTool showHUDLoading];
        @weakify(self);
        QJMineSetRequest *request = [[QJMineSetRequest alloc] init];
        [request requestUserUpdateCode:self.code mobile:self.phone newCode:self.codeTextField.text newMobile:self.phoneTextField.text completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
            [QJAppTool hideHUDLoading];
            @strongify(self);
            if (isSuccess) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"SetChangePhone" object:self.phoneTextField.text];
                [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:2] animated:YES];

            }

        }];
    }
    if (self.navStyle == QJSetPhoneCloseAccount) {
        [QJAppTool showHUDLoading];
        @weakify(self);
        QJMineSetRequest *request = [[QJMineSetRequest alloc] init];
        [request requestUserVerifyCode:self.codeTextField.text Mobile:self.phone type:@"CANCEL" completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
            [QJAppTool hideHUDLoading];
            @strongify(self);
            if (isSuccess) {
                QJMineSetCloseAccountViewController *vc = [[QJMineSetCloseAccountViewController alloc] init];
                vc.code = self.codeTextField.text;
                vc.phone = self.phone;
                [self.navigationController pushViewController:vc animated:YES];
                
            }

        }];
        
    }
    
}
- (void)textFeildChange:(UITextField *)textField {
    if (self.phoneTextField.text.length == 11 && self.codeTextField.text.length == 4) {
        [self.doneButton setEnabled:YES];
        [self.doneButton setBackgroundColor:kColorBtnBg];
    } else {
        [self.doneButton setEnabled:NO];
        [self.doneButton setBackgroundColor:UIColorFromRGB(0xc8cacc)];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField == self.phoneTextField) {
        if (range.location > 10) {
            return NO;
        }
    }
    if (textField == self.codeTextField) {
        if (range.location > 3) {
            return NO;
        }
    }
    
    return YES;
}

#pragma mark  ------- 定时器

- (void)startTimer {
    __weak typeof (self) weakSelf=self;
    __block NSInteger time = 60;
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
    dispatch_source_set_timer(timer, DISPATCH_TIME_NOW, 1.0 * NSEC_PER_SEC, 0 * NSEC_PER_SEC);
    dispatch_source_set_event_handler(timer, ^{
        if (time < 1) {
            dispatch_source_cancel(timer);
            
            dispatch_async(dispatch_get_main_queue(), ^{
                weakSelf.codeButton.userInteractionEnabled = YES;
                [weakSelf.codeButton setTitle:@"重新发送" forState:UIControlStateNormal];
                [weakSelf.codeButton setTitleColor:UIColorFromRGB(0x007aff) forState:UIControlStateNormal];
            });
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                weakSelf.codeButton.userInteractionEnabled = NO;
                [weakSelf.codeButton setTitle:[NSString stringWithFormat:@"重新发送(%lds)",time] forState:UIControlStateNormal];
                [weakSelf.codeButton setTitleColor:UIColorFromRGB(0x919599) forState:UIControlStateNormal];
            });
            time --;
        }
    });
    dispatch_resume(timer);
}

- (void)createView {
    self.navBar.backgroundColor = [UIColor clearColor];

    [self.view addSubview:self.phoneTextField];
    [self.phoneTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(NavigationBar_Bottom_Y + 8);
        make.left.right.mas_equalTo(0);
        make.height.mas_equalTo(48);
    }];
    
    [self.view addSubview:self.codeTextField];
    [self.codeTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.phoneTextField.mas_bottom).mas_offset(1);
        make.left.right.mas_equalTo(0);
        make.height.mas_equalTo(48);
    }];
    
    [self.view addSubview:self.codeButton];
    [self.codeButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.codeTextField);
        make.right.mas_equalTo(-15);
        make.height.mas_equalTo(48);
    }];
    
    [self.view addSubview:self.doneButton];
}

- (UITextField *)phoneTextField {
    if (!_phoneTextField) {
        _phoneTextField = [UITextField new];
        _phoneTextField.backgroundColor = [UIColor whiteColor];
        UIView *left = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 10)];
        _phoneTextField.leftView = left;
        _phoneTextField.leftViewMode = UITextFieldViewModeAlways;
        _phoneTextField.font = kFont(14);
        NSAttributedString *attStr = [[NSAttributedString alloc]initWithString:@"请输入手机号" attributes:@{NSFontAttributeName:FontRegular(14),NSForegroundColorAttributeName:[UIColor colorWithHexString:@"#919599"]}];
        _phoneTextField.attributedPlaceholder = attStr;
        _phoneTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
        _phoneTextField.keyboardType = UIKeyboardTypeNumberPad;
        _phoneTextField.delegate = self;
        [_phoneTextField addTarget:self action:@selector(textFeildChange:) forControlEvents:UIControlEventEditingChanged];

    }
    return _phoneTextField;
}

- (UITextField *)codeTextField {
    if (!_codeTextField) {
        _codeTextField = [UITextField new];
        _codeTextField.backgroundColor = [UIColor whiteColor];
        UIView *left = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 10)];
        _codeTextField.leftView = left;
        _codeTextField.leftViewMode = UITextFieldViewModeAlways;
        _codeTextField.font = kFont(14);
        NSAttributedString *attStr = [[NSAttributedString alloc]initWithString:@"请输入验证码" attributes:@{NSFontAttributeName:FontRegular(14),NSForegroundColorAttributeName:[UIColor colorWithHexString:@"#919599"]}];
        _codeTextField.attributedPlaceholder = attStr;
        _codeTextField.keyboardType = UIKeyboardTypeNumberPad;
        _codeTextField.delegate = self;
        [_codeTextField addTarget:self action:@selector(textFeildChange:) forControlEvents:UIControlEventEditingChanged];

    }
    return _codeTextField;
}

- (UIButton *)codeButton {
    if (!_codeButton) {
        _codeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_codeButton setTitle:@"获取验证码" forState:UIControlStateNormal];
        [_codeButton setTitleColor:UIColorFromRGB(0x007aff) forState:UIControlStateNormal];
        [_codeButton.titleLabel setFont:FontRegular(14)];
        [_codeButton addTarget:self action:@selector(codeAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _codeButton;
}

- (UIButton *)doneButton {
    if (!_doneButton) {
        _doneButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _doneButton.frame = CGRectMake(35, kScreenHeight-Bottom_iPhoneX_SPACE-20-48, kScreenWidth-70, 48);
        [_doneButton setTitle:@"完成" forState:UIControlStateNormal];
        [_doneButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_doneButton setBackgroundColor:UIColorFromRGB(0xc8cacc)];
        [_doneButton.titleLabel setFont:FontSemibold(16)];
        _doneButton.layer.cornerRadius = 4;
        [_doneButton addTarget:self action:@selector(doneAction) forControlEvents:UIControlEventTouchUpInside];
        [_doneButton setEnabled:NO];
    }
    return _doneButton;
}

- (QJImageVerifyCode *)imageVerifyCode {
    if (!_imageVerifyCode) {
        _imageVerifyCode = [QJImageVerifyCode new];
    }
    return _imageVerifyCode;
}
#pragma mark ---- QJImageVerifyCodeDelegate

/// 完成验证之后的回调
/// @param result 验证结果 BOOL:YES/NO
/// @param validate 二次校验数据，如果验证结果为false，validate返回空
/// @param message 结果描述信息
- (void)QJImageVerifyCodeValidateFinish:(BOOL)result validate:(NSString *)validate message:(NSString *)message {
    DLog(@"收到验证结果的回调:(%d,%@,%@)", result, validate, message);
    if (result) {
        [QJUserManager shareManager].validate = validate;
        [self getCode];
      
        
    }
  
}


@end
