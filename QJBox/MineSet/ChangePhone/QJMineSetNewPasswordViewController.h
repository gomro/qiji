//
//  QJMineSetNewPasswordViewController.h
//  QJBox
//
//  Created by macm on 2022/7/25.
//

#import "QJBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJMineSetNewPasswordViewController : QJBaseViewController
@property (nonatomic, copy) NSString *phone;
@property (nonatomic, copy) NSString *code;
@end

NS_ASSUME_NONNULL_END
