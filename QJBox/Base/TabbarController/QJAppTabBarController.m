//
//  QJAppTabBarController.m
//  QJBox
//
//  Created by wxy on 2022/6/1.
//

#import "QJAppTabBarController.h"
#import "QJCustomTabBarView.h"
#import "QJCustomTabBarItem.h"
#import "QJCustomTabBar.h"
#import "QJNavigationController.h"
#import "QJHomeViewController.h"
#import "QJCommunityViewController.h"
#import "QJMineViewController.h"
#import "QJProductCenterViewController.h"
#import "QJMyBoxListViewController.h"

@interface QJAppTabBarController ()

@property (nonatomic, strong) QJCustomTabBarView *tabBarView;

@property (nonatomic, strong) QJNavigationController *homeNavController;

@property (nonatomic, strong) QJNavigationController *weNavController;

@property (nonatomic, strong) QJNavigationController *myNavController;

@property (nonatomic, strong) QJNavigationController *communityNavController;

@property (nonatomic, strong) QJNavigationController *mineNavController;




@end

@implementation QJAppTabBarController

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (id)init {
    if (self = [super init]) {
        //解决UITabBar默认的ShadowImage导致的有一条细细的分割线的问题
        [[UITabBar appearance] setShadowImage:[UIImage new]];
        [[UITabBar appearance] setBackgroundImage:[[UIImage alloc]init]];
        self.tabBar.tintColor = [UIColor blackColor];
    }
    return  self;
}

- (void)initTabbar{
    self.delegate = self;
    
    self.tabBar.clipsToBounds = NO;
    //修复iPhone X底部Tab沉底问题
    if (iPhoneX) {
        self.tabBar.translucent = NO;
    }
    
    //设置自定义的tabBarView
    self.tabBarView = [[QJCustomTabBarView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 49+ Bottom_iPhoneX_SPACE)];
    self.tabBarView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.tabBarView.layer.shadowOpacity = 0.1;
    self.tabBarView.layer.shadowRadius = 2;
    self.tabBarView.layer.shadowOffset = CGSizeMake(0, -1);

    QJCustomTabBar *myTabBar = [[QJCustomTabBar alloc] init];
//    [myTabBar.centerBtn addTarget:self action:@selector(centerBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    myTabBar.backgroundColor = [UIColor whiteColor];
    [self setValue:myTabBar forKey:@"tabBar"];
    self.tabBarView.tabbar = self.tabBar;
    [self.tabBar addSubview:self.tabBarView];
}

//- (void)loadView{
//    [super loadView];
//
//    self.tabBar.clipsToBounds = NO;
//    //修复iPhone X底部Tab沉底问题
//    if (iPhoneX) {
//        self.tabBar.translucent = NO;
//    }
//
//    //设置自定义的tabBarView
//    self.tabBarView = [[QJCustomTabBarView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 49+ Bottom_iPhoneX_SPACE)];
//    self.tabBarView.layer.shadowColor = [UIColor blackColor].CGColor;
//    self.tabBarView.layer.shadowOpacity = 0.1;
//    self.tabBarView.layer.shadowRadius = 2;
//    self.tabBarView.layer.shadowOffset = CGSizeMake(0, -1);
//
//    QJCustomTabBar *myTabBar = [[QJCustomTabBar alloc] init];
////    [myTabBar.centerBtn addTarget:self action:@selector(centerBtnClick:) forControlEvents:UIControlEventTouchUpInside];
//    myTabBar.backgroundColor = [UIColor whiteColor];
//    [self setValue:myTabBar forKey:@"tabBar"];
//    self.tabBarView.tabbar = self.tabBar;
//    [self.tabBar addSubview:self.tabBarView];
//}

//-(void)centerBtnClick:(id *)sender{
//    NSLog(@"center btn click");
//    self.selectedIndex = 2;
//}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initTabbar];
    //首页
    QJHomeViewController *homeVC = [QJHomeViewController new];
    [homeVC setNavBarBackItemHidden:YES];
    homeVC.hidesBottomBarWhenPushed = NO;
    self.homeNavController = [[QJNavigationController alloc]initWithRootViewController:homeVC];
    
    //游戏
    QJProductCenterViewController *weVC = [QJProductCenterViewController new];
    [weVC setNavBarBackItemHidden:YES];
    weVC.hidesBottomBarWhenPushed = NO;
    self.weNavController = [[QJNavigationController alloc]initWithRootViewController:weVC];
    
    //我的游戏
    QJMyBoxListViewController *myVC = [QJMyBoxListViewController new];
    [myVC setNavBarBackItemHidden:YES];
    myVC.hidesBottomBarWhenPushed = NO;
    self.myNavController = [[QJNavigationController alloc]initWithRootViewController:myVC];
    
    //社区
    QJCommunityViewController *communityVC = [QJCommunityViewController new];
    [communityVC setNavBarBackItemHidden:YES];
    communityVC.hidesBottomBarWhenPushed = NO;
    self.communityNavController = [[QJNavigationController alloc]initWithRootViewController:communityVC];
    
    //我的
    QJMineViewController *mineVC = [QJMineViewController new];
    [mineVC setNavBarBackItemHidden:YES];
    mineVC.hidesBottomBarWhenPushed = NO;
    self.mineNavController = [[QJNavigationController alloc]initWithRootViewController:mineVC];
    
    
    self.viewControllers = @[self.homeNavController,self.weNavController,self.myNavController,self.communityNavController,self.mineNavController];
    
    [self showNormalTab];
}

/**
 *  手动点击底部的tabbar会调用这个方法
 *
 */
- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController{
    QJNavigationController *navController = (QJNavigationController *)viewController;
    WS(weakSelf)
    id obj = [navController.viewControllers firstObject];
    if (![QJUserManager shareManager].isLogin) {
        if ([obj isKindOfClass:[QJCommunityViewController class]] || [obj isKindOfClass:[QJMineViewController class]] || [obj isKindOfClass:[QJProductCenterViewController class]] || [obj isKindOfClass:[QJMyBoxListViewController class]]) {
            [QJUserManager shareManager].isNetLogout = NO;
            [self showLoginVCCompleted:^(BOOL isLogin) {
                NSInteger index = 0;
                if ([obj isKindOfClass:[QJProductCenterViewController class]]) {
                    index = 1;
                }else if ([obj isKindOfClass:[QJMyBoxListViewController class]]){
                    index = 2;
                }else if ([obj isKindOfClass:[QJCommunityViewController class]]){
                    index = 3;
                }else if ([obj isKindOfClass:[QJMineViewController class]]){
                    index = 4;
                }
                weakSelf.selectedIndex = index;
            }];
            return NO;
        }
    }
    return YES;
}

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController {
    QJNavigationController *navController = (QJNavigationController *)viewController;
    int tabBarIndex = 0;
    id obj = [navController.viewControllers firstObject];
    if ([obj isKindOfClass:[QJHomeViewController class]]) {//首页
        tabBarIndex = 0;
    }else if ([obj isKindOfClass:[QJProductCenterViewController class]]) {//游戏
        tabBarIndex = 1;
    }else if ([obj isKindOfClass:[QJMyBoxListViewController class]]) {//游戏
        tabBarIndex = 2;
    }else if ([obj isKindOfClass:[QJCommunityViewController class]]) {//社区
        tabBarIndex = 3;
    }else if ([obj isKindOfClass:[QJMineViewController class]]) {//我的
        tabBarIndex = 4;
    }
    [self.tabBarView setSelectedIndex:tabBarIndex];
}

// 重写系统函数
- (void)setSelectedIndex:(NSInteger)index {
    [super setSelectedIndex:index];
    [self.tabBarView setSelectedIndex:index];
}

#pragma mark ------ method
//显示tabbar
- (void)showNormalTab {
    //设置item
    QJCustomTabBarItem *homeItem = [[QJCustomTabBarItem alloc]initWithTitle:@"首页" normalImageName:@"tabbar_homeItem_normal" selectedImageName:@"tabbar_homeItem_selected" imageType:@"0"];
    QJCustomTabBarItem *weItem = [[QJCustomTabBarItem alloc]initWithTitle:@"游戏" normalImageName:@"tabbar_YXItem_normal" selectedImageName:@"tabbar_YXItem_selected" imageType:@"0"];
    QJCustomTabBarItem *myItem = [[QJCustomTabBarItem alloc]initWithTitle:@"在玩" normalImageName:@"tabbar_game_normal" selectedImageName:@"tabbar_game_selected" imageType:@"1"];
    QJCustomTabBarItem *communityItem = [[QJCustomTabBarItem alloc]initWithTitle:@"社区" normalImageName:@"tabbar_shequ_normal" selectedImageName:@"tabbar_shequ_selected" imageType:@"0"];
    QJCustomTabBarItem *mineItem = [[QJCustomTabBarItem alloc]initWithTitle:@"我的" normalImageName:@"tabbar_mine_normal" selectedImageName:@"tabbar_mine_selected" imageType:@"0"];
    [self.tabBarView setNewItemArray:@[homeItem,weItem,myItem,communityItem,mineItem]];
    [self.tabBarView setBackgroundWithColor:[UIColor whiteColor] img:@""];
    
}

#pragma mark --- 旋转控制
- (UIViewController *)sj_topViewController {
    if ( self.selectedIndex == NSNotFound )
        return self.viewControllers.firstObject;
    return self.selectedViewController;
}

- (BOOL)shouldAutorotate {
    return [[self sj_topViewController] shouldAutorotate];
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return [[self sj_topViewController] supportedInterfaceOrientations];
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return [[self sj_topViewController] preferredInterfaceOrientationForPresentation];
}

@end
