//
//  QJAppTabBarController.h
//  QJBox
//
//  Created by wxy on 2022/6/1.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJAppTabBarController : UITabBarController<UITabBarControllerDelegate>


// 重写系统函数
- (void)setSelectedIndex:(NSInteger)index;

 
 

@end

NS_ASSUME_NONNULL_END
