//
//  QJCustomTabBar.m
//  QJBox
//
//  Created by wxy on 2022/6/1.
//

#import "QJCustomTabBar.h"

@implementation QJCustomTabBar

//- (instancetype)init{
//    if (self = [super init]) {
//        [self initView];
//    }
//    return self;
//}

//- (void)initView{
//    _centerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//
//    //  设定button大小为适应图片
//    UIImage *normalImage = [UIImage imageNamed:@"tabbar_game_normal"];
//
//    _centerBtn.frame = CGRectMake(0, 0, normalImage.size.width, normalImage.size.height);
//    [_centerBtn setImage:normalImage forState:UIControlStateNormal];
//    //去除选择时高亮
//    _centerBtn.adjustsImageWhenHighlighted = NO;
//    _centerBtn.frame = CGRectMake((QJScreenWidth - normalImage.size.width)/2, -normalImage.size.height/2, normalImage.size.width, normalImage.size.height);
//    [self addSubview:_centerBtn];
//}

//处理超出区域点击无效的问题
//- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
//    if (self.isHidden == NO) {
//        CGPoint newPoint = [self convertPoint:point toView:self.centerBtn];
//        if ( [self.centerBtn pointInside:newPoint withEvent:event]) {
//            return self.centerBtn;
//        }else{
//            return [super hitTest:point withEvent:event];
//        }
//    }else {
//        return [super hitTest:point withEvent:event];
//    }
//}

@end
