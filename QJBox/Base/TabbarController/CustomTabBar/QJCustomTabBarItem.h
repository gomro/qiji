//
//  QJCustomTabBarItem.h
//  QJBox
//
//  Created by wxy on 2022/6/1.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJCustomTabBarItem : UIView

@property (nonatomic, copy) void (^clickTabBarItemBlock)(void);

- (id)initWithTitle:(NSString *)title normalImageName:(NSString *)normalImageName selectedImageName:(NSString *)selectedImageName imageType:(NSString *)type;

//imageType,a-一般，b－大图
- (id)initWithTitle:(NSString *)title
        selectTitle:(NSString *)sTitle
        normalImage:(UIImage *)normalImage
      selectedImage:(UIImage *)selectedImage
          imageType:(NSString *)type
   normalTitleColor:(NSString *)normalTitleColor
 selectedTitleColor:(NSString *)selectedTitleColor;

- (void)setSelected;

- (void)cancelSelected;

- (void)changeSelectImage:(UIImage *)selectImage;

- (void)changeSelectTitle:(NSString *)selectTitle;

- (void)changeSelectTitleColor:(UIColor *)color;

- (NSString *)selectTitle;




@end

NS_ASSUME_NONNULL_END
