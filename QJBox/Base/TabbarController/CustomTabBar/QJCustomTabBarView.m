//
//  QJCustomTabBarView.m
//  QJBox
//
//  Created by wxy on 2022/6/1.
//

#import "QJCustomTabBarView.h"
#import "QJCustomTabBarItem.h"



@interface QJCustomTabBarView ()

@property (nonatomic, strong) NSMutableArray *itemArray;

@property (nonatomic, strong) UIView *lineImageView;

@property (nonatomic, strong) UIImageView *backImageView;

@property (nonatomic, strong) UIColor *originalBgColor;

@property (nonatomic, strong) UIColor *originalLineColor;

@end


@implementation QJCustomTabBarView


- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        currentSelectIndex = 0;
        self.itemArray = [[NSMutableArray alloc] init];
        self.alpha = 1;
        self.backgroundColor = [UIColor colorWithHexString:@"ffffff"];
        
        self.lineImageView = [[UIView alloc] init];
        self.lineImageView.frame = CGRectMake(0, -1, frame.size.width, 1);
        self.lineImageView.backgroundColor = [UIColor colorWithHexString:@"0xf2f2f2"];
        [self addSubview:self.lineImageView];
        
        self.backImageView = [[UIImageView alloc] init];
        self.backImageView.frame = CGRectMake(0, 0, frame.size.width, frame.size.height);
        self.backImageView.backgroundColor = [UIColor clearColor];
        self.backImageView.contentMode = UIViewContentModeScaleAspectFill;
        self.backImageView.clipsToBounds = YES;
        [self addSubview:self.backImageView];

        
        [self setBackgroundGlassEffect];
    }
    
    return self;
}


- (void)setNewItemArray:(NSArray *)itemArrar {
    //data
    [self.itemArray removeAllObjects];
    [self.itemArray addObjectsFromArray:itemArrar];
    
    //view
    NSMutableArray *barItemArray = [NSMutableArray array];
    for (UIView *view in self.subviews) {
        if ([view isKindOfClass:[QJCustomTabBarItem class]]) {
            [barItemArray safeAddObject:view];
        }
    }
    [barItemArray makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    for (int i = 0; i < [itemArrar count]; i++) {
        QJCustomTabBarItem *item = (QJCustomTabBarItem *)[itemArrar safeObjectAtIndex:i];
        [self addSubview:item];
        [item cancelSelected];
        
    }
    
    
    [self setSelectedIndex:currentSelectIndex animal:NO];
    
    [self resizeAllSubView];
}




- (void)setSelectedIndex:(NSInteger)index animal:(BOOL)animal {
    //老的item取消选中状态
    QJCustomTabBarItem *oldItem = [self.itemArray safeObjectAtIndex:currentSelectIndex];
    [oldItem cancelSelected];
    UITabBarItem *tabItem = [self.tabbar.items safeObjectAtIndex:currentSelectIndex];
    
    QJCustomTabBarItem *newItem = [self.itemArray safeObjectAtIndex:index];
    [newItem setSelected];
    tabItem = [self.tabbar.items safeObjectAtIndex:index];
    currentSelectIndex = index;
}

- (void)setSelectedIndex:(NSInteger)index {
    [self setSelectedIndex:index animal:NO];
}

- (void)resizeAllSubView {
    NSMutableArray *barItemArray = [NSMutableArray array];
    for (UIView *view in self.subviews) {
        if ([view isKindOfClass:[QJCustomTabBarItem class]]) {
            [barItemArray safeAddObject:view];
        }
    }
    NSUInteger count = [barItemArray count];
    
    //count个item平分屏幕的宽度，但是内部的元素集中在50*49的size内
    CGFloat screenWidth = kScreenWidth;//MIN(kScreenWidth, kScreenHeight);
    int perWidth = screenWidth/count;
    
    for (int i = 0; i < count; i++) {
        UIView *view = [barItemArray safeObjectAtIndex:i];
        if ([view isKindOfClass:[QJCustomTabBarItem class]]) {
            QJCustomTabBarItem *tempItem = (QJCustomTabBarItem *)view;
            tempItem.frame = CGRectMake(i * perWidth, 0, perWidth, 49);
            [tempItem layoutSubviews];
        }
        else {
            NSLog(@"Warning: a subView of CustomTabBarView is not CustomTabBarItem instance!");
        }
    }
}

-(void)changeFirstTabBarSelectView:(UIImage *)selectImg title:(NSString *)title{
    
    QJCustomTabBarItem *firstItem = [self.itemArray safeObjectAtIndex:0];
    
    if (selectImg) {
        
         [firstItem changeSelectImage:selectImg];
    }
   
   
}

- (NSString *)selectTitle{
    
     QJCustomTabBarItem *newItem = [self.itemArray safeObjectAtIndex:currentSelectIndex];
    
    return [newItem selectTitle];
}

/**
 *  获取tabbar当前已经选中的index
 *
 *  @return index
 */
- (int)getCurrentSelectIndex {
    return (int)currentSelectIndex;
}

#pragma mark baackground

- (void)setBackgroundWithColor:(UIColor *)color img:(NSString *)imgUrl{
    
    if (IsStrEmpty(imgUrl)) {
        self.backImageView.hidden = YES;
        //不可以设置成透明色，tabbar切换显示会有问题
        if (color && color!=[UIColor clearColor]) {

            self.backgroundColor = color;
            self.lineImageView.backgroundColor = color;
        } else {
            
            self.lineImageView.backgroundColor = [UIColor colorWithHexString:@"0xf2f2f2"];
            [self setBackgroundGlassEffect];
        }
    }
    else{
        self.backImageView.hidden = NO;
        self.backImageView.imageURL = [NSURL URLWithString:imgUrl];
     
        self.backgroundColor = [UIColor whiteColor];
        self.lineImageView.backgroundColor = [UIColor clearColor];
    }

}
- (void)changeTabbarStyleToLucency:(NSNumber *)lucency {

    if (lucency.boolValue) {

    
        if (![self.lineImageView.backgroundColor isEqual:[UIColor clearColor]]) {

            self.originalBgColor = self.backgroundColor;

            self.originalLineColor = self.lineImageView.backgroundColor;
        }

        self.backgroundColor = [UIColor clearColor];

        self.lineImageView.backgroundColor = [UIColor clearColor];

        self.backImageView.hidden = YES;
        
        return;

    }

    if (!self.originalBgColor || !self.originalLineColor) {

    
        return;

    }

    self.backgroundColor = self.originalBgColor;

    self.lineImageView.backgroundColor = self.originalLineColor;

}
 
- (void)setBackgroundGlassEffect {
    self.backImageView.hidden = YES;
    self.backgroundColor = [UIColor whiteColor];
    
}

-(QJCustomTabBarItem*)getItemWithIndex:(NSInteger)index{
    
    QJCustomTabBarItem *item = [self.itemArray safeObjectAtIndex:index];
    
    return item;
}
@end
