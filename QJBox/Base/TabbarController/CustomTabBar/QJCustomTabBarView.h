//
//  QJCustomTabBarView.h
//  QJBox
//
//  Created by wxy on 2022/6/1.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class QJCustomTabBarItem;
 


@interface QJCustomTabBarView : UIView {
    NSInteger currentSelectIndex;
}

@property (nonatomic, weak) UITabBar *tabbar;

- (void)setNewItemArray:(NSArray *)itemArrar;

- (void)resizeAllSubView;

- (void)setSelectedIndex:(NSInteger)index;

- (int)getCurrentSelectIndex;

- (void)setBackgroundWithColor:(UIColor *)color img:(NSString *)imgUrl;

-(void)changeFirstTabBarSelectView:(UIImage *)selectImg title:(NSString *)title;

-(QJCustomTabBarItem *)getItemWithIndex:(NSInteger)index;


-(NSString *)selectTitle;

@end

NS_ASSUME_NONNULL_END
