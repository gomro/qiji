//
//  QJCustomTabBarItem.m
//  QJBox
//
//  Created by wxy on 2022/6/1.
//

#import "QJCustomTabBarItem.h"

@interface QJCustomTabBarItem ()

@property (nonatomic, strong) UILabel *titleLabel;//标题

@property (nonatomic, strong) NSString *normalTitle;//正常标题

@property (nonatomic, strong) NSString *selectTitle;//选中标题

@property (nonatomic, strong) NSString *myNormalImageName;//正常图片名称

@property (nonatomic, strong) NSString *mySelectedImageName;//选中图片名称

@property (nonatomic, strong) UIImage *myNormalImage;//正常图片

@property (nonatomic, strong) UIImage *mySelectedImage;//选中的图片

@property (nonatomic, strong) NSString *imageType;//图片类型

@property (nonatomic, strong) UIColor *myNormalTitleColor;//正常标题颜色

@property (nonatomic, strong) UIColor *mySelectedTitleColor;//选中标题颜色

@property (nonatomic,strong) UIImageView *imageView;//图片视图

@property (nonatomic, assign) BOOL isSelect;//是否选中

@end


@implementation QJCustomTabBarItem

- (id)initWithTitle:(NSString *)title normalImageName:(NSString *)normalImageName selectedImageName:(NSString *)selectedImageName imageType:(NSString *)type{
    self = [super init];
    if (self) {
        self = [self initWithTitle:title selectTitle:@"" normalImage:[UIImage imageNamed:normalImageName] selectedImage:[UIImage imageNamed:selectedImageName] imageType:type normalTitleColor:@"#000000CC" selectedTitleColor:@"#344382"];
    }
    
    return self;
}

- (id)initWithTitle:(NSString *)title
        selectTitle:(NSString *)sTitle
        normalImage:(UIImage *)normalImage
      selectedImage:(UIImage *)selectedImage
          imageType:(NSString *)type
   normalTitleColor:(NSString *)normalTitleColor
 selectedTitleColor:(NSString *)selectedTitleColor {
    
    self = [super init];
    if (self) {
        self.exclusiveTouch = YES;
        self.normalTitle = title;
        self.selectTitle = title;
        
        _imageView = [[UIImageView alloc] init];
        _imageView.backgroundColor = [UIColor clearColor];
        _imageView.image = normalImage;
        [self addSubview:_imageView];
        if ([type isEqualToString:@"1"]) {
            [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.equalTo(self);
                make.width.height.equalTo(@40);
                make.top.equalTo(self).offset(-10);
            }];
        } else {
            [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.equalTo(self);
                make.width.height.equalTo(@24);
                make.top.equalTo(self).offset(5);
            }];
        }
        
        self.myNormalImage = normalImage;
        self.mySelectedImage = selectedImage;
        
        self.myNormalTitleColor = [UIColor colorWithHexString:normalTitleColor];
        if (self.myNormalTitleColor == [UIColor clearColor]) {
            self.myNormalTitleColor = [UIColor colorWithHexString:@"#5c5952"];
        }
        
        self.mySelectedTitleColor = [UIColor colorWithHexString:selectedTitleColor];
        if (self.mySelectedTitleColor == [UIColor clearColor]) {
            self.mySelectedTitleColor = [UIColor colorWithHexString:@"#ffc30d"];
        }
        
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.titleLabel.backgroundColor = [UIColor clearColor];
        self.titleLabel.textColor = self.myNormalTitleColor;
        self.titleLabel.font = FontMedium(11);
        self.titleLabel.text = title ? title : @"";
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        self.titleLabel.isAccessibilityElement = NO;
        [self addSubview:self.titleLabel];
        
        if (iPhoneX) {
            [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//                make.top.equalTo(self.imageView.mas_bottom).offset(6);
                make.bottom.equalTo(self);//.offset(-6);
                make.width.equalTo(@50);
                make.height.equalTo(@13);
                make.centerX.equalTo(self);
            }];
        } else {
            [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                make.bottom.equalTo(self).offset(-6);
                make.width.equalTo(@50);
                make.height.equalTo(@13);
                make.centerX.equalTo(self);
            }];
        }
        
        self.clipsToBounds = NO;
        
        self.imageType = type;
    }
    
    return self;
}


- (void)layoutSubviews {
    
    [super layoutSubviews];
  
    
}

//选中图标
- (void)setSelected
{
    self.isSelect = YES;
    self.titleLabel.text = self.selectTitle;
    self.titleLabel.textColor = self.mySelectedTitleColor;
    if (self.mySelectedImage) {
        _imageView.image = self.mySelectedImage;
    } else {
        _imageView.image = [UIImage imageNamed:self.mySelectedImageName];
    }
}

//取消选中
- (void)cancelSelected {
    self.isSelect = NO;
    self.titleLabel.text = self.normalTitle;
    self.titleLabel.textColor = self.myNormalTitleColor;
    if (self.myNormalImage) {
        _imageView.image = self.myNormalImage;
    } else {
        _imageView.image = [UIImage imageNamed:self.myNormalImageName];
    }
}

- (NSString *)selectTitle {
    return self.titleLabel.text;
}

- (void)changeSelectTitleColor:(UIColor *)color {
    self.mySelectedTitleColor = color;
    self.titleLabel.textColor = color;
}

-(void)changeSelectImage:(UIImage *)selectImage {
    
    self.mySelectedImage = selectImage;
    
    if (self.isSelect) {
        
        _imageView.image = self.mySelectedImage;
    }
}

-(void)changeSelectTitle:(NSString *)sTitle {
    
    self.selectTitle = sTitle;
    
    if (self.isSelect) {
        
        self.titleLabel.text = self.selectTitle;
    }
    
}

 

@end
