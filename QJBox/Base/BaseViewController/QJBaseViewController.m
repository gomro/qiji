//
//  QJBaseViewController.m
//  QJBox
//
//  Created by wxy on 2022/5/31.
//

#import "QJBaseViewController.h"

@interface QJBaseViewController (){
@private
    BOOL _isAnimatedWhenPopOrDismiss; // animated flag when pop or dismiss,default YES
    BOOL _isNavBarHidden; // default NO,是否隐藏导航栏
    BOOL _isNavBackItemHidden; // default NO,是否隐藏导航栏 返回按钮
    BOOL _isNavRightItemHidden; // default YES,是否隐藏导航栏 右侧更多按钮
    UIStatusBarStyle _statusBarStyle;
}

@property (nonatomic,strong) QJNavigationBarView *kNavBar;

@property (nonatomic,strong) NSString        *titleString;

@end

@implementation QJBaseViewController


- (void)dealloc {
    /*
     * 如果你的ViewController当pop或dismiss后 未走或未及时走dealloc函数，请引起重视!!!
     */
#if DEBUG
    NSLog(@"dealloc: %@",self);
#endif
}

#ifdef __IPHONE_11_0
- (void)viewSafeAreaInsetsDidChange {
    [super viewSafeAreaInsetsDidChange];
    
    self.navBar.height = self.view.safeAreaInsets.top + 44;
    self.navBar.barItem.top = self.view.safeAreaInsets.top;
    self.navBar.bottomLine.top = self.navBar.bottom-0.5f;
}
#endif

- (id)init {
    if (self = [super init]) {
        self.hidesBottomBarWhenPushed = YES;
        _isAnimatedWhenPopOrDismiss   = YES;
        _isNavBarHidden               = NO;
        _isNavBackItemHidden          = NO;
        _isNavRightItemHidden         = YES;
        if (@available(iOS 13.0, *)) {
            _statusBarStyle               = UIStatusBarStyleDarkContent;
        } else {
            _statusBarStyle               = UIStatusBarStyleDefault;
        }
        // 强制隐藏系统导航栏
        self.navigationController.navigationBarHidden = YES;
        self.automaticallyAdjustsScrollViewInsets = NO;
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    return self;
}

- (QJNavigationBarView *)kNavBar {
    if (nil == _kNavBar) {
        _kNavBar = [QJNavigationBarView navigationBar];
        [_kNavBar.backButton addTarget:self
                                action:@selector(onBack:) forControlEvents:UIControlEventTouchUpInside];
        [_kNavBar.rightButton addTarget:self
                                 action:@selector(onNavRightItemClicked:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _kNavBar;
}

// 替换导航栏
- (void)replaceNavBar:(QJNavigationBarView *)navBar
{
    self.kNavBar = navBar;
    [self.kNavBar.backButton addTarget:self
                            action:@selector(onBack:) forControlEvents:UIControlEventTouchUpInside];
    [self.kNavBar.rightButton addTarget:self
                             action:@selector(onNavRightItemClicked:) forControlEvents:UIControlEventTouchUpInside];
}

- (UIView *)navBarItemView
{
    return self.kNavBar.barItem;
}

- (QJNavigationBarView *)navBar {
    return self.kNavBar;
}

- (NSString *)title {
    return _titleString;
}

- (void)setTitle:(NSString *)title {
    
    _titleString = title;
    _kNavBar.titleLabel.text = _titleString;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    UIColor *color = [UIColor colorWithRed:244/255.0f green:244/255.0f blue:244/255.0f alpha:1.0f];
    self.view.backgroundColor = color;
    
    self.navigationController.navigationBarHidden = YES;
    [self.view addSubview:self.kNavBar];
    
    [self setNavBarHidden:_isNavBarHidden];
    [self setNavBarBackItemHidden:_isNavBackItemHidden];
    [self setNavBarRightItemHidden:_isNavRightItemHidden];
    
    self.kNavBar.titleLabel.text = _titleString;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    // 系统侧滑使能
    self.navigationController.interactivePopGestureRecognizer.enabled = !_closePopGestureRecognizer;
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    
    // 自定导航栏
    if (!_isNavBarHidden && nil != _kNavBar) {
        [_kNavBar.superview bringSubviewToFront:_kNavBar];
    }
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onBack:(id)sender {
    
    // The view controller that was presented by this view controller or its nearest ancestor.
    NSArray *vcs = self.navigationController.viewControllers;
    if (vcs.count > 1 && [vcs objectAtIndex:vcs.count-1] == self) {
        // View is disappearing because a new view controller was pushed onto the stack
        [self.navigationController popViewControllerAnimated:_isAnimatedWhenPopOrDismiss];
    }else {
        [self dismissViewControllerAnimated:_isAnimatedWhenPopOrDismiss completion:nil];
    }
}

// 导航栏右侧更多按钮
- (void)onNavRightItemClicked:(id)sender {
}

// 设置animate flg when pop or dismiss
- (void)setNavBackAnimated:(BOOL)flag {
    _isAnimatedWhenPopOrDismiss = flag;
}

// 设置导航栏返回按钮隐藏
- (void)setNavBarBackItemHidden:(BOOL)bHidden {
    _isNavBackItemHidden = bHidden;
    _kNavBar.backButton.hidden = _isNavBackItemHidden;
}

// 设置导航栏右侧按钮隐藏(更多)
- (void)setNavBarRightItemHidden:(BOOL)bHidden {
    _isNavRightItemHidden = bHidden;
    _kNavBar.rightButton.hidden = _isNavRightItemHidden;
    
}

 
// 设置导航栏隐藏
- (void)setNavBarHidden:(BOOL)bHidden {
    _isNavBarHidden = bHidden;
    
    // 自定义导航栏
    _kNavBar.hidden = _isNavBarHidden;
}

/*
 * 设置返回按钮图片
 * @para normalImage 正常状态image
 * @para hlImage     高亮状态image
 *
 */
- (void)setNavBackImage:(UIImage *)normalImage
            highlighted:(UIImage *)hlImage {
    [_kNavBar.backButton setImage:normalImage
                         forState:UIControlStateNormal];
    [_kNavBar.backButton setImage:hlImage
                         forState:UIControlStateHighlighted];
}

 

 

- (void)addChildViewController:(UIViewController *)childController {
    [super addChildViewController:childController];
    
    if (nil != childController
        && [childController isKindOfClass:[QJBaseViewController class]]) {
        QJBaseViewController *vc = (QJBaseViewController *)childController;
        [vc setNavBarHidden:YES];
    }
}


//pop或者右滑返回后做的事
- (void)popOrSlideRightToForeVC
{
    
}
//捕捉系统右滑事件
-(void)didMoveToParentViewController:(UIViewController *)parent
{
    if (nil == parent) {//右滑或pop返回
        [self popOrSlideRightToForeVC];
    }
    [super didMoveToParentViewController:parent];
    
}

#pragma mark --- 旋转控制
- (BOOL)shouldAutorotate {
    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

@end
