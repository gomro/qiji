//
//  QJBaseRequest.m
//  QJBox
//
//  Created by wxy on 2022/5/31.
//

#import "QJBaseRequest.h"

@implementation QJBaseRequest

- (NSString *)baseUrl {
    return QJBaseUrl;
}

- (id)requestArgument {
    return self.dic;
}

- (void)start {
    [super start];
    DLog(@"request.request = %@",self);
}

- (YTKRequestMethod)requestMethod {
    return self.requestType;
}

- (NSString *)requestUrl {
    return self.urlStr;
}

- (YTKRequestSerializerType)requestSerializerType {
    return self.serializerType;
}

- (void)requestCompletePreprocessor {
    
    DLog(@"+++++++++++request.responseJSONObject = %@",self.responseJSONObject);
    DLog(@"+++++++++++request.responseHeaders = %@",self.responseHeaders);
    DLog(@"+++++++++++request.responseStatusCode = %ld",self.responseStatusCode);
    
    if ([self.urlStr isEqualToString:@"/user/info/mine"]) {//不做处理
        return;
    }
    
    if ([EncodeStringFromDic(self.responseJSONObject, @"code") isEqualToString:@"401"]) {
        LSUserDefaultsSET(@"", kQJUserId);
        LSUserDefaultsSET(@"",kQJToken);//退出登录
        LSUserDefaultsSET(@0, kQJIsRealName);
        LSUserDefaultsSET(@"", kQJUserId);
        [QJUserManager shareManager].isLogin = NO;
        [QJUserManager shareManager].isVerifyState = NO;
        [QJUserManager shareManager].userID = @"";
       
        dispatch_async(dispatch_get_main_queue(), ^{
            UIViewController *vc = [QJAppTool getCurrentViewController];
            if (vc) {
                [vc checkLogin:^(BOOL isLogin) {
                  
                }];
                [QJUserManager shareManager].isNetLogout = YES;
            }
            
        });
        
        
        
        
    }
    
}

- (void)requestFailedPreprocessor {
    
    DLog(@"+++++++++++request.responseJSONObject = %@",self.responseJSONObject);
    DLog(@"+++++++++++request.responseHeaders = %@",self.responseHeaders);
    DLog(@"+++++++++++request.responseStatusCode = %ld",self.responseStatusCode);
}


- (NSDictionary<NSString *,NSString *> *)requestHeaderFieldValueDictionary {
    NSMutableDictionary *muDic = [NSMutableDictionary dictionary];
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:kQJToken];
    if (!IsStrEmpty(token)) {
        [muDic setValue:token forKey:@"token"];
    }
    
    [muDic setValue:token forKey:@"sign"];//签名
    
    NSDate *datenow = [NSDate date];
    NSString *timeSp = [NSString stringWithFormat:@"%ld", (long)[datenow timeIntervalSince1970]];
    [muDic setValue:timeSp forKey:@"time"];//时间戳
    
    [muDic setValue:@"" forKey:@"appid"];//appid后端定义
    
    [muDic setValue:@"iOS" forKey:@"os"];//系统,Android/iOS
    
    
    NSString *UUID = [QJDeviceConstant getUUIDInKeychain];
    DLog(@"uuid === %@",UUID)
    [muDic setValue:UUID forKey:@"uuid"];//手机uuid
    
    [muDic setValue:[QJDeviceConstant appVersion] forKey:@"appVersion"];//app版本名
    
    [muDic setValue:[QJDeviceConstant appBuildID] forKey:@"appVersionCode"];//app版本号
    
    [muDic setValue:[QJDeviceConstant appPackageName] forKey:@"packageName"];//包名(iOS 传 Bundle Identifier)
    
    [muDic setValue:[QJDeviceConstant getCurrentDeviceModel] forKey:@"model"];//手机型号
    
    [muDic setValue:@"Apple" forKey:@"devicebrand"];//手机厂商
    
    NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
    [muDic setValue:currSysVer forKey:@"systemversion"];//系统版本号
    
    //增加滑块校验的参数validate
    NSString *validate = [QJUserManager shareManager].validate?:@"";
    [muDic setValue:validate forKey:@"validate"];
    
    return muDic.copy;
}
@end
