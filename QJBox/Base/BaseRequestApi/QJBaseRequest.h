//
//  QJBaseRequest.h
//  QJBox
//
//  Created by wxy on 2022/5/31.
//网络请求基类

#import <YTKNetwork/YTKNetwork.h>

NS_ASSUME_NONNULL_BEGIN

/// 网络请求基类
@interface QJBaseRequest : YTKRequest

/**请求参数*/
@property (nonatomic, strong) NSDictionary *dic;

/**请求地址*/
@property (nonatomic, copy) NSString *urlStr;

/**请求方式*/
@property (nonatomic, assign) YTKRequestMethod requestType;

/**表单格式*/
@property (nonatomic, assign) YTKRequestSerializerType serializerType;

@end

NS_ASSUME_NONNULL_END
