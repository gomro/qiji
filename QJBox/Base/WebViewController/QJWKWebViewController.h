//
//  QJWKWebViewController.h
//  QJBox
//
//  Created by wxy on 2022/6/2.
//

#import "QJBaseViewController.h"
#import <WebKit/WebKit.h>
NS_ASSUME_NONNULL_BEGIN

/// webview
@interface QJWKWebViewController : QJBaseViewController

@property (nonatomic,strong) WKWebView * webView;

@property (nonatomic,strong) UIProgressView * progressView;

@property (nonatomic) UIColor *progressViewColor;

@property (nonatomic,weak) WKWebViewConfiguration * webConfiguration;

@property (nonatomic, copy) NSString * url;

@property (nonatomic, assign) BOOL isHiddenNavgationBar;

@property (nonatomic, copy) NSString * htmlString;

@property (nonatomic, copy) NSString * navTitle;


@end

NS_ASSUME_NONNULL_END
