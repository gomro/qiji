//
//  QJWKWebViewController.m
//  QJBox
//
//  Created by wxy on 2022/6/2.
//

#import "QJWKWebViewController.h"

@interface QJWKWebViewController ()<WKNavigationDelegate,WKUIDelegate>

@end

@implementation QJWKWebViewController


- (void)dealloc {
    self.webView.navigationDelegate = nil;
    [self.webView removeObserver:self forKeyPath:@"estimatedProgress"];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (self.isHiddenNavgationBar) {
        //隐藏系统导航栏
        [self.navigationController setNavigationBarHidden:YES animated:YES];
    }
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    [self initWKWebView];
    if (@available(iOS 11.0, *)){
        self.webView.scrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }
    else{
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    self.title = self.navTitle == nil ? self.webView.title : self.navTitle;
}

#pragma mark --进度条
-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    
    self.progressView.alpha = 1;
    if(self.webView.estimatedProgress > 0){
        [self.progressView setProgress:self.webView.estimatedProgress animated:YES];
    }
     
    if (self.webView.estimatedProgress >= 1) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            self.progressView.alpha = 0;
            [self.progressView setProgress:0];
            
        });
    }
}
 
#pragma mark 初始化webview
-(void)initWKWebView
{
    
    NSString *jScript = @"var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport'); meta.setAttribute('content', 'width=device-width'); document.getElementsByTagName('head')[0].appendChild(meta); var imgs = document.getElementsByTagName('img');for (var i in imgs){imgs[i].style.maxWidth='100%';imgs[i].style.height='auto';}";
    WKUserScript *wkUScript = [[WKUserScript alloc] initWithSource:jScript injectionTime:WKUserScriptInjectionTimeAtDocumentEnd forMainFrameOnly:YES];
    WKUserContentController *wkUController = [[WKUserContentController alloc] init];
    [wkUController addUserScript:wkUScript];
    WKWebViewConfiguration * configuration = [[WKWebViewConfiguration alloc]init];
    configuration.userContentController = wkUController;
    configuration.preferences.javaScriptEnabled = YES;//打开js交互
    _webConfiguration = configuration;
    self.webView = [[WKWebView alloc]initWithFrame:CGRectZero configuration:configuration];
    _webView.navigationDelegate = self;
    _webView.UIDelegate = self;
    _webView.backgroundColor = [UIColor clearColor];
    _webView.allowsBackForwardNavigationGestures =YES;//打开网页间的 滑动返回
    _webView.scrollView.decelerationRate = UIScrollViewDecelerationRateNormal;
    //监控进度
    [_webView addObserver:self forKeyPath:@"estimatedProgress" options:NSKeyValueObservingOptionNew context:nil];
    [self.view addSubview:_webView];
    [self.webView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.navBar.mas_bottom);
        make.left.equalTo(self.view).mas_offset(16);
        make.right.equalTo(self.view).mas_offset(-16);
        make.bottom.equalTo(self.view);
    }];
    //进度条
    _progressView = [[UIProgressView alloc]initWithProgressViewStyle:UIProgressViewStyleDefault];
    _progressView.tintColor = _progressViewColor;
    _progressView.trackTintColor = [UIColor clearColor];
    _progressView.frame = CGRectMake(0, 0, self.view.bounds.size.width, 3.0);
    [_webView addSubview:_progressView];
    
    if (_url != nil) {
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:_url]];
        [_webView loadRequest:request];
        
    } else {
        [_webView loadHTMLString:[self setWebViewHtmlString:_htmlString] baseURL:nil];

    }
    
}

// 设置webview富文本
- (NSString *)setWebViewHtmlString:(NSString *)string {
    NSString *html = string;
    if (html == nil) {
        html = @"加载失败！";
    }
    NSString *headerString = @"<header><meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no'></header>";
    NSString *adaptString = [NSString stringWithFormat:@"<head><style>img{max-width:100%% !important;height:auto !important}</style></head><body style='margin:0;padding:0'>%@</body>", [NSString stringWithFormat:@"%@<br><br>",html]];
    NSString *path = [headerString stringByAppendingString:adaptString];
    
    return path;
    
}

@end
