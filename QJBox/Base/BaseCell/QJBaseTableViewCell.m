//
//  QJBaseTableViewCell.m
//  QJBox
//
//  Created by wxy on 2022/6/2.
//

#import "QJBaseTableViewCell.h"

@implementation QJBaseTableViewCell


+ (NSString *)cellIdentifier {
    return NSStringFromClass([self class]);
}

//计算行高
+ (CGFloat)cellHeight:(QJBaseCellModel *)cellDTO {
    return 0;
}

//配置数据
-(void)setCellModel:(QJBaseCellModel *)cellModel {
    _cellModel = cellModel;
}

- (instancetype)init {
    if (self = [super init]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

@end



@implementation QJBaseCellModel

- (Class)cellClass {
    return [QJBaseTableViewCell class];
}

- (CGFloat)height {
    if (self.custHeight > 0) {//有固定行高拿固定行高
        return self.custHeight;
    }
    if (_height <= 0) {
        [self calculateCellHeight];
    }
    return _height;
}

- (void)calculateCellHeight {
    Class class = NSClassFromString([self identifier]);
    if (class && [class respondsToSelector:@selector(cellHeight:)]) {
        self.height = [class cellHeight:self];
    }
}


@end
