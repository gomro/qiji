//
//  QJBaseTableViewCell.h
//  QJBox
//
//  Created by wxy on 2022/6/2.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN


@class QJBaseCellModel;
@interface QJBaseTableViewCell : UITableViewCell

//Cell的数据
@property (nonatomic, strong, nullable) QJBaseCellModel *cellModel;

/// Cell的Identifier
+ (NSString *_Nonnull)cellIdentifier;


/// 行高计算
/// @param cellModel 数据
+ (CGFloat)cellHeight:(nullable QJBaseCellModel *)cellModel;


@end



//初始化model时 identifier、relativeData这些需要给到
@interface QJBaseCellModel : NSObject

@property (nonatomic, copy) void(^didSelectedRow)(QJBaseCellModel *cellModel);

//cell的identifier
@property (nonatomic, copy, nullable) NSString *identifier;

//cell的高度
@property (nonatomic, assign) CGFloat height;

//需要用于逻辑处理等相关的数据
@property (nonatomic, strong, nullable) id relativeData;

@property (nonatomic, assign) CGFloat custHeight;//自定义固定高度

- (Class)cellClass;

//计算高度
- (void)calculateCellHeight;
@end

NS_ASSUME_NONNULL_END
