//
//  QJNavigationBarView.m
//  QJBox
//
//  Created by wxy on 2022/5/31.
//

#import "QJNavigationBarView.h"

@implementation QJNavigationBarView

+ (QJNavigationBarView *)navigationBar {
    QJNavigationBarView *bar = [[QJNavigationBarView alloc] init];
    
    bar.frame = CGRectMake(.0f,.0f, kScreenWidth, 64.0f);
    return bar;
}


- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame])
    {
         
        self.backgroundColor = [UIColor whiteColor];
        
        [self.barItem addSubview:self.titleLabel];
        [self.barItem addSubview:self.titleImageView];
        
        [self.barItem addSubview:self.backButton];
        [self.barItem addSubview:self.rightButton];
        
        [self addSubview:self.barItem];
        [self addSubview:self.bottomLine];
        // wb 8.9.5 9.0视觉规范没有底部这条线
        self.bottomLine.hidden = YES;
        
        _leftBarItems = [[NSArray alloc] init];
        _rightBarItems = [[NSArray alloc] init];
    }
    return self;
}


- (void)layoutSubviews
{
    [super layoutSubviews];
    if (IS_IPAD)
    {
        _barItem.width = kScreenWidth;
        _bottomLine.width = kScreenWidth;
        _titleLabel.center = CGPointMake(_barItem.width/2, 22);
        _titleImageView.center = CGPointMake(self.barItem.width/2, 22);
        _rightButton.frame = CGRectMake(kScreenWidth-9.f-30.f, 7.0f, 30.f, 30.f);
        


        if (!IsArrEmpty(_rightBarItems))
        {
            NSInteger index = 0;
            for (UIButton *button in _rightBarItems)
            {
                if (index > 2)
                {
                    break;
                }
                button.frame = CGRectMake((kScreenWidth - 9 - 30 - (30 + 12) * index), 7, 30, 30);
                index += 1;
            }
        }
    }
}


#pragma mark -------- getter

/*
 返回按钮
 */
- (CustomNavBackButton *)backButton
{
    if (nil == _backButton)
    {
        _backButton = [[CustomNavBackButton alloc] initWithFrame:CGRectMake(9.0f, 7.0f, 30.f, 30.f)];
        [_backButton setupTappableAreaOffset:UIOffsetMake(7, 0)];
        //TODO
        UIImage *image = [UIImage imageNamed:@"backimage_black"];
        
        [_backButton setImage:image forState:UIControlStateNormal];
        [_backButton setImage:image forState:UIControlStateHighlighted];
        
    }
    return _backButton;
}

/*
 右侧按钮
 */
- (UIButton *)rightButton
{
    if (nil == _rightButton)
    {
        _rightButton = [[UIButton alloc] initWithFrame:CGRectMake(kScreenWidth-9.f-30.f, 7.0f, 30.f, 30.f)];
        //TODO
        UIImage *image = [UIImage imageNamed:@""];
        [_rightButton setImage:image forState:UIControlStateNormal];
        [_rightButton setImage:image forState:UIControlStateHighlighted];
        
    }
    return _rightButton;
}

/*
 标题
 */
- (CustomTitleLabel *)titleLabel
{
    if (nil == _titleLabel)
    {
        _titleLabel = [[CustomTitleLabel alloc] init];
        _titleLabel.frame = CGRectMake(0, 0, 150.f, 44.0f);
        _titleLabel.center = CGPointMake(self.barItem.width/2, 22);
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.textColor = UIColorFromRGB(0x000000);
        _titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        _titleLabel.backgroundColor = [UIColor clearColor];
        _titleLabel.font = FontSemibold(16);
    }
    return _titleLabel;
}

/*
 标题图片
 */
- (UIImageView *)titleImageView
{
    if (!_titleImageView)
    {
        _titleImageView = [[UIImageView alloc] init];
        _titleImageView.frame = CGRectMake(0, 0, 84, 20);
        _titleImageView.center = CGPointMake(self.barItem.width/2, 22);
        _titleImageView.hidden = YES;
    }
    return _titleImageView;
}

- (UIView *)barItem
{
    if (nil == _barItem)
    {
        _barItem = [[UIView alloc] initWithFrame:CGRectMake(.0f, 20.0f, kScreenWidth, 44.0f)];
        _barItem.backgroundColor = [UIColor clearColor];
    }
    return _barItem;
}

- (UIView *)bottomLine
{
    if (nil == _bottomLine)
    {
        _bottomLine = [[UIView alloc] initWithFrame:CGRectMake(.0f,self.height-0.5,kScreenWidth,0.5)];
        _bottomLine.backgroundColor = [UIColor colorWithRed:230/255.0 green:230/255.0 blue:230/255.0 alpha:1];
    }
    return _bottomLine;
}

/*
 设置左侧多个按钮
 */
- (void)setLeftBarItems:(NSArray<UIButton *> *)leftBarItems
{
    if (nil != leftBarItems) {
        if (_leftBarItems.count > 0)
        {
            for (UIButton *barItem in _leftBarItems)
            {
                [barItem removeFromSuperview];
            }
        }
        _leftBarItems = leftBarItems;
        self.backButton.hidden = YES;
        NSInteger index = 0;
        for (UIButton *button in leftBarItems) {
            if (index > 1) {
                break;
            }
            button.frame = CGRectMake(9 + (30 + 12) * index, 7, 30, 30);
            [self.barItem addSubview:button];
            index += 1;
        }
    }
}

/*
 设置右侧多个按钮
 */
- (void)setRightBarItems:(NSArray<UIButton *> *)rightBarItems
{
    if (nil != rightBarItems) {
        if (_rightBarItems.count > 0)
        {
            for (UIButton *barItem in _rightBarItems)
            {
                [barItem removeFromSuperview];
            }
        }
        _rightBarItems = rightBarItems;
        self.rightButton.hidden = YES;
        NSInteger index = 0;
        for (UIButton *button in rightBarItems) {
            if (index > 2) {
                break;
            }
            button.frame = CGRectMake((kScreenWidth - 9 - 30*kWScale - (30*kWScale + 12) * index), 7, 30*kWScale, 30*kWScale);
            [self.barItem addSubview:button];
            index += 1;
        }
    }
}



@end


@implementation CustomTitleLabel

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame])
    {
        
    }
    
    return self;
}

- (void)setText:(NSString *)text
{
//    NSAssert(text.length <= 10, @"标题不能超过10个字");
    if (text && ![text isEqualToString:@""])
    {
        self.frame = CGRectMake(0, 0, 250.f, 44.0f);
        NSString  *subTitle = text;
        [super setText:subTitle];
        [self sizeToFit];
        self.center = CGPointMake(self.superview.width/2, 22);
    } else {
        [super setText:@""];
        self.frame = CGRectMake(0, 0, 1.f, 22.0f);
        self.center = CGPointMake(self.superview.width/2, 22);
    }
}

@end

@interface CustomNavBackButton ()

@property (nonatomic, assign) UIOffset tappableAreaOffset;

@end

@implementation CustomNavBackButton

// 设置点击区域
- (void)setupTappableAreaOffset:(UIOffset)tappableAreaOffset_
{
    _tappableAreaOffset = tappableAreaOffset_;
}

- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event{
    
    return CGRectContainsPoint(CGRectInset(self.bounds,  -_tappableAreaOffset.horizontal, -_tappableAreaOffset.vertical), point);
    
}

@end
