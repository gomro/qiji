//
//  QJNavigationBarView.h
//  QJBox
//
//  Created by wxy on 2022/5/31.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class CustomTitleLabel, CustomNavBackButton;

@interface QJNavigationBarView : UIView
/*
 返回按钮
 */
@property (nonatomic, strong) CustomNavBackButton *backButton;

/*
 右侧按钮
 */
@property (nonatomic, strong) UIButton *rightButton;

/*
 文字标题
 */
@property (nonatomic, strong) CustomTitleLabel *titleLabel;

@property (nonatomic, strong) UIView   *barItem;

/*
 图片标题
 */
@property (nonatomic, strong) UIImageView *titleImageView;

@property (nonatomic, strong) UIView   *bottomLine;

/*
 导航栏左侧按钮(多个)
 */
@property (nonatomic, strong) NSArray<UIButton *> *leftBarItems;

/*
 导航栏右侧按钮(多个)
 */
@property (nonatomic, strong) NSArray<UIButton *> *rightBarItems;


// 默认导航栏
+ (QJNavigationBarView *)navigationBar;


@end

@interface CustomTitleLabel : UILabel

@end

@interface CustomNavBackButton : UIButton

// 设置点击区域
- (void)setupTappableAreaOffset:(UIOffset)tappableAreaOffset_;

@end
NS_ASSUME_NONNULL_END
