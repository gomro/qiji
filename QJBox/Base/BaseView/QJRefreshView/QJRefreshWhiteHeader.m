//
//  QJRefreshWhiteHeader.m
//  QJBox
//
//  Created by rui on 2022/10/20.
//

#import "QJRefreshWhiteHeader.h"

@implementation QJRefreshWhiteHeader

- (void)prepare {
    [super prepare];
    
    // 设置控件的高度 300/5
    self.mj_h = Get375Height(156/2);
    self.laAnimation = [LOTAnimationView animationNamed:@"loadingwhite"];
    self.laAnimation.contentMode = UIViewContentModeScaleAspectFill;
    self.laAnimation.loopAnimation = YES;
    [self addSubview:self.laAnimation];
    
    self.statusLabel = [UILabel mj_label];
    self.statusLabel.font = kFont(10);
    self.statusLabel.textColor = UIColorFromRGB(0xFFFFFF);
    [self addSubview:self.statusLabel];
}

- (void)placeSubviews
{
    [super placeSubviews];
    self.laAnimation.frame = CGRectMake(0, 0, self.mj_w, Get375Height(156/2));
    self.statusLabel.frame = CGRectMake(0, Get375Height(156/2)-30, self.mj_w, 20);
    [self.laAnimation play];
}

#pragma mark 监听scrollView的contentOffset改变
- (void)scrollViewContentOffsetDidChange:(NSDictionary *)change
{
    [super scrollViewContentOffsetDidChange:change];
}

#pragma mark 监听scrollView的拖拽状态改变
- (void)scrollViewPanStateDidChange:(NSDictionary *)change
{
    [super scrollViewPanStateDidChange:change];
}

#pragma mark 监听控件的刷新状态
- (void)setState:(MJRefreshState)state
{
    MJRefreshCheckState;
    switch (state) {
        case MJRefreshStateIdle:
            self.statusLabel.text = @"下拉立即刷新";
            [self endAnimation];
            break;
        case MJRefreshStatePulling:
            self.statusLabel.text = @"释放立即刷新";
            [self endAnimation];
            break;
        case MJRefreshStateRefreshing:
            self.statusLabel.text = @"正在刷新中...";
            [self startAnimation];
            break;
        default:
            break;
    }
}
#pragma mark 监听拖拽比例（控件被拖出来的比例）
- (void)setPullingPercent:(CGFloat)pullingPercent
{
    [super setPullingPercent:pullingPercent];
}

- (void)endAnimation{
    [self.laAnimation pause];
}

- (void)startAnimation{
    [self.laAnimation play];
}
 
@end
