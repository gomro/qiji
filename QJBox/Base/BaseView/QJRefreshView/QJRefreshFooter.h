//
//  QJRefreshFooter.h
//  QJBox
//
//  Created by wxy on 2022/6/23.
//

#import <MJRefresh/MJRefresh.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJRefreshFooter : MJRefreshBackNormalFooter

@end

NS_ASSUME_NONNULL_END
