//
//  QJRefreshFooter.m
//  QJBox
//
//  Created by wxy on 2022/6/23.
//

#import "QJRefreshFooter.h"

@implementation QJRefreshFooter

- (void)prepare {
    [super prepare];
    [self setTitle:@"正在加载中…" forState:MJRefreshStateIdle];
    [self setTitle:@"正在加载中…" forState:MJRefreshStatePulling];
    [self setTitle:@"正在加载中…" forState:MJRefreshStateRefreshing];
    [self setTitle:@"已经到底啦~" forState:MJRefreshStateNoMoreData];
    self.arrowView.hidden = YES;
}

- (void)placeSubviews
{
    [super placeSubviews];
        //隐藏状态显示文字
    self.arrowView.hidden = YES;
    self.arrowView.width = 0;
    self.stateLabel.textColor = [UIColor colorWithHexString:@"0x919599"];
    self.stateLabel.font = kFont(14);
    self.stateLabel.hidden = YES;
    [self.loadingView startAnimating];
    self.labelLeftInset = 10;
 
    if (self.state == MJRefreshStateNoMoreData) {
        self.loadingView.hidden = YES;
        self.stateLabel.hidden = NO;
        [self.loadingView stopAnimating];
    }
}

#pragma mark 监听控件的刷新状态
//- (void)setState:(MJRefreshState)state
//{
//    MJRefreshCheckState;
//    switch (state) {
//        case MJRefreshStateIdle:
//            [self setTitle:@"正在加载中…" forState:MJRefreshStateIdle];
//            break;
//        case MJRefreshStatePulling:
//            [self setTitle:@"正在加载中…" forState:MJRefreshStatePulling];
//            break;
//        case MJRefreshStateRefreshing:
//            [self setTitle:@"正在加载中…" forState:MJRefreshStateRefreshing];
//            break;
//        case MJRefreshStateNoMoreData:
//            [self setTitle:@"已经到底了~" forState:MJRefreshStateNoMoreData];
//            break;
//        default:
//            break;
//    }
//}

@end
