//
//  QJRefreshHeader.h
//  QJBox
//
//  Created by wxy on 2022/6/23.
//

#import <MJRefresh/MJRefresh.h>
#import <Lottie/Lottie.h>

NS_ASSUME_NONNULL_BEGIN
//MJRefreshHeader MJRefreshStateHeader
@interface QJRefreshHeader : MJRefreshHeader

@property (nonatomic, strong) LOTAnimationView *laAnimation;
@property (nonatomic, strong) UILabel *statusLabel;

@end

NS_ASSUME_NONNULL_END
