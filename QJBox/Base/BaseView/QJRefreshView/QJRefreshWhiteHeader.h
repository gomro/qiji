//
//  QJRefreshWhiteHeader.h
//  QJBox
//
//  Created by rui on 2022/10/20.
//

#import <MJRefresh/MJRefresh.h>
#import <Lottie/Lottie.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJRefreshWhiteHeader : MJRefreshHeader

@property (nonatomic, strong) LOTAnimationView *laAnimation;
@property (nonatomic, strong) UILabel *statusLabel;

@end

NS_ASSUME_NONNULL_END
