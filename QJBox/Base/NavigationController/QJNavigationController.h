//
//  QJNavigationController.h
//  QJBox
//
//  Created by wxy on 2022/5/31.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJNavigationController : UINavigationController

- (id)initWithRootViewController:(UIViewController *)rootViewController;


@end

NS_ASSUME_NONNULL_END
