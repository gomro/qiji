//
//  QJNavigationController.m
//  QJBox
//
//  Created by wxy on 2022/5/31.
//

#import "QJNavigationController.h"

@interface QJNavigationController ()<UIGestureRecognizerDelegate,UINavigationControllerDelegate>

// navigation controller 最顶部的controller
@property (nonatomic,weak) UIViewController *activeVController;

@end

@implementation QJNavigationController


- (id)initWithRootViewController:(UIViewController *)rootViewController {
    if (self = [super initWithRootViewController:rootViewController]) {
        self.interactivePopGestureRecognizer.delegate = self;
        self.delegate = self;
    }
    return self;
}


- (void)navigationController:(UINavigationController *)navigationController
       didShowViewController:(UIViewController *)viewController
                    animated:(BOOL)animated {
    if (navigationController.viewControllers.count == 1) {
        self.activeVController = nil;
    }else {
        self.activeVController = viewController;
    }
}

-(BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    if (gestureRecognizer == self.interactivePopGestureRecognizer) {
        // wb:8.7.5 nav中只有一个vc，禁用测滑手势 解决我的易购模块偶现侧滑卡死问题
        if (self.viewControllers.count <= 1 || self.visibleViewController == [self.viewControllers firstObject]) {
            return NO;
        }
        return (self.activeVController == self.topViewController)?YES:NO;
    }
    return YES;
}

#pragma mark --- 旋转控制
- (BOOL)shouldAutorotate {
    return self.topViewController.shouldAutorotate;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return self.topViewController.supportedInterfaceOrientations;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return self.topViewController.preferredInterfaceOrientationForPresentation;
}

- (nullable UIViewController *)childViewControllerForStatusBarStyle {
    return self.topViewController;
}

- (nullable UIViewController *)childViewControllerForStatusBarHidden {
    return self.topViewController;
}

@end
