//
//  QJConsultDetailViewCell.h
//  QJBox
//
//  Created by Sun on 2022/7/11.
//

#import <UIKit/UIKit.h>
#import "QJCampsiteDetailModel.h"

NS_ASSUME_NONNULL_BEGIN
typedef NS_ENUM(NSInteger, QJConsultDetailViewCellStyle){
    QJConsultDetailViewCellStyleDefault,    // 仅文字
    QJConsultDetailViewCellStyleImage,      // 图片
    QJConsultDetailViewCellStyleVideo,      // 视频

};

@protocol QJConsultDetailViewCellDelegate <NSObject>

- (void)didVideoImageUrl;
@end
@interface QJConsultDetailViewCell : UITableViewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier cellStyle:(QJConsultDetailViewCellStyle)cellStyle;
- (void)configureCellWithData:(QJCampsiteDetailContentModel *)model;
@property (nonatomic, weak) id<QJConsultDetailViewCellDelegate> cellDelegate;

@end

NS_ASSUME_NONNULL_END
