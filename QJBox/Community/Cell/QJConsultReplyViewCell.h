//
//  QJConsultReplyViewCell.h
//  QJBox
//
//  Created by Sun on 2022/7/12.
//

#import <UIKit/UIKit.h>
#import "QJCommentReplyFrame.h"

NS_ASSUME_NONNULL_BEGIN

@protocol QJConsultReplyViewCellDelegate <NSObject>


- (void)touchConsultReplyComment:(NSString *)commentID name:(NSString *)name userId:(NSString *)userId toUser:(QJCommentUser *)toUser;

@end

@interface QJConsultReplyViewCell : UITableViewCell
+ (instancetype)cellWithTableView:(UITableView *)tableView;

/** 评论Frame */
@property (nonatomic , strong) QJCommentReplyFrame *commentFrame;
@property (nonatomic, weak) id<QJConsultReplyViewCellDelegate> cellDelegate;

@end

NS_ASSUME_NONNULL_END
