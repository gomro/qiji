//
//  QJImageButton.h
//  QJBox
//
//  Created by Sun on 2022/7/7.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, ImageButtonStyle){
    ImageButtonStyleTop,       // 图片在上，文字在下
    ImageButtonStyleLeft,      // 图片在左，文字在右
    ImageButtonStyleRight,     // 图片在右，文字在左
    ImageButtonStyleBottom,    // 图片在下，文字在上
};
@interface QJImageButton : UIButton
/**
 LPButton的样式(Top、Left、Right、Bottom)
 */
@property (nonatomic, assign) ImageButtonStyle style;

/**
 图片和文字的间距
 */
@property (nonatomic, assign) CGFloat space;

/**
 图片边距，上边距，左边距，右边距，下边距
 */
@property (nonatomic, assign) CGFloat delta;

/**图片尺寸*/
@property (nonatomic, assign) CGSize imageSize;

/**标题内容*/
@property (nonatomic, copy) NSString *titleString;
/**常态*/
@property (nonatomic, copy) NSString *iconString;

/**标题*/
@property (nonatomic, strong) UILabel *titleLb;

@end

