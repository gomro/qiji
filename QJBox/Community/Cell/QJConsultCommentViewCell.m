//
//  QJConsultCommentViewCell.m
//  QJBox
//
//  Created by Sun on 2022/7/11.
//

#import "QJConsultCommentViewCell.h"
#import "QJCampsiteUserView.h"
#import "QJCommentTopModel.h"
#import "QJCommentUser.h"
#import "QJConsultReplyViewCell.h"
#import "QJCommentReplyFrame.h"
#import "QJCommentBottomView.h"
#import "QJCampsiteRequest.h"

@interface QJConsultCommentViewCell ()< UITableViewDelegate, UITableViewDataSource, QJConsultReplyViewCellDelegate>
/**用户*/
@property (nonatomic, strong) QJCampsiteUserView *userView;
/**描述*/
@property (nonatomic, strong) YYLabel *titleLabel;
/** UITableView */
@property (nonatomic , strong) UITableView *tableView;
@end

@implementation QJConsultCommentViewCell


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor = [UIColor whiteColor];
        [self loadUI];
    }
    return self;
}

#pragma mark - Load UI
- (void)loadUI {
    [self.contentView addSubview:self.titleLabel];
    [self.contentView addSubview:self.userView];
    [self.contentView addSubview:self.tableView];
    
    [self makeConstraints];
}

- (void)makeConstraints {
    [self.userView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView.mas_left).offset(16);
        make.top.equalTo(self.contentView.mas_top).offset(10);
        make.width.mas_lessThanOrEqualTo(200*kWScale);
        make.height.mas_equalTo(40*kWScale);
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.userView.mas_bottom).offset(1);
        make.left.equalTo(self.contentView.mas_left).offset(59);
        make.right.equalTo(self.contentView.mas_right).offset(-21);
    }];
}

- (QJCampsiteUserView *)userView {
    if (!_userView) {
        _userView = [[QJCampsiteUserView alloc] init];
    }
    return _userView;
}

- (YYLabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [YYLabel new];
        _titleLabel.font = FontRegular(14);
        _titleLabel.numberOfLines = 0;
        _titleLabel.textColor = UIColorHex(16191C);
    }
    return _titleLabel;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.bounces = NO;
        _tableView.scrollEnabled = NO;
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.showsHorizontalScrollIndicator = NO;
        _tableView.backgroundColor = UIColorHex(F5F5F5);
    }
    return _tableView;
}


#pragma mark - Setter
- (void)setTopicFrame:(QJCommentTopFrame *)topicFrame {
    _topicFrame = topicFrame;
    
    QJCommentTopModel *topic = topicFrame.topic;
    [self.userView configureHeaderlCommentWithData:topic];
    
    
    NSMutableAttributedString *mutableAttributedString = [[NSMutableAttributedString alloc] init];
    [mutableAttributedString setAttributedString:topicFrame.topic.attributedText];
    
    //添加点击事件
 
    __weak typeof(self) weakSelf = self;
    self.titleLabel.textTapAction = ^(UIView * _Nonnull containerView, NSAttributedString * _Nonnull text, NSRange range, CGRect rect) {
         
        __typeof(&*weakSelf) strongSelf = weakSelf;
        if (strongSelf.cellDelegate && [strongSelf.cellDelegate respondsToSelector:@selector(touchReplyMainComment:name:userId:toUser:)]) {
            if (!topicFrame.topic.showAll) {
                QJCommentUser *user = [QJCommentUser new];
                user.avatarUrl = [topicFrame.topic.coverImage checkImageUrlString];
                user.commentStr = topicFrame.topic.content?:@"";
                [strongSelf.cellDelegate touchReplyMainComment:strongSelf.topicFrame.topic.commentId name:strongSelf.topicFrame.topic.userName userId:strongSelf.topicFrame.topic.userId toUser:user];
            }
            
        }
    };
    self.titleLabel.attributedText = mutableAttributedString;
    
    
    [self.titleLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(topicFrame.textFrame.size.height);
    }];    
    if (!topicFrame.topic.showAll) {
        [self addLableMore];
    } else {
        self.titleLabel.numberOfLines = 0;
        [self expandString];
    }
    
    // 刷新评论tableView
    self.tableView.frame = topicFrame.tableViewFrame;
    [self.tableView reloadData];
}

- (void)addLableMore {
    self.titleLabel.numberOfLines = 2;
    NSString *moreString = @"... 全部";
    NSMutableAttributedString *mutableAttributedString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@", moreString]];
    NSRange toUserRange = [moreString rangeOfString:@"... "];
    [mutableAttributedString setColor:UIColorHex(16191C) range:toUserRange];
    [mutableAttributedString setFont:FontRegular(14) range:toUserRange];
    NSRange expandRange = [moreString rangeOfString:@"全部"];
    [mutableAttributedString setColor:UIColorHex(007AFF) range:expandRange];
    [mutableAttributedString setFont:FontRegular(14) range:expandRange];

    //添加点击事件
    YYTextHighlight *toHighlight = [YYTextHighlight new];
    [mutableAttributedString setTextHighlight:toHighlight range:expandRange];
    __weak typeof(self) weakSelf = self;
    toHighlight.tapAction = ^(UIView *containerView, NSAttributedString *text, NSRange range, CGRect rect) {
        __typeof(&*weakSelf) strongSelf = weakSelf;
        if (strongSelf.cellDelegate && [strongSelf.cellDelegate respondsToSelector:@selector(didSelectShowAllTable:isShowAll:)]) {
            [strongSelf.cellDelegate didSelectShowAllTable:strongSelf.topicFrame isShowAll:YES];
        }
    };
    YYLabel *seeMore = [YYLabel new];
    seeMore.attributedText = mutableAttributedString;
    [seeMore sizeToFit];
    
    NSAttributedString *truncationToken = [NSAttributedString attachmentStringWithContent:seeMore contentMode:UIViewContentModeCenter attachmentSize:seeMore.frame.size alignToFont:FontRegular(14) alignment:YYTextVerticalAlignmentTop];
    self.titleLabel.truncationToken = truncationToken;
}

- (void)expandString {
    NSMutableAttributedString *attri = [self.titleLabel.attributedText mutableCopy];
    [attri appendAttributedString:[self appendAttriStringWithFont:attri.font]];
    self.titleLabel.attributedText = attri;
}

- (NSAttributedString *)appendAttriStringWithFont:(UIFont *)font {
    if (!font) {
        font = FontRegular(14);
    }
//    if ([_contentLabel.attributedText.string containsString:@"收起"]) {
//        return [[NSAttributedString alloc] initWithString:@""];
//    }
    
    NSString *appendText = @" 收起 ";
    NSMutableAttributedString *append = [[NSMutableAttributedString alloc] initWithString:appendText attributes:@{NSFontAttributeName : font, NSForegroundColorAttributeName : UIColorFromRGB(0x007aff)}];
    
    YYTextHighlight *hi = [YYTextHighlight new];
    [append setTextHighlight:hi range:[append.string rangeOfString:appendText]];
    
    __weak typeof(self) weakSelf = self;
    hi.tapAction = ^(UIView *containerView, NSAttributedString *text, NSRange range, CGRect rect) {
        //点击收起
        __typeof(&*weakSelf) strongSelf = weakSelf;
        if (strongSelf.cellDelegate && [strongSelf.cellDelegate respondsToSelector:@selector(didSelectShowAllTable:isShowAll:)]) {
            [strongSelf.cellDelegate didSelectShowAllTable:strongSelf.topicFrame isShowAll:NO];
        }
    };
    
    return append;
}

+ (instancetype)cellWithTableView:(UITableView *)tableView {
    static NSString *ID = @"TopicCell";
    QJConsultCommentViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[self alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cell;
}

#pragma mark - UITableViewDelegate , UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.topicFrame.commentFrames.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    QJConsultReplyViewCell *cell = [QJConsultReplyViewCell cellWithTableView:tableView];
    QJCommentReplyFrame *commentFrame = self.topicFrame.commentFrames[indexPath.row];
    cell.commentFrame = commentFrame;
    cell.cellDelegate = self;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    QJCommentReplyFrame *commentFrame = self.topicFrame.commentFrames[indexPath.row];
    return commentFrame.cellHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.01;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.contentView.width, 0.01)];
    return header;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    NSInteger total = self.topicFrame.topic.total;
    if (total > 2) {
        QJCommentBottomView *footer = [[QJCommentBottomView alloc] initWithFrame:CGRectMake(0, 0, self.contentView.width, 30)];
        [footer.expandButton addTarget:self action:@selector(expandTable:) forControlEvents:UIControlEventTouchUpInside];
        [footer.reductionButton addTarget:self action:@selector(reductionTable:) forControlEvents:UIControlEventTouchUpInside];
        [footer configureCommentBottomWithData:self.topicFrame.topic];
        return footer;
    } else {
        UIView *footer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.contentView.width, 0.01)];
        return footer;
    }
  
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    NSInteger total = self.topicFrame.topic.total;
    if (total > 2) {
        return 33*kWScale;
    } else {
        return 0.01;
    }
}

//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//
//}

- (void)expandTable:(UIButton *)sender {
    [self.cellDelegate didSelectExpandTable:self.topicFrame];
    
}

- (void)reductionTable:(UIButton *)sender {
    [self.cellDelegate didSelectReductionTable:self.topicFrame];
}


- (void)touchConsultReplyComment:(NSString *)commentID name:(NSString *)name userId:(NSString *)userId toUser:(nonnull QJCommentUser *)toUser{
    if (self.cellDelegate && [self.cellDelegate respondsToSelector:@selector(touchReplyMainComment:name:userId:toUser:)]) {
        [self.cellDelegate touchReplyMainComment:commentID name:name userId:userId toUser:toUser];
    }
}
@end
