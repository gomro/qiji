//
//  QJConsultCommentViewCell.h
//  QJBox
//
//  Created by Sun on 2022/7/11.
//

#import <UIKit/UIKit.h>
#import "QJCommentTopFrame.h"
#import "QJCommentReplyModel.h"

NS_ASSUME_NONNULL_BEGIN

@protocol QJConsultCommentViewCellDelegate <NSObject>

- (void)didSelectExpandTable:(QJCommentTopFrame *)topicFrame;

- (void)didSelectReductionTable:(QJCommentTopFrame *)topicFrame;

- (void)didSelectShowAllTable:(QJCommentTopFrame *)topicFrame isShowAll:(BOOL)isShowAll;

- (void)touchReplyMainComment:(NSString *)commentID name:(NSString *)name userId:(NSString *)userId toUser:(QJCommentUser *)toUser;

@end

@interface QJConsultCommentViewCell : UITableViewCell
/** 话题模型数据源 */
@property (nonatomic , strong) QJCommentTopFrame *topicFrame;
+ (instancetype)cellWithTableView:(UITableView *)tableView;

@property (nonatomic, weak) id<QJConsultCommentViewCellDelegate> cellDelegate;

@end

NS_ASSUME_NONNULL_END
