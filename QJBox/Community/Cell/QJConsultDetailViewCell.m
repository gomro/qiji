//
//  QJConsultDetailViewCell.m
//  QJBox
//
//  Created by Sun on 2022/7/11.
//

#import "QJConsultDetailViewCell.h"
@interface QJConsultDetailViewCell ()
/**描述*/
@property (nonatomic, strong) UILabel *titleLabel;
/**图片*/
@property (nonatomic, strong) UIImageView *detailImage;

@property (nonatomic, assign) QJConsultDetailViewCellStyle cellStyle;

/**视频*/
@property (nonatomic, strong) UIImageView *videoImage;
@property (nonatomic, strong) UIImageView *videoPlayer;
@end

@implementation QJConsultDetailViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier cellStyle:(QJConsultDetailViewCellStyle)cellStyle {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor = [UIColor whiteColor];
        self.cellStyle = cellStyle;
        [self loadUI];


    }
    return self;
}

#pragma mark - Load UI
- (void)loadUI {
    [self.contentView addSubview:self.titleLabel];
    if (self.cellStyle == QJConsultDetailViewCellStyleImage) {
        [self.contentView addSubview:self.detailImage];
    } else if (self.cellStyle == QJConsultDetailViewCellStyleVideo) {
        [self.contentView addSubview:self.videoImage];
//        [self.videoImage addSubview:self.videoPlayer];
    }
    [self makeConstraints];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(playNow)];
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(playNow)];
    [self.videoImage addGestureRecognizer:tap];
    [self.videoPlayer addGestureRecognizer:tap1];
}

- (void)makeConstraints {
    if (self.cellStyle == QJConsultDetailViewCellStyleImage) {
        [self.detailImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.contentView.mas_top).offset(10);
            make.bottom.equalTo(self.contentView);
            make.left.equalTo(self.contentView.mas_left).offset(16);
            make.right.equalTo(self.contentView.mas_right).offset(-16);
        }];
    } else if (self.cellStyle == QJConsultDetailViewCellStyleVideo) {
        [self.videoImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.contentView.mas_top).offset(10);
            make.left.equalTo(self.contentView.mas_left).offset(16);
            make.right.equalTo(self.contentView.mas_right).offset(-16);
            make.height.mas_equalTo(0.1);
            make.bottom.equalTo(self.contentView);
        }];
//        [self.videoPlayer mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.center.equalTo(self.videoImage);
//            make.width.height.mas_equalTo(40);
//        }];
    } else {
        [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.contentView.mas_top).offset(10);
            make.bottom.equalTo(self.contentView);
            make.left.equalTo(self.contentView.mas_left).offset(16);
            make.right.equalTo(self.contentView.mas_right).offset(-16);
        }];
    }
}

- (UIImageView *)detailImage {
    if (!_detailImage) {
        _detailImage = [UIImageView new];
        _detailImage.layer.masksToBounds = YES;
        _detailImage.layer.cornerRadius = 4;
        _detailImage.userInteractionEnabled = YES;
    }
    return _detailImage;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.font = MYFONTALL(FONT_REGULAR, 15);
        _titleLabel.numberOfLines = 0;
        _titleLabel.textColor = UIColorHex(333333);
    }
    return _titleLabel;
}

- (UIImageView *)videoImage {
    if (!_videoImage) {
        _videoImage = [UIImageView new];
        _videoImage.contentMode = UIViewContentModeScaleAspectFill;
        _videoImage.clipsToBounds = YES;
        _videoImage.layer.cornerRadius = 4;
        _videoImage.layer.masksToBounds = YES;
        _videoImage.userInteractionEnabled = YES;
    }
    return _videoImage;
}

- (UIImageView *)videoPlayer {
    if (!_videoPlayer) {
        _videoPlayer = [UIImageView new];
        _videoPlayer.image = [UIImage imageNamed:@"qj_home_play"];
        _videoPlayer.layer.masksToBounds = YES;
        _videoPlayer.userInteractionEnabled = YES;
    }
    return _videoPlayer;
}

- (void)configureCellWithData:(QJCampsiteDetailContentModel *)model {
    if ([model.type isEqualToString:@"P"]) {
        NSString *imageString = [kCheckStringNil(model.content) checkImageUrlString];
        NSURL *imageURL = [NSURL URLWithString:imageString];
        
        if (self.cellStyle == QJConsultDetailViewCellStyleVideo) {
//            [self.videoImage setImageURL:imageURL];
        } else {
            MJWeakSelf
            [self.detailImage setImageWithURL:imageURL placeholder:nil options:YYWebImageOptionAllowBackgroundTask | YYWebImageOptionProgressiveBlur completion:^(UIImage * _Nullable image, NSURL * _Nonnull url, YYWebImageFromType from, YYWebImageStage stage, NSError * _Nullable error) {
                __typeof(&*weakSelf) strongSelf = weakSelf;
                if (image) {
                    CGFloat width = kScreenWidth - 32;
                    CGFloat imageW = image.size.width;
                    CGFloat imageH = image.size.height;
                    CGFloat imageViewH = (width * imageH)/ (imageW * 1.0);
                    [strongSelf.detailImage mas_updateConstraints:^(MASConstraintMaker *make) {
                        make.height.mas_equalTo(imageViewH);
                    }];
                }
            }];
        }
    } else {
        self.titleLabel.text = kCheckStringNil(model.content);
    }

}

- (void)playNow {
    if (self.cellDelegate && [self.cellDelegate respondsToSelector:@selector(didVideoImageUrl)]) {
        [self.cellDelegate didVideoImageUrl];
    }
}
@end
