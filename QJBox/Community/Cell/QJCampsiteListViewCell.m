//
//  QJCampsiteListViewCell.m
//  QJBox
//
//  Created by Sun on 2022/7/7.
//

#import "QJCampsiteListViewCell.h"
#import "QJImageButton.h"
#import "QJCampsiteUserView.h"
#import "QJCampsiteRequest.h"
#import "HZPhotoBrowser.h"
#import "QJMineUserLabel.h"
#import "QJConsultDetailViewController.h"
@interface QJCampsiteListViewCell ()<HZPhotoBrowserDelegate>
/**个人视图*/
@property (nonatomic, strong) QJCampsiteUserView *userView;
/**标题*/
@property (nonatomic, strong) UILabel *titleLabel;
/**攻略*/
@property (nonatomic, strong) UILabel *raidersLabel;
/**视图集合*/
@property (nonatomic, strong) UIView *totalImageView;
/**视频*/
@property (nonatomic, strong) UIImageView *videoImage;
@property (nonatomic, strong) UIImageView *videoPlayer;

/**点赞*/
@property (nonatomic, strong) QJImageButton *likeButton;
/**评论*/
@property (nonatomic, strong) QJImageButton *commentButton;
/**分割线*/
@property (nonatomic, strong) UIView *lineView;

///在个人空间中需要显示的元素
@property (nonatomic, strong) UIButton *moreBtn;

@property (nonatomic, strong) UILabel *nodeLabel;//文章审核状态

@property (nonatomic, strong) UIImageView *browseImageView;

@property (nonatomic, strong) UILabel *browseLabel;//浏览人数

@property (nonatomic, strong) UIButton *starBtn;//

@property (nonatomic, strong) UILabel *starLabel;//点赞数

@property (nonatomic, strong) UIButton *commentBtn;//

@property (nonatomic, strong) UILabel *commentLabel;//评论数


@property (nonatomic, strong) UILabel *tagLabel;//永恒纪元类似标签

@property (nonatomic, strong) UILabel *examineLabel;//审核中

@property (nonatomic, strong) UILabel *hiddenArticleLabel;//该文章已被隐藏

@property (nonatomic, strong) UIImageView *hiddenArticleIcon;//隐藏图标

@property (nonatomic, strong) UIView *deleteBgView;//该文章已经删除背景

@property (nonatomic, strong) UIImageView *deleteIcon;

@property (nonatomic, strong) UILabel *deleteLabel;//该文章已经删除文本
///在个人空间中需要显示的元素


@property (nonatomic, assign) QJCampsiteListCellStyle cellStyle;

@property (nonatomic, strong) QJCampsiteDetailModel *dataModel;
@end

@implementation QJCampsiteListViewCell

#pragma mark - init
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier cellStyle:(QJCampsiteListCellStyle)cellStyle {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor = [UIColor whiteColor];
        self.cellStyle = cellStyle;
        [self loadUI];
        UITapGestureRecognizer *tapView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapUserView)];
        [self.userView addGestureRecognizer:tapView];
    }
    return self;
}

#pragma mark - Load UI
- (void)loadUI {
    [self.contentView addSubview:self.userView];
    [self.contentView addSubview:self.titleLabel];
    [self.contentView addSubview:self.raidersLabel];
    [self.contentView addSubview:self.likeButton];
    [self.contentView addSubview:self.commentButton];
    [self.contentView addSubview:self.lineView];
    if (self.cellStyle == QJCampsiteListCellStyleVideo) {
        [self.contentView addSubview:self.videoImage];
        [self.videoImage addSubview:self.videoPlayer];
    } else if (self.cellStyle == QJCampsiteListCellStyleImage) {
        [self.contentView addSubview:self.totalImageView];
    }
    [self.contentView addSubview:self.moreBtn];
    [self makeConstraints];
}

- (void)makeConstraints {
    [self.userView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView.mas_left).offset(16);
        make.top.equalTo(self.contentView.mas_top).offset(16);
        make.width.mas_lessThanOrEqualTo(200*kWScale);
        make.height.mas_equalTo(40*kWScale);
    }];
    [self.moreBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(22);
        make.width.height.equalTo(@(24*kWScale));
        make.right.equalTo(self.contentView).offset(-16);
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView.mas_left).offset(16);
        make.right.equalTo(self.contentView.mas_right).offset(-16);
        make.top.equalTo(self.userView.mas_bottom).offset(5);
    }];
    
    [self.raidersLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView.mas_left).offset(16);
        make.right.equalTo(self.contentView.mas_right).offset(-16);
        make.top.equalTo(self.titleLabel.mas_bottom);
    }];
    
    if (self.cellStyle == QJCampsiteListCellStyleDefault) {
        [self.commentButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.raidersLabel.mas_bottom).offset(12);
            make.right.equalTo(self.contentView.mas_right).offset(-16);
            make.height.mas_equalTo(25*kWScale);
        }];
   
    } else if (self.cellStyle == QJCampsiteListCellStyleVideo) {
        [self.videoImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.raidersLabel.mas_bottom).offset(10);
            make.left.equalTo(self.contentView.mas_left).offset(16);
            make.right.equalTo(self.contentView.mas_right).offset(-16);
            make.height.mas_equalTo(172*kWScale);
        }];
        [self.videoPlayer mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(self.videoImage);
            make.width.height.mas_equalTo(40*kWScale);
        }];
        [self.commentButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.videoImage.mas_bottom).offset(10);
            make.right.equalTo(self.contentView.mas_right).offset(-16);
            make.height.mas_equalTo(25*kWScale);
        }];
    } else if (self.cellStyle == QJCampsiteListCellStyleImage) {
        [self.totalImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.raidersLabel.mas_bottom).offset(10);
            make.left.equalTo(self.contentView.mas_left).offset(16);
            make.right.equalTo(self.contentView.mas_right).offset(-16);
            make.height.mas_equalTo(Get375Width(110));
        }];
        [self.commentButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.totalImageView.mas_bottom).offset(10);
            make.right.equalTo(self.contentView.mas_right).offset(-16);
            make.height.mas_equalTo(25*kWScale);
        }];
    }
    
    
    [self.likeButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.commentButton.mas_left).offset(-24);
        make.centerY.equalTo(self.commentButton);
        make.height.mas_equalTo(25*kWScale);
    }];
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.top.equalTo(self.likeButton.mas_bottom).offset(10);
        make.bottom.equalTo(self.contentView.mas_bottom);
        make.height.mas_equalTo(8*kWScale);
    }];
    
    [self.contentView addSubview:self.browseImageView];
    [self.contentView addSubview:self.browseLabel];
    [self.contentView addSubview:self.starBtn];
    [self.contentView addSubview:self.starLabel];
    [self.contentView addSubview:self.commentBtn];
    [self.contentView addSubview:self.commentLabel];
    
    [self.browseImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.browseLabel.mas_left).offset(-5);
        make.centerY.equalTo(self.browseLabel);
        make.width.equalTo(@(16*kWScale));
        make.height.equalTo(@(16*kWScale));
    }];
    
    [self.browseLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.starLabel);
        make.right.equalTo(self.starBtn.mas_left).offset(-26);
        
        
    }];
    
    [self.browseLabel setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
    
    
    
    [self.starBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.centerY.equalTo(self.starLabel);
        make.width.equalTo(@(16*kWScale));
        make.height.equalTo(@(16*kWScale));
        make.right.equalTo(self.starLabel.mas_left).offset(-5);
    }];
    
    [self.starLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView).offset(-16);
        make.bottom.equalTo(self.lineView.mas_top).offset(-22);
        make.left.equalTo(self.starBtn.mas_right).offset(2);
    }];
    
    [self.starLabel setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
    
    [self.contentView addSubview:self.tagLabel];
    [self.tagLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(16);
            make.centerY.equalTo(self.starLabel);
            
    }];
    
    [self.contentView addSubview:self.examineLabel];
    [self.contentView addSubview:self.hiddenArticleIcon];
    [self.contentView addSubview:self.hiddenArticleLabel];
    [self.contentView addSubview:self.deleteBgView];
    [self.deleteBgView addSubview:self.deleteIcon];
    [self.deleteBgView addSubview:self.deleteLabel];
    
    [self.examineLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.contentView).offset(-16);
            make.centerY.equalTo(self.moreBtn);
            make.height.equalTo(@(11*kWScale));
    }];
    
    [self.hiddenArticleIcon mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(16);
            make.width.height.equalTo(@(16*kWScale));
            make.centerY.equalTo(self.starLabel);
            
    }];
    
    [self.hiddenArticleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.hiddenArticleIcon.mas_right).offset(2);
            make.centerY.equalTo(self.hiddenArticleIcon);
            make.height.equalTo(@(13*kWScale));
    }];
    
    [self.deleteBgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self.contentView);
            make.bottom.equalTo(self.lineView.mas_top);
            make.top.equalTo(self.titleLabel);
            
    }];
    
    [self.deleteIcon mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.deleteBgView).offset(0);
        make.centerX.equalTo(self.deleteBgView);
            make.height.width.equalTo(@(Get375Width(240)));
             
            
    }];
    
    [self.deleteLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.deleteIcon);
            make.top.equalTo(self.deleteIcon).offset(Get375Width(180));
            
    }];
    
}

#pragma mark - Lazy Loading

- (QJCampsiteUserView *)userView {
    if (!_userView) {
        _userView = [QJCampsiteUserView new];
    }
    return _userView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.textColor = UIColorHex(474849);
        _titleLabel.font = MYFONTALL(FONT_BOLD, 16);
//        _titleLabel.numberOfLines = 0;
    }
    return _titleLabel;
}

- (UILabel *)raidersLabel {
    if (!_raidersLabel) {
        _raidersLabel = [UILabel new];
        _raidersLabel.textColor = UIColorHex(474849);
        _raidersLabel.font = MYFONTALL(FONT_REGULAR, 14);
        _raidersLabel.numberOfLines = 2;
    }
    return _raidersLabel;
}

- (UIImageView *)videoImage {
    if (!_videoImage) {
        _videoImage = [UIImageView new];
        _videoImage.contentMode = UIViewContentModeScaleAspectFill;
        _videoImage.clipsToBounds = YES;
        _videoImage.layer.cornerRadius = 4;
    }
    return _videoImage;
}

- (UIImageView *)videoPlayer {
    if (!_videoPlayer) {
        _videoPlayer = [UIImageView new];
        _videoPlayer.image = [UIImage imageNamed:@"qj_home_play"];
    }
    return _videoPlayer;
}

- (UIView *)totalImageView {
    if (!_totalImageView) {
        _totalImageView = [UIView new];
    }
    return _totalImageView;
}

- (UIView *)lineView {
    if (!_lineView) {
        _lineView = [UIView new];
        _lineView.backgroundColor = UIColorHex(F9F9F9);
    }
    return _lineView;
}

- (QJImageButton *)likeButton {
    if (!_likeButton) {
        _likeButton = [[QJImageButton alloc] init];
        _likeButton.imageSize = CGSizeMake(16, 16);
        _likeButton.space = 4;
        _likeButton.iconString = @"likeIcon";
        _likeButton.titleString = @"21";
        _likeButton.titleLb.font = MYFONTALL(FONT_REGULAR, 12);
        _likeButton.titleLb.textColor = UIColorHex(919599);
        _likeButton.style = ImageButtonStyleLeft;
        [_likeButton addTarget:self action:@selector(likeClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _likeButton;
}

- (QJImageButton *)commentButton {
    if (!_commentButton) {
        _commentButton = [[QJImageButton alloc] init];
        _commentButton.imageSize = CGSizeMake(16, 16);
        _commentButton.space = 4;
        _commentButton.iconString = @"commentIcon";
        _commentButton.titleLb.font = MYFONTALL(FONT_REGULAR, 12);
        _commentButton.titleLb.textColor = UIColorHex(919599);
        _commentButton.style = ImageButtonStyleLeft;
        [_commentButton addTarget:self action:@selector(commentClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _commentButton;
}

- (UIButton *)moreBtn {
    if (!_moreBtn) {
        _moreBtn = [UIButton new];
        [_moreBtn addTarget:self action:@selector(moreBtnAction) forControlEvents:UIControlEventTouchUpInside];
        [_moreBtn setImage:[UIImage imageNamed:@"moreDetailIcon"] forState:UIControlStateNormal];
        [_moreBtn setImage:[UIImage imageNamed:@"moreDetailIcon"] forState:UIControlStateHighlighted];
        _moreBtn.hidden = YES;
    }
    return _moreBtn;
}

- (UILabel *)browseLabel {
    if (!_browseLabel) {
        _browseLabel = [UILabel new];
        _browseLabel.font = FontMedium(11);
        _browseLabel.textAlignment = NSTextAlignmentLeft;
        _browseLabel.textColor = [UIColor colorWithHexString:@"#919599"];
        _browseLabel.hidden = YES;
    }
    return _browseLabel;
}

- (UIImageView *)browseImageView {
    if (!_browseImageView) {
        _browseImageView = [UIImageView new];
        _browseImageView.image = [UIImage imageNamed:@"home_preview"];
        _browseImageView.hidden = YES;
    }
    return _browseImageView;
}

- (UILabel *)starLabel {
    if (!_starLabel) {
        _starLabel = [UILabel new];
        _starLabel.font = FontMedium(11);
        _starLabel.text = @"";
        _starLabel.textColor = [UIColor colorWithHexString:@"#919599"];
        _starLabel.textAlignment = NSTextAlignmentLeft;
        _starLabel.hidden = YES;
    }
    return _starLabel;
}

- (UIButton *)starBtn {
    if (!_starBtn) {
        _starBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_starBtn addTarget:self action:@selector(starBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [_starBtn setBackgroundImage:[UIImage imageNamed:@"qj_star"] forState:UIControlStateNormal];
        [_starBtn setBackgroundImage:[UIImage imageNamed:@"qj_star_selected"] forState:UIControlStateSelected];
        [_starBtn setEnlargeEdgeWithTop:5 right:5 bottom:5 left:5];
        _starBtn.hidden = YES;
    }
    return _starBtn;
}


- (UILabel *)commentLabel {
    if (!_commentLabel) {
        _commentLabel = [UILabel new];
        _commentLabel.font = FontMedium(11);
        _commentLabel.text = @"";
        _commentLabel.textColor = [UIColor colorWithHexString:@"#919599"];
        _commentLabel.textAlignment = NSTextAlignmentLeft;
        _commentLabel.hidden = YES;
    }
    return _commentLabel;
}

- (UIButton *)commentBtn {
    if (!_commentBtn) {
        _commentBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_commentBtn addTarget:self action:@selector(commentBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [_commentBtn setBackgroundImage:[UIImage imageNamed:@"commentIcon"] forState:UIControlStateNormal];
        [_commentBtn setBackgroundImage:[UIImage imageNamed:@"commentIcon"] forState:UIControlStateSelected];
        [_commentBtn setEnlargeEdgeWithTop:5 right:5 bottom:5 left:5];
        
        _commentBtn.hidden = YES;
    }
    return _commentBtn;
}


- (UILabel *)tagLabel {
    if (!_tagLabel) {
        _tagLabel = [UILabel new];
        _tagLabel.font = FontRegular(12);
        _tagLabel.textColor = kColorWithHexString(@"#919599");
        _tagLabel.textAlignment = NSTextAlignmentLeft;
        _tagLabel.hidden = YES;
    }
    return _tagLabel;
}

- (UILabel *)examineLabel {
    if (!_examineLabel) {
        _examineLabel = [UILabel new];
        _examineLabel.font = FontRegular(10);
        _examineLabel.textColor = kColorWithHexString(@"#FF3B30");
        _examineLabel.textAlignment = NSTextAlignmentLeft;
        
    }
    return _examineLabel;
}

- (UIImageView *)hiddenArticleIcon {
    if (!_hiddenArticleIcon) {
        _hiddenArticleIcon = [UIImageView new];
        _hiddenArticleIcon.image = [UIImage imageNamed:@"qj_closeEye_16"];
        _hiddenArticleIcon.hidden = YES;
    }
    return _hiddenArticleIcon;
}


- (UILabel *)hiddenArticleLabel {
    if (!_hiddenArticleLabel) {
        _hiddenArticleLabel = [UILabel new];
        _hiddenArticleLabel.font = FontRegular(12);
        _hiddenArticleLabel.textColor = kColorWithHexString(@"#919599");
        _hiddenArticleLabel.textAlignment = NSTextAlignmentLeft;
        _hiddenArticleLabel.text = @"该文章已被隐藏";
        _hiddenArticleLabel.hidden = YES;
    }
    return _hiddenArticleLabel;
}

- (UIView *)deleteBgView {
    if (!_deleteBgView) {
        _deleteBgView = [UIView new];
        _deleteBgView.backgroundColor = [UIColor whiteColor];
        _deleteBgView.hidden = YES;
    }
    return _deleteBgView;
}


- (UIImageView *)deleteIcon {
    if (!_deleteIcon) {
        _deleteIcon = [UIImageView new];
        _deleteIcon.image = [UIImage imageWithColor:kColorWithHexString(@"#D9D9D9")];
        
    }
    return _deleteIcon;
}

- (UILabel *)deleteLabel {
    if (!_deleteLabel) {
        _deleteLabel = [UILabel new];
        _deleteLabel.font = FontRegular(12);
        _deleteLabel.textColor = kColorWithHexString(@"#000000");
        _deleteLabel.textAlignment = NSTextAlignmentCenter;
        _deleteLabel.numberOfLines = 2;
    }
    return _deleteLabel;
}


- (void)setIsFromUserSpaceVC:(BOOL)isFromUserSpaceVC {
    _isFromUserSpaceVC = isFromUserSpaceVC;
    
    self.starBtn.hidden = !isFromUserSpaceVC;
    self.starLabel.hidden = !isFromUserSpaceVC;
    self.browseLabel.hidden = YES;
    self.tagLabel.hidden = !isFromUserSpaceVC;
    self.browseImageView.hidden = YES;
    self.likeButton.hidden = isFromUserSpaceVC;
    self.commentButton.hidden = isFromUserSpaceVC;
    
}

#pragma mark  ------- action
//点击个人空间中的更多按钮
- (void)moreBtnAction {
    
    if (self.clickMoreBtnBlock) {
        self.clickMoreBtnBlock(self.dataModel);
    }
    
}

//点击个人空间中的点赞按钮
- (void)starBtnAction:(UIButton *)sender {
    [self likeClick];
}


//评论点击
- (void)commentBtnAction:(UIButton *)sender {
    
    [self pushDetailViewWithID:kCheckStringNil(self.dataModel.newsId) isComment:YES];
}


- (void)pushDetailViewWithID:(NSString *)campsiteID isComment:(BOOL)isComment {
    QJConsultDetailViewController *detailView = [[QJConsultDetailViewController alloc] init];
    detailView.isFromCamp = YES;
    detailView.idStr = campsiteID;
    detailView.isComment = isComment;
    UIViewController *vc =  [QJAppTool getCurrentViewController];
    [vc.navigationController pushViewController:detailView animated:YES];
}
#pragma mark - Configure Data
- (void)configureCellWithData:(QJCampsiteDetailModel *)model {
 
    
    if(self.isFromUserSpaceVC && [model.type isEqualToString:@"2"]){
        self.commentBtn.hidden = NO;
        self.commentLabel.hidden = NO;
        [self.commentBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerY.centerY.equalTo(self.starBtn);
            make.width.equalTo(@(16*kWScale));
            make.height.equalTo(@(16*kWScale));
            make.right.equalTo(self.contentView).offset(-50);
        }];
        
        [self.commentLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.contentView).offset(-16);
            make.centerY.equalTo(self.commentBtn);
            make.left.equalTo(self.commentBtn.mas_right).offset(4);
        }];
        
        [self.commentLabel setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
        
        [self.starBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerY.centerY.equalTo(self.starLabel);
            make.width.equalTo(@(16*kWScale));
            make.height.equalTo(@(16*kWScale));
            make.right.equalTo(self.starLabel.mas_left).offset(-5);
        }];
        
        [self.starLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.commentBtn.mas_left).offset(-26);
            make.bottom.equalTo(self.lineView.mas_top).offset(-22);
            make.left.equalTo(self.starBtn.mas_right).offset(2);
        }];
        
        [self.starLabel setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
        
        
    }else{
        self.commentBtn.hidden = YES;
        self.commentLabel.hidden = YES;
        [self.starBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerY.centerY.equalTo(self.starLabel);
            make.width.equalTo(@(16*kWScale));
            make.height.equalTo(@(16*kWScale));
            make.right.equalTo(self.starLabel.mas_left).offset(-5);
        }];
        
        [self.starLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.contentView).offset(-16);
            make.bottom.equalTo(self.lineView.mas_top).offset(-22);
            make.left.equalTo(self.starBtn.mas_right).offset(2);
        }];
        
        [self.starLabel setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
        
    }
   
    self.dataModel = model;
    self.titleLabel.text = kCheckStringNil(model.newsTitle);
    if (model.words.count) {
        NSString *str = model.words.firstObject;
        [str stringByReplacingOccurrencesOfString:@"\n" withString:@""];
         
        self.raidersLabel.text = str;
    }else{
        self.raidersLabel.text = @"";
    }
     
    self.starLabel.text = [NSString changeCountFromString:model.newsLikeCount];
    self.browseLabel.text =[NSString changeCountFromString:model.viewCount?:@""];
    self.commentLabel.text = [NSString getConvertTenThousandNumber:[model.newsCommentCount integerValue]];
    self.commentButton.titleString = [NSString getConvertTenThousandNumber:[model.newsCommentCount integerValue]];
    self.likeButton.titleString =  [NSString getConvertTenThousandNumber:[model.newsLikeCount integerValue]];
    
    self.tagLabel.text = model.tag?:@"";
    [self.userView configureHeaderlWithData:model];
    if (model.liked) {
        self.likeButton.iconString = @"likeIconSelected";
        self.starBtn.selected = YES;
    } else {
        self.likeButton.iconString = @"likeIcon";
        self.starBtn.selected = NO;
    }
    [self.likeButton layoutIfNeeded];
    self.likeButton.hitScale = 0.3;    
    [self.commentButton layoutIfNeeded];
    self.commentButton.hitScale = 1.1;
    
    if (![kCheckStringNil(model.videoAddress) isEqualToString:@""]) {
        NSString *imageString = [model.coverImage checkImageUrlString];
        
        NSURL *imageURL = [NSURL URLWithString:imageString];
        self.videoImage.hidden = NO;
        [self.videoImage setImageWithURL:imageURL options:YYWebImageOptionAllowBackgroundTask];
        if(self.isFromUserSpaceVC && model.deleted){
            self.videoImage.hidden = YES;
        }
    } else {
        if (model.pictures.count) {
            [self.totalImageView removeAllSubviews];
            CGFloat gapWidth = (kScreenWidth - 32) / 3;
            NSInteger count = model.pictures.count;
           
            for (NSInteger i = 0; i < model.pictures.count; i++) {
                if (i > 2) {
                    return;
                }
                UIImageView *imageView = [UIImageView new];
                imageView.layer.cornerRadius = 4;
                imageView.contentMode = UIViewContentModeScaleAspectFill;
                imageView.clipsToBounds = YES;
                imageView.userInteractionEnabled = YES;
                
                NSString *imageString = [model.pictures[i] checkImageUrlString];
                NSURL *imageURL = [NSURL URLWithString:imageString];
                [imageView setImageWithURL:imageURL options:YYWebImageOptionAllowBackgroundTask];
                [self.totalImageView addSubview:imageView];
                [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.equalTo(self.totalImageView.mas_left).offset(gapWidth * i);
                    make.width.height.mas_equalTo(Get375Width(110));
                    make.centerY.equalTo(self.totalImageView);
                }];
                imageView.tag = i + 40;
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickImage:)];
                [imageView addGestureRecognizer:tap];
                
                if (i == 2 && model.pictures.count > 3) {
                    UILabel *titleLb = [UILabel new];
                    titleLb.textColor = [UIColor whiteColor];
                    titleLb.font = MYFONTALL(FONT_BOLD, 16);
                    titleLb.text = [NSString stringWithFormat:@"+%ld", model.pictures.count];
                    [imageView addSubview:titleLb];
                    [titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
                        make.right.equalTo(imageView.mas_right).offset(-4);
                        make.bottom.equalTo(imageView.mas_bottom).offset(-5);
                    }];
                }
            }
    
        }
    }
    
    
    
    
    
    if (self.isFromUserSpaceVC) {
       
        self.moreBtn.hidden = NO;
        if (model.deleted || model.hidden) {//先判断删除或者隐藏
            self.deleteBgView.hidden = NO;
            [self.deleteBgView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.left.right.equalTo(self.contentView);
                    make.bottom.equalTo(self.lineView.mas_top);
                    make.top.equalTo(self.titleLabel);
                make.height.equalTo(@(Get375Width(256)));
            }];
            self.examineLabel.hidden = YES;
            if (model.deleted) {//判断是否删除
                if([model.sortType isEqualToString:@"3"]){//动态
                    self.deleteLabel.text = @"该动态已被删除~";
                }else{
                    self.deleteLabel.text = @"该文章已被删除~";
                }
                
                self.deleteIcon.image = [UIImage imageNamed:@"empty_no_delete"];
                
                
            }else{
                self.deleteIcon.image = [UIImage imageNamed:@"empty_no_authority"];
                if([model.sortType isEqualToString:@"3"]){//动态
                    self.deleteLabel.text = @"该动态已被隐藏~";
                }else{
                    self.deleteLabel.text = @"该文章已被隐藏~";
                }
                
            }
            
            if ([model.userId isEqualToString:[QJUserManager shareManager].userID] && model.hidden &&!model.deleted) {//作者自己的文章 并且隐藏了
                self.deleteBgView.hidden = YES;
                [self.deleteBgView mas_remakeConstraints:^(MASConstraintMaker *make) {
                        make.left.right.equalTo(self.contentView);
                        make.bottom.equalTo(self.lineView.mas_top);
                        make.top.equalTo(self.titleLabel);
                        
                }];
                self.hiddenArticleIcon.hidden = NO;
                self.hiddenArticleLabel.hidden = NO;
                self.tagLabel.text = @"";
            }else{
                self.hiddenArticleIcon.hidden = YES;
                self.hiddenArticleLabel.hidden = YES;
            }
            
            
        }else{
            self.deleteBgView.hidden = YES;
            self.hiddenArticleIcon.hidden = YES;
            self.hiddenArticleLabel.hidden = YES;
            
            [self.deleteBgView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.left.right.equalTo(self.contentView);
                    make.bottom.equalTo(self.lineView.mas_top);
                    make.top.equalTo(self.titleLabel);
                    
            }];
          
            
            
        }
        if (model.state == 1) {//审核中
            self.examineLabel.hidden = NO;
            self.examineLabel.text = @"审核中";
            self.moreBtn.hidden = YES;
        }else{
            self.examineLabel.hidden = YES;
            self.moreBtn.hidden = NO;
        }
        
    }
    
    
}

- (void)commentClick {
    if (self.cellDelegate && [self.cellDelegate respondsToSelector:@selector(didCommentButtonWithID:)]) {
        [self.cellDelegate didCommentButtonWithID:kCheckStringNil(self.dataModel.newsId)];
    }
}

- (void)likeClick {
//    [QJAppTool showHUDLoading];
    NSString *campsiteId = kCheckStringNil(self.dataModel.newsId);
    BOOL liked = self.dataModel.liked;
    NSString *action = @"1";
    if (liked) {
        action = @"0";
    }
    QJCampsiteRequest *startRequest = [[QJCampsiteRequest alloc] init];
    [startRequest netWorkPutCampLike:campsiteId type:self.dataModel.type?:@"" action:action];
    [startRequest setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
//        [QJAppTool hideHUDLoading];
        if (ResponseSuccess) {
//            UIWindow * window = [UIApplication sharedApplication].delegate.window;
            NSInteger newsLikeCount = [self.dataModel.newsLikeCount integerValue];
            if ([action isEqualToString:@"1"]) {
                
//                [window makeToast:@"点赞成功"];
                self.dataModel.liked = YES;
                newsLikeCount = newsLikeCount + 1;
                self.dataModel.newsLikeCount = [NSString stringWithFormat:@"%ld", newsLikeCount];
                
            } else {
                
//                [window makeToast:@"取消点赞"];
            
                self.dataModel.liked = NO;
                newsLikeCount = newsLikeCount - 1;
                self.dataModel.newsLikeCount = [NSString stringWithFormat:@"%ld", newsLikeCount];
            }
            
            if (self.dataModel.liked) {
                self.likeButton.iconString = @"likeIconSelected";
                self.starBtn.selected = YES;
            } else {
                self.likeButton.iconString = @"likeIcon";
                self.starBtn.selected = NO;
            }
 
            self.likeButton.titleString =  [NSString getConvertTenThousandNumber:[self.dataModel.newsLikeCount integerValue]];
            self.starLabel.text = [NSString getConvertTenThousandNumber:[self.dataModel.newsLikeCount integerValue]];
        } else {
            NSString *code = request.responseJSONObject[@"code"];
            NSString *message = request.responseJSONObject[@"message"];
            [[QJAppTool getCurrentViewController].view makeToast:kCheckStringNil(message)];
            if (self.cellDelegate && [self.cellDelegate respondsToSelector:@selector(didRefreshListView:)]) {
                [self.cellDelegate didRefreshListView:kCheckStringNil(code)];
            }
        }
    }];
    [startRequest setFailureCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
//        [QJAppTool hideHUDLoading];
    }];
}

- (void)clickImage:(UITapGestureRecognizer*)gesture {
    NSInteger tag = gesture.view.tag - 40;
    [self touchPictureIndex:tag];
    
}

- (void)tapUserView {
    if (self.cellDelegate && [self.cellDelegate respondsToSelector:@selector(didClickUserView:)]) {
        [self.cellDelegate didClickUserView:kCheckStringNil(self.dataModel.userId)];
    }
}

- (void)touchPictureIndex:(NSInteger)index {
    NSMutableArray *imageArr = [NSMutableArray arrayWithCapacity:0];
    for (NSString *picStr in self.dataModel.pictures) {
        NSString *imageString = [picStr checkImageUrlString];
        [imageArr addObject:imageString];
    }
    HZPhotoBrowser *browser = [[HZPhotoBrowser alloc] init];

    browser.isSavePhoto = YES;
    browser.isFullWidthForLandScape = NO;
    browser.isNeedLandscape = NO;
    browser.imageUrlHeader = @{};
    browser.imageArray = [imageArr copy];
    browser.imageCount = imageArr.count;
    browser.currentImageIndex = (int)index;
    browser.delegate = self;
    [browser show];
}

//// 返回临时占位图片（即原来的小图）
- (UIImage *)photoBrowser:(HZPhotoBrowser *)browser placeholderImageForIndex:(NSInteger)index {
//    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
//    SquareDynamicDetailViewCell * cell = (SquareDynamicDetailViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];//即为要得到的cell
    return [UIImage new];
}

// 返回高质量图片的url
- (NSURL *)photoBrowser:(HZPhotoBrowser *)browser highQualityImageURLForIndex:(NSInteger)index {
    
    NSMutableArray *imageArr = [NSMutableArray arrayWithCapacity:0];
    for (NSString *picStr in self.dataModel.pictures) {
        NSString *imageString = [picStr checkImageUrlString];
        [imageArr addObject:[NSURL URLWithString:imageString]];

    }
    return imageArr[index];
}


@end
