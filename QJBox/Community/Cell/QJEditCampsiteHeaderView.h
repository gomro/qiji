//
//  QJEditCampsiteHeaderView.h
//  QJBox
//
//  Created by Sun on 2022/7/6.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void(^didCampsiteHeader)(void);

@interface QJEditCampsiteHeaderView : UIView
@property (nonatomic, copy) didCampsiteHeader clickCampsite;

- (void)configureHeaderEditWithData:(NSArray *)dataArr;

@end

NS_ASSUME_NONNULL_END
