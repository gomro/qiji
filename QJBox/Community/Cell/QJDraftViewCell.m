//
//  QJDraftViewCell.m
//  QJBox
//
//  Created by Sun on 2022/7/22.
//

#import "QJDraftViewCell.h"

@interface QJDraftViewCell ()
@property (nonatomic, strong) UILabel *title;
@property (nonatomic, strong) UILabel *time;
@property (nonatomic, strong) UIButton *deleteBt;
@property (nonatomic, strong) UIView *backView;
@end

@implementation QJDraftViewCell

- (UILabel *)title {
    if (!_title) {
        _title = [UILabel new];
        _title.textColor = UIColorHex(16191C);
        _title.font = MYFONTALL(FONT_BOLD, 16);
        _title.numberOfLines = 0;
    }
    return _title;
}

- (UILabel *)time {
    if (!_time) {
        _time = [UILabel new];
        _time.textColor = UIColorHex(919599);
        _time.font = MYFONTALL(FONT_REGULAR, 10);
    }
    return _time;
}

- (UIButton *)deleteBt {
    if (!_deleteBt) {
        _deleteBt = [UIButton new];
        [_deleteBt setTitle:@"删除" forState:UIControlStateNormal];
        [_deleteBt setTitleColor:UIColorHex(FF3B30) forState:UIControlStateNormal];
        _deleteBt.titleLabel.font = MYFONTALL(FONT_REGULAR, 14);
    }
    return _deleteBt;
}

- (UIView *)backView {
    if (!_backView) {
        _backView = [UIView new];
        _backView.backgroundColor = [UIColor whiteColor];
        _backView.layer.masksToBounds = YES;
        _backView.layer.cornerRadius = 4;
    }
    return _backView;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor = UIColorHex(F5F5F5);
        [self.contentView addSubview:self.backView];
        [self.backView addSubview:self.title];
        [self.backView addSubview:self.time];
        [self.backView addSubview:self.deleteBt];
        [self makeConstraint];
    }
    return self;
}

- (void)makeConstraint {
    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top).offset(16);
        make.left.equalTo(self.contentView.mas_left).offset(16);
        make.right.equalTo(self.contentView.mas_right).offset(-16);
        make.bottom.equalTo(self.contentView.mas_bottom);
    }];
    [self.title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.backView.mas_top).offset(16);
        make.left.equalTo(self.backView.mas_left).offset(16);
        make.right.equalTo(self.backView.mas_right).offset(-16);
    }];
    [self.deleteBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.title.mas_bottom).offset(8);
        make.bottom.equalTo(self.backView.mas_bottom).offset(-16);
        make.right.equalTo(self.backView.mas_right).offset(-16);
        make.height.mas_equalTo(16);
    }];
    [self.time mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.backView.mas_left).offset(16);
        make.centerY.equalTo(self.deleteBt);
    }];
}

- (void)configureCellWithData:(QJDraftModel *)model {
    self.title.text = model.title;
    self.time.text = model.showTime;
}

@end
