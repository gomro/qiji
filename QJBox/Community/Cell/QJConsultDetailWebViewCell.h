//
//  QJConsultDetailWebViewCell.h
//  QJBox
//
//  Created by Sun on 2022/8/8.
//

#import <UIKit/UIKit.h>

@protocol QJConsultDetailWebViewCellDelegate <NSObject>

- (void)changeWebViewHeight:(CGFloat)height;
@end


NS_ASSUME_NONNULL_BEGIN

@interface QJConsultDetailWebViewCell : UITableViewCell
@property (weak, nonatomic) id<QJConsultDetailWebViewCellDelegate> delegate;


- (void)configureCellWithDataModel:(NSString *)htmlContent;

@end

NS_ASSUME_NONNULL_END
