//
//  QJCommunityInfoDraftTableCell.h
//  QJBox
//
//  Created by wxy on 2022/8/5.
//

#import <UIKit/UIKit.h>
@class QJCommunityDraftModel;
NS_ASSUME_NONNULL_BEGIN

/// 草稿箱cell
@interface QJCommunityInfoDraftTableCell : UITableViewCell

@property (nonatomic, strong) QJCommunityDraftModel *model;

@property (nonatomic, assign) BOOL isEdite;


@end

NS_ASSUME_NONNULL_END
