//
//  QJCommunityInfoDraftTableCell.m
//  QJBox
//
//  Created by wxy on 2022/8/5.
//

#import "QJCommunityInfoDraftTableCell.h"
#import "QJCommunityDraftModel.h"
@interface QJCommunityInfoDraftTableCell ()

@property (nonatomic, strong) UIImageView *checkBtn;

@property (nonatomic, strong) UIView *bgView;

@property (nonatomic, strong) UIImageView *productImageView;

@property (nonatomic, strong) UILabel *titleLabel;

@property (nonatomic, strong) UILabel *contentLabel;

@property (nonatomic, strong) UILabel *timeLabel;

@property (nonatomic, strong) UIImageView *paussImageView;//暂停

@property (nonatomic, strong) UILabel *editLabel;


@end


@implementation QJCommunityInfoDraftTableCell


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [self loadUI];
    }
    return self;
}



- (void)loadUI {
    self.contentView.backgroundColor = RGB(244, 244, 244);
    [self.contentView addSubview:self.checkBtn];
    [self.contentView addSubview:self.bgView];
    [self.bgView addSubview:self.productImageView];
    [self.bgView addSubview:self.titleLabel];
    [self.bgView addSubview:self.contentLabel];
    [self.bgView addSubview:self.timeLabel];
    [self.bgView addSubview:self.editLabel];
    [self.productImageView addSubview:self.paussImageView];
    
    [self.checkBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(24);
            make.centerY.equalTo(self.bgView);
            make.width.height.equalTo(@16);
    }];
    
    [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(16);
            make.right.equalTo(self.contentView).offset(-16);
            make.bottom.equalTo(self.contentView).offset(-16);
            make.top.equalTo(self.contentView);
    }];
    
    [self.productImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.bgView);
            make.width.equalTo(@100);
            make.height.equalTo(@56);
            make.left.equalTo(self.bgView).offset(16);
    }];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.productImageView.mas_right).offset(8);
            make.top.equalTo(self.productImageView);
            make.right.equalTo(self.bgView).offset(-16);
            
    }];
    
    [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.titleLabel);
            make.top.equalTo(self.titleLabel.mas_bottom).offset(8);
            make.right.equalTo(self.bgView).offset(-16);
    }];
    
    [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.titleLabel);
            make.bottom.equalTo(self.bgView).offset(-18);
    }];
    
    [self.editLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.bgView).offset(-12.5);
            make.bottom.equalTo(self.bgView).offset(-12.5);
    }];
    
    [self.paussImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(self.productImageView);
            make.width.height.equalTo(@24);
    }];
    
}


#pragma mark  -------- 数据填充

- (void)setModel:(QJCommunityDraftModel *)model {
    _model = model;
    
    if (model.isSelected) {
        self.checkBtn.image = [UIImage imageNamed:@"qj_check_icon"];
    }else{
        self.checkBtn.image = [UIImage imageNamed:@"qj_uncheck_icon"];
    }
    self.titleLabel.text = model.title;
  
    
    self.productImageView.image = model.overImage;
    self.timeLabel.text = [NSString getTimespString:model.timesp];
    if (model.richAttributedString.length > 0) {
        self.contentLabel.text = [self textString:model.richAttributedString];
    }else{
        self.contentLabel.text = @"(暂无内容编辑)";
    }
    
    if (model.viderUrl) {
        self.productImageView.hidden = NO;
         
        self.paussImageView.hidden = NO;
        [self.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.productImageView.mas_right).offset(8);
                make.top.equalTo(self.productImageView);
                make.right.equalTo(self.bgView).offset(-16);
                
        }];
    }else{
        if (model.title.length == 0) {
            self.titleLabel.text = @"无标题";
            if (model.overImage) {
                self.titleLabel.text = @"[封面图]";
            }
        }
        self.productImageView.hidden = YES;
        [self.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.bgView).offset(16);
                make.top.equalTo(self.bgView).offset(16);
                make.right.equalTo(self.bgView).offset(-16);
                
        }];
    }
    if (self.isEdite) {
        [self.bgView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.contentView).offset(48);
                make.right.equalTo(self.contentView).offset(-16);
                make.bottom.equalTo(self.contentView).offset(-16);
                make.top.equalTo(self.contentView);
        }];
    }else{
        [self.bgView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.contentView).offset(16);
                make.right.equalTo(self.contentView).offset(-16);
                make.bottom.equalTo(self.contentView).offset(-16);
                make.top.equalTo(self.contentView);
        }];
    }
}


- (NSString *)textString:(NSAttributedString *)attributedText
{
    NSMutableAttributedString * resutlAtt = [[NSMutableAttributedString alloc]initWithAttributedString:attributedText];
     
    //枚举出所有的附件字符串
    [attributedText enumerateAttributesInRange:NSMakeRange(0, attributedText.length) options:NSAttributedStringEnumerationReverse usingBlock:^(NSDictionary *attrs, NSRange range, BOOL *stop) {
        //从字典中取得那一个图片
        NSTextAttachment * textAtt = attrs[NSAttachmentAttributeName];
        if (textAtt)
        {
            NSString *text = @"[图片]";
            [resutlAtt replaceCharactersInRange:range withString:text];
        }
    }];
    return resutlAtt.string;
}

#pragma mark  ----- getter

- (UIImageView *)checkBtn {
    if (!_checkBtn) {
        _checkBtn = [UIImageView new];
        _checkBtn.image = [UIImage imageNamed:@"qj_uncheck_icon"];
         
    }
    return _checkBtn;
}

- (UIView *)bgView {
    if (!_bgView) {
        _bgView = [UIView new];
        _bgView.backgroundColor = [UIColor whiteColor];
        _bgView.layer.cornerRadius = 4;
    }
    return _bgView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.font = FontSemibold(14);
        _titleLabel.textColor = kColorWithHexString(@"#16191C");
        _titleLabel.textAlignment = NSTextAlignmentLeft;
        _titleLabel.numberOfLines = 1;
        _titleLabel.text = @"无标题";
    }
    return _titleLabel;
}

-(UILabel *)contentLabel {
    if (!_contentLabel) {
        _contentLabel = [UILabel new];
        _contentLabel.textColor = kColorWithHexString(@"#919599");
        _contentLabel.textAlignment = NSTextAlignmentLeft;
        _contentLabel.numberOfLines = 1;
        _contentLabel.text = @"(暂无内容编辑)";
        _contentLabel.font = FontRegular(10);
    }
    return _contentLabel;
}


- (UILabel *)timeLabel {
    if (!_timeLabel) {
        _timeLabel = [UILabel new];
        _timeLabel.font = FontRegular(10);
        _timeLabel.textColor = kColorWithHexString(@"#919599");
        _timeLabel.textAlignment = NSTextAlignmentLeft;
    }
    return _timeLabel;
}

- (UILabel *)editLabel {
    if (!_editLabel) {
        _editLabel = [UILabel new];
        _editLabel.font = FontRegular(14);
        _editLabel.textColor = kColorWithHexString(@"#007AFF");
        _editLabel.textAlignment = NSTextAlignmentRight;
        _editLabel.text = @"编辑";
    }
    return _editLabel;
}


- (UIImageView *)productImageView {
    if (!_productImageView) {
        _productImageView = [UIImageView new];
        _productImageView.layer.cornerRadius = 2;
        _productImageView.layer.masksToBounds = YES;
    }
    return _productImageView;
}


- (UIImageView *)paussImageView {
    if (!_paussImageView) {
        _paussImageView = [UIImageView new];
        _paussImageView.image = [UIImage imageNamed:@"qj_play_normal_24"];
        _paussImageView.hidden = YES;
    }
    return _paussImageView;
}


@end
