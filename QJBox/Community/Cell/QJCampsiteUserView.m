//
//  QJCampsiteUserView.m
//  QJBox
//
//  Created by Sun on 2022/7/7.
//

#import "QJCampsiteUserView.h"
#import "QJMySpaceViewController.h"
@interface QJCampsiteUserView ()

/**头像*/
@property (nonatomic, strong) UIImageView *avatar;

@property (nonatomic, strong) UIButton *avatarBtn;

/**昵称*/
@property (nonatomic, strong) UILabel *nickName;
/**发布时间*/
@property (nonatomic, strong) UILabel *timeLb;
/**等级*/
@property (nonatomic, strong) UIImageView *level;
/**会员标志*/
@property (nonatomic, strong) UIImageView *avatarLevel;

@property (nonatomic, copy) NSString *userId;


@end

@implementation QJCampsiteUserView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:self.avatar];
        [self addSubview:self.avatarBtn];
        [self addSubview:self.avatarLevel];
        [self addSubview:self.level];
        [self addSubview:self.nickName];
        [self addSubview:self.timeLb];
        [self makeConstraints];
    }
    return self;
}

- (void)makeConstraints {
    [self.avatar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top);
        make.left.equalTo(self.mas_left);
        make.width.height.mas_equalTo(36*kWScale);
    }];
    
    [self.avatarBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(self.avatar);
            make.width.height.equalTo(@(kWScale*40));
    }];
    
    [self.avatarLevel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.avatar.mas_bottom);
        make.right.equalTo(self.avatar.mas_right).offset(2);
    }];
    
    [self.nickName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.avatar.mas_top).offset(1);
        make.left.equalTo(self.avatar.mas_right).offset(9);
    }];
    
    [self.level mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.nickName);
        make.left.equalTo(self.nickName.mas_right).offset(2);
    }];
    
    [self.timeLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.avatar.mas_bottom).offset(-1);
        make.left.equalTo(self.nickName.mas_left);
    }];
}

- (UIImageView *)avatar {
    if (!_avatar) {
        _avatar = [UIImageView new];
        _avatar.layer.masksToBounds = YES;
        _avatar.layer.cornerRadius = 18*kWScale;
        _avatar.userInteractionEnabled = YES;
    }
    return _avatar;
}


- (UIButton *)avatarBtn {
    if(!_avatarBtn) {
        _avatarBtn = [UIButton new];
        [_avatarBtn addTarget:self action:@selector(avatarBtnAction) forControlEvents:UIControlEventTouchUpInside];
        _avatarBtn.backgroundColor = [UIColor clearColor];
    }
    return _avatarBtn;
}

- (UILabel *)nickName {
    if (!_nickName) {
        _nickName = [UILabel new];
        _nickName.font = MYFONTALL(FONT_REGULAR, 14);
        _nickName.textColor = UIColorHex(474849);
    }
    return _nickName;
}

- (UILabel *)timeLb {
    if (!_timeLb) {
        _timeLb = [UILabel new];
        _timeLb.font = MYFONTALL(FONT_REGULAR, 10);
        _timeLb.textColor = UIColorHex(919599);
    }
    return _timeLb;
}

- (UIImageView *)level {
    if(!_level) {
        _level = [UIImageView new];
        _level.hidden = YES;
    }
    return _level;
}

- (UIImageView *)avatarLevel {
    if(!_avatarLevel) {
        _avatarLevel = [UIImageView new];
        _avatarLevel.image = [UIImage imageNamed:@"avatarLevelIcon"];
        _avatarLevel.hidden = YES;
    }
    return _avatarLevel;
}

- (void)configureHeaderlWithData:(QJCampsiteDetailModel *)model {
    self.userId = model.userId?:@"";
    NSString *imageString = [kCheckStringNil(model.userCoverImage) checkImageUrlString];
    NSURL *imageURL = [NSURL URLWithString:imageString];
    [self.avatar setImageWithURL:imageURL options:YYWebImageOptionAllowBackgroundTask];
    self.nickName.text = kCheckStringNil(model.userNickName);
    self.timeLb.text = kCheckStringNil(model.newsAnnounceTime);

    if([model.userLevel integerValue]) {
        NSString *str = [NSString stringWithFormat:@"qj_level_%ld",(long)model.userLevel.integerValue];
        self.level.image = [UIImage imageNamed:str];
        self.level.hidden = NO;
    } else {
        self.level.hidden = YES;
    }

    self.avatarLevel.hidden = YES;
}

- (void)configureHeaderlCommentWithData:(QJCommentTopModel *)model {
    self.userId = model.userId?:@"";
    NSString *imageString = [kCheckStringNil(model.coverImage) checkImageUrlString];
    NSURL *imageURL = [NSURL URLWithString:imageString];
    [self.avatar setImageWithURL:imageURL options:YYWebImageOptionAllowBackgroundTask];
    self.nickName.text = kCheckStringNil(model.userName);
    self.timeLb.text = kCheckStringNil(model.timeShow);
    
    if([model.level integerValue]) {
        NSString *str = [NSString stringWithFormat:@"qj_level_%ld",(long)model.level.integerValue];
        self.level.image = [UIImage imageNamed:str];
        self.level.hidden = NO;
    } else {
        self.level.hidden = YES;
    }
    
    self.avatarLevel.hidden = NO;
    self.avatarLevel.image = [UIImage imageNamed:[NSString stringWithFormat:@"qj_bozhuLevel_%ld", model.bloggerLevel]];

     
}

- (void)configureHeaderNewsWithData:(QJNewsListModel *)model {
    self.userId = model.userId?:@"";
    NSString *imageString = [kCheckStringNil(model.userCoverImage) checkImageUrlString];
    NSURL *imageURL = [NSURL URLWithString:imageString];
    [self.avatar setImageWithURL:imageURL options:YYWebImageOptionAllowBackgroundTask];
    self.nickName.text = kCheckStringNil(model.userNickName);
    self.timeLb.text = kCheckStringNil(model.infoAnnounceTime);

    self.avatarLevel.hidden = NO;
    self.avatarLevel.image = [UIImage imageNamed:[NSString stringWithFormat:@"qj_bozhuLevel_%ld",model.userBloggerLevel]];
    self.level.hidden = YES;

}

//他人空间
- (void)avatarBtnAction {
    if(IsStrEmpty(self.userId)){
        return;
    }
    QJMySpaceViewController *vc = [QJMySpaceViewController new];
    vc.userId = self.userId;
    [[QJAppTool getCurrentViewController].navigationController pushViewController:vc animated:YES];
}

@end
