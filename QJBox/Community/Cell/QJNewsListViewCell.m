//
//  QJNewsListViewCell.m
//  QJBox
//
//  Created by Sun on 2022/7/28.
//

#import "QJNewsListViewCell.h"
#import "QJImageButton.h"
#import "QJNewsUserView.h"
#import "QJCampsiteRequest.h"
@interface QJNewsListViewCell ()
/**个人视图*/
@property (nonatomic, strong) QJNewsUserView *userView;
/**标题*/
@property (nonatomic, strong) UILabel *titleLabel;
/**图片*/
@property (nonatomic, strong) UIImageView *rightImage;
/**视频图片*/
@property (nonatomic, strong) UIImageView *videoPlayer;
/**视频按钮*/
@property (nonatomic, strong) UIButton *videoButton;

/**标签*/
@property (nonatomic, strong) UILabel *tagLabel;
/**点赞*/
@property (nonatomic, strong) QJImageButton *likeButton;
/**看*/
@property (nonatomic, strong) QJImageButton *watchButton;
/**分割线*/
@property (nonatomic, strong) UIView *lineView;
@property (nonatomic, strong) QJNewsListModel *dataModel;

@end

@implementation QJNewsListViewCell

#pragma mark - init
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor = [UIColor whiteColor];
        [self loadUI];
    }
    return self;
}

#pragma mark - Load UI
- (void)loadUI {
    [self.contentView addSubview:self.userView];
    [self.contentView addSubview:self.titleLabel];
    [self.contentView addSubview:self.likeButton];
    [self.contentView addSubview:self.watchButton];
    [self.contentView addSubview:self.lineView];
    [self.contentView addSubview:self.rightImage];
    [self.rightImage addSubview:self.tagLabel];
    [self.rightImage addSubview:self.videoPlayer];
    [self.rightImage addSubview:self.videoButton];
    [self makeConstraints];
}

- (void)makeConstraints {
    [self.rightImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView.mas_right).offset(-Get375Width(16));
        make.top.equalTo(self.contentView.mas_top).offset(Get375Width(16));
        make.width.mas_equalTo(Get375Width(114));
        make.height.mas_equalTo(Get375Width(76));
    }];
    [self.videoButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.bottom.equalTo(self.rightImage);
    }];
    
    [self.tagLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.equalTo(self.rightImage);
        make.height.mas_equalTo(Get375Width(16));
    }];
    
    [self.videoPlayer mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self.rightImage);
        make.width.height.mas_equalTo(Get375Width(24));
    }];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView.mas_left).offset(Get375Width(16));
        make.right.equalTo(self.rightImage.mas_left).offset(-Get375Width(9));
        make.top.equalTo(self.contentView.mas_top).offset(Get375Width(16));
    }];
    
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView.mas_left).offset(Get375Width(16));
        make.right.equalTo(self.contentView.mas_right).offset(-Get375Width(16));
        make.bottom.equalTo(self.contentView.mas_bottom);
        make.height.mas_equalTo(Get375Width(1));
    }];
    
    [self.userView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView.mas_left).offset(Get375Width(16));
        make.bottom.equalTo(self.lineView.mas_top).offset(-Get375Width(16));
        make.width.mas_lessThanOrEqualTo(Get375Width(200));
        make.height.mas_equalTo(Get375Width(16));
    }];
    
    [self.likeButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.lineView.mas_top).offset(-Get375Width(16));
        make.right.equalTo(self.rightImage.mas_left).offset(-Get375Width(39));
        make.height.mas_equalTo(Get375Width(16));
    }];
    
//    [self.watchButton mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.right.equalTo(self.likeButton.mas_left).offset(-16);
//        make.centerY.equalTo(self.likeButton);
//        make.height.mas_equalTo(16);
//    }];
}

#pragma mark - Lazy Loading

- (QJNewsUserView *)userView {
    if (!_userView) {
        _userView = [QJNewsUserView new];
    }
    return _userView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.textColor = UIColorHex(000000);
        _titleLabel.font = FontRegular(14);
        _titleLabel.numberOfLines = 2;
    }
    return _titleLabel;
}

- (UILabel *)tagLabel {
    if (!_tagLabel) {
        _tagLabel = [UILabel new];
        _tagLabel.textColor = UIColorHex(FFFFFF99);
        _tagLabel.font = MYFONTALL(FONT_REGULAR, 11);
        _tagLabel.backgroundColor = UIColorHex(00000099);
        _tagLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _tagLabel;
}

- (UIImageView *)rightImage {
    if (!_rightImage) {
        _rightImage = [UIImageView new];
        _rightImage.contentMode = UIViewContentModeScaleAspectFill;
        _rightImage.clipsToBounds = YES;
        _rightImage.layer.cornerRadius = 4;
        _rightImage.userInteractionEnabled = YES;
    }
    return _rightImage;
}

- (UIImageView *)videoPlayer {
    if (!_videoPlayer) {
        _videoPlayer = [UIImageView new];
        _videoPlayer.image = [UIImage imageNamed:@"qj_home_play"];
        _videoPlayer.hidden = YES;
        _videoPlayer.userInteractionEnabled = YES;
    }
    return _videoPlayer;
}

- (UIButton *)videoButton {
    if (!_videoButton) {
        _videoButton = [UIButton new];
        _videoButton.hidden = YES;
        [_videoButton addTarget:self action:@selector(videoPlay) forControlEvents:UIControlEventTouchUpInside];
    }
    return _videoButton;
}

- (UIView *)lineView {
    if (!_lineView) {
        _lineView = [UIView new];
        _lineView.backgroundColor = UIColorHex(EBEDF0);
    }
    return _lineView;
}

- (QJImageButton *)likeButton {
    if (!_likeButton) {
        _likeButton = [[QJImageButton alloc] init];
        _likeButton.imageSize = CGSizeMake(16, 16);
        _likeButton.space = 2;
        _likeButton.iconString = @"likeIcon";
        _likeButton.titleString = @"0";
        _likeButton.titleLb.font = MYFONTALL(FONT_REGULAR, 11);
        _likeButton.titleLb.textColor = UIColorHex(919599);
        _likeButton.style = ImageButtonStyleLeft;
        [_likeButton addTarget:self action:@selector(likeClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _likeButton;
}

- (QJImageButton *)watchButton {
    if (!_watchButton) {
        _watchButton = [[QJImageButton alloc] init];
        _watchButton.imageSize = CGSizeMake(16, 16);
        _watchButton.space = 4;
        _watchButton.iconString = @"home_preview";
        _watchButton.titleLb.font = MYFONTALL(FONT_REGULAR, 12);
        _watchButton.titleLb.textColor = UIColorHex(919599);
        _watchButton.style = ImageButtonStyleLeft;
        _watchButton.hidden = YES;//不要了
    }
    return _watchButton;
}
#pragma mark - Configure Data
- (void)configureCellWithData:(QJNewsListModel *)model {
    self.dataModel = model;
    self.titleLabel.text = kCheckStringNil(model.infoTitle);
    
//    self.watchButton.titleString = [NSString getConvertTenThousandNumber:model.infoVisitCount];
    self.likeButton.titleString = [NSString getConvertTenThousandNumber:model.infoLikeCount];
    self.tagLabel.text = kCheckStringNil(model.infoTag);
    [self.userView configureHeaderWithData:model];
    if (model.liked) {
        self.likeButton.iconString = @"likeIconSelected";
    } else {
        self.likeButton.iconString = @"likeIcon";
    }
    if ([kCheckStringNil(model.videoAddress) isEqualToString:@""]) {
        _videoPlayer.hidden = YES;
        _videoButton.hidden = YES;
    } else {
        _videoPlayer.hidden = NO;
        _videoButton.hidden = NO;
    }
    NSString *imageString = [kCheckStringNil(model.infoCoverImage) checkImageUrlString];
    NSURL *imageURL = [NSURL URLWithString:imageString];
    [self.rightImage setImageURL:imageURL];
}

- (void)videoPlay {
    if (self.cellDelegate && [self.cellDelegate respondsToSelector:@selector(didVideoButtonWithID:)]) {
        [self.cellDelegate didVideoButtonWithID:kCheckStringNil(self.dataModel.infoId)];
    }
}

- (void)likeClick {

    NSString *campsiteId = kCheckStringNil(self.dataModel.infoId);
    BOOL liked = self.dataModel.liked;
    NSString *action = @"1";
    if (liked) {
        action = @"0";
    }
    QJCampsiteRequest *startRequest = [[QJCampsiteRequest alloc] init];
    [startRequest netWorkPutCampLike:campsiteId type:@"1" action:action];
    [startRequest setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        if (ResponseSuccess) {
            
            NSInteger newsLikeCount = self.dataModel.infoLikeCount;
            if ([action isEqualToString:@"1"]) {
//                [[QJAppTool getCurrentViewController].view makeToast:@"点赞成功"];
                self.dataModel.liked = YES;
                newsLikeCount = newsLikeCount + 1;
                self.dataModel.infoLikeCount = newsLikeCount;
            } else {
//                [[QJAppTool getCurrentViewController].view makeToast:@"取消点赞"];
                self.dataModel.liked = NO;
                newsLikeCount = newsLikeCount - 1;
                self.dataModel.infoLikeCount = newsLikeCount;
            }
            
            if (self.dataModel.liked) {
                self.likeButton.iconString = @"likeIconSelected";
            } else {
                self.likeButton.iconString = @"likeIcon";
            }
            self.likeButton.titleString = [NSString getConvertTenThousandNumber:self.dataModel.infoLikeCount];
        }
    }];
    [startRequest setFailureCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        [QJAppTool hideHUDLoading];
    }];
}

@end
