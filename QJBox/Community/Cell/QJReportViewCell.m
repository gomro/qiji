//
//  QJReportViewCell.m
//  QJBox
//
//  Created by Sun on 2022/7/22.
//

#import "QJReportViewCell.h"
#import "QJImageButton.h"

@interface QJReportViewCell ()
@property (nonatomic, strong) QJImageButton *reportItem;
@property (nonatomic, strong) QJReportModel *dataModel;
@end
@implementation QJReportViewCell

- (QJImageButton *)reportItem {
    if (!_reportItem) {
        _reportItem = [QJImageButton new];
        _reportItem.titleString = @"0";
        _reportItem.titleLb.font = MYFONTALL(FONT_BOLD, 14);
        _reportItem.titleLb.textColor = UIColorHex(000000);
        _reportItem.iconString = @"reportUnselected";
        _reportItem.imageSize = CGSizeMake(16, 16);
        _reportItem.space = 16;
        _reportItem.style = ImageButtonStyleLeft;
        [_reportItem addTarget:self action:@selector(chooseItem:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _reportItem;
}


#pragma mark - init
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
//        self.contentView.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:self.reportItem];
        [self makeConstraints];
    }
    return self;
}

- (void)makeConstraints {
    [self.reportItem mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView.mas_left);
        make.centerY.equalTo(self.contentView);
        make.height.mas_equalTo(16);
    }];
}

- (void)configureCellWithData:(QJReportModel *)model {
    self.dataModel = model;
    _reportItem.titleString = kCheckStringNil(model.descriptionStr);
    if (self.dataModel.isSelected) {
        _reportItem.iconString = @"reportSelected";
    } else {
        _reportItem.iconString = @"reportUnselected";
    }
}

- (void)chooseItem:(UIButton *)sender {
    self.dataModel.isSelected = !self.dataModel.isSelected;
    if (self.clickButton) {
        self.clickButton(kCheckStringNil(self.dataModel.reportId), self.dataModel.isSelected);
    }
    
}

@end
