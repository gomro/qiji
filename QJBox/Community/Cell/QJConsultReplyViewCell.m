//
//  QJConsultReplyViewCell.m
//  QJBox
//
//  Created by Sun on 2022/7/12.
//

#import "QJConsultReplyViewCell.h"
#import "QJCommentReplyModel.h"
@interface QJConsultReplyViewCell ()

/** 文本内容 */
@property (nonatomic , weak) YYLabel *contentLabel;

@end
@implementation QJConsultReplyViewCell


- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}
+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *ID = @"CommentCell";
    QJConsultReplyViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[self alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
    }
    return cell;
}


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self)
    {
        // 初始化
        [self _setup];
        
        // 创建自控制器
        [self _setupSubViews];
        
        // 布局子控件
        [self _makeSubViewsConstraints];
        
        UITapGestureRecognizer *longTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapReplyMainComment:)];
        [self.contentLabel addGestureRecognizer:longTap];
        
    }
    
    return self;
}




#pragma mark - 公共方法
- (void)setCommentFrame:(QJCommentReplyFrame *)commentFrame {
    _commentFrame = commentFrame;
    
    QJCommentReplyModel *comment = commentFrame.comment;
    
    // 赋值
    self.contentLabel.frame = commentFrame.textFrame;
    // 设置值
    self.contentLabel.attributedText = comment.attributedText;
    
}


#pragma mark - 私有方法
#pragma mark - 初始化
- (void)_setup {
    // 设置颜色
    self.backgroundColor = UIColorHex(F5F5F5);
    self.contentView.backgroundColor = UIColorHex(F5F5F5);
}

#pragma mark - 创建自控制器
- (void)_setupSubViews
{
    // 文本
    YYLabel *contentLabel = [[YYLabel alloc] init];
    contentLabel.numberOfLines = 0 ;
    contentLabel.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:contentLabel];
    self.contentLabel = contentLabel;
    
    __weak typeof(self) weakSelf = self;
    contentLabel.highlightTapAction = ^(UIView *containerView, NSAttributedString *text, NSRange range, CGRect rect) {
        
        // 利用KVC获取UserInfo 其实可以在MHComment模型里面利用 通知告知控制器哪个用户被点击了
        YYTextHighlight *highlight = [containerView valueForKeyPath:@"_highlight"];
        
        
    };
}


#pragma mark - 布局子控件
- (void)_makeSubViewsConstraints
{
    
}

- (void)tapReplyMainComment:(UITapGestureRecognizer*)gesture {
    if (self.cellDelegate && [self.cellDelegate respondsToSelector:@selector(touchConsultReplyComment:name:userId:toUser:)]) {
        QJCommentUser *user = [QJCommentUser new];
        user.commentStr = self.commentFrame.comment.content?:@"";
        user.avatarUrl = [self.commentFrame.comment.coverImage checkImageUrlString];
         
        [self.cellDelegate touchConsultReplyComment:self.commentFrame.comment.commentId name:self.commentFrame.comment.userName userId:self.commentFrame.comment.userId toUser:user];
    }
}
@end
