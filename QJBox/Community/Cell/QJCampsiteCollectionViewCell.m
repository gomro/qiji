//
//  QJCampsiteCollectionViewCell.m
//  QJBox
//
//  Created by Sun on 2022/7/5.
//

#import "QJCampsiteCollectionViewCell.h"
@interface QJCampsiteCollectionViewCell ()
@property (nonatomic, strong) QJCampsiteListModel *model;
@property (nonatomic, assign) BOOL isFromEdit;
@property (nonatomic, strong) NSMutableArray *myCampsiteArr;

@end
#define angle2Rad(angle) ((angle) / 180.0 * M_PI)

@implementation QJCampsiteCollectionViewCell

- (UIImageView *)imageView {
    if (!_imageView) {
        _imageView = [UIImageView new];
        _imageView.userInteractionEnabled = YES;
        _imageView.layer.masksToBounds = YES;
        _imageView.layer.cornerRadius = 8;
    }
    return _imageView;
}

- (UILabel *)title {
    if (!_title) {
        _title = [UILabel new];
        _title.font = MYFONTALL(FONT_REGULAR, 12);
        _title.textColor = UIColorHex(000000);
        _title.textAlignment = NSTextAlignmentCenter;
    }
    return _title;
}

- (UIButton *)addButton {
    if (!_addButton) {
        _addButton = [UIButton new];
        _addButton.hidden = YES;
        [_addButton addTarget:self action:@selector(chooseItem:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _addButton;
}

- (NSMutableArray *)myCampsiteArr {
    if (!_myCampsiteArr) {
        _myCampsiteArr = [NSMutableArray arrayWithCapacity:0];
    }
    return _myCampsiteArr;
}

#pragma mark - init
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
//        self.contentView.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:self.imageView];
        [self.contentView addSubview:self.title];
        [self.contentView addSubview:self.addButton];
        [self makeConstraints];
    }
    return self;
}

- (void)makeConstraints {
    [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top).offset(8);
        make.centerX.equalTo(self.contentView);
        make.width.height.mas_equalTo(46);
    }];
    
    [self.addButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.imageView.mas_top).offset(-8);
        make.left.equalTo(self.imageView.mas_right).offset(-8);
        make.width.height.mas_equalTo(16);
    }];
    
    [self.title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.imageView.mas_bottom).offset(10);
        make.width.mas_equalTo(62);
        make.centerX.equalTo(self.contentView);
    }];
        
}
- (void)configureCellWithData:(QJCampsiteListModel *)model isEdit:(BOOL)isEdit isFromEdit:(BOOL)isFromEdit {
    NSArray *myCampsite = [QJAppTool shareManager].campsiteArr;
    [self.myCampsiteArr removeAllObjects];
    if (!IsArrEmpty(myCampsite)) {
        for (NSDictionary *dic in myCampsite) {
            QJCampsiteListModel *model = [QJCampsiteListModel modelWithDictionary:dic];
            [self.myCampsiteArr addObject:kCheckStringNil(model.campsiteID)];
        }
    }
    
    self.title.text = kCheckStringNil(model.name);
    NSString *imageString = [kCheckStringNil(model.iconAddress) checkImageUrlString];
    NSURL *imageURL = [NSURL URLWithString:imageString];
    [self.imageView setImageURL:imageURL];
    self.addButton.hidden = YES;
    if (isFromEdit) {
        if (isEdit) {
            self.addButton.hidden = NO;
            [self.addButton setImage:[UIImage imageNamed:@"campsiteReduce"] forState:UIControlStateNormal];
            [self iconAnimation];
            
        } else {
            self.addButton.hidden = YES;
        }
    } else {
        if (isEdit) {
            if ([self.myCampsiteArr containsObject:kCheckStringNil(model.campsiteID)]) {
                self.addButton.hidden = YES;
            } else {
                [self.addButton setImage:[UIImage imageNamed:@"campsiteAdd"] forState:UIControlStateNormal];
                self.addButton.hidden = NO;
            }
        } else {
            self.addButton.hidden = YES;
        }
    }
    self.model = model;
    self.isFromEdit = isFromEdit;
}

- (void)iconAnimation {
    [self.imageView.layer removeAllAnimations];

    //1.创建动画对象
    CAKeyframeAnimation *anim = [CAKeyframeAnimation animation];
    anim.keyPath = @"transform.rotation";
    anim.values = @[@(angle2Rad(-5)),@(angle2Rad(5)),@(angle2Rad(-5))];
    
    //3.设置动画执行次数
    anim.repeatCount =  MAXFLOAT;
    anim.duration = 0.5;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.imageView.layer addAnimation:anim forKey:nil];
    });
}

- (void)chooseItem:(UIButton *)sender {
    NSDictionary *dic = [self.model modelToJSONObject];
    if (self.isFromEdit) {//删除
        [[NSNotificationCenter defaultCenter] postNotificationName:QJEditMyCampsiteList object:nil userInfo:@{@"type":@"0", @"data":dic}];

    } else {//添加
        if (self.myCampsiteArr.count < 20) {
            self.addButton.hidden = YES;
            [[NSNotificationCenter defaultCenter] postNotificationName:QJEditMyCampsiteList object:nil userInfo:@{@"type":@"1", @"data":dic}];
        }
    }
}

@end
