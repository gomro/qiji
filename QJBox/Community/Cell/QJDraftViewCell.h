//
//  QJDraftViewCell.h
//  QJBox
//
//  Created by Sun on 2022/7/22.
//

#import <UIKit/UIKit.h>
#import "QJDraftModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface QJDraftViewCell : UITableViewCell
- (void)configureCellWithData:(QJDraftModel *)model;

@end

NS_ASSUME_NONNULL_END
