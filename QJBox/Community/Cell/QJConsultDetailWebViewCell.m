//
//  QJConsultDetailWebViewCell.m
//  QJBox
//
//  Created by Sun on 2022/8/8.
//

#import "QJConsultDetailWebViewCell.h"

@interface QJConsultDetailWebViewCell ()<WKUIDelegate,WKNavigationDelegate>

@property (nonatomic, strong) WKWebView *webView;
@property (nonatomic, assign) CGFloat height;

@end

@implementation QJConsultDetailWebViewCell


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self.contentView addSubview:self.webView];
        self.height = 0;
        /* 添加约束 */
        [self makeConstraints];
    }
    return self;
}

- (void)dealloc{
    [_webView.scrollView removeObserver:self forKeyPath:@"contentSize"];
}

- (void)makeConstraints{
    [self.webView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(self.contentView);
        make.left.equalTo(self.contentView).offset(10);
        make.right.equalTo(self.contentView).offset(-8);
        make.height.mas_equalTo(40);
    }];
}


- (void)configureCellWithDataModel:(NSString *)htmlContent {
    NSString *headerString = @"<header><meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no'><style>img{max-width:100%, border-radius:20px}</style></header>";
    [self.webView loadHTMLString:[headerString stringByAppendingString:kCheckStringNil(htmlContent)] baseURL:nil];
        
}

#pragma mark - Lazy Loading
- (WKWebView *)webView{
    if (!_webView) {
        NSString *jScript = @"var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport'); meta.setAttribute('content', 'width=device-width'); document.getElementsByTagName('head')[0].appendChild(meta); var imgs = document.getElementsByTagName('img');for (var i in imgs){imgs[i].style.maxWidth='100%';imgs[i].style.height='auto';}";
        WKUserScript *wkUScript = [[WKUserScript alloc] initWithSource:jScript injectionTime:WKUserScriptInjectionTimeAtDocumentEnd forMainFrameOnly:YES];
        WKUserContentController *wkUController = [[WKUserContentController alloc] init];
        [wkUController addUserScript:wkUScript];
        WKWebViewConfiguration *wkWebConfig = [[WKWebViewConfiguration alloc] init];
        wkWebConfig.userContentController = wkUController;
        _webView = [[WKWebView alloc] initWithFrame:CGRectZero configuration:wkWebConfig];        
        _webView.navigationDelegate = self;
        _webView.UIDelegate=self;
        _webView.scrollView.scrollEnabled = NO;
        [_webView.scrollView addObserver:self forKeyPath:@"contentSize" options:NSKeyValueObservingOptionNew context:nil];
    }
    return _webView;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context{

}



#pragma mark WKWebView
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    NSString *js1 = [NSString stringWithFormat:@"document.documentElement.style.webkitUserSelect='none';"];
    NSString *js2 = [NSString stringWithFormat:@"document.documentElement.style.webkitTouchCallout='none';"];
    [self.webView evaluateJavaScript:js1 completionHandler:nil];
    [self.webView evaluateJavaScript:js2 completionHandler:nil];

    MJWeakSelf
    [webView evaluateJavaScript:@"document.body.scrollHeight"
              completionHandler:^(id result, NSError *_Nullable error) {
       
        CGFloat newHeight = weakSelf.webView.scrollView.contentSize.height;
        if (weakSelf.height && newHeight == weakSelf.height) {
            return;
        }
        self.height = newHeight;
        weakSelf.webView.frame=CGRectMake(0, 0, QJScreenWidth, newHeight);
        [self.webView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(newHeight);
        }];
        weakSelf.webView.height = newHeight;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            if (weakSelf.delegate && [weakSelf.delegate respondsToSelector:@selector(changeWebViewHeight:)]) {
                [weakSelf.delegate changeWebViewHeight:newHeight];
            }
        });
      
    }];
    
}

- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(nonnull WKNavigationAction *)navigationAction decisionHandler:(nonnull void (^)(WKNavigationActionPolicy))decisionHandler {
//    NSString *requestString = [navigationAction.request.URL.absoluteString stringByRemovingPercentEncoding];
//    NSArray *components = [requestString componentsSeparatedByString:@":"];
    decisionHandler(WKNavigationActionPolicyAllow);
}

@end
