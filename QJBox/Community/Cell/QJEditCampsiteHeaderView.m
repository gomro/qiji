//
//  QJEditCampsiteHeaderView.m
//  QJBox
//
//  Created by Sun on 2022/7/6.
//

#import "QJEditCampsiteHeaderView.h"
#import "QJCampsiteCollectionViewCell.h"
#import "QJCampsiteListModel.h"
#import "QJCampsiteRequest.h"
@interface QJEditCampsiteHeaderView ()<UICollectionViewDelegate, UICollectionViewDataSource>
/**标题*/
@property (nonatomic, strong) UILabel *titleLabel;
/**编辑按钮*/
@property (nonatomic, strong) UIButton *editBt;

@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) NSMutableArray *dataSource;

@property (nonatomic, assign) BOOL isEdit;

@property (nonatomic, strong) UIImageView *emptyImage;
@end

@implementation QJEditCampsiteHeaderView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:self.titleLabel];
        [self addSubview:self.editBt];
        [self addSubview:self.collectionView];
        [self addSubview:self.emptyImage];
        [self makeConstraints];
        self.isEdit = NO;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeData:) name:QJEditMyCampsiteList object:nil];
    }
    return self;
}

- (void)makeConstraints {
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).offset(5);
        make.left.equalTo(self.mas_left).offset(20);
    }];
    
    [self.editBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.titleLabel);
        make.right.equalTo(self.mas_right).offset(-20);
    }];
    
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).offset(5);
        make.left.right.equalTo(self);
        make.height.mas_equalTo(110);
    }];
    [self.emptyImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(26);
        make.centerY.equalTo(self.collectionView);
        make.width.height.mas_equalTo(46);
    }];
    
    self.titleLabel.text = @"我的营地";
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.font = MYFONTALL(FONT_BOLD, 16);
        _titleLabel.textColor = UIColorHex(16191C);
    }
    return _titleLabel;
}

- (UIButton *)editBt {
    if (!_editBt) {
        _editBt = [UIButton new];
        [_editBt setTitle:@"编辑" forState:UIControlStateNormal];
        [_editBt setTitleColor:UIColorHex(007AFF) forState:UIControlStateNormal];
        _editBt.titleLabel.font = MYFONTALL(FONT_REGULAR, 14);
        [_editBt addTarget:self action:@selector(editCampsite:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _editBt;
}
- (NSMutableArray *)dataSource {
    if (!_dataSource) {
        _dataSource = [NSMutableArray arrayWithCapacity:0];
    }
    return _dataSource;
}

- (UIImageView *)emptyImage {
    if (!_emptyImage) {
        _emptyImage = [UIImageView new];
        _emptyImage.image = [UIImage imageNamed:@"campsiteEmptyAdd"];
        _emptyImage.hidden = YES;
    }
    return _emptyImage;
}

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.sectionInset = UIEdgeInsetsMake(0, 16, 0, 16);
        layout.itemSize = CGSizeMake(62, 84);
        layout.minimumLineSpacing = 18;
        layout.minimumInteritemSpacing = (kScreenWidth - 281)/ 3.0;
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.backgroundColor = [UIColor clearColor];
        [_collectionView registerClass:[QJCampsiteCollectionViewCell class] forCellWithReuseIdentifier:@"kIdentifierCampsiteCell"];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.showsHorizontalScrollIndicator = NO;

    }
    return _collectionView;
}

#pragma mark - collection
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataSource.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    QJCampsiteCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"kIdentifierCampsiteCell" forIndexPath:indexPath];
    
    QJCampsiteListModel *model = self.dataSource[indexPath.item];
    [cell configureCellWithData:model isEdit:self.isEdit isFromEdit:YES];
    
    return cell;
}

- (void)configureHeaderEditWithData:(NSArray *)dataArr {
    [self.dataSource removeAllObjects];
    [self.dataSource addObjectsFromArray:dataArr];
    [self.collectionView reloadData];
    [self showEmptyImage];
    self.titleLabel.text = [NSString stringWithFormat:@"我的营地 %ld/20", dataArr.count];
}

- (void)editCampsite:(UIButton *)sender {
    self.isEdit = !self.isEdit;
    [self.collectionView reloadData];
    [[NSNotificationCenter defaultCenter] postNotificationName:QJEditMyCampsiteType object:nil userInfo:@{@"type":@(self.isEdit)}];
    if (self.isEdit) {
        [self.editBt setTitle:@"保存" forState:UIControlStateNormal];
    } else {
        [self.editBt setTitle:@"编辑" forState:UIControlStateNormal];
        [self saveData:YES isPushNotification:NO];
    }
}

- (void)changeData:(NSNotification *)notification {
    NSDictionary *userInfo = notification.userInfo;
    NSString *type = userInfo[@"type"];
    NSDictionary *dataDic = userInfo[@"data"];
    QJCampsiteListModel *model = [QJCampsiteListModel modelWithDictionary:dataDic];
    if ([type isEqualToString:@"0"]) {
        NSMutableArray *dataArr = [self.dataSource mutableCopy];
        for (QJCampsiteListModel *dataModel in dataArr) {
            if ([dataModel.campsiteID isEqualToString:model.campsiteID]) {
                [self.dataSource removeObject:dataModel];
            }
        }
    } else {
        if(self.dataSource.count > 0){
            BOOL isContain = NO;
            for (QJCampsiteListModel *a in self.dataSource.copy) {
                if([a.campsiteID isEqualToString:model.campsiteID]){
                    isContain = YES;
                }
            }
            if(!isContain){
                [self.dataSource safeAddObject:model];
            }
           
        }else{
            [self.dataSource safeAddObject:model];
        }
       
    }
    self.titleLabel.text = [NSString stringWithFormat:@"我的营地 %ld/20", self.dataSource.count];
    [self showEmptyImage];
    if ([type isEqualToString:@"2"]) {
        [QJAppTool shareManager].campsiteID = kCheckStringNil(model.campsiteID);
        [self saveData:YES isPushNotification:YES];
    } else {
        [self saveData:NO isPushNotification:NO];
    }
    [self.collectionView reloadData];
}

- (void)saveData:(BOOL)save isPushNotification:(BOOL)isPushNotification {
    NSMutableArray *dataArr = [NSMutableArray arrayWithCapacity:0];
    NSMutableArray *idArr = [NSMutableArray arrayWithCapacity:0];
    for (QJCampsiteListModel *dataModel in self.dataSource) {
        NSDictionary *dic = [dataModel modelToJSONObject];
        [dataArr addObject:dic];
        [idArr addObject:dataModel.campsiteID];
    }    
    [QJAppTool shareManager].campsiteArr = dataArr;
    if(save) {
        [self chageListNetWork:[idArr copy] isPushNotification:isPushNotification];
        LSUserDefaultsSET([dataArr copy], @"kMyCampsiteList");
    }
}

- (void)showEmptyImage {
    if (self.dataSource.count == 0) {
        self.emptyImage.hidden = NO;
    } else {
        self.emptyImage.hidden = YES;
    }
}

- (void)chageListNetWork:(NSArray *)dataArr isPushNotification:(BOOL)isPushNotification {
    QJCampsiteRequest *startRequest = [[QJCampsiteRequest alloc] init];
    [startRequest netWorkPostCampSet:dataArr];
    [startRequest setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            if(self.clickCampsite && isPushNotification) {
                self.clickCampsite();
            }
            [[NSNotificationCenter defaultCenter] postNotificationName:QJMyCampsiteListRefresh object:nil userInfo:nil];
        }
    }];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:QJEditMyCampsiteList object:nil];
}
@end
