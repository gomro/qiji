//
//  QJCampsiteUserView.h
//  QJBox
//
//  Created by Sun on 2022/7/7.
//

#import <UIKit/UIKit.h>
#import "QJCampsiteDetailModel.h"
#import "QJCommentTopModel.h"
#import "QJNewsListModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface QJCampsiteUserView : UIView
- (void)configureHeaderlWithData:(QJCampsiteDetailModel *)model;

- (void)configureHeaderlCommentWithData:(QJCommentTopModel *)model;


- (void)configureHeaderNewsWithData:(QJNewsListModel *)model;

@end

NS_ASSUME_NONNULL_END
