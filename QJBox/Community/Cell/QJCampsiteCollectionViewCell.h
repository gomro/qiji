//
//  QJCampsiteCollectionViewCell.h
//  QJBox
//
//  Created by Sun on 2022/7/5.
//

#import <UIKit/UIKit.h>
#import "QJCampsiteListModel.h"
NS_ASSUME_NONNULL_BEGIN
@interface QJCampsiteCollectionViewCell : UICollectionViewCell
/**背景图片*/
@property (nonatomic, strong) UIImageView *imageView;
/**标题*/
@property (nonatomic, strong) UILabel *title;
/**添加按钮*/
@property (nonatomic, strong) UIButton *addButton;


- (void)configureCellWithData:(QJCampsiteListModel *)model isEdit:(BOOL)isEdit isFromEdit:(BOOL)isFromEdit;

@end

NS_ASSUME_NONNULL_END
