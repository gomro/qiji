//
//  QJReportViewCell.h
//  QJBox
//
//  Created by Sun on 2022/7/22.
//

#import <UIKit/UIKit.h>
#import "QJReportModel.h"
NS_ASSUME_NONNULL_BEGIN

typedef void (^QJReportViewCellButtonClick)(NSString *reportId, BOOL selected);

@interface QJReportViewCell : UICollectionViewCell
@property (nonatomic, copy) QJReportViewCellButtonClick clickButton;

- (void)configureCellWithData:(QJReportModel *)model;
@end

NS_ASSUME_NONNULL_END
