//
//  QJImageButton.m
//  QJBox
//
//  Created by Sun on 2022/7/7.
//

#import "QJImageButton.h"

@interface QJImageButton ()
@property (nonatomic, strong) UIImageView *iconImage;
@end

@implementation QJImageButton

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        // 默认 文字和图片的间距是 0
        _space = 0.0f;
        _imageSize = CGSizeMake(20, 20);
        _delta = 0;
        [self addSubview:self.iconImage];
        [self addSubview:self.titleLb];
    }
    return self;
}

#pragma mark - set
- (void)setStyle:(ImageButtonStyle)style {
    _style = style;
    [self makeConstraints];
}

- (void)setImageSize:(CGSize)imageSize {
    _imageSize = imageSize;
}

- (void)setDelta:(CGFloat)delta {
    _delta = delta;
}
- (void)setSpace:(CGFloat)space {
    _space = space;
}


- (void)makeConstraints {
    if (self.style == ImageButtonStyleTop) {
        [self.iconImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self);
            make.top.equalTo(self.mas_top).offset(self.delta);
            make.size.mas_equalTo(self.imageSize);
        }];
        
        [self.titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self);
            make.top.equalTo(self.iconImage.mas_bottom).offset(self.space);
            make.bottom.equalTo(self.mas_bottom);
        }];
    } else if (self.style == ImageButtonStyleLeft) {
        [self.iconImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self);
            make.left.equalTo(self.mas_left).offset(self.delta);
            make.size.mas_equalTo(self.imageSize);
        }];
        
        [self.titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self);
            make.left.equalTo(self.iconImage.mas_right).offset(self.space);
            make.right.equalTo(self.mas_right);
        }];
    } else if (self.style == ImageButtonStyleRight) {
        [self.titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self);
            make.left.equalTo(self.mas_left).offset(self.delta);
        }];
        [self.iconImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self);
            make.size.mas_equalTo(self.imageSize);
            make.left.equalTo(self.titleLb.mas_right).offset(self.space);
            make.right.equalTo(self.mas_right);
        }];
            
    } else if (self.style == ImageButtonStyleBottom) {
        [self.titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self);
            make.top.equalTo(self.mas_top).offset(self.delta);
        }];
        [self.iconImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self);
            make.top.equalTo(self.titleLb.mas_bottom).offset(self.space);
            make.size.mas_equalTo(self.imageSize);
            make.bottom.equalTo(self.mas_bottom);
        }];
        
     
    }
}

    
- (UIImageView *)iconImage {
    if (!_iconImage) {
        self.iconImage = [UIImageView new];
    }
    return _iconImage;
}
- (UILabel *)titleLb {
    if (!_titleLb) {
        self.titleLb = [UILabel new];
        self.titleLb.font = kboldFont(13);
        self.titleLb.textColor = UIColorHex(0x151D2F);
    }
    return _titleLb;
}


- (void)setTitleString:(NSString *)titleString {
    _titleString = titleString;
    self.titleLb.text = titleString;
}

- (void)setIconString:(NSString *)iconString {
    _iconString = iconString;
    self.iconImage.image = [UIImage imageNamed:iconString];
}

- (CGRect)imageRectForContentRect:(CGRect)contentRect {
    
    return CGRectZero;
}

- (CGRect)titleRectForContentRect:(CGRect)contentRect {
    return CGRectZero;

}
@end
