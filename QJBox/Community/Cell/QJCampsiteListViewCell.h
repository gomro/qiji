//
//  QJCampsiteListViewCell.h
//  QJBox
//
//  Created by Sun on 2022/7/7.
//

#import <UIKit/UIKit.h>
#import "QJCampsiteDetailModel.h"

typedef NS_ENUM(NSInteger, QJCampsiteListCellStyle){
    QJCampsiteListCellStyleDefault,    // 仅文字
    QJCampsiteListCellStyleImage,      // 文字加图片
    QJCampsiteListCellStyleVideo,      // 文字加视频
};

@protocol QJCampsiteListViewCellDelegate <NSObject>
/**点击评论*/
- (void)didCommentButtonWithID:(NSString *)campsiteID;
/**点击头像*/
- (void)didClickUserView:(NSString *)userId;

/**主页数据刷新*/
- (void)didRefreshListView:(NSString *)code;

@end

@interface QJCampsiteListViewCell : UITableViewCell

/**初始化*/
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier cellStyle:(QJCampsiteListCellStyle)cellStyle;

/**赋值*/
- (void)configureCellWithData:(QJCampsiteDetailModel *)model;

/**代理*/
@property (nonatomic, weak) id<QJCampsiteListViewCellDelegate> cellDelegate;


@property (nonatomic, assign) BOOL isFromUserSpaceVC;//是否来自个人空间页面

@property (nonatomic, copy) void(^clickMoreBtnBlock)(QJCampsiteDetailModel *model);



@end

