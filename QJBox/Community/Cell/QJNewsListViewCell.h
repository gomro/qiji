//
//  QJNewsListViewCell.h
//  QJBox
//
//  Created by Sun on 2022/7/28.
//

#import <UIKit/UIKit.h>
#import "QJNewsListModel.h"

NS_ASSUME_NONNULL_BEGIN

@protocol QJNewsListViewCellDelegate <NSObject>
/**点击右边视频按钮*/
- (void)didVideoButtonWithID:(NSString *)newsID;
@end

@interface QJNewsListViewCell : UITableViewCell
- (void)configureCellWithData:(QJNewsListModel *)model;
/**代理*/
@property (nonatomic, weak) id<QJNewsListViewCellDelegate> cellDelegate;
@end

NS_ASSUME_NONNULL_END
