//
//  QJCommunityDraftManager.h
//  QJBox
//
//  Created by wxy on 2022/8/5.
//

#import <Foundation/Foundation.h>

#define QJCommunityDraftCacheFilePath [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).firstObject stringByAppendingPathComponent:@"QJCommunityDraftCacheFilePath"]

#define QJCommunityDraftModelArrKey [NSString stringWithFormat:@"QJCommunityDraftModelArrKey_%@",[QJUserManager shareManager].userID]

#define QJCommunityPublishModelArrKey [NSString stringWithFormat:@"QJCommunityPublishModelArrKey_%@",[QJUserManager shareManager].userID]



NS_ASSUME_NONNULL_BEGIN

@class QJCommunityLocalDraftModel,QJCommunityDraftModel;
/// 管理草稿箱的对象
@interface QJCommunityDraftManager : NSObject

@property (nonatomic, strong) NSMutableArray <QJCommunityDraftModel *> * modelArr;

@property (nonatomic, strong) NSMutableArray <QJCommunityDraftModel *> * publishModelArr;

-(instancetype)init NS_UNAVAILABLE;

+(instancetype)new NS_UNAVAILABLE;

+(instancetype)allocWithZone:(struct _NSZone *)zone NS_UNAVAILABLE;

+ (instancetype)shared;

//添加对象
- (void)addModel:(QJCommunityDraftModel *)model;

//批量添加对象
- (void)addModelArr:(NSArray<QJCommunityDraftModel*>*)arr;


//批量删除草稿
- (void)deleteModelArr:(NSIndexSet *)sets;

//读取本地数据
- (void)readLocalDraftData;


//添加已经发布对象
- (void)addPublishModel:(QJCommunityDraftModel *)model;

//批量删除本地发布数据
- (void)deletePublishModelArr:(NSIndexSet *)sets;

//读取本地发布数据
- (void)readLocalPublishData;

@end

NS_ASSUME_NONNULL_END
