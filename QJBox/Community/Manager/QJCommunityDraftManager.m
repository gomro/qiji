//
//  QJCommunityDraftManager.m
//  QJBox
//
//  Created by wxy on 2022/8/5.
//

#import "QJCommunityDraftManager.h"
#import "QJCommunityLocalDraftModel.h"
#import "QJCommunityDraftModel.h"
@interface   QJCommunityDraftManager()

@property (nonatomic, strong) YYCache *cache;

@end


@implementation QJCommunityDraftManager


+ (instancetype)shared {
    static QJCommunityDraftManager *_instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [[QJCommunityDraftManager alloc] init];
    });
    return _instance;
}


//添加对象
- (void)addModel:(QJCommunityDraftModel *)model {
    //保存到内存
    if (self.modelArr.count == 0) {
        self.modelArr = [NSMutableArray array];
    }
    
    [self.modelArr insertObject:model atIndex:0];
    
    //保存到磁盘
    NSMutableArray *dataArr = (NSMutableArray *)[self.cache objectForKey:QJCommunityDraftModelArrKey];
    if (!dataArr) {
        dataArr = [NSMutableArray array];
    }
    
    
    QJCommunityLocalDraftModel *localModel = [self modelChangeFromModel:model];
    
    [dataArr insertObject:localModel atIndex:0];
    [self.cache setObject:dataArr forKey:QJCommunityDraftModelArrKey];
}


//批量添加对象
- (void)addModelArr:(NSArray<QJCommunityDraftModel*>*)arr {
    //保存到内存
    if (self.modelArr.count == 0) {
        self.modelArr = [NSMutableArray array];
    }
    
    [self.modelArr insertObjects:arr atIndex:0];
    
    //保存到磁盘
    NSMutableArray *dataArr = (NSMutableArray *)[self.cache objectForKey:QJCommunityDraftModelArrKey];
    if (!dataArr) {
        dataArr = [NSMutableArray array];
    }
    
    NSMutableArray *muLocalModels = [NSMutableArray array];
    for (QJCommunityDraftModel *model in arr) {
        QJCommunityLocalDraftModel *localModel = [self modelChangeFromModel:model];
        [muLocalModels safeAddObject:localModel];
    }
    
    
    [dataArr insertObjects:muLocalModels atIndex:0];
    [self.cache setObject:dataArr forKey:QJCommunityDraftModelArrKey];
}



//批量删除草稿
- (void)deleteModelArr:(NSIndexSet *)sets {
    if (self.modelArr) {//内存有的话先删除内存
        
        [self.modelArr removeObjectsAtIndexes:sets];
    }
    //删除磁盘
    NSMutableArray *dataArr = (NSMutableArray *)[self.cache objectForKey:QJCommunityDraftModelArrKey];
    if (dataArr) {
        [dataArr removeObjectsAtIndexes:sets];
        [self.cache setObject:dataArr forKey:QJCommunityDraftModelArrKey];
    }
    
    
}

//读取本地数据
- (void)readLocalDraftData {
    
    NSMutableArray *dataArr = (NSMutableArray *)[self.cache objectForKey:QJCommunityDraftModelArrKey];
    self.modelArr = [NSMutableArray array];
    for (QJCommunityLocalDraftModel *localModel in dataArr.copy) {
        QJCommunityDraftModel *model = [self modelChangeFromLocalModel:localModel];
        [self.modelArr safeAddObject:model];
    }
    
}

- (void)setModelArr:(NSMutableArray<QJCommunityDraftModel *> *)modelArr {
    _modelArr = modelArr;
    DLog(@"modelArr.count = %ld",modelArr.count);
}


//添加已经发布对象
- (void)addPublishModel:(QJCommunityDraftModel *)model {
    //保存到内存
    if (self.publishModelArr.count == 0) {
        self.publishModelArr = [NSMutableArray array];
    }
    [self.publishModelArr insertObject:model atIndex:0];
    //保存到磁盘
    NSMutableArray *dataArr = (NSMutableArray *)[self.cache objectForKey:QJCommunityPublishModelArrKey];
    if (!dataArr) {
        dataArr = [NSMutableArray array];
    }
    QJCommunityLocalDraftModel *localModel = [self modelChangeFromModel:model];
    
    [dataArr insertObject:localModel atIndex:0];
    [self.cache setObject:dataArr forKey:QJCommunityPublishModelArrKey];
    
}


//批量删除本地发布数据
- (void)deletePublishModelArr:(NSIndexSet *)sets {
    if (self.publishModelArr) {//内存有的话先删除内存
        [self.publishModelArr removeObjectsAtIndexes:sets];
    }
    //删除磁盘
    NSMutableArray *dataArr = (NSMutableArray *)[self.cache objectForKey:QJCommunityPublishModelArrKey];
    if (dataArr) {
        [dataArr removeObjectsAtIndexes:sets];
        [self.cache setObject:dataArr forKey:QJCommunityPublishModelArrKey];
    }
    
    
}

//读取本地发布数据
- (void)readLocalPublishData {
    
    NSMutableArray *dataArr = (NSMutableArray *)[self.cache objectForKey:QJCommunityPublishModelArrKey];
    self.publishModelArr = [NSMutableArray array];
    for (QJCommunityLocalDraftModel *localModel in dataArr.copy) {
        QJCommunityDraftModel *model = [self modelChangeFromLocalModel:localModel];
        [self.publishModelArr safeAddObject:model];
    }
    
    
}



- (NSString *)saveImage:(UIImage*)image ToDocmentWithFileName:(NSString*)fileName{
    NSString *name = [NSString stringWithFormat:@"%@.png",[NSString getRandomFileName]];
    NSString *filePath = [fileName stringByAppendingString:name];
    // 保存成功会返回YES
   BOOL success = [UIImagePNGRepresentation(image) writeToFile:filePath atomically:YES];
    if (success) {
        return name;
    }else{
        return @"";
    }
    
}

//内存对象转成保存对象
- (QJCommunityLocalDraftModel *)modelChangeFromModel:(QJCommunityDraftModel *)model {
    QJCommunityLocalDraftModel *localModel = [QJCommunityLocalDraftModel new];
    localModel.type = model.type;
    localModel.campId = model.campId;
    if (model.overImage) {
        NSString *file = [QJCommunityDraftCacheFilePath stringByAppendingPathComponent: [NSString stringWithFormat:@"image"]];
        
        file =  [self saveImage:model.overImage ToDocmentWithFileName:file];
        localModel.overImagePath = file;
    }
    if (model.viderUrl) {
        
        localModel.viderUrl = model.viderUrl;
    }
    if (model.title.length > 0) {
        localModel.title = model.title;
    }
    
    if (model.richAttributedString.length > 0) {
//        NSString *dataPath = [QJCommunityDraftCacheFilePath stringByAppendingPathComponent:@"attributedPath"];
//        dataPath = [dataPath stringByAppendingString:[NSString getRandomFileName]];
//        NSData *data = [model.richAttributedString dataFromRange:NSMakeRange(0, model.richAttributedString.length) documentAttributes:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} error:nil];
//        BOOL result = [data writeToFile:dataPath atomically:YES];
//        DLog(@"result = %d",result);
//        localModel.attributedStringPath = dataPath;
        localModel.richAttributedString = model.richAttributedString;
    }
    localModel.isOrignal = model.isOrignal;
    localModel.isForward = model.isForward;
    localModel.timesp = model.timesp;
    localModel.tag = model.tag;
    localModel.idStr = model.idStr;
    
    
    return localModel;
    
}

//保存对象转成内存对象
- (QJCommunityDraftModel *)modelChangeFromLocalModel:(QJCommunityLocalDraftModel *)localModel {
    QJCommunityDraftModel *model = [QJCommunityDraftModel new];
    model.type = localModel.type;
    model.campId = localModel.campId;
    model.isOrignal = localModel.isOrignal;
    model.isForward = localModel.isForward;
    model.timesp = localModel.timesp;
    model.tag = localModel.tag;
    model.idStr = localModel.idStr;
    if (localModel.overImagePath.length > 0) {
        NSString *file = [QJCommunityDraftCacheFilePath stringByAppendingPathComponent: [NSString stringWithFormat:@"image"]];
        NSString *filePath = [file stringByAppendingString:localModel.overImagePath];
        NSData *overData = [NSData dataWithContentsOfFile:filePath];
        UIImage *image = [UIImage imageWithData:overData];
        model.overImage = image;
    }
    model.title = localModel.title;
    model.viderUrl = localModel.viderUrl;
    model.richAttributedString = localModel.richAttributedString;
//    if (localModel.attributedStringPath.length > 0) {
//        NSData *data = [NSData dataWithContentsOfFile:localModel.attributedStringPath];
//        NSDictionary *options = @{NSDocumentTypeDocumentAttribute : NSHTMLTextDocumentType};
//        NSAttributedString *attributed = [[NSAttributedString alloc]initWithData:data options:options documentAttributes:nil error:nil];
//        model.richAttributedString = attributed;
//    }
    
    return model;
}

#pragma mark ----- setter && getter

- (YYCache *)cache {
    if (!_cache) {
        _cache = [[YYCache alloc]initWithPath:QJCommunityDraftCacheFilePath];
    }
    return _cache;
}



@end
