//
//  QJCommentInputToolbar.m
//  QJBox
//
//  Created by Sun on 2022/7/14.
//

#import "QJCommentInputToolbar.h"
#import "ICMessageConst.h"
#import "XZEmotion.h"
#import "NSString+Extension.h"

@interface QJCommentInputToolbar ()<UITextViewDelegate>

/**边框*/
@property (nonatomic, strong) UIView *edgeLineView;
/**textView占位符*/
@property (nonatomic, strong) UILabel *placeholderLabel;


/**发送回调*/
@property (nonatomic, copy) inputTextBlock inputTextBlock;
/**字数提示*/
@property (nonatomic, strong) UILabel *textNum;

@property (nonatomic, strong) UIView *userBgView;

@property (nonatomic, strong) UIImageView *avaterImageView;

@property (nonatomic, strong) UILabel *userContentLabel;

@property (nonatomic, strong) UIButton *sendBtn;//发送

@end
@implementation QJCommentInputToolbar

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.frame = CGRectMake(0, kScreenHeight, kScreenWidth, 56);
        [self initView];
        [self addNotification];

    }
    return self;
}
-(void)initView {
    self.backgroundColor = [UIColor whiteColor];
    UITapGestureRecognizer *tapView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapViewBack)];
    [self addGestureRecognizer:tapView];
    
    //边框
    self.edgeLineView = [[UIView alloc]init];
    self.edgeLineView.width = self.width - 116;
    self.edgeLineView.left = 16;
    self.edgeLineView.layer.cornerRadius = 20;
    self.edgeLineView.layer.masksToBounds = YES;
    self.edgeLineView.backgroundColor = UIColorHex(F5F5F5);
    [self addSubview:self.edgeLineView];
    //输入框
    self.textView = [[UITextView alloc] init];;
    self.textView.width = self.width - 33  - 10 - 84-14;
    self.textView.left = 33;
    self.textView.textColor = UIColorHex(474849);
    self.textView.enablesReturnKeyAutomatically = YES;
    self.textView.delegate = self;
    self.textView.layoutManager.allowsNonContiguousLayout = NO;
    self.textView.scrollsToTop = NO;
    self.textView.returnKeyType = UIReturnKeySend;
    self.textView.backgroundColor = [UIColor clearColor];
//    self.textView.textContainerInset = UIEdgeInsetsZero;
    self.textView.textContainer.lineFragmentPadding = 0;
    self.textView.textContainerInset = UIEdgeInsetsMake(0, 0, 0, 8);
    self.textView.inputAccessoryView = nil;
    [self addSubview:self.textView];
    //占位文字
    self.placeholderLabel = [[UILabel alloc] init];
    self.placeholderLabel.width = self.textView.width - 72;
    self.placeholderLabel.textColor = UIColorHex(919599);
    self.placeholderLabel.left = 36;
    [self addSubview:self.placeholderLabel];
    //发送按钮
//    self.emojiButton = [[UIButton alloc] init];
//    [self.emojiButton setBackgroundImage:[UIImage imageNamed:@"emojiPublish"] forState:UIControlStateNormal];
//    [self.emojiButton setImage:[UIImage imageNamed:@"ToolViewKeyboard"] forState:UIControlStateSelected];
//    [self addSubview:self.emojiButton];
//
//    MJWeakSelf
//    [self.emojiButton mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerY.equalTo(self.edgeLineView);
//        make.right.equalTo(weakSelf.mas_right).offset(-24);
//        make.width.mas_equalTo(24);
//        make.height.mas_equalTo(24);
//    }];
    
    
    [self addSubview:self.sendBtn];
    
    [self.sendBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self).offset(-24);
            make.width.equalTo(@60);
            make.height.equalTo(@28);
            make.centerY.equalTo(self.edgeLineView);
    }];
    [self addSubview:self.userBgView];
    
    [self.userBgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self).offset(-56);
            make.left.right.equalTo(self);
            make.height.equalTo(@40);
    }];
    
    [self.userBgView addSubview:self.avaterImageView];
    [self.userBgView addSubview:self.userContentLabel];
    
    [self.avaterImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.userBgView).offset(16);
            make.width.height.equalTo(@24);
            make.centerY.equalTo(self.userBgView);
    }];
    
    [self.userContentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.avaterImageView);
        make.right.equalTo(self.userBgView).offset(-16);
        make.left.equalTo(self.avaterImageView.mas_right).offset(8);
    }];
    
    
    
    self.fontSize = kFont(14);
    self.textViewMaxLine = 3;
    if (_maxLength <= 0) {
        _maxLength = 20;
    }

}

- (void)sendBtnAction:(UIButton *)btn {
   
    if (self.inputTextBlock) {
        self.inputTextBlock(self.textView.text);
    }
    [self.textView resignFirstResponder];
}


- (UIButton *)sendBtn {
    if(!_sendBtn){
        _sendBtn = [UIButton new];
        [_sendBtn addTarget:self action:@selector(sendBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        _sendBtn.layer.cornerRadius = 14;
        _sendBtn.backgroundColor = kColorWithHexString(@"#0F1833");
        _sendBtn.titleLabel.font = FontSemibold(14);
        [_sendBtn setTitleColor:kColorWithHexString(@"#F5F5F5") forState:UIControlStateNormal];
        [_sendBtn setTitleColor:kColorWithHexString(@"#F5F5F5") forState:UIControlStateHighlighted];
        [_sendBtn setTitle:@"发送" forState:UIControlStateNormal];
        
    }
    return _sendBtn;
}

- (void)setToUser:(QJCommentUser *)toUser {
    _toUser = toUser;
    if(!toUser) return;
    if(toUser.commentStr.length == 0) return;
    self.frame = CGRectMake(0, kScreenHeight, kScreenWidth, 96);
    self.userBgView.hidden = NO;
    self.avaterImageView.imageURL = [NSURL URLWithString:toUser.avatarUrl?:@""];
    self.userContentLabel.text = toUser.commentStr?:@"";
    
}


- (UIView *)userBgView {
    if(!_userBgView){
        _userBgView = [UIView new];
        _userBgView.backgroundColor = [UIColor whiteColor];
        _userBgView.hidden = YES;
    }
    return _userBgView;
}

- (UILabel *)userContentLabel {
    if(!_userContentLabel){
        _userContentLabel = [UILabel new];
        _userContentLabel.font = FontRegular(12);
        _userContentLabel.textColor = kColorWithHexString(@"#16191C");
        _userContentLabel.textAlignment = NSTextAlignmentLeft;
    }
    return _userContentLabel;
}


- (UIImageView *)avaterImageView {
    if(!_avaterImageView){
        _avaterImageView = [UIImageView new];
        _avaterImageView.layer.cornerRadius = 12;
        _avaterImageView.layer.masksToBounds = YES;
    }
    return _avaterImageView;
}


// 添加通知
-(void)addNotification {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHidden:) name:UIKeyboardWillHideNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(emotionDidSelected:) name:GXEmotionDidSelectNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deleteBtnClicked) name:GXEmotionDidDeleteNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sendMessage) name:GXEmotionDidSendNotification object:nil];
}

- (void)setFontSize:(UIFont *)fontSize {
    _fontSize = fontSize;
    self.textView.font = fontSize;
    self.placeholderLabel.font = self.textView.font;
    CGFloat lineH = self.textView.font.lineHeight;
    if(self.toUser){
        self.height = ceil(lineH) + 38 + 40;
    }else{
        self.height = ceil(lineH) + 38;
    }
   
    self.textView.height = lineH;
}
- (void)setTextViewMaxLine:(NSInteger)textViewMaxLine {
    _textViewMaxLine = textViewMaxLine;
    if (!_textViewMaxLine || _textViewMaxLine <= 0) {
        _textViewMaxLine = 3;
    }
}
-(void)setPlaceholder:(NSString *)placeholder{
    _placeholder = placeholder;
    self.placeholderLabel.text = placeholder;
}
- (void)setTextString:(NSString *)textString {
    _textString = textString;
    if (![kCheckStringNil(textString) isEqualToString:@""]) {
        self.textView.text = textString;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.textView.delegate textViewDidChange:self.textView];
        });
    }
}
-(void)setMaxLength:(NSInteger)maxLength{
    _maxLength = maxLength;
    if (_maxLength) {
        self.textNum.text = [NSString stringWithFormat:@"00/%ld",(long)_maxLength];
    }
}
#pragma mark keyboardnotification
- (void)keyboardWillShow:(NSNotification *)notification {
    CGRect keyboardFrame = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    _keyboardHeight = keyboardFrame.size.height;
    CGFloat duration = [notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    [UIView animateWithDuration:duration animations:^{
        self.top = keyboardFrame.origin.y - self.height;
    }];
}
- (void)keyboardWillHidden:(NSNotification *)notification {
    CGFloat duration = [notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    [UIView animateWithDuration:duration animations:^{
        self.top = kScreenHeight;
    }];
}
#pragma mark UITextViewDelegate
- (void)textViewDidChange:(UITextView *)textView {
    self.placeholderLabel.hidden = textView.text.length;
    CGFloat contentSizeH = textView.contentSize.height;
    CGFloat lineH = textView.font.lineHeight;
    CGFloat maxTextViewHeight = ceil(lineH * self.textViewMaxLine + textView.textContainerInset.top + textView.textContainerInset.bottom);
    if (contentSizeH <= maxTextViewHeight) {
        textView.height = contentSizeH;
    }else{
        textView.height = maxTextViewHeight;
    }
    if(self.toUser){
        self.height = ceil(textView.height) + 38 + 40;
    }else{
        self.height = ceil(textView.height) + 38;
    }
    
    self.bottom = kScreenHeight - _keyboardHeight;
    [textView scrollRangeToVisible:NSMakeRange(textView.selectedRange.location, 1)];
    
    NSString *lang = textView.textInputMode.primaryLanguage;//键盘输入模式
        NSInteger length = 0;
    if ([lang isEqualToString:@"zh-Hans"]){
        UITextRange *selectedRange = [textView markedTextRange];
        length = textView.text.length;

    }else{
        length = textView.text.length;
    }
    if (length > 0) {
        
        if (length > _maxLength) {
             textView.text = [textView.text substringToIndex:_maxLength];
            self.textNum.text = [NSString stringWithFormat:@"%ld/%ld",_maxLength,(long)_maxLength];
            return;
        }
        
        self.textNum.text = [NSString stringWithFormat:@"%ld/%ld",length,(long)_maxLength];
    }
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    self.textNum.text = [NSString stringWithFormat:@"%ld/%ld",textView.text.length,(long)_maxLength];
}

- (void)inputToolbarSendText:(inputTextBlock)sendText{
    self.inputTextBlock = sendText;
}
- (NSString *)getTextViewString {
    return self.textView.text;
}
- (void)popToolbar{
    self.fontSize = _fontSize;
    [self.textView becomeFirstResponder];
}
// 发送成功 清空文字 更新输入框大小
-(void)bounceToolbar {
    self.textView.text = nil;
    [self.textView.delegate textViewDidChange:self.textView];
    [self endEditing:YES];
}
-(void)layoutSubviews{
    [super layoutSubviews];
    if(self.toUser.commentStr.length > 0){
        self.edgeLineView.top = 48;
        self.edgeLineView.height = self.textView.height + 20;
        self.textView.top = 58;
        self.placeholderLabel.height = self.textView.height;
        self.placeholderLabel.top = 58;
    }else{
        self.edgeLineView.top = 8;
        self.edgeLineView.height = self.textView.height + 20;
        self.textView.top = 18;
        self.placeholderLabel.height = self.textView.height;
        self.placeholderLabel.top = 18;
    }
   
}

- (void)tapViewBack {
    
}
- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if ([text isEqualToString:@"\n"]){ //判断输入的字是否是回车，即按下return
        //在这里做你响应return键的代码
        if (self.inputTextBlock) {
            self.inputTextBlock(self.textView.text);
        }
        [self.textView resignFirstResponder];
        return NO; //这里返回NO，就代表return键值失效，即页面上按下return，不会出现换行，如果为yes，则输入页面会换行
    }

    return YES;
}


//选中表情
- (void)emotionDidSelected:(NSNotification *)notifi {
    XZEmotion *emotion = notifi.userInfo[GXSelectEmotionKey];
    if (emotion.code) {
        [self.textView insertText:emotion.code.emoji];
        [self.textView scrollRangeToVisible:NSMakeRange(self.textView.text.length, 0)];
    } else if (emotion.face_name) {
        [self.textView insertText:emotion.face_name];
    }
}

// 删除表情
- (void)deleteBtnClicked {
    [self.textView deleteBackward];
}

//发送消息
- (void)sendMessage {
    if (self.inputTextBlock) {
        self.inputTextBlock(self.textView.text);
    }
    
}



@end
