//
//  QJNewsSearchTypeView.m
//  QJBox
//
//  Created by macm on 2022/7/27.
//

#import "QJNewsSearchTypeView.h"
#import "QJImageButton.h"

@interface QJNewsSearchTypeView ()
@property (nonatomic, strong) QJImageButton *titleBt;
@end

@implementation QJNewsSearchTypeView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        self.layer.cornerRadius = 2;
        self.layer.shadowColor = UIColorHex(00000014).CGColor;
        self.layer.shadowOpacity = 10;
        self.layer.shadowRadius = 10;
        self.layer.shadowOffset = CGSizeMake(0, 4);
        
    }
    return self;
}

- (void)setTitleArray:(NSArray *)titleArray {
    _titleArray = titleArray;
    if (_titleArray.count > 0) {
        [self createView];
    }
}

- (void)createView {
    [self addSubview:self.titleBt];
    [self.titleBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).offset(5);
        make.height.mas_equalTo(30);
        make.width.mas_equalTo(70);
        make.centerX.mas_equalTo(self);
    }];
    self.titleBt.titleString = self.titleArray.firstObject;
    
    int i = 0;
    for (NSString *string in self.titleArray) {
        UIButton *btn = [UIButton new];
        [self addSubview:btn];
        btn.tag = 100+i;
        [btn setTitle:string forState:UIControlStateNormal];
        [btn setTitleColor:UIColorHex(474849) forState:UIControlStateNormal];
        btn.titleLabel.font = MYFONTALL(FONT_REGULAR, 14);
        btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.titleBt.mas_bottom).mas_offset(i*35);
            make.left.mas_equalTo(15);
            make.right.mas_equalTo(0);
            make.height.mas_equalTo(35);
        }];
        i++;
    }
    
}

- (void)btnClick:(UIButton *)btn {
    NSInteger selectIndex = btn.tag-100;
    self.titleBt.titleString = self.titleArray[selectIndex];

    if (self.clickButton) {
        self.clickButton(self.titleArray[selectIndex]);
    }
    [self cancelAction];
}

- (void)cancelAction {
    [self setHidden:YES];
}

- (QJImageButton *)titleBt {
    if (!_titleBt) {
        _titleBt = [QJImageButton new];
        _titleBt.titleLb.font = MYFONTALL(FONT_REGULAR, 14);
        _titleBt.titleLb.textColor = UIColorHex(474849);
        _titleBt.iconString = @"iconDownOne";
        _titleBt.space = 3;
        _titleBt.size = CGSizeMake(16, 16);
        _titleBt.style = ImageButtonStyleRight;
        [_titleBt addTarget:self action:@selector(cancelAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _titleBt;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
