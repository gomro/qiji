//
//  QJCommunityScrollView.h
//  QJBox
//
//  Created by Sun on 2022/7/27.
//

#import "GKPageScrollView.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJCommunityScrollView : GKPageScrollView

@property (nonatomic, assign) CGFloat contentInsetTop;
@end

NS_ASSUME_NONNULL_END
