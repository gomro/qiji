//
//  QJDetailAlertView.m
//  QJBox
//
//  Created by Sun on 2022/7/21.
//

#import "QJDetailAlertView.h"

@interface QJDetailAlertView()
@property (nonatomic, copy) QJDetailAlertViewButtonClick clickHandler;
@property (nonatomic, strong) UILabel *title;//标题
@property (nonatomic, strong) UILabel *describe;//文本内容呢
@property (nonatomic, assign) QJDetailAlertViewTitleType alertViewTitleType;
@property (nonatomic, assign) QJDetailAlertViewButtonType alertViewButtonType;
@end
@implementation QJDetailAlertView
- (UILabel *)title {
    if (!_title) {
        self.title = [[UILabel alloc] init];
        self.title.textColor = [UIColor blackColor];
        self.title.font =  MYFONTALL(FONT_BOLD, 16);
        self.title.textAlignment = NSTextAlignmentCenter;
        self.title.text = @"";
        self.title.numberOfLines = 0;

    }
    return _title;
}

- (UILabel *)describe {
    if (!_describe) {
        self.describe = [[UILabel alloc] init];
        self.describe.textColor = [UIColor blackColor];
        self.describe.font =  MYFONTALL(FONT_REGULAR, 14);
        self.describe.numberOfLines = 0;
        self.describe.text = @"";
        self.describe.textAlignment = NSTextAlignmentCenter;
    }
    return _describe;
}

- (UIButton *)leftButton {
    if (!_leftButton) {
        self.leftButton = [[UIButton alloc] init];
        [self.leftButton addTarget:self action:@selector(leftClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.leftButton setTitleColor:UIColorHex(919599) forState:UIControlStateNormal];
        self.leftButton.titleLabel.font = MYFONTALL(FONT_REGULAR, 14);
    }
    return _leftButton;
}

- (UIButton *)rightButton {
    if (!_rightButton) {
        self.rightButton = [[UIButton alloc] init];
        [self.rightButton addTarget:self action:@selector(rightClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.rightButton setTitleColor:UIColorHex(FF3B30) forState:UIControlStateNormal];
        self.rightButton.titleLabel.font = MYFONTALL(FONT_REGULAR, 14);
    }
    return _rightButton;
}

- (UIButton *)onlyButton {
    if (!_onlyButton) {
        self.onlyButton = [[UIButton alloc] init];
        [self.onlyButton addTarget:self action:@selector(onlyClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.onlyButton setTitleColor:UIColorHex(FF3B30) forState:UIControlStateNormal];
        self.onlyButton.titleLabel.font = MYFONTALL(FONT_REGULAR, 14);
    }
    return _onlyButton;
}

- (instancetype)initWithFrame:(CGRect)frame titleType:(QJDetailAlertViewTitleType)titleType buttonType:(QJDetailAlertViewButtonType)buttonType {
    self = [super initWithFrame:frame];
    self.frame = CGRectMake(0, 0, 270, 130);
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        self.layer.masksToBounds = YES;
        self.layer.cornerRadius = 4;
        self.alertViewTitleType = titleType;
        self.alertViewButtonType = buttonType;
        
        [self addSubview:self.describe];
        if (self.alertViewTitleType == QJDetailAlertViewTitleTypeDefault) {
            [self addSubview:self.title];
        }
        if (self.alertViewButtonType == QJDetailAlertViewButtonTypeDefault) {
            [self addSubview:self.leftButton];
            [self addSubview:self.rightButton];

        } else {
            [self addSubview:self.onlyButton];
 
        }
        [self subTitleView];
        [self subButtonView];
    }
    return self;
}

#pragma mark - 视图层
- (void)subTitleView {
    __weak typeof(self) weakSelf = self;
    if (self.alertViewTitleType == QJDetailAlertViewTitleTypeDefault) {
        [self.title mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(weakSelf.mas_top).offset(16);
            make.left.equalTo(weakSelf.mas_left).offset(16);
            make.right.equalTo(weakSelf.mas_right).offset(-16);
        }];
        [self.describe mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(weakSelf.title.mas_bottom).offset(5);
            make.left.equalTo(weakSelf.mas_left).offset(16);
            make.right.equalTo(weakSelf.mas_right).offset(-16);
        }];
    } else {
        [self.describe mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(weakSelf.mas_top).offset(16);
            make.left.equalTo(weakSelf.mas_left).offset(16);
            make.right.equalTo(weakSelf.mas_right).offset(-16);
        }];
    }

}

- (void)subButtonView {
    __weak typeof(self) weakSelf = self;
    UIView *line = [[UIView alloc] init];
    line.backgroundColor = UIColorHex(3C3C435C);
    [self addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(weakSelf.mas_bottom).offset(-43);
        make.left.equalTo(weakSelf.mas_left);
        make.right.equalTo(weakSelf.mas_right);
        make.height.mas_equalTo(1);
    }];
    if (self.alertViewButtonType == QJDetailAlertViewButtonTypeDefault) {
        UIView *centerLine = [[UIView alloc] init];
        centerLine.backgroundColor = UIColorHex(3C3C435C);
        [self addSubview:centerLine];
        [centerLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(line.mas_bottom);
            make.bottom.equalTo(weakSelf.mas_bottom);
            make.centerX.equalTo(weakSelf);
            make.width.mas_equalTo(1);
        }];
       
        [self.leftButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(line.mas_bottom);
            make.bottom.equalTo(weakSelf.mas_bottom);
            make.left.equalTo(weakSelf.mas_left);
            make.right.equalTo(centerLine.mas_left);
        }];
        
        [self.rightButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(line.mas_bottom);
            make.bottom.equalTo(weakSelf.mas_bottom);
            make.left.equalTo(centerLine.mas_right);
            make.right.equalTo(weakSelf.mas_right);
        }];
    } else {
        [self.onlyButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(line.mas_bottom);
            make.bottom.equalTo(weakSelf.mas_bottom);
            make.left.equalTo(weakSelf.mas_left);
            make.right.equalTo(weakSelf.mas_right);
        }];
  
    }
}

#pragma mark - 点击事件
- (void)leftClick:(UIButton *)bt {
    if (self.clickHandler != nil) {
        self.clickHandler(QJDetailAlertViewButtonClickTypeLeft);
    }
}

- (void)rightClick:(UIButton *)bt {
    if (self.clickHandler != nil) {
        self.clickHandler(QJDetailAlertViewButtonClickTypeRight);
    }
}

- (void)onlyClick:(UIButton *)bt {
    if (self.clickHandler != nil) {
        self.clickHandler(QJDetailAlertViewButtonClickTypeLeft);
    }
}


- (void)returnClick:(QJDetailAlertViewButtonClick)clickHandler {
    self.clickHandler = clickHandler;
}

#pragma mark - 标题文字
- (void)showTitleText:(NSString *)titleText describeText:(NSString *)describeText {
    [self showTitleText:titleText titleFont:nil titleColor:nil describeText:describeText describeFont:nil describeColor:nil];
}

- (void)showTitleText:(NSString *)titleText titleFont:(UIFont *)titleFont titleColor:(UIColor *)titleColor describeText:(NSString *)describeText describeFont:(UIFont *)describeFont describeColor:(UIColor *)describeColor {
    self.title.text = titleText;
    if (titleColor) {
        self.title.textColor = titleColor;
        
    }
    if (titleFont) {
        self.title.font = titleFont;
        
    }
    self.describe.text = describeText;
    if (describeColor) {
        self.describe.textColor = describeColor;
        
    }
    if (describeFont) {
        self.describe.font = describeFont;
        
    }
    
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineSpacing = 10 - ( self.describe.font.lineHeight -  self.describe.font.pointSize);
    NSMutableDictionary *attributes = [NSMutableDictionary dictionary];
    [attributes setObject:paragraphStyle forKey:NSParagraphStyleAttributeName];
     self.describe.attributedText = [[NSAttributedString alloc] initWithString: self.describe.text attributes:attributes];
    self.describe.textAlignment = NSTextAlignmentCenter;
    
    CGFloat titleH = 0;
    CGFloat describeH = 0;
    if (self.alertViewTitleType == QJDetailAlertViewTitleTypeOnlyDeatil) {
        [self.describe layoutIfNeeded];
        describeH = self.describe.height + 16 + 16;
    } else {
        [self.describe layoutIfNeeded];
        [self.title layoutIfNeeded];
        describeH = self.describe.height + 5 + 16;
        titleH = self.title.height + 16 + 16;
    }
    CGFloat viewHeight = describeH + titleH + 43;
    __weak typeof(self) weakSelf = self;
    if (self.alertViewTitleType == QJDetailAlertViewTitleTypeOnlyDeatil) {
        [self.describe mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(weakSelf.mas_centerY).offset(-16);
            make.left.equalTo(weakSelf.mas_left).offset(16);
            make.right.equalTo(weakSelf.mas_right).offset(-16);
        }];
    }
    self.frame = CGRectMake(0, 0, 270, viewHeight);

}

- (void)setDescribeAlignment:(NSString *)describeAlignment {
    _describeAlignment = describeAlignment;
    __weak typeof(self) weakSelf = self;
    if ([describeAlignment isEqualToString:@"1"]) {
        if (self.alertViewTitleType == QJDetailAlertViewTitleTypeDefault) {
            self.describe.textAlignment = NSTextAlignmentLeft;
            [self.describe mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(weakSelf.title.mas_bottom).offset(16);
                make.width.mas_lessThanOrEqualTo(240);
                make.centerX.equalTo(weakSelf);
            }];
        }
    }
}
@end
