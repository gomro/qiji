//
//  QJBottomDeleteAllView.m
//  QJBox
//
//  Created by wxy on 2022/8/5.
//

#import "QJBottomDeleteAllView.h"


@interface QJBottomDeleteAllView ()

@property (nonatomic, strong) UIButton *leftBtn;

@property (nonatomic, strong) UIButton *rightBtn;

     
@end


@implementation QJBottomDeleteAllView

 
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self addSubview:self.leftBtn];
        [self addSubview:self.rightBtn];
        CGFloat width = (kScreenWidth - 16 - 43)/2.0;
        [self.leftBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.width.equalTo(@(width));
                    make.height.equalTo(@34);
                    make.left.equalTo(self).offset(16);
                    make.bottom.equalTo(self).offset(-Bottom_iPhoneX_SPACE);
        }];
        [self.rightBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.width.equalTo(@(width));
                    make.height.equalTo(@34);
                    make.right.equalTo(self).offset(-16);
                    make.bottom.equalTo(self).offset(-Bottom_iPhoneX_SPACE);
        }];
        self.backgroundColor = [UIColor whiteColor];
        [self viewShadowPathWithColor:[UIColor hx_colorWithHexStr:@"000000" alpha:0.08] shadowOpacity:0.3 shadowRadius:3 shadowPathType:QJShadowPathTop shadowPathWidth:6];
    }
    return self;
}


#pragma mark  -------- btnAction



- (void)leftBtnAction {
    
    if (self.selectedAll) {
        self.selectedAll();
    }
    
}


- (void)rightBtnAction {
    
    if (self.deleteAction) {
        self.deleteAction();
    }
    
}


#pragma mark  ----- getter

- (UIButton *)leftBtn {
    if (!_leftBtn) {
        _leftBtn = [UIButton new];
        _leftBtn.layer.cornerRadius = 17;
        _leftBtn.layer.borderColor = kColorWithHexString(@"#919599").CGColor;
        _leftBtn.layer.borderWidth = 1;
        [_leftBtn setTitleColor:kColorWithHexString(@"000000") forState:UIControlStateNormal];
        [_leftBtn setTitle:@"全选" forState:UIControlStateNormal];
        _leftBtn.titleLabel.font = FontMedium(14);
        [_leftBtn addTarget:self action:@selector(leftBtnAction) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _leftBtn;
}


- (UIButton *)rightBtn {
    if (!_rightBtn) {
        _rightBtn = [UIButton new];
        _rightBtn.layer.cornerRadius = 17;
        [_rightBtn setTitle:@"删除" forState:UIControlStateNormal];
        [_rightBtn setTitleColor:kColorWithHexString(@"ffffff") forState:UIControlStateNormal];
        _rightBtn.titleLabel.font = FontSemibold(14);
        _rightBtn.backgroundColor = kColorWithHexString(@"#FF9500");
        [_rightBtn addTarget:self action:@selector(rightBtnAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _rightBtn;
}

@end
