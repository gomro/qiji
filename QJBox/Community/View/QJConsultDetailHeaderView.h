//
//  QJConsultDetailHeaderView.h
//  QJBox
//
//  Created by Sun on 2022/7/11.
//

#import <UIKit/UIKit.h>
#import "QJCampsiteDetailModel.h"
#import "QJNewsListModel.h"

NS_ASSUME_NONNULL_BEGIN
@protocol QJConsultDetailHeaderViewDelegate <NSObject>

- (void)didClickUserView;

- (void)didVideoImageUrl:(UIView *)headerView;
- (void)changeWebViewHeight:(CGFloat)height;

@end
@interface QJConsultDetailHeaderView : UIView
- (void)configureDetailHeaderWithData:(QJCampsiteDetailModel *)dataModel;
- (void)configureDetailHeaderWithNewsData:(QJNewsListModel *)dataModel;
@property (nonatomic, weak) id<QJConsultDetailHeaderViewDelegate> headerDelegate;

@end

NS_ASSUME_NONNULL_END
