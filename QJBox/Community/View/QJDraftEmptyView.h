//
//  QJDraftEmptyView.h
//  QJBox
//
//  Created by wxy on 2022/9/22.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJDraftEmptyView : UIView


@property (nonatomic, strong) UIColor *bgColor;

@property (nonatomic, copy) NSString *string;

//图片距离上面距离，不用乘以 375系数了，你面做了
@property (nonatomic, assign) CGFloat offsetY;

@property (nonatomic, copy) NSString *imageStr;

@end

NS_ASSUME_NONNULL_END
