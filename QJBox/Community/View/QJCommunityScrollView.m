//
//  QJCommunityScrollView.m
//  QJBox
//
//  Created by Sun on 2022/7/27.
//

#import "QJCommunityScrollView.h"
@interface QJCommunityScrollView()
@property (nonatomic, weak) id<GKPageScrollViewDelegate> delegate;

// 是否滑动到临界点，可有偏差
@property (nonatomic, assign) BOOL                      isCriticalPoint;
// 是否到达临界点，无偏差
@property (nonatomic, assign) BOOL                      isCeilPoint;
// mainTableView是否可滑动
@property (nonatomic, assign) BOOL                      isMainCanScroll;
// listScrollView是否可滑动
@property (nonatomic, assign) BOOL                      isListCanScroll;

// 是否开始拖拽，只有在拖拽中才去处理滑动，解决使用mj_header可能出现的bug
@property (nonatomic, assign) BOOL                      isBeginDragging;

// 快速切换原点和临界点
@property (nonatomic, assign) BOOL                      isScrollToOriginal;
@property (nonatomic, assign) BOOL                      isScrollToCritical;

// 是否加载
@property (nonatomic, assign) BOOL                      isLoaded;

@end
@implementation QJCommunityScrollView

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.mainTableView.frame = self.bounds;
    
    
}


- (void)setContentInsetTop:(CGFloat)contentInsetTop {
    _contentInsetTop = contentInsetTop;
    DLog(@"距上边距==%f",self.contentInsetTop);
    self.mainTableView.contentInset = UIEdgeInsetsMake(contentInsetTop, 0, 0, 0);
    
 
}

- (void)listScrollViewDidScroll:(UIScrollView *)scrollView {
    // 获取listScrollview偏移量
    CGFloat offsetY = scrollView.contentOffset.y;

    // listScrollView下滑至offsetY小于0，禁止其滑动，让mainTableView可下滑
    if (offsetY <= 0) {
        if (self.isAllowListRefresh && offsetY < 0 && self.mainTableView.contentOffset.y == -self.contentInsetTop) {
            self.isMainCanScroll = NO;
            self.isListCanScroll = YES;
        }else {
            self.isMainCanScroll = YES;
            self.isListCanScroll = NO;
            
            if (scrollView.isDecelerating) return;
            scrollView.contentOffset = CGPointZero;
            if (self.isControlVerticalIndicator) {
                scrollView.showsVerticalScrollIndicator = NO;
            }
        }
    } else {
        if (self.isListCanScroll) {
            if (self.isControlVerticalIndicator) {
                scrollView.showsVerticalScrollIndicator = YES;
            }
            
            CGFloat headerHeight = CGRectGetHeight([self.delegate headerViewInPageScrollView:self].frame);
            
            if (floor(headerHeight) == 0) {
                CGFloat criticalPoint = [self.mainTableView rectForSection:0].origin.y - self.ceilPointHeight;
                self.mainTableView.contentOffset = CGPointMake(0, criticalPoint);
            }else {
                // 如果此时mianTableView并没有滑动，则禁止listView滑动
                if (self.mainTableView.contentOffset.y == 0 && floor(headerHeight) != 0) {
                    self.isMainCanScroll = YES;
                    self.isListCanScroll = NO;

                    if (scrollView.isDecelerating) return;
                    scrollView.contentOffset = CGPointZero;
                    if (self.isControlVerticalIndicator) {
                        scrollView.showsVerticalScrollIndicator = NO;
                    }
                }else { // 矫正mainTableView的位置
                    CGFloat criticalPoint = [self.mainTableView rectForSection:0].origin.y - self.ceilPointHeight;
                    self.mainTableView.contentOffset = CGPointMake(0, criticalPoint);
                }
            }
        }else {
            if (scrollView.isDecelerating) return;
            scrollView.contentOffset = CGPointZero;
        }
    }
}

- (void)mainScrollViewDidScroll:(UIScrollView *)scrollView {

    // 获取mainScrollview偏移量
    CGFloat offsetY = scrollView.contentOffset.y;
    
    // 临界点
    CGFloat criticalPoint = [self.mainTableView rectForSection:0].origin.y - self.ceilPointHeight;
    
    if (self.isScrollToOriginal || self.isScrollToCritical) return;
    
    // 根据偏移量判断是否上滑到临界点
    if (offsetY >= criticalPoint) {
        self.isCriticalPoint = YES;
    }else {
        self.isCriticalPoint = NO;
    }
    
    // 无偏差临界点，对float值取整判断
    if (!self.isCeilPoint ) {
        if (floor(offsetY) == floor(criticalPoint)) {
            self.isCeilPoint = YES;
        }
    }
    
    if (self.isCriticalPoint) {
        // 上滑到临界点后，固定其位置
        scrollView.contentOffset = CGPointMake(0, criticalPoint);
        self.isMainCanScroll = NO;
        self.isListCanScroll = YES;
    }else {
        // 如果允许列表刷新，并且mainTableView的offsetY小于0 或者 当前列表的offsetY小于0,mainScrollView不可滑动
        if (self.isAllowListRefresh && ((offsetY <= -self.contentInsetTop && self.isMainCanScroll) || (self.currentListScrollView.contentOffset.y < 0 && self.isListCanScroll))) {
            scrollView.contentOffset = CGPointMake(0, -self.contentInsetTop);
        } else {
            if (self.isMainCanScroll) {
                // 未达到临界点，mainScrollview可滑动，需要重置所有listScrollView的位置
                [self listScrollViewOffsetFixed];
            }else {
                // 未到达临界点，mainScrollView不可滑动，固定mainScrollView的位置
                [self mainScrollViewOffsetFixed];
            }
        }
    }
    
    [self mainTableViewCanScrollUpdate];
}

// 修正mainTableView的位置
- (void)mainScrollViewOffsetFixed {
    // 获取临界点位置
    CGFloat criticalPoint = self.mainTableView.contentOffset.y;
    if (self.mainTableView.contentOffset.y <= -self.contentInsetTop) {
        criticalPoint = -self.contentInsetTop;
    }
    
    [[self.delegate listViewsInPageScrollView:self] enumerateObjectsUsingBlock:^(id<GKPageListViewDelegate>  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        UIScrollView *listScrollView = [obj listScrollView];
        if (listScrollView.contentOffset.y != 0) {
            self.mainTableView.contentOffset = CGPointMake(0, criticalPoint);
        }
    }];
}

// 修正listScrollView的位置
- (void)listScrollViewOffsetFixed {
    
    [[self.delegate listViewsInPageScrollView:self] enumerateObjectsUsingBlock:^(id<GKPageListViewDelegate>  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        UIScrollView *listScrollView = [obj listScrollView];
        listScrollView.contentOffset = CGPointZero;
        if (self.isControlVerticalIndicator) {
            listScrollView.showsVerticalScrollIndicator = NO;
        }
    }];
}

- (void)mainTableViewCanScrollUpdate {
    if ([self.delegate respondsToSelector:@selector(mainTableViewDidScroll:isMainCanScroll:)]) {
        [self.delegate mainTableViewDidScroll:self.mainTableView isMainCanScroll:self.isMainCanScroll];
    }
}

- (void)scrollToOriginalPoint {
    // 这里做了0.01秒的延时，是为了解决一个坑：当通过手势滑动结束调用此方法时，会有可能出现动画结束后UITableView没有回到原点的bug
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (self.isScrollToOriginal) return;
        
        self.isScrollToOriginal  = YES;
        self.isCeilPoint         = NO;
        
        self.isMainCanScroll     = YES;
        self.isListCanScroll     = NO;
        
        [self.mainTableView setContentOffset:CGPointMake(0, -self.contentInsetTop) animated:YES];
    });
}
@end
