//
//  QJNewsHeaderView.h
//  QJBox
//
//  Created by Sun on 2022/7/27.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJNewsHeaderView : UIView

@property (nonatomic, copy) NSArray *dataModelArr;

- (void)getBackImageStr;
@end

NS_ASSUME_NONNULL_END
