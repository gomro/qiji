//
//  QJVideoPlayerView.h
//  QJBox
//
//  Created by Sun on 2022/8/2.
//

#import <SJVideoPlayer/SJVideoPlayer.h>
#import "QJCampsiteDetailModel.h"
#import "QJNewsListModel.h"
NS_ASSUME_NONNULL_BEGIN
typedef NS_ENUM(NSUInteger, QJVideoPlayerViewType) {
    QJVideoPlayerViewTypeMore,        //更多
    QJVideoPlayerViewTypeMoreVertical,//竖屏更多
    QJVideoPlayerViewTypeRate,        //速率
    QJVideoPlayerViewTypeRateVertical//竖屏速率
};

typedef void (^ClickButton)(QJVideoPlayerViewType type);

@interface QJVideoPlayerView : SJVideoPlayer
@property (nonatomic, copy) ClickButton clickButton;

/**动态*/
- (void)changePlayerView:(QJCampsiteDetailModel *)dataModel;

/**资讯*/
- (void)changePlayerNewsView:(QJNewsListModel *)dataModel showVolume:(BOOL)showVolume;

/**全屏缩放**/
- (void)videoFullItemWasTapped;

@end

NS_ASSUME_NONNULL_END
