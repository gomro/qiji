//
//  QJDetailShareView.h
//  QJBox
//
//  Created by Sun on 2022/7/20.
//

#import <UIKit/UIKit.h>
#import "QJNewsListModel.h"
#import "QJCampsiteDetailModel.h"

NS_ASSUME_NONNULL_BEGIN
typedef NS_ENUM(NSInteger, QJDetailShareViewStyle){
    QJDetailShareViewStyleQQZone = 100,         //QQ空间
    QJDetailShareViewStyleQQ = 101,             //QQ
    QJDetailShareViewStyleWeChat = 102,         //微信
    QJDetailShareViewStyleWeChatCircle = 103,   //朋友圈
    QJDetailShareViewStyleSave = 104,           //收藏
    QJDetailShareViewStyleCancleSave = 105,    //取消收藏
    QJDetailShareViewStyleReport = 106,         // 举报
    QJDetailShareViewStyleDelete = 107,         // 删除
};
typedef void (^ClickShareItem)(QJDetailShareViewStyle headerType);

@interface QJDetailShareView : UIView

- (void)showView;

- (void)hideView;

@property (nonatomic, copy) ClickShareItem clickButton;

- (void)configChangeSaveIsMySelf:(BOOL)isMySelf isSave:(BOOL)isSave isNews:(BOOL)isNews newsID:(NSString *)newsID;
@end

NS_ASSUME_NONNULL_END
