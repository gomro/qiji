//
//  QJNewsHeaderView.m
//  QJBox
//
//  Created by Sun on 2022/7/27.
//

#import "QJNewsHeaderView.h"
#import "NewPagedFlowView.h"
#import "QJBannerDataModel.h"

@interface QJNewsHeaderView ()<NewPagedFlowViewDelegate, NewPagedFlowViewDataSource>
@property (nonatomic, strong) NewPagedFlowView *pageFlowView;
@property (nonatomic, assign) NSInteger pageNumber;

@end
@implementation QJNewsHeaderView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:self.pageFlowView];
        self.pageNumber = 0;
        [self getBackImageStr];
    }
    return self;
}
- (void)getBackImageStr {
    if (self.pageNumber < self.dataModelArr.count) {
        QJBannerDataModel *model = self.dataModelArr[self.pageNumber];
        NSString *image = kCheckStringNil(model.backImgUrl);
        [[NSNotificationCenter defaultCenter] postNotificationName:QJCampsiteBackImage object:nil userInfo:@{@"image":image}];
    }
}

#pragma mark NewPagedFlowView Delegate
- (CGSize)sizeForPageInFlowView:(NewPagedFlowView *)flowView {
    return CGSizeMake(296*kWScale, 166*kWScale);
}

- (void)didSelectCell:(UIView *)subView withSubViewIndex:(NSInteger)subIndex {
    
    NSLog(@"点击了第%ld张图",(long)subIndex + 1);
    
}

- (void)didScrollToPage:(NSInteger)pageNumber inFlowView:(NewPagedFlowView *)flowView {
    self.pageNumber = pageNumber;
    NSLog(@"ViewController 滚动到了第%ld页",pageNumber);
    [self getBackImageStr];
}

#pragma mark NewPagedFlowView Datasource
- (NSInteger)numberOfPagesInFlowView:(NewPagedFlowView *)flowView {
    
    return self.dataModelArr.count;
    
}

- (PGIndexBannerSubiew *)flowView:(NewPagedFlowView *)flowView cellForPageAtIndex:(NSInteger)index{
    PGIndexBannerSubiew *bannerView = [flowView dequeueReusableCell];
    if (!bannerView) {
        bannerView = [[PGIndexBannerSubiew alloc] init];
        bannerView.tag = index;
        bannerView.layer.cornerRadius = 4;
        bannerView.layer.masksToBounds = YES;
    }
    //在这里下载网络图片  
    QJBannerDataModel *model = self.dataModelArr[index];
    NSString *imageString = [model.imgUrl checkImageUrlString];
    NSURL *imageURL = [NSURL URLWithString:imageString];
//    [bannerView.mainImageView setImageWithURL:imageURL options:YYWebImageOptionAllowBackgroundTask];
    [bannerView.mainImageView setImageWithURL:imageURL placeholder:[UIImage imageWithColor:[UIColor grayColor]] options:YYWebImageOptionAllowBackgroundTask completion:^(UIImage * _Nullable image, NSURL * _Nonnull url, YYWebImageFromType from, YYWebImageStage stage, NSError * _Nullable error) {
        
    }];
    return bannerView;
}

- (void)setDataModelArr:(NSArray *)dataModelArr {
    _dataModelArr = dataModelArr;
    self.pageNumber = 0;
    [self.pageFlowView reloadData];
    [self getBackImageStr];
}

#pragma mark --懒加载
- (NewPagedFlowView *)pageFlowView {
    if (!_pageFlowView) {
        _pageFlowView = [[NewPagedFlowView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 170*kWScale)];
        _pageFlowView.delegate = self;
        _pageFlowView.dataSource = self;
        _pageFlowView.minimumPageAlpha = 0.1;
        _pageFlowView.orientation = NewPagedFlowViewOrientationHorizontal;
        _pageFlowView.isOpenAutoScroll = NO;
    }
    return _pageFlowView;
}
@end
