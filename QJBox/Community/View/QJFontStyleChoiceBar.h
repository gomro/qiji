//
//  QJFontStyleChoiceBar.h
//  QJBox
//
//  Created by wxy on 2022/8/3.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class RZRichTextViewOptions;
@interface QJFontStyleChoiceBar : UIView

 

@property (nonatomic, strong) NSDictionary *typingAttributes;

@property (nonatomic, copy) void(^choiceIndex)(NSDictionary *typingAttributes);

@property (nonatomic, strong) RZRichTextViewOptions *options;

- (instancetype)initWithFrame:(CGRect)frame options:(RZRichTextViewOptions *)options;
@end

NS_ASSUME_NONNULL_END
