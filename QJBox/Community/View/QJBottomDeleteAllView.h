//
//  QJBottomDeleteAllView.h
//  QJBox
//
//  Created by wxy on 2022/8/5.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJBottomDeleteAllView : UIView


@property (nonatomic, copy) void(^selectedAll)(void);


@property (nonatomic, copy) void(^deleteAction)(void);


@end

NS_ASSUME_NONNULL_END
