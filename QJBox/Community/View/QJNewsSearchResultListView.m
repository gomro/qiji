//
//  QJNewsSearchResultListView.m
//  QJBox
//
//  Created by macm on 2022/7/28.
//

#import "QJNewsSearchResultListView.h"
#import "QJNewsListViewCell.h"
#import "QJCampsiteRequest.h"
#import "QJHomeGLModel.h"
#import "QJConsultDetailViewController.h"

@interface QJNewsSearchResultListView ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, assign) NSInteger page;
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, copy) NSString *Keyword;
@property (nonatomic, copy) NSString *keywordType;
@property (nonatomic, copy) NSString *categoryCode;
@property (nonatomic, copy) NSString *sort;

@end

@implementation QJNewsSearchResultListView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.page = 1;
        self.dataArray = [NSMutableArray array];
        [self createView];
    }
    return self;
}
/**
 * 刷新数据
 keyWordType    关键字类型，标题还是游戏,可用值:GAME,TITLE
 keyword    关键字
 categoryCode    资讯分类，不传查询综合
 sort    排序(仅2种)：发布时间倒序TD、热门倒序HD,可用值:HD,TD
 */
- (void)reloadListViewKeywordType:(NSString *)keywordType keyword:(NSString *)keyword categoryCode:(NSString *)categoryCode sort:(NSString *)sort {
    self.keywordType = keywordType;
    self.Keyword = keyword;
    self.categoryCode = categoryCode;
    self.sort = sort;

    [self loadData];
}

- (void)loadData {
    _page = 1;
    [self.dataArray removeAllObjects];
    [self sendRequest];
}

- (void)loadMoreData {
    _page++;
    [self sendRequest];
}

- (void)sendRequest {

    @weakify(self);
    QJCampsiteRequest *startRequest = [[QJCampsiteRequest alloc] init];
    [startRequest netWorkGetInfoListInSearchKeywords:self.Keyword keywordType:self.keywordType categoryCode:self.categoryCode sort:self.sort page:self.page];
    [startRequest setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        @strongify(self);
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        if (ResponseSuccess) {
            NSArray *dataArr = request.responseJSONObject[@"data"][@"records"];
            for (NSDictionary *dic in dataArr) {
                
                QJNewsListModel *model = [QJNewsListModel modelWithDictionary:dic];
                [self.dataArray addObject:model];
            }
            [self.tableView reloadData];
            
            if ([request.responseJSONObject[@"data"][@"hasNext"] integerValue] == 0 || dataArr.count == 0) {
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
            }
                            
        }
        QJEmptyCommonView *empty = [QJEmptyCommonView new];
        empty.string = @"暂无搜索结果";
        empty.emptyImage = @"empty_no_search";
        [self.tableView tableViewDisplayWhenHaveNoDataWithView:empty ifNecessaryForRowCount:self.dataArray.count];

    }];
    
    [startRequest setFailureCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        @strongify(self);
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];

        QJEmptyCommonView *empty = [QJEmptyCommonView new];
        empty.string = @"暂无搜索结果";
        empty.emptyImage = @"empty_no_search";
        [self.tableView tableViewDisplayWhenHaveNoDataWithView:empty ifNecessaryForRowCount:self.dataArray.count];
    }];
}


- (void)createView {
    [self addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.left.right.mas_equalTo(0);
    }];
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellNewsList = @"cellNewsListId";

    QJNewsListViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellNewsList];
    if (!cell) {
        cell = [[QJNewsListViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellNewsList];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    QJNewsListModel *model = self.dataArray[indexPath.row];
    [cell configureCellWithData:model];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    QJConsultDetailViewController *detailView = [[QJConsultDetailViewController alloc] init];
    QJNewsListModel *model = self.dataArray[indexPath.row];
    detailView.idStr = model.infoId;
    detailView.isNews = YES;
    [[QJAppTool getCurrentViewController].navigationController pushViewController:detailView animated:YES];
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return Get375Width(109);
}

- (UITableView *)tableView {
    if (!_tableView) {
        self.tableView = [[UITableView alloc] init];
        self.tableView.backgroundColor = UIColorFromRGB(0xf5f5f5);
        self.tableView.dataSource = self;
        self.tableView.delegate = self;
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.tableView.showsVerticalScrollIndicator = NO;
        self.tableView.showsHorizontalScrollIndicator = NO;

        if (@available(iOS 11.0, *)) {
            self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
        self.tableView.mj_header = [QJRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadData)];
        self.tableView.mj_footer = [QJRefreshFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
 
    }
    return _tableView;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
