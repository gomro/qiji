//
//  QJCommentInputKit.h
//  QJBox
//
//  Created by Sun on 2022/7/14.
//

#import <UIKit/UIKit.h>
#import "QJCommentUser.h"
NS_ASSUME_NONNULL_BEGIN
typedef void(^ConfirmBlock)(NSString *inputContent);

@interface QJCommentInputKit : UIView
/**
 *  点击确定按钮回调
 */
- (void)showInput:(ConfirmBlock)confirmHandler;

/**
 *  关闭键盘回调
 */
- (void)closeInput:(ConfirmBlock)confirmHandler;
/**
 *  设置默认文字
 */
- (void)setNormalContent:(NSString *)content;

/**
 *  输入文字的最大长度
 */
- (void)setMaxContentLength:(NSInteger)lenght;

/**
 *  输入文字显示行数
 */
- (void)setMaxLineNum:(NSInteger)num;

/**
 *  设置文本内容
 */
- (void)setDescTitle:(NSString *)descTitle;


@property (nonatomic, strong) QJCommentUser *toUser;
@end

NS_ASSUME_NONNULL_END
