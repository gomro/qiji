//
//  QJNewsTagItem.m
//  QJBox
//
//  Created by macm on 2022/7/28.
//

#import "QJNewsTagItem.h"

@interface QJNewsTagItem ()
@property (nonatomic, strong) UILabel *itemLabel;

@end

@implementation QJNewsTagItem

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.layer.backgroundColor = UIColorFromRGB(0xf7f7f7).CGColor;
        self.layer.cornerRadius = 2;
        
        [self setupUI];
    }
    return self;
}

- (void)setupUI {
    
    //初始化参数
    self.font = kFont(12);
    self.titleColor = UIColorFromRGB(0x1c1c1c);
    self.leftMargin = 14;
    self.topMargin = 5;
    
    self.itemLabel = [UILabel new];
    self.itemLabel.font = self.font;
    self.itemLabel.textColor = self.titleColor;
    self.itemLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    [self addSubview:self.itemLabel];
    [self.itemLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_offset(self.topMargin);
        make.bottom.mas_offset(-self.topMargin);
        make.left.mas_offset(self.leftMargin);
        make.right.mas_offset(-self.leftMargin);
    }];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    if (self.tagItemClickEvent) {
        self.tagItemClickEvent(self.tag);
    }
}

#pragma mark - setter getter

- (CGFloat)viewWidth {
    return [self tagWidthForTitle];
}

- (void)setTitle:(NSString *)title {
    _title = title;
    self.itemLabel.text = title;
}

- (void)setFont:(UIFont *)font {
    _font = font;
    self.itemLabel.font = font;
}

- (void)setTitleColor:(UIColor *)titleColor {
    _titleColor = titleColor;
    self.itemLabel.textColor = titleColor;
}

- (void)setTopMargin:(CGFloat)topMargin {
    _topMargin = topMargin;
    //重新布局
    if (self.itemLabel && self.itemLabel.superview) {
        [self.itemLabel mas_updateConstraints:^(MASConstraintMaker *make) {
            make.topMargin.mas_offset(topMargin);
            make.bottomMargin.mas_offset(-topMargin);
        }];
    }
}

- (void)setLeftMargin:(CGFloat)leftMargin {
    _leftMargin = leftMargin;
    //重新布局
    if (self.itemLabel && self.itemLabel.superview) {
        [self.itemLabel mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.mas_offset(leftMargin);
            make.right.mas_offset(-leftMargin);
        }];
    }
}

#pragma mark - Private

- (CGFloat)tagWidthForTitle {
    //根据文字内容和margin返回文字的真实宽度
    if (self.title.length == 0) {
        return 0.f;
    }
    return  [self.title boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX) options:(NSStringDrawingUsesLineFragmentOrigin) attributes:@{NSFontAttributeName:self.itemLabel.font} context:nil].size.width + self.leftMargin*2 + 0.5;  //masonry布局会四舍五入 + 0.5防止宽度不够
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
