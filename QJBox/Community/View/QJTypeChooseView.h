//
//  QJTypeChooseView.h
//  QJBox
//
//  Created by Sun on 2022/7/26.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef NS_ENUM(NSUInteger, QJTypeChooseViewStype) {
    QJTypeChooseViewStypeTime,//发布时间
    QJTypeChooseViewStypePopular//热门排行
};

typedef void (^ClickItem)(QJTypeChooseViewStype chooseType);
@interface QJTypeChooseView : UIView

@property (nonatomic, assign) QJTypeChooseViewStype stype;

/**发布时间*/
@property (nonatomic, strong) UIButton *timeBt;
/**热门排行*/
@property (nonatomic, strong) UIButton *popularBt;

@property (nonatomic, copy) ClickItem clickButton;
@end

NS_ASSUME_NONNULL_END
