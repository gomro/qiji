//
//  QJNewsTagView.m
//  QJBox
//
//  Created by macm on 2022/7/28.
//

#import "QJNewsTagView.h"
#import "QJNewsTagItem.h"
#import "QJNewsTagModel.h"
@interface QJNewsTagView ()
@property (nonatomic, strong) NSMutableArray<QJNewsTagItem *> *tagList;
// 已选择tag下标数组
@property (nonatomic, strong) NSMutableArray *tagIndexArray;
@end

@implementation QJNewsTagView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setupUI];
    }
    return self;
}

- (void)setupUI {
    self.tagList = [NSMutableArray array];
    self.margin = 8;
    self.top = 16;
    self.tagHeight = 24*kWScale;
}

- (void)setTagModelArray:(NSArray *)tagModelArray {
    //先移除之前的
    if (tagModelArray.count == _tagModelArray.count && _tagModelArray.count!= 0) {
        //直接赋值
        for (int i = 0; i < tagModelArray.count; i++) {
            QJNewsTagItem *tagItem = [self.tagList objectAtIndex:i];
            QJNewsTagModel *model = [tagModelArray objectAtIndex:i];
            tagItem.title = model.name;
        }
        [self setNeedsLayout];
    }else{
        //创建tagView 并赋值
        [self creatTags:tagModelArray];
        
    }
    _tagModelArray = tagModelArray;
}

- (void)setIsSingleChoice:(BOOL)isSingleChoice {
    _isSingleChoice = isSingleChoice;
    
}

- (void)creatTags:(NSArray *)items {
    //先移除
    for (UIView *v in self.tagList) {
        [v removeFromSuperview];
    }
    [self.tagList removeAllObjects];
    
    NSInteger count = items.count;
    for (int i = 0; i < count; i++) {
        QJNewsTagItem *tagItem = [QJNewsTagItem new];
        tagItem.tag = i;
        QJNewsTagModel *model = items[i];
        tagItem.title = model.name;
        @weakify(self);
        tagItem.tagItemClickEvent =^(NSInteger tag){
            
            @strongify(self);
            if (self.isSingleChoice) {
                [self.tagIndexArray removeAllObjects];
                [self.tagIndexArray addObject:[NSString stringWithFormat:@"%ld",tag]];
                [self reloadTagView];
                
            }else{
                if ([self.tagIndexArray containsObject:[NSString stringWithFormat:@"%ld",tag]]) {
                    [self.tagIndexArray removeObject:[NSString stringWithFormat:@"%ld",tag]];
                } else {
                    [self.tagIndexArray addObject:[NSString stringWithFormat:@"%ld",tag]];
                }
                
                [self reloadTagView];
            }
           
        };
        [self addSubview:tagItem];
        [self.tagList addObject:tagItem];
        
        // 如果已选择标签，更改选择标签颜色
        if (self.selectTagArray.count > 0) {
            if ([model.code isEqualToString:self.selectTagArray.firstObject] && _isSingleChoice) {
                [tagItem setBackgroundColor:RGBAlpha(255, 149, 0, 0.1)];
                tagItem.titleColor = UIColorFromRGB(0xff9500);
            } else {
                if ([self.selectTagArray containsObject:model.code]) {
                    [tagItem setBackgroundColor:RGBAlpha(255, 149, 0, 0.1)];
                    tagItem.titleColor = UIColorFromRGB(0xff9500);
                    [self.tagIndexArray addObject:[NSString stringWithFormat:@"%d",i]];
                }
            }
        }
    }
}

// 选中标签，刷新view
- (void)reloadTagView {
    for (QJNewsTagItem *item in self.tagList) {
        if ([self.tagIndexArray containsObject:[NSString stringWithFormat:@"%ld",item.tag]]) {
            [item setBackgroundColor:RGBAlpha(255, 149, 0, 0.1)];
            item.titleColor = UIColorFromRGB(0xff9500);
        } else {
            [item setBackgroundColor:UIColorFromRGB(0xf7f7f7)];
            item.titleColor = UIColorFromRGB(0x1c1c1c);
        }
    }
    [self.selectTagArray removeAllObjects];
    NSMutableArray *modelArr = [NSMutableArray array];
    for (NSString *row in self.tagIndexArray) {
        QJNewsTagModel *model = self.tagModelArray[row.integerValue];
        [self.selectTagArray addObject:model.code];
        [modelArr addObject:model];
    }

    if (self.tagViewClickEvent) {
        self.tagViewClickEvent(self.selectTagArray,modelArr);
    }
}




// 重置标签数组
- (void)resetTagArray {
    [self.tagIndexArray removeAllObjects];
    [self.selectTagArray removeAllObjects];
    [self reloadTagView];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    //重新设置TagView约束
    if (self.tagList.count == self.tagModelArray.count && self.tagList != 0) {
        //重新设置约束
        NSInteger count = self.tagModelArray.count;
        NSInteger margin = self.margin;
        NSInteger top = self.top;
        CGFloat width = self.bounds.size.width;   //总宽度
        CGFloat rowWidth = 0;  //单行内容的宽度
        CGFloat height = self.tagHeight;
        __block BOOL isChange = YES;  //是否需要换行
        QJNewsTagItem *last = nil;
        for (int i = 0; i < count; i++) {
            QJNewsTagItem *tagItem = self.tagList[i];

            //判断宽度是否可以在该行布局 可以布局直接布局 不可以换行
            CGFloat tagWidth = tagItem.viewWidth;
            rowWidth += tagWidth + margin;
            
            if (rowWidth  > width - margin) {      //需要换行
                isChange = YES;
                //判断是否超过最大值
                if (tagWidth + margin *2 > width) {
                    tagWidth = (width - margin*2);
                }
                //换行后重新设置当前行的总宽度
                rowWidth = tagWidth + margin;
            }
            
            [tagItem mas_makeConstraints:^(MASConstraintMaker *make) {
                if (isChange) {  //换行
                    if (!last) {
                        make.top.mas_offset(top);
                    }else{
                        make.top.mas_equalTo(last.mas_bottom).mas_offset(top);
                    }
                    make.left.mas_offset(margin);
                    isChange = NO;
                }else{
                    make.left.mas_equalTo(last.mas_right).mas_offset(margin);
                    make.top.mas_equalTo(last.mas_top);
                }
                make.height.mas_equalTo(height);
                make.width.mas_equalTo(tagWidth);

                //设置最后一个item
                if (i == count -1 && !self.isSingleChoice) {
                    make.bottom.mas_offset(-top);
                }
            }];
            
            last = tagItem;
        }
    }
}

- (NSMutableArray *)tagIndexArray {
    if (!_tagIndexArray) {
        _tagIndexArray = [NSMutableArray array];
    }
    return _tagIndexArray;
}

- (NSMutableArray *)selectTagArray {
    if (!_selectTagArray) {
        _selectTagArray = [NSMutableArray array];
    }
    return _selectTagArray;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
