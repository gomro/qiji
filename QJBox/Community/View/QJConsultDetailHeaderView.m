//
//  QJConsultDetailHeaderView.m
//  QJBox
//
//  Created by Sun on 2022/7/11.
//

#import "QJConsultDetailHeaderView.h"
#import "QJCampsiteUserView.h"
#import "QJCampsiteRequest.h"
#import "QJDetailAlertView.h"
#import "TYAlertController.h"
#import "QJConsultDetailFooterView.h"
#import "QJWKWebViewController.h"
@interface QJConsultDetailHeaderView ()<WKUIDelegate,WKNavigationDelegate>

/**用户*/
@property (nonatomic, strong) QJCampsiteUserView *userView;
/**关注*/
@property (nonatomic, strong) UIButton *attentionBt;
/**标题*/
@property (nonatomic, strong) YYLabel *titleLabel;
/**用户-是否已关注*/
@property (nonatomic, assign) BOOL stared;
/**用户ID*/
@property (nonatomic, copy) NSString *userId;
/**营地id**/
@property (nonatomic, copy) NSString *newsID;

/**视频*/
@property (nonatomic, strong) UIImageView *videoImage;
/**视频播放按钮**/
@property (nonatomic, strong) UIImageView *videoPlayer;
/**web视图**/
@property (nonatomic, strong) WKWebView *webView;
/**web高度**/
@property (nonatomic, assign) CGFloat height;

/**资讯底部视图**/
@property (nonatomic, strong) QJConsultDetailFooterView *footerView;


@property (nonatomic, strong) UIView *lineView;

@end
@implementation QJConsultDetailHeaderView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.newsID = @"";
        self.height = 50;
        self.backgroundColor = [UIColor whiteColor];
        [self addSubview:self.titleLabel];
        [self addSubview:self.attentionBt];
        [self addSubview:self.userView];
        [self addSubview:self.videoImage];
        [self.videoImage addSubview:self.videoPlayer];
        [self addSubview:self.webView];
        [self addSubview:self.footerView];
        [self addSubview:self.lineView];
        [self makeConstraints];
        
        UITapGestureRecognizer *tapView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapUserView)];
        [self.userView addGestureRecognizer:tapView];
    }
    return self;
}
- (void)tapUserView {
    if (self.headerDelegate && [self.headerDelegate respondsToSelector:@selector(didClickUserView)]) {
        [self.headerDelegate didClickUserView];
    }
}

- (void)makeConstraints {
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(16);
        make.right.equalTo(self.mas_right).offset(-16);
        make.top.equalTo(self.mas_top).offset(8);
    }];
    
    [self.userView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(16);
        make.top.equalTo(self.titleLabel.mas_bottom).offset(10);
        make.width.mas_lessThanOrEqualTo(200*kWScale);
        make.height.mas_equalTo(40*kWScale);
    }];
    
    [self.attentionBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(-16);
        make.height.mas_equalTo(28*kWScale);
        make.width.mas_equalTo(60*kWScale);
        make.centerY.equalTo(self.userView);
    }];
    
    [self.videoImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.userView.mas_bottom).offset(0);
        make.left.equalTo(self).mas_equalTo(16);
        make.right.equalTo(self).mas_equalTo(-16);
        make.height.mas_equalTo(0);
    }];
    [self.videoPlayer mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self.videoImage);
        make.width.height.mas_equalTo(40*kWScale);
    }];
    
    [self.webView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.videoImage.mas_bottom);
        make.left.equalTo(self).offset(10);
        make.right.equalTo(self).offset(-8);
        make.height.mas_equalTo(0);
    }];
    
    [self.footerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.webView.mas_bottom);
        make.left.right.equalTo(self);
        make.height.mas_equalTo(0);
    }];
    
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.footerView.mas_bottom).offset(10);
        make.left.right.equalTo(self);
        make.height.mas_equalTo(8);
    }];
}
#pragma mark - Lazy Loading

- (QJCampsiteUserView *)userView {
    if (!_userView) {
        _userView = [[QJCampsiteUserView alloc] init];
    }
    return _userView;
}

- (UIButton *)attentionBt {
    if (!_attentionBt) {
        _attentionBt = [UIButton new];
        _attentionBt.backgroundColor = UIColorHex(1F2A4D);
        [_attentionBt setTitle:@"+ 关注" forState:UIControlStateNormal];
        [_attentionBt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _attentionBt.titleLabel.font = MYFONTALL(FONT_REGULAR, 12);
        _attentionBt.layer.masksToBounds = YES;
        _attentionBt.layer.cornerRadius = 2;
        [_attentionBt addTarget:self action:@selector(attentionUser:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _attentionBt;
}

- (YYLabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [YYLabel new];
        _titleLabel.font = MYFONTALL(FONT_BOLD, 16);
        _titleLabel.numberOfLines = 0;
        _titleLabel.textColor = UIColorHex(16191C);
    }
    return _titleLabel;
    
}
- (UIImageView *)videoImage {
    if (!_videoImage) {
        _videoImage = [UIImageView new];
        _videoImage.contentMode = UIViewContentModeScaleAspectFill;
        _videoImage.clipsToBounds = YES;
        _videoImage.layer.cornerRadius = Get375Width(4);
        _videoImage.userInteractionEnabled = YES;
        _videoImage.hidden = YES;
    }
    return _videoImage;
}

- (UIImageView *)videoPlayer {
    if (!_videoPlayer) {
        _videoPlayer = [UIImageView new];
        _videoPlayer.image = [UIImage imageNamed:@"qj_home_play"];
        _videoPlayer.layer.masksToBounds = YES;
        _videoPlayer.userInteractionEnabled = YES;
    }
    return _videoPlayer;
}

- (WKWebView *)webView{
    if (!_webView) {
        NSString *jScript = @"var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport'); meta.setAttribute('content', 'width=device-width'); document.getElementsByTagName('head')[0].appendChild(meta); var imgs = document.getElementsByTagName('img');for (var i in imgs){imgs[i].style.maxWidth='100%';imgs[i].style.height='auto';}";
        WKUserScript *wkUScript = [[WKUserScript alloc] initWithSource:jScript injectionTime:WKUserScriptInjectionTimeAtDocumentEnd forMainFrameOnly:YES];
        WKUserContentController *wkUController = [[WKUserContentController alloc] init];
        [wkUController addUserScript:wkUScript];
        WKWebViewConfiguration *wkWebConfig = [[WKWebViewConfiguration alloc] init];
        wkWebConfig.userContentController = wkUController;
        _webView = [[WKWebView alloc] initWithFrame:CGRectZero configuration:wkWebConfig];
        _webView.navigationDelegate = self;
        _webView.UIDelegate=self;
        _webView.scrollView.scrollEnabled = NO;
        [_webView.scrollView addObserver:self forKeyPath:@"contentSize" options:NSKeyValueObservingOptionNew context:nil];
    }
    return _webView;
}

- (QJConsultDetailFooterView *)footerView {
    if(!_footerView) {
        _footerView = [[QJConsultDetailFooterView alloc] init];
        _footerView.hidden = YES;
    }
    return _footerView;
}

- (UIView *)lineView {
    if (!_lineView) {
        _lineView = [UIView new];
        _lineView.backgroundColor = UIColorHex(F9F9F9);
    }
    return _lineView;
}


- (void)configureDetailHeaderWithData:(QJCampsiteDetailModel *)dataModel {
    NSString *userIdSave = LSUserDefaultsGET(kQJUserId);
    BOOL isSelf = NO;
    if ([kCheckStringNil(userIdSave) isEqualToString:kCheckStringNil(dataModel.userId)] && ![kCheckStringNil(dataModel.userId) isEqualToString:@""]) {
        isSelf = YES;
    }
    BOOL isCancle = dataModel.cancel == 0 || dataModel.cancel == 1;
    if (isSelf || !isCancle) {
        self.attentionBt.hidden = YES;
    } else {
        self.attentionBt.hidden = NO;
    }
    self.newsID = dataModel.newsId;
    NSString *textString = kCheckStringNil(dataModel.newsTitle);
    NSMutableAttributedString *mutableAttributedString = [[NSMutableAttributedString alloc] initWithString:textString];
    mutableAttributedString.font = MYFONTALL(FONT_BOLD, 16);
    mutableAttributedString.color = UIColorHex(16191C);
    mutableAttributedString.lineSpacing = 10;
    self.titleLabel.attributedText = mutableAttributedString;
    [self.titleLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(dataModel.titleHeight);
    }];
    
    [self.userView configureHeaderlWithData:dataModel];
    self.stared = dataModel.stared;
    self.userId = kCheckStringNil(dataModel.userId);
    [self changeAttention];
    
    if (![kCheckStringNil(dataModel.videoAddress) isEqualToString:@""]) {
        self.videoImage.hidden = NO;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(playNow)];
        UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(playNow)];
        [self.videoImage addGestureRecognizer:tap];
        [self.videoPlayer addGestureRecognizer:tap1];
            
        NSString *imageString = [kCheckStringNil(dataModel.coverImage) checkImageUrlString];
        NSURL *imageURL = [NSURL URLWithString:imageString];
        [self.videoImage setImageURL:imageURL];
        [self.videoImage mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.userView.mas_bottom).offset(15);
            make.height.mas_equalTo(188*kWScale);
        }];
    }
    [self configureCellWithDataModel:kCheckStringNil(dataModel.htmlContent)];

}

- (void)configureDetailHeaderWithNewsData:(QJNewsListModel *)dataModel {
    NSString *userIdSave = LSUserDefaultsGET(kQJUserId);
    BOOL isSelf = NO;
    if ([kCheckStringNil(userIdSave) isEqualToString:kCheckStringNil(dataModel.userId)] && ![kCheckStringNil(dataModel.userId) isEqualToString:@""]) {
        isSelf = YES;
    }
    BOOL isCancle = dataModel.cancel == 0 || dataModel.cancel == 1;
    if (isSelf || !isCancle) {
        self.attentionBt.hidden = YES;
    } else {
        self.attentionBt.hidden = NO;
    }
    self.newsID = @"";
    NSString *textString = kCheckStringNil(dataModel.infoTitle);
    NSMutableAttributedString *mutableAttributedString = [[NSMutableAttributedString alloc] initWithString:textString];
    mutableAttributedString.font = MYFONTALL(FONT_BOLD, 16);
    mutableAttributedString.color = UIColorHex(16191C);
    mutableAttributedString.lineSpacing = 10;
    self.titleLabel.attributedText = mutableAttributedString;
    [self.titleLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(dataModel.titleHeight);
    }];
    
    [self.userView configureHeaderNewsWithData:dataModel];
    self.stared = dataModel.stared;
    self.userId = kCheckStringNil(dataModel.userId);
    [self changeAttention];
    
    if (![kCheckStringNil(dataModel.videoAddress) isEqualToString:@""]) {
        self.videoImage.hidden = NO;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(playNow)];
        UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(playNow)];
        [self.videoImage addGestureRecognizer:tap];
        [self.videoPlayer addGestureRecognizer:tap1];
        NSString *imageString = [kCheckStringNil(dataModel.infoCoverImage) checkImageUrlString];
        NSURL *imageURL = [NSURL URLWithString:imageString];
        [self.videoImage setImageURL:imageURL];
        [self.videoImage mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.userView.mas_bottom).offset(15);
            make.height.mas_equalTo(188*kWScale);
        }];
    }
    self.footerView.hidden = NO;
    [self configureCellWithDataModel:kCheckStringNil(dataModel.infoHtmlContent)];
    [self.footerView configureDetailFooterWithNewsData:dataModel];
    [self.footerView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(140*kWScale);
    }];
}

- (void)playNow {
    if (self.headerDelegate && [self.headerDelegate respondsToSelector:@selector(didVideoImageUrl:)]) {
        [self.headerDelegate didVideoImageUrl:self];
    }
}

- (void)changeAttention {
    if (self.stared) {
        _attentionBt.backgroundColor = UIColorHex(919599);
        [_attentionBt setTitle:@"已关注" forState:UIControlStateNormal];
        [_attentionBt setTitleColor:UIColorHex(919599) forState:UIControlStateNormal];
        _attentionBt.backgroundColor = [UIColor whiteColor];
        _attentionBt.layer.borderColor = UIColorHex(919599).CGColor;
        _attentionBt.layer.borderWidth = 1;
        
    } else {
        _attentionBt.backgroundColor = UIColorHex(1F2A4D);
        [_attentionBt setTitle:@"+ 关注" forState:UIControlStateNormal];
        [_attentionBt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _attentionBt.backgroundColor = UIColorHex(1F2A4D);
        _attentionBt.layer.borderColor = UIColorHex(1F2A4D).CGColor;
        _attentionBt.layer.borderWidth = 0;
    }
}

- (void)attentionUser:(UIButton *)sender {
    if (self.stared) {
        [QJAppTool hideHUDLoading];
        QJDetailAlertView *alertView = [[QJDetailAlertView alloc] initWithFrame:CGRectZero titleType:QJDetailAlertViewTitleTypeDefault buttonType:QJDetailAlertViewButtonTypeDefault];
        [alertView showTitleText:@"提示" describeText:@"确定要取消关注吗?"];
        [alertView.leftButton setTitle:@"取消" forState:UIControlStateNormal];
        [alertView.rightButton setTitle:@"确认" forState:UIControlStateNormal];
        [alertView.rightButton setTitleColor:UIColorHex(007AFF) forState:UIControlStateNormal];
        MJWeakSelf
        [alertView returnClick:^(QJDetailAlertViewButtonClickType type) {
            [[QJAppTool getCurrentViewController] dismissViewControllerAnimated:YES completion:nil];
            if (type == QJDetailAlertViewButtonClickTypeRight) {
                [weakSelf postAttention:@"0"];
            }
        }];
        TYAlertController *alertController = [TYAlertController alertControllerWithAlertView:alertView preferredStyle:TYAlertControllerStyleAlert];
        [[QJAppTool getCurrentViewController] presentViewController:alertController animated:YES completion:nil];
    } else {
        [self postAttention:@"1"];
    }

}

- (void)postAttention:(NSString *)type {
    [QJAppTool showHUDLoading];
    QJCampsiteRequest *startRequest = [[QJCampsiteRequest alloc] init];
    [startRequest netWorkPostAttentionUserid:self.userId type:type newsId:self.newsID];
    [startRequest setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        [QJAppTool hideHUDLoading];
        if (ResponseSuccess) {
            if ([type isEqualToString:@"1"]) {
                [QJAppTool showToast:@"关注成功"];
                self.stared = YES;
            } else {
                self.stared = NO;
                [QJAppTool showToast:@"取消关注"];
            }
            [self changeAttention];
        }  else {
            NSString *message = request.responseJSONObject[@"message"];
            [QJAppTool showToast:kCheckStringNil(message)];
        }
    }];
    [startRequest setFailureCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        [QJAppTool hideHUDLoading];
    }];
}





- (void)configureCellWithDataModel:(NSString *)htmlContent {
    NSString *headerString = @"<header><meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no'><style>img{max-width:100%, border-radius:20px}</style></header>";
    [self.webView loadHTMLString:[headerString stringByAppendingString:kCheckStringNil(htmlContent)] baseURL:nil];
        
}



- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context{

}



#pragma mark WKWebView
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    NSString *js1 = [NSString stringWithFormat:@"document.documentElement.style.webkitUserSelect='none';"];
    NSString *js2 = [NSString stringWithFormat:@"document.documentElement.style.webkitTouchCallout='none';"];
    [self.webView evaluateJavaScript:js1 completionHandler:nil];
    [self.webView evaluateJavaScript:js2 completionHandler:nil];

    MJWeakSelf
    [webView evaluateJavaScript:@"document.body.scrollHeight"
              completionHandler:^(id result, NSError *_Nullable error) {
       
        CGFloat newHeight = weakSelf.webView.scrollView.contentSize.height;
        if (weakSelf.height && newHeight == weakSelf.height) {
            return;
        }
        self.height = newHeight;
        weakSelf.webView.frame=CGRectMake(0, 0, QJScreenWidth, newHeight);
        [self.webView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(newHeight);
        }];
        weakSelf.webView.height = newHeight;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            if (weakSelf.headerDelegate && [weakSelf.headerDelegate respondsToSelector:@selector(changeWebViewHeight:)]) {
                [weakSelf.headerDelegate changeWebViewHeight:newHeight];
            }
        });
      
    }];
    
}

- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(nonnull WKNavigationAction *)navigationAction decisionHandler:(nonnull void (^)(WKNavigationActionPolicy))decisionHandler {
    NSString *requestString = [navigationAction.request.URL.absoluteString stringByRemovingPercentEncoding];
//    NSArray *components = [requestString componentsSeparatedByString:@":"];
//    if(navigationAction.navigationType == WKNavigationTypeLinkActivated && requestString.length > 0){
//        QJWKWebViewController *webVC = [QJWKWebViewController new];
//        webVC.url = requestString;
//        [[QJAppTool getCurrentViewController].navigationController pushViewController:webVC animated:YES];
//    }
   
    decisionHandler(WKNavigationActionPolicyAllow);
}

- (void)dealloc{
    [_webView.scrollView removeObserver:self forKeyPath:@"contentSize"];
}
@end
