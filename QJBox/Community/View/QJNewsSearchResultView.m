//
//  QJNewsSearchResultView.m
//  QJBox
//
//  Created by macm on 2022/7/27.
//

#import "QJNewsSearchResultView.h"
#import "QJSegmentLineView.h"
#import "QJCampsiteRequest.h"
#import "QJNewsCategoryModel.h"

@interface QJNewsSearchResultView ()
@property (nonatomic, strong) UIView *segmentView;
@property (nonatomic, strong) QJSegmentLineView *segment;
@property (nonatomic, strong) NSMutableArray *titleArray;
@property (nonatomic, strong) UIButton *hotButton;
@property (nonatomic, strong) UIButton *newButton;
@end

@implementation QJNewsSearchResultView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        self.sortString = @"HD";
        self.categoryCode = @"";
        [self createView];
        [self getCategoryList];
    }
    return self;
}

- (void)getCategoryList {
    QJCampsiteRequest *startRequest = [[QJCampsiteRequest alloc] init];
    [startRequest netWorkGetInfoCategory];
    [startRequest setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            self.titleArray = [NSMutableArray arrayWithCapacity:0];
            NSMutableArray *segmentArray = [NSMutableArray array];

            QJNewsCategoryModel *model = [[QJNewsCategoryModel alloc] init];
            model.name = @"综合";
            model.code = @"";
            [self.titleArray addObject:model];
            [segmentArray addObject:model.name];
            
            NSArray *dataArr = request.responseJSONObject[@"data"];
            for (NSDictionary *dic in dataArr) {
                QJNewsCategoryModel *model = [QJNewsCategoryModel modelWithDictionary:dic];
                [self.titleArray addObject:model];
                [segmentArray addObject:model.name];
            }
            
            [self.segment setTitleArray:segmentArray];

        }
    }];
}

- (void)createView {
    [self addSubview:self.segmentView];
    [self.segmentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@10);
        make.left.right.equalTo(self);
        make.height.equalTo(@(70*kWScale));
    }];
    
    [self.segmentView addSubview:self.segment];
    
    [self.segmentView addSubview:self.hotButton];
    [self.hotButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-(kScreenWidth/2));
        make.width.mas_equalTo(60*kWScale);
        make.height.mas_equalTo(30*kWScale);
        make.top.mas_equalTo(30);
    }];
    
    [self.segmentView addSubview:self.newButton];
    [self.newButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kScreenWidth/2);
        make.width.mas_equalTo(60*kWScale);
        make.height.mas_equalTo(30*kWScale);
        make.top.mas_equalTo(30);
    }];
    
    [self addSubview:self.listView];
    [self.listView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.segmentView.mas_bottom);
        make.left.right.bottom.mas_equalTo(0);
    }];
    
}

- (UIView *)segmentView{
    if (!_segmentView) {
        _segmentView = [UIView new];
        _segmentView.backgroundColor = [UIColor whiteColor];
    }
    return _segmentView;
}

// 最热
- (void)hotAction {
    self.sortString = @"HD";
    [_hotButton setTitleColor:UIColorFromRGB(0xff9500) forState:UIControlStateNormal];
    [_newButton setTitleColor:UIColorFromRGB(0x16191c) forState:UIControlStateNormal];
    [_hotButton.titleLabel setFont:FontSemibold(16)];
    [_newButton.titleLabel setFont:FontRegular(16)];
    if (self.didSelectedSort) {
        self.didSelectedSort();
    }
}
// 最新
- (void)newAction {
    self.sortString = @"TD";
    [_newButton setTitleColor:UIColorFromRGB(0xff9500) forState:UIControlStateNormal];
    [_hotButton setTitleColor:UIColorFromRGB(0x16191c) forState:UIControlStateNormal];
    [_newButton.titleLabel setFont:FontSemibold(16)];
    [_hotButton.titleLabel setFont:FontRegular(16)];
    if (self.didSelectedSort) {
        self.didSelectedSort();
    }
}

// 重置搜索条件
- (void)resetSearchSort {
    [self setHidden:YES];
    
    self.sortString = @"HD";
    [_hotButton setTitleColor:UIColorFromRGB(0xff9500) forState:UIControlStateNormal];
    [_newButton setTitleColor:UIColorFromRGB(0x16191c) forState:UIControlStateNormal];
    [_hotButton.titleLabel setFont:FontSemibold(16)];
    [_newButton.titleLabel setFont:FontRegular(16)];
    
    [self.segment setSegmentSelectedIndex:0];
    QJNewsCategoryModel *model = self.titleArray[0];
    self.categoryCode = model.code;
}

- (QJSegmentLineView *)segment{
    if (!_segment) {
        _segment = [[QJSegmentLineView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 30)];
        _segment.backgroundColor = [UIColor whiteColor];
        _segment.segmentWidthStyle = QJSegmentViewWidthStyleAuto;
        _segment.itemTextOffset = 5;
        _segment.leftOffset = 15;
        _segment.font = kFont(14);
        _segment.selectedFont = kboldFont(18);
        _segment.textColor = UIColorFromRGB(0x000000);
        _segment.selectedTextColor = UIColorFromRGB(0x000000);
        _segment.showBottomLine = NO;
        _segment.tag = 10000;
        WS(weakSelf)
        [_segment setSegmentedItemSelectedBlock:^(QJSegmentLineView * _Nonnull segment, NSInteger selectedIndex) {
            QJNewsCategoryModel *model = weakSelf.titleArray[selectedIndex];
            weakSelf.categoryCode = model.code;
            if (weakSelf.didSelectedSort) {
                weakSelf.didSelectedSort();
            }
        }];
    }
    return _segment;
}

- (UIButton *)hotButton {
    if (!_hotButton) {
        _hotButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_hotButton setTitleColor:UIColorFromRGB(0xff9500) forState:UIControlStateNormal];
        [_hotButton.titleLabel setFont:FontSemibold(16)];
        [_hotButton setTitle:@"最热" forState:UIControlStateNormal];
        [_hotButton addTarget:self action:@selector(hotAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _hotButton;
}

- (UIButton *)newButton {
    if (!_newButton) {
        _newButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_newButton setTitleColor:UIColorFromRGB(0x16191c) forState:UIControlStateNormal];
        [_newButton.titleLabel setFont:FontRegular(16)];
        [_newButton setTitle:@"最新" forState:UIControlStateNormal];
        [_newButton addTarget:self action:@selector(newAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _newButton;
}

- (QJNewsSearchResultListView *)listView {
    if (!_listView) {
        _listView = [[QJNewsSearchResultListView alloc] init];
    }
    return _listView;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
