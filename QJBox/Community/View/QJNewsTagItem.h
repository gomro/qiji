//
//  QJNewsTagItem.h
//  QJBox
//
//  Created by macm on 2022/7/28.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJNewsTagItem : UIView

//文字
@property (nonatomic, copy) NSString *title;

//默认 Black
@property (nonatomic, strong) UIColor *titleColor;

//默认12
@property (nonatomic, strong) UIFont *font;

//默认14
@property (nonatomic, assign) CGFloat leftMargin;

//默认5
@property (nonatomic, assign) CGFloat topMargin;

//根据文字自动计算出内容的宽度
@property (nonatomic, assign) CGFloat viewWidth;

@property (nonatomic, copy) void (^tagItemClickEvent)(NSInteger index);

@end

NS_ASSUME_NONNULL_END
