//
//  QJFontStyleChoiceBar.m
//  QJBox
//
//  Created by wxy on 2022/8/3.
//

#import "QJFontStyleChoiceBar.h"
#import <RZRichTextView-Swift.h>

@interface QJFontStyleChoiceBar ()



@property (nonatomic, strong) NSMutableArray *btnArr;

@end


@implementation QJFontStyleChoiceBar


- (instancetype)initWithFrame:(CGRect)frame options:(RZRichTextViewOptions *)options {
    if (self = [super initWithFrame:frame]) {
        self.options = options;
        self.backgroundColor = kColorWithHexString(@"#F5F5F5");
        NSArray *arr = @[@"标题",@"正文",@"加粗",@"下划线",@"斜体"];
        UIScrollView *bgView = [UIScrollView new];
        bgView.backgroundColor =  kColorWithHexString(@"#F5F5F5");
        bgView.contentSize = CGSizeMake(414, 48);
        bgView.showsHorizontalScrollIndicator = NO;
        [self addSubview:bgView];
        [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
        }];
        self.btnArr = [NSMutableArray array];
        for (int i = 0; i < arr.count; i++) {
            
            UIButton *btn = [UIButton new];
            btn.tag = 1000 + i;
            [btn addTarget:self action:@selector(btnAction:) forControlEvents:UIControlEventTouchUpInside];
            [btn setTitleColor:kColorWithHexString(@"#16191C") forState:UIControlStateNormal];
            [btn setTitleColor:kColorWithHexString(@"#FF9500") forState:UIControlStateSelected];
            btn.titleLabel.font = FontRegular(16);
            [btn setTitle:arr[i] forState:UIControlStateNormal];
            [bgView addSubview:btn];
            [self.btnArr safeAddObject:btn];
            
            if (i == 0) {
                [btn mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.equalTo(bgView).offset(4);
                    make.width.equalTo(@56);
                    make.height.equalTo(@48);
                    make.top.equalTo(bgView);
                }];
                
            }else if (i == 2) {
                UIView *vLine = [UIView new];
                [bgView addSubview:vLine];
                UIButton *berforBtn = [self viewWithTag:(1000 + i-1)];
                [btn mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.equalTo(berforBtn.mas_right);
                    make.width.equalTo(@56);
                    make.height.equalTo(@48);
                    make.centerY.equalTo(bgView);
                }];
                vLine.backgroundColor = kColorWithHexString(@"#C8CACC");
                [vLine mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.equalTo(btn.mas_right).offset(12);
                    make.width.equalTo(@1);
                    make.height.equalTo(@16);
                    make.centerY.equalTo(bgView);
                }];
                
                
                
            }else if (i == 3){
                UIButton *berforBtn = [self viewWithTag:1002];
                [btn mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.equalTo(berforBtn.mas_right).offset(24);
                    make.width.equalTo(@56);
                    make.height.equalTo(@48);
                    make.centerY.equalTo(bgView);
                }];
            }else{
                UIButton *berforBtn = [self viewWithTag:(1000 + i-1)];
                [btn mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.equalTo(berforBtn.mas_right);
                    make.width.equalTo(@56);
                    make.height.equalTo(@48);
                    make.centerY.equalTo(bgView);
                }];
                
            }
            
        }
        
    }
    return self;
    
}


#pragma mark ------ btnAction

- (void)btnAction:(UIButton *)sender {
    sender.selected = !sender.selected;
    NSMutableArray *muArr = [NSMutableArray array];
    BOOL contain = (sender.tag == 1000 || sender.tag == 1001 || sender.tag == 1002);
    if (contain) {
        for (UIButton *btn in self.btnArr.copy) {
            if (btn.selected) {
                if (btn.tag != sender.tag && (btn.tag == 1000 || btn.tag == 1001 || btn.tag == 1002)) {
                    btn.titleLabel.font = FontRegular(16);
                    btn.selected = NO;
                }
            }
        }
    }
    
    for (UIButton *btn in self.btnArr.copy) {
        if (btn.selected) {
            btn.titleLabel.font = FontSemibold(16);
            [muArr safeAddObject:@(btn.tag - 1000)];
        }else{
            btn.titleLabel.font = FontRegular(16);
        }
    }
    NSMutableDictionary *typingAttributes = [NSMutableDictionary dictionary];
    
    [typingAttributes setValue:self.options.normalFont forKey:NSFontAttributeName];
    [typingAttributes setValue:kColorWithHexString(@"16191C") forKey:NSForegroundColorAttributeName];
    if ([muArr containsObject:@0]) {//标题
        [typingAttributes setValue:self.options.titleFont forKey:NSFontAttributeName];
        [typingAttributes setValue:kColorWithHexString(@"16191C") forKey:NSForegroundColorAttributeName];
    }
    if ([muArr containsObject:@1]) {//正文
        [typingAttributes setValue:self.options.normalFont forKey:NSFontAttributeName];
        [typingAttributes setValue:kColorWithHexString(@"16191C") forKey:NSForegroundColorAttributeName];
    }
    if ([muArr containsObject:@2]) {//加粗
        [typingAttributes setValue:self.options.boldFont forKey:NSFontAttributeName];
    }
    CGFloat value = 0;
    if([UIDevice currentDevice].systemVersion.floatValue <16.0){
        value = 0.3;
    }
    if ([muArr containsObject:@4]) {//正文斜体
        [typingAttributes setValue:self.options.obliqueFont forKey:NSFontAttributeName];
        [typingAttributes setValue:@(value) forKey:NSObliquenessAttributeName];
        
    }
    if ([muArr containsObject:@2] && [muArr containsObject:@4]) {//加粗斜体
        [typingAttributes setValue:self.options.boldObliqueFont forKey:NSFontAttributeName];
        [typingAttributes setValue:@(value) forKey:NSObliquenessAttributeName];
    }
    
    if ([muArr containsObject:@1] && [muArr containsObject:@4]) {//正文斜体
        [typingAttributes setValue:self.options.obliqueFont forKey:NSFontAttributeName];
        [typingAttributes setValue:@(value) forKey:NSObliquenessAttributeName];
        
    }
    
    if ([muArr containsObject:@0] && [muArr containsObject:@4]) {//标题斜体
        [typingAttributes setValue:self.options.titleObliqueFont forKey:NSFontAttributeName];
        [typingAttributes setValue:@(value) forKey:NSObliquenessAttributeName];
        
    }
    
    if ([muArr containsObject:@3]) {//下划线
        [typingAttributes setValue:@1 forKey:NSUnderlineStyleAttributeName];
    }
    
    
    
    
    
    if (self.choiceIndex) {
        self.choiceIndex(typingAttributes.copy);
    }
}



#pragma mark -- getter setter




- (void)setTypingAttributes:(NSDictionary *)typingAttributes {
    _typingAttributes = typingAttributes;
    UIFont *font = typingAttributes[NSFontAttributeName];
    int under = [typingAttributes[NSUnderlineStyleAttributeName] intValue];
    
    UIColor *color = typingAttributes[NSForegroundColorAttributeName];
    NSMutableArray *selecteIndexArr = [NSMutableArray array];
    if ([self fontIsBold:font]) {//加粗
        [selecteIndexArr addObject:@2];
    }
    if ([self fontIsOblique:font]) {//斜体
        [selecteIndexArr addObject:@4];
    }
    if (under != 0) {
        [selecteIndexArr addObject:@3];
    }
    if ([self fontIsTitle:font color:color]) {
        [selecteIndexArr addObject:@0];
    }
    if ([self fontIsContent:font color:color]) {
        [selecteIndexArr addObject:@1];
    }
    
    for (UIButton *btn in self.btnArr.copy) {
        btn.titleLabel.font = FontRegular(16);
        btn.selected = NO;
    }
    
    for (int i = 0; i < selecteIndexArr.count; i++) {
        int index = [[selecteIndexArr safeObjectAtIndex:i] intValue];
        UIButton *btn = [self viewWithTag:(1000 + index)];
        btn.titleLabel.font = FontSemibold(16);
        btn.selected = YES;
    }
    
    
    
}

//是否加粗
- (BOOL)fontIsBold:(UIFont *)font {
    return font == self.options.boldFont  || font  == self.options.boldObliqueFont;
    
}

//是否斜体
- (BOOL)fontIsOblique:(UIFont *)font {
    return font.fontName == self.options.obliqueFont.fontName || font.fontName == self.options.boldObliqueFont.fontName || font.fontName == self.options.titleObliqueFont.fontName;
    
}

//是否正文
- (BOOL)fontIsContent:(UIFont *)font color:(UIColor *)color{
    return (font == self.options.normalFont) ||  (font == self.options.obliqueFont) ;
}

//是否标题
- (BOOL)fontIsTitle:(UIFont *)font color:(UIColor *)color{
    return (font == self.options.titleFont) || (font == self.options.titleObliqueFont);
}



@end
