//
//  QJConsultDetailBottomView.m
//  QJBox
//
//  Created by Sun on 2022/7/11.
//

#import "QJConsultDetailBottomView.h"
#import "QJImageButton.h"
#import "QJCampsiteRequest.h"

@interface QJConsultDetailBottomView ()
/**背景*/
@property (nonatomic, strong) UIButton *backView;
/**输入框文字*/
@property (nonatomic, strong) UILabel *textLb;
/**点赞*/
@property (nonatomic, strong) QJImageButton *likeBt;
/**收藏*/
@property (nonatomic, strong) QJImageButton *saveBt;
@property (nonatomic, strong) QJCampsiteDetailModel *dataModel;
@property (nonatomic, strong) QJNewsListModel *dataNewsModel;
/**是否是资讯*/
@property (nonatomic, assign) BOOL isNews;

@end
@implementation QJConsultDetailBottomView
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:self.backView];
        [self.backView addSubview:self.textLb];
        [self addSubview:self.likeBt];
        [self addSubview:self.saveBt];
        [self makeConstraints];
    }
    return self;
}

- (void)makeConstraints {
    [self.saveBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(10);
        make.right.equalTo(self.mas_right).offset(-24);
        make.width.mas_lessThanOrEqualTo(24*kWScale);
    }];
    
    [self.likeBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(10);
        make.right.equalTo(self.saveBt.mas_left).offset(-24);
        make.width.mas_lessThanOrEqualTo(24*kWScale);
    }];
    
    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(16);
        make.centerY.equalTo(self);
        make.right.equalTo(self.mas_right).offset(-120);
        make.height.mas_equalTo(40*kWScale);
    }];
    
    [self.textLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(32*kWScale);
        make.centerY.equalTo(self.backView);
        make.left.equalTo(self.backView.mas_left).offset(17);
    }];
    
}

- (UIButton *)backView {
    if (!_backView) {
        _backView = [UIButton new];
        _backView.backgroundColor = UIColorHex(F5F5F5);
        _backView.layer.masksToBounds = YES;
        _backView.layer.cornerRadius = 20;
        [_backView addTarget:self action:@selector(textClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _backView;
}

- (UILabel *)textLb {
    if (!_textLb) {
        _textLb = [UILabel new];
        _textLb.text = @"评论";
        _textLb.textColor = UIColorHex(919599);
        _textLb.font = MYFONTALL(FONT_REGULAR, 14);
    }
    return _textLb;
}

- (QJImageButton *)likeBt {
    if (!_likeBt) {
        _likeBt = [QJImageButton new];
        _likeBt.titleString = @"0";
        _likeBt.titleLb.font = MYFONTALL(FONT_BOLD, 10);
        _likeBt.titleLb.textColor = UIColorHex(919599);
        _likeBt.iconString = @"qj_like_24";
        _likeBt.imageSize = CGSizeMake(24, 24);
        _likeBt.space = 2;
        _likeBt.style = ImageButtonStyleTop;
        [_likeBt addTarget:self action:@selector(likeClick) forControlEvents:UIControlEventTouchUpInside];        
    }
    return _likeBt;
}

- (QJImageButton *)saveBt {
    if (!_saveBt) {
        _saveBt = [QJImageButton new];
        _saveBt.titleString = @"0";
        _saveBt.titleLb.font = MYFONTALL(FONT_BOLD, 10);
        _saveBt.titleLb.textColor = UIColorHex(919599);
        _saveBt.iconString = @"collectSaveIcon";
        _saveBt.imageSize = CGSizeMake(24, 24);
        _saveBt.space = 2;
        _saveBt.style = ImageButtonStyleTop;
        [_saveBt addTarget:self action:@selector(saveClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _saveBt;
}

- (void)configureDetailBottomWithData:(QJCampsiteDetailModel *)dataModel {
    self.dataModel = dataModel;
    self.textLb.text = @"评论";
    if ([kCheckStringNil(dataModel.newsCommentCount) integerValue]) {
        self.textLb.text = [NSString stringWithFormat:@"评论（已有%@条评论）", [NSString getConvertTenThousandNumber:[dataModel.newsCommentCount integerValue]]];

    }
    self.likeBt.titleString = [NSString getConvertTenThousandNumber:[dataModel.newsLikeCount integerValue]];
    self.saveBt.titleString = [NSString getConvertTenThousandNumber:[dataModel.newsCollectCount integerValue]];

    if (dataModel.liked) {
        self.likeBt.iconString = @"likeIconSelected";
    } else {
        self.likeBt.iconString = @"likeIcon";
    }
    
    if (dataModel.collected) {
        self.saveBt.iconString = @"collectSaveIconSelected";
    } else {
        self.saveBt.iconString = @"collectSaveIcon";
    }
}

- (void)configureDetailBottomWithNewsData:(QJNewsListModel *)dataModel {
    self.isNews = YES;
    self.dataNewsModel = dataModel;
    self.textLb.text = @"评论";
    if (dataModel.infoCommentCount) {
        self.textLb.text = [NSString stringWithFormat:@"评论（已有%@条评论）", [NSString getConvertTenThousandNumber:dataModel.infoCommentCount]];
    }
    self.likeBt.titleString = [NSString getConvertTenThousandNumber:dataModel.infoLikeCount];
    self.saveBt.titleString = [NSString getConvertTenThousandNumber:dataModel.infoCollectCount];

    if (dataModel.liked) {
        self.likeBt.iconString = @"likeIconSelected";
    } else {
        self.likeBt.iconString = @"likeIcon";
    }
    
    if (dataModel.collected) {
        self.saveBt.iconString = @"collectSaveIconSelected";
    } else {
        self.saveBt.iconString = @"collectSaveIcon";
    }
}

- (void)likeClick {
//    [QJAppTool showHUDLoading];
    QJCampsiteRequest *startRequest = [[QJCampsiteRequest alloc] init];

    if (self.isNews) {
        NSString *campsiteId = kCheckStringNil(self.dataNewsModel.infoId);
        BOOL liked = self.dataNewsModel.liked;
        NSString *action = @"1";
        if (liked) {
            action = @"0";
        }
        [startRequest netWorkPutCampLike:campsiteId type:@"1" action:action];
        [startRequest setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
            [QJAppTool hideHUDLoading];
            if (ResponseSuccess) {
                
                NSInteger newsLikeCount = self.dataNewsModel.infoLikeCount;
                if ([action isEqualToString:@"1"]) {
//                    [[QJAppTool getCurrentViewController].view makeToast:@"点赞成功"];
                    self.dataNewsModel.liked = YES;
                    newsLikeCount = newsLikeCount + 1;
                    self.dataNewsModel.infoLikeCount = newsLikeCount;
                } else {
//                    [[QJAppTool getCurrentViewController].view makeToast:@"取消点赞"];
                    self.dataNewsModel.liked = NO;
                    newsLikeCount = newsLikeCount - 1;
                    self.dataNewsModel.infoLikeCount = newsLikeCount;
                }
                if (self.clickButton) {
                    self.clickButton(2);
                }
                if (self.dataNewsModel.liked) {
                    self.likeBt.iconString = @"likeIconSelected";
                } else {
                    self.likeBt.iconString = @"likeIcon";
                }
                self.likeBt.titleString = [NSString getConvertTenThousandNumber:self.dataNewsModel.infoLikeCount];
            }
        }];
    } else {
        NSString *campsiteId = kCheckStringNil(self.dataModel.newsId);
        BOOL liked = self.dataModel.liked;
        NSString *action = @"1";
        if (liked) {
            action = @"0";
        }
        [startRequest netWorkPutCampLike:campsiteId type:@"2" action:action];
        [startRequest setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
            [QJAppTool hideHUDLoading];
            if (ResponseSuccess) {
                
                NSInteger newsLikeCount = [self.dataModel.newsLikeCount integerValue];
                if ([action isEqualToString:@"1"]) {
//                    [[QJAppTool getCurrentViewController].view makeToast:@"点赞成功"];
                    self.dataModel.liked = YES;
                    newsLikeCount = newsLikeCount + 1;
                    self.dataModel.newsLikeCount = [NSString stringWithFormat:@"%ld", newsLikeCount];
                } else {
//                    [[QJAppTool getCurrentViewController].view makeToast:@"取消点赞"];
                    self.dataModel.liked = NO;
                    newsLikeCount = newsLikeCount - 1;
                    self.dataModel.newsLikeCount = [NSString stringWithFormat:@"%ld", newsLikeCount];
                }
                if (self.clickButton) {
                    self.clickButton(2);
                }
                if (self.dataModel.liked) {
                    self.likeBt.iconString = @"likeIconSelected";
                } else {
                    self.likeBt.iconString = @"likeIcon";
                }
                self.likeBt.titleString = [NSString getConvertTenThousandNumber:[self.dataModel.newsLikeCount integerValue]];

            }  else {
                NSString *message = request.responseJSONObject[@"message"];
                [[QJAppTool getCurrentViewController].view makeToast:kCheckStringNil(message)];
            }
        }];
    }
    [startRequest setFailureCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        [QJAppTool hideHUDLoading];
    }];
}

- (void)saveClick {
//    [QJAppTool showHUDLoading];
    QJCampsiteRequest *startRequest = [[QJCampsiteRequest alloc] init];
    if (self.isNews) {
        NSString *campsiteId = kCheckStringNil(self.dataNewsModel.infoId);
        BOOL collected = self.dataNewsModel.collected;
        NSString *action = @"1";
        if (collected) {
            action = @"0";
        }
        [startRequest netWorkPutCampSave:campsiteId type:@"1" action:action];
        [startRequest setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
            [QJAppTool hideHUDLoading];
            if (ResponseSuccess) {
                
                NSInteger newsCollectCount = self.dataNewsModel.infoCollectCount;
                if ([action isEqualToString:@"1"]) {
                    [QJAppTool showToast:@"收藏成功"];
                    self.dataNewsModel.collected = YES;
                    newsCollectCount = newsCollectCount + 1;
                    self.dataNewsModel.infoCollectCount = newsCollectCount;
                } else {
                    [QJAppTool showToast:@"取消收藏"];

                    self.dataNewsModel.collected = NO;
                    newsCollectCount = newsCollectCount - 1;
                    self.dataNewsModel.infoCollectCount = newsCollectCount;
                }
                if (self.clickButton) {
                    self.clickButton(2);
                }
                if (self.dataNewsModel.collected) {
                    self.saveBt.iconString = @"collectSaveIconSelected";
                } else {
                    self.saveBt.iconString = @"collectSaveIcon";
                }
                self.saveBt.titleString = [NSString getConvertTenThousandNumber:self.dataNewsModel.infoCollectCount];
            }
        }];
    } else {
        NSString *campsiteId = kCheckStringNil(self.dataModel.newsId);
        BOOL collected = self.dataModel.collected;
        NSString *action = @"1";
        if (collected) {
            action = @"0";
        }
        [startRequest netWorkPutCampSave:campsiteId type:@"2" action:action];
        [startRequest setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
            [QJAppTool hideHUDLoading];
            if (ResponseSuccess) {
                
                NSInteger newsCollectCount = [self.dataModel.newsCollectCount integerValue];
                if ([action isEqualToString:@"1"]) {
                    [QJAppTool showToast:@"收藏成功"];
                    self.dataModel.collected = YES;
                    newsCollectCount = newsCollectCount + 1;
                    self.dataModel.newsCollectCount = [NSString stringWithFormat:@"%ld", newsCollectCount];
                } else {
                    [QJAppTool showToast:@"取消收藏"];

                    self.dataModel.collected = NO;
                    newsCollectCount = newsCollectCount - 1;
                    self.dataModel.newsCollectCount = [NSString stringWithFormat:@"%ld", newsCollectCount];
                }
                
                if (self.dataModel.collected) {
                    self.saveBt.iconString = @"collectSaveIconSelected";
                } else {
                    self.saveBt.iconString = @"collectSaveIcon";
                }
                self.saveBt.titleString = [NSString getConvertTenThousandNumber:[self.dataModel.newsCollectCount integerValue]];
                if (self.clickButton) {
                    self.clickButton(2);
                }
                
            } else {
                NSString *message = request.responseJSONObject[@"message"];
                [QJAppTool showToast:kCheckStringNil(message)];
            }
        }];
    }
    
    
    [startRequest setFailureCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        [QJAppTool hideHUDLoading];
    }];
}


- (void)textClick:(UIButton *)sender {
    if (self.clickButton) {
        self.clickButton(1);
    }
}
@end
