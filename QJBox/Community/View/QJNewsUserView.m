//
//  QJNewsUserView.m
//  QJBox
//
//  Created by Sun on 2022/7/28.
//

#import "QJNewsUserView.h"

@interface QJNewsUserView ()
/**头像*/
@property (nonatomic, strong) UIImageView *avatar;
/**博主等级头像*/
@property (nonatomic, strong) UIImageView *bloggerImage;
/**昵称*/
@property (nonatomic, strong) UILabel *nickName;
@end
@implementation QJNewsUserView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:self.avatar];
        [self addSubview:self.bloggerImage];
        [self addSubview:self.nickName];
        [self makeConstraints];
    }
    return self;
}

- (void)makeConstraints {
    [self.avatar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top);
        make.left.equalTo(self.mas_left);
        make.width.height.mas_equalTo(15);
    }];
    [self.bloggerImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.avatar.mas_bottom).mas_offset(0);
        make.right.equalTo(self.avatar.mas_right).offset(0);
        make.width.mas_equalTo(6);
        make.height.mas_equalTo(7);
    }];
    [self.nickName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.avatar.mas_right).offset(4);
        make.centerY.equalTo(self.avatar);
    }];

}

- (UIImageView *)avatar {
    if (!_avatar) {
        _avatar = [UIImageView new];
        _avatar.layer.masksToBounds = YES;
        _avatar.layer.cornerRadius = 7.5;
    }
    return _avatar;
}

- (UILabel *)nickName {
    if (!_nickName) {
        _nickName = [UILabel new];
        _nickName.font = MYFONTALL(FONT_REGULAR, 11);
        _nickName.textColor = UIColorHex(919599);
    }
    return _nickName;
}

- (UIImageView *)bloggerImage {
    if (!_bloggerImage) {
        self.bloggerImage = [UIImageView new];
        self.bloggerImage.image = [UIImage imageNamed:@"avatarLevelIcon"];
    }
    return _bloggerImage;
}

- (void)configureHeaderWithData:(QJNewsListModel *)model {
    NSString *imageString = [kCheckStringNil(model.userCoverImage) checkImageUrlString];
    NSURL *imageURL = [NSURL URLWithString:imageString];
    [self.avatar setImageURL:imageURL];
    self.nickName.text = kCheckStringNil(model.userNickName);
    self.bloggerImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"qj_bozhuLevel_%ld",model.userBloggerLevel]];
}

@end
