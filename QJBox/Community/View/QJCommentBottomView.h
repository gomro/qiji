//
//  QJCommentBottomView.h
//  QJBox
//
//  Created by Sun on 2022/7/19.
//

#import <UIKit/UIKit.h>
#import "QJImageButton.h"
#import "QJCommentTopModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface QJCommentBottomView : UIView
/**展开按钮*/
@property (nonatomic, strong) QJImageButton *expandButton;

/**收起按钮*/
@property (nonatomic, strong) QJImageButton *reductionButton;

- (void)configureCommentBottomWithData:(QJCommentTopModel *)model;

@end

NS_ASSUME_NONNULL_END
