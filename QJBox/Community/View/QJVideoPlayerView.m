//
//  QJVideoPlayerView.m
//  QJBox
//
//  Created by Sun on 2022/8/2.
//

#import "QJVideoPlayerView.h"
#import "QJImageButton.h"
#import "QJCampsiteRequest.h"
@interface QJVideoPlayerView ()
/**点赞*/
@property (nonatomic, strong) QJImageButton *likeBt;
@property (nonatomic, strong) QJCampsiteDetailModel *dataModel;

@property (nonatomic, strong) QJNewsListModel *dataNewsModel;

/**是否是资讯*/
@property (nonatomic, assign) BOOL isNews;

/**是否展示静音*/
@property (nonatomic, assign) BOOL showVolume;

/**静音按钮*/
@property (nonatomic, strong) UIButton *volumeBt;

@end

static SJEdgeControlButtonItemTag const SJLikeImageItemTag = 21;
static SJEdgeControlButtonItemTag const SJMoreImageItemTag = 22;

@implementation QJVideoPlayerView

/**动态数据*/
- (void)changePlayerView:(QJCampsiteDetailModel *)dataModel {
    self.dataModel = dataModel;
    [self subPlayerUI];
    self.likeBt.titleString = kCheckStringNil(dataModel.newsLikeCount);
    if (dataModel.liked) {
        self.likeBt.iconString = @"likeWhiteSelected";
    } else {
        self.likeBt.iconString = @"likeIconWhite";
    }
}

/**资讯数据*/
- (void)changePlayerNewsView:(QJNewsListModel *)dataModel showVolume:(BOOL)showVolume {
    self.dataNewsModel = dataModel;
    self.isNews = YES;
    [self subPlayerUI];
    self.likeBt.titleString = [NSString stringWithFormat:@"%ld", dataModel.infoLikeCount];
    if (dataModel.liked) {
        self.likeBt.iconString = @"likeWhiteSelected";
    } else {
        self.likeBt.iconString = @"likeIconWhite";
    }
    if (showVolume) {
        self.playerVolume = 0;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            self.showVolume = YES;
            [self changeVolumeBt];
        });     
    }
}

#pragma mark - player视图
- (void)subPlayerUI {
    self.allowHorizontalTriggeringOfPanGesturesInCells = YES;
    //    self.automaticallyPerformRotationOrFitOnScreen = NO;
    self.defaultEdgeControlLayer.hiddenTitleItemWhenOrientationIsPortrait = YES;
    [self.defaultEdgeControlLayer.bottomAdapter removeItemForTag:SJEdgeControlLayerBottomItem_Separator];
    [self.defaultEdgeControlLayer.bottomAdapter exchangeItemForTag:SJEdgeControlLayerBottomItem_DurationTime withItemForTag:SJEdgeControlLayerBottomItem_Progress];
    self.defaultEdgeControlLayer.titleView.scrollEnabled = NO;
    self.defaultEdgeControlLayer.showsMoreItem = NO;
    if (@available(iOS 14.0, *)) {
        self.defaultEdgeControlLayer.automaticallyShowsPictureInPictureItem = NO;
    }
    
    SJVideoPlayer.update(^(SJVideoPlayerConfigurations * _Nonnull commonSettings) {
        // 注意, 该block将在子线程执行
        commonSettings.resources.progressTrackColor = UIColorHex(FFFFFF4D);
        commonSettings.resources.progressTraceColor = [UIColor whiteColor];
        commonSettings.resources.progressTrackHeight = 1;
        commonSettings.resources.progressThumbSize = 11;
        commonSettings.resources.progressThumbImage = [UIImage imageNamed:@"EllipseProgress"];
        commonSettings.resources.pauseImage = [UIImage imageNamed:@"iconVideoStop"];
        commonSettings.resources.playImage = [UIImage imageNamed:@"iconVideoStart"];        
    });
    
    /**底部视图*/
    UIView *bottomView = [UIView.alloc initWithFrame:CGRectZero];
    bottomView.hidden = YES;
    [self.defaultEdgeControlLayer.bottomContainerView addSubview:bottomView];
    [bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.left.right.offset(0);
        make.height.offset(44);
    }];
    /**左边锁屏按钮*/
    UIView *lockView = [self.defaultEdgeControlLayer.leftAdapter viewForItemForTag:SJEdgeControlLayerLeftItem_Lock];
    lockView.hidden = YES;
    
    /**全屏按钮*/
    UIView *fullView = [self.defaultEdgeControlLayer.bottomAdapter viewForItemForTag:SJEdgeControlLayerBottomItem_Full];
    UIButton *speedButton = [UIButton new];
    [bottomView addSubview:speedButton];
    [speedButton setTitle:@"倍速" forState:UIControlStateNormal];
    [speedButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    speedButton.titleLabel.font = [UIFont fontWithName:FONT_REGULAR size:14]; //这里字体不缩放
    [speedButton addTarget:self action:@selector(switchControlLayer) forControlEvents:UIControlEventTouchUpInside];
    [speedButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(bottomView);
        make.centerX.equalTo(fullView.mas_centerX);
    }];
    
    /**点赞按钮*/
    UIView *likeView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    SJEdgeControlButtonItem *item = [[SJEdgeControlButtonItem alloc] initWithCustomView:likeView tag:SJLikeImageItemTag];
    [self.defaultEdgeControlLayer.rightAdapter addItem:item];
    [likeView addSubview:self.likeBt];
    [self.likeBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(likeView);
        make.width.mas_lessThanOrEqualTo(30);
    }];

    /**更多按钮*/
    UIView *moreView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    moreView.hidden = YES;
    SJEdgeControlButtonItem *item1 = [[SJEdgeControlButtonItem alloc] initWithCustomView:moreView tag:SJMoreImageItemTag];
    [self.defaultEdgeControlLayer.topAdapter addItem:item1];
    UIButton *moreButton = [[UIButton alloc] init];
    [moreButton setBackgroundImage:[UIImage imageNamed:@"qj_mySpace_more"] forState:UIControlStateNormal];
    [moreView addSubview:moreButton];
    [moreButton addTarget:self action:@selector(moreClick) forControlEvents:UIControlEventTouchUpInside];
    [moreButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(moreView);
        make.width.mas_lessThanOrEqualTo(24);
    }];
    
    
    /**静音按钮*/
    [self.defaultEdgeControlLayer addSubview:self.volumeBt];
    self.volumeBt.hidden = YES;
    [self.volumeBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.defaultEdgeControlLayer.mas_bottom).offset(-8);
        make.right.equalTo(self.defaultEdgeControlLayer.mas_right).offset(-8);
        make.width.height.mas_equalTo(24);
    }];
    
    MJWeakSelf
    self.rotationObserver.onTransitioningChanged = ^(id<SJRotationManager>  _Nonnull mgr, BOOL isTransitioning) {
        __strong typeof(weakSelf) strongSelf = weakSelf;
        if (!strongSelf) return ;
        //判断是否是小屏状态
        BOOL isSmallscreen = !strongSelf.isFullscreen && !strongSelf.isFitOnScreen;
        bottomView.hidden = isSmallscreen;
        moreView.hidden = isSmallscreen;
        strongSelf.likeBt.hidden = isSmallscreen;
        [strongSelf changeVolumeBt];
    };
    
    
    /**音量变化*/
    self.deviceVolumeAndBrightnessObserver.volumeDidChangeExeBlock = ^(id<SJDeviceVolumeAndBrightnessController>  _Nonnull mgr, float volume) {
        __strong typeof(weakSelf) strongSelf = weakSelf;
        if (!strongSelf) return ;
        if (volume > 0) {
            strongSelf.showVolume = NO;
        } else {
            strongSelf.showVolume = YES;
        }
        strongSelf.playerVolume = volume;

        [strongSelf changeVolumeBt];
    };
    
    
    // 修改bottomAdapter的约束
    self.controlLayerAppearObserver.onAppearChanged = ^(id<SJControlLayerAppearManager>  _Nonnull mgr) {
        __strong typeof(weakSelf) strongSelf = weakSelf;
        if ( !strongSelf ) return;
        [strongSelf changeVolumeBt];
        BOOL isSmallscreen = !strongSelf.isFullscreen && !strongSelf.isFitOnScreen;
        bottomView.hidden = isSmallscreen;
        strongSelf.likeBt.hidden = isSmallscreen;
        moreView.hidden = isSmallscreen;
        // 仅在控制层显示后更新约束
        if (mgr.isAppeared && isSmallscreen == NO) {
            [strongSelf.defaultEdgeControlLayer.bottomAdapter mas_updateConstraints:^(MASConstraintMaker *make) {
                make.bottom.offset(-44);
            }];
        } else {
            [strongSelf.defaultEdgeControlLayer.bottomAdapter mas_updateConstraints:^(MASConstraintMaker *make) {
                make.bottom.offset(0);
            }];
        }
    };
    
    
}


- (void)changeVolumeBt {
    BOOL isSmallscreen = !self.isFullscreen && !self.isFitOnScreen;
    if (isSmallscreen && !self.controlLayerAppeared && self.showVolume) {
        self.volumeBt.hidden = NO;
    } else {
        self.volumeBt.hidden = YES;
    }
    
}

- (QJImageButton *)likeBt {
    if (!_likeBt) {
        _likeBt = [QJImageButton new];
        _likeBt.titleString = @"0";
        _likeBt.titleLb.font = MYFONTALL(FONT_BOLD, 10);
        _likeBt.titleLb.textColor = UIColorHex(FFFFFF);
        _likeBt.iconString = @"likeIconWhite";
        _likeBt.imageSize = CGSizeMake(24, 24);
        _likeBt.space = 2;
        _likeBt.hidden = YES;
        _likeBt.style = ImageButtonStyleTop;
        [_likeBt addTarget:self action:@selector(likeClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _likeBt;
}

- (UIButton *)volumeBt {
    if (!_volumeBt) {
        _volumeBt = [UIButton new];
        [_volumeBt setBackgroundImage:[UIImage imageNamed:@"volumeMuteIcon"] forState:UIControlStateNormal];
        [_volumeBt addTarget:self action:@selector(volumePlayer) forControlEvents:UIControlEventTouchUpInside];
    }
    return _volumeBt;
}

- (void)volumePlayer {
    self.playerVolume = 0.2;
    self.showVolume = NO;
    [self changeVolumeBt];
}

- (void)switchControlLayer {
    if (self.clickButton) {
        if (self.isFullscreen) {
            self.clickButton(QJVideoPlayerViewTypeRate);
        } else {
            self.clickButton(QJVideoPlayerViewTypeRateVertical);
        }
    }
}

- (void)moreClick {
    if (self.clickButton) {
        if (self.isFullscreen) {
            self.clickButton(QJVideoPlayerViewTypeMore);
        } else {
            self.clickButton(QJVideoPlayerViewTypeMoreVertical);
        }
    }
}


- (void)likeClick {
    [QJAppTool showHUDLoading];
    QJCampsiteRequest *startRequest = [[QJCampsiteRequest alloc] init];
    if (self.isNews) {
        NSString *campsiteId = kCheckStringNil(self.dataNewsModel.infoId);
        BOOL liked = self.dataNewsModel.liked;
        NSString *action = @"1";
        if (liked) {
            action = @"0";
        }
        [startRequest netWorkPutCampLike:campsiteId type:@"1" action:action];
        [startRequest setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
            [QJAppTool hideHUDLoading];
            if (ResponseSuccess) {
                
                NSInteger newsLikeCount = self.dataNewsModel.infoLikeCount;
                if ([action isEqualToString:@"1"]) {
//                    [[QJAppTool getCurrentViewController].view makeToast:@"点赞成功"];
                    self.dataNewsModel.liked = YES;
                    newsLikeCount = newsLikeCount + 1;
                    self.dataNewsModel.infoLikeCount = newsLikeCount;
                } else {
//                    [[QJAppTool getCurrentViewController].view makeToast:@"取消点赞"];
                    self.dataNewsModel.liked = NO;
                    newsLikeCount = newsLikeCount - 1;
                    self.dataNewsModel.infoLikeCount = newsLikeCount;
                }
                [[NSNotificationCenter defaultCenter] postNotificationName:QJCampsiteClickLikeType object:nil userInfo:@{@"liked":@(self.dataNewsModel.liked), @"isNews":@"1", @"campsiteId":campsiteId}];

                if (self.dataNewsModel.liked) {
                    self.likeBt.iconString = @"likeWhiteSelected";
                } else {
                    self.likeBt.iconString = @"likeIconWhite";
                }
                self.likeBt.titleString =  [NSString stringWithFormat:@"%ld", self.dataNewsModel.infoLikeCount];

            }
        }];
    } else {
        NSString *campsiteId = kCheckStringNil(self.dataModel.newsId);
        BOOL liked = self.dataModel.liked;
        NSString *action = @"1";
        if (liked) {
            action = @"0";
        }
        [startRequest netWorkPutCampLike:campsiteId type:@"2" action:action];
        [startRequest setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
            [QJAppTool hideHUDLoading];
            if (ResponseSuccess) {
                
                NSInteger newsLikeCount = [self.dataModel.newsLikeCount integerValue];
                if ([action isEqualToString:@"1"]) {
//                    [[QJAppTool getCurrentViewController].view makeToast:@"点赞成功"];
                    self.dataModel.liked = YES;
                    newsLikeCount = newsLikeCount + 1;
                    self.dataModel.newsLikeCount = [NSString stringWithFormat:@"%ld", newsLikeCount];
                } else {
//                    [[QJAppTool getCurrentViewController].view makeToast:@"取消点赞"];
                    self.dataModel.liked = NO;
                    newsLikeCount = newsLikeCount - 1;
                    self.dataModel.newsLikeCount = [NSString stringWithFormat:@"%ld", newsLikeCount];
                }
                
                if (self.dataModel.liked) {
                    self.likeBt.iconString = @"likeWhiteSelected";
                } else {
                    self.likeBt.iconString = @"likeIconWhite";
                }
                self.likeBt.titleString =  kCheckStringNil(self.dataModel.newsLikeCount);
                [[NSNotificationCenter defaultCenter] postNotificationName:QJCampsiteClickLikeType object:nil userInfo:@{@"liked":@(self.dataModel.liked), @"isNews":@"0", @"campsiteId":campsiteId}];
            }
        }];
    }
    
    [startRequest setFailureCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        [QJAppTool hideHUDLoading];
    }];
}

- (void)videoFullItemWasTapped {

    if ( self.isFitOnScreen) {
        [self setFitOnScreen:NO];
        return;
    }
    [self rotate];
}
@end
