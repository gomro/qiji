//
//  QJDetailAlertView.h
//  QJBox
//
//  Created by Sun on 2022/7/21.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef NS_ENUM(NSUInteger, QJDetailAlertViewTitleType) {
    //0：默认有标题、1：只有详情
    QJDetailAlertViewTitleTypeDefault,
    QJDetailAlertViewTitleTypeOnlyDeatil
};

typedef NS_ENUM(NSUInteger, QJDetailAlertViewButtonType) {
    //0：两个按钮、1：一个按钮
    QJDetailAlertViewButtonTypeDefault,
    QJDetailAlertViewButtonTypeOnly
};

typedef NS_ENUM(NSUInteger, QJDetailAlertViewButtonClickType) {
    //0：左边按钮事件，一个默认左边、1：右边按钮事件
    QJDetailAlertViewButtonClickTypeLeft,
    QJDetailAlertViewButtonClickTypeRight,
};

typedef void (^QJDetailAlertViewButtonClick)(QJDetailAlertViewButtonClickType type);
@interface QJDetailAlertView : UIView
@property (nonatomic, strong) UIButton *leftButton;
@property (nonatomic, strong) UIButton *rightButton;

@property (nonatomic, strong) UIButton *onlyButton;//单个按钮

@property (nonatomic, copy) NSString *describeAlignment;//详情字体居左

- (instancetype)initWithFrame:(CGRect)frame titleType:(QJDetailAlertViewTitleType)titleType buttonType:(QJDetailAlertViewButtonType)buttonType;

//点击事件
- (void)returnClick:(QJDetailAlertViewButtonClick)clickHandler;

//标题，详情
- (void)showTitleText:(NSString *)titleText describeText:(NSString *)describeText;

- (void)showTitleText:(NSString *)titleText titleFont:(UIFont *)titleFont titleColor:(UIColor *)titleColor describeText:(NSString *)describeText describeFont:(UIFont *)describeFont describeColor:(UIColor *)describeColor;

@end

NS_ASSUME_NONNULL_END
