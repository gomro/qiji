//
//  QJNewsHotTagView.h
//  QJBox
//
//  Created by macm on 2022/7/28.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJNewsHotTagView : UIView

@property (nonatomic, copy) void (^tagViewClickEvent)(NSArray *tagArray);

- (instancetype)initWithTitles:(NSArray *)titles selectTag:(NSArray *)selectTag;
- (void)show;

@end

NS_ASSUME_NONNULL_END
