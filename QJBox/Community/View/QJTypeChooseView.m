//
//  QJTypeChooseView.m
//  QJBox
//
//  Created by Sun on 2022/7/26.
//

#import "QJTypeChooseView.h"
#import "QJImageButton.h"

@interface QJTypeChooseView ()
@property (nonatomic, strong) QJImageButton *titleBt;



@end

@implementation QJTypeChooseView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self addSubview:self.titleBt];
        [self addSubview:self.timeBt];
        [self addSubview:self.popularBt];
        [self makeConstraints];
    }
    return self;
}

- (void)makeConstraints {
    [self.titleBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).offset(5);
        make.left.equalTo(self.mas_left).offset(8);
        make.right.equalTo(self.mas_right).offset(-4);
        make.height.mas_equalTo(24*kWScale);
    }];
    
    [self.timeBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.top.equalTo(self.titleBt.mas_bottom).offset(5);
        make.height.mas_equalTo(30*kWScale);
    }];
    
    [self.popularBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.top.equalTo(self.timeBt.mas_bottom);
        make.height.mas_equalTo(30*kWScale);
    }];
    
}


- (QJImageButton *)titleBt {
    if (!_titleBt) {
        _titleBt = [QJImageButton new];
        _titleBt.titleString = @"发布时间";
        _titleBt.titleLb.font = MYFONTALL(FONT_REGULAR, 12);
        _titleBt.titleLb.textColor = UIColorHex(474849);
        _titleBt.iconString = @"iconDownOne";
        _titleBt.space = 4;
        _titleBt.size = CGSizeMake(16*kWScale, 16*kWScale);
        _titleBt.style = ImageButtonStyleRight;
    }
    return _titleBt;
}

- (UIButton *)timeBt {
    if (!_timeBt) {
        _timeBt = [UIButton new];
        [_timeBt setTitle:@"发布时间     " forState:UIControlStateNormal];
        [_timeBt setTitleColor:UIColorHex(474849) forState:UIControlStateNormal];
        _timeBt.titleLabel.font = MYFONTALL(FONT_REGULAR, 11);
        [_timeBt addTarget:self action:@selector(timeClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _timeBt;
}

- (UIButton *)popularBt {
    if (!_popularBt) {
        _popularBt = [UIButton new];
        [_popularBt setTitle:@"热门排行     " forState:UIControlStateNormal];
        [_popularBt setTitleColor:UIColorHex(474849) forState:UIControlStateNormal];
        _popularBt.titleLabel.font = MYFONTALL(FONT_REGULAR, 11);
        [_popularBt addTarget:self action:@selector(popularClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _popularBt;
}

- (void)setStype:(QJTypeChooseViewStype)stype {
    _stype = stype;
    if (stype == QJTypeChooseViewStypeTime) {
        _titleBt.titleString = @"发布时间";
    } else {
        _titleBt.titleString = @"热门排行";
    }
}

- (void)timeClick {
    if (self.clickButton) {
        self.clickButton(QJTypeChooseViewStypeTime);
    }
}

- (void)popularClick {
    if (self.clickButton) {
        self.clickButton(QJTypeChooseViewStypePopular);
    }
}
@end
