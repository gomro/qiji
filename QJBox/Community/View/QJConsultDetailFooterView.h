//
//  QJConsultDetailFooterView.h
//  QJBox
//
//  Created by Sun on 2022/8/8.
//

#import <UIKit/UIKit.h>
#import "QJNewsListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJConsultDetailFooterView : UIView
- (void)configureDetailFooterWithNewsData:(QJNewsListModel *)dataModel;

@end

NS_ASSUME_NONNULL_END
