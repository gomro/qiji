//
//  QJDetailShareView.m
//  QJBox
//
//  Created by Sun on 2022/7/20.
//

#import "QJDetailShareView.h"
#import "QJImageButton.h"
#import "QJCampsiteRequest.h"

@interface QJDetailShareView ()

@property (nonatomic, strong) UIButton *closeBt;
@property (nonatomic, strong) QJImageButton *saveBt;
@property (nonatomic, strong) QJImageButton *deleteBt;
@property (nonatomic, strong) QJImageButton *reportBt;

@property (nonatomic, assign) BOOL isSave;
@property (nonatomic, assign) BOOL isNews;
@property (nonatomic, copy) NSString *newsID;

@end

@implementation QJDetailShareView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.frame = [UIScreen mainScreen].bounds;
        self.backgroundColor = UIColorHex(F2F2F2CC);
        
        UIBlurEffect *blur = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
        UIVisualEffectView *effectview = [[UIVisualEffectView alloc] initWithEffect:blur];
        effectview.frame = [UIScreen mainScreen].bounds;
        effectview.alpha = 0.9;
        [self addSubview:effectview];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideView)];
        [self addGestureRecognizer:tap];

        [self makeConstraints];
        [self makeSubView];
    }
    return self;
}

- (void)makeSubView {
    CGFloat width = (kScreenWidth - 280*kWScale) / 3.0 + 48*kWScale;
    NSArray *imageArr = @[@"shareVectorIcon", @"login_qq", @"login_wx", @"friendscircle"];
    NSArray *titleArr = @[@"QQ空间", @"QQ", @"微信", @"朋友圈"];
    for (int i = 0; i < 4; i++) {
        QJImageButton *imageBt = [[QJImageButton alloc] init];
        imageBt.tag = 100 + i;
        imageBt.titleString = titleArr[i];
        imageBt.titleLb.font = MYFONTALL(FONT_REGULAR, 12);
        imageBt.titleLb.textColor = UIColorHex(000000);
        imageBt.iconString = imageArr[i];
        imageBt.imageSize = CGSizeMake(48*kWScale, 48*kWScale);
        [imageBt addTarget:self action:@selector(clickItem:) forControlEvents:UIControlEventTouchUpInside];
        imageBt.space = 10;
        imageBt.style = ImageButtonStyleTop;
        [self addSubview:imageBt];
        [imageBt mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.mas_centerY).offset(-24);
            make.left.equalTo(self.mas_left).offset(44 + width * i);
            make.width.mas_equalTo(48*kWScale);
        }];
    }
    
}

- (void)makeConstraints {
    [self addSubview:self.closeBt];
    [self addSubview:self.saveBt];
    [self addSubview:self.deleteBt];
    [self addSubview:self.reportBt];

    [self.closeBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.mas_bottom).offset(- Bottom_iPhoneX_SPACE - 93);
        make.centerX.equalTo(self);
        make.width.height.mas_equalTo(40*kWScale);
    }];
    
    CGFloat width = (kScreenWidth - 280*kWScale) / 3.0;
    [self.saveBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_centerY).offset(24);
        make.left.equalTo(self.mas_left).offset(44);
        make.width.mas_equalTo(48*kWScale);
    }];
    
    [self.deleteBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.saveBt);
        make.left.equalTo(self.saveBt.mas_right).offset(width);
        make.width.mas_equalTo(48*kWScale);
    }];
    
    [self.reportBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.saveBt);
        make.left.equalTo(self.saveBt.mas_right).offset(width);
        make.width.mas_equalTo(48*kWScale);
    }];
}

- (UIButton *)closeBt {
    if (!_closeBt) {
        _closeBt = [UIButton new];
        [_closeBt setBackgroundImage:[UIImage imageNamed:@"shareClose"] forState:UIControlStateNormal];
        [_closeBt addTarget:self action:@selector(hideView) forControlEvents:UIControlEventTouchUpInside];
    }
    return _closeBt;
}

- (QJImageButton *)saveBt {
    if (!_saveBt) {
        _saveBt = [[QJImageButton alloc] init];
        _saveBt.titleString = @"收藏";
        _saveBt.titleLb.font = MYFONTALL(FONT_REGULAR, 12);
        _saveBt.titleLb.textColor = UIColorHex(000000);
        _saveBt.iconString = @"shareCollectIcon";
        _saveBt.imageSize = CGSizeMake(48, 48);
        _saveBt.space = 10;
        _saveBt.style = ImageButtonStyleTop;
        [_saveBt addTarget:self action:@selector(saveClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _saveBt;
}

- (QJImageButton *)deleteBt {
    if (!_deleteBt) {
        _deleteBt = [[QJImageButton alloc] init];
        _deleteBt.titleString = @"删除";
        _deleteBt.titleLb.font = MYFONTALL(FONT_REGULAR, 12);
        _deleteBt.titleLb.textColor = UIColorHex(000000);
        _deleteBt.iconString = @"shareDeleteIcon";
        _deleteBt.imageSize = CGSizeMake(48*kWScale, 48*kWScale);
        _deleteBt.space = 10;
        _deleteBt.tag = 107;
        _deleteBt.hidden = YES;
        _deleteBt.style = ImageButtonStyleTop;
        [_deleteBt addTarget:self action:@selector(clickItem:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _deleteBt;
}

- (QJImageButton *)reportBt {
    if (!_reportBt) {
        _reportBt = [[QJImageButton alloc] init];
        _reportBt.titleString = @"举报";
        _reportBt.titleLb.font = MYFONTALL(FONT_REGULAR, 12);
        _reportBt.titleLb.textColor = UIColorHex(000000);
        _reportBt.iconString = @"shareCautionIcon";
        _reportBt.imageSize = CGSizeMake(48*kWScale, 48*kWScale);
        _reportBt.space = 10;
        _reportBt.tag = 106;
        _reportBt.style = ImageButtonStyleTop;
        _reportBt.hidden = YES;
        [_reportBt addTarget:self action:@selector(clickItem:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _reportBt;
}



#pragma mark - 出现隐藏
- (void)showView {
    UIWindow *keywindow = [[UIApplication sharedApplication] keyWindow];
    [keywindow addSubview:self];
    [self showInView];
}

- (void)showInView {
    self.alpha = 0;
    [UIView animateWithDuration:0.5 animations:^{
        self.transform = CGAffineTransformMakeTranslation(0, 0);
        self.alpha = 1;
    }];
}

- (void)hideView {
    [UIView animateWithDuration:0.35 animations:^{
//        self.transform = CGAffineTransformMakeTranslation(0, kScreenHeight);
        self.alpha = 0.0;
    } completion:^(BOOL finished) {
        if (finished) {
            [self removeFromSuperview];
        }
    }];
}

- (void)clickItem:(UIButton *)sender {
    [self hideView];
    if (self.clickButton) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            self.clickButton(sender.tag);

        });
    }
    
}

- (void)configChangeSaveIsMySelf:(BOOL)isMySelf isSave:(BOOL)isSave isNews:(BOOL)isNews newsID:(NSString *)newsID {
    if (isMySelf) {
        _deleteBt.hidden = NO;
        _reportBt.hidden = YES;
    } else {
        _deleteBt.hidden = YES;
        _reportBt.hidden = NO;
    }
    self.isSave = isSave;
    self.newsID = newsID;
    self.isNews = isNews;
    if (self.isSave) {
        _saveBt.iconString = @"shareCollectIconSelected";
        _saveBt.titleString = @"已收藏";
    } else {
        _saveBt.iconString = @"shareCollectIcon";
        _saveBt.titleString = @"收藏";
    }
    
}

- (void)saveClick {
//    [MBProgressHUD showHUDAddedTo:self animated:YES];
    QJCampsiteRequest *startRequest = [[QJCampsiteRequest alloc] init];
    NSString *type = @"2";
    if (self.isNews) {
        type = @"1";
    }
    NSString *action = @"1";
    if (self.isSave) {
        action = @"0";
    }
    [startRequest netWorkPutCampSave:kCheckStringNil(self.newsID) type:type action:action];
    [startRequest setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        [MBProgressHUD hideHUDForView:self animated:YES];
        if (ResponseSuccess) {
            
            if ([action isEqualToString:@"1"]) {
                self.isSave = YES;
                [self hideView];
                [[QJAppTool getCurrentViewController].navigationController.view makeToast:@"收藏成功"];
                if (self.clickButton) {
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        self.clickButton(QJDetailShareViewStyleSave);

                    });
                }
            } else {
                self.isSave = NO;
                [self hideView];
                [[QJAppTool getCurrentViewController].navigationController.view makeToast:@"取消收藏"];
                if (self.clickButton) {
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        self.clickButton(QJDetailShareViewStyleCancleSave);

                    });
            }
            }
            if (self.isSave) {
                self.saveBt.iconString = @"shareCollectIconSelected";
                self.saveBt.titleString = @"已收藏";
            } else {
                self.saveBt.iconString = @"shareCollectIcon";
                self.saveBt.titleString = @"收藏";
            }
        }
    }];
    
    
    [startRequest setFailureCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        [MBProgressHUD hideHUDForView:self animated:YES];
    }];
}

@end
