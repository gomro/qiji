//
//  QJNewsUserView.h
//  QJBox
//
//  Created by Sun on 2022/7/28.
//

#import <UIKit/UIKit.h>
#import "QJNewsListModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface QJNewsUserView : UIView
- (void)configureHeaderWithData:(QJNewsListModel *)model;

@end

NS_ASSUME_NONNULL_END
