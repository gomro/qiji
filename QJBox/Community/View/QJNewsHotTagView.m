//
//  QJNewsHotTagView.m
//  QJBox
//
//  Created by macm on 2022/7/28.
//

#import "QJNewsHotTagView.h"
#import "QJNewsTagView.h"

@interface QJNewsHotTagView ()
@property (nonatomic, strong) UIView *contentView;
@property (nonatomic, strong) QJNewsTagView *tagView;
@property (strong, nonatomic) MASConstraint *bottomConstraint;
@property (nonatomic, strong) UIButton *cancelButton;
@property (nonatomic, strong) UIButton *doneButton;
@end

@implementation QJNewsHotTagView

- (instancetype)initWithTitles:(NSArray *)titles selectTag:(NSArray *)selectTag {
    if (self = [super init]) {
        self.frame = [UIScreen mainScreen].bounds;
        self.backgroundColor = RGBAlpha(0, 0, 0, 0.4);
        
        self.tagView.selectTagArray = [NSMutableArray arrayWithArray:selectTag];
        [self createView:titles];
        [self setDoneButtonStatus];
        
        @weakify(self);
        self.tagView.tagViewClickEvent = ^(NSArray * _Nonnull tagArray,NSArray *modelArr) {
            @strongify(self);
            [self setDoneButtonStatus];
        };
    }
    return self;
}

// 设置确认按钮状态
- (void)setDoneButtonStatus {
    if (self.tagView.selectTagArray.count > 0) {
        [self.cancelButton setEnabled:YES];
        self.cancelButton.layer.borderColor = UIColorFromRGB(0x000000).CGColor;
        [self.cancelButton setTitleColor:UIColorFromRGB(0x000000) forState:UIControlStateNormal];
    } else {
        [self.cancelButton setEnabled:NO];
        self.cancelButton.layer.borderColor = UIColorFromRGB(0xc8cacc).CGColor;
        [self.cancelButton setTitleColor:UIColorFromRGB(0xc8cacc) forState:UIControlStateNormal];
    }
}

- (void)cancelAction {
    [self.tagView resetTagArray];
}

- (void)doneAction {
    if (self.tagViewClickEvent) {
        self.tagViewClickEvent(self.tagView.selectTagArray);
    }
    [self fadeOut];
}

- (void)createView:(NSArray *)array {
    [self addSubview:self.contentView];

    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.text = @"热门标签";
    titleLabel.textColor = UIColorFromRGB(0x16191c);
    titleLabel.font = FontSemibold(16);
    [self.contentView addSubview:titleLabel];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.centerX.mas_equalTo(self.contentView);
        make.height.mas_equalTo(60*kWScale);
    }];
    
    //给tagView赋值
    self.tagView.tagModelArray = array;
    [self.contentView addSubview:self.tagView];
    [self.tagView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(50);
        make.left.mas_equalTo(8);
        make.right.mas_equalTo(-8);
        make.bottom.mas_equalTo(-Bottom_iPhoneX_SPACE-60*kWScale);
    }];
    
    [self.contentView addSubview:self.cancelButton];
    [self.cancelButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.tagView.mas_bottom).mas_offset(10);
        make.left.mas_equalTo(16);
        make.height.mas_equalTo(34*kWScale);
        make.width.mas_equalTo((kScreenWidth-64)/2);
    }];
    
    [self.contentView addSubview:self.doneButton];
    [self.doneButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.tagView.mas_bottom).mas_offset(10);
        make.right.mas_equalTo(-16);
        make.height.mas_equalTo(34*kWScale);
        make.width.mas_equalTo((kScreenWidth-64)/2);
    }];
        
    [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        self.bottomConstraint = make.bottom.mas_equalTo(1000);

    }];
    
    // 让约束生效，tabview和contentview
    [self layoutIfNeeded];

    [self.contentView showCorner:16 rectCorner:UIRectCornerTopLeft | UIRectCornerTopRight];
    
}

- (void)show {
    UIWindow *keywindow = [[UIApplication sharedApplication] keyWindow];
    [keywindow addSubview:self];
    [self fadeIn];
}

- (void)fadeIn {
    [self.bottomConstraint uninstall];
    [self.contentView mas_updateConstraints:^(MASConstraintMaker *make) {
        self.bottomConstraint = make.bottom.mas_equalTo(0);
    }];
    
    self.alpha = 0;
    [UIView animateWithDuration:0.25 animations:^{
        self.alpha = 1;
        [self layoutIfNeeded];
    }];
}

- (void)fadeOut {
    [self.bottomConstraint uninstall];
    [self.contentView mas_updateConstraints:^(MASConstraintMaker *make) {
        self.bottomConstraint = make.bottom.mas_equalTo(1000);
    }];
    
    [UIView animateWithDuration:0.35 animations:^{
        self.alpha = 0.0;
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        if (finished) {
            [self removeFromSuperview];
        }
    }];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];

    UIView *view = [touch view];

    if (view == self) {
        [self fadeOut];
    }

}

- (UIView *)contentView {
    if (!_contentView) {
        _contentView = [[UIView alloc] init];
        _contentView.backgroundColor = [UIColor whiteColor];
    }
    return _contentView;
}

- (QJNewsTagView *)tagView {
    if (!_tagView) {
        _tagView = [[QJNewsTagView alloc] init];
        _tagView.backgroundColor = [UIColor whiteColor];
    }
    return _tagView;
}

- (UIButton *)cancelButton {
    if (!_cancelButton) {
        _cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_cancelButton setTitle:@"重置" forState:UIControlStateNormal];
        [_cancelButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_cancelButton setBackgroundColor:[UIColor whiteColor]];
        [_cancelButton.titleLabel setFont:FontRegular(14)];
        _cancelButton.layer.cornerRadius = 17;
        _cancelButton.layer.borderColor = UIColorFromRGB(0x000000).CGColor;
        _cancelButton.layer.borderWidth = 1;
        [_cancelButton addTarget:self action:@selector(cancelAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _cancelButton;
}

- (UIButton *)doneButton {
    if (!_doneButton) {
        _doneButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_doneButton setTitle:@"确认" forState:UIControlStateNormal];
        [_doneButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_doneButton setBackgroundColor:UIColorFromRGB(0xff9500)];
        [_doneButton.titleLabel setFont:FontRegular(14)];
        _doneButton.layer.cornerRadius = 17;
        [_doneButton addTarget:self action:@selector(doneAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _doneButton;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
