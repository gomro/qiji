//
//  QJNewsSearchResultListView.h
//  QJBox
//
//  Created by macm on 2022/7/28.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class QJMineFansModel;

@interface QJNewsSearchResultListView : UIView

/**
 * 刷新数据
 keyWordType    关键字类型，标题还是游戏,可用值:GAME,TITLE
 keyword    关键字
 categoryCode    资讯分类，不传查询综合
 sort    排序(仅2种)：发布时间倒序TD、热门倒序HD,可用值:HD,TD
 */
- (void)reloadListViewKeywordType:(NSString *)keywordType keyword:(NSString *)keyword categoryCode:(NSString *)categoryCode sort:(NSString *)sort;

@end

NS_ASSUME_NONNULL_END
