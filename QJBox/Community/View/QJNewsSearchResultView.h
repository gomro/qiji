//
//  QJNewsSearchResultView.h
//  QJBox
//
//  Created by macm on 2022/7/27.
//

#import <UIKit/UIKit.h>
#import "QJNewsSearchResultListView.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJNewsSearchResultView : UIView
@property (nonatomic, strong) QJNewsSearchResultListView *listView;
@property (nonatomic, copy) NSString *sortString; // 排序(仅2种)：发布时间倒序TD、热门倒序HD
@property (nonatomic, copy) NSString *categoryCode; // 资讯分类，不传查询综合

// 点击筛选分类的按钮回调，刷新搜索数据
@property (nonatomic, copy) void(^didSelectedSort)(void);

// 重置搜索条件
- (void)resetSearchSort;
@end

NS_ASSUME_NONNULL_END
