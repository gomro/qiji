//
//  QJCampsiteTapView.m
//  QJBox
//
//  Created by Sun on 2022/7/8.
//

#import "QJCampsiteTapView.h"
@interface QJCampsiteTapView ()
/**tap图片*/
@property (nonatomic, strong) UIImageView *imageView;
/**标题*/
@property (nonatomic, strong) UILabel *title;
@end

@implementation QJCampsiteTapView
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:self.imageView];
        [self addSubview:self.title];
        [self makeConstraints];
    }
    return self;
}

- (void)makeConstraints {
    [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.equalTo(self);
        make.width.height.mas_equalTo(30 * kWScale);
    }];
    [self.title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.imageView.mas_right).offset(12);
        make.centerY.equalTo(self.imageView);
    }];
    
}

- (UIImageView *)imageView {
    if (!_imageView) {
        _imageView = [UIImageView new];
        _imageView.layer.cornerRadius = 8 * kWScale;
        _imageView.layer.masksToBounds = YES;
    }
    return _imageView;
}

- (UILabel *)title {
    if (!_title) {
        _title = [UILabel new];
        _title.textColor = [UIColor whiteColor];
        _title.font = MYFONTALL(FONT_BOLD, 16);        
    }
    return _title;
}

- (void)configureTapWithData:(QJCampsiteListModel *)model {
    NSString *imageString = [kCheckStringNil(model.iconAddress) checkImageUrlString];
    [self.imageView setImageWithURL:[NSURL URLWithString:imageString] placeholder:nil];
    self.title.text = kCheckStringNil(model.name);
    
}

@end
