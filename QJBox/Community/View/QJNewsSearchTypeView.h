//
//  QJNewsSearchTypeView.h
//  QJBox
//
//  Created by macm on 2022/7/27.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void (^ClickItem)(NSString *titleString);

@interface QJNewsSearchTypeView : UIView
// 标题数组
@property (nonatomic, strong) NSArray *titleArray;

@property (nonatomic, copy) ClickItem clickButton;

@end

NS_ASSUME_NONNULL_END
