//
//  QJConsultCommentHeaderView.h
//  QJBox
//
//  Created by Sun on 2022/7/11.
//

#import <UIKit/UIKit.h>
#import "QJTypeChooseView.h"
#import "QJImageButton.h"

NS_ASSUME_NONNULL_BEGIN

typedef void (^ClickCommentStyle)(BOOL isDown);

@interface QJConsultCommentHeaderView : UIView
@property (nonatomic, copy) ClickCommentStyle clickComment;
/**发布时间*/
@property (nonatomic, strong) QJImageButton *timeBt;
- (void)configureCommentHeaderWithData:(BOOL)isDown emptyTitle:(NSString *)emptyTitle;

@end

NS_ASSUME_NONNULL_END
