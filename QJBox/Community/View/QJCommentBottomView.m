//
//  QJCommentBottomView.m
//  QJBox
//
//  Created by Sun on 2022/7/19.
//

#import "QJCommentBottomView.h"

@interface QJCommentBottomView ()
@property (nonatomic, strong) UIView *lineView;

@end

@implementation QJCommentBottomView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = UIColorHex(F5F5F5);
        [self addSubview:self.lineView];
        [self addSubview:self.expandButton];
        [self addSubview:self.reductionButton];
        [self makeConstraints];
    }
    return self;
}

- (void)makeConstraints {
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self);
        make.left.equalTo(self.mas_left).offset(8);
        make.right.equalTo(self.mas_right).offset(-8);
        make.height.mas_equalTo(1);
    }];
    [self.expandButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.lineView.mas_bottom).offset(8);
        make.height.mas_equalTo(16);
        make.centerX.equalTo(self);
    }];
    [self.reductionButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.lineView.mas_bottom).offset(8);
        make.right.equalTo(self.mas_right).offset(-8);
        make.height.mas_equalTo(16);
    }];
    
    
}

- (UIView *)lineView {
    if (!_lineView) {
        _lineView = [UIView new];
        _lineView.backgroundColor = UIColorHex(EBEDF0);
    }
    return _lineView;
}

- (QJImageButton *)expandButton {
    if (!_expandButton) {
        _expandButton = [QJImageButton new];
        _expandButton.titleLb.font = MYFONTALL(FONT_REGULAR, 10);
        _expandButton.titleLb.textColor = UIColorHex(919599);
        _expandButton.iconString = @"downOneIcon";
        _expandButton.imageSize = CGSizeMake(16, 16);
        _expandButton.space = 0;
        _expandButton.style = ImageButtonStyleRight;
    }
    return _expandButton;
}

- (QJImageButton *)reductionButton {
    if (!_reductionButton) {
        _reductionButton = [QJImageButton new];
        _reductionButton.titleString = @"收起回复";
        _reductionButton.titleLb.font = MYFONTALL(FONT_REGULAR, 10);
        _reductionButton.titleLb.textColor = UIColorHex(919599);
        _reductionButton.iconString = @"vectorUpIcon";
        _reductionButton.imageSize = CGSizeMake(16, 16);
        _reductionButton.space = 0;
        _reductionButton.style = ImageButtonStyleRight;
    }
    return _reductionButton;
}

- (void)configureCommentBottomWithData:(QJCommentTopModel *)model {
    self.expandButton.titleString = [NSString stringWithFormat:@"全部%ld条回复", model.total];
    if (model.isExpand) {
        _reductionButton.hidden = YES;
        if (model.total == model.comments.count) {
            _expandButton.hidden = YES;
            _reductionButton.hidden = NO;
            [self.reductionButton mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self.lineView.mas_bottom).offset(8);
                make.centerX.equalTo(self);
                make.height.mas_equalTo(16);
            }];
        }
        
        
    } else {
        _reductionButton.hidden = YES;
    }
    
}

@end
