//
//  QJConsultDetailFooterView.m
//  QJBox
//
//  Created by Sun on 2022/8/8.
//

#import "QJConsultDetailFooterView.h"
#import "QJImageButton.h"
#import "QJProductDetailViewController.h"//游戏详情页面
@interface QJConsultDetailFooterView ()

@property (nonatomic, strong) UIButton *tagBtn;

/**标签*/
@property (nonatomic, strong) UILabel *tagLb;
/**声明*/
@property (nonatomic, strong) UILabel *statementLb;
/**声明背景*/
@property (nonatomic, strong) UIView *statementView;
/**浏览量*/
@property (nonatomic, strong) QJImageButton *watchBt;

@property (nonatomic, strong) UILabel *timeLabel;

@property (nonatomic, strong) QJNewsListModel *dataModel;

@property (nonatomic, strong) UIImageView *hiddenArticleIcon;

@property (nonatomic, strong) UILabel *hiddenArticleLabel;

/**正在审核**/
@property (nonatomic, strong) UILabel *auditLable;

@end

@implementation QJConsultDetailFooterView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self addSubview:self.tagLb];
        [self addSubview:self.tagBtn];
        [self addSubview:self.statementView];
        [self.statementView addSubview:self.statementLb];
        [self addSubview:self.watchBt];
        [self addSubview:self.hiddenArticleIcon];
        [self addSubview:self.hiddenArticleLabel];
        [self addSubview:self.auditLable];

        [self makeConstraints];
    }
    return self;
}

- (void)makeConstraints {
    [self.tagLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(16);
        make.height.mas_equalTo(20*kWScale);
        make.top.equalTo(self.mas_top).offset(8);
    }];
    
    [self.tagBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(self.tagLb);
            make.width.height.equalTo(@(80*kWScale));
            make.height.equalTo(@(30*kWScale));
    }];
    
    [self.statementView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(16);
        make.right.equalTo(self.mas_right).offset(-16);
        make.top.equalTo(self.tagLb.mas_bottom).offset(10);
    }];
    
    [self.statementLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.statementView.mas_left).offset(8);
        make.right.equalTo(self.statementView.mas_right).offset(-8);
        make.top.equalTo(self.statementView.mas_top).offset(8);
        make.bottom.equalTo(self.statementView.mas_bottom).offset(-8);
    }];
    
    [self.watchBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.statementView.mas_bottom).offset(15);
        make.right.equalTo(self.mas_right).offset(-16);
    }];
    
    [self.auditLable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(16);
        make.centerY.equalTo(self.watchBt);
    }];
    
    [self.hiddenArticleIcon mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).offset(16);
            make.width.height.equalTo(@(16*kWScale));
            make.centerY.equalTo(self.watchBt);
            
    }];
    
    [self.hiddenArticleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.hiddenArticleIcon.mas_right).offset(2);
            make.centerY.equalTo(self.hiddenArticleIcon);
            make.height.equalTo(@(13*kWScale));
    }];

}

 


- (UILabel *)tagLb {
    if (!_tagLb) {
        _tagLb = [UILabel new];
        _tagLb.backgroundColor = UIColorHex(F2E5FF);
        _tagLb.textColor = UIColorHex(7341A3);
        _tagLb.font = MYFONTALL(FONT_REGULAR, 10);
        _tagLb.layer.cornerRadius = 2;
    }
    return _tagLb;
}

- (UIView *)statementView {
    if (!_statementView) {
        _statementView = [UIView new];
        _statementView.backgroundColor = UIColorHex(F5F5F5);
        _statementView.layer.cornerRadius = 2;
    }
    return _statementView;
}

- (UILabel *)statementLb {
    if (!_statementLb) {
        _statementLb = [UILabel new];
        _statementLb.textColor = UIColorHex(919599);
        _statementLb.font = MYFONTALL(FONT_REGULAR, 12);
        _statementLb.numberOfLines = 0;
        _statementLb.text = @"免责申明:本文仅代表该作者观点 不代表奇迹盒子立场 所有信息请以奇迹盒子官方公告为准.";
    }
    return _statementLb;
}

- (UIButton *)tagBtn {
    if(!_tagBtn){
        _tagBtn = [UIButton new];
        _tagBtn.backgroundColor = [UIColor clearColor];
        [_tagBtn addTarget:self action:@selector(tagBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _tagBtn;
}

- (QJImageButton *)watchBt {
    if (!_watchBt) {
        _watchBt = [QJImageButton new];
        _watchBt.titleString = @"浏览";
        _watchBt.titleLb.font = MYFONTALL(FONT_REGULAR, 11);
        _watchBt.titleLb.textColor = UIColorHex(919599);
        _watchBt.iconString = @"home_preview";
        _watchBt.space = 2;
        _watchBt.size = CGSizeMake(16*kWScale, 16*kWScale);
        _watchBt.style = ImageButtonStyleLeft;
    }
    return _watchBt;
}

- (UIImageView *)hiddenArticleIcon {
    if (!_hiddenArticleIcon) {
        _hiddenArticleIcon = [UIImageView new];
        _hiddenArticleIcon.image = [UIImage imageNamed:@"qj_closeEye_16"];
        _hiddenArticleIcon.hidden = YES;
    }
    return _hiddenArticleIcon;
}


- (UILabel *)hiddenArticleLabel {
    if (!_hiddenArticleLabel) {
        _hiddenArticleLabel = [UILabel new];
        _hiddenArticleLabel.font = FontRegular(12);
        _hiddenArticleLabel.textColor = kColorWithHexString(@"#919599");
        _hiddenArticleLabel.textAlignment = NSTextAlignmentLeft;
        _hiddenArticleLabel.text = @"该文章已被隐藏";
        _hiddenArticleLabel.hidden = YES;
    }
    return _hiddenArticleLabel;
}

- (UILabel *)auditLable {
    if (!_auditLable) {
        _auditLable = [UILabel new];
        _auditLable.text = @"资讯审核中...";
        _auditLable.textColor = UIColorHex(FF3B30);
        _auditLable.font = MYFONTALL(FONT_REGULAR, 10);
        _auditLable.hidden = YES;
    }
    return _auditLable;
}


- (void)configureDetailFooterWithNewsData:(QJNewsListModel *)dataModel {
    self.dataModel = dataModel;
    self.watchBt.titleString = [NSString stringWithFormat:@"浏览%@", [NSString getConvertTenThousandNumber:dataModel.infoViewCount]];
    self.tagLb.text = [NSString stringWithFormat:@"  %@  ", kCheckStringNil(dataModel.infoTag)];
    if(dataModel.hidden){
        self.hiddenArticleIcon.hidden = NO;
        self.hiddenArticleLabel.hidden = NO;
    }
    
    if(!dataModel.checked) {
        self.watchBt.hidden = YES;
        self.auditLable.hidden = NO;
    }
}

//跳到游戏详情
- (void)tagBtnAction:(UIButton *)sender {
   UIViewController *vc = [QJAppTool getCurrentViewController];
    QJProductDetailViewController *checkInVC = [[QJProductDetailViewController alloc] init];
    if (self.dataModel.gameId) {
        checkInVC.idStr = self.dataModel.gameId;
        [vc.navigationController pushViewController:checkInVC animated:YES];
    }
}

@end
