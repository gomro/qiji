//
//  QJCommentInputToolbar.h
//  QJBox
//
//  Created by Sun on 2022/7/14.
//

#import <UIKit/UIKit.h>
#import "QJCommentUser.h"
NS_ASSUME_NONNULL_BEGIN
typedef void(^inputTextBlock)(NSString *text);

@interface QJCommentInputToolbar : UIView
/**设置输入框最大行数*/
@property (nonatomic, assign) NSInteger textViewMaxLine;
/**输入框文字字体*/
@property (nonatomic, strong) UIFont *fontSize;
/**占位文字*/
@property (nonatomic, copy) NSString *placeholder;
/**输入文字的最大长度*/
@property (nonatomic, assign) NSInteger maxLength;
/**文本框文案*/
@property (nonatomic, copy) NSString *textString;
/**表情按钮*/
@property (nonatomic, strong) UIButton *emojiButton;
/**文本输入框*/
@property (nonatomic, strong) UITextView *textView;

@property (nonatomic, strong) QJCommentUser *toUser;//要回复的用户信息
/**键盘高度*/
@property (nonatomic, assign) CGFloat keyboardHeight;


/**收回键盘*/
-(void)bounceToolbar;
/**弹出键盘*/
- (void)popToolbar;
/**点击发送后的文字*/
- (void)inputToolbarSendText:(inputTextBlock)sendText;
/**获取textView 文案*/
- (NSString *)getTextViewString;
@end

NS_ASSUME_NONNULL_END
