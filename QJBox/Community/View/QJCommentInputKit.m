//
//  QJCommentInputKit.m
//  QJBox
//
//  Created by Sun on 2022/7/14.
//

#import "QJCommentInputKit.h"
#import "QJCommentInputToolbar.h"
#import "ICChatBoxFaceView.h"

@interface QJCommentInputKit ()
@property (nonatomic, strong) QJCommentInputToolbar *inputToolbar;
@property (nonatomic, strong) UIButton *backgroundView;
@property (nonatomic, copy) ConfirmBlock closeBlock;
@property (nonatomic, strong) ICChatBoxFaceView *faceListView;
@property (nonatomic, strong) UIView *faceBackView;



@end
@implementation QJCommentInputKit

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight);
        [[UIApplication sharedApplication].keyWindow addSubview:self.backgroundView];
        [self.backgroundView addSubview:self.inputToolbar];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHidden:) name:UIKeyboardWillHideNotification object:nil];
        [self.backgroundView addSubview:self.faceListView];
        [self.backgroundView addSubview:self.faceBackView];
        [self.inputToolbar.emojiButton addTarget:self action:@selector(didClickemojiButton) forControlEvents:UIControlEventTouchUpInside];


    }
    return self;
}

- (void)showInput:(ConfirmBlock)confirmHandler {
    __weak typeof(self) weakSelf = self;
    [IQKeyboardManager sharedManager].enableAutoToolbar = NO; // 控制是否显示键盘上的工具条

    [self.inputToolbar popToolbar];
    [self.inputToolbar inputToolbarSendText:^(NSString *text) {
        __typeof(&*weakSelf) strongSelf = weakSelf;
        // 清空输入框文字
        [strongSelf.inputToolbar bounceToolbar];
        [strongSelf.backgroundView removeFromSuperview];
        [self removeFromSuperview];
        confirmHandler(text);
    }];
    
    
}

- (void)closeInput:(ConfirmBlock)confirmHandler {
    [IQKeyboardManager sharedManager].enableAutoToolbar = YES; // 控制是否显示键盘上的工具条

    self.closeBlock = confirmHandler;
}

- (QJCommentInputToolbar *)inputToolbar {
    if (!_inputToolbar) {
        self.inputToolbar = [[QJCommentInputToolbar alloc] init];
        self.inputToolbar.textViewMaxLine = 5;
        self.inputToolbar.fontSize = MYFONTALL(FONT_REGULAR, 15);
        self.inputToolbar.placeholder = @"写评论...";
        self.inputToolbar.maxLength = 200;
    }
    return _inputToolbar;
    
}

- (UIButton *)backgroundView {
    if (!_backgroundView) {
        self.backgroundView = [[UIButton alloc] init];
        self.backgroundView.backgroundColor = RGBAlpha(0, 0, 0, 0.3);
        self.backgroundView.frame = [UIScreen mainScreen].bounds;
        [self.backgroundView addTarget:self action:@selector(dismissKeyboardAndBgBtutton:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _backgroundView;
}

-(void)dismissKeyboardAndBgBtutton:(UIButton *)sender{
    if (self.closeBlock) {
         NSString *textString = [self.inputToolbar getTextViewString];
         self.closeBlock(kCheckStringNil(textString));
     }
    [self.inputToolbar bounceToolbar];
    [sender removeFromSuperview];
    [self removeFromSuperview];
}

-(void)setNormalContent:(NSString *)content{
    self.inputToolbar.placeholder = content;
}


- (void)setMaxContentLength:(NSInteger)lenght{
    self.inputToolbar.maxLength = lenght;
}

- (void)setMaxLineNum:(NSInteger)num {
    self.inputToolbar.textViewMaxLine = num;
}

- (void)setDescTitle:(NSString *)descTitle {
    self.inputToolbar.textString = descTitle;
}

- (void)keyboardWillHidden:(NSNotification *)notification {
    [self.inputToolbar bounceToolbar];
//    [self.backgroundView removeFromSuperview];
//    [self removeFromSuperview];
//    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didClickemojiButton {
    self.inputToolbar.emojiButton.selected = !self.inputToolbar.emojiButton.selected;
    if (self.inputToolbar.emojiButton.selected) {
        [self.inputToolbar.textView resignFirstResponder];
        self.inputToolbar.keyboardHeight = 260 + Bottom_SN_iPhoneX_OR_LATER_SPACE;
        
        // 定义好动作
        void (^animation)(void) = ^void(void) {
            self.faceListView.transform = CGAffineTransformMakeTranslation(0, - (self.inputToolbar.keyboardHeight));
            self.faceBackView.transform = CGAffineTransformMakeTranslation(0, - Bottom_SN_iPhoneX_OR_LATER_SPACE);
            self.inputToolbar.top = kScreenHeight -  260 - self.inputToolbar.height -Bottom_SN_iPhoneX_OR_LATER_SPACE;
        };
        [UIView animateWithDuration:0.25 animations:animation];

    } else {
        [self.inputToolbar.textView becomeFirstResponder];
        
        // 定义好动作
        void (^animation)(void) = ^void(void) {
            self.faceListView.transform = CGAffineTransformIdentity;
            self.faceBackView.transform = CGAffineTransformIdentity;
        };

        [UIView animateWithDuration:0.25 animations:animation];
    
    }
}


- (UIView *)faceBackView {
    if (!_faceBackView) {
        _faceBackView = [UIView new];
        _faceBackView.backgroundColor = [UIColor whiteColor];
        _faceBackView.frame = CGRectMake(0, kScreenHeight, kScreenWidth, Bottom_SN_iPhoneX_OR_LATER_SPACE);
    }
    return _faceBackView;
}

- (ICChatBoxFaceView *)faceListView {
    if (!_faceListView) {
        _faceListView = [[ICChatBoxFaceView alloc] initWithFrame:CGRectMake(0, kScreenHeight, kScreenWidth, 260)];
    }
    return _faceListView;
}

-(void)setToUser:(QJCommentUser *)toUser {
    _toUser = toUser;
    self.inputToolbar.toUser = toUser;
}

@end
