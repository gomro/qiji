//
//  QJNewsTagView.h
//  QJBox
//
//  Created by macm on 2022/7/28.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJNewsTagView : UIView
/**
 有多少个标签,存放的是model
 */
@property (nonatomic, strong) NSArray *tagModelArray;

//默认24
@property (nonatomic, assign) CGFloat tagHeight;

//tag之间的列距离 默认8
@property (nonatomic, assign) CGFloat margin;

//tag之间的行距离 默认16
@property (nonatomic, assign) CGFloat top;

@property (nonatomic, copy) void (^tagViewClickEvent)(NSArray *tagArray,NSArray *modelArr);

// 已选择的tag数组
@property (nonatomic, strong) NSMutableArray *selectTagArray;

// 是否单选
@property (nonatomic, assign) BOOL isSingleChoice;

// 重置标签数组
- (void)resetTagArray;

@end

NS_ASSUME_NONNULL_END
