//
//  QJDraftEmptyView.m
//  QJBox
//
//  Created by wxy on 2022/9/22.
//

#import "QJDraftEmptyView.h"

@interface QJDraftEmptyView ()


@property (nonatomic, strong) UIImageView *topImageView;

@property (nonatomic, strong) UILabel *titleLabel;

@end



@implementation QJDraftEmptyView

 
- (instancetype)initWithFrame:(CGRect)frame {
    if(self = [super initWithFrame:frame]) {
        [self addSubview:self.topImageView];
        [self addSubview:self.titleLabel];
        [self.topImageView mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.centerX.equalTo(self);
                    make.height.width.equalTo(@(Get375Width(240)));
                    make.top.equalTo(self).offset(Get375Width(94));
        }];
        [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.centerX.equalTo(self);
                    make.bottom.equalTo(self.topImageView.mas_bottom).offset(-Get375Width(24));
                    make.left.equalTo(self.topImageView).offset(Get375Width(60));
            make.right.equalTo(self.topImageView).offset(-Get375Width(60));
        }];
        
    }
    return self;
}




#pragma mark ---------- getter && setter

- (UIImageView *)topImageView {
    if(!_topImageView) {
        _topImageView = [UIImageView new];
        _topImageView.image = [UIImage imageWithColor:kColorWithHexString(@"#D9D9D9")];
        
    }
    return _topImageView;
}


- (UILabel *)titleLabel {
    if(!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.font = FontRegular(12);
        _titleLabel.textColor = kColorWithHexString(@"000000");
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.numberOfLines =0;
    }
    return _titleLabel;
}

-(void)setBgColor:(UIColor *)bgColor {
    _bgColor = bgColor;
    self.backgroundColor = bgColor;
}

- (void)setString:(NSString *)string {
    _string = string;
    self.titleLabel.text = string;
}
//
- (void)setOffsetY:(CGFloat)offsetY {
    _offsetY = offsetY;
    [self.topImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.centerX.equalTo(self);
                make.height.width.equalTo(@(Get375Width(240)));
                make.top.equalTo(self).offset(Get375Width(offsetY));
    }];
}


- (void)setImageStr:(NSString *)imageStr {
    _imageStr = imageStr;
    self.topImageView.image = [UIImage imageNamed:imageStr];
    
}

@end
