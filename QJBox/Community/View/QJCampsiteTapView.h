//
//  QJCampsiteTapView.h
//  QJBox
//
//  Created by Sun on 2022/7/8.
//

#import <UIKit/UIKit.h>
#import "QJCampsiteListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJCampsiteTapView : UIView

- (void)configureTapWithData:(QJCampsiteListModel *)model;
@end

NS_ASSUME_NONNULL_END
