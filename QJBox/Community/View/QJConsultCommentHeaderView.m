//
//  QJConsultCommentHeaderView.m
//  QJBox
//
//  Created by Sun on 2022/7/11.
//

#import "QJConsultCommentHeaderView.h"

@interface QJConsultCommentHeaderView ()
/**标题*/
@property (nonatomic, strong) UILabel *title;


@property (nonatomic, strong) UIView *lineView;
/**时间倒序**/
@property (nonatomic, assign) BOOL isDown;

/**空白页**/
@property (nonatomic, strong) UIView *emptyView;
/**空白页标题**/
@property (nonatomic, strong) UILabel *emptyTitle;
/**空白页图片**/
@property (nonatomic, strong) UIImageView *emptyImage;

@end

@implementation QJConsultCommentHeaderView
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        self.isDown = YES;
//        [self addSubview:self.lineView];
        [self addSubview:self.title];
        [self addSubview:self.timeBt];
        [self addSubview:self.emptyView];
        [self.emptyView addSubview:self.emptyTitle];
        [self.emptyView addSubview:self.emptyImage];
        [self makeConstraints];
    
    }
    return self;
}

- (void)makeConstraints {

    
    [self.title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(16);
        make.centerY.equalTo(self);
    }];
    
    [self.timeBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(-16);  
        make.centerY.equalTo(self.title);
    }];
        
//    [self.emptyView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(self.title.mas_bottom).offset(34);
//        make.centerX.equalTo(self);
//        make.width.mas_equalTo(kScreenWidth);
//        make.height.mas_equalTo(Get375Width(350));
//    }];
//
//    [self.emptyImage mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(self.emptyView.mas_top).offset(Get375Width(22));
//        make.centerX.equalTo(self);
//        make.width.height.equalTo(@(Get375Width(240)));
//    }];
//
//    [self.emptyTitle mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(self.emptyImage).offset(Get375Width(180));
//        make.centerX.equalTo(self);
//        make.height.mas_equalTo(Get375Width(18));
//    }];
}

- (UILabel *)title {
    if (!_title) {
        _title = [UILabel new];
        _title.font = MYFONTALL(FONT_REGULAR, 16);
        _title.textColor = UIColorHex(000000);
    }
    return _title;
}

- (QJImageButton *)timeBt {
    if (!_timeBt) {
        _timeBt = [QJImageButton new];
        _timeBt.titleString = @"发布时间";
        _timeBt.titleLb.font = MYFONTALL(FONT_REGULAR, 12);
        _timeBt.titleLb.textColor = UIColorHex(474849);
        _timeBt.iconString = @"iconDownOne";
        _timeBt.space = 4;
        _timeBt.size = CGSizeMake(16, 16);
        _timeBt.style = ImageButtonStyleRight;
        [_timeBt addTarget:self action:@selector(timeChoose:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _timeBt;
}

- (UIView *)lineView {
    if (!_lineView) {
        _lineView = [UIView new];
        _lineView.backgroundColor = UIColorHex(F9F9F9);
    }
    return _lineView;
}

- (UIView *)emptyView {
    if(!_emptyView) {
        _emptyView = [[UIView alloc] init];
        _emptyView.hidden = YES;
    }
    return _emptyView;
}

- (UILabel *)emptyTitle {
    if (!_emptyTitle) {
        _emptyTitle = [UILabel new];
        _emptyTitle.font = MYFONTALL(FONT_REGULAR, 12);
        _emptyTitle.textColor = UIColorHex(000000);
    }
    return _emptyTitle;
}

- (UIImageView *)emptyImage {
    if(!_emptyImage) {
        _emptyImage = [UIImageView new];
        _emptyImage.image = [UIImage imageNamed:@"empty_no_data"];
    }
    return _emptyImage;
}

- (void)configureCommentHeaderWithData:(BOOL)isDown emptyTitle:(NSString *)emptyTitle {
    self.title.text = @"全部评论";
    [self.timeBt setTitle:@"发布时间" forState:UIControlStateNormal];
    self.isDown = isDown;
    [self changeTimeImage];
    if(![kCheckStringNil(emptyTitle) isEqualToString:@""]) {
        self.emptyView.hidden = NO;
        self.emptyTitle.text = emptyTitle;
    } else {
        self.emptyView.hidden = YES;
    }
 
}

- (void)timeChoose:(UIButton *)sender {
    self.isDown = !self.isDown;
    [self changeTimeImage];
    if(self.clickComment) {
        self.clickComment(self.isDown);
    }
}

- (void)changeTimeImage {
    if(self.isDown) {
        _timeBt.iconString = @"iconDownOne";
    } else {
        _timeBt.iconString = @"iconUpOne";
    }
}

@end
