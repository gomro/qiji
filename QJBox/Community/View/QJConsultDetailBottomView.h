//
//  QJConsultDetailBottomView.h
//  QJBox
//
//  Created by Sun on 2022/7/11.
//

#import <UIKit/UIKit.h>
#import "QJCampsiteDetailModel.h"
#import "QJNewsListModel.h"
NS_ASSUME_NONNULL_BEGIN
typedef void(^ButtonClickBlock)(NSInteger type);

@interface QJConsultDetailBottomView : UIView

@property (nonatomic, copy) ButtonClickBlock clickButton;


- (void)configureDetailBottomWithData:(QJCampsiteDetailModel *)dataModel;
- (void)configureDetailBottomWithNewsData:(QJNewsListModel *)dataModel;

@end

NS_ASSUME_NONNULL_END
