//
//  QJCampsiteDescriptionModel.h
//  QJBox
//
//  Created by Sun on 2022/7/13.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJCampsiteDescriptionModel : NSObject
/**营地背景地址*/
@property (nonatomic, copy) NSString *bgImage;
/**营地卡片背景地址*/
@property (nonatomic, copy) NSString *cardImage;
/**营地名称*/
@property (nonatomic, copy) NSString *name;
/**营地粉丝数量*/
@property (nonatomic, copy) NSString *fansCount;
/**营地动态数*/
@property (nonatomic, copy) NSString *newsCount;
/**简介*/
@property (nonatomic, copy) NSString *shotDesc;
/**营地粉丝*/
@property (nonatomic, copy) NSArray *users;
/**游戏id*/
@property (nonatomic, copy) NSString *gameId;
@end

NS_ASSUME_NONNULL_END
