//
//  QJCampsiteDescriptionModel.m
//  QJBox
//
//  Created by Sun on 2022/7/13.
//

#import "QJCampsiteDescriptionModel.h"
#import "QJCommentUser.h"
@implementation QJCampsiteDescriptionModel
+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"users" : [QJCommentUser class]};
}
@end
