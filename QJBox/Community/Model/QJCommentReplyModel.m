//
//  QJCommentReplyModel.m
//  QJBox
//
//  Created by Sun on 2022/7/11.
//

#import "QJCommentReplyModel.h"

@implementation QJCommentReplyModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        _mediabase_id = @"89757";
    }
    return self;
}


- (NSAttributedString *)attributedText {
    if (self.replUserName.length > 0 && self.userName.length > 0) {
        // 有回复
        NSString *textString = [NSString stringWithFormat:@"%@ 回复 %@: %@ %@", self.userName, self.replUserName, self.content, self.timeShow];
        NSMutableAttributedString *mutableAttributedString = [[NSMutableAttributedString alloc] initWithString:textString];
        mutableAttributedString.font = MYFONTALL(FONT_REGULAR, 12);
        mutableAttributedString.color = UIColorHex(16191C);
        mutableAttributedString.lineSpacing = 10;
        
        NSRange fromUserRange = NSMakeRange(0, self.userName.length);
//        YYTextHighlight *fromUserHighlight = [YYTextHighlight highlightWithBackgroundColor:UIColorHex(2261A9)];
////        fromUserHighlight.userInfo = @{@"CommentUserKey":self.fromUser};
//        [mutableAttributedString setTextHighlight:fromUserHighlight range:fromUserRange];
        // 设置昵称颜色
        [mutableAttributedString setColor:UIColorHex(2261A9) range:NSMakeRange(0, self.userName.length)];
        
        
        
        NSRange toUserRange = [textString rangeOfString:[NSString stringWithFormat:@"回复 %@:", self.replUserName]];
        // 文本高亮模型
//        YYTextHighlight *toUserHighlight = [YYTextHighlight highlightWithBackgroundColor:UIColorHex(919599)];
        // 这里痛过属性的userInfo保存User模型，后期通过获取模型然后获取User模型
//        toUserHighlight.userInfo = @{@"CommentUserKey":self.toUser};
        
        // 点击用户的昵称的事件传递
//        toUserHighlight.tapAction = ^(UIView *containerView, NSAttributedString *text, NSRange range, CGRect rect)
//        {
//            // 这里通过通知把用户的模型传递出去
//        };
        
//        [mutableAttributedString setTextHighlight:toUserHighlight range:toUserRange];
        [mutableAttributedString setColor:UIColorHex(919599) range:toUserRange];
        
        
        NSRange toTimerRange = [textString rangeOfString:[NSString stringWithFormat:@"%@", self.timeShow]];

        [mutableAttributedString setColor:UIColorHex(C8CACC) range:toTimerRange];
        [mutableAttributedString setFont:MYFONTALL(FONT_REGULAR, 8) range:toTimerRange];
        
        return mutableAttributedString;
        
        
    }else{
        
        // 没有回复
        NSString *textString = [NSString stringWithFormat:@"%@: %@", self.replUserName, self.text];
        NSMutableAttributedString *mutableAttributedString = [[NSMutableAttributedString alloc] initWithString:textString];
        mutableAttributedString.font = MYFONTALL(FONT_REGULAR, 12);
        mutableAttributedString.color = UIColorHex(16191C);
        mutableAttributedString.lineSpacing = 10;
        
        NSRange fromUserRange = NSMakeRange(0, self.replUserName.length+1);
        // 设置昵称颜色
        [mutableAttributedString setColor:UIColorHex(2261A9) range:fromUserRange];
        
        YYTextHighlight *fromUserHighlight = [YYTextHighlight highlightWithBackgroundColor:[UIColor colorWithWhite:0.000 alpha:0.220]];
//        fromUserHighlight.userInfo = @{@"CommentUserKey":self.fromUser};
        [mutableAttributedString setTextHighlight:fromUserHighlight range:fromUserRange];
        
        
        
        return mutableAttributedString;
    }
    
    return nil;
}
@end
