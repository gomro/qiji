//
//  QJCommentUser.h
//  QJBox
//
//  Created by Sun on 2022/7/11.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJCommentUser : NSObject
/** userId */
@property (nonatomic , copy) NSString *userId;

/** 用户昵称 */
@property (nonatomic , copy) NSString *nickname;

/** 头像地址 */
@property (nonatomic , copy) NSString *avatarUrl;


@property (nonatomic , copy) NSString *coverImage;

@property (nonatomic, copy) NSString *commentStr;//评论
@end

NS_ASSUME_NONNULL_END
