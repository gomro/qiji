//
//  QJCommentTopFrame.m
//  QJBox
//
//  Created by Sun on 2022/7/11.
//

#import "QJCommentTopFrame.h"
#import "QJCommentReplyModel.h"
#import "QJCommentReplyFrame.h"


@interface QJCommentTopFrame ()
/** 头像frame */
@property (nonatomic , assign) CGRect avatarFrame;

/** 更多frame */
@property (nonatomic , assign) CGRect moreFrame;

/** 话题内容frame */
@property (nonatomic , assign) CGRect textFrame;

/** height*/
@property (nonatomic , assign) CGFloat height;

/** tableViewFrame cell嵌套tableView用到 本人有点懒 ，公用了一套模型 */
@property (nonatomic , assign ) CGRect tableViewFrame;

@end
@implementation QJCommentTopFrame

- (instancetype)init {
    self = [super init];
    if (self) {
        // 初始化
        _commentFrames = [NSMutableArray array];
    }
    return self;
}

#pragma mark - Setter
- (void)setTopic:(QJCommentTopModel *)topic {
    _topic = topic;
    
    // 整个宽度
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    
    // 头像
    CGFloat avatarX = 16;
    CGFloat avatarY = 10;
    CGFloat avatarW = 200;
    CGFloat avatarH = 40;
    self.avatarFrame = (CGRect){{avatarX , avatarY},{avatarW , avatarH}};

    // 内容
    CGFloat textX = 59;
    CGSize textLimitSize = CGSizeMake(width - textX - 16, MAXFLOAT);
    CGFloat textY = 55;
    
    CGFloat textH = [YYTextLayout layoutWithContainerSize:textLimitSize text:topic.attributedText].textBoundingSize.height;
    if (textH > 40 && !topic.showAll) {
        textH = 60;
    } else {
#warning TODO 删除多余的20高度
        textH = textH;
    }
        
    self.textFrame = (CGRect){{textX , textY} , {textLimitSize.width, textH}};
        
    
    CGFloat tableViewX = textX;
    CGFloat tableViewY = CGRectGetMaxY(self.textFrame)+6;
    CGFloat tableViewW = textLimitSize.width;
    CGFloat tableViewH = 0;
    // 评论数据
    if (topic.comments > 0) {
        for (QJCommentReplyModel *comment in topic.comments) {
            QJCommentReplyFrame *commentFrame = [[QJCommentReplyFrame alloc] init];
            commentFrame.maxW = textLimitSize.width;
            commentFrame.comment = comment;
            [self.commentFrames addObject:commentFrame];
            tableViewH += commentFrame.cellHeight;
        }
        if (topic.total > 2) {
            tableViewH += 33;
        }
        
    }
    self.tableViewFrame = CGRectMake(tableViewX, tableViewY, tableViewW, tableViewH);
    // 自身高度
    self.height = CGRectGetMaxY(self.textFrame);
}


@end
