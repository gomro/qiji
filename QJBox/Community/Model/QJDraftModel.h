//
//  QJDraftModel.h
//  QJBox
//
//  Created by Sun on 2022/7/22.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJDraftModel : NSObject
/**草稿id*/
@property (nonatomic, copy) NSString *draftId;

/**标题*/
@property (nonatomic, copy) NSString *title;

/**时间*/
@property (nonatomic, copy) NSString *showTime;
@end

NS_ASSUME_NONNULL_END
