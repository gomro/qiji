//
//  QJCommentTopFrame.h
//  QJBox
//
//  Created by Sun on 2022/7/11.
//

#import <Foundation/Foundation.h>
#import "QJCommentTopModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJCommentTopFrame : NSObject
/** 头像frame */
@property (nonatomic , assign , readonly) CGRect avatarFrame;


/** 话题内容frame */
@property (nonatomic , assign , readonly) CGRect textFrame;

/** height 这里只是 整个话题占据的高度 */
@property (nonatomic , assign , readonly) CGFloat height;


/** 评论尺寸模型 由于后期需要用到，所以不涉及为只读 */
@property (nonatomic , strong ) NSMutableArray *commentFrames;


/** tableViewFrame cell嵌套tableView用到  */
@property (nonatomic , assign , readonly) CGRect tableViewFrame;


/** 话题模型 */
@property (nonatomic , strong) QJCommentTopModel *topic;



@end

NS_ASSUME_NONNULL_END
