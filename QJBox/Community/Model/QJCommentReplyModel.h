//
//  QJCommentReplyModel.h
//  QJBox
//
//  Created by Sun on 2022/7/11.
//

#import <Foundation/Foundation.h>
#import "QJCommentUser.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJCommentReplyModel : NSObject
/** 视频的id */
@property (nonatomic , copy) NSString *mediabase_id;

/** 创建时间 */
@property (nonatomic , copy) NSString *creatTime;

/** 回复用户模型 */
@property (nonatomic , strong) QJCommentUser *toUser;

/** 来源用户模型 */
@property (nonatomic , strong) QJCommentUser *fromUser;

/** 话题内容 */
@property (nonatomic, copy) NSString *text;


/** 获取富文本 */
- (NSAttributedString *)attributedText;


/**评论-评论id*/
@property (nonatomic, copy) NSString *commentId;
/**评论-子评论内容*/
@property (nonatomic, copy) NSString *content;
/**被回复用户-用户id*/
@property (nonatomic, copy) NSString *replUserId;
/**被回复用户-用户名*/
@property (nonatomic, copy) NSString *replUserName;
/**被回复用户头像*/
@property (nonatomic, copy) NSString *replCoverImage;

/**评论-展示时间*/
@property (nonatomic, copy) NSString *timeShow;
/**评论用户-用户id*/
@property (nonatomic, copy) NSString *userId;
/**评论用户-用户名*/
@property (nonatomic, copy) NSString *userName;

/**评论用户头像*/
@property (nonatomic, copy) NSString *coverImage;
@end

NS_ASSUME_NONNULL_END
