//
//  QJCampsiteListModel.h
//  QJBox
//
//  Created by Sun on 2022/7/13.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJCampsiteListModel : NSObject
/**营地图标*/
@property (nonatomic, copy) NSString *iconAddress;
/**营地id*/
@property (nonatomic, copy) NSString *campsiteID;
/**营地名称*/
@property (nonatomic, copy) NSString *name;


@end

NS_ASSUME_NONNULL_END
