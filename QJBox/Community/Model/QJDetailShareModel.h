//
//  QJDetailShareModel.h
//  QJBox
//
//  Created by Sun on 2022/10/12.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJDetailShareModel : NSObject

/**图片*/
@property (nonatomic, copy) NSString *picture;

/**分享链接*/
@property (nonatomic, copy) NSString *shareUrl;

/**详情*/
@property (nonatomic, copy) NSString *summary;

/**标题*/
@property (nonatomic, copy) NSString *title;

@end

NS_ASSUME_NONNULL_END
