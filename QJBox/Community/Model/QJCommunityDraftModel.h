//
//  QJCommunityDraftModel.h
//  QJBox
//
//  Created by wxy on 2022/8/7.
//

#import <Foundation/Foundation.h>
#import "QJNewsTagModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface QJCommunityDraftModel : NSObject

@property (nonatomic, copy) NSAttributedString *richAttributedString;

@property (nonatomic, strong) UIImage *overImage;

@property (nonatomic, strong) NSURL *viderUrl;

@property (nonatomic, strong) QJNewsTagModel *tag;

@property (nonatomic, assign) BOOL isOrignal;

@property (nonatomic, assign) BOOL isForward;

@property (nonatomic, copy) NSString *title;

@property (nonatomic, assign) NSInteger type;//0是图片类型，1是视频类型

@property (nonatomic, assign) NSInteger timesp;//时间戳

@property (nonatomic, copy) NSString *idStr;//已发布的id

@property (nonatomic, copy) NSString *campId;//动态频道id

@property (nonatomic, assign) BOOL isSelected;




@end

NS_ASSUME_NONNULL_END
