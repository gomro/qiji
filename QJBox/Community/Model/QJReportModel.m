//
//  QJReportModel.m
//  QJBox
//
//  Created by Sun on 2022/7/22.
//

#import "QJReportModel.h"

@implementation QJReportModel
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"reportId" : @"id", @"descriptionStr":@"description"};
}
@end
