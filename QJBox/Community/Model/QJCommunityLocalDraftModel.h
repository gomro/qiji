//
//  QJCommunityLocalDraftModel.h
//  QJBox
//
//  Created by wxy on 2022/8/5.
//

#import <Foundation/Foundation.h>
#import "MJExtension.h"
#import "QJNewsTagModel.h"
NS_ASSUME_NONNULL_BEGIN

/// 草稿箱模型
@interface QJCommunityLocalDraftModel : NSObject<NSCoding>

@property (nonatomic, copy) NSAttributedString *richAttributedString;

@property (nonatomic, copy) NSString *overImagePath;

@property (nonatomic, strong) NSURL *viderUrl;

@property (nonatomic, strong) QJNewsTagModel *tag;

@property (nonatomic, assign) BOOL isOrignal;

@property (nonatomic, assign) BOOL isForward;

@property (nonatomic, copy) NSString *title;

@property (nonatomic, assign) NSInteger timesp;//时间戳

@property (nonatomic, assign) NSInteger type;//0是图片类型，1是视频类型

@property (nonatomic, copy) NSString *idStr;//已发布的文章id

@property (nonatomic, copy) NSString *campId;//动态频道id
@end

NS_ASSUME_NONNULL_END
