//
//  QJCommentReplyFrame.h
//  QJBox
//
//  Created by Sun on 2022/7/11.
//

#import <Foundation/Foundation.h>
#import "QJCommentReplyModel.h"


NS_ASSUME_NONNULL_BEGIN

@interface QJCommentReplyFrame : NSObject
/** 内容尺寸 */
@property (nonatomic , assign , readonly) CGRect textFrame;
/** cell高度 */
@property (nonatomic , assign , readonly) CGFloat cellHeight;

/** 最大宽度 外界传递 */
@property (nonatomic , assign) CGFloat maxW;

/** 评论模型 外界传递 */
@property (nonatomic , strong) QJCommentReplyModel *comment;
@end

NS_ASSUME_NONNULL_END
