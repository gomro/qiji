//
//  QJCommentTopModel.m
//  QJBox
//
//  Created by Sun on 2022/7/11.
//

#import "QJCommentTopModel.h"
#import "QJCommentReplyModel.h"

@interface QJCommentTopModel ()

/** 点赞数string */
@property (nonatomic , copy) NSString * thumbNumsString;

@end

@implementation QJCommentTopModel
- (instancetype)init {
    self = [super init];
    if (self) {
        // 初始化
        _comments = [NSMutableArray array];
        // 由于这里只是评论一个视频
        _mediabase_id = @"89757";
        _page = 1;

    }
    return self;
}


#pragma mark - 公共方法

- (NSAttributedString *)attributedText {
    if (self.content == nil) return nil;

    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:self.content];
    attributedString.font = MYFONTALL(FONT_REGULAR, 14);
    attributedString.color = UIColorHex(16191C);
    attributedString.lineSpacing = 5;
    return attributedString;
}

#pragma mark - Setter

- (void)setThumbNums:(long long)thumbNums
{
    _thumbNums = thumbNums;
    
    self.thumbNumsString = [self _thumbNumsStringWithThumbNums:thumbNums];
}

- (void)setSubComment:(NSDictionary *)subComment {
    _subComment = subComment;
    NSArray *records = subComment[@"records"];
    self.total = [subComment[@"total"] integerValue];
    [self.comments removeAllObjects];
    if (records.count) {
        for (NSDictionary *dic in records) {
            QJCommentReplyModel *replyModel = [QJCommentReplyModel modelWithDictionary:dic];
            [self.comments addObject:replyModel];
        }
    }
    
}


#pragma mark - 私有方法
// 点赞
- (NSString *)_thumbNumsStringWithThumbNums:(long long)thumbNums
{
    NSString *titleString = nil;
    
    if (thumbNums >= 10000) { // 上万
        CGFloat final = thumbNums / 10000.0;
        titleString = [NSString stringWithFormat:@"%.1f万", final];
        // 替换.0为空串
        titleString = [titleString stringByReplacingOccurrencesOfString:@".0" withString:@""];
    } else if (thumbNums > 0) { // 一万以内
        titleString = [NSString stringWithFormat:@"%lld", thumbNums];
    }
    
    return titleString;
}

@end
