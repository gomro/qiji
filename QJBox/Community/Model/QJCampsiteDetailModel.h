//
//  QJCampsiteDetailModel.h
//  QJBox
//
//  Created by Sun on 2022/7/14.
//

#import <Foundation/Foundation.h>
#import "QJDetailShareModel.h"

@interface QJCampsiteDetailModel : NSObject
/**动态-分享*/
@property (nonatomic, strong) QJDetailShareModel *shareInfo;
/**动态-id*/
@property (nonatomic, copy) NSString *newsId;
/**动态-发布时间*/
@property (nonatomic, copy) NSString *newsAnnounceTime;
/**动态-评论数*/
@property (nonatomic, copy) NSString *newsCommentCount;
/**动态-点赞数*/
@property (nonatomic, copy) NSString *newsLikeCount;
/**动态-收藏数*/
@property (nonatomic, copy) NSString *newsCollectCount;
/**动态-标题*/
@property (nonatomic, copy) NSString *newsTitle;

/**用户-头像*/
@property (nonatomic, copy) NSString *userCoverImage;
/**用户-id*/
@property (nonatomic, copy) NSString *userId;
/**用户-等级*/
@property (nonatomic, copy) NSString *userLevel;
/**用户-昵称*/
@property (nonatomic, copy) NSString *userNickName;
/**用户-标签*/
@property (nonatomic, strong) NSArray *userTags;

/**动态-视频地址*/
@property (nonatomic, copy) NSString *videoAddress;
/**动态-图片地址*/
@property (nonatomic, copy) NSArray *pictures;
/**动态-文字*/
@property (nonatomic, copy) NSArray *words;

/**动态-视频封面*/
@property (nonatomic, copy) NSString *coverImage;
/**动态-内容*/
@property (nonatomic, copy) NSArray *newsContent;

/**动态-是否已收藏*/
@property (nonatomic, assign) BOOL collected;
/**动态-是否已点赞*/
@property (nonatomic, assign) BOOL liked;
/**用户-是否已关注*/
@property (nonatomic, assign) BOOL stared;

@property (nonatomic, copy) NSString *userBloggerLevel;

@property (nonatomic, copy) NSString *type;//1资讯2动态

@property (nonatomic, copy) NSString *viewCount;//浏览量

@property (nonatomic, copy) NSString *tag;

/**详情标题高度*/
@property (nonatomic, assign) CGFloat titleHeight;

/**动态-html内容*/
@property (nonatomic, copy) NSString *htmlContent;


//原内容是否已删除（收藏、点赞用）
@property (nonatomic, assign) BOOL deleted;

//原内容是否已隐藏（收藏、点赞用）
@property (nonatomic, assign) BOOL hidden;

//state 状态（1审核中、2已发布、3已封禁）(创作、动态用)（查看他人的都是2）（查看自己的动态有2、3）（查看自己的创作有1、2、3）
@property (nonatomic, assign) NSInteger state;
/**是否注销（0，1 正常）其他为注销*/
@property (nonatomic, assign) NSInteger cancel;

@property (nonatomic, copy) NSString *sortType;///* 0点赞，1收藏，2创作，3动态 */
@end


@interface QJCampsiteDetailContentModel : NSObject
/**内容*/
@property (nonatomic, copy) NSString *content;

/**类型*/
@property (nonatomic, copy) NSString *type;
@end
