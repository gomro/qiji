//
//  QJCommunityArticleCheckModel.h
//  QJBox
//
//  Created by wxy on 2022/8/12.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJCommunityArticleCheckModel : NSObject

//2审核通过，3审核未通过
@property (nonatomic, copy) NSString *checkState;

@property (nonatomic, copy) NSString *idStr;


@end

NS_ASSUME_NONNULL_END
