//
//  QJCampsiteDetailModel.m
//  QJBox
//
//  Created by Sun on 2022/7/14.
//

#import "QJCampsiteDetailModel.h"

@implementation QJCampsiteDetailModel
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"newsId" : @[@"newId",@"sourceId"],
             @"newsTitle":@[@"newsTitle",@"title"],
             @"userTags":@[@"userTags"],
             @"newsAnnounceTime":@[@"timeShow",@"newsAnnounceTime"],
             @"newsLikeCount":@[@"likeCount",@"newsLikeCount"],
             @"newsCollectCount":@[@"newsCollectCount",@"collectCount"],
             @"newsCommentCount":@[@"newsCommentCount",@"commentCount"],
    };
}

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"newsContent" : [QJCampsiteDetailContentModel class],
             @"userTags" : [NSString class],
             @"pictures" : [NSString class],
//             @"shareInfo" : @"QJDetailShareModel",
    };
}
@end


@implementation QJCampsiteDetailContentModel

@end
