//
//  QJCommentTopModel.h
//  QJBox
//
//  Created by Sun on 2022/7/11.
//

#import <Foundation/Foundation.h>
#import "QJCommentUser.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJCommentTopModel : NSObject
/** 视频的id */
@property (nonatomic , copy) NSString *mediabase_id;

/** 话题id */
@property (nonatomic , copy) NSString * topicId;

/** 点赞数 */
@property (nonatomic , assign) long long thumbNums;

/** 点赞数string //辅助属性// */
@property (nonatomic , copy , readonly) NSString * thumbNumsString;

/** 是否点赞  0 没有点赞 1 是点赞*/
@property (nonatomic , assign , getter = isThumb) BOOL thumb;


/** 话题内容 */
@property (nonatomic, copy) NSString *text;



/** 用户模型 */
@property (nonatomic , strong) QJCommentUser *user;

/** 评论总数 */
@property (nonatomic , assign) NSInteger commentsCount;

/** 富文本 */
- (NSAttributedString *)attributedText;

@property (nonatomic, assign) BOOL showAll;

/**评论-评论id*/
@property (nonatomic, copy) NSString *commentId;
/**评论-评论内容*/
@property (nonatomic, copy) NSString *content;
/**评论用户-用户头像*/
@property (nonatomic, copy) NSString *coverImage;
/**评论用户-用户等级*/
@property (nonatomic, copy) NSString *level;
/**评论用户-博主等级*/
@property (nonatomic, assign) NSInteger bloggerLevel;

/**评论-展示时间*/
@property (nonatomic, copy) NSString *timeShow;
/**评论用户-用户id*/
@property (nonatomic, copy) NSString *userId;
/**评论用户-用户名*/
@property (nonatomic, copy) NSString *userName;
/**评论用户-用户标签*/
@property (nonatomic, copy) NSArray *tags;
/** 所有子评论  */
@property (nonatomic , strong) NSMutableArray *comments;
/**子评论*/
@property (nonatomic, copy) NSDictionary *subComment;
/**子评论总数*/
@property (nonatomic, assign) NSInteger total;

/**第几页*/
@property (nonatomic , assign) NSInteger page;

@property (nonatomic, assign) BOOL isExpand;

@end


NS_ASSUME_NONNULL_END
