//
//  QJDraftModel.m
//  QJBox
//
//  Created by Sun on 2022/7/22.
//

#import "QJDraftModel.h"

@implementation QJDraftModel
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"draftId" : @"id"};
}
@end
