//
//  QJNewsTagModel.h
//  QJBox
//
//  Created by macm on 2022/7/28.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJNewsTagModel : NSObject<NSCoding>
@property (nonatomic, copy) NSString *code;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) UIColor *color;
@property (nonatomic, copy) UIColor *backgroundColor;
@end

NS_ASSUME_NONNULL_END
