//
//  QJCommentReplyFrame.m
//  QJBox
//
//  Created by Sun on 2022/7/11.
//

#import "QJCommentReplyFrame.h"
@interface QJCommentReplyFrame ()

/** 内容尺寸 */
@property (nonatomic , assign) CGRect textFrame;
/** cell高度 */
@property (nonatomic , assign) CGFloat cellHeight;

@end
@implementation QJCommentReplyFrame

- (void)setComment:(QJCommentReplyModel *)comment {
    _comment = comment;
    // 文本内容
    CGFloat textX = 11;
    CGFloat textY = 7;
    CGSize  textLimitSize = CGSizeMake(self.maxW - 2 * textX, MAXFLOAT);
    CGFloat textH = [YYTextLayout layoutWithContainerSize:textLimitSize text:comment.attributedText].textBoundingSize.height;
    self.textFrame = (CGRect){{textX , textY} , {textLimitSize.width , textH}};
    self.cellHeight = CGRectGetMaxY(self.textFrame) + 7;
}

@end

