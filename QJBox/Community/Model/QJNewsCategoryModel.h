//
//  QJNewsCategoryModel.h
//  QJBox
//
//  Created by Sun on 2022/7/28.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJNewsCategoryModel : NSObject
/**分类code*/
@property (nonatomic, copy) NSString *code;

/**分类名称，如攻略、新闻    */
@property (nonatomic, copy) NSString *name;
@end

NS_ASSUME_NONNULL_END
