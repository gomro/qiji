//
//  QJReportModel.h
//  QJBox
//
//  Created by Sun on 2022/7/22.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJReportModel : NSObject
/**举报id*/
@property (nonatomic, copy) NSString *reportId;

/**举报名称*/
@property (nonatomic, copy) NSString *descriptionStr;

/**是否选择*/
@property (nonatomic, assign) BOOL isSelected;

@end

NS_ASSUME_NONNULL_END
