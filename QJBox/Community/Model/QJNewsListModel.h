//
//  QJNewsListModel.h
//  QJBox
//
//  Created by Sun on 2022/7/28.
//

#import <Foundation/Foundation.h>
#import "QJDetailShareModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJNewsListModel : NSObject

/**资讯-分享*/
@property (nonatomic, strong) QJDetailShareModel *shareInfo;

/**资讯-封面图*/
@property (nonatomic, copy) NSString *infoCoverImage;

/**资讯-id*/
@property (nonatomic, copy) NSString *infoId;

/**资讯-点赞数*/
@property (nonatomic, assign) NSInteger infoLikeCount;

/**资讯-标题*/
@property (nonatomic, copy) NSString *infoTitle;

/**资讯-标签*/
@property (nonatomic, copy) NSString *infoTag;

/**资讯-浏览数*/
@property (nonatomic, assign) NSInteger infoVisitCount;

/**资讯-是否已点赞*/
@property (nonatomic, assign) BOOL liked;

/**用户-是否已关注*/
@property (nonatomic, assign) BOOL stared;

/**资讯-是否已收藏*/
@property (nonatomic, assign) BOOL collected;

/** 资讯-展示时间*/
@property (nonatomic, copy) NSString *timeShow;

/** 用户-博主等级*/
@property (nonatomic, assign) NSInteger userBloggerLevel;

/**用户-id*/
@property (nonatomic, copy) NSString *userId;

/**用户-昵称*/
@property (nonatomic, copy) NSString *userNickName;

/**用户-头像*/
@property (nonatomic, copy) NSString *userCoverImage;

/**资讯-视频地址*/
@property (nonatomic, copy) NSString *videoAddress;

/**资讯-发布时间*/
@property (nonatomic, copy) NSString *infoAnnounceTime;

/**资讯-收藏数*/
@property (nonatomic, assign) NSInteger infoCollectCount;

/**资讯-评论数*/
@property (nonatomic, assign) NSInteger infoCommentCount;

/**资讯-html内容*/
@property (nonatomic, copy) NSString *infoHtmlContent;

/**资讯-浏览数(详情)*/
@property (nonatomic, assign) NSInteger infoViewCount;

/**详情标题高度*/
@property (nonatomic, assign) CGFloat titleHeight;

/**是否注销（0，1 正常）其他为注销*/
@property (nonatomic, assign) NSInteger cancel;

/**资讯-是否已审核完成*/
@property (nonatomic, assign) BOOL checked;

//state 状态（1审核中、2已发布、3已封禁）(创作、动态用)（查看他人的都是2）（查看自己的动态有2、3）（查看自己的创作有1、2、3）
@property (nonatomic, assign) NSInteger state;

@property (nonatomic, copy) NSString *gameId;   //资讯-游戏id

@property (nonatomic, assign)  BOOL hidden;//是否隐藏
@end

NS_ASSUME_NONNULL_END
