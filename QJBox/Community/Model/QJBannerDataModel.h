//
//  QJBannerDataModel.h
//  QJBox
//
//  Created by Sun on 2022/7/28.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJBannerDataModel : NSObject
/**图片地址*/
@property (nonatomic, copy) NSString *imgUrl;

/**背景图片地址*/
@property (nonatomic, copy) NSString *backImgUrl;

/**类型--banner（0：游戏，1：资讯，2：营地/动态，3：短视频*/
@property (nonatomic, copy) NSString *innerType;

/**跳转类型（0不跳转、1跳转内部、2跳转外部）*/
@property (nonatomic, copy) NSString *jumpType;

/**原数据id，针对指定inner_type*/
@property (nonatomic, copy) NSString *sourceId;

/**标题*/
@property (nonatomic, copy) NSString *title;

@end

NS_ASSUME_NONNULL_END
