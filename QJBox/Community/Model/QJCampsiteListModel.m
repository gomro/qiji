//
//  QJCampsiteListModel.m
//  QJBox
//
//  Created by Sun on 2022/7/13.
//

#import "QJCampsiteListModel.h"

@implementation QJCampsiteListModel

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"campsiteID" : @"id"};
}
@end
