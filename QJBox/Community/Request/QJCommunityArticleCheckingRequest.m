//
//  QJCommunityArticleCheckingRequest.m
//  QJBox
//
//  Created by wxy on 2022/8/12.
//

#import "QJCommunityArticleCheckingRequest.h"

@implementation QJCommunityArticleCheckingRequest
- (NSString *)requestUrl {
    
    return @"/info/listChecking";
    
}

 

- (YTKRequestMethod)requestMethod {
    
    return YTKRequestMethodGET;
}


- (YTKRequestSerializerType)requestSerializerType {
    
    return YTKRequestSerializerTypeHTTP;
}

@end
