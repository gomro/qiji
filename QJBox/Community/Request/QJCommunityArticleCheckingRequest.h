//
//  QJCommunityArticleCheckingRequest.h
//  QJBox
//
//  Created by wxy on 2022/8/12.
//

#import "QJBaseRequest.h"

NS_ASSUME_NONNULL_BEGIN


/// 文章审核状态查询请求
@interface QJCommunityArticleCheckingRequest : QJBaseRequest




@end

NS_ASSUME_NONNULL_END
