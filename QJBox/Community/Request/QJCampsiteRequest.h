//
//  QJCampsiteRequest.h
//  QJBox
//
//  Created by Sun on 2022/7/12.
//

#import "QJBaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJCampsiteRequest : QJBaseRequest
/**获取营地分类*/
- (void)netWorkGetCampCategory;

/**用户修改个人关注营地*/
- (void)netWorkPostCampSet:(NSArray *)dataArr;

/**获取用户营地*/
- (void)netWorkGetMineCamp;

/**获取营地列表*/
- (void)netWorkGetPageCampByCategory:(NSString *)categoryId page:(NSString *)page;

/**搜索营地*/
- (void)netWorkGetCampByKeywords:(NSString *)keyword;

/**获取营地介绍*/
- (void)netWorkGetCampDescription:(NSString *)campsiteID;

/**获取营地动态*/
- (void)netWorkGetCampNews:(NSString *)campsiteID page:(NSString *)page sort:(NSString *)sort;

/**获取动态详情*/
- (void)netWorkGetCampDetail:(NSString *)campsiteID isFromCamp:(BOOL)isFromCamp;

/**动态收藏*/
- (void)netWorkPutCampSave:(NSString *)campsiteID type:(NSString *)type action:(NSString *)action;

/**点赞
 type    0-游戏，1-资讯，2-动态
 action    行为: 点赞=1, 取消点赞=0*/
- (void)netWorkPutCampLike:(NSString *)campsiteID type:(NSString *)type action:(NSString *)action;

/**
 * @author: zjr
 * @date: 2022-8-5
 * @desc: 个人空间内容
 *  sortType:0点赞，1收藏，2创作，3动态
 *  sort:接口获取，默认为空字符串
 *  uid:查询他人用户需传
 */
- (void)netWorkGetGuideContent:(NSString *)sortType page:(NSString *)page sort:(NSString *)sort uid:(NSString *)uid;

/**oss自签名模式*/
- (void)netWorkGetSign:(NSString *)content;

/**保存（草稿-发布-编辑）*/
- (void)netWorkPostNews:(NSArray *)content action:(NSString *)action campId:(NSString *)campId title:(NSString *)title videoAddress:(NSString *)videoAddress html:(NSString *)htmlString coverImage:(NSString *)coverImage isVertical:(BOOL)isVertical;


/**获取根评论
 page    页码    query    true
 integer(int32)
 size    每页数量    query    true
 integer(int32)
 type    0-游戏，1-资讯，2-动态
 desc    是否发布时间倒序展示，不传默认是    query    false
 */
- (void)netWorkGetCommentRoot:(NSString *)campsiteID page:(NSString *)page type:(NSString *)type desc:(BOOL)desc;

/**获取子评论*/
- (void)netWorkGetCommentSub:(NSString *)commentID page:(NSString *)page newsId:(NSString *)newsId;


/**content    评论内容
 replyCommentId    回复评论id
 sourceId    类型对应id
 type    0-游戏，1-攻略，2-动态,可用值:CAMP,GAME,STRATEGY,USER,VIDEO
 发布评论
 */
- (void)netWorkPostComment:(NSString *)content replyCommentId:(NSString *)replyCommentId sourceId:(NSString *)sourceId type:(NSString *)type;

/**删除评论  newsId营地ID，可为空*/
- (void)netWorkDeleteComment:(NSString *)commentID newsId:(NSString *)newsId;

/**删除动态*/
- (void)netWorkDeleteCampNews:(NSString *)campsiteID;

/**关注、取消关注 type 1关注，0取消关注    */
- (void)netWorkPostAttentionUserid:(NSString *)userId type:(NSString *)type newsId:(NSString *)newsId;


/**获取举报类型 0-游戏，1-攻略，2-动态*/
- (void)netWorkGetReportReasons:(NSString *)type;

/**举报
 complainReasonId    举报类型id
 description    举报描述
 sourceId    原记录id，如动态id
 type    0-游戏，1-攻略，2-动态*/
- (void)netWorkPostReportReasons:(NSString *)type sourceId:(NSString *)sourceId description:(NSString *)description complainReasonId:(NSString *)complainReasonId;


/**
 草稿列表*/
- (void)netWorkGetDraftsList:(NSString *)page;

/**
 草稿详情*/
- (void)netWorkGetDraftsDetail:(NSString *)draftId;


#pragma mark - 资讯
/**查询banner
 APP_HOME:app首页、GAME:游戏首页、INFO:资讯首页,可用值:APP_HOME,GAME,INFO*/
- (void)netWorkGetBannerData:(NSString *)location;


/**查询资讯分类*/
- (void)netWorkGetInfoCategory;

/**资讯列表(资讯首页查询)
 page    页码
 size    每页数量
 ids    标签数组
 categoryCode    资讯分类，不传查询综合
 sort    排序(仅2种)：发布时间倒序TD、热门倒序HD,可用值:HD,TD
 */
- (void)netWorkGetInfoListPage:(NSString *)page ids:(NSArray *)ids sort:(NSString *)sort categoryCode:(NSString *)categoryCode;

/**资讯列表（我的关注查询*/
- (void)netWorkGetInfoListInStarPage:(NSString *)page;

/**删除资讯*/
- (void)netWorkDeleteInfoWithId:(NSString *)ids;

/**资讯详情*/
- (void)netWorkGetInfoDetailWithId:(NSString *)ids;

/**资讯列表(搜索页查询)
 * keyWordType    关键字类型，标题还是游戏,可用值:GAME,TITLE
 * keyword    关键字
 * categoryCode    资讯分类，不传查询综合
 * sort    排序(仅2种)：发布时间倒序TD、热门倒序HD,可用值:HD,TD
 */
- (void)netWorkGetInfoListInSearchKeywords:(NSString *)keyword keywordType:(NSString *)keywordType categoryCode:(NSString *)categoryCode sort:(NSString *)sort page:(NSInteger)page;

/**获取热搜词
 * type    0-游戏，1-资讯，2-动态
 */
- (void)netWorkGetHotWordsSearchType:(NSString *)type;

/**查询标签
 * keyword    关键字
 */
- (void)netWorkGetInfoTagKeyword:(NSString *)keyword;


@end

NS_ASSUME_NONNULL_END
