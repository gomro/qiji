//
//  QJCommunityNewsRequest.m
//  QJBox
//
//  Created by wxy on 2022/8/4.
//

#import "QJCommunityNewsRequest.h"

@implementation QJCommunityNewsRequest

- (NSString *)requestUrl {
    
    return @"/info/post";
    
}

 

- (YTKRequestMethod)requestMethod {
    
    return YTKRequestMethodPOST;
}


- (YTKRequestSerializerType)requestSerializerType {
    
    return YTKRequestSerializerTypeJSON;
}


@end
