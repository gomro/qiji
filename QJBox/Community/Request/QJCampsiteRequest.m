//
//  QJCampsiteRequest.m
//  QJBox
//
//  Created by Sun on 2022/7/12.
//

#import "QJCampsiteRequest.h"

@implementation QJCampsiteRequest
- (void)netWorkGetCampCategory {
    self.dic = @{};
    self.urlStr = @"/camp/campCategory";
    [self start];
}

- (void)netWorkPostCampSet:(NSArray *)dataArr {
    self.dic = @{
        @"ids":dataArr
    };
    self.requestType = YTKRequestMethodPOST;
    self.serializerType = YTKRequestSerializerTypeJSON;
    self.urlStr = @"/camp/campSet";
    [self start];
}

- (void)netWorkGetMineCamp {
    self.dic = @{};
    self.urlStr = @"/camp/mine";
    [self start];
}

- (void)netWorkGetPageCampByCategory:(NSString *)categoryId page:(NSString *)page {
    self.dic = @{
        @"categoryId":categoryId,
        @"page":page,
        @"size":@"20"
    };
    self.urlStr = @"/camp/pageCampByCategory";
    [self start];
}

- (void)netWorkGetCampByKeywords:(NSString *)keyword {
    self.dic = @{
        @"keyword":keyword
    };
    self.urlStr = @"/camp/pageCampByKeywords";
    [self start];
}

- (void)netWorkGetCampDescription:(NSString *)campsiteID {
    self.dic = @{};
    self.urlStr = [NSString stringWithFormat:@"/camp/%@/description", campsiteID];
    [self start];
}

- (void)netWorkGetCampNews:(NSString *)campsiteID page:(NSString *)page sort:(NSString *)sort {
    self.dic = @{
        @"page":page,
        @"size":@"10",
        @"sort":sort
    };
    self.urlStr = [NSString stringWithFormat:@"/news/%@/campNews", campsiteID];
    [self start];
}

/**
 * @author: zjr
 * @date: 2022-8-5
 * @desc: 个人空间内容
 */
- (void)netWorkGetGuideContent:(NSString *)sortType page:(NSString *)page sort:(NSString *)sort uid:(NSString *)uid{
    NSMutableDictionary *muDic = [NSMutableDictionary dictionary];
    [muDic setValue:page forKey:@"page"];
    [muDic setValue:@"10" forKey:@"size"];
    [muDic setValue:sortType forKey:@"type"];
    if (![kCheckStringNil(uid) isEqualToString:@""]) {
        [muDic setValue:kCheckStringNil(uid) forKey:@"uid"];
    }
    if (![kCheckStringNil(sort) isEqualToString:@""]) {
        [muDic setValue:kCheckStringNil(sort) forKey:@"innerType"];
    }
    self.dic = muDic.copy;
    self.urlStr = @"/user/zone/userContent";
    [self start];
}

- (void)netWorkGetCampDetail:(NSString *)campsiteID  isFromCamp:(BOOL)isFromCamp {
    self.dic = @{
        @"fromCamp":@(isFromCamp),
    };
    self.urlStr = [NSString stringWithFormat:@"/news/%@/detail", campsiteID];
    [self start];
}

- (void)netWorkPutCampSave:(NSString *)campsiteID type:(NSString *)type action:(NSString *)action{
    self.dic = @{};
    self.requestType = YTKRequestMethodPUT;
    self.urlStr = [NSString stringWithFormat:@"/collect/%@/%@/%@",type, campsiteID, action];
    [self start];
}

- (void)netWorkPutCampLike:(NSString *)campsiteID type:(NSString *)type action:(NSString *)action {
    self.dic = @{};
    self.requestType = YTKRequestMethodPUT;
    self.urlStr = [NSString stringWithFormat:@"/like/%@/%@/%@",type, campsiteID, action];
    [self start];
}

- (void)netWorkGetSign:(NSString *)content {
    self.dic = @{
        @"content":content
    };
    self.urlStr = @"/sign";
    self.serializerType = YTKRequestSerializerTypeJSON;
    [self start];
}

- (void)netWorkPostNews:(NSArray *)content action:(NSString *)action campId:(NSString *)campId title:(NSString *)title videoAddress:(NSString *)videoAddress html:(NSString *)htmlString coverImage:(NSString *)coverImage  isVertical:(BOOL)isVertical{
    self.dic = @{
        @"content":content?:@"",
        @"action":action?:@"",
        @"campId":campId?:@"",
        @"title":title?:@"",
        @"videoAddress":videoAddress?:@"",
        @"htmlContent":htmlString?:@"",
        @"coverImage":coverImage?:@"",
        @"vertical":@(isVertical),
    };
    self.requestType = YTKRequestMethodPOST;
    self.serializerType = YTKRequestSerializerTypeJSON;
    self.urlStr = @"/news/post";
    [self start];
}

- (void)netWorkGetCommentRoot:(NSString *)campsiteID page:(NSString *)page type:(NSString *)type desc:(BOOL)desc {
    self.dic = @{
        @"page":page,
        @"type":type,
        @"desc":@(desc),
        @"size":@"20",
    };
    self.urlStr = [NSString stringWithFormat:@"/comment/%@/root", campsiteID];
    [self start];
}

- (void)netWorkGetCommentSub:(NSString *)commentID page:(NSString *)page newsId:(NSString *)newsId {
    if([kCheckStringNil(newsId) isEqualToString:@""]) {
        self.dic = @{
            @"page":page,
            @"size":@"10",
        };
    } else {
        self.dic = @{
            @"page":page,
            @"size":@"10",
            @"newsId":newsId
        };
    }

    self.urlStr = [NSString stringWithFormat:@"/comment/%@/sub", commentID];
    [self start];
}

- (void)netWorkPostComment:(NSString *)content replyCommentId:(NSString *)replyCommentId sourceId:(NSString *)sourceId type:(NSString *)type {
    if ([kCheckStringNil(replyCommentId) isEqualToString:@""]) {
        self.dic = @{
            @"content":content,
            @"sourceId":sourceId,
            @"type":type,
        };
    } else {
        self.dic = @{
            @"content":content,
            @"replyCommentId":replyCommentId,
            @"sourceId":sourceId,
            @"type":type,
        };
    }
 
    self.requestType = YTKRequestMethodPOST;
    self.serializerType = YTKRequestSerializerTypeJSON;
    self.urlStr = @"/comment";
    [self start];
}

- (void)netWorkDeleteComment:(NSString *)commentID newsId:(NSString *)newsId {
    if([kCheckStringNil(newsId) isEqualToString:@""]) {
        self.dic = @{};
    } else {
        self.dic = @{
            @"newsId":newsId
        };
    }
    self.requestType = YTKRequestMethodDELETE;
    self.urlStr = [NSString stringWithFormat:@"/comment/%@", commentID];
    [self start];
}

- (void)netWorkDeleteCampNews:(NSString *)campsiteID {
    self.dic = @{};
    self.requestType = YTKRequestMethodDELETE;
    self.urlStr = [NSString stringWithFormat:@"/news/%@/post", campsiteID];
    [self start];
}

- (void)netWorkPostAttentionUserid:(NSString *)userId type:(NSString *)type newsId:(NSString *)newsId {
    if([kCheckStringNil(newsId) isEqualToString:@""]) {
        self.dic = @{};
    } else {
        self.dic = @{
            @"newsId":newsId
        };
    }
    self.requestType = YTKRequestMethodPOST;
    self.urlStr = [NSString stringWithFormat:@"/user/%@/star/%@", userId, type];
    [self start];
}

- (void)netWorkGetReportReasons:(NSString *)type {
    self.dic = @{};
    self.urlStr = [NSString stringWithFormat:@"/complain/%@/reasons", type];
    [self start];
}

- (void)netWorkPostReportReasons:(NSString *)type sourceId:(NSString *)sourceId description:(NSString *)description complainReasonId:(NSString *)complainReasonId {
    self.dic = @{
        @"sourceId":sourceId,
        @"description":description,
        @"complainReasonId":complainReasonId,
        @"type":type,
    };
    self.requestType = YTKRequestMethodPOST;
    self.serializerType = YTKRequestSerializerTypeJSON;
    self.urlStr = [NSString stringWithFormat:@"/complain/post"];
    [self start];
}

- (void)netWorkGetDraftsList:(NSString *)page {
    self.dic = @{
        @"page":page,
        @"size":@"10",
    };
    self.urlStr = @"/news/drafts";
    [self start];
}


- (void)netWorkGetDraftsDetail:(NSString *)draftId {
    self.dic = @{};
    self.urlStr = [NSString stringWithFormat:@"/news/%@/draft", draftId];
    [self start];
}

- (void)netWorkGetBannerData:(NSString *)location {
    self.dic = @{
        @"location":location
    };
    self.urlStr =  @"/banner";
    [self start];
}

- (void)netWorkGetInfoCategory {
    self.dic = @{};
    self.urlStr =  @"/info/category";
    [self start];
}

- (void)netWorkGetInfoListPage:(NSString *)page ids:(NSArray *)ids sort:(NSString *)sort categoryCode:(NSString *)categoryCode {
    NSString *tempString = @"";
    if (ids.count) {
        tempString = [ids componentsJoinedByString:@","];
    }
    if ([kCheckStringNil(categoryCode) isEqualToString:@""]) {
        self.dic = @{
            @"page":page,
            @"size":@"20",
            @"ids":tempString,
            @"sort":sort
        };
    } else {
        self.dic = @{
            @"page":page,
            @"size":@"20",
            @"ids":tempString,
            @"sort":sort,
            @"categoryCode":categoryCode
        };
    }
    self.urlStr =  @"/info/listInRecommend";
    self.serializerType = YTKRequestSerializerTypeJSON;
    [self start];
}

- (void)netWorkGetInfoListInStarPage:(NSString *)page {
    self.dic = @{
        @"page":page,
        @"size":@"20",
    };
    self.urlStr =  @"/info/listInStar";
    [self start];
}

- (void)netWorkDeleteInfoWithId:(NSString *)ids {
    self.dic = @{
        @"ids":ids,
    };
    self.urlStr =  @"/info/delete";
    self.requestType = YTKRequestMethodDELETE;
    [self start];
}

- (void)netWorkGetInfoDetailWithId:(NSString *)ids {
    self.dic = @{};
    self.urlStr = [NSString stringWithFormat:@"/info/%@/detail", ids];
    [self start];
}

/**资讯列表(搜索页查询)
 * keyWordType    关键字类型，标题还是游戏,可用值:GAME,TITLE
 * keyword    关键字
 * categoryCode    资讯分类，不传查询综合
 * sort    排序(仅2种)：发布时间倒序TD、热门倒序HD,可用值:HD,TD
 */
- (void)netWorkGetInfoListInSearchKeywords:(NSString *)keyword keywordType:(NSString *)keywordType categoryCode:(NSString *)categoryCode sort:(NSString *)sort page:(NSInteger)page {
    
    NSString *type = @"";
    if ([keywordType isEqualToString:@"搜标题"]) {
        type = @"TITLE";
    } else {
        type = @"GAME";
    }
    
    self.dic = @{
        @"page":@(page),
        @"size":@"10",
        @"keyWordType":type,
        @"keyword":keyword,
        @"categoryCode":categoryCode,
        @"sort":sort,
    };
    self.urlStr = @"/info/listInSearch";

    [self start];
}

/**获取热搜词
 * type    0-游戏，1-资讯，2-动态
 */
- (void)netWorkGetHotWordsSearchType:(NSString *)type {
    self.dic = @{
        @"type":type,
    };
    self.urlStr = @"/hotWords/search";

    [self start];
}

/**查询标签
 * keyword    关键字
 */
- (void)netWorkGetInfoTagKeyword:(NSString *)keyword {
    self.dic = @{
        @"keyword":keyword,
    };
    self.urlStr = @"/info/tag";

    [self start];
}

@end
