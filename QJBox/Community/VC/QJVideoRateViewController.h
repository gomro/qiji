//
//  QJVideoRateViewController.h
//  QJBox
//
//  Created by Sun on 2022/8/1.
//

#import <UIKit/UIKit.h>

#import <SJVideoPlayer/SJControlLayerDefines.h>

NS_ASSUME_NONNULL_BEGIN
@protocol QJVideoRateViewControllerDelegate <NSObject>
///
/// 点击空白区域的回调
///
- (void)tappedBlankAreaOnTheControlLayer:(id<SJControlLayer>)controlLayer;
- (void)tappedButtonRate:(NSString *)rate;

@end
@interface QJVideoRateViewController : UIViewController<SJControlLayer>
@property (nonatomic, weak, nullable) id<QJVideoRateViewControllerDelegate> delegate;
/**是否是竖屏样式*/
@property (nonatomic, assign) BOOL isVertical;
@end

NS_ASSUME_NONNULL_END
