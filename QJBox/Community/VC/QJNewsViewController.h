//
//  QJNewsViewController.h
//  QJBox
//
//  Created by Sun on 2022/7/26.
//

#import "QJListViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJNewsViewController : QJListViewController

- (void)changeBackViewImage;
@end

NS_ASSUME_NONNULL_END
