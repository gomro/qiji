//
//  QJNewsPageViewController.m
//  QJBox
//
//  Created by Sun on 2022/7/27.
//

#import "QJNewsPageViewController.h"
#import "JXCategoryTitleView.h"
#import "QJImageButton.h"
#import "QJCampsiteListViewController.h"
#import "QJNewsHotTagView.h"
#import "QJNewsListViewController.h"

@interface QJNewsPageViewController ()
@property (nonatomic, strong) JXCategoryTitleView *myCategoryView;
@property (nonatomic, strong) NSArray *childVCs;

/**筛选*/
@property (nonatomic, strong) QJImageButton *filterBt;
/**分割线*/
@property (nonatomic, strong) UIView *lineView;
@property (nonatomic, copy) NSArray *tagArray; //存放标签id数组
@end

@implementation QJNewsPageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor clearColor];
    self.tagArray = @[];
    self.myCategoryView.titleColor = UIColorHex(16191C);
    self.myCategoryView.titleSelectedColor = UIColorHex(16191C);
    self.myCategoryView.titleFont = MYFONTALL(FONT_REGULAR, 16);
    self.myCategoryView.titleSelectedFont = FontSemibold(18);
    self.myCategoryView.titleLabelStrokeWidthEnabled = NO;
    self.myCategoryView.selectedAnimationEnabled = YES;
    
    self.myCategoryView.averageCellSpacingEnabled = NO;
    self.myCategoryView.contentEdgeInsetLeft = 20;
    self.myCategoryView.cellSpacing = 24;
    [self.myCategoryView reloadData];
    
    [self.view addSubview:self.filterBt];
    [self.filterBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.myCategoryView);
        make.right.equalTo(self.view.mas_right).offset(-16);
        make.height.mas_equalTo(25*kWScale);
    }];
    
    [self.view addSubview:self.lineView];
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.top.equalTo(self.myCategoryView.mas_bottom).offset(5);
        make.height.mas_equalTo(1);
    }];
    
    self.backImage.backgroundColor = [UIColor whiteColor];
    self.backImage.frame = CGRectMake(0, 0, kScreenWidth, 100*kWScale);
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect: self.backImage.bounds byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(16, 16)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.backImage.bounds;
    maskLayer.path = maskPath.CGPath;
    self.backImage.layer.mask = maskLayer;
    
    
}

- (void)configTitles:(NSArray *)titles childVCs:(NSArray *)childVCs {
    self.titles = titles;
    self.childVCs = childVCs;
    self.myCategoryView.titles = self.titles;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.myCategoryView reloadData];
    });
}

- (QJImageButton *)filterBt {
    if (!_filterBt) {
        _filterBt = [QJImageButton new];
        _filterBt.titleString = @"筛选";
        _filterBt.titleLb.font = MYFONTALL(FONT_REGULAR, 12);
        _filterBt.titleLb.textColor = UIColorHex(474849);
        _filterBt.iconString = @"sortFilterIcon";
        _filterBt.space = 4;
        _filterBt.size = CGSizeMake(16*kWScale, 16*kWScale);
        _filterBt.style = ImageButtonStyleRight;
        [_filterBt addTarget:self action:@selector(filterChoose:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _filterBt;
}

- (void)selectItemView:(NSInteger)index {
    self.myCategoryView.defaultSelectedIndex = index;
    [self.myCategoryView reloadData];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    self.myCategoryView.frame = CGRectMake(0, 20, kScreenWidth - 100*kWScale, 35*kWScale);
    self.listContainerView.frame = CGRectMake(0, 63*kWScale, kScreenWidth, kScreenHeight - Bottom_iPhoneX_SPACE - NavigationBar_Bottom_Y - 63*kWScale - 49);
}

- (JXCategoryTitleView *)myCategoryView {
    return (JXCategoryTitleView *)self.categoryView;
}

- (JXCategoryBaseView *)preferredCategoryView {
    return [[JXCategoryTitleView alloc] init];
}

- (CGFloat)preferredCategoryViewHeight {
    return 55*kWScale;
}

- (UIView *)lineView {
    if (!_lineView) {
        _lineView = [UIView new];
        _lineView.backgroundColor = UIColorHex(EBEDF0);
    }
    return _lineView;
}

#pragma mark - JXCategoryListContainerViewDelegate
- (id<JXCategoryListContentViewDelegate>)listContainerView:(JXCategoryListContainerView *)listContainerView initListForIndex:(NSInteger)index {
    QJNewsListViewController *list = self.childVCs[index];
    list.filterIds = self.tagArray;
    return list;
}

- (void)categoryView:(JXCategoryBaseView *)categoryView didSelectedItemAtIndex:(NSInteger)index {
    if (index == self.childVCs.count - 1) {
        [self changeFilterBtStyleWithFollow:YES];
    } else {
        [self changeFilterBtStyleWithFollow:NO];
    }
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    if ([self.scrollDelegate respondsToSelector:@selector(pageScrollViewWillBeginScroll)]) {
        [self.scrollDelegate pageScrollViewWillBeginScroll];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if ([self.scrollDelegate respondsToSelector:@selector(pageScrollViewDidEndedScroll)]) {
        [self.scrollDelegate pageScrollViewDidEndedScroll];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if ([self.scrollDelegate respondsToSelector:@selector(pageScrollViewDidEndedScroll)]) {
        [self.scrollDelegate pageScrollViewDidEndedScroll];
    }
}

- (void)filterChoose:(UIButton *)sender {

    QJNewsHotTagView *vc = [[QJNewsHotTagView alloc] initWithTitles:self.tags selectTag:self.tagArray];
    @weakify(self);
    vc.tagViewClickEvent = ^(NSArray * _Nonnull tagArray) {
        @strongify(self);
        self.tagArray = tagArray;
        [self.categoryView reloadData];
        [self changeFilterBtStyleWithFollow:NO];
    };
    [vc show];
}

- (void)changeFilterBtStyleWithFollow:(BOOL)isFollow {
    if (isFollow) {
        self.filterBt.titleLb.textColor = UIColorHex(C8CACC);
        self.filterBt.iconString = @"sortFilterGray";
        self.filterBt.userInteractionEnabled = NO;
    } else {
        self.filterBt.userInteractionEnabled = YES;
        if (self.tagArray.count) {
            self.filterBt.titleLb.textColor = UIColorHex(FF9500);
            self.filterBt.iconString = @"sortFilterSelected";
        } else {
            _filterBt.titleLb.textColor = UIColorHex(474849);
            _filterBt.iconString = @"sortFilterIcon";
        }
    }
}
@end
