//
//  QJCampsitePageViewController.m
//  QJBox
//
//  Created by Sun on 2022/7/6.
//

#import "QJCampsitePageViewController.h"
#import "JXCategoryTitleView.h"
#import "QJImageButton.h"
#import "QJCampsiteListViewController.h"
#import "QJTypeChooseView.h"

@interface QJCampsitePageViewController ()
@property (nonatomic, strong) JXCategoryTitleView *myCategoryView;
@property (nonatomic, strong) NSArray *childVCs;

/**发布时间*/
@property (nonatomic, strong) QJImageButton *timeBt;

@property (nonatomic, strong) QJTypeChooseView *typeChoose;

@property (nonatomic, assign) QJTypeChooseViewStype chooseType;

@property (nonatomic, strong) UIView *backView;

@property (nonatomic, assign) CGRect typeChooseRect;
@end

@implementation QJCampsitePageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor clearColor];

    self.myCategoryView.titleColor = UIColorFromRGB(0x000000);
    self.myCategoryView.titleSelectedColor = UIColorFromRGB(0x000000);
    self.myCategoryView.titleFont = FontRegular(16);
    self.myCategoryView.titleSelectedFont = FontSemibold(18);
    self.myCategoryView.titleLabelStrokeWidthEnabled = NO;
    self.myCategoryView.selectedAnimationEnabled = YES;
    
    self.myCategoryView.averageCellSpacingEnabled = NO;
    self.myCategoryView.contentEdgeInsetLeft = 20;
    self.myCategoryView.cellSpacing = 24;
    [self.myCategoryView reloadData];
    
    [self.view addSubview:self.timeBt];
    [self.timeBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.myCategoryView);
        make.right.equalTo(self.view.mas_right).offset(-16);
        make.height.mas_equalTo(25*kWScale);
    }];
    
    self.backImage.backgroundColor = [UIColor whiteColor];
    self.backImage.frame = CGRectMake(0, 0, kScreenWidth, 100);
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect: self.backImage.bounds byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(16, 16)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.backImage.bounds;
    maskLayer.path = maskPath.CGPath;
    self.backImage.layer.mask = maskLayer;
    
    self.chooseType = QJTypeChooseViewStypeTime;
    MJWeakSelf
    self.typeChoose.clickButton = ^(QJTypeChooseViewStype chooseType) {
//        weakSelf.typeChoose.hidden = YES;
        [weakSelf hideTypeView];
        if (weakSelf.chooseType != chooseType) {
            weakSelf.chooseType = chooseType;
            [weakSelf changeHeaderButton];
        }
    };
 
  
    
}

- (void)changeHeaderButton {
    if (self.chooseType == QJTypeChooseViewStypeTime) {
        _timeBt.titleString = @"发布时间";
    } else {
        _timeBt.titleString = @"热门排行";
    }
    [self.myCategoryView reloadData];

}

- (void)configTitles:(NSArray *)titles childVCs:(NSArray *)childVCs {
    self.titles = titles;
    self.childVCs = childVCs;
    self.myCategoryView.titles = self.titles;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.myCategoryView reloadData];
    });
}

- (QJImageButton *)timeBt {
    if (!_timeBt) {
        _timeBt = [QJImageButton new];
        _timeBt.titleString = @"发布时间";
        _timeBt.titleLb.font = MYFONTALL(FONT_REGULAR, 12);
        _timeBt.titleLb.textColor = UIColorHex(474849);
        _timeBt.iconString = @"iconDownOne";
        _timeBt.space = 4;
        _timeBt.size = CGSizeMake(16*kWScale, 16*kWScale);
        _timeBt.style = ImageButtonStyleRight;
        [_timeBt addTarget:self action:@selector(timeChoose:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _timeBt;
}

- (QJTypeChooseView *)typeChoose {
    if (!_typeChoose) {
        _typeChoose = [QJTypeChooseView new];
        _typeChoose.backgroundColor = [UIColor whiteColor];
        _typeChoose.layer.cornerRadius = 2;
        _typeChoose.layer.shadowColor = UIColorHex(00000014).CGColor;
        _typeChoose.layer.shadowOpacity = 10;
        _typeChoose.layer.shadowRadius = 10;
        _typeChoose.layer.shadowOffset = CGSizeMake(0, 4);
    }
    return _typeChoose;
}

- (void)selectItemView:(NSInteger)index {
    self.myCategoryView.defaultSelectedIndex = index;
    [self.myCategoryView reloadData];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    self.myCategoryView.frame = CGRectMake(0, 20, kScreenWidth - 100, 35);
    self.listContainerView.frame = CGRectMake(0, 63, kScreenWidth, kScreenHeight - Bottom_iPhoneX_SPACE - NavigationBar_Bottom_Y - 109 - 63);
        
//    [self.timeBt layoutIfNeeded];
//    CGRect rect = [self.timeBt convertRect:self.timeBt.bounds toView:self.backView];
//    self.typeChooseRect = CGRectMake(kScreenWidth - 16 - 85*kWScale, rect.origin.y, 85*kWScale, 94*kWScale);
}

- (JXCategoryTitleView *)myCategoryView {
    return (JXCategoryTitleView *)self.categoryView;
}

- (JXCategoryBaseView *)preferredCategoryView {
    return [[JXCategoryTitleView alloc] init];
}

- (CGFloat)preferredCategoryViewHeight {
    return 55;
}

#pragma mark - JXCategoryListContainerViewDelegate
- (id<JXCategoryListContentViewDelegate>)listContainerView:(JXCategoryListContainerView *)listContainerView initListForIndex:(NSInteger)index {
    QJCampsiteListViewController *list = self.childVCs[index];
    list.campsiteID = self.campsiteID;
    if (self.chooseType == QJTypeChooseViewStypeTime) {
        list.sort = @"TD";
    } else {
        list.sort = @"HD";
    }
    
    return list;    
}
- (void)categoryView:(JXCategoryBaseView *)categoryView didSelectedItemAtIndex:(NSInteger)index {
    //侧滑手势处理
    self.navigationController.interactivePopGestureRecognizer.enabled = (index == 0);
    NSDictionary *dic = @{@"index":[NSNumber numberWithUnsignedLong:index]};
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    if ([self.scrollDelegate respondsToSelector:@selector(pageScrollViewWillBeginScroll)]) {
        [self.scrollDelegate pageScrollViewWillBeginScroll];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if ([self.scrollDelegate respondsToSelector:@selector(pageScrollViewDidEndedScroll)]) {
        [self.scrollDelegate pageScrollViewDidEndedScroll];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if ([self.scrollDelegate respondsToSelector:@selector(pageScrollViewDidEndedScroll)]) {
        [self.scrollDelegate pageScrollViewDidEndedScroll];
    }
}

- (void)timeChoose:(UIButton *)sender {
    _typeChoose.stype = self.chooseType;
    [self showTypeView];
}

- (void)showTypeView {
 
    
    UIWindow * window = [[[UIApplication sharedApplication] delegate] window];
    self.backView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight)];
    [window addSubview:self.backView];
    
    [self.backView addSubview:self.typeChoose];
        
    [self.timeBt layoutIfNeeded];
    CGRect rect = [self.timeBt convertRect:self.timeBt.bounds toView:self.backView];
    self.typeChooseRect = CGRectMake(kScreenWidth - 16 - 85*kWScale, rect.origin.y, 85*kWScale, 94*kWScale);

    self.typeChoose.frame = self.typeChooseRect;

    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideTypeView)];
    [self.backView addGestureRecognizer:tap];
    
}

- (void)hideTypeView {
    [self.backView removeFromSuperview];
}


@end
