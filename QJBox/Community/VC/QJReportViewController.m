//
//  QJReportViewController.m
//  QJBox
//
//  Created by Sun on 2022/7/22.
//

#import "QJReportViewController.h"
#import "QJReportViewCell.h"
#import "QJCampsiteRequest.h"
#import "QJReportModel.h"

@interface QJReportViewController ()<UICollectionViewDelegate, UICollectionViewDataSource, YYTextViewDelegate>
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) NSMutableArray *dataSource;
@property (nonatomic, strong) YYTextView *textView;
@property (nonatomic, strong) UILabel *titleNum;
@property (nonatomic, strong) UIButton *publishButton;

@property (nonatomic, copy) NSString *selectId;
@end

@implementation QJReportViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"举报";
    self.selectId = @"";
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.collectionView];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_top).offset(NavigationBar_Bottom_Y);
        make.left.bottom.right.mas_equalTo(0);
    }];
   
    [self.view addSubview:self.publishButton];
    [self.publishButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.view.mas_bottom).offset(- 69 - Bottom_iPhoneX_SPACE);
        make.left.equalTo(self.view.mas_left).offset(37);
        make.right.equalTo(self.view.mas_right).offset(-37);
        make.height.mas_equalTo(48);
    }];
    [self getReportList];
   
}


#pragma mark - Lazy Loading
- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        CGFloat width = kScreenWidth / 2.0 - 24;
        
        layout.sectionInset = UIEdgeInsetsMake(0, 24, 0, 0);
        layout.itemSize = CGSizeMake(width, 16);
        layout.minimumLineSpacing = 20;
        layout.minimumInteritemSpacing = 0;
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.backgroundColor = [UIColor clearColor];
        [_collectionView registerClass:[QJReportViewCell class] forCellWithReuseIdentifier:@"kIdentifierQJReportViewCell"];
        
        [_collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"QJReportViewCellHeaderView"];
        [_collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"QJReportViewCellFooterView"];

        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
    }
    return _collectionView;
}

- (NSMutableArray *)dataSource {
    if (!_dataSource) {
        _dataSource = [NSMutableArray arrayWithCapacity:0];
    }
    return _dataSource;
}


#pragma mark - collection
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataSource.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    QJReportViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"kIdentifierQJReportViewCell" forIndexPath:indexPath];
    MJWeakSelf
    cell.clickButton = ^(NSString * _Nonnull reportId, BOOL selected) {
        [weakSelf changeData:reportId selected:selected];
    };
    QJReportModel *model = self.dataSource[indexPath.item];
    [cell configureCellWithData:model];
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    // 区头
    if (kind == UICollectionElementKindSectionHeader) {
        UICollectionReusableView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"QJReportViewCellHeaderView" forIndexPath:indexPath];
        UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(16, 25, 200, 20)];
        title.text = @"举报类型";
        title.textColor = UIColorHex(000000);
        title.font = MYFONTALL(FONT_REGULAR, 16);
        [headerView addSubview:title];
        return headerView;
    } else if (kind == UICollectionElementKindSectionFooter) {
        UICollectionReusableView *footerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"QJReportViewCellFooterView" forIndexPath:indexPath];
        UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(16, 45, 200, 20)];
        title.text = @"举报原因";
        title.textColor = UIColorHex(000000);
        title.font = MYFONTALL(FONT_REGULAR, 16);
        [footerView addSubview:title];
        
        UIView *backView = [UIView new];
        backView.backgroundColor = UIColorHex(F5F5F5);
        [footerView addSubview:backView];
        [backView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(title.mas_bottom).offset(16);
            make.left.equalTo(footerView.mas_left).offset(16);
            make.right.equalTo(footerView.mas_right).offset(-16);
            make.height.mas_equalTo(78);
        }];
        [backView addSubview:self.titleNum];
        [backView addSubview:self.textView];
        [self.titleNum mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(backView.mas_bottom).offset(-9);
            make.right.equalTo(backView.mas_right).offset(-9);
        }];
        [self.textView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(backView.mas_top).offset(5);
            make.left.equalTo(backView.mas_left).offset(12);
            make.right.equalTo(backView.mas_right).offset(-12);
            make.bottom.equalTo(self.titleNum.mas_top);
        }];
        return footerView;
    }
    UICollectionReusableView *reusableView = nil;

    return reusableView;
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    return CGSizeMake(kScreenWidth, 70);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section {
    return CGSizeMake(kScreenWidth, 250);
}

- (void)changeData:(NSString *)reportId selected:(BOOL)selected {
    self.selectId = @"";
    for (QJReportModel *dataModel in self.dataSource) {
        dataModel.isSelected = NO;
        if ([dataModel.reportId isEqualToString:reportId]) {
            dataModel.isSelected = selected;
            if (selected) {
                self.selectId = reportId;
            }
        }
    }
    [self.collectionView reloadData];
    if ([self.selectId isEqualToString:@""]) {
        self.publishButton.enabled = NO;
        _publishButton.backgroundColor = UIColorHex(C8CACC);
    } else {
        self.publishButton.enabled = YES;
        _publishButton.backgroundColor = UIColorHex(1F2A4D);
    }
}

- (YYTextView *)textView {
    if (!_textView) {
        _textView = [YYTextView new];
        _textView.placeholderText = @"请描述您所举报对象具体违规情况";
        _textView.placeholderFont = MYFONTALL(FONT_REGULAR, 14);
        _textView.placeholderTextColor = UIColorHex(919599);
        _textView.font = MYFONTALL(FONT_REGULAR, 14);
        _textView.textColor = UIColorHex(919599);
        _textView.delegate = self;
        [_textView addDoneOnKeyboardWithTarget:self action:@selector(endEdite)];
    }
    return _textView;
}

- (UILabel *)titleNum {
    if (!_titleNum) {
        _titleNum = [UILabel new];
        _titleNum.textColor = UIColorHex(919599);
        _titleNum.font = MYFONTALL(FONT_REGULAR, 14);
        _titleNum.text = @"0/500";
    }
    return _titleNum;
}

- (UIButton *)publishButton {
    if (!_publishButton) {
        _publishButton = [UIButton new];
        _publishButton.backgroundColor = UIColorHex(C8CACC);
        [_publishButton setTitle:@"提交" forState:UIControlStateNormal];
        [_publishButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _publishButton.titleLabel.font = MYFONTALL(FONT_BOLD, 16);
        _publishButton.layer.masksToBounds = YES;
        _publishButton.layer.cornerRadius = 4;
        [_publishButton addTarget:self action:@selector(publishPost:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _publishButton;
}

- (void)getReportList {
    QJCampsiteRequest *startRequest = [[QJCampsiteRequest alloc] init];
    NSString *type = @"2";
    if (self.isNews) {
        type = @"1";
    }
    if (self.userId.length > 0) {//如果是举报个人
        type = @"4";
    }
    [startRequest netWorkGetReportReasons:type];
    [startRequest setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            NSArray *dataArr = request.responseJSONObject[@"data"];
            [self.dataSource removeAllObjects];
            for (NSDictionary *dic in dataArr) {
                QJReportModel *model = [QJReportModel modelWithDictionary:dic];
                [self.dataSource addObject:model];
            }
            [self.collectionView reloadData];
        }
    }];
}

- (void)textViewDidChange:(YYTextView *)textView {
    NSInteger length = 0;
    length = textView.text.length;
    if (length > 500) {
        textView.text = [textView.text substringToIndex:500];
        self.titleNum.text = @"500/500";
    } else if (length > 0) {
        self.titleNum.text = [NSString stringWithFormat:@"%ld/500", length];
    }
  
}

//收起键盘
- (void)endEdite {
    [self.textView resignFirstResponder];
}

#pragma mark -- 举报
- (void)publishPost:(UIButton *)sender {
    [self postReportList];
}

- (void)postReportList {
    [QJAppTool showHUDLoading];
    QJCampsiteRequest *startRequest = [[QJCampsiteRequest alloc] init];
    NSString *type = @"2";
    if (self.isNews) {
        type = @"1";
    }
    if (self.userId.length > 0) {
        type = @"4";
        self.newsID = self.userId;
    }
    
    [startRequest netWorkPostReportReasons:type sourceId:self.newsID description:self.textView.text complainReasonId:self.selectId];
    [startRequest setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        [QJAppTool hideHUDLoading];
        NSString *message = request.responseJSONObject[@"message"];
        if (ResponseSuccess) {
            [[QJAppTool getCurrentViewController].view makeToast:@"举报成功"];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.navigationController popViewControllerAnimated:YES];

            });
        } else {
            [[QJAppTool getCurrentViewController].view makeToast:kCheckStringNil(message)];
        }
    }];
    [startRequest setFailureCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        [QJAppTool hideHUDLoading];

    }];
}


- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:QJEditMyCampsiteList object:nil];
}
@end

