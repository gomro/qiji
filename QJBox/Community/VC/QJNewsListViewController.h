//
//  QJNewsListViewController.h
//  QJBox
//
//  Created by Sun on 2022/7/26.
//

#import "QJListViewController.h"
#import "GKPageScrollView.h"
#import "QJNewsCategoryModel.h"
NS_ASSUME_NONNULL_BEGIN


@interface QJNewsListViewController : QJListViewController<GKPageListViewDelegate>

@property (nonatomic, strong) QJNewsCategoryModel *categoryModel;
@property (nonatomic, copy) NSArray *filterIds;
@end

NS_ASSUME_NONNULL_END
