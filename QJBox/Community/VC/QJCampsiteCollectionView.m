//
//  QJCampsiteCollectionView.m
//  QJBox
//
//  Created by Sun on 2022/7/5.
//

#import "QJCampsiteCollectionView.h"
#import "QJCampsiteCollectionViewCell.h"
#import "QJCampsiteRequest.h"
#import "QJCampsiteListModel.h"

@interface QJCampsiteCollectionView ()<UICollectionViewDelegate, UICollectionViewDataSource>
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) NSMutableArray *dataSource;
@property (nonatomic, assign) NSInteger page;

@end

@implementation QJCampsiteCollectionView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self.view addSubview:self.collectionView];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(0);
        make.bottom.equalTo(self.view.mas_bottom).offset(Bottom_iPhoneX_SPACE);
    }];
    self.page = 1;
    [self getCampsiteList];
    self.collectionView.mj_header = [QJRefreshHeader headerWithRefreshingBlock:^{
        self.page = 1;
        [self getCampsiteList];
    }];
    self.collectionView.mj_footer = [QJRefreshFooter footerWithRefreshingBlock:^{
        [self getCampsiteList];
    }];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeData:) name:QJEditMyCampsiteList object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshEdit:) name:QJEditMyCampsiteType object:nil];
    
}

#pragma mark - Lazy Loading
- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.sectionInset = UIEdgeInsetsMake(10, 16, 0, 16);
        layout.itemSize = CGSizeMake(62, 84);
        layout.minimumLineSpacing = 18;
        layout.minimumInteritemSpacing = (kScreenWidth - 281)/ 3.0;
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        [_collectionView registerClass:[QJCampsiteCollectionViewCell class] forCellWithReuseIdentifier:@"kIdentifierCampsiteCollectionCell"];
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
    }
    return _collectionView;
}

- (NSMutableArray *)dataSource {
    if (!_dataSource) {
        _dataSource = [NSMutableArray arrayWithCapacity:0];
    }
    return _dataSource;
}


#pragma mark - collection
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataSource.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    QJCampsiteCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"kIdentifierCampsiteCollectionCell" forIndexPath:indexPath];
    QJCampsiteListModel *model = self.dataSource[indexPath.item];
    [cell configureCellWithData:model isEdit:self.isEdit isFromEdit:self.isFromEdit];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (!self.isEdit) {
        QJCampsiteListModel *model = self.dataSource[indexPath.item];
        NSDictionary *dic = [model modelToJSONObject];
        
        NSArray *myCampsite = [QJAppTool shareManager].campsiteArr;
        NSMutableArray *myCampsiteArr = [NSMutableArray arrayWithCapacity:0];
        if (!IsArrEmpty(myCampsite)) {
            for (NSDictionary *dic in myCampsite) {
                QJCampsiteListModel *model = [QJCampsiteListModel modelWithDictionary:dic];
                [myCampsiteArr addObject:kCheckStringNil(model.campsiteID)];
            }
        }
        
        if ([myCampsiteArr containsObject:kCheckStringNil(model.campsiteID)]) {
            [QJAppTool shareManager].campsiteID = kCheckStringNil(model.campsiteID);
            if (self.isFromQJHome) {
                [self.navigationController popViewControllerAnimated:YES];
            } else {
                [self.navigationController popToRootViewControllerAnimated:YES];
            }
        } else {
            if(myCampsite.count < 20) {
                if(self.campsiteStyle == QJCampsiteViewStyleFirst) {
                    [self saveData:dic];
                } else {
                    [[NSNotificationCenter defaultCenter] postNotificationName:QJEditMyCampsiteList object:nil userInfo:@{@"type":@"2", @"data":dic}];
                }
            }
        }
        
    }
}

- (void)saveData:(NSDictionary *)dic {
    QJCampsiteListModel *model = [QJCampsiteListModel modelWithDictionary:dic];
    NSMutableArray *dataArr = [NSMutableArray arrayWithCapacity:0];
    NSMutableArray *idArr = [NSMutableArray arrayWithCapacity:0];
    [dataArr addObject:dic];
    [idArr addObject:model.campsiteID];
    [QJAppTool shareManager].campsiteArr = dataArr;
    [self chageListNetWork:[idArr copy]];
    LSUserDefaultsSET([dataArr copy], @"kMyCampsiteList");
}

- (void)chageListNetWork:(NSArray *)dataArr {
    QJCampsiteRequest *startRequest = [[QJCampsiteRequest alloc] init];
    [startRequest netWorkPostCampSet:dataArr];
    [startRequest setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            [[NSNotificationCenter defaultCenter] postNotificationName:QJMyCampsiteListRefresh object:nil userInfo:nil];
        }
    }];
}


- (void)changeData:(NSNotification *)notification {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.collectionView reloadData];
    });
}

- (void)refreshEdit:(NSNotification *)notification {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        NSDictionary *userInfo = notification.userInfo;
        self.isEdit = [userInfo[@"type"] boolValue];
        [self.collectionView reloadData];
    });
}

- (void)getCampsiteList {
    QJCampsiteRequest *startRequest = [[QJCampsiteRequest alloc] init];
    NSString *page = [NSString stringWithFormat:@"%ld", self.page];
    [startRequest netWorkGetPageCampByCategory:self.categoryId page:page];
    [startRequest setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            NSArray *dataArr = request.responseJSONObject[@"data"][@"records"];
            if (self.page == 1) {
                [self.dataSource removeAllObjects];
                [self.collectionView.mj_footer resetNoMoreData];
                [self.collectionView.mj_header endRefreshing];
            } else {
                [self.collectionView.mj_footer endRefreshing];
            }
            BOOL hasNext = [request.responseJSONObject[@"data"][@"hasNext"] boolValue];
            if (hasNext) {
                self.page += 1;
            } else {
                [self.collectionView.mj_footer endRefreshingWithNoMoreData];
            }
            for (NSDictionary *dic in dataArr) {
                QJCampsiteListModel *model = [QJCampsiteListModel modelWithDictionary:dic];
                [self.dataSource addObject:model];
            }
            [self.collectionView reloadData];
        }
    }];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:QJEditMyCampsiteList object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:QJEditMyCampsiteType object:nil];

    
}
@end
