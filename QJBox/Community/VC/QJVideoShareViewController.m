//
//  QJVideoShareViewController.m
//  QJBox
//
//  Created by Sun on 2022/8/1.
//

#import "QJVideoShareViewController.h"
#import <SJBaseVideoPlayer/SJBaseVideoPlayer.h>
#import <SJVideoPlayer/UIView+SJAnimationAdded.h>
#import "QJImageButton.h"
#import "QJCampsiteRequest.h"

#define QJMYScale(w)       [self myScalce:w]


@interface QJVideoShareViewController ()
@property (nonatomic, strong) UIView *rightContainerView;

@property (nonatomic, strong) UIView *shareBackView;

@property (nonatomic, weak) SJBaseVideoPlayer *player;
@property (nonatomic, strong) QJImageButton *saveBt;
@property (nonatomic, strong) QJImageButton *deleteBt;
@property (nonatomic, strong) QJImageButton *reportBt;

@property (nonatomic, strong) UIButton *closeBt;

@property (nonatomic, assign) BOOL isSave;
@property (nonatomic, assign) BOOL isNews;
@property (nonatomic, assign) BOOL isMySelf;
@property (nonatomic, copy) NSString *newsID;

@end

@implementation QJVideoShareViewController
@synthesize restarted = _restarted;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view addSubview:self.rightContainerView];
    [_rightContainerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.bottom.offset(0);
    }];
  
    UIBlurEffect *blur = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    UIVisualEffectView *effectview = [[UIVisualEffectView alloc] initWithEffect:blur];
    effectview.frame = [UIScreen mainScreen].bounds;
    effectview.alpha = 0.9;
    [_rightContainerView addSubview:effectview];
   
    if (self.isVertical) {
        [self makeVerticalView];
    } else {
        [self makeFullView];
    }
}

- (void)setIsVertical:(BOOL)isVertical {
    _isVertical = isVertical;
    if (isVertical) {
        [self makeVerticalView];
    } else {
        [self makeFullView];
    }
}

- (void)makeVerticalView {
    CGFloat width = (kScreenWidth - QJMYScale(280)) / 3.0 + QJMYScale(48);
    NSArray *imageArr = @[@"shareVectorIcon", @"login_qq", @"login_wx", @"friendscircle"];
    NSArray *titleArr = @[@"QQ空间", @"QQ", @"微信", @"朋友圈"];
    for (int i = 0; i < 4; i++) {
        QJImageButton *imageBt = [[QJImageButton alloc] init];
        imageBt.tag = 10 + i;
        imageBt.titleString = titleArr[i];
        imageBt.titleLb.font = [UIFont fontWithName:FONT_REGULAR size:QJMYScale(12)];;
        imageBt.titleLb.textColor = UIColorHex(000000);
        imageBt.iconString = imageArr[i];
        imageBt.imageSize = CGSizeMake(QJMYScale(48), QJMYScale(48));
        [imageBt addTarget:self action:@selector(clickItem:) forControlEvents:UIControlEventTouchUpInside];
        imageBt.space = QJMYScale(10);
        imageBt.style = ImageButtonStyleTop;
        [self.rightContainerView addSubview:imageBt];
        [imageBt mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.rightContainerView.mas_centerY).offset(-QJMYScale(24));
            make.left.equalTo(self.rightContainerView.mas_left).offset(QJMYScale(44) + width * i);
            make.width.mas_equalTo(QJMYScale(48));
        }];
    }
    [self.rightContainerView addSubview:self.closeBt];
    [self.rightContainerView addSubview:self.saveBt];
    [self.rightContainerView addSubview:self.deleteBt];
    [self.rightContainerView addSubview:self.reportBt];

    [self.closeBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.rightContainerView.mas_bottom).offset(- Bottom_iPhoneX_SPACE - QJMYScale(93));
        make.centerX.equalTo(self.rightContainerView);
        make.width.height.mas_equalTo(QJMYScale(40));
    }];
    
    CGFloat width1 = (kScreenWidth - QJMYScale(280)) / 3.0;
    [self.saveBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.rightContainerView.mas_centerY).offset(QJMYScale(24));
        make.left.equalTo(self.rightContainerView.mas_left).offset(QJMYScale(44));
        make.width.mas_equalTo(QJMYScale(48));
    }];
    
    [self.deleteBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.saveBt);
        make.left.equalTo(self.saveBt.mas_right).offset(width1);
        make.width.mas_equalTo(QJMYScale(48));
    }];
    
    [self.reportBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.saveBt);
        make.left.equalTo(self.saveBt.mas_right).offset(width1);
        make.width.mas_equalTo(QJMYScale(48));
    }];
    [self configData];
}

- (void)makeFullView {
    [self.rightContainerView addSubview:self.shareBackView];
    [self.shareBackView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.rightContainerView);
        make.width.mas_equalTo(QJMYScale(288));
        make.height.mas_equalTo(QJMYScale(75));
        make.bottom.equalTo(self.rightContainerView.mas_centerY).offset(-QJMYScale(20));
    }];
    
    CGFloat width = QJMYScale(70);
    NSArray *imageArr = @[@"shareVectorIcon", @"login_qq", @"login_wx", @"friendscircle"];
    NSArray *titleArr = @[@"QQ空间", @"QQ", @"微信", @"朋友圈"];
    for (int i = 0; i < 4; i++) {
        QJImageButton *imageBt = [[QJImageButton alloc] init];
        imageBt.tag = 10 + i;
        imageBt.titleString = titleArr[i];
        imageBt.titleLb.font = [UIFont fontWithName:FONT_REGULAR size:QJMYScale(12)];;
        imageBt.titleLb.textColor = UIColorHex(000000);
        imageBt.iconString = imageArr[i];
        imageBt.titleLabel.adjustsFontSizeToFitWidth = YES;
        imageBt.imageSize = CGSizeMake(QJMYScale(48), QJMYScale(48));
        [imageBt addTarget:self action:@selector(clickItem:) forControlEvents:UIControlEventTouchUpInside];
        imageBt.space = QJMYScale(10);
        imageBt.style = ImageButtonStyleTop;
        [self.shareBackView addSubview:imageBt];
        [imageBt mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.shareBackView.mas_left).offset(width * i);
            make.width.mas_equalTo(QJMYScale(48));
        }];
    }
    [self.view addSubview:self.closeBt];
    [self.view addSubview:self.saveBt];
    [self.view addSubview:self.deleteBt];
    [self.view addSubview:self.reportBt];

    [self.closeBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.shareBackView.mas_right).offset(QJMYScale(90));
        make.centerY.equalTo(self.rightContainerView);
        make.width.height.mas_equalTo(QJMYScale(40));
    }];
    
    [self.saveBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.rightContainerView.mas_centerY).offset(QJMYScale(20));
        make.left.equalTo(self.shareBackView.mas_left);
        make.width.mas_equalTo(QJMYScale(48));
    }];
    
    [self.deleteBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.saveBt);
        make.left.equalTo(self.saveBt.mas_right).offset(QJMYScale(32));
        make.width.mas_equalTo(QJMYScale(48));
    }];
    
    [self.reportBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.saveBt);
        make.left.equalTo(self.saveBt.mas_right).offset(QJMYScale(32));
        make.width.mas_equalTo(QJMYScale(48));
    }];
    [self configData];
}

- (void)configChangeSaveIsMySelf:(BOOL)isMySelf isSave:(BOOL)isSave isNews:(BOOL)isNews newsID:(NSString *)newsID {
    self.isMySelf = isMySelf;
    self.isSave = isSave;
    self.newsID = newsID;
    self.isNews = isNews;
    [self configData];
}

- (void)configData {
    if (self.isMySelf) {
        _deleteBt.hidden = NO;
        _reportBt.hidden = YES;
    } else {
        _deleteBt.hidden = YES;
        _reportBt.hidden = NO;
    }
    if (self.isSave) {
        _saveBt.iconString = @"shareCollectIconSelected";
        _saveBt.titleString = @"已收藏";
    } else {
        _saveBt.iconString = @"shareCollectIcon";
        _saveBt.titleString = @"收藏";
    }
}

- (UIButton *)closeBt {
    if (!_closeBt) {
        _closeBt = [UIButton new];
        [_closeBt setBackgroundImage:[UIImage imageNamed:@"shareClose"] forState:UIControlStateNormal];
        [_closeBt addTarget:self action:@selector(closeShareView) forControlEvents:UIControlEventTouchUpInside];
    }
    return _closeBt;
}

- (QJImageButton *)saveBt {
    if (!_saveBt) {
        _saveBt = [[QJImageButton alloc] init];
        _saveBt.titleString = @"收藏";
        _saveBt.titleLb.font = [UIFont fontWithName:FONT_REGULAR size:QJMYScale(12)];
        _saveBt.titleLb.textColor = UIColorHex(000000);
        _saveBt.iconString = @"shareCollectIcon";
        _saveBt.imageSize = CGSizeMake(QJMYScale(48), QJMYScale(48));
        _saveBt.space = QJMYScale(10);
        _saveBt.style = ImageButtonStyleTop;
        _saveBt.titleLabel.adjustsFontSizeToFitWidth = YES;
        [_saveBt addTarget:self action:@selector(saveClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _saveBt;
}

- (QJImageButton *)deleteBt {
    if (!_deleteBt) {
        _deleteBt = [[QJImageButton alloc] init];
        _deleteBt.titleString = @"删除";
        _deleteBt.titleLb.font = [UIFont fontWithName:FONT_REGULAR size:QJMYScale(12)];
        _deleteBt.titleLabel.adjustsFontSizeToFitWidth = YES;
        _deleteBt.titleLb.textColor = UIColorHex(000000);
        _deleteBt.iconString = @"shareDeleteIcon";
        _deleteBt.imageSize = CGSizeMake(QJMYScale(48), QJMYScale(48));
        _deleteBt.space = QJMYScale(10);
        _deleteBt.tag = 17;
        _deleteBt.hidden = YES;
        _deleteBt.style = ImageButtonStyleTop;
        [_deleteBt addTarget:self action:@selector(clickItem:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _deleteBt;
}

- (QJImageButton *)reportBt {
    if (!_reportBt) {
        _reportBt = [[QJImageButton alloc] init];
        _reportBt.titleString = @"举报";
        _reportBt.titleLb.font = [UIFont fontWithName:FONT_REGULAR size:QJMYScale(12)];
        _reportBt.titleLb.textColor = UIColorHex(000000);
        _reportBt.iconString = @"shareCautionIcon";
        _reportBt.imageSize = CGSizeMake(QJMYScale(48), QJMYScale(48));
        _reportBt.titleLabel.adjustsFontSizeToFitWidth = YES;
        _reportBt.space = QJMYScale(10);
        _reportBt.tag = 16;
        _reportBt.style = ImageButtonStyleTop;
        _reportBt.hidden = YES;
        [_reportBt addTarget:self action:@selector(clickItem:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _reportBt;
}

- (void)clickItem:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(clickShareItemTag:)] ) {
        [self.delegate clickShareItemTag:sender.tag];
    }
}

- (void)closeShareView {
    if ([self.delegate respondsToSelector:@selector(closeView:)] ) {
        [self.delegate closeView:self];
    }
}

///
/// 控制层入场
///     当播放器将要切换到此控制层时, 该方法将会被调用
///     可以在这里做入场的操作
///
- (void)restartControlLayer {
    _restarted = YES;
    if (self.player.isFullscreen) [self.player needHiddenStatusBar];
    sj_view_makeAppear(self.controlView, YES);
    sj_view_makeAppear(self.rightContainerView, YES);
}


///
/// 退出控制层
///     当播放器将要切换到其他控制层时, 该方法将会被调用
///     可以在这里处理退出控制层的操作
///
- (void)exitControlLayer {
    _restarted = NO;
    
//    sj_view_makeDisappear(self.rightContainerView, YES);
    sj_view_makeDisappear(self.controlView, YES, ^{
        if ( !self->_restarted ) [self.controlView removeFromSuperview];
    });
}

///
/// 控制层视图
///     当切换为当前控制层时, 该视图将会被添加到播放器中
///
- (UIView *)controlView {
    return self.view;
}

///
/// 当controlView被添加到播放器时, 该方法将会被调用
///
- (void)installedControlViewToVideoPlayer:(__kindof SJBaseVideoPlayer *)videoPlayer {
    _player = videoPlayer;
    
    if ( self.view.layer.needsLayout ) {
        sj_view_initializes(self.rightContainerView);
    }

    sj_view_makeDisappear(self.rightContainerView, NO);
}

///
/// 当调用播放器的controlLayerNeedAppear时, 播放器将会回调该方法
///
- (void)controlLayerNeedAppear:(__kindof SJBaseVideoPlayer *)videoPlayer {}

///
/// 当调用播放器的controlLayerNeedDisappear时, 播放器将会回调该方法
///
- (void)controlLayerNeedDisappear:(__kindof SJBaseVideoPlayer *)videoPlayer {}

///
/// 当将要触发某个手势时, 该方法将会被调用. 返回NO, 将不触发该手势
///
- (BOOL)videoPlayer:(__kindof SJBaseVideoPlayer *)videoPlayer gestureRecognizerShouldTrigger:(SJPlayerGestureType)type location:(CGPoint)location {
    if ( type == SJPlayerGestureType_SingleTap ) {
        if ( !CGRectContainsPoint(self.rightContainerView.frame, location) ) {
          
        }
    }
    return NO;
}


///
/// 当将要触发旋转时, 该方法将会被调用. 返回NO, 将不触发旋转
///
- (BOOL)canTriggerRotationOfVideoPlayer:(__kindof SJBaseVideoPlayer *)videoPlayer {
    return NO;
}

//@synthesize rightContainerView = _rightContainerView;
- (UIView *)rightContainerView {
    if ( _rightContainerView == nil ) {
        _rightContainerView = [UIView.alloc initWithFrame:CGRectZero];
        _rightContainerView.backgroundColor = UIColorHex(F2F2F2CC);
        _rightContainerView.sjv_disappearDirection = SJViewDisappearAnimation_None;
    }
    return _rightContainerView;
}

- (UIView *)shareBackView {
    if (!_shareBackView) {
        _shareBackView = [UIView new];
    }
    return _shareBackView;
}


- (CGFloat)myScalce:(CGFloat)w {
    if(self.isVertical){
        return Get375Width(w);
    }else{
        return Get375HorizontalScreen(w);
    }
}


- (void)saveClick {
//    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    QJCampsiteRequest *startRequest = [[QJCampsiteRequest alloc] init];
    NSString *type = @"2";
    if (self.isNews) {
        type = @"1";
    }
    NSString *action = @"1";
    if (self.isSave) {
        action = @"0";
    }
    [startRequest netWorkPutCampSave:kCheckStringNil(self.newsID) type:type action:action];
    [startRequest setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (ResponseSuccess) {
            
            if ([action isEqualToString:@"1"]) {
                self.isSave = YES;
//                [self.view makeToast:@"收藏成功"];
//                [QJAppTool showToast:@"收藏成功"];
                if ([self.delegate respondsToSelector:@selector(clickShareItemTag:)] ) {
                    [self.delegate clickShareItemTag:18];
                }
            } else {
                self.isSave = NO;
//                [self.view makeToast:@"取消收藏"];
//                [QJAppTool showToast:@"取消收藏"];
                if ([self.delegate respondsToSelector:@selector(clickShareItemTag:)] ) {
                    [self.delegate clickShareItemTag:19];
                }
            }
            if (self.isSave) {
                self.saveBt.iconString = @"shareCollectIconSelected";
                self.saveBt.titleString = @"已收藏";
            } else {
                self.saveBt.iconString = @"shareCollectIcon";
                self.saveBt.titleString = @"收藏";
            }
        }
    }];
    
    
    [startRequest setFailureCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
}
@end
