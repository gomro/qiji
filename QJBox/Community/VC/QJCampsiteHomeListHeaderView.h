//
//  QJCampsiteHomeListHeaderView.h
//  QJBox
//
//  Created by Sun on 2022/7/6.
//

#import <UIKit/UIKit.h>
#import "QJCampsiteDescriptionModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJCampsiteHomeListHeaderView : UIView
- (void)configureHeaderDetailWithData:(QJCampsiteDescriptionModel *)dataModel;

@end


@interface QJCampsiteHeaderFansView : UIView
- (void)configureHeaderFansWithData:(QJCampsiteDescriptionModel *)dataModel;
@end
NS_ASSUME_NONNULL_END
