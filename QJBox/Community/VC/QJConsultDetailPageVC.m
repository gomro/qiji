//
//  QJConsultDetailPageVC.m
//  QJBox
//
//  Created by Sun on 2022/10/9.
//

#import "QJConsultDetailPageVC.h"

@interface QJConsultDetailPageVC ()<QJConsultDetailListVCDelegate>
@property (nonatomic, strong) NSArray *childVCs;

/**评论排序**/
@property (nonatomic, assign) BOOL commentDesc;
@end

@implementation QJConsultDetailPageVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.view addSubview:self.commentHeader];
    [self.commentHeader configureCommentHeaderWithData:YES emptyTitle:@""];

    self.commentDesc = YES;
    self.myCategoryView.titleColor = UIColorFromRGB(0x000000);
    self.myCategoryView.titleSelectedColor = UIColorFromRGB(0x000000);
    self.myCategoryView.titleFont = FontRegular(16);
    self.myCategoryView.titleSelectedFont = FontSemibold(18);
    self.myCategoryView.titleLabelStrokeWidthEnabled = NO;
    self.myCategoryView.selectedAnimationEnabled = YES;
    
    self.myCategoryView.averageCellSpacingEnabled = NO;
    self.myCategoryView.contentEdgeInsetLeft = 20;
    self.myCategoryView.cellSpacing = 24;
    self.myCategoryView.hidden = YES;
        
    MJWeakSelf
    self.commentHeader.clickComment = ^(BOOL isDown) {
        weakSelf.commentDesc = isDown;
        [weakSelf.myCategoryView reloadData];        
    };
//    if(!self.commentData.count) {
//        if(self.isMySelf) {
//            [header configureCommentHeaderWithData:self.commentDesc emptyTitle:@"您还没有收到任何评论哦~"];
//
//        } else {
//            [header configureCommentHeaderWithData:self.commentDesc emptyTitle:@"没有任何评价哦～ "];
//
//        }
//    } else {
//        [header configureCommentHeaderWithData:self.commentDesc emptyTitle:@""];
//    }
    
}

- (void)setIsMySelf:(BOOL)isMySelf {
    _isMySelf = isMySelf;
    [self.myCategoryView reloadData];
}

- (void)configTitles:(NSArray *)titles childVCs:(NSArray *)childVCs {
    self.titles = titles;
    self.childVCs = childVCs;
    self.myCategoryView.titles = self.titles;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.myCategoryView reloadData];
    });
}

- (QJConsultCommentHeaderView *)commentHeader {
    if(!_commentHeader) {
        _commentHeader = [[QJConsultCommentHeaderView alloc] init];
    }
    return _commentHeader;
}

- (QJConsultDetailListVC *)detailListVC {
    if(!_detailListVC) {
        _detailListVC = [[QJConsultDetailListVC alloc] init];
    }
    return _detailListVC;
}


- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    self.commentHeader.frame = CGRectMake(0, 0, kScreenWidth, 34*kWScale + 10);
    self.listContainerView.frame = CGRectMake(0, 34*kWScale + 10, kScreenWidth, self.view.height - 34*kWScale - 10);

}

- (JXCategoryTitleView *)myCategoryView {
    return (JXCategoryTitleView *)self.categoryView;
}

- (JXCategoryBaseView *)preferredCategoryView {
    return [[JXCategoryTitleView alloc] init];
}

- (CGFloat)preferredCategoryViewHeight {
    return 55;
}

#pragma mark - JXCategoryListContainerViewDelegate
- (id<JXCategoryListContentViewDelegate>)listContainerView:(JXCategoryListContainerView *)listContainerView initListForIndex:(NSInteger)index {
    QJConsultDetailListVC *list = self.childVCs[index];
    list.isNews = self.isNews;
    list.idStr = self.idStr;
    list.commentDesc = self.commentDesc;
    list.isMySelf = self.isMySelf;
    list.listDelegate = self;
    return list;
}
- (void)categoryView:(JXCategoryBaseView *)categoryView didSelectedItemAtIndex:(NSInteger)index {
    //侧滑手势处理
    self.navigationController.interactivePopGestureRecognizer.enabled = (index == 0);
}

- (void)touchReplyMainComment:(NSString *)commentID name:(NSString *)name userId:(NSString *)userId toUser:(id)toUser {
    if (self.scrollDelegate && [self.scrollDelegate respondsToSelector:@selector(touchReplyMainComment:name:userId:toUser:)]) {
        [self.scrollDelegate touchReplyMainComment:commentID name:name userId:userId toUser:toUser];
    }
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    if ([self.scrollDelegate respondsToSelector:@selector(pageScrollViewWillBeginScroll)]) {
        [self.scrollDelegate pageScrollViewWillBeginScroll];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if ([self.scrollDelegate respondsToSelector:@selector(pageScrollViewDidEndedScroll)]) {
        [self.scrollDelegate pageScrollViewDidEndedScroll];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if ([self.scrollDelegate respondsToSelector:@selector(pageScrollViewDidEndedScroll)]) {
        [self.scrollDelegate pageScrollViewDidEndedScroll];
    }
}
@end
