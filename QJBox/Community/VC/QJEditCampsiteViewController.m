//
//  QJEditCampsiteViewController.m
//  QJBox
//
//  Created by Sun on 2022/7/5.
//

#import "QJEditCampsiteViewController.h"
#import "QJCampsiteCollectionView.h"
#import "QJEditCampsiteHeaderView.h"
#import "QJCampsiteRequest.h"
#import "QJCampsiteListModel.h"
#import "QJCampsiteSearchViewController.h"

@interface QJEditCampsiteViewController ()
/**搜索*/
@property (nonatomic, strong) UIButton *searchButton;
/**无营地前提*/
@property (nonatomic, strong) UIButton *firstCampsiteButton;
/**我的营地视图*/
@property (nonatomic, strong) QJEditCampsiteHeaderView *myCampsiteView;

@property (nonatomic, strong) NSMutableArray *dataSource;

/**是否编辑*/
@property (nonatomic, assign) BOOL isEdit;
@end

@implementation QJEditCampsiteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.isEdit = NO;
    self.titles = @[];
    self.myCategoryView.titleColor = UIColorHex(16191C);
    self.myCategoryView.titleSelectedColor = UIColorHex(16191);
    self.myCategoryView.titleFont = MYFONTALL(FONT_REGULAR, 14);
    self.myCategoryView.titleSelectedFont = MYFONTALL(FONT_BOLD, 18);
    self.myCategoryView.titles = self.titles;
    self.myCategoryView.contentEdgeInsetLeft = 20;
    self.myCategoryView.cellSpacing = 16;
    self.myCategoryView.averageCellSpacingEnabled = NO;
    self.myCategoryView.cellWidthZoomEnabled = YES;
    self.myCategoryView.contentScrollViewClickTransitionAnimationEnabled = NO;
    [self makeSearchButton];
    if (self.campsiteStyle == QJCampsiteViewStyleEdit) {
        self.title = @"发现营地";
        [self setNavBarHidden:NO];
        [self makeMyCampsite];
    } else {
        [self setNavBarHidden:YES];
        [self makefirstCampsite];
    }
    [self getCampsiteList];
    [self getMyCampsiteList];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshEdit:) name:QJEditMyCampsiteType object:nil];
}
- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    if (self.campsiteStyle == QJCampsiteViewStyleEdit) {
        self.myCategoryView.frame = CGRectMake(0, 200 + NavigationBar_Bottom_Y, kScreenWidth, 44);
        self.listContainerView.frame = CGRectMake(0, 244 + NavigationBar_Bottom_Y, kScreenWidth, kScreenHeight - NavigationBar_Bottom_Y  - 244);
    } else {
        self.myCategoryView.frame = CGRectMake(0, 130, kScreenWidth, 44);
        self.listContainerView.frame = CGRectMake(0, 174, kScreenWidth, kScreenHeight - Bottom_iPhoneX_SPACE - NavigationBar_Bottom_Y - 174 - 49);
    }
    
}
- (JXCategoryTitleView *)myCategoryView {
    return (JXCategoryTitleView *)self.categoryView;
}

- (JXCategoryBaseView *)preferredCategoryView {
    return [[JXCategoryTitleView alloc] init];
}

- (CGFloat)preferredCategoryViewHeight {
    return 0;
}

- (NSMutableArray *)dataSource {
    if (!_dataSource) {
        _dataSource = [NSMutableArray arrayWithCapacity:0];
    }
    return _dataSource;
}

#pragma mark - JXCategoryListContainerViewDelegate
- (NSInteger)numberOfListsInlistContainerView:(JXCategoryListContainerView *)listContainerView {
    return self.titles.count;
}
- (id<JXCategoryListContentViewDelegate>)listContainerView:(JXCategoryListContainerView *)listContainerView initListForIndex:(NSInteger)index {
    QJCampsiteCollectionView *collectionView = [[QJCampsiteCollectionView alloc] init];    
    QJCampsiteListModel *model = self.dataSource[index];
    collectionView.categoryId = model.campsiteID;
    collectionView.isFromQJHome = self.isFromQJHome;
    collectionView.isFromEdit = NO;
    collectionView.isEdit = self.isEdit;
    collectionView.campsiteStyle = self.campsiteStyle;
    return collectionView;
}

#pragma mark - JXCategoryViewDelegate
- (void)categoryView:(JXCategoryBaseView *)categoryView didSelectedItemAtIndex:(NSInteger)index {
    //作为嵌套的子容器，不需要处理侧滑手势处理。示例demo因为是继承，所以直接覆盖掉该代理方法，达到父类不调用下面一行处理侧滑手势的代码。
//    self.navigationController.interactivePopGestureRecognizer.enabled = (index == 0);
}

- (UIView *)listView {
    return self.view;
}
#pragma mark - topView
- (UIButton *)searchButton {
    if (!_searchButton) {
        _searchButton = [UIButton new];
        _searchButton.backgroundColor = UIColorHex(F5F5F5);
        _searchButton.layer.cornerRadius = 15;
        _searchButton.layer.masksToBounds = YES;
    }
    return _searchButton;
}

- (UIButton *)firstCampsiteButton {
    if (!_firstCampsiteButton) {
        _firstCampsiteButton = [UIButton new];
    }
    return _firstCampsiteButton;
}

- (void)makeSearchButton {
    [self.view addSubview:self.searchButton];
    CGFloat topHeight = 10;
    if (self.campsiteStyle == QJCampsiteViewStyleEdit) {
        topHeight = 10 + NavigationBar_Bottom_Y;
    }
    [self.searchButton addTarget:self action:@selector(searchView:) forControlEvents:UIControlEventTouchUpInside];
    [self.searchButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_top).offset(topHeight);
        make.left.equalTo(self.view.mas_left).offset(16);
        make.right.equalTo(self.view.mas_right).offset(-16);
        make.height.mas_equalTo(34);
    }];
    UIImageView *image = [UIImageView new];
    image.image = [UIImage imageNamed:@"iconSearch"];
    [self.searchButton addSubview:image];
    [image mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.searchButton.mas_left).offset(8);
        make.centerY.equalTo(self.searchButton);
        make.width.height.mas_equalTo(16);
    }];
    
    UILabel *searchLb = [UILabel new];
    searchLb.textColor = UIColorHex(C8CACC);
    searchLb.text = @"输入关键词搜索游戏";
    searchLb.font = MYFONTALL(FONT_REGULAR, 14);
    [self.searchButton addSubview:searchLb];
    [searchLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(image.mas_right).offset(4);
        make.centerY.equalTo(self.searchButton);
    }];
    
}

- (void)makefirstCampsite {
    [self.view addSubview:self.firstCampsiteButton];
    [self.firstCampsiteButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.searchButton.mas_bottom).offset(26);
        make.left.equalTo(self.view.mas_left).offset(20);
        make.right.equalTo(self.view.mas_right).offset(-5);
        make.height.mas_equalTo(46);
    }];
    
    UIImageView *image = [UIImageView new];
    [self.firstCampsiteButton addSubview:image];
    image.image = [UIImage imageNamed:@"campsiteEmptyAdd"];
    [image mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.firstCampsiteButton.mas_left);
        make.centerY.equalTo(self.firstCampsiteButton);
        make.width.height.mas_equalTo(46);
    }];
    
    UILabel *campsiteLb = [UILabel new];
    campsiteLb.textColor = UIColorHex(919599);
    campsiteLb.text = @"您还没有营地哦！赶紧加入营地一起讨论吧～";
    campsiteLb.font = MYFONTALL(FONT_REGULAR, 14);
    [self.firstCampsiteButton addSubview:campsiteLb];
    [campsiteLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(image.mas_right).offset(8);
        make.right.equalTo(self.firstCampsiteButton.mas_right);
        make.centerY.equalTo(self.firstCampsiteButton);
    }];
    
    [self.firstCampsiteButton addTarget:self action:@selector(editCampsite:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)editCampsite:(UIButton *)sender {
    QJEditCampsiteViewController *edit = [[QJEditCampsiteViewController alloc] init];
    edit.campsiteStyle = QJCampsiteViewStyleEdit;
    [self.navigationController pushViewController:edit animated:YES];
    
}

- (QJEditCampsiteHeaderView *)myCampsiteView {
    if (!_myCampsiteView) {
        _myCampsiteView = [[QJEditCampsiteHeaderView alloc] init];
    }
    return _myCampsiteView;
}

- (void)makeMyCampsite {
    [self.view addSubview:self.myCampsiteView];
    [self.myCampsiteView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.searchButton.mas_bottom).offset(10);
        make.left.right.equalTo(self.view);
        make.height.mas_equalTo(130);
    }];
    MJWeakSelf
    self.myCampsiteView.clickCampsite = ^{
        if (weakSelf.campsiteStyle == QJCampsiteViewStyleEdit) {
            [weakSelf.navigationController popToRootViewControllerAnimated:YES];
        }
    };
}

- (void)searchView:(UIButton *)sender {
    QJCampsiteSearchViewController *search = [[QJCampsiteSearchViewController alloc] init];
    [self.navigationController pushViewController:search animated:YES];
}

#pragma mark -- 数据请求
- (void)getCampsiteList {
    QJCampsiteRequest *startRequest = [[QJCampsiteRequest alloc] init];
    [startRequest netWorkGetCampCategory];
    [startRequest setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            NSArray *dataArr = request.responseJSONObject[@"data"];
            [self.dataSource removeAllObjects];
            NSMutableArray *titleArr = [NSMutableArray arrayWithCapacity:0];
            for (NSDictionary *dic in dataArr) {
                QJCampsiteListModel *model = [QJCampsiteListModel modelWithDictionary:dic];
                [titleArr addObject:kCheckStringNil(model.name)];
                [self.dataSource addObject:model];
            }
            self.titles  = [titleArr copy];
            self.myCategoryView.titles = self.titles;
            [self.myCategoryView reloadData];
        }
    }];
}

- (void)getMyCampsiteList {
    QJCampsiteRequest *startRequest = [[QJCampsiteRequest alloc] init];
    [startRequest netWorkGetMineCamp];
    [startRequest setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            NSArray *dataArr = request.responseJSONObject[@"data"];
            LSUserDefaultsSET(dataArr, @"kMyCampsiteList");
            NSMutableArray *dataModel = [NSMutableArray arrayWithCapacity:0];
            for (NSDictionary *dic in dataArr) {
                QJCampsiteListModel *model = [QJCampsiteListModel modelWithDictionary:dic];
                [dataModel addObject:model];
            }
            [QJAppTool shareManager].campsiteArr = dataArr;
            if (self.campsiteStyle == QJCampsiteViewStyleEdit) {
                [self.myCampsiteView configureHeaderEditWithData:[dataModel copy]];
            }
        }
    }];
}

- (void)refreshEdit:(NSNotification *)notification {
    NSDictionary *userInfo = notification.userInfo;
    self.isEdit = [userInfo[@"type"] boolValue];
}




- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:QJEditMyCampsiteType object:nil];

    
}
@end
