//
//  QJCampsiteCollectionView.h
//  QJBox
//
//  Created by Sun on 2022/7/5.
//

#import "QJListViewController.h"

#import "QJEditCampsiteViewController.h"
NS_ASSUME_NONNULL_BEGIN

@interface QJCampsiteCollectionView : QJListViewController
@property (nonatomic, copy) NSString *categoryId;
@property (nonatomic, assign) BOOL isFromEdit;

@property (nonatomic, assign) QJCampsiteViewStyle campsiteStyle;

/* 是否来自奇迹有礼 */
@property (nonatomic, assign) BOOL isFromQJHome;
/**是否编辑*/
@property (nonatomic, assign) BOOL isEdit;
@end

NS_ASSUME_NONNULL_END
