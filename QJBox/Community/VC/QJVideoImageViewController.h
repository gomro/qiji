//
//  QJVideoImageViewController.h
//  QJBox
//
//  Created by Sun on 2022/8/4.
//

#import "QJBaseViewController.h"


typedef void (^ClickFinish)(UIImage *image);

@interface QJVideoImageViewController : QJBaseViewController
/**视频地址*/
@property (nonatomic, copy) NSURL *videoUrl;
/**完成回调*/
@property (nonatomic, copy) ClickFinish clickFinish;


@end

@interface QJVideoImageViewControllerCell : UICollectionViewCell
@property (nonatomic,strong) UIImageView *icon;
@end

@interface QJVideoImageTopView : UIView
@property (nonatomic,strong) UIView *topView;
@end
