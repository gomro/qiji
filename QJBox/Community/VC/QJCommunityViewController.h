//
//  QJCommunityViewController.h
//  QJBox
//
//  Created by wxy on 2022/6/1.
//社区

#import "QJContentBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJCommunityViewController : QJContentBaseViewController

@property (nonatomic, assign) BOOL isCountDownTask;
@property (nonatomic, assign) NSInteger communitySelectRow;

@end

NS_ASSUME_NONNULL_END
