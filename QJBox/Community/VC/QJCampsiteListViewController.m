//
//  QJCampsiteListViewController.m
//  QJBox
//
//  Created by Sun on 2022/7/6.
//

#import "QJCampsiteListViewController.h"
#import "QJCampsiteListViewCell.h"
#import "QJConsultDetailViewController.h"
#import "QJCampsiteRequest.h"
#import "QJCampsiteDetailModel.h"
#import "QJMySpaceViewController.h"

@interface QJCampsiteListViewController ()<UITableViewDelegate, UITableViewDataSource, QJCampsiteListViewCellDelegate, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate>
@property (nonatomic, strong) UITableView *tableView;
/**数据*/
@property (nonatomic, strong) NSMutableArray *dataArr;
@property (nonatomic, copy) void(^listScrollViewDidScroll)(UIScrollView *scrollView);

@property (nonatomic, assign) NSInteger page;


@end

@implementation QJCampsiteListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadUI];
    self.view.backgroundColor = [UIColor clearColor];
    self.banType = 0;
    self.tableView.mj_header = [QJRefreshHeader headerWithRefreshingBlock:^{
        [[NSNotificationCenter defaultCenter] postNotificationName:QJMyCampsitePageRefresh object:nil userInfo:nil];        
        self.page = 1;
        [self getCampsiteList];
    }];
    self.tableView.mj_footer = [QJRefreshFooter footerWithRefreshingBlock:^{
        [self getCampsiteList];
    }];
}
- (void)setSort:(NSString *)sort {
    [self.tableView scrollToTop];
    _sort = sort;
    self.page = 1;
    [self getCampsiteList];
}

#pragma mark - Load UI
- (void)loadUI {
    [self.view addSubview:self.tableView];
    
    [self makeConstraints];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
}

- (void)makeConstraints {
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.right.bottom.equalTo(self.view);
//        make.top.equalTo(self.view.mas_top).offset(44);
        make.edges.equalTo(self.view);
    }];
}

#pragma mark - Lazy Loading
- (UITableView *)tableView {
    if (!_tableView) {
        self.tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        self.tableView.backgroundColor = [UIColor whiteColor];
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.tableView.showsVerticalScrollIndicator = NO;
        self.tableView.showsHorizontalScrollIndicator = NO;
        self.tableView.estimatedRowHeight = 200;
        if (@available(iOS 11.0, *)) {
            self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
        self.tableView.emptyDataSetSource = self;
        self.tableView.emptyDataSetDelegate = self;
    }
    return _tableView;
}

- (NSMutableArray *)dataArr {
    if (!_dataArr) {
        self.dataArr = [NSMutableArray arrayWithCapacity:0];
    }
    return _dataArr;
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return self.dataArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellMineList = @"cellMineListId";
    QJCampsiteListCellStyle style = QJCampsiteListCellStyleDefault;
    QJCampsiteDetailModel *model = self.dataArr[indexPath.row];
    model.type = @"2";
    if (![kCheckStringNil(model.videoAddress) isEqualToString:@""]) {
        cellMineList = @"cellMineListIdVideo";
        style = QJCampsiteListCellStyleVideo;
    } else {
        if (model.pictures.count) {
            cellMineList = @"cellMineListIdPictures";
            style = QJCampsiteListCellStyleImage;
        }
    }
    QJCampsiteListViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellMineList];
    if (!cell) {
        cell = [[QJCampsiteListViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellMineList cellStyle:style];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.cellDelegate = self;
    [cell configureCellWithData:model];
    return cell;
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    QJCampsiteDetailModel *model = self.dataArr[indexPath.row];
    [self pushDetailViewWithID:kCheckStringNil(model.newsId) isComment:NO];
}

- (void)didCommentButtonWithID:(NSString *)campsiteID {
    [self pushDetailViewWithID:campsiteID isComment:YES];
}

- (void)pushDetailViewWithID:(NSString *)campsiteID isComment:(BOOL)isComment {
    QJConsultDetailViewController *detailView = [[QJConsultDetailViewController alloc] init];
    detailView.isFromCamp = YES;
    detailView.idStr = campsiteID;
    detailView.isComment = isComment;
    [self.navigationController pushViewController:detailView animated:YES];
}

#pragma mark - GKPageListViewDelegate
- (UIScrollView *)listScrollView {
    return self.tableView;
}

- (void)listViewDidScrollCallback:(void (^)(UIScrollView * _Nonnull))callback {
    self.listScrollViewDidScroll = callback;
}
#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    !self.listScrollViewDidScroll ? : self.listScrollViewDidScroll(scrollView);
}

#pragma mark - NetWork
- (void)getCampsiteList {
    QJCampsiteRequest *startRequest = [[QJCampsiteRequest alloc] init];
    NSString *page = [NSString stringWithFormat:@"%ld", self.page];
    [startRequest netWorkGetCampNews:self.campsiteID page:page sort:self.sort];
    [startRequest setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        self.banType = 1;
        if (ResponseSuccess) {
            NSArray *dataArr = request.responseJSONObject[@"data"][@"records"];
            if (self.page == 1) {
                [self.dataArr removeAllObjects];
                [self.tableView.mj_footer resetNoMoreData];
                [self.tableView.mj_header endRefreshing];
            } else {
                [self.tableView.mj_footer endRefreshing];
            }
            BOOL hasNext = [request.responseJSONObject[@"data"][@"hasNext"] boolValue];
            if (hasNext) {
                self.page += 1;
            } else {
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
            }
            
            for (NSDictionary *dic in dataArr) {
                QJCampsiteDetailModel *model = [QJCampsiteDetailModel modelWithDictionary:dic];
                [self.dataArr addObject:model];
            }
            [self.tableView reloadData];
        } else {
            if (self.page == 1) {
                [self.tableView.mj_footer resetNoMoreData];
                [self.tableView.mj_header endRefreshing];
            } else {
                [self.tableView.mj_footer endRefreshing];
            }
            NSString *code = request.responseJSONObject[@"code"];
            if([kCheckStringNil(code) isEqualToString:@"21602"]) {//营地封禁
                self.banType = 2;
                
                [self.dataArr removeAllObjects];
            } else if([kCheckStringNil(code) isEqualToString:@"21603"]) {//营地封停
                self.banType = 3;
                
                [[NSNotificationCenter defaultCenter] postNotificationName:QJMyCampsiteListRefresh object:nil userInfo:nil];
            }
            [self.tableView reloadData];
        }
    }];
    [startRequest setFailureCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        self.banType = 1;
        [self.tableView reloadData];
        if (self.page == 1) {
            [self.tableView.mj_footer resetNoMoreData];
            [self.tableView.mj_header endRefreshing];
        } else {
            [self.tableView.mj_footer endRefreshing];
        }
    }];

}

#pragma mark - 封停封禁数据刷新
- (void)didRefreshListView:(NSString *)code {
    if([kCheckStringNil(code) isEqualToString:@"21602"]) {//营地封禁
        self.page = 1;
        [self getCampsiteList];
    } else if([kCheckStringNil(code) isEqualToString:@"21603"]) {//营地封停
        
        [[NSNotificationCenter defaultCenter] postNotificationName:QJMyCampsiteListRefresh object:nil userInfo:nil];

    }
}

#pragma mark - 个人空间
- (void)didClickUserView:(NSString *)userId {
    QJMySpaceViewController *vc = [QJMySpaceViewController new];
    vc.userId = userId;
    [self.navigationController pushViewController:vc animated:YES];
}



#pragma mark -- DZNEmptyDataSetSource, DZNEmptyDataSetDelegate
- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView {
    NSString *text = @"这里什么也没有～";
    if(self.banType == 2) {
        text = @"很抱歉～营地违反平台规则\n已封禁";
    }
    
    NSMutableParagraphStyle *paragraph = [NSMutableParagraphStyle new];
    paragraph.lineBreakMode = NSLineBreakByWordWrapping;
    paragraph.alignment = NSTextAlignmentCenter;
    NSDictionary *attributes = @{
                                 NSFontAttributeName:MYFONTALL(FONT_REGULAR, 12),
                                 NSForegroundColorAttributeName:UIColorHex(000000),
                                 NSParagraphStyleAttributeName:paragraph
                                 };
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

- (CGFloat)spaceHeightForEmptyDataSet:(UIScrollView *)scrollView {
    return 16.0f;
}

- (CGFloat)verticalOffsetForEmptyDataSet:(UIScrollView *)scrollView {
    return - self.tableView.frame.size.height / 4.0 ;
}

- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView {
    return [UIImage imageNamed:@"empty_no_error"];
}

- (BOOL)emptyDataSetShouldDisplay:(UIScrollView *)scrollView {
    if (self.banType) {
        return YES;
    }
    return NO;
}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView {
    return YES;
}
@end
