//
//  QJCampsiteSearchViewController.m
//  QJBox
//
//  Created by Sun on 2022/7/18.
//

#import "QJCampsiteSearchViewController.h"
#import "QJCampsiteCollectionViewCell.h"
#import "QJCampsiteRequest.h"

@interface QJCampsiteSearchViewController ()<UITextFieldDelegate, UICollectionViewDelegate, UICollectionViewDataSource>
@property (nonatomic, strong) UITextField *searchTf;
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) NSMutableArray *dataSource;


@end

@implementation QJCampsiteSearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavBarHidden:YES];
    self.view.backgroundColor = [UIColor whiteColor];
    [self subSetUI];
    [self.searchTf becomeFirstResponder];
    
    [self.view addSubview:self.collectionView];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.mas_equalTo(0);
        make.top.equalTo(self.view.mas_top).offset(54 + QJStatusBarHeight);
    }];
}

- (void)subSetUI {
    UIButton *cancleBt = [UIButton new];
    [cancleBt setTitle:@"取消" forState:UIControlStateNormal];
    [cancleBt setTitleColor:UIColorHex(090909) forState:UIControlStateNormal];
    cancleBt.titleLabel.font = MYFONTALL(FONT_REGULAR, 14);
    [cancleBt addTarget:self action:@selector(popView:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:cancleBt];
    
    UIView *backView = [[UIView alloc] init];
    backView.backgroundColor = UIColorHex(F5F5F5);
    backView.layer.cornerRadius = 15;
    backView.layer.masksToBounds = YES;
    [self.view addSubview:backView];
    
    CGFloat topHeight = 10 + QJStatusBarHeight;
    [backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_top).offset(topHeight);
        make.left.equalTo(self.view.mas_left).offset(16);
        make.right.equalTo(cancleBt.mas_left).offset(-16);
        make.height.mas_equalTo(34*kWScale);
    }];
    
    [cancleBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.view.mas_right).offset(-16);
        make.centerY.equalTo(backView);
        make.width.mas_equalTo(30*kWScale);
    }];
    
    UIImageView *image = [UIImageView new];
    image.image = [UIImage imageNamed:@"iconSearch"];
    [backView addSubview:image];
    [image mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(backView.mas_left).offset(8);
        make.centerY.equalTo(backView);
        make.width.height.mas_equalTo(16*kWScale);
    }];
    
    self.searchTf = [UITextField new];
    self.searchTf.textColor = UIColorHex(474849);
    NSMutableAttributedString *placeholderString = [[NSMutableAttributedString alloc] initWithString:@"搜索营地名称" attributes:@{NSForegroundColorAttributeName : UIColorHex(C8CACC)}];
    self.searchTf.attributedPlaceholder = placeholderString;
    self.searchTf.font = MYFONTALL(FONT_REGULAR, 14);
    self.searchTf.delegate = self;
    self.searchTf.returnKeyType = UIReturnKeySearch;
    self.searchTf.clearButtonMode = UITextFieldViewModeWhileEditing;
    [backView addSubview:self.searchTf];
    [self.searchTf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(image.mas_right).offset(4);
        make.right.equalTo(backView.mas_right).offset(-4);
        make.centerY.equalTo(backView);
    }];
}

- (void)popView:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - Lazy Loading
- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.sectionInset = UIEdgeInsetsMake(10, 16, 0, 16);
        layout.itemSize = CGSizeMake(62, 84);
        layout.minimumLineSpacing = 18;
        layout.minimumInteritemSpacing = (kScreenWidth - 281)/ 3.0;
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.backgroundColor = [UIColor clearColor];
        [_collectionView registerClass:[QJCampsiteCollectionViewCell class] forCellWithReuseIdentifier:@"kIdentifierCampsiteCollectionCell"];
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
    }
    return _collectionView;
}

- (NSMutableArray *)dataSource {
    if (!_dataSource) {
        _dataSource = [NSMutableArray arrayWithCapacity:0];
    }
    return _dataSource;
}


#pragma mark - collection
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
//    if (self.searchTf.text.length) {
//        [collectionView collectionViewDisplayWhenHaveNoDataWithMsg:@"未找到您所搜索的营地" centerImage:[UIImage imageNamed:@"icon_empty_no_data"] ifNecessaryForRowCount:self.dataSource.count];
//    }
    return self.dataSource.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    QJCampsiteCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"kIdentifierCampsiteCollectionCell" forIndexPath:indexPath];
    QJCampsiteListModel *model = self.dataSource[indexPath.item];
    [cell configureCellWithData:model isEdit:NO isFromEdit:YES];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    QJCampsiteListModel *model = self.dataSource[indexPath.item];
    NSDictionary *dic = [model modelToJSONObject];
    NSArray *myCampsite = [QJAppTool shareManager].campsiteArr;
    NSMutableArray *myCampsiteArr = [NSMutableArray arrayWithCapacity:0];
    if (!IsArrEmpty(myCampsite)) {
        for (NSDictionary *dic in myCampsite) {
            QJCampsiteListModel *model = [QJCampsiteListModel modelWithDictionary:dic];
            [myCampsiteArr addObject:kCheckStringNil(model.campsiteID)];
        }
    }
    if ([myCampsiteArr containsObject:kCheckStringNil(model.campsiteID)]) {
        [QJAppTool shareManager].campsiteID = kCheckStringNil(model.campsiteID);
        [self.navigationController popToRootViewControllerAnimated:YES];

    } else {
        if(myCampsite.count < 20) {
            [[NSNotificationCenter defaultCenter] postNotificationName:QJEditMyCampsiteList object:nil userInfo:@{@"type":@"2", @"data":dic}];
        }
    }
    
}

- (void)getSearchCampsiteList:(NSString *)keyWord {
    QJCampsiteRequest *startRequest = [[QJCampsiteRequest alloc] init];
    [startRequest netWorkGetCampByKeywords:keyWord];
    [startRequest setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            NSArray *dataArr = request.responseJSONObject[@"data"];
            [self.dataSource removeAllObjects];
            for (NSDictionary *dic in dataArr) {
                QJCampsiteListModel *model = [QJCampsiteListModel modelWithDictionary:dic];
                [self.dataSource addObject:model];
            }
            [self.collectionView reloadData];
            QJEmptyCommonView *empty = [QJEmptyCommonView new];
            empty.string = @"还没有任何您搜索的营地~";
            empty.emptyImage = @"empty_no_search";
            [self.collectionView collectionViewDisplayWhenHaveNoDataWithView:empty ifNecessaryForRowCount:self.dataSource.count];

        }
    }];
    [startRequest setFailureCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        QJEmptyCommonView *empty = [QJEmptyCommonView new];
        empty.string = @"还没有任何您搜索的营地~";
        empty.emptyImage = @"empty_no_search";
        [self.collectionView collectionViewDisplayWhenHaveNoDataWithView:empty ifNecessaryForRowCount:self.dataSource.count];
    }];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    NSString *keyWord = textField.text;
    [self getSearchCampsiteList:keyWord];
    return YES;
}

@end
