//
//  QJNewsListViewController.m
//  QJBox
//
//  Created by Sun on 2022/7/26.
//

#import "QJNewsListViewController.h"
#import "QJCampsiteRequest.h"
#import "QJNewsListViewCell.h"
#import "QJNewsListModel.h"
#import "QJConsultDetailViewController.h"
@interface QJNewsListViewController ()<UITableViewDelegate, UITableViewDataSource, QJNewsListViewCellDelegate>
@property (nonatomic, strong) UITableView *tableView;
/**数据*/
@property (nonatomic, strong) NSMutableArray *dataArr;

@property (nonatomic, assign) NSInteger page;
@property (nonatomic, copy) void(^listScrollViewDidScroll)(UIScrollView *scrollView);
/**是否是热度排序*/
@property (nonatomic, assign) BOOL isHot;

@end

@implementation QJNewsListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadUI];
    self.view.backgroundColor = [UIColor clearColor];
    self.isHot = YES;
    self.tableView.mj_header = [QJRefreshHeader headerWithRefreshingBlock:^{
        self.page = 1;
        self.isHot = !self.isHot;
        [self getNewsList];
    }];
    self.tableView.mj_footer = [QJRefreshFooter footerWithRefreshingBlock:^{
        [self getNewsList];
    }];

}

- (void)setFilterIds:(NSArray *)filterIds {
    _filterIds = filterIds;
    self.page = 1;
    [self getNewsList];
}

#pragma mark - Load UI
- (void)loadUI {
    [self.view addSubview:self.tableView];
    
    [self makeConstraints];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
}

- (void)makeConstraints {
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.right.bottom.equalTo(self.view);
//        make.top.equalTo(self.view.mas_top).offset(44);
        make.edges.equalTo(self.view);
    }];
}

#pragma mark - Lazy Loading
- (UITableView *)tableView {
    if (!_tableView) {
        self.tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        self.tableView.backgroundColor = [UIColor whiteColor];
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.tableView.showsVerticalScrollIndicator = NO;
        self.tableView.showsHorizontalScrollIndicator = NO;
        if (@available(iOS 11.0, *)) {
            self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
    }
    return _tableView;
}

- (NSMutableArray *)dataArr {
    if (!_dataArr) {
        self.dataArr = [NSMutableArray arrayWithCapacity:0];
    }
    return _dataArr;
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellNewsList = @"cellNewsListId";

    QJNewsListViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellNewsList];
    if (!cell) {
        cell = [[QJNewsListViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellNewsList];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.cellDelegate = self;
    QJNewsListModel *model = self.dataArr[indexPath.row];
    [cell configureCellWithData:model];
    return cell;
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return Get375Width(109);
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    QJConsultDetailViewController *detailView = [[QJConsultDetailViewController alloc] init];
    QJNewsListModel *model = self.dataArr[indexPath.row];
    detailView.idStr = model.infoId;
    detailView.isNews = YES;
    [self.navigationController pushViewController:detailView animated:YES];
}

- (void)didVideoButtonWithID:(NSString *)newsID {
    QJConsultDetailViewController *detailView = [[QJConsultDetailViewController alloc] init];
    detailView.idStr = newsID;
    detailView.isNews = YES;
    detailView.isVolume = YES;
    [self.navigationController pushViewController:detailView animated:YES];
}

#pragma mark - GKPageListViewDelegate
- (UIScrollView *)listScrollView {
    return self.tableView;
}

- (void)listViewDidScrollCallback:(void (^)(UIScrollView * _Nonnull))callback {
    self.listScrollViewDidScroll = callback;
}
#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    !self.listScrollViewDidScroll ? : self.listScrollViewDidScroll(scrollView);
}

- (void)getNewsList {
    QJCampsiteRequest *startRequest = [[QJCampsiteRequest alloc] init];
    NSString *page = [NSString stringWithFormat:@"%ld", self.page];
    if ([self.categoryModel.name isEqualToString:@"关注"]) {
        [startRequest netWorkGetInfoListInStarPage:page];
    } else {
        if (self.isHot) {
            [startRequest netWorkGetInfoListPage:page ids:self.filterIds sort:@"HD" categoryCode:kCheckStringNil(self.categoryModel.code)];
        } else {
            [startRequest netWorkGetInfoListPage:page ids:self.filterIds sort:@"TD" categoryCode:kCheckStringNil(self.categoryModel.code)];
        }
    }
    [startRequest setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            NSArray *dataArr = request.responseJSONObject[@"data"][@"records"];
            if (self.page == 1) {
                [self.dataArr removeAllObjects];
                [self.tableView.mj_footer resetNoMoreData];
                [self.tableView.mj_header endRefreshing];
            } else {
                [self.tableView.mj_footer endRefreshing];
            }
            BOOL hasNext = [request.responseJSONObject[@"data"][@"hasNext"] boolValue];
            if (hasNext) {
                self.page += 1;
            } else {
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
            }
            for (NSDictionary *dic in dataArr) {
                QJNewsListModel *model = [QJNewsListModel modelWithDictionary:dic];
                [self.dataArr addObject:model];
            }
            [self.tableView reloadData];
        }
        QJEmptyCommonView *emptyView = [QJEmptyCommonView new];
        if ([self.categoryModel.name isEqualToString:@"关注"]) {
            emptyView.string = @"暂无关注列表噢~";
        }else{
            emptyView.string = @"暂无数据";
        }
        [self.tableView tableViewDisplayWhenHaveNoDataWithView:emptyView ifNecessaryForRowCount:self.dataArr.count];
    }];
    [startRequest setFailureCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        if (self.page == 1) {
            [self.tableView.mj_footer resetNoMoreData];
            [self.tableView.mj_header endRefreshing];
        } else {
            [self.tableView.mj_footer endRefreshing];
        }
        QJEmptyCommonView *emptyView = [QJEmptyCommonView new];
        if ([self.categoryModel.name isEqualToString:@"关注"]) {
            emptyView.string = @"暂无关注列表噢~";
        }else{
            emptyView.string = @"暂无数据";
        }
        [self.tableView tableViewDisplayWhenHaveNoDataWithView:emptyView ifNecessaryForRowCount:self.dataArr.count];
    }];
}

@end
