//
//  QJEditCampsiteViewController.h
//  QJBox
//
//  Created by Sun on 2022/7/5.
//

#import "QJContentBaseViewController.h"

typedef NS_ENUM(NSInteger, QJCampsiteViewStyle){
    QJCampsiteViewStyleFirst,  //首次营地展示
    QJCampsiteViewStyleEdit,  //营地编辑
};

NS_ASSUME_NONNULL_BEGIN

@interface QJEditCampsiteViewController : QJContentBaseViewController<JXCategoryListContentViewDelegate>
@property (nonatomic, assign) QJCampsiteViewStyle campsiteStyle;
@property (nonatomic, assign) BOOL isFromQJHome;
 
@end

NS_ASSUME_NONNULL_END
