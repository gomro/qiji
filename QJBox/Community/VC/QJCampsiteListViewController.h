//
//  QJCampsiteListViewController.h
//  QJBox
//
//  Created by Sun on 2022/7/6.
//

#import "QJListViewController.h"
#import "GKPageScrollView.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJCampsiteListViewController : QJListViewController<GKPageListViewDelegate>
@property (nonatomic, copy) NSString *campsiteID;

/**发布时间倒序TD、热门倒序HD*/
@property (nonatomic, copy) NSString *sort;

/**请求成功1 封禁2 封停3 **/
@property (nonatomic, assign) NSInteger banType;
@end

NS_ASSUME_NONNULL_END
