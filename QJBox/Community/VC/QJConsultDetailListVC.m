//
//  QJConsultDetailListVC.m
//  QJBox
//
//  Created by Sun on 2022/10/9.
//

#import "QJConsultDetailListVC.h"
#import "QJConsultCommentViewCell.h"
#import "QJCampsiteRequest.h"
@interface QJConsultDetailListVC ()<UITableViewDelegate, UITableViewDataSource, QJConsultCommentViewCellDelegate>
@property (nonatomic, copy) void(^listScrollViewDidScroll)(UIScrollView *scrollView);
@property (nonatomic, strong) UITableView *tableView;
/**评论*/
@property (nonatomic, strong) NSMutableArray *commentData;

@property (nonatomic, assign) NSInteger page;
@end

@implementation QJConsultDetailListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    
    self.page = 1;
    [self getCommentDetail];
    self.tableView.mj_footer = [QJRefreshFooter footerWithRefreshingBlock:^{
        [self getCommentDetail];
    }];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadComment:) name:QJCommentRefresh object:nil];

}

- (void)reloadComment:(NSNotification *)notification {
    NSDictionary *userInfo = notification.userInfo;
    BOOL refresh = [userInfo[@"refresh"] boolValue];
    self.page = 1;
    [self getCommentDetail];

}

- (void)setCommentDesc:(BOOL)commentDesc {
    _commentDesc = commentDesc;
    self.page = 1;
    [self getCommentDetail];
}

- (void)setIsMySelf:(BOOL)isMySelf {
    _isMySelf = isMySelf;
    [self.tableView reloadData];
}

- (UITableView *)tableView {
    if (!_tableView) {
        self.tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        self.tableView.backgroundColor = [UIColor whiteColor];
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.tableView.showsVerticalScrollIndicator = NO;
        self.tableView.showsHorizontalScrollIndicator = NO;
        self.tableView.estimatedRowHeight = 200;
        self.tableView.estimatedSectionHeaderHeight = 0;
        self.tableView.estimatedSectionFooterHeight = 0;
        self.tableView.rowHeight = UITableViewAutomaticDimension;
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        if (@available(iOS 11.0, *)) {
            self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
        self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    }
    return _tableView;
}

- (NSMutableArray *)commentData {
    if (!_commentData) {
        self.commentData = [NSMutableArray arrayWithCapacity:0];
    }
    return _commentData;
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section { 
    QJEmptyCommonView *emptyView = [QJEmptyCommonView new];
    if(self.isMySelf) {
        emptyView.string = @"您还没有收到任何评论哦~";
    } else {
        emptyView.string = @"没有任何评价哦～";
    }
    emptyView.emptyImage = @"empty_no_data";
    emptyView.topSpace = 100;
    [tableView tableViewDisplayWhenHaveNoDataWithView:emptyView ifNecessaryForRowCount:self.commentData.count];
    return self.commentData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    QJConsultCommentViewCell *cell = [QJConsultCommentViewCell cellWithTableView:tableView];
    cell.backgroundColor = [UIColor whiteColor];
    cell.cellDelegate = self;
    QJCommentTopFrame *topicFrame = self.commentData[indexPath.row];
    cell.topicFrame = topicFrame;
    return cell;
}


    
#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    QJCommentTopFrame *topicFrame = self.commentData[indexPath.row];
    if (topicFrame.tableViewFrame.size.height == 0) {
        return topicFrame.height + topicFrame.tableViewFrame.size.height + 10;
    }else{
        return topicFrame.height + topicFrame.tableViewFrame.size.height + 10;
    }
}
    
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.01;
}
    
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 0.01)];
    return header;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 0.01)];
    return footerView;
}
    
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

- (void)getCommentDetail {
    QJCampsiteRequest *startRequest = [[QJCampsiteRequest alloc] init];
    NSString *page = [NSString stringWithFormat:@"%ld", self.page];
    if (self.isNews) {
        [startRequest netWorkGetCommentRoot:self.idStr page:page type:@"1" desc:!self.commentDesc];
    } else {
        [startRequest netWorkGetCommentRoot:self.idStr page:page type:@"2" desc:!self.commentDesc];
    }
    [startRequest setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            NSArray *dataArr = request.responseJSONObject[@"data"][@"records"];
            if (self.page == 1) {
                [self.commentData removeAllObjects];
                [self.tableView.mj_footer resetNoMoreData];
            } else {
                [self.tableView.mj_footer endRefreshing];
            }
            BOOL hasNext = [request.responseJSONObject[@"data"][@"hasNext"] boolValue];
            if (hasNext) {
                self.page += 1;
            } else {
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
            }
            CGFloat cellHeight = 0;
            for (NSDictionary *dic in dataArr) {
                QJCommentTopModel *model = [QJCommentTopModel modelWithDictionary:dic];
                QJCommentTopFrame *topicFrame = [self topicFrameWithTopic:model];
                if (topicFrame.tableViewFrame.size.height == 0) {
                    cellHeight = cellHeight + topicFrame.height + topicFrame.tableViewFrame.size.height;
                }else{
                    cellHeight = cellHeight + topicFrame.height + topicFrame.tableViewFrame.size.height + 10;
                }
                [self.commentData addObject:topicFrame];
            }            
            [UIView performWithoutAnimation:^{
                [self.tableView reloadData];
            }];
        } else {
            if (self.page == 1) {
                [self.tableView.mj_footer resetNoMoreData];
            } else {
                [self.tableView.mj_footer endRefreshing];
            }
            [self.tableView reloadData];
        }
    }];
}
- (QJCommentTopFrame *)topicFrameWithTopic:(QJCommentTopModel *)topic {
    QJCommentTopFrame *topicFrame = [[QJCommentTopFrame alloc] init];
    // 传递模型数据，计算所有子控件的frame
    topicFrame.topic = topic;
    
    return topicFrame;
}

- (void)didSelectShowAllTable:(QJCommentTopFrame *)topicFrame isShowAll:(BOOL)isShowAll {
    QJCommentTopModel *topModel = topicFrame.topic;
    topModel.showAll = isShowAll;
    QJCommentTopFrame *topicF = [self topicFrameWithTopic:topModel];
    
    NSMutableArray *arr = [self.commentData mutableCopy];
    NSInteger row = 0;
    for (NSInteger i = 0; i < self.commentData.count; i++) {
        QJCommentTopFrame *model = self.commentData[i];
        if ([model.topic.commentId isEqualToString:topicF.topic.commentId]) {
            arr[i] = topicF;
            row = i;
        }
    }
    self.commentData = arr;
    [UIView performWithoutAnimation:^{
        [self.tableView reloadData];
    }];
}


- (void)didSelectExpandTable:(QJCommentTopFrame *)topicFrame {
    [self getSubCommentDetail:topicFrame];
}

- (void)didSelectReductionTable:(QJCommentTopFrame *)topicFrame {
    QJCommentTopModel *topModel = topicFrame.topic;
    NSMutableArray *comments = topModel.comments;
    
    NSMutableArray *commentArr = [NSMutableArray arrayWithCapacity:0];
    if (comments.count > 2) {
        [commentArr addObject:comments[0]];
        [commentArr addObject:comments[1]];
    }
    topModel.comments = commentArr;
    topModel.isExpand = NO;
    topModel.page = 1;
    QJCommentTopFrame *topicF = [self topicFrameWithTopic:topModel];
    [self changeData:topicF];
}

- (void)getSubCommentDetail:(QJCommentTopFrame *)topicFrame {
    QJCampsiteRequest *startRequest = [[QJCampsiteRequest alloc] init];
    if (self.isNews) {
        [startRequest netWorkGetCommentSub:topicFrame.topic.commentId page:[NSString stringWithFormat:@"%ld", topicFrame.topic.page] newsId:@""];
    } else {
        [startRequest netWorkGetCommentSub:topicFrame.topic.commentId page:[NSString stringWithFormat:@"%ld", topicFrame.topic.page] newsId:self.idStr];
    }
    [startRequest setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            NSArray *dataArr = request.responseJSONObject[@"data"][@"records"];
            NSInteger total = [request.responseJSONObject[@"data"][@"total"] integerValue];
            QJCommentTopModel *topModel = topicFrame.topic;
            NSMutableArray *commentArr = [NSMutableArray arrayWithCapacity:0];
            if (topModel.page > 1) {
                commentArr = topModel.comments;
            }
            topModel.page = topModel.page + 1;
            for (NSDictionary *dic in dataArr) {
                QJCommentReplyModel *model = [QJCommentReplyModel modelWithDictionary:dic];
                [commentArr addObject:model];
            }
            topModel.comments = commentArr;
            topModel.total = total;
            topModel.isExpand = YES;
            QJCommentTopFrame *topicF = [self topicFrameWithTopic:topModel];
            [self changeData:topicF];
        }  else {
            NSString *message = request.responseJSONObject[@"message"];
            [[QJAppTool getCurrentViewController].view makeToast:kCheckStringNil(message)];
        }
    }];
}

- (void)changeData:(QJCommentTopFrame *)topicFrame {
    NSMutableArray *arr = [self.commentData mutableCopy];
    NSInteger row = 0;
    for (NSInteger i = 0; i < self.commentData.count; i++) {
        QJCommentTopFrame *model = self.commentData[i];
        if ([model.topic.commentId isEqualToString:topicFrame.topic.commentId]) {
            arr[i] = topicFrame;
            row = i;
        }
    }
    self.commentData = arr;
    [UIView performWithoutAnimation:^{
        [self.tableView reloadData];
    }];
    

}
- (void)touchReplyMainComment:(NSString *)commentID name:(NSString *)name userId:(NSString *)userId toUser:(nonnull QJCommentUser *)toUser{
    if (self.listDelegate && [self.listDelegate respondsToSelector:@selector(touchReplyMainComment:name:userId:toUser:)]) {
        [self.listDelegate touchReplyMainComment:commentID name:name userId:userId toUser:toUser];
    }
}

#pragma mark - GKPageListViewDelegate
- (UIScrollView *)listScrollView {
    return self.tableView;
}

- (void)listViewDidScrollCallback:(void (^)(UIScrollView * _Nonnull))callback {
    self.listScrollViewDidScroll = callback;
}
#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    !self.listScrollViewDidScroll ? : self.listScrollViewDidScroll(scrollView);
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:QJCommentRefresh object:nil];
}

@end
