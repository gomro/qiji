//
//  QJVideoRateViewController.m
//  QJBox
//
//  Created by Sun on 2022/8/1.
//

#import "QJVideoRateViewController.h"
#import <SJBaseVideoPlayer/SJBaseVideoPlayer.h>
#import <SJVideoPlayer/UIView+SJAnimationAdded.h>

@interface QJVideoRateViewController ()
@property (nonatomic, strong) UIView *rightContainerView;
@property (nonatomic, weak) SJBaseVideoPlayer *player;
@property (nonatomic, copy) NSArray *rateArr;
@end

@implementation QJVideoRateViewController
@synthesize restarted = _restarted;

- (void)viewDidLoad {
    [super viewDidLoad];
    if (self.isVertical) {
        [self makeVerticalView];
    } else {
        [self makeFullView];
    }
}

- (void)setIsVertical:(BOOL)isVertical {
    _isVertical = isVertical;
    if (isVertical) {
        [self makeVerticalView];
    } else {
        [self makeFullView];
    }
}

#pragma mark - 竖屏状态
- (void)makeVerticalView {
    _rightContainerView.sjv_disappearDirection = SJViewDisappearAnimation_Bottom;
    [self.view addSubview:self.rightContainerView];
    CGFloat height = Bottom_iPhoneX_SPACE ? 330 : 312;    
    [_rightContainerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.offset(0);
        make.height.offset(height);
    }];
    self.rateArr = @[@"2.0", @"1.5", @"1.25", @"1.0", @"0.75", @"0.5"];
    for (int i = 0; i < self.rateArr.count; i++) {
        UIButton *sender = [UIButton new];
        sender.tag = 3000 + i;
        [sender addTarget:self action:@selector(clickRate:) forControlEvents:UIControlEventTouchUpInside];
        [sender setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [sender setTitle:[NSString stringWithFormat:@"%@x", self.rateArr[i]] forState:UIControlStateNormal];
        
        CGFloat scale =  (QJScreenWidth > QJScreenHeight ? QJScreenHeight : QJScreenWidth) / 375.0;

        sender.titleLabel.font = [UIFont fontWithName:[NSString stringWithFormat:FONT_REGULAR] size:(16*scale)];
        [self.rightContainerView addSubview:sender];
        [sender mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.rightContainerView.mas_top).offset(18 + 52 * i);
            make.centerX.equalTo(self.rightContainerView);
            make.height.mas_offset(16);
        }];
    }
}

#pragma mark - 横屏状态
- (void)makeFullView {
    _rightContainerView.sjv_disappearDirection = SJViewDisappearAnimation_Right;
    [self.view addSubview:self.rightContainerView];
    [_rightContainerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.right.bottom.offset(0);
        make.width.offset(200);
    }];
    CGRect bounds = UIScreen.mainScreen.bounds;
    CGFloat height = MIN(bounds.size.width, bounds.size.height);
    CGFloat spacing = (height - 32)/ 7.0 - 20;
    self.rateArr = @[@"2.0", @"1.5", @"1.25", @"1.0", @"0.75", @"0.5"];
    for (int i = 0; i < self.rateArr.count; i++) {
        UIButton *sender = [UIButton new];
        sender.tag = 3000 + i;
        [sender addTarget:self action:@selector(clickRate:) forControlEvents:UIControlEventTouchUpInside];
        [sender setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [sender setTitle:[NSString stringWithFormat:@"%@x", self.rateArr[i]] forState:UIControlStateNormal];
        CGFloat scale =  (QJScreenWidth > QJScreenHeight ? QJScreenHeight : QJScreenWidth) / 375.0;

        sender.titleLabel.font = [UIFont fontWithName:[NSString stringWithFormat:FONT_REGULAR] size:(16*scale)];
        [self.rightContainerView addSubview:sender];
        [sender mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.rightContainerView.mas_top).offset(16 + spacing *(i + 1) + 20 * i);
            make.centerX.equalTo(self.rightContainerView);
            make.height.mas_offset(20);
        }];
    }
}

- (void)clickRate:(UIButton *)sender {
    NSInteger tag = sender.tag - 3000;
    if ([self.delegate respondsToSelector:@selector(tappedButtonRate:)] ) {
        [self.delegate tappedButtonRate:self.rateArr[tag]];
    }
    
}

///
/// 控制层入场
///     当播放器将要切换到此控制层时, 该方法将会被调用
///     可以在这里做入场的操作
///
- (void)restartControlLayer {
    _restarted = YES;
    if (self.player.isFullscreen) [self.player needHiddenStatusBar];
    sj_view_makeAppear(self.controlView, YES);
    sj_view_makeAppear(self.rightContainerView, YES);
}


///
/// 退出控制层
///     当播放器将要切换到其他控制层时, 该方法将会被调用
///     可以在这里处理退出控制层的操作
///
- (void)exitControlLayer {
    _restarted = NO;
    
    sj_view_makeDisappear(self.rightContainerView, YES);
    sj_view_makeDisappear(self.controlView, YES, ^{
        if ( !self->_restarted ) [self.controlView removeFromSuperview];
    });
}

///
/// 控制层视图
///     当切换为当前控制层时, 该视图将会被添加到播放器中
///
- (UIView *)controlView {
    return self.view;
}

///
/// 当controlView被添加到播放器时, 该方法将会被调用
///
- (void)installedControlViewToVideoPlayer:(__kindof SJBaseVideoPlayer *)videoPlayer {
    _player = videoPlayer;
    
    if ( self.view.layer.needsLayout ) {
        sj_view_initializes(self.rightContainerView);
    }
    
    sj_view_makeDisappear(self.rightContainerView, NO);
}

///
/// 当调用播放器的controlLayerNeedAppear时, 播放器将会回调该方法
///
- (void)controlLayerNeedAppear:(__kindof SJBaseVideoPlayer *)videoPlayer {}

///
/// 当调用播放器的controlLayerNeedDisappear时, 播放器将会回调该方法
///
- (void)controlLayerNeedDisappear:(__kindof SJBaseVideoPlayer *)videoPlayer {}

///
/// 当将要触发某个手势时, 该方法将会被调用. 返回NO, 将不触发该手势
///
- (BOOL)videoPlayer:(__kindof SJBaseVideoPlayer *)videoPlayer gestureRecognizerShouldTrigger:(SJPlayerGestureType)type location:(CGPoint)location {
    if ( type == SJPlayerGestureType_SingleTap ) {
        if ( !CGRectContainsPoint(self.rightContainerView.frame, location) ) {
            if ( [self.delegate respondsToSelector:@selector(tappedBlankAreaOnTheControlLayer:)] ) {
                [self.delegate tappedBlankAreaOnTheControlLayer:self];
            }
        }
    }
    return NO;
}


///
/// 当将要触发旋转时, 该方法将会被调用. 返回NO, 将不触发旋转
///
- (BOOL)canTriggerRotationOfVideoPlayer:(__kindof SJBaseVideoPlayer *)videoPlayer {
    return NO;
}

//@synthesize rightContainerView = _rightContainerView;
- (UIView *)rightContainerView {
    if ( _rightContainerView == nil ) {
        _rightContainerView = [UIView.alloc initWithFrame:CGRectZero];
        _rightContainerView.backgroundColor = UIColorHex(00000099);
    }
    return _rightContainerView;
}


@end
