//
//  QJCampsiteHomeListViewController.m
//  QJBox
//
//  Created by Sun on 2022/7/6.
//

#import "QJCampsiteHomeListViewController.h"
#import "QJEditCampsiteViewController.h"
#import "GKPageScrollView.h"
#import "QJCampsitePageViewController.h"
#import "QJCampsiteListViewController.h"
#import "QJCampsiteHomeListHeaderView.h"
#import "QJCampsiteRequest.h"
#import "QJCampsiteDescriptionModel.h"
#import "QJCommunityScrollView.h"
#import "QJProductDetailViewController.h"
@interface QJCampsiteHomeListViewController ()<QJCampsitePageViewControllerDelegate, GKPageScrollViewDelegate>
/**联动scrollView*/
@property (nonatomic, strong) QJCommunityScrollView *pageScrollView;
/**顶部视图*/
@property (nonatomic, strong) QJCampsiteHomeListHeaderView *headerView;
/**底部视图*/
@property (nonatomic, strong) QJCampsitePageViewController *pageVC;

/**数据model*/
@property (nonatomic, strong) QJCampsiteDescriptionModel *dataModel;

@property (nonatomic, strong) UIView *pageView;
@property (nonatomic, strong) NSArray *titles;


/**返回顶部*/
@property (nonatomic, strong) UIButton *topUpBt;
@end
@implementation QJCampsiteHomeListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.view.backgroundColor = UIColorHex(f8f8f8);
    self.view.backgroundColor = [UIColor clearColor];

    [self.view addSubview:self.pageScrollView];
    [self.pageScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.bottom.equalTo(self.view);
    }];
    [self.pageScrollView addSubview:self.headerView];
    [self.pageScrollView sendSubviewToBack:self.headerView];

    self.titles = @[@"推荐"];
    self.childVCs = @[[QJCampsiteListViewController new]];
    [self.pageVC configTitles:self.titles childVCs:self.childVCs];
    self.pageVC.campsiteID = self.campsiteID;
    [self.pageScrollView reloadData];
    [self getCampsiteDescription];
    
    [self.view addSubview:self.topUpBt];
    [self.topUpBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.view.mas_right).offset(-15);
        make.bottom.equalTo(self.view.mas_bottom).offset(-100);
        make.width.height.mas_equalTo(70);
    }];
    
    UITapGestureRecognizer *tapView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapHeaderView)];
    [self.headerView addGestureRecognizer:tapView];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshPage) name:QJMyCampsitePageRefresh object:nil];

}

- (void)changeBackViewImage {
    NSString *image = kCheckStringNil(self.dataModel.bgImage);
    [[NSNotificationCenter defaultCenter] postNotificationName:QJCampsiteBackImage object:nil userInfo:@{@"image":image}];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

#pragma mark - GKPageScrollViewDelegate
- (UIView *)headerViewInPageScrollView:(GKPageScrollView *)pageScrollView {
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 0)];
    
    return header;
}

- (UIView *)pageViewInPageScrollView:(GKPageScrollView *)pageScrollView {
    return self.pageView;
}

- (NSArray<id<GKPageListViewDelegate>> *)listViewsInPageScrollView:(GKPageScrollView *)pageScrollView {
    return self.childVCs;
}

- (void)mainTableViewDidScroll:(UIScrollView *)scrollView isMainCanScroll:(BOOL)isMainCanScroll {
    CGFloat offsetY = scrollView.contentOffset.y + 134*kWScale;
    if (!isMainCanScroll && offsetY >= 134*kWScale) {
        self.topUpBt.hidden = NO;
    } else {
        self.topUpBt.hidden = YES;
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"kScrollContentOffsetNotification" object:nil userInfo:@{@"offsetY": @(offsetY)}];
}


#pragma mark - GKWBPageViewControllDelegate
- (void)pageScrollViewWillBeginScroll {
    [self.pageScrollView horizonScrollViewWillBeginScroll];
}

- (void)pageScrollViewDidEndedScroll {
    [self.pageScrollView horizonScrollViewDidEndedScroll];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    self.pageScrollView.ceilPointHeight = 0;
}

#pragma mark - 懒加载
- (QJCommunityScrollView *)pageScrollView {
    if (!_pageScrollView) {
        _pageScrollView = [[QJCommunityScrollView alloc] initWithDelegate:self];
        _pageScrollView.mainTableView.backgroundColor = [UIColor clearColor];
        _pageScrollView.isControlVerticalIndicator = YES;
        _pageScrollView.isAllowListRefresh = YES;
        self.pageScrollView.mainTableView.isAllowCustomHit = YES;
        NSInteger insetTopInt = 134*kWScale;
        self.pageScrollView.contentInsetTop = insetTopInt;
    }
    return _pageScrollView;
}

- (QJCampsiteHomeListHeaderView *)headerView {
    if (!_headerView) {
        _headerView = [[QJCampsiteHomeListHeaderView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 134*kWScale)];
    }
    return _headerView;
}

- (QJCampsitePageViewController *)pageVC {
    if (!_pageVC) {
        self.pageVC = [[QJCampsitePageViewController alloc] init];
        self.pageVC.scrollDelegate = self;
    }
    return _pageVC;
}

- (UIView *)pageView {
    if (!_pageView) {
        [self addChildViewController:self.pageVC];
        [self.pageVC didMoveToParentViewController:self];
        _pageView = self.pageVC.view;
    }
    return _pageView;
}

- (UIButton *)topUpBt {
    if (!_topUpBt) {
        _topUpBt = [UIButton new];
        [_topUpBt setBackgroundImage:[UIImage imageNamed:@"topUpIcon"] forState:UIControlStateNormal];
        [_topUpBt addTarget:self action:@selector(topUpView:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _topUpBt;
}

- (void)topUpView:(UIButton *)sender {
    [self.pageScrollView scrollToOriginalPoint];
}

#pragma mark - NetWork
- (void)getCampsiteDescription {
    QJCampsiteRequest *startRequest = [[QJCampsiteRequest alloc] init];
    [startRequest netWorkGetCampDescription:self.campsiteID];
    [startRequest setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            QJCampsiteDescriptionModel *model = [QJCampsiteDescriptionModel modelWithDictionary:request.responseJSONObject[@"data"]];
            self.dataModel = model;
            [self.headerView configureHeaderDetailWithData:model];
        }
    }];
}


#pragma mark - 点击事件
- (void)tapHeaderView {
    QJProductDetailViewController *detailVC = [[QJProductDetailViewController alloc] init];
    detailVC.idStr = kCheckStringNil(self.dataModel.gameId);
    [[QJAppTool getCurrentViewController].navigationController pushViewController:detailVC animated:YES];
}

- (void)refreshPage {
    [self getCampsiteDescription];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:QJMyCampsitePageRefresh object:nil];
}
@end
