//
//  QJCampsiteHomeListViewController.h
//  QJBox
//
//  Created by Sun on 2022/7/6.
//

#import "QJListViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJCampsiteHomeListViewController : QJListViewController

@property (nonatomic, copy) NSString *campsiteID;
- (void)changeBackViewImage;
@property (nonatomic, strong) NSArray *childVCs;
@end

NS_ASSUME_NONNULL_END
