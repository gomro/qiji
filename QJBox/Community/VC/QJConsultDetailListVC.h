//
//  QJConsultDetailListVC.h
//  QJBox
//
//  Created by Sun on 2022/10/9.
//
#import "QJListViewController.h"
#import "GKPageScrollView.h"
#import "QJCommentUser.h"
NS_ASSUME_NONNULL_BEGIN

@protocol QJConsultDetailListVCDelegate <NSObject>

- (void)touchReplyMainComment:(NSString *)commentID name:(NSString *)name userId:(NSString *)userId toUser:(QJCommentUser *)toUser;

@end

@interface QJConsultDetailListVC : QJListViewController<GKPageListViewDelegate>
/**动态、资讯id**/
@property (nonatomic, copy) NSString *idStr;

/**是否是资讯*/
@property (nonatomic, assign) BOOL isNews;

/**评论排序**/
@property (nonatomic, assign) BOOL commentDesc;

/**判断是否是个人*/
@property (nonatomic, assign) BOOL isMySelf;

@property (nonatomic, weak) id<QJConsultDetailListVCDelegate> listDelegate;

@end

NS_ASSUME_NONNULL_END
