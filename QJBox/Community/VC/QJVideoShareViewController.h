//
//  QJVideoShareViewController.h
//  QJBox
//
//  Created by Sun on 2022/8/1.
//

#import <UIKit/UIKit.h>
#import <SJVideoPlayer/SJControlLayerDefines.h>
#import "QJCampsiteDetailModel.h"
NS_ASSUME_NONNULL_BEGIN
@protocol QJVideoShareViewControllerDelegate <NSObject>
///
/// 点击关闭
///
- (void)closeView:(id<SJControlLayer>)controlLayer;

- (void)clickShareItemTag:(NSInteger)tag;

@end
@interface QJVideoShareViewController : UIViewController<SJControlLayer>
@property (nonatomic, weak, nullable) id<QJVideoShareViewControllerDelegate> delegate;

/**是否是竖屏样式*/
@property (nonatomic, assign) BOOL isVertical;
- (void)configChangeSaveIsMySelf:(BOOL)isMySelf isSave:(BOOL)isSave isNews:(BOOL)isNews newsID:(NSString *)newsID;

@end

NS_ASSUME_NONNULL_END
