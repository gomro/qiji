//
//  QJNewsSearchViewController.m
//  QJBox
//
//  Created by macm on 2022/7/27.
//

#import "QJNewsSearchViewController.h"
/* 默认view */
#import "QJHomeSearchContentDefaultView.h"
/* 请求 */
#import "QJCampsiteRequest.h"
#import "QJNewsSearchTypeView.h"
#import "QJImageButton.h"
#import "QJNewsSearchResultView.h"

@interface QJNewsSearchViewController ()<UITextFieldDelegate>

/* 自定义导航View */
@property (nonatomic, strong) UIView *navView;
/* 搜索 */
@property (nonatomic, strong) UIView *searchView;
@property (nonatomic, strong) UITextField *searchTF;
@property (nonatomic, strong) UIButton *cancelBtn;
/* 搜索key */
@property (nonatomic, copy) NSString *searchKey;

/* mornview */
@property (nonatomic, strong) QJHomeSearchContentDefaultView *searchDeaultView;

@property (nonatomic, strong) QJSearchSortModel *searchHotKeyModel;
/* 搜索右侧选择方框 */
@property (nonatomic, strong) QJNewsSearchTypeView *typeChoose;
/* 搜索右侧选择按钮 */
@property (nonatomic, strong) QJImageButton *timeBt;
/* 搜索结果页面 */
@property (nonatomic, strong) QJNewsSearchResultView *resultView;

@end

@implementation QJNewsSearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navBar.hidden = YES;
    self.searchHotKeyModel = [QJSearchSortModel new];
    /* 初始化导航 */
    [self initNav];
    /* 初始化UI */
    [self initUI];
    /* 加载搜索记录 */
    [self sendRequest];
    [self initHistoryData];
    [self.searchTF becomeFirstResponder];
}

- (void)initUI{
    [self.view addSubview:self.searchDeaultView];
    
    /* 默认页面-首页事件回调 */
    WS(weakSelf)
    _searchDeaultView.scrollViewWillBeginDraggingBlock = ^{
        [weakSelf.searchTF resignFirstResponder];
    };
    _searchDeaultView.cellSelectBlock = ^(id  _Nonnull item) {
        QJSearchTagDetailModel *model = (QJSearchTagDetailModel *)item;
        weakSelf.searchTF.text = model.name;
        [weakSelf searchAction];
    };
    
    [self.view addSubview:self.resultView];
    [self.resultView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.navView.mas_bottom);
        make.left.right.bottom.mas_equalTo(0);
    }];
    
    [self.view addSubview:self.typeChoose];
    [self.typeChoose mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.timeBt);
        make.height.mas_equalTo(110*kWScale);
        make.width.mas_equalTo(90*kWScale);
        make.centerX.mas_equalTo(self.timeBt);
    }];
    self.typeChoose.clickButton = ^(NSString *titleString) {
        weakSelf.timeBt.titleString = titleString;
        if (self.searchTF.text.length > 0) {
            [weakSelf searchAction];
        }
        
    };

}

- (void)initNav{
    self.view.backgroundColor = UIColorFromRGB(0xFFFFFF);
    self.navView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, QJScreenWidth, NavigationBar_Bottom_Y)];
    self.navView.backgroundColor = UIColorFromRGB(0xFFFFFF);
    [self.view addSubview:self.navView];
    
    [self.navView addSubview:self.searchView];
    [self.searchView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.mas_equalTo(0);
        make.height.mas_equalTo(44*kWScale);
    }];
    
    /* searchView */
    self.searchTF = [[UITextField alloc] initWithFrame:CGRectMake(15, NavigationBar_Bottom_Y+15, QJScreenWidth-30, 32)];
    [self.searchView addSubview:self.searchTF];
    [self.searchTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.searchView).offset(15);
        make.top.equalTo(self.searchView).offset(5);
        make.bottom.equalTo(self.searchView).offset(-5);
        make.right.equalTo(self.searchView).offset(-170);
//        make.height.mas_equalTo(30);
    }];
    self.searchTF.backgroundColor = [UIColor colorWithRed:0.842 green:0.842 blue:0.842 alpha:0.19];
    self.searchTF.textColor = UIColorFromRGB(0x474849);
    self.searchTF.font = kFont(14);
    self.searchTF.layer.cornerRadius = 15;
    self.searchTF.returnKeyType = UIReturnKeySearch;

    NSMutableAttributedString *placeholderString = [[NSMutableAttributedString alloc] initWithString:@"输入关键词搜索创作" attributes:@{NSForegroundColorAttributeName : UIColorFromRGB(0xC8CACC), NSFontAttributeName : kFont(14)}];
    self.searchTF.attributedPlaceholder = placeholderString;

    UIView *searchView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 34, 30)];
    UIImageView *searchImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_search"]];
    searchImage.frame = CGRectMake(14, 7, 16, 16);
    [searchView addSubview:searchImage];
    self.searchTF.leftView = searchView;
    self.searchTF.leftViewMode = UITextFieldViewModeAlways;
    self.searchTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.searchTF.delegate = self;
    [self.searchTF addTarget:self action:@selector(textFeildChange:) forControlEvents:UIControlEventEditingChanged];

    self.cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.searchView addSubview:self.cancelBtn];
    [self.cancelBtn setTitle:@"取消" forState:UIControlStateNormal];
    self.cancelBtn.titleLabel.font = kFont(14);
    [self.cancelBtn setTitleColor:UIColorFromRGB(0x101010) forState:UIControlStateNormal];
    [self.cancelBtn addTarget:self action:@selector(backBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [self.cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-10);
        make.bottom.equalTo(self.searchView).offset(-6);
        make.height.mas_equalTo(30*kWScale);
        make.width.mas_equalTo(40*kWScale);
    }];
    
    [self.searchView addSubview:self.timeBt];
    [self.timeBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.cancelBtn.mas_left).offset(-20);
        make.width.mas_equalTo(70*kWScale);
        make.centerY.equalTo(self.cancelBtn);
    }];
    
}

- (void)timeChoose:(UIButton *)btn {
    [self.typeChoose setHidden:NO];
}

- (void)backBtnAction{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -- UITextFieldDelegate

// 值有变化
- (void)textFeildChange:(UITextField *)textFeild {
    if (textFeild.text.length == 0) {
        [self.resultView resetSearchSort];
    }
    if (textFeild.markedTextRange == nil) {
        NSString *toBeString = textFeild.text;
        NSInteger kMaxLength = 10;
        if (toBeString.length > kMaxLength) {
            textFeild.text = [toBeString substringToIndex:kMaxLength];
        }
        if (self.searchKey.hash == textFeild.text.hash) {
            return;
        }
        self.searchKey = textFeild.text;
        
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    NSString * text = textField.text;
    self.searchKey = textField.text;
    NSCharacterSet *set = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    NSString *trimedString = [text stringByTrimmingCharactersInSet:set];
    if ([trimedString length] == 0) {
        [self.view makeToast:@"请输入内容"];
        return YES;
    }

    NSArray *historyArray = [[NSUserDefaults standardUserDefaults] valueForKey:@"newsHistorySearch"];
    NSMutableArray *historyArrayNew = [NSMutableArray array];
    if (!historyArray) {
        NSMutableArray *listArray = [NSMutableArray array];
        [listArray safeAddObject:text];
        historyArrayNew = listArray;
    }else{
        NSMutableArray *listArray = [historyArray mutableCopy];
        if (listArray.count > 10) {
            [listArray removeFirstObject];
        }
        [listArray safeAddObject:text];
        for (NSString *text in listArray) {
            if (![historyArrayNew containsObject:text]) {
                [historyArrayNew safeAddObject:text];
            }
        }
    }
    
    [[NSUserDefaults standardUserDefaults] setValue:historyArrayNew forKey:@"newsHistorySearch"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self searchAction];
    return YES;
}


// 开始搜索
- (void)searchAction {
    // 显示搜索结果
    [self.resultView setHidden:NO];
    [self.resultView.listView reloadListViewKeywordType:self.timeBt.titleString keyword:self.searchTF.text categoryCode:self.resultView.categoryCode sort:self.resultView.sortString];
    [self.searchTF resignFirstResponder];
}
 
#pragma mark -- Request

- (void)initHistoryData{
    NSMutableArray *modelHeaderArray = [NSMutableArray array];
    NSMutableArray *historyArray = [[NSUserDefaults standardUserDefaults] valueForKey:@"newsHistorySearch"];
    if (historyArray && historyArray.count > 0) {
        QJSearchTagDetailModel *model = [QJSearchTagDetailModel new];
        model.name = @"最近搜索";
        model.multiple = YES;
        model.list = [NSMutableArray array];
        NSMutableArray *listArray = [historyArray mutableCopy];
        for (NSString *text in listArray) {
            QJSearchTagDetailModel *listModel = [QJSearchTagDetailModel new];
            listModel.name = text;
            [model.list safeAddObject:listModel];
        }
        [modelHeaderArray safeAddObject:model];
    }
        
    QJSearchSortModel *dataHeaderModel = [QJSearchSortModel new];
    dataHeaderModel.type = 1;
    dataHeaderModel.types = modelHeaderArray;
    [_searchDeaultView showFilterHistoryWithModel:dataHeaderModel];
}

- (void)sendRequest{
    WS(weakSelf)
    QJCampsiteRequest *startRequest = [[QJCampsiteRequest alloc] init];
    [startRequest netWorkGetHotWordsSearchType:@"1"];
    [startRequest setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            if ([EncodeArrayFromDic(request.responseObject, @"data") isKindOfClass:[NSArray class]]) {
                weakSelf.searchHotKeyModel.data = [NSArray modelArrayWithClass:[QJSearchTagDetailModel class] json:EncodeArrayFromDic(request.responseObject, @"data")].mutableCopy;
            }
        }else{
            NSString *msg = EncodeStringFromDic(request.responseObject, @"message");
            [weakSelf.view makeToast:msg];
        }
        
        [weakSelf resetHotKeyModel];
    }];
}

- (void)resetHotKeyModel{
    QJSearchTagDetailModel *listModel = [QJSearchTagDetailModel new];
    listModel.name = @"热门搜索";
    listModel.multiple = YES;
    listModel.list = [NSMutableArray array];
    for (QJSearchTagDetailModel *model in self.searchHotKeyModel.data) {
        QJSearchTagDetailModel *detailModel = [QJSearchTagDetailModel new];
        detailModel.name = model.keyword;
        [listModel.list safeAddObject:detailModel];
    }
    
    NSMutableArray *modelArray = [NSMutableArray array];
    [modelArray safeAddObject:listModel];
    QJSearchSortModel *dataHeaderModel = [QJSearchSortModel new];
    dataHeaderModel.types = modelArray;
    [_searchDeaultView showFilterWithModel:dataHeaderModel];
}

#pragma mark -------- Lazy Loading

-(QJHomeSearchContentDefaultView *)searchDeaultView{
    if (!_searchDeaultView) {
        _searchDeaultView = [[QJHomeSearchContentDefaultView alloc] initWithFrame:CGRectMake(0, NavigationBar_Bottom_Y, QJScreenWidth, QJScreenHeight - (NavigationBar_Bottom_Y))];
    }
    return _searchDeaultView;
}

- (UIView *)searchView{
    if (!_searchView) {
        _searchView = [[UIView alloc] init];
        _searchView.backgroundColor = [UIColor whiteColor];
    }
    return _searchView;
}

- (QJNewsSearchTypeView *)typeChoose {
    if (!_typeChoose) {
        _typeChoose = [QJNewsSearchTypeView new];
        _typeChoose.titleArray = @[@"搜标题", @"搜游戏"];
        _typeChoose.hidden = YES;
    }
    return _typeChoose;
}

- (QJImageButton *)timeBt {
    if (!_timeBt) {
        _timeBt = [QJImageButton new];
        _timeBt.titleString = @"搜标题";
        _timeBt.titleLb.font = MYFONTALL(FONT_REGULAR, 14);
        _timeBt.titleLb.textColor = UIColorHex(474849);
        _timeBt.iconString = @"iconDownOne";
        _timeBt.space = 3;
        _timeBt.size = CGSizeMake(16, 16);
        _timeBt.style = ImageButtonStyleRight;
        [_timeBt addTarget:self action:@selector(timeChoose:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _timeBt;
}

- (QJNewsSearchResultView *)resultView {
    if (!_resultView) {
        _resultView = [[QJNewsSearchResultView alloc] init];
        [_resultView setHidden:YES];
        @weakify(self);
        _resultView.didSelectedSort = ^{
            @strongify(self);
            [self searchAction];
        };
    }
    return _resultView;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
