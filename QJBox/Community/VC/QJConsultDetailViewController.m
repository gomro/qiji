//
//  QJConsultDetailViewController.m
//  QJBox
//
//  Created by Sun on 2022/7/11.
//

#import "QJConsultDetailViewController.h"
#import "JXCategoryView.h"
#import "QJConsultDetailHeaderView.h"
#import "QJConsultDetailBottomView.h"
#import "QJCommentTopModel.h"
#import "QJCommentUser.h"
#import "QJCommentReplyModel.h"
#import "QJCommentTopFrame.h"
#import "QJConsultCommentViewCell.h"
#import "QJCampsiteRequest.h"
#import "QJCampsiteDetailModel.h"
#import "QJCommentInputKit.h"
#import "QJMineMsgDeleteView.h"
#import "QJDetailShareView.h"
#import "QJDetailAlertView.h"
#import "TYAlertController.h"
#import "QJReportViewController.h"
#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>
#import <SJVideoPlayer/SJVideoPlayer.h>
#import "QJVideoRateViewController.h"
#import "QJVideoPlayerView.h"
#import "QJVideoShareViewController.h"
#import "QJNewsListModel.h"
#import "QJConsultDetailWebViewCell.h"
#import "QJConsultDetailFooterView.h"
#import "QJMySpaceViewController.h"
#import "QJDraftEmptyView.h"
#import "GKPageScrollView.h"

#import "QJConsultDetailPageVC.h"
#import "QJConsultDetailListVC.h"
#import "QJDetailShareModel.h"

static SJControlLayerIdentifier SJDetailControlLayerIdentifier = 101;
static SJControlLayerIdentifier SJDetailControlLayerVerticalIdentifier = 102;
static SJControlLayerIdentifier SJDetailControlShareIdentifier = 103;
static SJControlLayerIdentifier SJDetailControlShareVerticalIdentifier = 104;

@interface QJConsultDetailViewController ()<JXCategoryViewDelegate, QJConsultDetailHeaderViewDelegate, SJControlLayerSwitcherDelegate, QJVideoRateViewControllerDelegate, QJVideoShareViewControllerDelegate, QJConsultDetailWebViewCellDelegate, QQApiManagerDelegate, GKPageScrollViewDelegate, QJConsultDetailPageVCDelegate, WXApiManagerDelegate>


/**联动scrollView*/
@property (nonatomic, strong) GKPageScrollView *pageScrollView;
/**顶部视图*/
@property (nonatomic, strong) QJConsultDetailHeaderView *headerView;
/**底部视图*/
@property (nonatomic, strong) QJConsultDetailPageVC *pageVC;

@property (nonatomic, strong) UIView *pageView;

/**详情*/
@property (nonatomic, strong) NSMutableArray *dataArr;

@property (nonatomic, strong) QJConsultDetailBottomView *bottomView;

@property (nonatomic, strong) JXCategoryTitleView *myCategoryView;

@property (nonatomic , copy) NSString *textString;
@property (nonatomic , strong) NSMutableArray *users;

@property (nonatomic, strong) QJCampsiteDetailModel *dataModel;

@property (nonatomic, strong) QJNewsListModel *dataNewsModel;

/**更多**/
@property (nonatomic, strong) UIButton *moreBt;

/**判断是否是个人*/
@property (nonatomic, assign) BOOL isMySelf;

/**视频*/
@property (nonatomic, strong, nullable) QJVideoPlayerView *player;
@property (nonatomic, assign) NSInteger page;

@property (nonatomic, assign) CGFloat webHeight;


/**评论排序**/
@property (nonatomic, assign) BOOL commentDesc;

/**视频分享视图**/
@property (nonatomic, strong) QJVideoShareViewController *videoShareView;

/**滑动位置**/
@property (nonatomic, assign) CGFloat lastSectionHeight;

@property (nonatomic, strong) QJConsultDetailListVC *detailListVC;
@property (nonatomic, strong) NSArray *childVCs;

@property (nonatomic, strong) UIImage *thumbImage;

@end

@implementation QJConsultDetailViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    self.isMySelf = NO;
    self.webHeight = 0;
    self.lastSectionHeight = 0;
    self.commentDesc = NO;
    [self setNavBarHidden:YES];
    [self loadUI];
    self.pageVC.idStr = self.idStr;
    self.pageVC.isNews = self.isNews;
    
    self.childVCs = @[[QJConsultDetailListVC new]];
    [self.pageVC configTitles:@[@"推荐"] childVCs:self.childVCs];
    [self.pageScrollView reloadData];

    [self getCampsiteDetail];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadBottomLike:) name:QJCampsiteClickLikeType object:nil];

}

#pragma mark - Load UI
- (void)loadUI {
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.pageScrollView];
    [self.view addSubview:self.myCategoryView];
    [self.view addSubview:self.bottomView];
    MJWeakSelf
    self.bottomView.clickButton = ^(NSInteger type) {
        if(type == 1) {
            [weakSelf showKeyBoardName:@"我也说一句" replyCommentId:@"" toUser:nil];
        } else {
            [weakSelf reloadVideoData];
        }
    };
    
    [self makeConstraints];
    
    self.myCategoryView.titleColor = [UIColor blackColor];
    self.myCategoryView.titleSelectedColor =  [UIColor blackColor];
    self.myCategoryView.titleFont = MYFONTALL(FONT_REGULAR, 16);
    self.myCategoryView.titleSelectedFont =  MYFONTALL(FONT_BOLD, 10);
    self.myCategoryView.titleLabelZoomEnabled = YES;
    self.myCategoryView.titleLabelZoomScale = 1.1;
    self.myCategoryView.titleLabelStrokeWidthEnabled = YES;
    self.myCategoryView.selectedAnimationEnabled = YES;
    self.myCategoryView.cellWidthZoomEnabled = YES;
    self.myCategoryView.cellWidthZoomScale = 1.3;
    self.myCategoryView.titles = @[@"正文", @"评论"];
    self.myCategoryView.delegate = self;
    self.myCategoryView.defaultSelectedIndex = 0;
    UIButton *backBt = [UIButton new];
    [backBt setImage:[UIImage imageNamed:@"backimage_black"] forState:UIControlStateNormal];
    [backBt addTarget:self action:@selector(popView:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backBt];
    [backBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(30);
        make.height.mas_equalTo(30);
        make.centerY.equalTo(self.myCategoryView);
        make.left.equalTo(self.view).offset(16);
    }];
    
    UIButton *moreBt = [UIButton new];
    [moreBt setBackgroundImage:[UIImage imageNamed:@"moreDetailIcon"] forState:UIControlStateNormal];
    [moreBt setBackgroundImage:[UIImage imageNamed:@"moreDetailIcon"] forState:UIControlStateHighlighted];
    [moreBt addTarget:self action:@selector(moreView:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:moreBt];
    [moreBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(24);
        make.height.mas_equalTo(24);
        make.centerY.equalTo(self.myCategoryView);
        make.right.equalTo(self.view.mas_right).offset(-32);
    }];
    self.moreBt = moreBt;
}

- (void)makeConstraints {
    [self.myCategoryView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(200*kWScale);
        make.height.mas_equalTo(44*kWScale);
        make.centerX.equalTo(self.view);
        make.top.equalTo(self.view).offset(Top_iPhoneX_SPACE + 20);
    }];
    
    [self.pageScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.bottomView.mas_top);
        make.top.equalTo(self.myCategoryView.mas_bottom);
    }];
    
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view.mas_bottom).offset(-Bottom_iPhoneX_SPACE);
        make.height.mas_equalTo(56*kWScale);
    }];
}

#pragma mark - Lazy Loading


- (NSMutableArray *)dataArr {
    if (!_dataArr) {
        self.dataArr = [NSMutableArray arrayWithCapacity:0];
    }
    return _dataArr;
}

- (JXCategoryTitleView *)myCategoryView {
    if (!_myCategoryView) {
        _myCategoryView = [[JXCategoryTitleView alloc] init];
    }
    return _myCategoryView;
}

- (QJConsultDetailBottomView *)bottomView {
    if (!_bottomView) {
        _bottomView = [[QJConsultDetailBottomView alloc] init];
    }
    return _bottomView;
}

#pragma mark - 返回
- (void)popView:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - 更多
- (void)moreView:(UIButton *)sender {
    QJDetailShareView *shareView = [[QJDetailShareView alloc] init];
    if (self.isNews) {
        [shareView configChangeSaveIsMySelf:self.isMySelf isSave:self.dataNewsModel.collected isNews:self.isNews newsID:self.dataNewsModel.infoId];
    } else {
        [shareView configChangeSaveIsMySelf:self.isMySelf isSave:self.dataModel.collected isNews:self.isNews newsID:self.dataModel.newsId];
    }
    MJWeakSelf
    shareView.clickButton = ^(QJDetailShareViewStyle headerType) {
        switch (headerType) {
            case QJDetailShareViewStyleQQ:
                [weakSelf shareQQWithType:0];
                break;
            case QJDetailShareViewStyleQQZone:
                [weakSelf shareQQWithType:1];
                break;
            case QJDetailShareViewStyleWeChat:
                [weakSelf shareWXWithType:0];
                break;
            case QJDetailShareViewStyleWeChatCircle:
                [weakSelf shareWXWithType:1];
                break;
            case QJDetailShareViewStyleSave:
                [weakSelf reloadBottomSave:YES];
                break;
            case QJDetailShareViewStyleCancleSave:
                [weakSelf reloadBottomSave:NO];
                break;
            case QJDetailShareViewStyleReport:
                [weakSelf pushReportView];
                break;
            case QJDetailShareViewStyleDelete:
                [weakSelf showDeleteView];
                break;
            default:
                break;
        }
    };
    [shareView showView];
}

- (void)reloadBottomSave:(BOOL)isSave {
    
    if (self.isNews) {
        NSInteger newsCollectCount = self.dataNewsModel.infoCollectCount;
        if (isSave) {
            newsCollectCount = newsCollectCount + 1;
            self.dataNewsModel.collected = YES;
        } else {
            newsCollectCount = newsCollectCount - 1;
            self.dataNewsModel.collected = NO;
        }
        self.dataNewsModel.infoCollectCount = newsCollectCount;
        [self.bottomView configureDetailBottomWithNewsData:self.dataNewsModel];

    } else {
        NSInteger newsCollectCount = [self.dataModel.newsCollectCount integerValue];
        if (isSave) {
            newsCollectCount = newsCollectCount + 1;
            self.dataModel.collected = YES;
        } else {
            newsCollectCount = newsCollectCount - 1;
            self.dataModel.collected = NO;
        }
        self.dataModel.newsCollectCount = [NSString stringWithFormat:@"%ld", newsCollectCount];
        [self.bottomView configureDetailBottomWithData:self.dataModel];
    }
}
#pragma mark - 分享

- (void)shareQQWithType:(int)type {
    QJQQAPIMANAGER.delegate = self;
    if (self.isNews) {
        NSString *imageStr = [kCheckStringNil(self.dataNewsModel.shareInfo.picture) checkImageUrlString];
        [QJQQAPIMANAGER QQShareWithTitle:kCheckStringNil(self.dataNewsModel.shareInfo.title) description:kCheckStringNil(self.dataNewsModel.shareInfo.summary) imageUrl:imageStr url:kCheckStringNil(self.dataNewsModel.shareInfo.shareUrl) scene:type];
    } else {
        NSString *imageStr = [kCheckStringNil(self.dataModel.shareInfo.picture) checkImageUrlString];
        [QJQQAPIMANAGER QQShareWithTitle:kCheckStringNil(self.dataModel.shareInfo.title) description:kCheckStringNil(self.dataModel.shareInfo.summary) imageUrl:imageStr url:kCheckStringNil(self.dataModel.shareInfo.shareUrl) scene:type];
    }

}

- (void)shareWXWithType:(int)type {
    QJWXMANAGER.delegate = self;
    if (self.isNews) {
        UIImage *image =  [UIImage imageNamed:@""];
        if(self.thumbImage) {
            image = [self compressImage:self.thumbImage toByte:64 * 1024];
        }
        [QJWXMANAGER WXShareWithTitle:kCheckStringNil(self.dataNewsModel.shareInfo.title) description:kCheckStringNil(self.dataNewsModel.shareInfo.summary) image:image url:kCheckStringNil(self.dataNewsModel.shareInfo.shareUrl) scene:type];
    } else {
        UIImage *image =  [UIImage imageNamed:@""];
        if(self.thumbImage) {
            image = [self compressImage:self.thumbImage toByte:64 * 1024];
        }
        [QJWXMANAGER WXShareWithTitle:kCheckStringNil(self.dataModel.shareInfo.title) description:kCheckStringNil(self.dataModel.shareInfo.summary) image:image url:kCheckStringNil(self.dataModel.shareInfo.shareUrl) scene:type];

    }

}

- (void)getImageFromURL:(NSString *)fileURL {
    YYWebImageManager *manager = [YYWebImageManager sharedManager];
    if([kCheckStringNil(fileURL) isEqualToString:@""]) {
        return;
    }
    
    [manager requestImageWithURL:[NSURL URLWithString:[fileURL checkImageUrlString]] options:YYWebImageOptionAllowBackgroundTask progress:nil transform:nil completion:^(UIImage * _Nullable image, NSURL * _Nonnull url, YYWebImageFromType from, YYWebImageStage stage, NSError * _Nullable error) {
        if (image) {
            self.thumbImage = image;
        }
    }];

}

#pragma mark - 压缩图片
- (UIImage *)compressImage:(UIImage *)image toByte:(NSUInteger)maxLength {
    // Compress by quality
    CGFloat compression = 1;
    NSData *data = UIImageJPEGRepresentation(image, compression);
    if (data.length < maxLength) return image;
    
    CGFloat max = 1;
    CGFloat min = 0;
    for (int i = 0; i < 6; ++i) {
        compression = (max + min) / 2;
        data = UIImageJPEGRepresentation(image, compression);
        if (data.length < maxLength * 0.9) {
            min = compression;
        } else if (data.length > maxLength) {
            max = compression;
        } else {
            break;
        }
    }
    UIImage *resultImage = [UIImage imageWithData:data];
    if (data.length < maxLength) return resultImage;
    
    // Compress by size
    NSUInteger lastDataLength = 0;
    while (data.length > maxLength && data.length != lastDataLength) {
        lastDataLength = data.length;
        CGFloat ratio = (CGFloat)maxLength / data.length;
        CGSize size = CGSizeMake((NSUInteger)(resultImage.size.width * sqrtf(ratio)),
                                 (NSUInteger)(resultImage.size.height * sqrtf(ratio))); // Use NSUInteger to prevent white blank
        UIGraphicsBeginImageContext(size);
        [resultImage drawInRect:CGRectMake(0, 0, size.width, size.height)];
        resultImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        data = UIImageJPEGRepresentation(resultImage, compression);
    }
    
    return resultImage;
}

- (void)managerDidRecvMessageResponse:(SendMessageToWXResp *)response {
    if (response.errCode == 0) {
        [[UIApplication sharedApplication].keyWindow makeToast:@"分享成功"];
    }else{
        [[UIApplication sharedApplication].keyWindow makeToast:@"分享失败"];
    }
}

- (void)managerDidRecvQQResponse:(QQBaseResp *)resp {
    if ([resp.result integerValue] == 0) {
        [[UIApplication sharedApplication].keyWindow makeToast:@"分享成功"];
    } else {
        [[UIApplication sharedApplication].keyWindow makeToast:@"分享失败"];
    }
}

#pragma mark - 举报

- (void)pushReportView {
    QJReportViewController *report = [[QJReportViewController alloc] init];
    report.newsID = self.idStr;
    report.isNews = self.isNews;
    [self.navigationController pushViewController:report animated:YES];
}

#pragma mark - 删除
- (void)showDeleteView {
    QJDetailAlertView *alertView = [[QJDetailAlertView alloc] initWithFrame:CGRectZero titleType:QJDetailAlertViewTitleTypeDefault buttonType:QJDetailAlertViewButtonTypeDefault];
    [alertView showTitleText:@"提示" describeText:@"该文章删除后不可恢复,是否删除?"];
    [alertView.leftButton setTitle:@"取消" forState:UIControlStateNormal];
    [alertView.rightButton setTitle:@"删除" forState:UIControlStateNormal];
    MJWeakSelf
    [alertView returnClick:^(QJDetailAlertViewButtonClickType type) {
        [weakSelf dismissViewControllerAnimated:YES completion:nil];
        [weakSelf deleteCampNews];
    }];
    TYAlertController *alertController = [TYAlertController alertControllerWithAlertView:alertView preferredStyle:TYAlertControllerStyleAlert];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)deleteCampNews {
    [QJAppTool showHUDLoading];
    QJCampsiteRequest *startRequest = [[QJCampsiteRequest alloc] init];
    if (self.isNews) {
        [startRequest netWorkDeleteInfoWithId:self.idStr];
    } else {
        [startRequest netWorkDeleteCampNews:self.idStr];
    }
    [startRequest setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        [QJAppTool hideHUDLoading];
        if (ResponseSuccess) {
            [self.navigationController popViewControllerAnimated:YES];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.view makeToast:@"删除成功"];
            });
        }
    }];
    [startRequest setFailureCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        [QJAppTool hideHUDLoading];
    }];
}

- (void)changeWebViewHeight:(CGFloat)height {
    self.webHeight = height;
    [self reloadScrollView];
}

#pragma mark - JXCategoryViewDelegate

- (void)categoryView:(JXCategoryBaseView *)categoryView didClickSelectedItemAtIndex:(NSInteger)index {
    if (index == 0) {
        [self.pageScrollView scrollToOriginalPoint];
    }else {
        //不是第一个，需要滚动到categoryView下面
        [self.pageScrollView scrollToCriticalPoint];
    }
}


#pragma mark - 个人空间
- (void)didClickUserView {
    BOOL isCancel = YES;
    if (self.isNews) {
        isCancel = self.dataNewsModel.cancel == 0 || self.dataNewsModel.cancel == 1;
    } else {
        isCancel = self.dataModel.cancel == 0 || self.dataModel.cancel == 1;
    }
    if (!isCancel) {
        [self.view makeToast:@"用户已注销"];
        return;
    }
    
    NSString *userID = @"";
    if (!self.isMySelf) {
        if (self.isNews) {
            userID = kCheckStringNil(self.dataNewsModel.userId);
        } else {
            userID = kCheckStringNil(self.dataModel.userId);
        }
    }
    QJMySpaceViewController *vc = [QJMySpaceViewController new];
    vc.userId = userID;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - NetWork
- (void)getCampsiteDetail {
    NSString *userId = LSUserDefaultsGET(kQJUserId);
    QJCampsiteRequest *startRequest = [[QJCampsiteRequest alloc] init];
    if (self.isNews) {
        [startRequest netWorkGetInfoDetailWithId:self.idStr];
        [startRequest setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
            if (ResponseSuccess) {
                NSDictionary *dataDic = request.responseJSONObject[@"data"];
                QJNewsListModel *model = [QJNewsListModel modelWithDictionary:dataDic];
                self.dataNewsModel = model;
                CGSize  textLimitSize = CGSizeMake(kScreenWidth - 2 * 16, MAXFLOAT);
                NSString *textString = kCheckStringNil(self.dataNewsModel.infoTitle);
                NSMutableAttributedString *mutableAttributedString = [[NSMutableAttributedString alloc] initWithString:textString];
                mutableAttributedString.font = MYFONTALL(FONT_BOLD, 16);
                mutableAttributedString.color = UIColorHex(16191C);
                mutableAttributedString.lineSpacing = 10;
                CGFloat textH = [YYTextLayout layoutWithContainerSize:textLimitSize text:mutableAttributedString].textBoundingSize.height;
                self.dataNewsModel.titleHeight = textH;
                [self.bottomView configureDetailBottomWithNewsData:self.dataNewsModel];
                self.dataNewsModel.hidden = self.isHidden;
                [self reloadScrollView];
                if(self.isNews && !self.dataNewsModel.checked) {
                    self.moreBt.hidden = YES;
                    self.pageVC.commentHeader.timeBt.hidden = YES;
                }               
                if ([kCheckStringNil(userId) isEqualToString:kCheckStringNil(self.dataNewsModel.userId)] && ![kCheckStringNil(userId) isEqualToString:@""]) {
                    self.isMySelf = YES;
                    self.pageVC.isMySelf = self.isMySelf;
                }
                if (self.isVolume) {
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [self didVideoImageUrl:self.headerView];
                    });
                }
                [self getImageFromURL:self.dataNewsModel.shareInfo.picture];
            } else {
                NSString *message = request.responseJSONObject[@"message"];
                [self.view makeToast:kCheckStringNil(message)];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self.navigationController popViewControllerAnimated:YES];
                });
            }
        }];
    } else {
        [startRequest netWorkGetCampDetail:self.idStr isFromCamp:self.isFromCamp];
        [startRequest setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
            if (ResponseSuccess) {
                NSDictionary *dataDic = request.responseJSONObject[@"data"];
                QJCampsiteDetailModel *model = [QJCampsiteDetailModel modelWithDictionary:dataDic];
                self.dataModel = model;
                
                CGSize  textLimitSize = CGSizeMake(kScreenWidth - 2 * 16, MAXFLOAT);
                NSString *textString = kCheckStringNil(self.dataModel.newsTitle);
                NSMutableAttributedString *mutableAttributedString = [[NSMutableAttributedString alloc] initWithString:textString];
                mutableAttributedString.font = MYFONTALL(FONT_BOLD, 16);
                mutableAttributedString.color = UIColorHex(16191C);
                mutableAttributedString.lineSpacing = 10;
                
                CGFloat textH = [YYTextLayout layoutWithContainerSize:textLimitSize text:mutableAttributedString].textBoundingSize.height;
                self.dataModel.titleHeight = textH;
                [self.bottomView configureDetailBottomWithData:self.dataModel];
                [self reloadScrollView];
                if ([kCheckStringNil(userId) isEqualToString:kCheckStringNil(self.dataModel.userId)] && ![kCheckStringNil(userId) isEqualToString:@""]) {
                    self.isMySelf = YES;
                    self.pageVC.isMySelf = self.isMySelf;
                }
                [self getImageFromURL:self.dataModel.shareInfo.picture];

            } else {
                NSString *message = request.responseJSONObject[@"message"];
                [self.view makeToast:kCheckStringNil(message)];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self.navigationController popViewControllerAnimated:YES];
                });
            }
        }];
    }  
}



- (void)touchReplyMainComment:(NSString *)commentID name:(NSString *)name userId:(NSString *)userId toUser:(nonnull QJCommentUser *)toUser{
    
    NSString *userIdSave = LSUserDefaultsGET(kQJUserId);
    BOOL isSelf = NO;
    if ([kCheckStringNil(userIdSave) isEqualToString:kCheckStringNil(userId)] && ![kCheckStringNil(userId) isEqualToString:@""]) {
        isSelf = YES;
    }
    if (isSelf) {
        QJMineMsgDeleteView *view = [[QJMineMsgDeleteView alloc] init];
        @weakify(self);
        view.deleteViewBlock = ^{
            @strongify(self);
            [self deleteCommentView:commentID];
        };
        [view show];
    } else {
        [self showKeyBoardName:[NSString stringWithFormat:@" 回复  %@", name] replyCommentId:commentID toUser:toUser];
    }

}

#pragma mark - 评论
- (void)showKeyBoardName:(NSString *)commentName replyCommentId:(NSString *)replyCommentId toUser:(QJCommentUser *)toUser{
    
    QJCommentInputKit *inputView = [[QJCommentInputKit alloc] initWithFrame:CGRectZero];
    [self.view addSubview:inputView];
    [inputView setMaxLineNum:5];
    [inputView setMaxContentLength:200];
    [inputView setNormalContent:commentName];
    inputView.toUser = toUser;
    
    MJWeakSelf
    [inputView showInput:^(NSString * _Nonnull inputContent) {
        [weakSelf postCommentString:inputContent replyCommentId:replyCommentId];
    }];
    [inputView closeInput:^(NSString * _Nonnull inputContent) {
    }];
}

- (void)postCommentString:(NSString *)string replyCommentId:(NSString *)replyCommentId{
    [QJAppTool showHUDLoading];
    QJCampsiteRequest *startRequest = [[QJCampsiteRequest alloc] init];
    if (self.isNews) {
        [startRequest netWorkPostComment:string replyCommentId:replyCommentId sourceId:self.idStr type:@"1"];
    } else {
        [startRequest netWorkPostComment:string replyCommentId:replyCommentId sourceId:self.idStr type:@"2"];
    }
    [startRequest setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        [QJAppTool hideHUDLoading];
        if (ResponseSuccess) {
            [self.view makeToast:@"评论成功"];
            [[NSNotificationCenter defaultCenter] postNotificationName:QJCommentRefresh object:nil userInfo:@{@"refresh":@(1)}];
        } else {
            NSString *message = request.responseJSONObject[@"message"];
            [[QJAppTool getCurrentViewController].view makeToast:kCheckStringNil(message)];
        }
    }];
    [startRequest setFailureCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        [QJAppTool hideHUDLoading];
        [[QJAppTool getCurrentViewController].view makeToast:@"评论失败"];
    }];
}

- (void)deleteCommentView:(NSString *)commentID {
    [QJAppTool showHUDLoading];
    QJCampsiteRequest *startRequest = [[QJCampsiteRequest alloc] init];
    if (self.isNews) {
        [startRequest netWorkDeleteComment:commentID newsId:@""];
    } else {
        [startRequest netWorkDeleteComment:commentID newsId:self.idStr];
    }
    [startRequest setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        [QJAppTool hideHUDLoading];
        if (ResponseSuccess) {
            [self.view makeToast:@"删除成功"];
            [[NSNotificationCenter defaultCenter] postNotificationName:QJCommentRefresh object:nil userInfo:@{@"refresh":@(1)}];
        } else {
            NSString *message = request.responseJSONObject[@"message"];
            [[QJAppTool getCurrentViewController].view makeToast:kCheckStringNil(message)];
        }
    }];
    [startRequest setFailureCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        [QJAppTool hideHUDLoading];

    }];
}



#pragma mark - 视频
- (void)didVideoImageUrl:(UIView *)headerView {
    if (self.isNews) {
        NSString *videoString = [kCheckStringNil(self.dataNewsModel.videoAddress) checkImageUrlString];
        NSURL *videoURL = [NSURL URLWithString:videoString];
        if ( !_player ) {
            _player = [QJVideoPlayerView player];
        }
        [self.player changePlayerNewsView:self.dataNewsModel showVolume:self.isVolume];
        self.player.URLAsset = [SJVideoPlayerURLAsset.alloc initWithURL:videoURL playModel:[SJPlayModel playModelWithTableView:self.pageScrollView.mainTableView tableHeaderView:headerView superviewSelector:NSSelectorFromString(@"videoImage")]];
        self.player.URLAsset.title = kCheckStringNil(self.dataNewsModel.infoTitle);

        [self addCustomControlLayerToSwitcher];
        MJWeakSelf
        self.player.clickButton = ^(QJVideoPlayerViewType type) {
            if (type == QJVideoPlayerViewTypeRate) {
                [weakSelf switchControlLayer:YES];
            } else if (type == QJVideoPlayerViewTypeRateVertical) {
                [weakSelf switchControlLayer:NO];
            } else if (type == QJVideoPlayerViewTypeMore) {
                [weakSelf switchControlLayerMore:YES];
            } else if (type == QJVideoPlayerViewTypeMoreVertical) {
                [weakSelf switchControlLayerMore:NO];
            }            
        };

        self.player.switcher.delegate = self;
    } else {
        NSString *videoString = [kCheckStringNil(self.dataModel.videoAddress) checkImageUrlString];
        NSURL *videoURL = [NSURL URLWithString:videoString];
        if ( !_player ) {
            _player = [QJVideoPlayerView player];
        }
        [self.player changePlayerView:self.dataModel];
        self.player.URLAsset = [SJVideoPlayerURLAsset.alloc initWithURL:videoURL playModel:[SJPlayModel playModelWithTableView:self.pageScrollView.mainTableView tableHeaderView:headerView superviewSelector:NSSelectorFromString(@"videoImage")]];
        self.player.URLAsset.title = kCheckStringNil(self.dataModel.newsTitle);
        [self addCustomControlLayerToSwitcher];
        MJWeakSelf
        self.player.clickButton = ^(QJVideoPlayerViewType type) {
            if (type == QJVideoPlayerViewTypeRate) {
                [weakSelf switchControlLayer:YES];
            } else if (type == QJVideoPlayerViewTypeRateVertical) {
                [weakSelf switchControlLayer:NO];
            } else if (type == QJVideoPlayerViewTypeMore) {
                [weakSelf switchControlLayerMore:YES];
            } else if (type == QJVideoPlayerViewTypeMoreVertical) {
                [weakSelf switchControlLayerMore:NO];
            }
        };
        self.player.switcher.delegate = self;
    }
}

- (void)switchControlLayerMore:(BOOL)isFull {
    if (isFull) {
        [self.player.switcher switchControlLayerForIdentifier:SJDetailControlShareIdentifier];
    } else {
        [self.player.switcher switchControlLayerForIdentifier:SJDetailControlShareVerticalIdentifier];
    }
}

- (void)switchControlLayer:(BOOL)isFull {
    if (isFull) {
        [self.player.switcher switchControlLayerForIdentifier:SJDetailControlLayerIdentifier];
    } else {
        [self.player.switcher switchControlLayerForIdentifier:SJDetailControlLayerVerticalIdentifier];
    }
}


- (void)addCustomControlLayerToSwitcher {
    __weak typeof(self) _self = self;
    [self.player.switcher addControlLayerForIdentifier:SJDetailControlLayerIdentifier lazyLoading:^id<SJControlLayer> _Nonnull(SJControlLayerIdentifier identifier) {
        __strong typeof(_self) self = _self;
        if (!self ) return nil;
        QJVideoRateViewController *vc = QJVideoRateViewController.new;
        vc.delegate = self;
        vc.isVertical = NO;
        return vc;
    }];
    
    [self.player.switcher addControlLayerForIdentifier:SJDetailControlLayerVerticalIdentifier lazyLoading:^id<SJControlLayer> _Nonnull(SJControlLayerIdentifier identifier) {
        __strong typeof(_self) self = _self;
        if (!self ) return nil;
        QJVideoRateViewController *vc = QJVideoRateViewController.new;
        vc.delegate = self;
        vc.isVertical = YES;
        return vc;
    }];
    
    [self.player.switcher addControlLayerForIdentifier:SJDetailControlShareIdentifier lazyLoading:^id<SJControlLayer> _Nonnull(SJControlLayerIdentifier identifier) {
        __strong typeof(_self) self = _self;
        if (!self ) return nil;
        QJVideoShareViewController *vc = QJVideoShareViewController.new;
        self.videoShareView = vc;
        if (self.isNews) {
            [vc configChangeSaveIsMySelf:self.isMySelf isSave:self.dataNewsModel.collected isNews:self.isNews newsID:self.dataNewsModel.infoId];
        } else {
            [vc configChangeSaveIsMySelf:self.isMySelf isSave:self.dataModel.collected isNews:self.isNews newsID:self.dataModel.newsId];
        }
        vc.delegate = self;
        vc.isVertical = NO;
        return vc;
    }];
    
    [self.player.switcher addControlLayerForIdentifier:SJDetailControlShareVerticalIdentifier lazyLoading:^id<SJControlLayer> _Nonnull(SJControlLayerIdentifier identifier) {
        __strong typeof(_self) self = _self;
        if (!self ) return nil;
        QJVideoShareViewController *vc = QJVideoShareViewController.new;
        self.videoShareView = vc;
        if (self.isNews) {
            [vc configChangeSaveIsMySelf:self.isMySelf isSave:self.dataNewsModel.collected isNews:self.isNews newsID:self.dataNewsModel.infoId];
        } else {
            [vc configChangeSaveIsMySelf:self.isMySelf isSave:self.dataModel.collected isNews:self.isNews newsID:self.dataModel.newsId];
        }
        vc.delegate = self;
        vc.isVertical = YES;
        return vc;
    }];
}
- (void)tappedBlankAreaOnTheControlLayer:(id<SJControlLayer>)controlLayer {
    [self.player.switcher switchControlLayerForIdentifier:SJControlLayer_Edge];
}

- (void)closeView:(id<SJControlLayer>)controlLayer {
    [self.player.switcher switchControlLayerForIdentifier:SJControlLayer_Edge];
}

- (void)clickShareItemTag:(NSInteger)tag {
    [self.player.switcher switchControlLayerForIdentifier:SJControlLayer_Edge];

    if (tag == 18) {//收藏成功
        [self reloadBottomSave:YES];
        
        CGFloat scale =  (QJScreenWidth > QJScreenHeight ? QJScreenHeight : QJScreenWidth) / 375.0;        
        NSAttributedString *attStr = [[NSAttributedString alloc]initWithString:@"收藏成功" attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:12 * scale],NSForegroundColorAttributeName:[UIColor whiteColor]}];
        self.player.textPopupController.contentInset = UIEdgeInsetsMake(30, 40, 30, 40);
        self.player.textPopupController.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.8];
        self.player.textPopupController.cornerRadius = 5;        
        [self.player.textPopupController show:attStr duration:1];

        return;
    } else if (tag == 19) {//取消收藏
        [self reloadBottomSave:NO];
        CGFloat scale =  (QJScreenWidth > QJScreenHeight ? QJScreenHeight : QJScreenWidth) / 375.0;
        NSAttributedString *attStr = [[NSAttributedString alloc]initWithString:@"取消收藏" attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:12 * scale],NSForegroundColorAttributeName:[UIColor whiteColor]}];
        self.player.textPopupController.contentInset = UIEdgeInsetsMake(30, 40, 30, 40);
        self.player.textPopupController.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.8];
        self.player.textPopupController.cornerRadius = 5;
        [self.player.textPopupController show:attStr duration:1];

        return;
    }
    
    if (tag == 10) {//QQ空间
        [self shareQQWithType:1];
        return;
    } else if (tag == 11) {//QQ
        [self shareQQWithType:0];
        return;
    } else if (tag == 12) {//微信
        [self shareWXWithType:0];
        return;
    } else if (tag == 13) {//朋友圈
        [self shareWXWithType:1];
        return;
    }
    
    
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.player videoFullItemWasTapped];
    });

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (tag == 16) {//举报
            [self pushReportView];
        } else if (tag == 17) {//删除
            [self showDeleteView];
        }        
    });
   
}

- (void)tappedButtonRate:(NSString *)rate {
    if ([rate floatValue]) {
        self.player.rate = [rate floatValue];
    }
    [self.player.switcher switchControlLayerForIdentifier:SJControlLayer_Edge];
}

- (BOOL)shouldAutorotate {
    return NO;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [_player vc_viewDidAppear];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [_player vc_viewWillDisappear];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [_player vc_viewDidDisappear];
}

#pragma mark - 刷新点赞

- (void)reloadBottomLike:(NSNotification *)notification {
    NSDictionary *userInfo = notification.userInfo;
    BOOL liked = [userInfo[@"liked"] boolValue];
    if (self.isNews) {                
//        NSInteger newsLikeCount = self.dataNewsModel.infoLikeCount;
//        if (liked) {
//            self.dataNewsModel.liked = YES;
//            newsLikeCount = newsLikeCount + 1;
//        } else {
//            self.dataNewsModel.liked = NO;
//            newsLikeCount = newsLikeCount - 1;
//        }
//        self.dataNewsModel.infoLikeCount = newsLikeCount;
        [self.bottomView configureDetailBottomWithNewsData:self.dataNewsModel];

    } else {
//        NSInteger newsLikeCount = [self.dataModel.newsLikeCount integerValue];
//        if (liked) {
//            self.dataModel.liked = YES;
//            newsLikeCount = newsLikeCount + 1;
//        } else {
//            self.dataModel.liked = NO;
//            newsLikeCount = newsLikeCount - 1;
//        }
//        self.dataModel.newsLikeCount = [NSString stringWithFormat:@"%ld", newsLikeCount];
        [self.bottomView configureDetailBottomWithData:self.dataModel];
    }
}

- (void)reloadVideoData {
    if(self.videoShareView) {
        if (self.isNews) {
            [self.videoShareView configChangeSaveIsMySelf:self.isMySelf isSave:self.dataNewsModel.collected isNews:self.isNews newsID:self.dataNewsModel.infoId];
        } else {
            [self.videoShareView configChangeSaveIsMySelf:self.isMySelf isSave:self.dataModel.collected isNews:self.isNews newsID:self.dataModel.newsId];
        }
    }
    if(self.player.isPlaying) {
        if(self.isNews) {
            [self.player changePlayerNewsView:self.dataNewsModel showVolume:self.isVolume];
        } else {
            [self.player changePlayerView:self.dataModel];
        }
    }
}

- (void)reloadScrollView {
    CGFloat videoH = 0;
    if (self.isNews) {
        if (![kCheckStringNil(self.dataNewsModel.videoAddress) isEqualToString:@""]) {
            videoH = 198*kWScale + 60*kWScale + self.dataNewsModel.titleHeight;
        } else {
            videoH = 60*kWScale + self.dataNewsModel.titleHeight;
        }
        videoH = videoH + 140*kWScale;
        [self.headerView configureDetailHeaderWithNewsData:self.dataNewsModel];
    } else {
        if (![kCheckStringNil(self.dataModel.videoAddress) isEqualToString:@""]) {
            videoH = 198*kWScale + 60*kWScale + self.dataModel.titleHeight;
        } else {
            videoH = 60*kWScale + self.dataModel.titleHeight;
        }
        [self.headerView configureDetailHeaderWithData:self.dataModel];
    }
    videoH = videoH + self.webHeight + 30;
    self.headerView.frame = CGRectMake(0, 0, kScreenWidth, videoH);

    self.pageScrollView.ceilPointHeight = 0;
    [self.pageScrollView reloadData];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (self.isComment) {
            self.myCategoryView.defaultSelectedIndex = 1;
            [self.pageScrollView scrollToCriticalPoint];
        }
    });
   
}

#pragma mark - GKPageScrollViewDelegate
- (UIView *)headerViewInPageScrollView:(GKPageScrollView *)pageScrollView {
    return self.headerView;
}

- (UIView *)pageViewInPageScrollView:(GKPageScrollView *)pageScrollView {
    return self.pageView;
}

- (NSArray<id<GKPageListViewDelegate>> *)listViewsInPageScrollView:(GKPageScrollView *)pageScrollView {
    return self.childVCs;
}

- (void)mainTableViewDidScroll:(UIScrollView *)scrollView isMainCanScroll:(BOOL)isMainCanScroll {
  
    if (self.myCategoryView.selectedIndex != 0 && scrollView.contentOffset.y == 0) {
        //点击了状态栏滚动到顶部时的处理
        [self.myCategoryView selectItemAtIndex:0];
        return;
    }
    if (!(scrollView.isTracking || scrollView.isDecelerating)) {
        //不是用户滚动的，比如setContentOffset等方法，引起的滚动不需要处理。
        return;
    }
    CGFloat velocity = scrollView.contentOffset.y;
    if(velocity == 0) {
        return;
    }
    
    if(isMainCanScroll) {
        [self.myCategoryView selectItemAtIndex:0];
    } else {
        [self.myCategoryView selectItemAtIndex:1];
    }
    
}


#pragma mark - GKWBPageViewControllDelegate
- (void)pageScrollViewWillBeginScroll {
    [self.pageScrollView horizonScrollViewWillBeginScroll];
}

- (void)pageScrollViewDidEndedScroll {
    [self.pageScrollView horizonScrollViewDidEndedScroll];
}

#pragma mark - 懒加载
- (GKPageScrollView *)pageScrollView {
    if (!_pageScrollView) {
        _pageScrollView = [[GKPageScrollView alloc] initWithDelegate:self];
        _pageScrollView.mainTableView.backgroundColor = [UIColor whiteColor];
        _pageScrollView.isControlVerticalIndicator = YES;
        _pageScrollView.isAllowListRefresh = YES;
    }
    return _pageScrollView;
}

- (QJConsultDetailHeaderView *)headerView {
    if (!_headerView) {
        _headerView = [[QJConsultDetailHeaderView alloc] init];
        _headerView.headerDelegate = self;
    }
    return _headerView;
}

- (QJConsultDetailPageVC *)pageVC {
    if (!_pageVC) {
        self.pageVC = [[QJConsultDetailPageVC alloc] init];
        self.pageVC.scrollDelegate = self;
    }
    return _pageVC;
}

- (UIView *)pageView {
    if (!_pageView) {
        [self addChildViewController:self.pageVC];
        self.pageVC.detailListVC = self.detailListVC;
        [self.pageVC didMoveToParentViewController:self];
        _pageView = self.pageVC.view;
    }
    return _pageView;
}

- (QJConsultDetailListVC *)detailListVC {
    if(!_detailListVC) {
        _detailListVC = [[QJConsultDetailListVC alloc] init];
    }
    return _detailListVC;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:QJCampsiteClickLikeType object:nil];
}
@end
