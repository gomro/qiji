//
//  QJCommunityViewController.m
//  QJBox
//
//  Created by wxy on 2022/6/1.
//

#import "QJCommunityViewController.h"

#import "QJCampsiteViewController.h"
#import "QJNewsViewController.h"
#import "QJEditCampsiteViewController.h"
#import "QJCampsiteRequest.h"
#import "QJCampsiteListModel.h"
#import "QJMineMessageViewController.h"
#import "QJMineMsgUnReadView.h"
#import "QJMineMsgRequest.h"
#import "QJNewsSearchViewController.h"
@interface QJCommunityViewController ()
@property (nonatomic, strong) NSMutableArray *myCampsiteData;

/**未读消息*/
@property (nonatomic, strong) QJMineMsgUnReadView *unReadView;

@property (nonatomic, strong) QJNewsViewController *newsVC;
@property (nonatomic, strong) QJCampsiteViewController *campsiteVC;



@property (nonatomic, strong) UIButton *messageBtn;

@property (nonatomic, strong) UIButton *searchButton;

@property (nonatomic, strong) UIView *layerImageView;
@end

@implementation QJCommunityViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self setNavBarHidden:YES];
    [self changeCampsiteData];
    
    NSArray *myCampsite = LSUserDefaultsGET(@"kMyCampsiteList");
    if (!IsArrEmpty(myCampsite)) {
        [self.myCampsiteData removeAllObjects];
        for (NSDictionary *dic in myCampsite) {
            QJCampsiteListModel *model = [QJCampsiteListModel modelWithDictionary:dic];
            [self.myCampsiteData addObject:model];
        }
    }
    
//    [self setStatusBarStyle:UIStatusBarStyleLightContent];
    self.titles = @[@"资讯", @"营地"];
    self.myCategoryView.titleColor = UIColorHex(FFFFFFCC);
    self.myCategoryView.titleSelectedColor =  [UIColor whiteColor];
    self.myCategoryView.titleFont = MYFONTALL(FONT_REGULAR, 16);
    self.myCategoryView.titleSelectedFont =  MYFONTALL(FONT_BOLD, 18);
    self.myCategoryView.titleLabelZoomEnabled = YES;
//    self.myCategoryView.titleLabelZoomScale = 1.1;
    self.myCategoryView.titleLabelStrokeWidthEnabled = YES;
    self.myCategoryView.selectedAnimationEnabled = YES;
    self.myCategoryView.cellWidthZoomEnabled = YES;
    self.myCategoryView.cellWidthZoomScale = 1.3;
    self.myCategoryView.titles = self.titles;
    
    self.myCategoryView.averageCellSpacingEnabled = NO;
    self.myCategoryView.contentEdgeInsetLeft = 20;
    self.myCategoryView.cellSpacing = 24;
    
    JXCategoryIndicatorLineView *lineView = [[JXCategoryIndicatorLineView alloc] init];
    lineView.indicatorWidth = 24 *kWScale;
    lineView.indicatorHeight = 2*kWScale;
    lineView.indicatorColor = UIColorHex(FFFFFF);
    lineView.indicatorCornerRadius = 1.75*kWScale;
    lineView.verticalMargin = 4*kWScale;
    self.myCategoryView.indicators = @[lineView];
    
    self.backImage.image = [UIImage imageNamed:@"bgCampsiteIcon"];
    [self getMyCampsiteList];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeData:) name:QJEditMyCampsiteList object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeBackImage:) name:QJCampsiteBackImage object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshList:) name:QJMyCampsiteListRefresh object:nil];
    [self.myCategoryView setDefaultSelectedIndex:self.communitySelectRow];
    self.searchButton.hidden = self.communitySelectRow;
   
    [self setNavBarButton];
    [self.backImage addSubview:self.layerImageView];
}

- (void)setIsCountDownTask:(BOOL)isCountDownTask{
    _isCountDownTask = isCountDownTask;
    if (self.isCountDownTask == YES) {
        [self.campsiteVC resetCountButtonStatus];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self sendRequestUnRead];
    if (self.myCategoryView.selectedIndex == 0) {
        [self.newsVC changeBackViewImage];
    } else {
        if(self.myCampsiteData.count) {
            [self.campsiteVC changeBackViewImage];
        } else {
            [self changeWhiteBackView];
        }
    }
    
    [self.myCategoryView selectItemAtIndex:self.communitySelectRow];
    self.searchButton.hidden = self.communitySelectRow;
    
    if ([QJAppTool shareManager].campsiteID.length != 0) {
        [self getMyCampsiteList];
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    if (@available(iOS 13.0, *)) {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDarkContent];
        
    } else {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    }
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    self.backImage.frame = CGRectMake(0, 0, kScreenWidth, 230*kWScale);
    self.layerImageView.frame = CGRectMake(0, 190*kWScale, kScreenWidth, 40*kWScale);
    [_layerImageView graduateLeftColor:UIColorHex(FFFFFF00) ToColor:UIColorHex(FFFFFF) startPoint:CGPointMake(0, 0) endPoint:CGPointMake(0, 1)];

    self.myCategoryView.frame = CGRectMake(0, Top_iPhoneX_SPACE + 20, kScreenWidth - 120*kWScale, 44*kWScale);
    self.listContainerView.scrollView.scrollEnabled = NO;
    self.listContainerView.frame = CGRectMake(0, Top_iPhoneX_SPACE + 20 + 44*kWScale, kScreenWidth, kScreenHeight - Bottom_iPhoneX_SPACE - 49 - 44*kWScale - 20 - Top_iPhoneX_SPACE);
}

- (JXCategoryTitleView *)myCategoryView {
    return (JXCategoryTitleView *)self.categoryView;
}

- (JXCategoryBaseView *)preferredCategoryView {
    return [[JXCategoryTitleView alloc] init];
}

- (CGFloat)preferredCategoryViewHeight {
    return 0;
}

- (NSMutableArray *)myCampsiteData {
    if (!_myCampsiteData) {
        _myCampsiteData = [NSMutableArray arrayWithCapacity:0];
    }
    return _myCampsiteData;
}

#pragma mark - JXCategoryListContainerViewDelegate
- (id<JXCategoryListContentViewDelegate>)listContainerView:(JXCategoryListContainerView *)listContainerView initListForIndex:(NSInteger)index {
    if (index == 1) {
        if (self.myCampsiteData.count) {
            QJCampsiteViewController *campsite = [[QJCampsiteViewController alloc] init];
            campsite.myCampsiteData = self.myCampsiteData;
            self.campsiteVC = campsite;
            if (self.isCountDownTask == YES) {
                [self.campsiteVC resetCountButtonStatus];
            }
            return campsite;
        } else {
            QJEditCampsiteViewController *editCampsite = [[QJEditCampsiteViewController alloc] init];
            return editCampsite;
        }
    } else {
        QJNewsViewController *consult = [[QJNewsViewController alloc] init];
        self.newsVC = consult;
        return consult;
    }
}
#pragma mark - JXCategoryViewDelegate
- (void)categoryView:(JXCategoryBaseView *)categoryView didSelectedItemAtIndex:(NSInteger)index {
    self.communitySelectRow = index;
    if (index == 0) {
        self.searchButton.hidden = NO;
        [self.newsVC changeBackViewImage];
    } else {
        self.searchButton.hidden = YES;
        if(self.myCampsiteData.count) {
            [self.campsiteVC changeBackViewImage];
        } else {
            [self changeWhiteBackView];
        }
    }
}

- (void)changeCampsiteData {
    NSArray *myCampsite = LSUserDefaultsGET(@"kMyCampsiteList");
    if (!IsArrEmpty(myCampsite)) {
        for (NSDictionary *dic in myCampsite) {
            QJCampsiteListModel *model = [QJCampsiteListModel modelWithDictionary:dic];
            if(self.myCampsiteData.count > 0){
                for (QJCampsiteListModel *a in self.myCampsiteData.copy) {
                    if(![a.campsiteID isEqualToString:model.campsiteID]){
                        if(![self.myCampsiteData containsObject:model]){
                            [self.myCampsiteData safeAddObject:model];
                        }
                    }
                }
                
            }else{
                [self.myCampsiteData safeAddObject:model];
            }
        }
    }
}
- (QJMineMsgUnReadView *)unReadView {
    if (!_unReadView) {
        _unReadView = [[QJMineMsgUnReadView alloc] init];
        _unReadView.layer.cornerRadius = 7;
        _unReadView.layer.borderColor = [UIColor whiteColor].CGColor;
        _unReadView.layer.borderWidth = 1;
        [_unReadView setHidden:YES];
    }
    return _unReadView;
}


#pragma mark - NavBar
- (void)setNavBarButton {
    /**消息中心*/
    UIButton *messageBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.messageBtn = messageBtn;
    [self.view addSubview:messageBtn];
    [messageBtn setImage:[UIImage imageNamed:@"commentChatMessage"] forState:UIControlStateNormal];
    [messageBtn addTarget:self action:@selector(messageBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [messageBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.view.mas_right).offset(-22);
        make.centerY.equalTo(self.categoryView);
        make.width.height.mas_equalTo(24*kWScale);
    }];
    
    /**未读消息数*/
    [self.view addSubview:self.unReadView];
    [self.unReadView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(messageBtn.mas_right).mas_offset(-3);
        make.centerY.equalTo(messageBtn.mas_top).mas_offset(3);
        make.height.mas_equalTo(14*kWScale);
        make.width.mas_greaterThanOrEqualTo(14*kWScale);
    }];
    
    /**搜索*/
    [self.view addSubview:self.searchButton];
    [self.searchButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(messageBtn.mas_left).offset(-20);
        make.centerY.equalTo(messageBtn);
        make.width.height.mas_equalTo(24*kWScale);
    }];
}

- (UIButton *)searchButton {
    if (!_searchButton) {
        _searchButton = [UIButton new];
        [_searchButton setImage:[UIImage imageNamed:@"searchWhiteIcon"] forState:UIControlStateNormal];
        [_searchButton addTarget:self action:@selector(searchBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _searchButton;
}

- (UIView *)layerImageView {
    if(!_layerImageView) {
        _layerImageView = [UIView new];
    }
    return _layerImageView;
}

- (void)messageBtnClick:(UIButton *)sender {
    if ([QJUserManager shareManager].isLogin) {
        QJMineMessageViewController *vc = [[QJMineMessageViewController alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
    } else {
        [self showLoginVCCompleted:^(BOOL isLogin) {

        }];
    }
}

- (void)searchBtnClick:(UIButton *)sender {
    QJNewsSearchViewController *vc = [[QJNewsSearchViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}
#pragma mark - NetWork
- (void)getMyCampsiteList {
    QJCampsiteRequest *startRequest = [[QJCampsiteRequest alloc] init];
    [startRequest netWorkGetMineCamp];
    [startRequest setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            NSArray *dataArr = request.responseJSONObject[@"data"];
            LSUserDefaultsSET(dataArr, @"kMyCampsiteList");
            [QJAppTool shareManager].campsiteArr = dataArr;
            [self.myCampsiteData removeAllObjects];
            for (NSDictionary *dic in dataArr) {
                QJCampsiteListModel *model = [QJCampsiteListModel modelWithDictionary:dic];
                [self.myCampsiteData addObject:model];
            }
            [self.myCategoryView reloadData];
            if (self.myCategoryView.selectedIndex == 1) {
                if(self.myCampsiteData.count) {
                    [self.campsiteVC changeBackViewImage];
                } else {
                    [self changeWhiteBackView];
                }
            }
            
        }
    }];
}

- (void)changeData:(NSNotification *)notification {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.myCampsiteData removeAllObjects];
        //处理删除操作
        NSDictionary *userInfo = notification.userInfo;
        NSString *type = userInfo[@"type"];
        NSDictionary *dataDic = userInfo[@"data"];
        
        if ([type isEqualToString:@"0"]) {//删除
            NSArray *myCampsite = LSUserDefaultsGET(@"kMyCampsiteList");
            NSMutableArray *myCampMuArr = myCampsite.mutableCopy;
            for (NSDictionary *b in myCampsite) {
                if([EncodeStringFromDic(b, @"campsiteID") isEqualToString:EncodeStringFromDic(dataDic, @"campsiteID")]){
                    [myCampMuArr removeObject:b];
                }
            }
            LSUserDefaultsSET(myCampMuArr.copy, @"kMyCampsiteList");
            
        }
        [self changeCampsiteData];
        [self.myCategoryView reloadData];
    });
}

- (void)changeBackImage:(NSNotification *)notification {
 
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    NSDictionary *userInfo = notification.userInfo;
    
    
    YYWebImageManager *manager = [YYWebImageManager sharedManager];
    NSString *imageStr = [userInfo[@"image"] checkImageUrlString];

//    @weakify(self);
    [manager requestImageWithURL:[NSURL URLWithString:imageStr] options:YYWebImageOptionAllowBackgroundTask progress:nil transform:nil completion:^(UIImage * _Nullable image, NSURL * _Nonnull url, YYWebImageFromType from, YYWebImageStage stage, NSError * _Nullable error) {
//        @strongify(self);
        if (image) {
            dispatch_async(dispatch_get_main_queue(), ^{
                CATransition *transition = [CATransition animation];
                transition.type = kCATransitionFade;
                transition.duration = 0.6f;
                transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
                [self.backImage.layer addAnimation:transition forKey:nil];
                self.backImage.image = image;
            });
        }
    }];

    [self sendRequestUnRead];
    self.messageBtn.hidden = NO;
    self.myCategoryView.titleColor = UIColorHex(FFFFFFCC);
    self.myCategoryView.titleSelectedColor =  [UIColor whiteColor];
    JXCategoryIndicatorLineView *lineView = [[JXCategoryIndicatorLineView alloc] init];
    lineView.indicatorWidth = 24 *kWScale;
    lineView.indicatorHeight = 2*kWScale;
    lineView.indicatorColor = UIColorHex(FFFFFF);
    lineView.indicatorCornerRadius = 1.75*kWScale;
    lineView.verticalMargin = 4*kWScale;
    self.myCategoryView.indicators = @[lineView];
    [self.myCategoryView reloadDataWithoutListContainer];
}

- (void)changeWhiteBackView {
    if (@available(iOS 13.0, *)) {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDarkContent];
        
    } else {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    }
    self.backImage.image = [UIImage imageWithColor:[UIColor whiteColor]];
    self.myCategoryView.titleColor = UIColorHex(919599);
    self.myCategoryView.titleSelectedColor =  UIColorHex(000000);
    JXCategoryIndicatorLineView *lineView = [[JXCategoryIndicatorLineView alloc] init];
    lineView.indicatorWidth = 24 *kWScale;
    lineView.indicatorHeight = 2*kWScale;
    lineView.indicatorColor = UIColorHex(16191C);
    lineView.indicatorCornerRadius = 1.75*kWScale;
    lineView.verticalMargin = 4*kWScale;
    self.myCategoryView.indicators = @[lineView];
    [self.myCategoryView reloadDataWithoutListContainer];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.unReadView.hidden = YES;
        self.messageBtn.hidden = NO;
    });
}

- (void)refreshList:(NSNotification *)notification {
    [self getMyCampsiteList];
}

- (void)sendRequestUnRead {
    @weakify(self);
    QJMineMsgRequest *request = [[QJMineMsgRequest alloc] init];
    [request requestNotifyUnread_completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        @strongify(self);
        if (isSuccess) {
            NSInteger num = [request.responseJSONObject[@"data"][@"count"] integerValue];
            [self.unReadView reloadUnReadView:num];

        }
    }];
    
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:QJEditMyCampsiteList object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:QJCampsiteBackImage object:nil];
}
@end
