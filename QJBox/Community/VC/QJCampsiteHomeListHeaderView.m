//
//  QJCampsiteHomeListHeaderView.m
//  QJBox
//
//  Created by Sun on 2022/7/6.
//

#import "QJCampsiteHomeListHeaderView.h"
#import "QJImageButton.h"
#import "QJCommentUser.h"

@interface QJCampsiteHomeListHeaderView ()
/**背景图*/
@property (nonatomic, strong) UIImageView *backImage;
/**标题*/
@property (nonatomic, strong) UILabel *titleLb;
/**详情*/
@property (nonatomic, strong) UILabel *detailLb;
/**粉丝数*/
@property (nonatomic, strong) UILabel *fansNum;
/**营地动态数*/
@property (nonatomic, strong) UILabel *campsiteNum;
/**分割线*/
@property (nonatomic, strong) UIView *lineView;
/**粉丝头像*/
@property (nonatomic, strong) QJCampsiteHeaderFansView *fansView;
/**福利*/
@property (nonatomic, strong) QJImageButton *welfareBt;


@end

@implementation QJCampsiteHomeListHeaderView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:self.backImage];
        [self.backImage addSubview:self.titleLb];
        [self.backImage addSubview:self.detailLb];
        [self.backImage addSubview:self.fansNum];
        [self.backImage addSubview:self.fansView];
        [self.backImage addSubview:self.welfareBt];
        [self.backImage addSubview:self.campsiteNum];
        [self.backImage addSubview:self.lineView];
        [self makeConstraints];
    }
    return self;
}

- (void)makeConstraints {
    [self.backImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).offset(5 * kWScale);
        make.left.equalTo(self.mas_left).offset(16);
        make.right.equalTo(self.mas_right).offset(-16).priorityHigh();
        make.height.mas_equalTo(124 * kWScale);
    }];
    [self.titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.backImage.mas_top).offset(16* kWScale);
        make.left.equalTo(self.backImage.mas_left).offset(16);
        make.height.mas_equalTo(18* kWScale);
    }];

    [self.detailLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.backImage.mas_top).offset(74* kWScale);
        make.left.equalTo(self.backImage.mas_left).offset(16);
        make.right.equalTo(self.backImage.mas_right).offset(-16);
    }];
    
    [self.fansView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLb.mas_bottom).offset(14* kWScale);
        make.left.equalTo(self.backImage.mas_left).offset(16);
        make.width.mas_lessThanOrEqualTo(32* kWScale);
    }];
    [self.fansNum mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.fansView);
        make.left.equalTo(self.fansView.mas_right).offset(6);
    }];
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.fansView);
        make.left.equalTo(self.fansNum.mas_right).offset(16);
        make.width.mas_equalTo(1);
        make.height.mas_equalTo(8* kWScale);
    }];
    [self.campsiteNum mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.fansView);
        make.left.equalTo(self.lineView.mas_right).offset(16);
    }];
 
    [self.welfareBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.backImage.mas_top).offset(14* kWScale);
        make.right.equalTo(self.backImage.mas_right).offset(-16);
        make.height.mas_equalTo(16* kWScale);
    }];
}

#pragma mark - Lazy Loading
- (UIImageView *)backImage {
    if (!_backImage) {
        _backImage = [UIImageView new];
        _backImage.userInteractionEnabled = YES;
        _backImage.layer.cornerRadius = 8* kWScale;
        _backImage.clipsToBounds = YES;
        _backImage.contentMode = UIViewContentModeScaleAspectFill;
    }
    return _backImage;
}

- (UILabel *)titleLb {
    if (!_titleLb) {
        _titleLb = [UILabel new];
        _titleLb.textColor = [UIColor whiteColor];
        _titleLb.font = MYFONTALL(FONT_BOLD, 18);
    }
    return _titleLb;
}

- (UILabel *)detailLb {
    if (!_detailLb) {
        _detailLb = [UILabel new];
        _detailLb.textColor = [UIColor whiteColor];
        _detailLb.font = MYFONTALL(FONT_REGULAR, 13);
        _detailLb.numberOfLines = 2;
    }
    return _detailLb;
}

- (UILabel *)fansNum {
    if (!_fansNum) {
        _fansNum = [UILabel new];
        _fansNum.textColor = [UIColor whiteColor];
        _fansNum.font = MYFONTALL(FONT_REGULAR, 10);
    }
    return _fansNum;
}

- (UIView *)lineView {
    if (!_lineView) {
        _lineView = [UIView new];
        _lineView.backgroundColor = UIColorHex(919599);
    }
    return _lineView;
}

- (UILabel *)campsiteNum {
    if (!_campsiteNum) {
        _campsiteNum = [UILabel new];
        _campsiteNum.textColor = [UIColor whiteColor];
        _campsiteNum.font = MYFONTALL(FONT_REGULAR, 10);
    }
    return _campsiteNum;
}

- (QJCampsiteHeaderFansView *)fansView {
    if (!_fansView) {
        _fansView = [QJCampsiteHeaderFansView new];
    }
    return _fansView;
}

- (QJImageButton *)welfareBt {
    if (!_welfareBt) {
        _welfareBt = [[QJImageButton alloc] init];
        _welfareBt.titleString = @"福利多多";
        _welfareBt.titleLb.font = MYFONTALL(FONT_REGULAR, 12);
        _welfareBt.titleLb.textColor = [UIColor whiteColor];
        _welfareBt.iconString = @"giftIcon";
        _welfareBt.imageSize = CGSizeMake(16* kWScale, 16* kWScale);
        _welfareBt.space = 4;
        _welfareBt.style = ImageButtonStyleLeft;
    }
    return _welfareBt;
}

- (void)configureHeaderDetailWithData:(QJCampsiteDescriptionModel *)dataModel {
    [self.fansView configureHeaderFansWithData:dataModel];
    NSString *cardImage = [kCheckStringNil(dataModel.cardImage) checkImageUrlString];
    [self.backImage setImageWithURL:[NSURL URLWithString:cardImage] placeholder:[UIImage imageNamed:@"headerBanner"]];
    self.titleLb.text = kCheckStringNil(dataModel.name);
    self.fansNum.text = [NSString stringWithFormat:@"粉丝 %@", kCheckStringNil(dataModel.fansCount)];
    self.campsiteNum.text = [NSString stringWithFormat:@"营地动态 %@", kCheckStringNil(dataModel.newsCount)];
//    self.detailLb.text = kCheckStringNil(dataModel.shotDesc);

    NSString *detail = kCheckStringNil(dataModel.shotDesc);
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
    [style setLineBreakMode:NSLineBreakByCharWrapping];
    style.lineSpacing = 1.5;

    NSMutableAttributedString *attriString = [[NSMutableAttributedString alloc] initWithString:detail];
    [attriString addAttribute:NSParagraphStyleAttributeName value:style range:NSMakeRange(0, [detail length])];
    // 设置字间距（同一行的文字内容）
    // [attriString addAttribute:NSKernAttributeName value:@([UIScreen mainScreen].bounds.size.width/375*7) range:NSMakeRange(0, [self.phenology_TitleL.text length])];
    // 设置“字间距”之后，这样会居中显示style.alignment = NSTextAlignmentRight;（这样不居中 NSTextAlignmentCenter;）
    self.detailLb.attributedText = attriString;
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    UIView *view = [super hitTest:point withEvent:event];
    if (view == self.welfareBt) {
          return self;
    }
    return view;
}
@end



@interface QJCampsiteHeaderFansView ()
/**粉丝1*/
@property (nonatomic, strong) UIImageView *fansFirstImage;
/**粉丝2*/
@property (nonatomic, strong) UIImageView *fansSecondImage;
/**粉丝3*/
@property (nonatomic, strong) UIImageView *fansThirdImage;

@property (nonatomic, strong) NSMutableArray *imageArr;
@end

@implementation QJCampsiteHeaderFansView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:self.fansFirstImage];
        [self addSubview:self.fansSecondImage];
        [self addSubview:self.fansThirdImage];
        [self.imageArr addObject:self.fansFirstImage];
        [self.imageArr addObject:self.fansSecondImage];
        [self.imageArr addObject:self.fansThirdImage];
    }
    return self;
}

- (UIImageView *)fansFirstImage {
    if (!_fansFirstImage) {
        _fansFirstImage = [UIImageView new];
        _fansFirstImage.layer.masksToBounds = YES;
        _fansFirstImage.layer.cornerRadius = 7* kWScale;
    }
    return _fansFirstImage;
}

- (UIImageView *)fansSecondImage {
    if (!_fansSecondImage) {
        _fansSecondImage = [UIImageView new];
        _fansSecondImage.layer.masksToBounds = YES;
        _fansSecondImage.layer.cornerRadius = 7* kWScale;
    }
    return _fansSecondImage;
}

- (UIImageView *)fansThirdImage {
    if (!_fansThirdImage) {
        _fansThirdImage = [UIImageView new];
        _fansThirdImage.layer.masksToBounds = YES;
        _fansThirdImage.layer.cornerRadius = 7* kWScale;
    }
    return _fansThirdImage;
}

- (NSMutableArray *)imageArr {
    if (!_imageArr) {
        _imageArr = [NSMutableArray arrayWithCapacity:0];
    }
    return _imageArr;
}

- (void)configureHeaderFansWithData:(QJCampsiteDescriptionModel *)dataModel {
    
    CGFloat leftHeight = 0;
    for (int i = 0; i < dataModel.users.count; i++) {
        UIImageView *image = self.imageArr[i];
        leftHeight = 9 * i;
        if (i ==  dataModel.users.count - 1) {
            [image mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.top.bottom.right.equalTo(self);
                make.left.equalTo(self).offset(leftHeight);
                make.width.height.mas_equalTo(14* kWScale);
            }];
        } else {
            [image mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.top.bottom.equalTo(self);
                make.left.equalTo(self).offset(leftHeight);
                make.width.height.mas_equalTo(14* kWScale);
            }];
        }
        QJCommentUser *userModel = dataModel.users[i];
        NSString *imageString = [kCheckStringNil(userModel.coverImage) checkImageUrlString];
        [image setImageWithURL:[NSURL URLWithString:imageString] placeholder:nil];
    }
    
}

- (void)makeConstraints {
}

@end
