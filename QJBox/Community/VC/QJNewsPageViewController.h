//
//  QJNewsPageViewController.h
//  QJBox
//
//  Created by Sun on 2022/7/27.
//

#import "QJContentBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN
@protocol QJNewsPageViewControllerDelegate <NSObject>

- (void)pageScrollViewWillBeginScroll;
- (void)pageScrollViewDidEndedScroll;

@end

@interface QJNewsPageViewController : QJContentBaseViewController
@property (nonatomic, weak) id<QJNewsPageViewControllerDelegate> scrollDelegate;
- (void)configTitles:(NSArray *)titles childVCs:(NSArray *)childVCs;

- (void)selectItemView:(NSInteger)index;

@property (nonatomic, copy) NSString *campsiteID;
@property (nonatomic, strong) NSArray *tags;// 标签数组
@end

NS_ASSUME_NONNULL_END
