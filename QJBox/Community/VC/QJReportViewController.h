//
//  QJReportViewController.h
//  QJBox
//
//  Created by Sun on 2022/7/22.
//

#import "QJBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJReportViewController : QJBaseViewController
@property (nonatomic, copy) NSString *newsID;
@property (nonatomic, assign) BOOL isNews;

@property (nonatomic, copy) NSString *userId;//被举报人的id

@end

NS_ASSUME_NONNULL_END
