//
//  QJVideoImageViewController.m
//  QJBox
//
//  Created by Sun on 2022/8/4.
//

#import "QJVideoImageViewController.h"
#import "WMZBannerView.h"
#import "UIImage+Resize.h"

@interface QJVideoImageViewController ()
@property (nonatomic, strong) UIView *backView;
@property (nonatomic, strong) UIImageView *imageVC;

@property (nonatomic, strong) UIView *imageChange;
@property (nonatomic, strong) WMZBannerView *bannerView;
@property (nonatomic, strong) WMZBannerParam *bannerParam;


@property (nonatomic, strong) NSMutableArray *imageArr;
@property (nonatomic, strong) UIImage *finishImage;

@property (nonatomic, strong) UIImageView *imageBgView;

@property (nonatomic, strong) UIVisualEffectView *bgImageMaskView;

@end

@implementation QJVideoImageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNavBarHidden:YES];
    [self loadUI];
    [self loadVideoImageUI];
    [self bannerImageUI];
}

- (void)loadVideoImageUI {
    [self.backView addSubview:self.imageBgView];
    [self.imageBgView addSubview:self.bgImageMaskView];
    [self.imageBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.backView);
        make.height.mas_equalTo(kScreenWidth * (2/3.0));
        }];
    [self.bgImageMaskView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.imageBgView);
    }];
    [self.imageBgView addSubview:self.imageVC];
    [self.imageVC mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self.imageBgView);
        make.height.mas_equalTo(kScreenWidth *(2/3.0));
    }];
    
 
    UILabel *bottomLb = [UILabel new];
    bottomLb.text = @"滑动选择封面";
    bottomLb.textColor = UIColorHex(FFFFFF);
    bottomLb.font = MYFONTALL(FONT_REGULAR, 14);
    [self.backView addSubview:bottomLb];
    [bottomLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.backView);
        make.bottom.equalTo(self.backView.mas_bottom).offset(- Bottom_iPhoneX_SPACE - 30);
    }];
    
    [self.backView addSubview:self.imageChange];
    [self.imageChange mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(bottomLb.mas_top).offset(-24);
        make.left.equalTo(self.backView.mas_left).offset(16.5);
        make.right.equalTo(self.backView.mas_right).offset(-16.5);
        make.height.mas_equalTo(80);
    }];
    
    UILabel *bottomTitle = [UILabel new];
    bottomTitle.text = @"视频";
    bottomTitle.textColor = UIColorHex(FFFFFF);
    bottomTitle.font = MYFONTALL(FONT_BOLD, 16);
    [self.backView addSubview:bottomTitle];
    [bottomTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.backView);
        make.bottom.equalTo(self.imageChange.mas_top).offset(- 24);
    }];
    
}

- (void)loadUI {
    self.view.backgroundColor = [UIColor whiteColor];

    UIView *navBack = [UIView new];
    [self.view addSubview:navBack];
    [navBack mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.top.equalTo(self.view.mas_top).offset(Top_iPhoneX_SPACE + 20);
        make.height.mas_equalTo(44);
    }];
    [self.view addSubview:self.backView];
    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.top.equalTo(navBack.mas_bottom);
    }];
    
    UIButton *backBt = [UIButton new];
    [backBt setImage:[UIImage imageNamed:@"backimage_black"] forState:UIControlStateNormal];
    [backBt addTarget:self action:@selector(popView:) forControlEvents:UIControlEventTouchUpInside];
    [navBack addSubview:backBt];
    [backBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(30);
        make.height.mas_equalTo(30);
        make.centerY.equalTo(navBack);
        make.left.equalTo(self.view).offset(16);
    }];
    
    UILabel *title = [UILabel new];
    title.text = @"选择封面";
    title.textColor = UIColorHex(16191C);
    title.font = MYFONTALL(FONT_BOLD, 16);
    [navBack addSubview:title];
    [title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(navBack);
    }];
    
    UIButton *finishBt = [UIButton new];
    [finishBt setTitle:@"完成" forState:UIControlStateNormal];
    [finishBt setTitleColor:UIColorHex(007AFF) forState:UIControlStateNormal];
    finishBt.titleLabel.font = MYFONTALL(FONT_BOLD, 14);
    [finishBt addTarget:self action:@selector(finishView:) forControlEvents:UIControlEventTouchUpInside];
    [navBack addSubview:finishBt];
    [finishBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(24);
        make.centerY.equalTo(navBack);
        make.right.equalTo(self.view.mas_right).offset(-32);
    }];
    
}



- (void)bannerImageUI {
    WMZBannerParam *param = BannerParam();
   //自定义视图必传
    param.wMyCellClassNameSet(@"QJVideoImageViewControllerCell");
    param.wMyCellSet(^UICollectionViewCell *(NSIndexPath *indexPath, UICollectionView *collectionView, id model, UIImageView *bgImageView,NSArray*dataArr) {
              //自定义视图
       QJVideoImageViewControllerCell *cell = (QJVideoImageViewControllerCell *)[collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([QJVideoImageViewControllerCell class]) forIndexPath:indexPath];
       if (![kCheckStringNil(model) isEqualToString:@""]) {
           cell.icon.image = self.imageArr[indexPath.item];
       } else {
           cell.icon.image = [UIImage imageNamed:@""];
       }
       return cell;
    });
    param.wFrameSet(CGRectMake(0, 0, kScreenWidth - 33, 80));
    param.wDataSet(@[]);
   //关闭pageControl
    param.wHideBannerControlSet(YES);
   //自定义item的大小
    param.wItemSizeSet(CGSizeMake((kScreenWidth - 33)*0.333, 80));
   //固定移动的距离
    param.wContentOffsetXSet(0.5);
//   //自动滚动
    param.wAutoScrollSet(NO);
    //循环
    param.wRepeatSet(NO);
   //整体左右间距  设置为size.width的一半 让最后一个可以居中
    param.wSectionInsetSet(UIEdgeInsetsMake(0,0, 0, 0));
   //间距
    param.wLineSpacingSet(0);
    param.wEventScrollEndSet( ^(id anyID, NSInteger index, BOOL isCenter,UICollectionViewCell *cell) {
        //毛玻璃效果外部调整
        [self changeImageView:index];
    });
    self.bannerParam = param;
   WMZBannerView *bannerView = [[WMZBannerView alloc]initConfigureWithModel:param];
   [self.imageChange addSubview:bannerView];
    self.bannerView = bannerView;
    
    QJVideoImageTopView *topView = [[QJVideoImageTopView alloc] initWithFrame:CGRectMake((kScreenWidth - 33)*0.333 - 5, -5, (kScreenWidth - 33)*0.333 + 10, 90)];
    [self.imageChange addSubview:topView];
    
}

- (void)changeImageView:(NSInteger)index {
    UIImage *oldImage = self.imageArr[index + 1];
    CGFloat width = oldImage.size.width;
    CGFloat height = oldImage.size.height;
    float radio = width / height;
    
    CGFloat kuangH = kScreenWidth/3*2.0;
    CGFloat kuangW = kScreenWidth;
    
    height = kuangW/radio;//宽度固定屏幕宽
    if(height > kuangH){
        height = kuangH;
        width = kuangH*radio;
    }else{
        width = kuangW;
    }
    
    
    
    
    self.imageVC.image = oldImage;
    self.imageBgView.image = oldImage;
    [self.imageVC mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(self.imageBgView);
            make.width.equalTo(@(width));
            make.height.equalTo(@(height));
    }];
}

#pragma mark ---截图
- (UIImage *)screenshotWithView:(UIView *)view {
    if (![view isKindOfClass:UIView.class]) {
        return nil;
    }
    UIImage *screenshotImage = nil;
    @try {
        CGSize size = view.bounds.size;
        UIGraphicsBeginImageContextWithOptions(size, NO, 0);
        CGRect rect = view.bounds;
        //  drawViewHierarchyInRect:afterScreenUpdates: 截取一个UIView或者其子类中的内容，并且以位图的形式（bitmap）保存到UIImage中
        // afterUpdates 参数表示是否在所有效果应用在视图上了以后再获取快照
        [view drawViewHierarchyInRect:rect afterScreenUpdates:NO];
        screenshotImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    } @catch (NSException *exception) {
        DLog(@"screenshot fail，error %@: %@", self, exception);
    }
    return screenshotImage;
}
#pragma mark - 点击事件
- (void)finishView:(UIButton *)sender {
    self.finishImage = [self screenshotWithView:self.imageBgView];
    if (self.clickFinish && self.finishImage) {
        self.clickFinish(self.finishImage);
    }
    [self.navigationController popViewControllerAnimated:YES];

}
- (void)popView:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - 懒加载

- (UIView *)backView {
    if (!_backView) {
        _backView = [UIView new];
        _backView.backgroundColor = [UIColor hx_colorWithHexStr:@"a7a7a7" alpha:1];
    }
    return _backView;
}

- (UIImageView *)imageVC {
    if (!_imageVC) {
        _imageVC = [UIImageView new];
//        _imageVC.contentMode = UIViewContentModeScaleAspectFill;
//        _imageVC.clipsToBounds = YES;
    }
    return _imageVC;
}

- (UIImageView *)imageBgView {
    if (!_imageBgView) {
        _imageBgView = [UIImageView new];
        _imageBgView.contentMode = UIViewContentModeScaleAspectFill;
        _imageBgView.clipsToBounds = YES;
    }
    return _imageBgView;
}

- (UIVisualEffectView *)bgImageMaskView {
    if (!_bgImageMaskView) {
        _bgImageMaskView = [UIVisualEffectView new];
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];

        _bgImageMaskView = [[UIVisualEffectView alloc]initWithEffect:blurEffect];
        
    }
    return _bgImageMaskView;
}


- (UIView *)imageChange {
    if (!_imageChange) {
        _imageChange = [UIView new];
        _imageChange.backgroundColor = UIColorHex(D9D9D9);
    }
    return _imageChange;
}

- (NSMutableArray *)imageArr {
    if (!_imageArr) {
        _imageArr = [NSMutableArray arrayWithCapacity:0];
    }
    return _imageArr;
}

#pragma mark - 数据
- (void)setVideoUrl:(NSURL *)videoUrl {
    _videoUrl = videoUrl;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self handleVideo:self.videoUrl];
    });
}

- (void)handleVideo:(NSURL *)videoUrl {
    HXPhotoModel *model = [HXPhotoModel photoModelWithVideoURL:videoUrl];
    NSTimeInterval videoDuration = model.videoDuration;
    AVAsset * asset = [AVAsset assetWithURL:videoUrl];
    
    NSTimeInterval spacingTime = videoDuration / 20;
    NSTimeInterval time = 0;
    [self.imageArr removeAllObjects];
    [self.imageArr addObject:@""];
    for (int i = 0; i < 20; i++) {
        UIImage *image = [self thumbnailImageForVideo:videoUrl atTime:time videoDuration:asset];
        if (image) {
            [self.imageArr addObject:image];
        }
        time = time + spacingTime;
    }
    [self.imageArr addObject:@""];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.bannerParam.wData = [self.imageArr copy];
        [self.bannerView updateUI];
    });
 
}

- (UIImage *)thumbnailImageForVideo:(NSURL *)videoURL atTime:(NSTimeInterval)time videoDuration:(AVAsset *)videoDuration {
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:videoURL options:nil];
    if (!asset) {
        return nil;
    }
    AVAssetImageGenerator *assetImageGenerator =[[AVAssetImageGenerator alloc] initWithAsset:asset];
    assetImageGenerator.appliesPreferredTrackTransform = YES;
    assetImageGenerator.requestedTimeToleranceBefore = kCMTimeZero;
    assetImageGenerator.requestedTimeToleranceAfter = kCMTimeZero;
    assetImageGenerator.apertureMode = AVAssetImageGeneratorApertureModeProductionAperture;
    CGImageRef thumbnailImageRef = NULL;
    CMTime time12 = CMTimeMakeWithSeconds(time, videoDuration.duration.timescale + 100);

    NSError *thumbnailImageGenerationError = nil;
    thumbnailImageRef = [assetImageGenerator copyCGImageAtTime:time12 actualTime:NULL error:&thumbnailImageGenerationError];
    UIImage*thumbnailImage = thumbnailImageRef ? [[UIImage alloc]initWithCGImage: thumbnailImageRef] : nil;
    CGImageRelease(thumbnailImageRef);
    return thumbnailImage;
}

@end


@implementation QJVideoImageViewControllerCell
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self){
        self.icon = [UIImageView new];
        self.icon.contentMode = UIViewContentModeScaleAspectFill;
        self.icon.clipsToBounds = YES;
        [self.contentView addSubview:self.icon];
        [self.icon mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.left.right.equalTo(self.contentView);
        }];
    }
    return self;
}
@end

@implementation QJVideoImageTopView
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self){
        self.topView = [UIView new];
        [self addSubview:self.topView];
        self.topView.frame = self.bounds;
        self.topView.layer.borderColor = [UIColor whiteColor].CGColor;
        self.topView.layer.borderWidth = 5;
        self.topView.layer.cornerRadius = 5;
    }
    return self;
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    return nil;
}
@end
