//
//  QJConsultDetailPageVC.h
//  QJBox
//
//  Created by Sun on 2022/10/9.
//

#import "QJContentBaseViewController.h"
#import "QJConsultCommentHeaderView.h"
#import "QJConsultDetailListVC.h"
#import "QJCommentUser.h"

NS_ASSUME_NONNULL_BEGIN
@protocol QJConsultDetailPageVCDelegate <NSObject>

- (void)pageScrollViewWillBeginScroll;
- (void)pageScrollViewDidEndedScroll;
- (void)touchReplyMainComment:(NSString *)commentID name:(NSString *)name userId:(NSString *)userId toUser:(QJCommentUser *)toUser;

@end
@interface QJConsultDetailPageVC : QJContentBaseViewController
@property (nonatomic, weak) id<QJConsultDetailPageVCDelegate> scrollDelegate;

/**评论顶部视图**/
@property (nonatomic, strong) QJConsultCommentHeaderView *commentHeader;

/**动态、资讯id**/
@property (nonatomic, copy) NSString *idStr;

/**是否是资讯*/
@property (nonatomic, assign) BOOL isNews;

/**判断是否是个人*/
@property (nonatomic, assign) BOOL isMySelf;

@property (nonatomic, strong) QJConsultDetailListVC *detailListVC;
- (void)configTitles:(NSArray *)titles childVCs:(NSArray *)childVCs;

@end

NS_ASSUME_NONNULL_END
