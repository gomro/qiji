//
//  QJConsultDetailViewController.h
//  QJBox
//
//  Created by Sun on 2022/7/11.
//

#import "QJBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJConsultDetailViewController : QJBaseViewController
/**动态、资讯id**/
@property (nonatomic, copy) NSString *idStr;

/**是否是资讯*/
@property (nonatomic, assign) BOOL isNews;

/**资讯是否隐藏*/
@property (nonatomic, assign) BOOL isHidden;

/**跳转评论*/
@property (nonatomic, assign) BOOL isComment;

/**是否静音播放*/
@property (nonatomic, assign) BOOL isVolume;

/**是否从营地进入*/
@property (nonatomic, assign) BOOL isFromCamp;
@end

NS_ASSUME_NONNULL_END
