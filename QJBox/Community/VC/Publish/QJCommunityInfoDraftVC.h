//
//  QJCommunityInfoDraftVC.h
//  QJBox
//
//  Created by wxy on 2022/8/5.
//

#import "QJBaseViewController.h"
#import "QJArticleEditerVC.h"
NS_ASSUME_NONNULL_BEGIN

/// 咨询草稿箱
@interface QJCommunityInfoDraftVC : QJBaseViewController

@property (nonatomic, assign) QJEditerType type;//编辑类型0图片1视频2动态


@end

NS_ASSUME_NONNULL_END
