//
//  QJArticleEditerVC.h
//  QJBox
//
//  Created by wxy on 2022/8/1.
//

#import "QJBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN
@class QJCommunityDraftModel;
typedef NS_ENUM(NSUInteger, QJEditerType) {
    QJEditerTypeImage,
    QJEditerTypeVideo,
    QJEditerTypeDongTai,//动态
};


 



/// 资讯图文编辑器vc   全部必选
@interface QJArticleEditerVC : QJBaseViewController

//编辑类型
@property (nonatomic, assign) QJEditerType type;

@property (nonatomic, copy) NSString *campsiteID;

@property (nonatomic, strong) QJCommunityDraftModel *model;

@property (nonatomic, assign) BOOL isFromDraftVC;//是否从草稿箱跳转来的


@end

NS_ASSUME_NONNULL_END
