//
//  QJCommunityInfoDraftVC.m
//  QJBox
//
//  Created by wxy on 2022/8/5.
//

#import "QJCommunityInfoDraftVC.h"
#import "QJBottomDeleteAllView.h"
#import "QJCommunityInfoDraftTableCell.h"
#import "QJCommunityDraftManager.h"
#import "QJCommunityDraftModel.h"
#import "QJArticleEditerVC.h"
#import "QJDetailAlertView.h"
#import "TYAlertController.h"
#import "QJCommunityArticleCheckingRequest.h"
#import "QJCommunityArticleCheckModel.h"
#import "QJDraftEmptyView.h"
@interface QJCommunityInfoDraftVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) UIButton *btn;//管理按钮

@property (nonatomic, strong) QJBottomDeleteAllView *bottomView;

@property (nonatomic, strong) NSArray *dataArr;

@property (nonatomic, assign) BOOL isEdit;

@property (nonatomic, assign) CGFloat headViewHeight;

@property (nonatomic, strong) QJCommunityArticleCheckingRequest *checkingReq;//校验文章id

@end

@implementation QJCommunityInfoDraftVC

- (void)viewDidLoad {
    [super viewDidLoad];
     
    self.title = @"草稿箱";
    [self makeUI];
    [[QJCommunityDraftManager shared] readLocalDraftData];
 
    [self queryArticlesStatus];
    WS(weakSelf)
    self.bottomView.selectedAll = ^{
        DLog(@"全选");
        
        for (QJCommunityDraftModel *model in weakSelf.dataArr) {
            model.isSelected = YES;
        }
        
        [weakSelf.tableView reloadData];
    };
    self.bottomView.deleteAction = ^{
        DLog(@"删除");
        NSMutableIndexSet *set = [NSMutableIndexSet new];
        NSMutableArray *muArr = [NSMutableArray array];
        NSArray *totalArr = [QJCommunityDraftManager shared].modelArr;
        
        for (QJCommunityDraftModel *model in weakSelf.dataArr) {
            if (model.isSelected) {
                NSInteger index = [totalArr indexOfObject:model];
                [set addIndex:index];
                [muArr safeAddObject:model];
            }
            
            
        }
        QJDetailAlertView *alertView = [[QJDetailAlertView alloc] initWithFrame:CGRectZero titleType:QJDetailAlertViewTitleTypeDefault buttonType:QJDetailAlertViewButtonTypeDefault];
        [alertView showTitleText:@"提示" describeText:[NSString stringWithFormat:@"删除后无法恢复草稿,确认要删除%ld篇草稿吗?",set.count]];
        [alertView.leftButton setTitle:@"否" forState:UIControlStateNormal];
        [alertView.rightButton setTitle:@"是" forState:UIControlStateNormal];
        [alertView.rightButton setTitleColor:UIColorHex(007AFF) forState:UIControlStateNormal];
        
        [alertView returnClick:^(QJDetailAlertViewButtonClickType type) {
            [[QJAppTool getCurrentViewController] dismissViewControllerAnimated:YES completion:nil];
            if (type == QJDetailAlertViewButtonClickTypeRight) {
               
                NSMutableArray *arr = [NSMutableArray arrayWithArray:weakSelf.dataArr];
                [arr removeObjectsInArray:muArr.copy];
                weakSelf.dataArr = arr.copy;
                if (weakSelf.dataArr.count == 0) {
                    weakSelf.btn.hidden = YES;
                    weakSelf.bottomView.hidden = YES;
                    weakSelf.btn.selected = NO;
                }
                [[QJCommunityDraftManager shared] deleteModelArr:set];
                [weakSelf emptyViewMake];
                [weakSelf.tableView reloadData];
                [weakSelf.view makeToast:@"删除成功" duration:3 position:CSToastPositionCenter image:[UIImage imageNamed:@"success_login"]];
            }
        }];
        TYAlertController *alertController = [TYAlertController alertControllerWithAlertView:alertView preferredStyle:TYAlertControllerStyleAlert];
        [weakSelf presentViewController:alertController animated:YES completion:nil];
       
    };
}

- (void)emptyViewMake {
    
    if (self.dataArr.count == 0) {
        self.tableView.tableHeaderView = nil;
        self.headViewHeight = 0;
    }else {
        self.headViewHeight = 46;
    }
    QJDraftEmptyView *emptyView = [QJDraftEmptyView new];
    emptyView.string = @"暂无草稿";
    emptyView.imageStr = @"empty_no_data";
    [self.tableView tableViewDisplayWhenHaveNoDataWithView:emptyView ifNecessaryForRowCount:self.dataArr.count];
   
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self reloadDraft];
}

- (void)reloadDraft {
    NSMutableArray *muArr = [NSMutableArray array];
    NSArray *totalArr = [QJCommunityDraftManager shared].modelArr.copy;
    for (QJCommunityDraftModel *model in totalArr) {
        if (model.type == self.type && model.idStr.length == 0) {
            [muArr safeAddObject:model];
        }
    }
    self.dataArr = muArr.copy;
    if (self.dataArr.count == 0) {
        self.btn.hidden = YES;
        self.btn.selected = NO;
    }
    [self emptyViewMake];
    [self.tableView reloadData];
}


- (void)makeUI {
    self.navBar.rightBarItems = @[self.btn];
    self.view.backgroundColor = RGB(244, 244, 244);
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.bottomView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self.view);
            make.top.equalTo(self.navBar.mas_bottom);
            make.bottom.equalTo(self.view).offset(-Bottom_iPhoneX_SPACE);
    }];
    
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.left.right.equalTo(self.view);
            make.height.equalTo(@(50 + Bottom_iPhoneX_SPACE));
            
    }];
    
    
    
}

//查询文章状态 发布成功的本地数据删除掉，发布失败的放入草稿箱
- (void)queryArticlesStatus {
    [[QJCommunityDraftManager shared] readLocalPublishData];
    NSMutableArray *arr = [QJCommunityDraftManager shared].publishModelArr;
    NSMutableArray *idArr = [NSMutableArray array];
    for (QJCommunityDraftModel *model in arr.copy) {
        [idArr safeAddObject:model.idStr];
        
    }
    NSString *ids = [idArr componentsJoinedByString:@","];
    self.checkingReq.dic = @{@"ids":ids};
    WS(weakSelf)
    self.checkingReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            NSArray *dataArr = EncodeArrayFromDic(request.responseJSONObject, @"data");
            NSArray *articleArr = [NSArray modelArrayWithClass:[QJCommunityArticleCheckModel class] json:dataArr];
            NSMutableArray *muArr = [NSMutableArray array];
            NSMutableIndexSet *indexSet = [NSMutableIndexSet new];
            for (QJCommunityArticleCheckModel *model in articleArr) {
                if ([model.checkState isEqualToString:@"3"]) {//审核没过
                    for (QJCommunityDraftModel *draftModel in arr.copy) {
                        if ([draftModel.idStr isEqualToString:model.idStr]) {
                            draftModel.idStr = @"";
                            [muArr safeAddObject:draftModel];
                        }
                    }
                }else if ([model.checkState isEqualToString:@"2"]) {//通过审核了删除
                    int i = 0;
                    for (QJCommunityDraftModel *draftModel in arr.copy) {
                        if ([draftModel.idStr isEqualToString:model.idStr]) {
                            [indexSet addIndex:i];
                            i++;
                        }
                    }
                }
            }
            if (indexSet.count > 0) {
                [[QJCommunityDraftManager shared] deletePublishModelArr:indexSet.copy];
            }
            [[QJCommunityDraftManager shared] addModelArr:muArr.copy];//添加到草稿箱列表
            [weakSelf reloadDraft];
        }
    };
    
    self.checkingReq.failureCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
         
    };
    [self.checkingReq start];
    
}



#pragma mark ------ action

- (void)rightBtnAction:(UIButton *)sender {
    sender.selected = !sender.selected;
    self.bottomView.hidden = !sender.selected;
    if (self.bottomView.hidden) {
        [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.right.equalTo(self.view);
                make.top.equalTo(self.navBar.mas_bottom);
                make.bottom.equalTo(self.view).offset(-Bottom_iPhoneX_SPACE);
        }];
    }else{
        [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.right.equalTo(self.view);
                make.top.equalTo(self.navBar.mas_bottom);
                make.bottom.equalTo(self.bottomView);
        }];
    }
    self.isEdit = sender.selected;
    [self emptyViewMake];
    [self.tableView reloadData];
}



#pragma mark ------ getter

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        [_tableView registerClass:[QJCommunityInfoDraftTableCell class] forCellReuseIdentifier:@"QJCommunityInfoDraftTableCell"];
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.backgroundColor = RGB(244, 244, 244);
        
    }
    return _tableView;
}

- (UIButton *)btn {
    if (!_btn) {
        _btn = [UIButton new];
        [_btn addTarget:self action:@selector(rightBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [_btn setTitle:@"管理" forState:UIControlStateNormal];
        [_btn setTitleColor:kColorWithHexString(@"#007AFF") forState:UIControlStateNormal];
        [_btn setTitleColor:kColorWithHexString(@"#007AFF") forState:UIControlStateSelected];
        
        _btn.titleLabel.font = FontRegular(14);
        [_btn setTitle:@"取消" forState:UIControlStateSelected];
        
        
        
    }
    return _btn;
}

- (QJBottomDeleteAllView *)bottomView {
    if (!_bottomView) {
        _bottomView = [QJBottomDeleteAllView new];
        _bottomView.hidden = YES;
    }
    return _bottomView;
}
 
- (QJCommunityArticleCheckingRequest *)checkingReq {
    if (!_checkingReq) {
        _checkingReq = [QJCommunityArticleCheckingRequest new];
        
    }
    return _checkingReq;
}

#pragma mark ------- UITableViewDelegate, UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 116;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return self.headViewHeight;;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *bgView = [UIView new];
    bgView.backgroundColor = RGB(244, 244, 244);
    UILabel *label = [UILabel new];
    label.text = [NSString stringWithFormat:@"%ld篇草稿",self.dataArr.count];
    label.textColor = kColorWithHexString(@"#919599");
    label.font = FontRegular(14);
    label.textAlignment = NSTextAlignmentLeft;
    bgView.frame = CGRectMake(0, 0, kScreenWidth, 46);
    [bgView addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(bgView).offset(20);
            make.centerY.equalTo(bgView);
            make.right.equalTo(bgView);
    }];
    
    return bgView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    QJCommunityDraftModel *model = [self.dataArr safeObjectAtIndex:indexPath.row];
    QJCommunityInfoDraftTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QJCommunityInfoDraftTableCell" forIndexPath:indexPath];
    cell.isEdite = self.isEdit;
    cell.model = model;
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    QJCommunityDraftModel *model = [self.dataArr safeObjectAtIndex:indexPath.row];
    if (self.isEdit) {
        model.isSelected = !model.isSelected;
        [self.tableView reloadData];
        return;
    }
    
    QJArticleEditerVC *vc = [QJArticleEditerVC new];
    vc.model = model;
    vc.campsiteID = model.campId;
    vc.isFromDraftVC = YES;
    [self.navigationController pushViewController:vc animated:YES];
    
}

@end
