//
//  QJPublishNewsTagsViewController.h
//  QJBox
//
//  Created by wxy on 2022/8/4.
//

#import "QJBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJPublishNewsTagsViewController : QJBaseViewController

@property (nonatomic, copy) void (^tagViewClickEvent)(NSArray *tagArray);

@property (nonatomic, strong) NSArray *selectTagArray;
@end

NS_ASSUME_NONNULL_END
