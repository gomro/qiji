//
//  QJDraftViewController.m
//  QJBox
//
//  Created by Sun on 2022/7/22.
//

#import "QJDraftViewController.h"
#import "QJCampsiteRequest.h"
#import "QJDraftViewCell.h"
#import "QJDraftModel.h"

@interface QJDraftViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
/**数据*/
@property (nonatomic, strong) NSMutableArray *dataArr;
@end

@implementation QJDraftViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"草稿箱";
    self.view.backgroundColor = [UIColor whiteColor];
    [self loadUI];

    [self getDraftsList];
    self.tableView.mj_header = [QJRefreshHeader headerWithRefreshingBlock:^{
        [self getDraftsList];

        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.tableView.mj_header endRefreshing];
        
        });
    }];
}

#pragma mark - Load UI
- (void)loadUI {
    [self.view addSubview:self.tableView];
    
    [self makeConstraints];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
}

- (void)makeConstraints {
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_top).offset(NavigationBar_Bottom_Y);
        make.left.bottom.right.mas_equalTo(0);
    }];
}

#pragma mark - Lazy Loading
- (UITableView *)tableView {
    if (!_tableView) {
        self.tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        self.tableView.backgroundColor = UIColorHex(F5F5F5);
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.tableView.showsVerticalScrollIndicator = NO;
        self.tableView.showsHorizontalScrollIndicator = NO;
        self.tableView.estimatedRowHeight = 200;
        if (@available(iOS 11.0, *)) {
            self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
    }
    return _tableView;
}

- (NSMutableArray *)dataArr {
    if (!_dataArr) {
        self.dataArr = [NSMutableArray arrayWithCapacity:0];
    }
    return _dataArr;
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellMineList = @"cellQJDraftViewCellId";
   
    QJDraftViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellMineList];
    if (!cell) {
        cell = [[QJDraftViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellMineList];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    QJDraftModel *model = self.dataArr[indexPath.row];
    [cell configureCellWithData:model];
    return cell;
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    QJDraftModel *model = self.dataArr[indexPath.row];
    [[NSNotificationCenter defaultCenter] postNotificationName:QJDraftNotification object:nil userInfo:@{ @"draftId":kCheckStringNil(model.draftId)}];

}

#pragma mark - NetWork
- (void)getDraftsList {
    QJCampsiteRequest *startRequest = [[QJCampsiteRequest alloc] init];
    [startRequest netWorkGetDraftsList:@"1"];
    [startRequest setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            NSArray *dataArr = request.responseJSONObject[@"data"][@"records"];
            [self.dataArr removeAllObjects];
            for (NSDictionary *dic in dataArr) {
                QJDraftModel *model = [QJDraftModel modelWithDictionary:dic];
                [self.dataArr addObject:model];
            }
            [self.tableView reloadData];
        }
    }];
}
@end
