//
//  QJPublishNewsTagsViewController.m
//  QJBox
//
//  Created by wxy on 2022/8/4.
//

#import "QJPublishNewsTagsViewController.h"
#import "QJNewsTagView.h"
#import "QJProductSearchView.h"
#import "QJCampsiteRequest.h"
#import "QJNewsTagModel.h"
@interface QJPublishNewsTagsViewController ()

/** 暗色背景 */
@property (nonatomic, strong) UIView *bgView;
/** 控件View */
@property (nonatomic, strong) UIView *contentView;

@property (nonatomic, strong) QJNewsTagView *tagView;//标签

@property (nonatomic, strong) QJProductSearchView *searchView;

@property (nonatomic, strong) NSArray *dataArr;

@property (nonatomic, strong) UIButton *canCelBtn;//取消按钮

@property (nonatomic, strong) UIView *lineView;

@property (nonatomic, strong) UILabel *topLabel;

@end

@implementation QJPublishNewsTagsViewController

- (instancetype)init {
    if (self = [super init]) {
        self.modalPresentationStyle = UIModalPresentationCustom;
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.navBar.hidden = YES;
    [self getGetInfoTag:@""];
    [self setUI];
    WS(weakSelf)
    self.tagView.tagViewClickEvent = ^(NSArray * _Nonnull tagArray,NSArray *modelArr) {
        if (weakSelf.tagViewClickEvent) {
            weakSelf.tagViewClickEvent(modelArr);
        }
        [weakSelf clickBgView];
       
    };
    
    self.searchView.searchStrBlock = ^(NSString * _Nonnull str) {
        [weakSelf getGetInfoTag:str];
    };
    
    
   
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self startAnimation];
}

- (void)startAnimation {
    CGFloat height = 470 + Bottom_SN_iPhoneX_OR_LATER_SPACE;
    self.bgView.frame = [UIScreen mainScreen].bounds;
    [UIView animateWithDuration:0.4 delay:0 usingSpringWithDamping:0.95 initialSpringVelocity:0.05 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        
        self.bgView.alpha = 1.0f ;
        
        self.contentView.frame = CGRectMake(0, [UIScreen mainScreen].bounds.size.height-height, CGRectGetWidth([UIScreen mainScreen].bounds), height);
        
    } completion:^(BOOL finished) {
    }];
}

- (void)setUI {
    
    self.view.backgroundColor = [UIColor clearColor];
    UIView *bgView = [[UIView alloc] init];
    bgView.backgroundColor = [UIColor colorWithRed:0.25 green:0.25 blue:0.25 alpha:0.5f];
    [self.view addSubview:bgView];
    self.bgView = bgView;
    self.bgView.alpha = 0.0f;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickBgView)];
    [bgView addGestureRecognizer:tap];
    
    UIView *cView = [[UIView alloc] init];
    cView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:cView];
    self.contentView = cView;
    self.contentView.layer.cornerRadius = 24;
    self.contentView.layer.masksToBounds = YES;
    CGFloat height = 470 + Bottom_SN_iPhoneX_OR_LATER_SPACE;
    self.contentView.frame = CGRectMake(0,[UIScreen mainScreen].bounds.size.height, CGRectGetWidth([UIScreen mainScreen].bounds), height);
     
    [self.contentView addSubview:self.tagView];
    [self.contentView addSubview:self.searchView];
    
    
    [self.contentView addSubview:self.topLabel];
    [self.contentView addSubview:self.lineView];
    [self.contentView addSubview:self.canCelBtn];
    
    
    [self.topLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.contentView).offset(16);
            make.centerX.equalTo(self.contentView);
        make.height.equalTo(@17);
    }];
    
    [self.searchView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(16);
            make.right.equalTo(self.contentView).offset(63-16);
            make.height.equalTo(@34);
            make.top.equalTo(self.topLabel.mas_bottom).offset(24);
    }];
    
    [self.tagView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(8);
        make.right.equalTo(self.contentView).offset(-8);
        make.bottom.equalTo(self.lineView);
        make.top.equalTo(self.searchView.mas_bottom).offset(24);
    }];
    
    [self.canCelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.contentView).offset(-Bottom_SN_iPhoneX_OR_LATER_SPACE);
            make.right.left.equalTo(self.contentView);
            make.height.equalTo(@48);
    }];
    
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.canCelBtn.mas_top);
            make.left.right.equalTo(self.contentView);
            make.height.equalTo(@2);
    }];
    
    
    
    
}

// 获取标签数组
- (void)getGetInfoTag:(NSString *)keyWords {
    QJCampsiteRequest *startRequest = [[QJCampsiteRequest alloc] init];
    [startRequest netWorkGetInfoTagKeyword:keyWords?:@""];
    [startRequest setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            NSArray *dataArr = request.responseJSONObject[@"data"];
            NSMutableArray *tagArr = [NSMutableArray array];
            for (NSDictionary *dic in dataArr) {
                QJNewsTagModel *model = [QJNewsTagModel modelWithDictionary:dic];
                [tagArr addObject:model];
            }
            self.dataArr = tagArr.copy;
            self.tagView.selectTagArray = self.selectTagArray.mutableCopy;
            self.tagView.tagModelArray = self.dataArr;
           
        }
    }];
}


#pragma mark ---- setter getter

- (QJNewsTagView *)tagView {
    if (!_tagView) {
        _tagView = [QJNewsTagView new];
        _tagView.isSingleChoice = YES;
        _tagView.backgroundColor = [UIColor whiteColor];
    }
    return _tagView;
}

- (QJProductSearchView *)searchView {
    if (!_searchView) {
        _searchView = [QJProductSearchView new];
        NSMutableAttributedString *placeholderString = [[NSMutableAttributedString alloc] initWithString:@"输入关键词搜索标签" attributes:@{NSForegroundColorAttributeName : UIColorFromRGB(0xC8CACC), NSFontAttributeName : kFont(14)}];
        _searchView.searchTF.attributedPlaceholder = placeholderString;
        _searchView.hiddenCancelBtn = YES;
    }
    return _searchView;
}

- (NSArray *)dataArr {
    if (!_dataArr) {
        _dataArr = [NSArray array];
    }
    return _dataArr;
}


- (UIButton *)canCelBtn {
    if (!_canCelBtn) {
        _canCelBtn = [UIButton new];
        [_canCelBtn addTarget:self action:@selector(cancelBtnAction) forControlEvents:UIControlEventTouchUpInside];
        [_canCelBtn setTitle:@"取消" forState:UIControlStateNormal];
        [_canCelBtn setTitleColor:kColorWithHexString(@"#16191C") forState:UIControlStateNormal];
        _canCelBtn.titleLabel.font = FontRegular(14);
        _canCelBtn.backgroundColor = [UIColor whiteColor];
    }
    return _canCelBtn;
}

- (UIView *)lineView {
    if (!_lineView) {
        _lineView = [UIView new];
        _lineView.backgroundColor = kColorWithHexString(@"#F5F5F5");
    }
    return _lineView;
}


- (UILabel *)topLabel {
    if (!_topLabel) {
        _topLabel = [UILabel new];
        _topLabel.font = FontMedium(16);
        _topLabel.textColor = kColorWithHexString(@"#16191C");
        _topLabel.textAlignment = NSTextAlignmentCenter;
        _topLabel.text = @"选择标签";
    }
    return _topLabel;
}

#pragma mark ---- action

- (void)cancelBtnAction {
    
    [self clickBgView];
}
 

#pragma mark - 消失
- (void)clickBgView
{
    [UIView animateWithDuration:0.4 delay:0 usingSpringWithDamping:0.95 initialSpringVelocity:0.05 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        
        self.bgView.alpha = 0.0f ;
        self.contentView.frame = CGRectMake(0,[UIScreen mainScreen].bounds.size.height, CGRectGetWidth([UIScreen mainScreen].bounds), 470+Bottom_SN_iPhoneX_OR_LATER_SPACE);
        
    } completion:^(BOOL finished) {
        // 动画Animated必须是NO，不然消失之后，会有0.35s时间，再点击无效
        [self dismissViewControllerAnimated:NO completion:nil];
    }];
}

// 这里主动释放一些空间，加速内存的释放，防止有时候消失之后，再点不出来。
- (void)dealloc
{
    NSLog(@"%@ --> dealloc",[self class]);
    [self.bgView removeFromSuperview];
    self.bgView = nil;
    [self.contentView removeFromSuperview];
    self.contentView = nil;
}



@end
