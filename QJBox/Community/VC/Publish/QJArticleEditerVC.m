//
//  QJArticleEditerVC.m
//  QJBox
//
//  Created by wxy on 2022/8/1.
//

#import "QJArticleEditerVC.h"
#import <RZRichTextView-Swift.h>
#import "RZColorful.h"
#import "ICChatBoxFaceView.h"
#import "XZEmotion.h"
#import "ICMessageConst.h"
#import "NSString+Extension.h"
#import "TZImagePickerController.h"
#import "QJFontStyleChoiceBar.h"
#import "QJCampsiteRequest.h"
#import "QJCommunityNewsRequest.h"
#import "QJPublishNewsTagsViewController.h"
#import "QJNewsTagModel.h"
#import "QJNewsTagItem.h"
#import "QJVideoImageViewController.h"
#import "QJWKWebViewController.h"
#import "QJDetailAlertView.h"
#import "TYAlertController.h"
#import "QJCommunityLocalDraftModel.h"
#import "QJCommunityDraftManager.h"
#import "QJCommunityInfoDraftVC.h"
#import "QJCommunityDraftModel.h"
#import "QJMineImageViewCropViewController.h"
#import "UIImage+Resize.h"


static  int enterCount = 0;
static  int leaveCount = 0;
@interface QJArticleEditerVC ()<UITextViewDelegate,QJMineImageViewCropControllerDelegate>

@property (nonatomic, strong) UIScrollView *bgScrollView;

@property (nonatomic, strong) UIButton *imageBtn;

@property (nonatomic, strong) UIButton *deleteBtn;//删除视频按钮(只有动态时才有)

@property (nonatomic, strong) UILabel *imageBottomLabel;//上传封面

@property (nonatomic, strong) UIButton *publishBtn;

@property (nonatomic, strong) UIButton *publishButton;

@property (nonatomic, strong) UIButton *reUpVideoBtn;//重新上传视频按钮

@property (nonatomic, strong) UITextView *titleText;

@property (nonatomic, strong) UILabel *titleNum;

@property (nonatomic, strong) UIView *lineView;

@property (nonatomic, strong) RZRichTextView *richTextView;

@property (nonatomic, strong) UILabel *richTextPlaceHolder;

@property (nonatomic, strong) ICChatBoxFaceView *faceListView;

@property (nonatomic, strong) UIButton *addLabelBtn;

@property (nonatomic, strong) UILabel *richNumLabel;

@property (nonatomic, strong) UIButton *originalBtn;//原创

@property (nonatomic, strong) UIButton *forwardBtn;//转发

@property (nonatomic, strong) QJNewsTagItem *bottomTagView;

@property (nonatomic, strong) UIImageView *tagRightImageView;

@property (nonatomic, strong) UIView *bottomView;

//@property (nonatomic, strong) UIButton *emojiBtn;

@property (nonatomic, strong) UIButton *ABtn;

@property (nonatomic, strong) UIButton *pictureBtn;

@property (nonatomic, strong) UILabel *titleTextPlaceholder;

@property (nonatomic, assign) BOOL isClickEmojiBtn;

@property (nonatomic, assign) BOOL isTitleTextViewActivity;//YES 标题输入框活跃，No内容输入框活跃

@property (nonatomic, strong) QJFontStyleChoiceBar *fontStyleBar;

@property (nonatomic, strong) RZToolBarItem *fontItem;

//数据处理

@property (nonatomic, strong) UIImage *coverImage;//封面图数据

@property (nonatomic, strong) NSMutableArray *contentImageArr;//内容数组

@property (nonatomic, strong) NSMutableArray *contentImageKeys;//保存上传图片每张图片对应的key


@property (nonatomic, strong) NSMutableArray *publishModel;

@property (nonatomic, strong) dispatch_group_t group;

@property (nonatomic, strong) NSString *overImageName;

@property (nonatomic, strong) NSMutableArray *imageUrls;

@property (nonatomic, strong) NSURL *videoUrl;//视频

@property (nonatomic, strong) QJCommunityNewsRequest *newsPostReq;//发布咨询

@property (nonatomic, strong) NSArray *tagArr;//标签

@property (nonatomic, assign) NSInteger contentImageCount;//文本中图片数量 动态最多支持9张，资讯支持最多15张

@property (nonatomic, assign) BOOL videoIsvertical;//是否是垂直视频


@end

@implementation QJArticleEditerVC


- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void)onBack:(id)sender {
    if (self.titleText.text.length > 0 || self.richTextView.attributedText.length > 0 || self.videoUrl.absoluteString.length > 0 || self.coverImage || self.originalBtn.selected || self.forwardBtn.selected || self.tagArr.count > 0) {
        QJDetailAlertView *alertView = [[QJDetailAlertView alloc] initWithFrame:CGRectZero titleType:QJDetailAlertViewTitleTypeDefault buttonType:QJDetailAlertViewButtonTypeDefault];
        NSString *content = @"未发布内容将会保存在草稿箱中是否退出发布?";
        NSString *leftTitle = @"是";
        NSString *rightTitle = @"否";
        
        if (self.type == QJEditerTypeDongTai) {
            content = @"您还未保存，是否存入草稿";
            leftTitle = @"取消";
            rightTitle = @"确认";
        }
        
        [alertView showTitleText:@"提示" describeText:content];
        [alertView.leftButton setTitle:leftTitle forState:UIControlStateNormal];
        [alertView.rightButton setTitle:rightTitle forState:UIControlStateNormal];
        [alertView.rightButton setTitleColor:UIColorHex(007AFF) forState:UIControlStateNormal];
        
        [alertView returnClick:^(QJDetailAlertViewButtonClickType type) {
            [[QJAppTool getCurrentViewController] dismissViewControllerAnimated:YES completion:nil];
            if (type == QJDetailAlertViewButtonClickTypeRight) {
                DLog(@"保存到草稿");
                if (self.type == QJEditerTypeDongTai) {
                    [self.view endEditing:YES];
                    [self saveTodraftWithId:@""];
                    if (self.presentingViewController && !self.isFromDraftVC) {
                        [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
                    }else{
                        [self.navigationController popViewControllerAnimated:YES];
                    }
                }
                
            }else{
                if (self.type != QJEditerTypeDongTai) {
                    [self.view endEditing:YES];
                    [self saveTodraftWithId:@""];
                }
                if (self.presentingViewController && !self.isFromDraftVC) {
                    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
                }else{
                    [self.navigationController popViewControllerAnimated:YES];
                }
            }
        }];
        TYAlertController *alertController = [TYAlertController alertControllerWithAlertView:alertView preferredStyle:TYAlertControllerStyleAlert];
        [self presentViewController:alertController animated:YES completion:nil];
        
    }else{
        if (self.presentingViewController && !self.isFromDraftVC) {
            [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
        }else{
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
   
}




- (void)viewDidLoad {
    [super viewDidLoad];
    if (self.model) {
        self.type = self.model.type;
    }
    [self setNavUI];
    [self setViewUI];
    if (self.model) {
        [self makeDraftContent];
    }
    
    [self.view addSubview:self.faceListView];
    [IQKeyboardManager sharedManager].enableAutoToolbar = NO;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    //增加监听，当键退出时收出消息
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(emotionDidSelected:) name:GXEmotionDidSelectNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deleteBtnClicked) name:GXEmotionDidDeleteNotification object:nil];
}


#pragma mark  ---- setNav

- (void)setNavUI {
    UIButton *draftButton = [UIButton new];
    [draftButton setTitle:@"草稿" forState:UIControlStateNormal];
    [draftButton setTitleColor:UIColorHex(474849) forState:UIControlStateNormal];
    draftButton.titleLabel.font = FontSemibold(14);
    [draftButton addTarget:self action:@selector(draftView:) forControlEvents:UIControlEventTouchUpInside];
    
    self.navBar.rightBarItems = @[self.publishBtn,draftButton];
    if (self.model) {
        draftButton.hidden = YES;
    }
    self.navBar.titleLabel.textColor = kColorWithHexString(@"#16191C");
    self.navBar.titleLabel.font = FontSemibold(16);
    switch (self.type) {
        case QJEditerTypeImage:
            self.title = @"发布图文";
            [self.reUpVideoBtn setTitle:@"上传封面" forState:UIControlStateNormal];
            break;
        case QJEditerTypeVideo:
            self.title = @"发布视频";
            [self.reUpVideoBtn setTitle:@"上传视频" forState:UIControlStateNormal];
            break;
        case QJEditerTypeDongTai:
            self.title = @"动态";
            [self.reUpVideoBtn setTitle:@"上传视频" forState:UIControlStateNormal];
            break;
        default:
            self.title = @"发布图文";
            break;
    }
    
    
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
}

 

- (void)setViewUI {
    WS(weakSelf)
       
    [self.view addSubview:self.bgScrollView];
    [self.bgScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view);
            make.width.equalTo(@(kScreenWidth));
            make.height.equalTo(@(kScreenHeight));
            make.top.equalTo(self.navBar.mas_bottom);
    }];
    
    
    
    [self.bgScrollView addSubview:self.imageBtn];
    [self.bgScrollView addSubview:self.deleteBtn];
    [self.bgScrollView addSubview:self.reUpVideoBtn];
    [self.bgScrollView addSubview:self.titleText];
    [self.titleText addSubview:self.titleTextPlaceholder];
    [self.bgScrollView addSubview:self.titleNum];
    [self.bgScrollView addSubview:self.lineView];
    [self.bgScrollView addSubview:self.richTextView];
    [self.richTextView addSubview:self.richTextPlaceHolder];
    [self.bgScrollView addSubview:self.richNumLabel];
    [self.view addSubview:self.tagRightImageView];
    [self.view addSubview:self.bottomTagView];
    [self.view addSubview:self.addLabelBtn];

    [self.view addSubview:self.originalBtn];
    [self.view addSubview:self.forwardBtn];

    [self.view addSubview:self.bottomView];
//    [self.bottomView addSubview:self.emojiBtn];
    [self.bottomView addSubview:self.ABtn];
    [self.bottomView addSubview:self.pictureBtn];

    [self.view addSubview:self.fontStyleBar];

    self.view.backgroundColor = [UIColor whiteColor];
    [self.imageBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.bgScrollView);
        make.width.equalTo(@114);
        make.height.equalTo(@76);
        make.top.equalTo(self.bgScrollView.mas_top).offset(12);

    }];

    [self.deleteBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.imageBtn.mas_right);
            make.centerY.equalTo(self.imageBtn.mas_top);
            make.width.height.equalTo(@30);
    }];

    [self.reUpVideoBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.imageBtn);
            make.width.equalTo(@80);
            make.height.equalTo(@24);
        make.top.equalTo(self.imageBtn.mas_bottom).offset(16);
    }];


    [self.titleText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@44.6);
        make.left.equalTo(self.view).offset(16);
        make.right.equalTo(self.view).offset(-16);
        make.top.equalTo(self.imageBtn.mas_bottom).offset(42);
    }];

    [self.titleTextPlaceholder mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.titleText).offset(8);
        make.right.equalTo(self.titleText);
        make.height.equalTo(@17);
        make.top.equalTo(self.titleText).offset(10);
    }];

    [self.titleNum mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.titleText);
        make.height.equalTo(@17);
        make.top.equalTo(self.titleText.mas_bottom);
        make.width.equalTo(@100);
    }];

    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.height.equalTo(@1);
        make.top.equalTo(self.titleNum.mas_bottom).offset(15);
    }];

    [self.richTextView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).offset(16);
        make.top.equalTo(self.lineView.mas_bottom).offset(16);
        CGFloat offset = 134 + Bottom_iPhoneX_SPACE;
        make.bottom.equalTo(self.view).offset(-offset);
        make.right.equalTo(self.view).offset(-16);
    }];

    [self.richTextPlaceHolder mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.equalTo(self.richTextView).offset(6);

    }];

    [self.richNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.richTextView);
        make.height.equalTo(@15);
        make.top.equalTo(self.richTextView.mas_bottom);
    }];


    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.view).offset(-Bottom_iPhoneX_SPACE);
        make.left.right.equalTo(self.view);
        make.height.equalTo(@48);
    }];

//    [self.emojiBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(self.bottomView).offset(4);
//        make.width.height.equalTo(@48);
//        make.centerY.equalTo(self.bottomView);
//    }];
    [self.ABtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(self.emojiBtn.mas_right);
//        make.size.centerY.equalTo(self.emojiBtn);
        make.left.equalTo(self.bottomView).offset(4);
        make.width.height.equalTo(@48);
        make.centerY.equalTo(self.bottomView);
    }];
    [self.pictureBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.ABtn.mas_right);
        make.width.height.equalTo(@48);
    }];

    [self.bottomTagView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).offset(16);
        make.height.equalTo(@24);

        make.bottom.equalTo(self.bottomView.mas_top).offset(-12);
    }];

    [self.tagRightImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.bottomTagView.mas_right).offset(8);
        make.centerY.equalTo(self.bottomTagView);
        make.width.height.equalTo(@16);
    }];

    [self.addLabelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).offset(16);
        make.height.equalTo(@(Get375Width(24)));
        make.right.equalTo(self.originalBtn.mas_left).offset(-31);
        make.bottom.equalTo(self.bottomView.mas_top).offset(-12);
    }];

    [self.forwardBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.bottomView).offset(-18);
        make.width.equalTo(@(Get375Width(84)));
        make.height.equalTo(@(Get375Width(48)));
        make.bottom.equalTo(self.bottomView.mas_top);
    }];

    [self.originalBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.forwardBtn.mas_left);
        make.width.equalTo(@(Get375Width(84)));
        make.height.equalTo(@(Get375Width(48)));
        make.bottom.equalTo(self.bottomView.mas_top);
    }];

    self.fontStyleBar.choiceIndex = ^(NSDictionary * _Nonnull typingAttributes) {
        if (typingAttributes.allKeys.count > 0) {
            weakSelf.fontItem.selected = YES;
            weakSelf.ABtn.selected = YES;
        }else{
            weakSelf.fontItem.selected = NO;
        }
        [weakSelf.richTextView.kinputAccessoryView reloadData];
        weakSelf.richTextView.typingAttributes = typingAttributes;
    };
    [self.fontStyleBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.height.equalTo(@48);
        make.bottom.equalTo(self.bottomView.mas_top);
    }];
    
    
    
}

//草稿箱数据填写
- (void)makeDraftContent {
    self.coverImage = self.model.overImage;
    self.videoUrl = self.model.viderUrl;
    self.originalBtn.selected = self.model.isOrignal;
    self.forwardBtn.selected = self.model.isForward;
    if (self.coverImage) {
        [self.imageBtn setImage:self.coverImage forState:UIControlStateNormal];
        [self.reUpVideoBtn setTitle:@"重新上传" forState:UIControlStateNormal];
        if (self.type == QJEditerTypeDongTai || self.type == QJEditerTypeVideo) {
            UIView *bg = nil;
            if (![self.imageBtn viewWithTag:1001]) {
                bg = [UIView new];
                bg.userInteractionEnabled = NO;
                bg.tag = 1001;
                UILabel *labe = [UILabel new];
                labe.font = FontRegular(12);
                labe.textColor = [UIColor whiteColor];
                [bg addSubview:labe];
                labe.text = @"选择封面";
                labe.textAlignment = NSTextAlignmentCenter;
                [labe mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.center.equalTo(bg);
                }];
            }
            
            
            bg.backgroundColor = [UIColor hx_colorWithHexStr:@"000000" alpha:0.6];
            [self.imageBtn addSubview:bg];
            [bg mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.right.equalTo(self.imageBtn);
                make.height.equalTo(@20);
                make.bottom.equalTo(self.imageBtn);
            }];
        }
    }
 
    if (self.model.richAttributedString.length > 0) {
        [self.model.richAttributedString enumerateAttributesInRange:NSMakeRange(0, self.model.richAttributedString.length) options:NSAttributedStringEnumerationLongestEffectiveRangeNotRequired usingBlock:^(NSDictionary<NSAttributedStringKey,id> * _Nonnull attrs, NSRange range, BOOL * _Nonnull stop) {
            NSString *string = self.model.richAttributedString.string;
            NSString *selectString = [string substringWithRange:range];
            
            if ([selectString isEqualToString:@"\U0000fffc"]) {
                NSTextAttachment *attachment = attrs[NSAttachmentAttributeName];
                UIImage *image = attachment.image;
                if (image) {
                    CGFloat width = image.size.width;
                    CGFloat height = image.size.height;
                    if (width > kScreenWidth - 32 - 20) {
                        width = kScreenWidth - 32 - 20;
                        height = height *(width/image.size.width);
                    }
                    attachment.bounds = CGRectMake(0, 0, width, height);
                }
            }
          
        }];
        
    }
    
    self.richTextView.attributedText = self.model.richAttributedString;
    self.titleText.text = self.model.title;
    if (self.titleText.text.length > 0) {
        self.titleTextPlaceholder.hidden = YES;
    }
    
    if (self.richTextView.attributedText.length > 0) {
        self.richTextPlaceHolder.hidden = YES;
    }
    if (self.model.tag) {
        self.bottomTagView.title = self.model.tag.name;
        self.bottomTagView.titleColor = kColorWithHexString(@"#7341A3");
        self.bottomTagView.backgroundColor = kColorWithHexString(@"#F2E5FF");
        self.bottomTagView.hidden = NO;
        self.tagRightImageView.hidden = NO;
        self.addLabelBtn.backgroundColor = [UIColor clearColor];
        [self.addLabelBtn setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
        [self.addLabelBtn setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        self.tagRightImageView.hidden = YES;
        self.tagArr = @[self.model.tag];
    }
    [self canPublish];
}

//选择视频
- (void)choiseVideo{
    TZImagePickerController *vc = [[TZImagePickerController alloc]initWithMaxImagesCount:1 delegate:nil];
    vc.allowPickingVideo = YES;
    vc.allowPickingImage = NO;
    
    vc.allowPickingOriginalPhoto = NO;
    vc.didFinishPickingVideoHandle = ^(UIImage *coverImage, PHAsset *asset) {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self choiseVideoAfter:coverImage outputPath:@"" asset:asset];
        });
       
    };
    
    vc.didFinishPickingAndEditingVideoHandle =  ^(UIImage *coverImage, NSString *outputPath, NSString *errorMsg) {
        dispatch_async(dispatch_get_main_queue(), ^{
        [self choiseVideoAfter:coverImage outputPath:outputPath asset:nil];
        });
    };
    vc.modalPresentationStyle = UIModalPresentationCustom;
    [self presentViewController:vc animated:YES completion:nil];
}


- (void)choiseVideoAfter:(UIImage *)coverImage outputPath:(NSString *)outputPath asset:(PHAsset *)asset {
    
    if (coverImage) {
        self.imageBtn.imageView.contentMode =UIViewContentModeScaleAspectFill;
        self.coverImage = coverImage;
        [self.imageBtn setImage:coverImage forState:UIControlStateNormal];
        UIView *bg = nil;
        if (![self.imageBtn viewWithTag:1001]) {
            bg = [UIView new];
            bg.userInteractionEnabled = NO;
            bg.tag = 1001;
            UILabel *labe = [UILabel new];
            labe.font = FontRegular(12);
            labe.textColor = [UIColor whiteColor];
            [bg addSubview:labe];
            labe.text = @"选择封面";
            labe.textAlignment = NSTextAlignmentCenter;
            [labe mas_makeConstraints:^(MASConstraintMaker *make) {
                make.center.equalTo(bg);
            }];
           
        }
        
        if (self.type == QJEditerTypeDongTai) {
            self.deleteBtn.hidden = NO;
            
        }
        [self.reUpVideoBtn setTitle:@"重新上传" forState:UIControlStateNormal];
        bg.hidden = NO;
        bg.backgroundColor = [UIColor hx_colorWithHexStr:@"000000" alpha:0.6];
        [self.imageBtn addSubview:bg];
        [bg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self.imageBtn);
            make.height.equalTo(@20);
            make.bottom.equalTo(self.imageBtn);
        }];
    }
    if (outputPath.length > 0) {
        NSURL *videoURL =  [NSURL fileURLWithPath:outputPath];
        self.videoUrl = videoURL;
        
    }else{
        if (asset.mediaType == PHAssetMediaTypeVideo) {
            PHVideoRequestOptions *options = [[PHVideoRequestOptions alloc]init];
            options.version = PHVideoRequestOptionsVersionCurrent;
            options.deliveryMode = PHVideoRequestOptionsDeliveryModeAutomatic;
            PHImageManager *manager = [PHImageManager defaultManager];
            [manager requestAVAssetForVideo:asset options:options resultHandler:^(AVAsset * _Nullable asset, AVAudioMix * _Nullable audioMix, NSDictionary * _Nullable info) {
                AVURLAsset *urlAsset = (AVURLAsset *)asset;
                
                NSURL *url = urlAsset.URL;
                [self fixVideoDirection:urlAsset];
                self.videoUrl = url;
                [self canPublish];
                
            }];
        }
    }
    [self canPublish];
    
}

#pragma mark --------- 处理视频方向
- (void)fixVideoDirection:(AVURLAsset *)asset
{
    // 获取视频轨道
    AVAssetTrack *videoTrack = [asset tracksWithMediaType:AVMediaTypeVideo].firstObject;
    // 获取视频修正方向（默认为摄像头方向）
    CGAffineTransform t = videoTrack.preferredTransform;
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGSize renderSize = CGSizeMake(videoTrack.naturalSize.width, videoTrack.naturalSize.height);
    DLog(@"之前视频的宽高是多少%@",NSStringFromCGSize(renderSize));
    if(t.a == 1.0 && t.b == 0 && t.c == 0 && t.d == 1.0) { // LandscapeRight, 0度
       //不需要作处理
        NSLog(@"视频没有被旋转");
        self.videoIsvertical = NO;
        
   } else if (t.a == 0 && t.b == 1.0 && t.c == -1.0 && t.d == 0){ //90度
       
        transform = CGAffineTransformTranslate(transform, videoTrack.naturalSize.height, 0);
        transform = CGAffineTransformRotate(transform, M_PI_2*1);
        renderSize = CGSizeMake(videoTrack.naturalSize.height, videoTrack.naturalSize.width);
        NSLog(@"视频被旋转了90度");
       self.videoIsvertical = YES;
       
    } else if (t.a == -1.0 && t.b == 0 && t.c == 0 && t.d == -1.0) { //180度
        
        transform = CGAffineTransformTranslate(transform, videoTrack.naturalSize.width, videoTrack.naturalSize.height);
        transform = CGAffineTransformRotate(transform, M_PI_2*2);
        renderSize = CGSizeMake(videoTrack.naturalSize.width, videoTrack.naturalSize.height);
        NSLog(@"视频被旋转了180度");
        self.videoIsvertical = NO;

    } else if(t.a == 0 && t.b == -1.0 && t.c == 1.0 && t.d == 0) { //270度
        
        transform = CGAffineTransformTranslate(transform, 0, videoTrack.naturalSize.width);
        transform = CGAffineTransformRotate(transform, M_PI_2*3);
        renderSize = CGSizeMake(videoTrack.naturalSize.height, videoTrack.naturalSize.width);
        NSLog(@"视频被旋转了270度");
        self.videoIsvertical = YES;
    }
    
   
 
}

 

//isStr发布后的文章id
- (void)saveTodraftWithId:(NSString *)idStr {
    if (self.model) {//如果是草稿箱过来的 移除老数据
        NSMutableIndexSet *set = [NSMutableIndexSet new];
        NSMutableArray *arr = [QJCommunityDraftManager shared].modelArr;
        if (arr.count > 0 && [arr containsObject:self.model]) {
            NSInteger index = [arr indexOfObject:self.model];
            [set addIndex:index];
            [[QJCommunityDraftManager shared] deleteModelArr:set];
        }
        
         
    }
    QJCommunityDraftModel *model = [QJCommunityDraftModel new];
    model.type = self.type;
    model.campId = self.campsiteID;
    model.idStr = idStr;
    model.timesp = [[NSDate date] timeIntervalSince1970];
    model.overImage = self.coverImage;
    if (self.videoUrl) {
        model.viderUrl = self.videoUrl;
    }
    if (self.titleText.text.length > 0) {
        model.title = self.titleText.text;
    }
    
    if (self.richTextView.attributedText.length > 0) {
        model.richAttributedString = self.richTextView.attributedText;
    }
    if (self.originalBtn.selected || self.forwardBtn.selected) {
        model.isOrignal = self.originalBtn.selected;
        model.isForward = self.forwardBtn.selected;
    }
    if (self.tagArr.count > 0) {
        QJNewsTagModel *tag = self.tagArr.firstObject;
        model.tag = tag;
    }
    if (idStr.length > 0) {
        [[QJCommunityDraftManager shared] addPublishModel:model];//保存发布数据到本地
    }else{
        [[QJCommunityDraftManager shared] addModel:model];//保存草稿到本地
    }
    
}

#pragma mark --- btnAction

//上传图片
- (void)btnAction:(UIButton *)btn {
    switch (self.type) {
        case  QJEditerTypeImage: {
            DLog(@"图片");
            TZImagePickerController *vc = [[TZImagePickerController alloc]initWithMaxImagesCount:1 delegate:nil];
            vc.allowPickingVideo = NO;
            
            vc.allowCrop = YES;
            vc.showCustomDone = YES;
            vc.naviBgColor = kColorWithHexString(@"ffffff");
            vc.naviTitleFont = FontSemibold(16);
            vc.naviTitleColor = kColorWithHexString(@"#16191C");
            vc.barItemTextColor = kColorWithHexString(@"#007AFF");
            vc.navLeftBarButtonSettingBlock = ^(UIButton *leftButton) {
                [leftButton setImage:[UIImage imageNamed:@"backimage_black"] forState:UIControlStateNormal];
            };
            vc.photoPreviewPageUIConfigBlock = ^(UICollectionView *collectionView, UIView *naviBar, UIButton *backButton, UIButton *selectButton, UILabel *indexLabel, UIView *toolBar, UIButton *originalPhotoButton, UILabel *originalPhotoLabel, UIButton *doneButton, UIImageView *numberImageView, UILabel *numberLabel) {
                naviBar.backgroundColor = [UIColor whiteColor];
                [backButton setImage:[UIImage imageNamed:@"backimage_black"] forState:UIControlStateNormal];
               
                toolBar.hidden = YES;
            };
            CGFloat he = kScreenWidth*0.75;
            vc.cropRect = CGRectMake(16, kScreenHeight/2.0-(he/2.0), kScreenWidth-32, he);
      
            vc.allowPickingOriginalPhoto = NO;
            vc.didFinishPickingPhotosHandle = ^(NSArray<UIImage *> *photos, NSArray *assets, BOOL isSelectOriginalPhoto) {
                UIImage *image = photos.firstObject;
                if (image) {
                    self.coverImage = image;
                    [self canPublish];
                    [self.imageBtn setImage:image forState:UIControlStateNormal];
                    [self.reUpVideoBtn setTitle:@"重新上传" forState:UIControlStateNormal];
                    
                  
                   
                }
            };
            vc.modalPresentationStyle = UIModalPresentationCustom;
            [self presentViewController:vc animated:YES completion:nil];
            
        }
            break;
        case  QJEditerTypeDongTai:
        case  QJEditerTypeVideo: {
            
            if (self.coverImage) {
                DLog(@"选择封面");
                QJVideoImageViewController *vc = [QJVideoImageViewController new];
                vc.videoUrl = self.videoUrl;
                vc.clickFinish = ^(UIImage *image) {
                    if (image) {
                        self.coverImage = image;
                        [self.imageBtn setImage:image forState:UIControlStateNormal];
                    }
                };
                [self.navigationController pushViewController:vc animated:YES];
            }else{
                [self choiseVideo];
                
            }
            DLog(@"视频");
        }
            break;
        default:
            break;
    }
}




- (void)publishAll:(UIButton *)sender {
    
    [self publishAllData:@"1"];//发布
    
    
}


- (void)draftView:(UIButton *)sender {
    QJCommunityInfoDraftVC *vc = [QJCommunityInfoDraftVC new];
    vc.type = self.type;
    [self.navigationController pushViewController:vc animated:YES];
    
}


- (void)addLabelAction {
    DLog(@"添加标签");
    QJPublishNewsTagsViewController *vc = [QJPublishNewsTagsViewController new];
    vc.tagViewClickEvent = ^(NSArray * _Nonnull tagArray) {
        self.tagArr = tagArray;
        QJNewsTagModel *mode = tagArray.firstObject;
        self.bottomTagView.title = mode.name;
        self.bottomTagView.hidden = NO;
        self.bottomTagView.titleColor = kColorWithHexString(@"#7341A3");
        self.bottomTagView.backgroundColor = kColorWithHexString(@"#F2E5FF");
        self.tagRightImageView.hidden = NO;
        self.addLabelBtn.backgroundColor = [UIColor clearColor];
        [self.addLabelBtn setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
        [self.addLabelBtn setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        self.tagRightImageView.hidden = YES;
        [self canPublish];
    };
    NSMutableArray *arr = [NSMutableArray array];
    for (QJNewsTagModel *news in self.tagArr) {
        [arr safeAddObject:news.code];
    }
    vc.selectTagArray = arr.copy;
    vc.modalPresentationStyle = UIModalPresentationCustom;
    [self presentViewController:vc animated:NO completion:nil];
}

- (void)originalBtnAction {
    DLog(@"原创");
    self.originalBtn.selected = YES;
    self.forwardBtn.selected = NO;
    [self canPublish];
    
}

- (void)forwardBtnAction {
    DLog(@"转载");
    self.originalBtn.selected = NO;
    self.forwardBtn.selected = YES;
    [self canPublish];
}

- (void)ABtnAction {
    DLog(@"字号");
    self.fontStyleBar.typingAttributes = self.richTextView.typingAttributes;
    self.fontStyleBar.hidden = !self.fontStyleBar.hidden;
    if (!self.isEditing) {
        [self.fontStyleBar mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self.view);
            make.height.equalTo(@48);
            make.bottom.equalTo(self.bottomView.mas_top);
        }];
    }
    
}

- (void)emojiAction {
    self.fontStyleBar.hidden = YES;
    if (self.richTextView.isFirstResponder) {
        [self.richTextView resignFirstResponder];
        [self.richTextView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.view).offset(- Bottom_SN_iPhoneX_OR_LATER_SPACE  - 260-48);
        }];
    }
    if (self.titleText.isFirstResponder) {
        self.isClickEmojiBtn = YES;
        [self.titleText resignFirstResponder];
    }
    // 定义好动作
    void (^animation)(void) = ^void(void) {
        self.bottomView.transform = CGAffineTransformMakeTranslation(0,  - 260);
//        self.faceListView.transform = CGAffineTransformMakeTranslation(0, - Bottom_SN_iPhoneX_OR_LATER_SPACE  - 260);
    };
    [UIView animateWithDuration:0.25 animations:animation completion:^(BOOL finished) {
        if (finished) {
            if (self.isClickEmojiBtn) {
                self.isClickEmojiBtn = NO;
            }
        }
    }];
    
    
    
}


- (void)tagImage:(UIGestureRecognizer *)tap {
    DLog(@"点击图片了");
}

#pragma mark ---- 校验图片张数
- (void)checkImageCount {
    __block NSInteger count = 0;
    NSAttributedString *attributString = self.richTextView.attributedText;
    [attributString enumerateAttributesInRange:NSMakeRange(0, attributString.length) options:NSAttributedStringEnumerationLongestEffectiveRangeNotRequired usingBlock:^(NSDictionary<NSAttributedStringKey,id> * _Nonnull attrs, NSRange range, BOOL * _Nonnull stop) {
        NSString *string = attributString.string;
        NSString *selectString = [string substringWithRange:range];
        
        if ([selectString isEqualToString:@"\U0000fffc"]) {
            NSTextAttachment *attachment = attrs[NSAttachmentAttributeName];
            UIImage *image = attachment.image;
            if (image) {
                count++;
            }
        }
        
    }];
    
    self.contentImageCount = count;

    
}

- (void)pictureBtnAction {
    DLog(@"图片");
    self.fontStyleBar.hidden = YES;
    [self checkImageCount];
    if (self.type != QJEditerTypeDongTai) {
        if (self.contentImageCount >= 15) {
            
            [self.view makeToast:@"内容中图片数量不能超过15张"];
            
            return;
        }
    }else{
        if (self.contentImageCount >= 9) {
            
            [self.view makeToast:@"内容中图片数量不能超过9张"];
            
            return;
        }
    }
    TZImagePickerController *vc = [[TZImagePickerController alloc]initWithMaxImagesCount:9 delegate:nil];
    if (self.type != QJEditerTypeDongTai) {
        NSInteger max = 15 - self.contentImageCount;
        vc.maxImagesCount = max;
    }else{
        NSInteger max = 9 - self.contentImageCount;
        vc.maxImagesCount = max;
        
    }
    
    
    vc.allowPickingVideo = NO;
    vc.showSelectBtn = YES;
    vc.didFinishPickingPhotosHandle = ^(NSArray<UIImage *> *photos, NSArray *assets, BOOL isSelectOriginalPhoto) {
        for (UIImage *image in photos) {
             
            [self.richTextView.helper insertforOCWithText:nil image:image asset:assets.firstObject changeParagraph:NO replaceRange:NSMakeRange(0, 0)];
        }
        
        [self.richTextView scrollRangeToVisible:NSMakeRange(self.richTextView.text.length, 0)];
    };
    vc.modalPresentationStyle = UIModalPresentationCustom;
    [self presentViewController:vc animated:YES completion:nil];
    
}








- (void)keyboardWillShow:(NSNotification *)notification {
    
    //获取键盘的高度
    NSDictionary *userInfo = [notification userInfo];
    NSValue *aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [aValue CGRectValue];
    int height = keyboardRect.size.height;   //height 就是键盘的高度
    
    [self.richTextView mas_updateConstraints:^(MASConstraintMaker *make) {
        if(self.fontStyleBar.hidden == NO){
            make.bottom.equalTo(self.view).offset(-height - 20-48);
        }else{
            make.bottom.equalTo(self.view).offset(-height - 20);
        }
       
    }];
    
    if (self.titleText.isFirstResponder) {
//        self.ABtn.enabled = NO;
//        self.pictureBtn.enabled = NO;
//        void (^animation)(void) = ^void(void) {
//            self.bottomView.transform = CGAffineTransformMakeTranslation(0,  (-height+Bottom_SN_iPhoneX_OR_LATER_SPACE));
////            self.faceListView.transform = CGAffineTransformIdentity;
//        };
//        [UIView animateWithDuration:0.25 animations:animation];
        
    }else{
        // 定义好动作
        self.ABtn.enabled = YES;
        self.pictureBtn.enabled = YES;
        [self.fontStyleBar mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.view).offset(-height);
            make.left.right.equalTo(self.view);
            make.height.equalTo(@48);
        }];
        
        void (^animation)(void) = ^void(void) {
            self.bottomView.transform = CGAffineTransformIdentity;
//            self.faceListView.transform = CGAffineTransformIdentity;
        };
        
        [UIView animateWithDuration:0.25 animations:animation];
    }
}


//当键盘退出时调用
- (void)keyboardWillHide:(NSNotification *)aNotification {
    self.fontStyleBar.hidden = YES;
    [self.richTextView mas_updateConstraints:^(MASConstraintMaker *make) {
        CGFloat offset = 134 + Bottom_iPhoneX_SPACE;
        make.bottom.equalTo(self.view).offset(-offset);
    }];
    
    if (self.titleText.isFirstResponder && !self.isClickEmojiBtn) {
        
        // 定义好动作
        void (^animation)(void) = ^void(void) {
            self.bottomView.transform = CGAffineTransformIdentity;
            
        };
        [UIView animateWithDuration:0.25 animations:animation];
    }
    
    if (!self.fontStyleBar.hidden) {
        [self.fontStyleBar mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self.view);
            make.height.equalTo(@48);
            make.bottom.equalTo(self.bottomView.mas_top);
        }];
    }
    
}


//选中表情
- (void)emotionDidSelected:(NSNotification *)notifi {
    XZEmotion *emotion = notifi.userInfo[GXSelectEmotionKey];
    
    if (emotion.code) {
        if (self.isTitleTextViewActivity) {
            [self.titleText insertText:emotion.code.emoji];
            [self.titleText scrollRangeToVisible:NSMakeRange(self.richTextView.text.length, 0)];
            [self canPublish];
        }else{
            [self.richTextView insertText:emotion.code.emoji];
            [self.richTextView scrollRangeToVisible:NSMakeRange(self.richTextView.text.length, 0)];
            [self canPublish];
        }
        
    } else if (emotion.face_name) {
        if (self.isTitleTextViewActivity){
            [self.titleText insertText:emotion.face_name];
            [self canPublish];
        }else{
            [self.richTextView insertText:emotion.face_name];
            [self canPublish];
        }
        
    }
}

// 删除表情
- (void)deleteBtnClicked {
    if (!self.isTitleTextViewActivity) {
        [self.richTextView deleteBackward];
        [self canPublish];
    }else{
        [self.titleText deleteBackward];
        [self canPublish];
    }
    
}

//重新上传
- (void)reUpVideoBtnAction {
    DLog(@"重新上传");
    switch (self.type) {
        case  QJEditerTypeImage: {
                DLog(@"图片");
                TZImagePickerController *vc = [[TZImagePickerController alloc]initWithMaxImagesCount:1 delegate:nil];
                vc.allowPickingVideo = NO;
            vc.showCustomDone = YES;
                vc.allowCrop = YES;
            vc.naviBgColor = kColorWithHexString(@"ffffff");
            vc.naviTitleFont = FontSemibold(16);
            vc.naviTitleColor = kColorWithHexString(@"#16191C");
            vc.barItemTextColor = kColorWithHexString(@"#007AFF");
            vc.navLeftBarButtonSettingBlock = ^(UIButton *leftButton) {
                [leftButton setImage:[UIImage imageNamed:@"backimage_black"] forState:UIControlStateNormal];
            };
            vc.photoPreviewPageUIConfigBlock = ^(UICollectionView *collectionView, UIView *naviBar, UIButton *backButton, UIButton *selectButton, UILabel *indexLabel, UIView *toolBar, UIButton *originalPhotoButton, UILabel *originalPhotoLabel, UIButton *doneButton, UIImageView *numberImageView, UILabel *numberLabel) {
                naviBar.backgroundColor = [UIColor whiteColor];
                [backButton setImage:[UIImage imageNamed:@"backimage_black"] forState:UIControlStateNormal];
               
                toolBar.hidden = YES;
            };
                CGFloat he = kScreenWidth*0.75;
                vc.cropRect = CGRectMake(16, kScreenHeight/2.0-(he/2.0), kScreenWidth-32, he);
        
                vc.allowPickingOriginalPhoto = NO;
                vc.didFinishPickingPhotosHandle = ^(NSArray<UIImage *> *photos, NSArray *assets, BOOL isSelectOriginalPhoto) {
                    UIImage *image = photos.firstObject;
                    if (image) {
                        self.coverImage = image;
                        [self canPublish];
                        [self.imageBtn setImage:image forState:UIControlStateNormal];
                        [self.reUpVideoBtn setTitle:@"重新上传" forState:UIControlStateNormal];
                         
                    }
                };
                vc.modalPresentationStyle = UIModalPresentationCustom;
                [self presentViewController:vc animated:YES completion:nil];
                
            
        }
            break;
        case  QJEditerTypeVideo:
        case  QJEditerTypeDongTai:{
            [self choiseVideo];
        }
            break;
        default:
            break;
    }
    
    
    
}


//删除按钮
- (void)deleteBtnAction:(UIButton *)btn {
    
    self.videoUrl = nil;
    self.coverImage = nil;
    [_imageBtn setImage:nil forState:UIControlStateNormal];
    [_imageBtn setImage:nil forState:UIControlStateHighlighted];
    [_imageBtn setBackgroundImage:[UIImage imageNamed:@"qj_edite_creat"] forState:UIControlStateNormal];
    [_imageBtn setBackgroundImage:[UIImage imageNamed:@"qj_edite_creat"] forState:UIControlStateHighlighted];
    UIView *bg = [self.imageBtn viewWithTag:1001];
    bg.hidden = YES;
    self.deleteBtn.hidden = YES;
    [self.reUpVideoBtn setTitle:@"上传视频" forState:UIControlStateNormal];
    
    
}

#pragma mark -----  将换行转成新的p标签
- (NSString *)duanluo:(NSString *)html {
    NSMutableAttributedString *tempAttr = self.richTextView.attributedText.mutableCopy;
        // 先将图片占位，等替换完成html标签之后，在将图片url替换回准确的
    __block NSInteger idx = 0;
    NSMutableArray *tempPlaceHolders = [NSMutableArray new];
   
    [tempAttr enumerateAttributesInRange:NSMakeRange(0, tempAttr.length) options:NSAttributedStringEnumerationLongestEffectiveRangeNotRequired usingBlock:^(NSDictionary<NSAttributedStringKey,id> * _Nonnull attrs, NSRange range, BOOL * _Nonnull stop) {
        NSString *string = tempAttr.string;
        NSString *selectString = [string substringWithRange:range];
        
        if ([selectString isEqualToString:@"\n"]) {
            NSString *placeHolder = [NSString stringWithFormat:@"rz_attributed_duanluo_placeHolder_index_%lu", (unsigned long)idx];
            idx++;
            [tempAttr replaceCharactersInRange:range withString:placeHolder];
            [tempPlaceHolders addObject:placeHolder];
        }
      
    }];
    
    html = [tempAttr rz_codingToCompleteHtmlByWeb];
    NSInteger index = 0;
    for (NSInteger i = tempPlaceHolders.count - 1; i >= 0; i--) {
        NSString *placeholder = tempPlaceHolders[i];
        
        NSString *duan =  @"<p>\n</p>";
        index++;
        html = [html stringByReplacingOccurrencesOfString:placeholder withString:duan];
    }
    return html;
}

#pragma mark --------- configerParam  构造参数

- (void)configerParam:(NSAttributedString *)attributString {
    [self.contentImageArr removeAllObjects];
    [self.contentImageKeys removeAllObjects];
    [self.publishModel removeAllObjects];
    
    [attributString enumerateAttributesInRange:NSMakeRange(0, attributString.length) options:NSAttributedStringEnumerationLongestEffectiveRangeNotRequired usingBlock:^(NSDictionary<NSAttributedStringKey,id> * _Nonnull attrs, NSRange range, BOOL * _Nonnull stop) {
        NSString *string = attributString.string;
        NSString *selectString = [string substringWithRange:range];
        
        if ([selectString isEqualToString:@"\U0000fffc"]) {
            NSTextAttachment *attachment = attrs[NSAttachmentAttributeName];
            UIImage *image = attachment.image;
            if (image) {
                
                NSString *str = [NSString getRandomFileName];
                str = [NSString stringWithFormat:@"image/%@.jpg", str];
                NSDictionary *dic = @{@"content":[NSString stringWithFormat:@"/%@", str], @"type":@"P"};
                [self.contentImageArr safeAddObject:image];
                [self.contentImageKeys safeAddObject:str];
                [self.publishModel safeAddObject:dic];
            }
            
            
        }else{
            NSDictionary *dic = @{@"content":kCheckStringNil(selectString), @"type":@"W"};
            [self.publishModel safeAddObject:dic];
            
        }
        
    }];
}

//判断是否可以发布
- (BOOL)canPublish {
    if (self.titleText.text.length == 0) {
        self.publishBtn.enabled = NO;
        return NO;
    }
    if (self.richTextView.attributedText.length == 0) {
        self.publishBtn.enabled = NO;
        return NO;
    }
    if (self.type != QJEditerTypeDongTai) {
    
    switch (self.type) {
        case  QJEditerTypeImage:
            DLog(@"图片");
            if (!self.coverImage) {
                self.publishBtn.enabled = NO;
                return NO;
            }
            break;
        case  QJEditerTypeVideo:
            if (!self.videoUrl) {
                self.publishBtn.enabled = NO;
                return NO;
            }
            DLog(@"视频");
            break;
        default:
            break;
    }
    
    if (self.tagArr.count == 0) {
        self.publishBtn.enabled = NO;
        return NO;
    }
    if (!self.originalBtn.selected && !self.forwardBtn.selected) {
        self.publishBtn.enabled = NO;
        return NO;
    }
}
    self.publishBtn.enabled = YES;
    return YES;
    
}

#pragma mark ------ 发布
- (void)publishAllData:(NSString *)action {
    
    if (self.titleText.text.length == 0) {
        [self.view makeToast:@"标题不能为空"];
        return;
    }
    if (self.richTextView.attributedText.length == 0) {
        [self.view makeToast:@"内容不能为空"];
        return;
    }
    [self.imageUrls removeAllObjects];
    [QJAppTool showHUDLoading];
    
    if (!self.group) {
        self.group = dispatch_group_create();
        
    }
    NSAttributedString *attributedText = self.richTextView.attributedText;
    [self configerParam:attributedText];
   
    NSString *videoAddress = @"";
    if (self.videoUrl) {
        
        NSString *str = [NSString getRandomFileName];
        NSData *videoData = [NSData dataWithContentsOfURL:self.videoUrl];
        [self pushData:videoData objectKey:[NSString stringWithFormat:@"video/%@.mp4", str]];
        videoAddress = [NSString stringWithFormat:@"/video/%@.mp4", str];
        
    }
    if (self.coverImage) {
        self.overImageName = [NSString stringWithFormat:@"image/%@.jpg", [NSString getRandomFileName]];
        NSData *data = UIImageJPEGRepresentation(self.coverImage, 0.5);
        [self pushData:data objectKey:self.overImageName];
        
    }
    
    for (int i = 0; i < self.contentImageArr.count; i++) {
        UIImage *currentImages = [self.contentImageArr safeObjectAtIndex:i];
        NSString *objectKey = [self.contentImageKeys safeObjectAtIndex:i];
        NSData *data = UIImageJPEGRepresentation(currentImages, 0.5);
        [self pushData:data objectKey:objectKey];
    }
    
    
    
    if (self.type == QJEditerTypeDongTai) {
        [self publishDongTai:videoAddress];
    }else{
        [self publishNews:videoAddress];
    }
  
    
    
    
}


#pragma mark ----- 发布动态
- (void)publishDongTai:(NSString *)videoAddress {
    WS(weakSelf)
    dispatch_group_notify(self.group, dispatch_get_main_queue(), ^{
        
        for (NSString *url in self.contentImageKeys) {
            NSString *urlStr = url;
            urlStr = [NSString stringWithFormat:@"/%@",url];
            [self.imageUrls safeAddObject:[urlStr checkImageUrlString]];
        }
        NSAttributedString *attributedText = self.richTextView.attributedText;
        NSString *html = [attributedText rz_codingToHtmlByWebWithImagesURLSIfHad:self.imageUrls];
//        html = [self duanluo:html];
        html = [html stringByReplacingOccurrencesOfString:@"<style type=\"text/css\">" withString:@"<style type=\"text/css\">\nimg {width: auto;height: auto;max-width: 100%; border-radius:4px;}\np{line-height:24px !important;letter-spacing: 0.02em;}"];
        html = [html stringByReplacingOccurrencesOfString:@"<head>" withString:@"<head id = \"8ekEiOSjD9o6wUwut6o2fKtYfaXKgwY6\">"];
        
        html = [html stringByReplacingOccurrencesOfString:@"p>\n<p" withString:@"p>\n<p>\n</p>\n<p"];
        QJCampsiteRequest *startRequest = [[QJCampsiteRequest alloc] init];
        NSString *overImage = [NSString stringWithFormat:@"/%@",self.overImageName];
        [startRequest netWorkPostNews:[self.publishModel copy] action:@"1" campId:self.campsiteID title:self.titleText.text videoAddress:videoAddress html:html coverImage:overImage isVertical:self.videoIsvertical];
        [startRequest setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
            [QJAppTool hideHUDLoading];
            NSString *message = request.responseJSONObject[@"message"];
            DLog(@"error--%@", message);
            if (ResponseSuccess) {
             
                    [[QJAppTool getCurrentViewController].view makeToast:@"发布成功"];
                if (self.model) {//如果是草稿箱过来的 移除老数据
                    NSMutableIndexSet *set = [NSMutableIndexSet new];
                    NSMutableArray *arr = [QJCommunityDraftManager shared].modelArr;
                    if (arr.count > 0 && [arr containsObject:self.model]) {
                        NSInteger index = [arr indexOfObject:self.model];
                        [set addIndex:index];
                        [[QJCommunityDraftManager shared] deleteModelArr:set];
                    }
                    
                     
                }
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    if (weakSelf.presentingViewController && !weakSelf.isFromDraftVC) {
                        [weakSelf.presentingViewController dismissViewControllerAnimated:YES completion:nil];
                    }else{
                        [weakSelf.navigationController popViewControllerAnimated:YES];
                    }
                });
            }else{
                NSString *code = request.responseJSONObject[@"code"];
                if([kCheckStringNil(code) isEqualToString:@"21602"]) {//营地封禁
                    [QJAppTool showWarningToast:ResponseMsg];
                }else{
                    [weakSelf.view makeToast:ResponseMsg];
                }
               
            }
        }];
        [startRequest setFailureCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
            [QJAppTool hideHUDLoading];

        }];
    });
}

#pragma mark ------ 发布资讯
- (void)publishNews:(NSString *)videoAddress {
    dispatch_group_notify(self.group, dispatch_get_main_queue(), ^{
        for (NSString *url in self.contentImageKeys) {
            NSString *urlStr = url;
            urlStr = [NSString stringWithFormat:@"/%@",url];
            [self.imageUrls safeAddObject:[urlStr checkImageUrlString]];
        }
        NSAttributedString *attributedText = self.richTextView.attributedText;
        NSString *html = [attributedText rz_codingToHtmlByWebWithImagesURLSIfHad:self.imageUrls];
        html = [html stringByReplacingOccurrencesOfString:@"<style type=\"text/css\">" withString:@"<style type=\"text/css\">\nimg {width: auto;height: auto;max-width: 100%; border-radius:4px;}\np{line-height:24px !important;letter-spacing: 0.02em;}"];
        html = [html stringByReplacingOccurrencesOfString:@"<head>" withString:@"<head id = \"8ekEiOSjD9o6wUwut6o2fKtYfaXKgwY6\">"];
        html = [html stringByReplacingOccurrencesOfString:@"p>\n<p" withString:@"p>\n<p>\n</p>\n<p"];
        QJNewsTagModel *tag = self.tagArr.firstObject;
        NSString *overImage = [NSString stringWithFormat:@"/%@",self.overImageName];
        NSDictionary *dic = @{
            @"content":self.publishModel.copy?:@"",
            @"action":@"1",
            @"title":self.titleText.text?:@"",
            @"videoAddress":videoAddress?:@"",
            @"htmlContent":html?:@"",
            @"coverImage":overImage?:@"",
            @"tagId":tag.code ?:@"",
            @"original":self.originalBtn.selected?@1:@0,
            @"vertical":@(self.videoIsvertical),
        };
        WS(weakSelf)
        self.newsPostReq.dic = dic;
        [self.newsPostReq setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
            [QJAppTool hideHUDLoading];
            NSString *message = request.responseJSONObject[@"message"];
            
            
            DLog(@"error--%@", message);
            if (ResponseSuccess) {
                [[NSNotificationCenter defaultCenter] postNotificationName:QJPublish object:nil];
                __strong typeof(weakSelf)strongSelf = weakSelf;
                NSDictionary *data = EncodeDicFromDic(request.responseJSONObject, @"data");
                NSString *idStr = EncodeStringFromDic(data, @"id");
                    [strongSelf.view makeToast:@"提交发布成功 请等待审核~预计将在3个工作日内完成审核" duration:3 position:CSToastPositionCenter image:[UIImage imageNamed:@"success_login"]];
                
                //发布完成暂时保存本地
                [strongSelf saveTodraftWithId:idStr];
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    if (strongSelf.presentingViewController && !self.isFromDraftVC) {
                        [strongSelf.presentingViewController dismissViewControllerAnimated:YES completion:nil];
                    }else{
                        [strongSelf.navigationController popViewControllerAnimated:YES];
                    }
                });
            }else{
                [weakSelf.view makeToast:ResponseMsg];
            }
            
        }];
        [self.newsPostReq setFailureCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
            [QJAppTool hideHUDLoading];
            
        }];
        
        [self.newsPostReq start];
    });
}



//上传图片和视频
- (void)pushData:(NSData *)data objectKey:(NSString *)objectKey {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        id<OSSCredentialProvider> credential = [[OSSCustomSignerCredentialProvider alloc] initWithImplementedSigner:^NSString * _Nullable(NSString * _Nonnull contentToSign, NSError *__autoreleasing  _Nullable * _Nullable error) {
            OSSTaskCompletionSource * tcs = [OSSTaskCompletionSource taskCompletionSource];
            __block NSString *signToken= @"";
            QJCampsiteRequest *startRequest = [[QJCampsiteRequest alloc] init];
            [startRequest netWorkGetSign:contentToSign];
            [startRequest setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
                if (ResponseSuccess) {
                    NSString *data = request.responseJSONObject[@"data"][@"contentSign"];
                    signToken = data;
                    [tcs setResult:kCheckNil(data)];
                } else {
                    [tcs setError:request.error];
                }
            }];
            
            [tcs.task waitUntilFinished];
            if (tcs.task.error) {
                return nil;
            } else {
                return signToken;
            }
        }];
        OSSClient *client = [[OSSClient alloc] initWithEndpoint: @"https://oss-cn-hangzhou.aliyuncs.com" credentialProvider:credential];
        
        //        NSData *imageData = UIImageJPEGRepresentation(currentImage, 0.5);
        OSSPutObjectRequest * put = [OSSPutObjectRequest new];
        put.bucketName = @"sxqj";
        put.objectKey = objectKey;
        put.uploadingData = data;
        // （可选）设置上传进度。
        put.uploadProgress = ^(int64_t bytesSent, int64_t totalByteSent, int64_t totalBytesExpectedToSend) {
            // 指定当前上传长度、当前已经上传总长度、待上传的总长度。
            NSLog(@"%lld, %lld, %lld", bytesSent, totalByteSent, totalBytesExpectedToSend);
        };
        dispatch_group_enter(self.group);
        enterCount++;
        OSSTask * putTask = [client putObject:put];
        
        [putTask waitUntilFinished];
        [putTask continueWithBlock:^id(OSSTask *task) {
            if (!task.error) {
                NSLog(@"upload object success!");
                leaveCount++;
                if (leaveCount <= enterCount) {
                    if (self) {
                        dispatch_group_leave(self.group);
                    }
                    
                }
            } else {
                leaveCount++;
                if (leaveCount <= enterCount) {
                    if (self) {
                        dispatch_group_leave(self.group);
                    }
                    
                }
                NSLog(@"upload object failed, error: %@" , task.error);
            }
            return nil;
        }];
    });
}



#pragma mark  ------- getter

- (UIButton *)imageBtn {
    if (!_imageBtn) {
        _imageBtn = [UIButton new];
        [_imageBtn addTarget:self action:@selector(btnAction:) forControlEvents:UIControlEventTouchUpInside];
        [_imageBtn setBackgroundImage:[UIImage imageNamed:@"qj_edite_creat"] forState:UIControlStateNormal];
        [_imageBtn setBackgroundImage:[UIImage imageNamed:@"qj_edite_creat"] forState:UIControlStateHighlighted];
        _imageBtn.imageView.contentMode =UIViewContentModeScaleAspectFill;
    }
    return _imageBtn;
}

- (UIButton *)deleteBtn {
    if (!_deleteBtn) {
        _deleteBtn = [UIButton new];
        [_deleteBtn addTarget:self action:@selector(deleteBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [_deleteBtn setImage:[UIImage imageNamed:@"qj_delete_black_24"] forState:UIControlStateNormal];
        [_deleteBtn setImage:[UIImage imageNamed:@"qj_delete_black_24"] forState:UIControlStateHighlighted];
        [_deleteBtn setEnlargeEdgeWithTop:10 right:10 bottom:10 left:10];
        _deleteBtn.hidden = YES;
         
    }
    return _deleteBtn;
}

- (UIButton *)publishBtn {
    if (!_publishBtn) {
        _publishBtn = [UIButton new];
        [_publishBtn setTitle:@"发布" forState:UIControlStateNormal];
        _publishBtn.titleLabel.font = FontSemibold(14);
        [_publishBtn setTitleColor:UIColorHex(007AFF) forState:UIControlStateNormal];
        [_publishBtn setTitleColor:UIColorHex(C8CACC) forState:UIControlStateDisabled];
        [_publishBtn addTarget:self action:@selector(publishAll:) forControlEvents:UIControlEventTouchUpInside];
        _publishBtn.titleLabel.font = MYFONTALL(FONT_REGULAR, 14);
        _publishBtn.enabled = NO;
    }
    return _publishBtn;
}

- (ICChatBoxFaceView *)faceListView {
    if (!_faceListView) {
        _faceListView = [[ICChatBoxFaceView alloc] initWithFrame:CGRectMake(0, kScreenHeight, kScreenWidth, 260)];
        _faceListView.backgroundColor = [UIColor whiteColor];
        
    }
    return _faceListView;
}

- (RZRichTextView *)richTextView {
    if (!_richTextView) {
        
        RZRichTextViewOptions *options = [RZRichTextViewOptions new];
        WS(weakSelf)
        options.didInsetAttachment = ^(NSTextAttachment * _Nonnull attahcment) {
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:weakSelf action:@selector(tagImage:)];
            [attahcment.rtInfo.maskView addGestureRecognizer:tap]; ;
        };
        options.openPhotoLibrary = ^(void (^ _Nonnull complete)(UIImage * _Nullable image, PHAsset * _Nullable asset)) {
            weakSelf.fontStyleBar.hidden = YES;
            [weakSelf checkImageCount];
            if (weakSelf.type != QJEditerTypeDongTai) {
                if (weakSelf.contentImageCount >= 15) {
                    
                    [weakSelf.view makeToast:@"内容中图片数量不能超过15张"];
                    
                    return;
                }
            }else{
                if (weakSelf.contentImageCount >= 9) {
                    
                    [weakSelf.view makeToast:@"内容中图片数量不能超过9张"];
                    
                    return;
                }
            }
            TZImagePickerController *vc = [[TZImagePickerController alloc]initWithMaxImagesCount:9 delegate:nil];
            if (weakSelf.type != QJEditerTypeDongTai) {
                NSInteger max = 15 - weakSelf.contentImageCount;
                vc.maxImagesCount = max;
            }else{
                NSInteger max = 9 - weakSelf.contentImageCount;
                vc.maxImagesCount = max;
                
            }
            vc.allowPickingVideo = NO;
            vc.showSelectBtn = YES;
            vc.didFinishPickingPhotosHandle = ^(NSArray<UIImage *> *photos, NSArray *assets, BOOL isSelectOriginalPhoto) {
                for (UIImage *image in photos) {
                    [weakSelf.richTextView.helper insertforOCWithText:nil image:image asset:assets.firstObject changeParagraph:NO replaceRange:NSMakeRange(0, 0)];
                    
                }
                
                [weakSelf scrollToCursorPosition];
            };
            vc.modalPresentationStyle = UIModalPresentationOverFullScreen;
            [weakSelf presentViewController:vc animated:YES completion:nil];
        };
 
        options.titleFont = FontSemibold(16);
        options.normalFont = FontRegular(15);
        options.boldFont = [UIFont systemFontOfSize:kWScale*15 weight:UIFontWeightHeavy];
 
        
        options.titleObliqueFont = [UIFont QJDisplayFontWithSize:16 bold:YES itatic:YES weight:1];
        options.obliqueFont = [UIFont QJDisplayFontWithSize:15 bold:NO itatic:YES weight:0];
        options.boldObliqueFont = [UIFont QJDisplayFontWithSize:15 bold:YES itatic:YES weight:1];
        
        
        options.icon_image = [UIImage imageNamed:@"picturePublish"];
        options.icon_font = [UIImage imageNamed:@"shezhiwenzi"];
        //自定义工具条
//        RZToolBarItem *emojiItem = [[RZToolBarItem alloc]initWithType:100 image:[UIImage imageNamed:@"emojiPublish"] selectedImage:[UIImage imageNamed:@"emojiPublish"] selected:NO exparams:nil items:@[@0]];
        self.fontItem = [[RZToolBarItem alloc]initWithType:101 image:[UIImage imageNamed:@"shezhiwenzi"] selectedImage:[UIImage imageNamed:@"wenzishezhi_selected"] selected:NO exparams:nil items:@[@0]];
        NSMutableArray *muArr = options.toolbarItems.mutableCopy;
//        [muArr insertObject:emojiItem atIndex:0];
        [muArr insertObject:self.fontItem atIndex:0];
        options.toolbarItems = muArr.copy;
        
        options.didSelectedToolbarItem = ^BOOL(RZToolBarItem * _Nonnull item, NSInteger index) {
            if (item.type == 100) {
                DLog(@"点击emoji");
                [weakSelf emojiAction];
                weakSelf.fontStyleBar.hidden = YES;
                return YES;
            }else if(item.type == 101) {
                DLog(@"点击font");
                weakSelf.fontStyleBar.typingAttributes = weakSelf.richTextView.typingAttributes;
                weakSelf.fontStyleBar.hidden = !weakSelf.fontStyleBar.hidden;
                
                
            }
            return YES;
        };
        _richTextView = [[RZRichTextView alloc]initWithFrame:CGRectZero options:options];;
        
        _richTextView.delegate = self;
        
    }
    return _richTextView;
}

 

- (UITextView *)titleText {
    if (!_titleText) {
        _titleText = [UITextView new];
        _titleText.textColor = UIColorHex(16191C);
        _titleText.font = FontSemibold(16);
        _titleText.delegate = self;
        [_titleText becomeFirstResponder];
        self.isTitleTextViewActivity = YES;
    }
    return _titleText;
}

-(UILabel *)titleTextPlaceholder {
    if (!_titleTextPlaceholder) {
        _titleTextPlaceholder = [UILabel new];
        NSAttributedString *attr = [[NSAttributedString alloc]initWithString:@"标题" attributes:@{NSForegroundColorAttributeName:UIColorHex(919599),NSFontAttributeName:FontRegular(16)}];
        
        _titleTextPlaceholder.attributedText = attr;
    }
    return _titleTextPlaceholder;
}


-(UILabel *)richTextPlaceHolder {
    if (!_richTextPlaceHolder) {
        _richTextPlaceHolder = [UILabel new];
        NSAttributedString *attr = [[NSAttributedString alloc]initWithString:@"输入正文" attributes:@{NSForegroundColorAttributeName:UIColorHex(919599),NSFontAttributeName:FontRegular(16)}];
        
        _richTextPlaceHolder.attributedText = attr;
    }
    return _richTextPlaceHolder;
}




- (UILabel *)titleNum {
    if (!_titleNum) {
        _titleNum = [UILabel new];
        _titleNum.textColor = UIColorHex(C8CACC);
        _titleNum.font = FontRegular(14);
        _titleNum.textAlignment = NSTextAlignmentRight;
        _titleNum.text = @"0/50";
    }
    return _titleNum;
}

- (UIView *)lineView {
    if (!_lineView) {
        _lineView = [UIView new];
        _lineView.backgroundColor = UIColorHex(EBEDF0);
    }
    return _lineView;
}

- (UILabel *)richNumLabel {
    if (!_richNumLabel) {
        _richNumLabel = [UILabel new];
        _richNumLabel.textColor = kColorWithHexString(@"#C8CACC");
        _richNumLabel.font = FontRegular(14);
        _richNumLabel.text = @"0/10000";
        _richNumLabel.textAlignment = NSTextAlignmentRight;
        if (self.type == QJEditerTypeDongTai) {
            _richNumLabel.text = @"0/500";
        }
    }
    return _richNumLabel;
}

- (UIButton *)addLabelBtn {
    if (!_addLabelBtn) {
        _addLabelBtn = [UIButton new];
        [_addLabelBtn addTarget:self action:@selector(addLabelAction) forControlEvents:UIControlEventTouchUpInside];
        [_addLabelBtn setImage:[UIImage imageNamed:@"qj_addLabel"] forState:UIControlStateNormal];
        _addLabelBtn.backgroundColor = kColorWithHexString(@"#F7F7F7");
        _addLabelBtn.titleLabel.font = FontMedium(12);
        [_addLabelBtn setTitle:@"标签(请添加一个标签)" forState:UIControlStateNormal];
        [_addLabelBtn setTitleColor:kColorWithHexString(@"#919599") forState:UIControlStateNormal];
        [_addLabelBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 14, 0, 0)];
        [_addLabelBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 24, 0, 0)];
        if (self.type == QJEditerTypeDongTai) {
            _addLabelBtn.hidden = YES;
        }else{
            _addLabelBtn.hidden = NO;
        }
        
    }
    return _addLabelBtn;
}

- (UIButton *)originalBtn {
    if (!_originalBtn) {
        _originalBtn = [UIButton new];
        [_originalBtn addTarget:self action:@selector(originalBtnAction) forControlEvents:UIControlEventTouchUpInside];
        [_originalBtn setImage:[UIImage imageNamed:@"reportUnselected"] forState:UIControlStateNormal];
        [_originalBtn setImage:[UIImage imageNamed:@"reportUnselected"] forState:UIControlStateHighlighted];
        [_originalBtn setImage:[UIImage imageNamed:@"reportSelected"] forState:UIControlStateSelected];
        
        _originalBtn.titleLabel.font = FontMedium(12);
        [_originalBtn setTitle:@"原创文章" forState:UIControlStateNormal];
        [_originalBtn setTitleColor:kColorWithHexString(@"#16191C") forState:UIControlStateNormal];
        
        [_originalBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
        [_originalBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 20, 0, 0)];
        if (self.type == QJEditerTypeDongTai) {
            _originalBtn.hidden = YES;
        }else{
            _originalBtn.hidden = NO;
        }
    }
    return _originalBtn;
}

- (UIButton *)forwardBtn {
    if (!_forwardBtn) {
        _forwardBtn = [UIButton new];
        [_forwardBtn addTarget:self action:@selector(forwardBtnAction) forControlEvents:UIControlEventTouchUpInside];
        [_forwardBtn setImage:[UIImage imageNamed:@"reportUnselected"] forState:UIControlStateNormal];
        [_forwardBtn setImage:[UIImage imageNamed:@"reportUnselected"] forState:UIControlStateHighlighted];
        [_forwardBtn setImage:[UIImage imageNamed:@"reportSelected"] forState:UIControlStateSelected];
        _forwardBtn.titleLabel.font = FontMedium(12);
        [_forwardBtn setTitle:@"转载文章" forState:UIControlStateNormal];
        [_forwardBtn setTitleColor:kColorWithHexString(@"#16191C") forState:UIControlStateNormal];
        [_forwardBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
        [_forwardBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 20, 0, 0)];
        if (self.type == QJEditerTypeDongTai) {
            _forwardBtn.hidden = YES;
        }else{
            _forwardBtn.hidden = NO;
        }
    }
    return _forwardBtn;
}

- (UIView *)bottomView {
    if (!_bottomView) {
        _bottomView = [UIView new];
        _bottomView.backgroundColor = [UIColor whiteColor];
    }
    return _bottomView;
}

//- (UIButton *)emojiBtn {
//    if (!_emojiBtn) {
//        _emojiBtn = [UIButton new];
//        [_emojiBtn addTarget:self action:@selector(emojiAction) forControlEvents:UIControlEventTouchUpInside];
//        [_emojiBtn setImage:[UIImage imageNamed:@"emojiPublish"] forState:UIControlStateNormal];
//
//    }
//    return _emojiBtn;
//}

- (UIButton *)ABtn {
    if (!_ABtn) {
        _ABtn = [UIButton new];
        [_ABtn addTarget:self action:@selector(ABtnAction) forControlEvents:UIControlEventTouchUpInside];
        [_ABtn setImage:[UIImage imageNamed:@"shezhiwenzi"] forState:UIControlStateNormal];
        [_ABtn setImage:[UIImage imageNamed:@"shezhiwenzi_disable"] forState:UIControlStateDisabled];
        [_ABtn setImage:[UIImage imageNamed:@"wenzishezhi_selected"] forState:UIControlStateSelected];
        _ABtn.enabled = NO;
    }
    return _ABtn;
}

- (UIButton *)pictureBtn {
    if (!_pictureBtn) {
        _pictureBtn = [UIButton new];
        [_pictureBtn addTarget:self action:@selector(pictureBtnAction) forControlEvents:UIControlEventTouchUpInside];
        [_pictureBtn setImage:[UIImage imageNamed:@"picturePublish"] forState:UIControlStateNormal];
        [_pictureBtn setImage:[UIImage imageNamed:@"picture_unenable"] forState:UIControlStateDisabled];
        _pictureBtn.enabled = NO;
    }
    return _pictureBtn;
}

///字体设置bar
- (QJFontStyleChoiceBar *)fontStyleBar {
    if (!_fontStyleBar) {
        _fontStyleBar = [[QJFontStyleChoiceBar alloc]initWithFrame:CGRectZero options:self.richTextView.options];
        _fontStyleBar.typingAttributes = self.richTextView.typingAttributes;
        _fontStyleBar.hidden = YES;
    }
    return _fontStyleBar;
}

- (NSMutableArray *)contentImageArr {
    if (!_contentImageArr) {
        _contentImageArr = [NSMutableArray array];
        
    }
    return _contentImageArr;
}

- (NSMutableArray *)contentImageKeys {
    if (!_contentImageKeys) {
        _contentImageKeys = [NSMutableArray array];
    }
    return _contentImageKeys;
}

 

- (NSMutableArray *)publishModel {
    if (!_publishModel) {
        _publishModel = [NSMutableArray arrayWithCapacity:0];
    }
    return _publishModel;
}

- (NSMutableArray *)imageUrls {
    if (!_imageUrls) {
        _imageUrls = [NSMutableArray array];
    }
    return _imageUrls;
}

- (QJCommunityNewsRequest *)newsPostReq {
    if (!_newsPostReq) {
        _newsPostReq = [QJCommunityNewsRequest new];
    }
    return _newsPostReq;
}

- (UIImageView *)tagRightImageView {
    if (!_tagRightImageView) {
        _tagRightImageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"edit_gray"]];
        _tagRightImageView.hidden = YES;
    }
    return _tagRightImageView;
}

- (QJNewsTagItem *)bottomTagView {
    if (!_bottomTagView) {
        _bottomTagView = [QJNewsTagItem new];
        _bottomTagView.hidden = YES;
    }
    return _bottomTagView;
}

-  (UIButton *)reUpVideoBtn {
    if (!_reUpVideoBtn) {
        _reUpVideoBtn = [UIButton new];
        [_reUpVideoBtn addTarget:self action:@selector(reUpVideoBtnAction) forControlEvents:UIControlEventTouchUpInside];
        _reUpVideoBtn.backgroundColor = kColorWithHexString(@"#1F2A4D");
        [_reUpVideoBtn setTitle:@"上传封面" forState:UIControlStateNormal];
        _reUpVideoBtn.titleLabel.font = FontSemibold(12);
        [_reUpVideoBtn setTitleColor:kColorWithHexString(@"ffffff") forState:UIControlStateNormal];
        _reUpVideoBtn.layer.cornerRadius = 12;
        
    }
    return _reUpVideoBtn;
}

- (UIScrollView *)bgScrollView {
    if (!_bgScrollView) {
        _bgScrollView = [UIScrollView new];
        _bgScrollView.delegate = self;
        _bgScrollView.contentSize = CGSizeMake(kScreenWidth, kScreenHeight-400);
        _bgScrollView.bounces = YES;
        
    }
    return _bgScrollView;
}

 

#pragma mark  ----- 滚动
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    if (scrollView == self.richTextView) {
        if (self.richTextView.isFirstResponder || self.titleText.isFirstResponder) {
//            [self.view endEditing:YES];
        }
       
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if(scrollView == self.bgScrollView){
        CGFloat offset = self.bgScrollView.contentOffset.y;
        DLog(@"拖拽====%f",offset);
        if(offset < -20){
            [self.view endEditing:YES];
        }
    }
}


#pragma mark -----滚动到光标位置
- (void)scrollToCursorPosition {
    if (!self.richTextView.isFirstResponder) {
        return;
    }
    CGRect caret = [self.richTextView caretRectForPosition:self.richTextView.selectedTextRange.start];
    [self.richTextView scrollRectToVisible:caret animated:YES];
}




#pragma mark ------ UITextViewDelegate
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    if (textView == self.titleText) {
        self.isTitleTextViewActivity = YES;
       
    }else if (textView == self.richTextView) {
        self.isTitleTextViewActivity = NO;
        [self scrollToCursorPosition];
        
    }
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    [self canPublish];
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
   
    if (textView == self.titleText){
        if ([text isEqualToString:@"\n"]) {
            //禁止输入换行
            [self.titleText resignFirstResponder];
            return NO;
        }
    }
    
    
    
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView {
    [self canPublish];
    if (textView == self.titleText){
        if (textView.text.length == 0) {
            self.titleTextPlaceholder.hidden = NO;
        }else{
            self.titleTextPlaceholder.hidden = YES;
        }
        CGFloat height = [self heightForString:textView andWidth:kScreenWidth-32];
        [self.titleText mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(height + 6);
        }];
        if (self.titleText.text.length >= 50) {
            self.titleText.text = [self.titleText.text substringToIndex:50];
            
        }
    }
    if (textView == self.richTextView) {
//        NSMutableDictionary *mudic = [NSMutableDictionary dictionaryWithDictionary:textView.typingAttributes];
//               NSMutableParagraphStyle* paragraphStyle=[[NSMutableParagraphStyle alloc] init];
//               paragraphStyle.paragraphSpacing =16;//段与段之间的间距
//               paragraphStyle.lineBreakMode = NSLineBreakByCharWrapping;
//               [mudic setValue:paragraphStyle forKey:NSParagraphStyleAttributeName];
//               textView.typingAttributes = mudic.copy;
        if (textView.text.length == 0) {
            self.richTextPlaceHolder.hidden = NO;
        }else{
            self.richTextPlaceHolder.hidden = YES;
        }
        self.fontStyleBar.typingAttributes = textView.typingAttributes;
        [self scrollToCursorPosition];
    }
    
    
    NSInteger maxFontNum = 50;//最大输入限制
    if (textView == self.richTextView) {
        if (self.type == QJEditerTypeDongTai) {
            maxFontNum = 500;
        }else{
        maxFontNum = 10000;
        }
    }
    NSString *toBeString = textView.text;
    // 获取键盘输入模式
    NSString *lang = [[UIApplication sharedApplication] textInputMode].primaryLanguage;
    if ([lang isEqualToString:@"zh-Hans"]) { // zh-Hans代表简体中文输入，包括简体拼音，健体五笔，简体手写
        UITextRange *selectedRange = [textView markedTextRange];
        //获取高亮部分
        UITextPosition *position = [textView positionFromPosition:selectedRange.start offset:0];
        // 没有高亮选择的字，则对已输入的文字进行字数统计和限制
        if (!position) {
            if (toBeString.length > maxFontNum) {
                textView.text = [toBeString substringToIndex:maxFontNum];//超出限制则截取最大限制的文本
                if (textView == self.titleText) {
                    self.titleNum.text = @"50/50";
                }else if(textView == self.richTextView){
                    self.richNumLabel.text = [NSString stringWithFormat:@"%ld/%ld",maxFontNum,maxFontNum];
                }
                
            } else {
                if (textView == self.titleText) {
                    self.titleNum.text = [NSString stringWithFormat:@"%ld/50",toBeString.length];
                }else if(textView == self.richTextView){
                    self.richNumLabel.text = [NSString stringWithFormat:@"%ld/%ld",toBeString.length,maxFontNum];
                }
                
            }
        }
    } else {// 中文输入法以外的直接统计
        if (toBeString.length > maxFontNum) {
            textView.text = [toBeString substringToIndex:maxFontNum];
            if (textView == self.titleText) {
                self.titleNum.text = [NSString stringWithFormat:@"%ld/50",maxFontNum];
            }else if(textView == self.richTextView){
                self.richNumLabel.text = [NSString stringWithFormat:@"%ld/%ld",maxFontNum,maxFontNum];
            }
            
        } else {
            if (textView == self.titleText) {
                self.titleNum.text = [NSString stringWithFormat:@"%ld/%ld",toBeString.length,maxFontNum];
            }else if(textView == self.richTextView){
                self.richNumLabel.text = [NSString stringWithFormat:@"%ld/%ld",toBeString.length,maxFontNum];
            }
            
        }
    }
    
}

/**
 @method 获取指定宽度width的字符串在UITextView上的高度
 @param textView 待计算的UITextView
 @param width 限制字符串显示区域的宽度
 @result float 返回的高度
 */
- (float)heightForString:(UITextView *)textView andWidth:(float)width{
    CGSize sizeToFit = [textView sizeThatFits:CGSizeMake(width, MAXFLOAT)];
    return sizeToFit.height;
}

@end
