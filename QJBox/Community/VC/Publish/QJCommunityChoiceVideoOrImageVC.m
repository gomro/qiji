//
//  QJCommunityChoiceVideoOrImageVC.m
//  QJBox
//
//  Created by wxy on 2022/8/4.
//

#import "QJCommunityChoiceVideoOrImageVC.h"
#import "QJArticleEditerVC.h"
@interface QJCommunityChoiceVideoOrImageVC ()

/** 暗色背景 */
@property (nonatomic, strong) UIView *bgView;
/** 控件View */
@property (nonatomic, strong) UIView *contentView;

@property (nonatomic, strong) UIImageView *leftTopImageView;

@property (nonatomic, strong) UILabel *leftNameLabel;

@property (nonatomic, strong) UIImageView *rightTopImageView;

@property (nonatomic, strong) UILabel *rightNameLabel;

@property (nonatomic, strong) UIButton *videoBtn;

@property (nonatomic, strong) UIButton *imageBtn;//图片

@property (nonatomic, strong) UIButton *closeBtn;//关闭按钮

@end

@implementation QJCommunityChoiceVideoOrImageVC


- (instancetype)init {
    if (self = [super init]) {
        self.modalPresentationStyle = UIModalPresentationCustom;
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.navBar.hidden = YES;
    [self setUI];
     
    [self startAnimation];
}

- (void)startAnimation {
    self.bgView.frame = [UIScreen mainScreen].bounds;
    self.contentView.frame = CGRectMake(0,[UIScreen mainScreen].bounds.size.height, CGRectGetWidth([UIScreen mainScreen].bounds), Get375Width(110) + Bottom_iPhoneX_SPACE);
    
    
    [UIView animateWithDuration:0.4 delay:0 usingSpringWithDamping:0.95 initialSpringVelocity:0.05 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        
        self.bgView.alpha = 1.0f ;
        
        self.contentView.frame = CGRectMake(0, [UIScreen mainScreen].bounds.size.height - (Get375Width(110) + Bottom_iPhoneX_SPACE), CGRectGetWidth([UIScreen mainScreen].bounds), Get375Width(110) + Bottom_iPhoneX_SPACE);
        
    } completion:^(BOOL finished) {
    }];
}

- (void)setUI {
    
    self.view.backgroundColor = [UIColor clearColor];
    UIView *bgView = [[UIView alloc] init];
    bgView.backgroundColor = [UIColor colorWithRed:0.25 green:0.25 blue:0.25 alpha:0.5f];
    [self.view addSubview:bgView];
    self.bgView = bgView;
    self.bgView.alpha = 0.0f;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickBgView)];
    [bgView addGestureRecognizer:tap];
    
    UIView *cView = [[UIView alloc] init];
    cView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:cView];
    self.contentView = cView;
    self.contentView.layer.cornerRadius = 16;
    self.contentView.layer.masksToBounds = YES;
    
    [self.contentView addSubview:self.leftTopImageView];
    [self.contentView addSubview:self.leftNameLabel];
    [self.contentView addSubview:self.rightTopImageView];
    [self.contentView addSubview:self.rightNameLabel];
    [self.contentView addSubview:self.videoBtn];
    [self.contentView addSubview:self.imageBtn];
    [self.contentView addSubview:self.closeBtn];
    [self.leftTopImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.contentView).offset(Get375Width(32));
            make.width.height.equalTo(@(Get375Width(24)));
            make.right.equalTo(self.contentView.mas_centerX).offset(-Get375Width(64));
            
    }];
    
    [self.leftNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.leftTopImageView);
            make.top.equalTo(self.leftTopImageView.mas_bottom).offset(Get375Width(16));
            make.height.equalTo(@(Get375Width(21)));
            
    }];
    
    [self.rightTopImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.contentView).offset(Get375Width(32));
            make.width.height.equalTo(@(Get375Width(24)));
            make.left.equalTo(self.contentView.mas_centerX).offset(Get375Width(64));
            
    }];
    
    [self.rightNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.rightTopImageView);
            make.top.equalTo(self.rightTopImageView.mas_bottom).offset(Get375Width(16));
            make.height.equalTo(@(Get375Width(21)));
            
    }];
    
    
    [self.videoBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.imageBtn.mas_right);
            make.width.equalTo(self.imageBtn);
            make.height.equalTo(@(Get375Width(100)));
            make.centerY.equalTo(self.contentView);
    }];
    [self.imageBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView);
            make.width.equalTo(@(kScreenWidth/2.0));
            make.centerY.equalTo(self.contentView);
            make.height.equalTo(@(Get375Width(100)));
    }];
    
    [self.closeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.contentView).offset(Get375Width(16));
            make.right.equalTo(self.contentView).offset(-Get375Width(16));
            make.width.height.equalTo(@(Get375Width(24)));
    }];
}



- (UIButton *)videoBtn {
    if (!_videoBtn) {
        _videoBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_videoBtn addTarget:self action:@selector(videoBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        _videoBtn.backgroundColor = [UIColor clearColor];
    }
    return _videoBtn;
}


- (UIButton *)imageBtn {
    if (!_imageBtn) {
        _imageBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_imageBtn addTarget:self action:@selector(imageBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        _imageBtn.backgroundColor = [UIColor clearColor];
    }
    return _imageBtn;
}

- (UIImageView *)leftTopImageView {
    if (!_leftTopImageView) {
        _leftTopImageView = [UIImageView new];
        _leftTopImageView.image = [UIImage imageNamed:@"qj_picture_24"];
        
    }
    return _leftTopImageView;
}

- (UILabel *)leftNameLabel {
    if (!_leftNameLabel) {
        _leftNameLabel = [UILabel new];
        _leftNameLabel.font = FontSemibold(14);
        _leftNameLabel.textColor = kColorWithHexString(@"#16191C");
        _leftNameLabel.text = @"发布图文";
        _leftNameLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _leftNameLabel;
}

- (UIImageView *)rightTopImageView {
    if (!_rightTopImageView) {
        _rightTopImageView = [UIImageView new];
        _rightTopImageView.image = [UIImage imageNamed:@"qj_video_24"];
        
    }
    return _rightTopImageView;
}

- (UILabel *)rightNameLabel {
    if (!_rightNameLabel) {
        _rightNameLabel = [UILabel new];
        _rightNameLabel.font = FontSemibold(14);
        _rightNameLabel.textColor = kColorWithHexString(@"#16191C");
        _rightNameLabel.text = @"发布视频";
        _rightNameLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _rightNameLabel;
}



#pragma mark ---- action

- (void)imageBtnAction:(UIButton *)sender {
    
    QJArticleEditerVC *vc = [QJArticleEditerVC new];
    vc.type = QJEditerTypeImage;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)videoBtnAction:(UIButton *)sender {
    
    QJArticleEditerVC *vc = [QJArticleEditerVC new];
    vc.type = QJEditerTypeVideo;
    [self.navigationController pushViewController:vc animated:YES];
}

- (UIButton *)closeBtn {
    if (!_closeBtn) {
        _closeBtn = [UIButton new];
        [_closeBtn addTarget:self action:@selector(closeBtnAction) forControlEvents:UIControlEventTouchUpInside];
        [_closeBtn setImage:[UIImage imageNamed:@"qj_close_24"] forState:UIControlStateNormal];
        
    }
    return  _closeBtn;
}

- (void)closeBtnAction {
    [self clickBgView];
}

#pragma mark - 消失
- (void)clickBgView
{
    [UIView animateWithDuration:0.4 delay:0 usingSpringWithDamping:0.95 initialSpringVelocity:0.05 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        
        self.bgView.alpha = 0.0f ;
        self.contentView.frame = CGRectMake(0,[UIScreen mainScreen].bounds.size.height, CGRectGetWidth([UIScreen mainScreen].bounds), Get375Width(210));
        
    } completion:^(BOOL finished) {
        // 动画Animated必须是NO，不然消失之后，会有0.35s时间，再点击无效
        [self dismissViewControllerAnimated:NO completion:nil];
    }];
}

// 这里主动释放一些空间，加速内存的释放，防止有时候消失之后，再点不出来。
- (void)dealloc
{
    NSLog(@"%@ --> dealloc",[self class]);
    [self.bgView removeFromSuperview];
    self.bgView = nil;
    [self.contentView removeFromSuperview];
    self.contentView = nil;
}
@end
