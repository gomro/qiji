//
//  QJCampsitePageViewController.h
//  QJBox
//
//  Created by Sun on 2022/7/6.
//

#import "QJContentBaseViewController.h"

@protocol QJCampsitePageViewControllerDelegate <NSObject>

- (void)pageScrollViewWillBeginScroll;
- (void)pageScrollViewDidEndedScroll;

@end

@interface QJCampsitePageViewController : QJContentBaseViewController
@property (nonatomic, weak) id<QJCampsitePageViewControllerDelegate> scrollDelegate;
- (void)configTitles:(NSArray *)titles childVCs:(NSArray *)childVCs;

- (void)selectItemView:(NSInteger)index;

@property (nonatomic, copy) NSString *campsiteID;

@end

