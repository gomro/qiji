//
//  QJNewsViewController.m
//  QJBox
//
//  Created by Sun on 2022/7/26.
//

#import "QJNewsViewController.h"
#import "GKPageScrollView.h"
#import "QJNewsListViewController.h"
#import "QJNewsPageViewController.h"

#import "QJNewsHeaderView.h"
#import "QJCommunityScrollView.h"
#import "QJCampsiteRequest.h"
#import "QJBannerDataModel.h"
#import "QJNewsCategoryModel.h"
#import "QJNewsTagModel.h"
#import "QJArticleEditerVC.h"
#import "QJCommunityChoiceVideoOrImageVC.h"
#import "QJMineAuthentViewController.h"
@interface QJNewsViewController ()<QJNewsPageViewControllerDelegate, GKPageScrollViewDelegate>
/**联动scrollView*/
@property (nonatomic, strong) QJCommunityScrollView *pageScrollView;
/**顶部视图*/
@property (nonatomic, strong) QJNewsHeaderView *headerView;
/**底部视图*/
@property (nonatomic, strong) QJNewsPageViewController *pageVC;
/**banner数据*/
@property (nonatomic, strong) NSMutableArray *bannerArr;
/**发布*/
@property (nonatomic, strong) UIButton *publishBt;

@property (nonatomic, strong) UIView *pageView;
@property (nonatomic, copy) NSArray *childVCs;
@end

@implementation QJNewsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.view.backgroundColor = UIColorHex(f8f8f8);
    self.view.backgroundColor = [UIColor clearColor];

    [self.view addSubview:self.pageScrollView];
    [self.pageScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.bottom.equalTo(self.view);
    }];
    [self.pageScrollView addSubview:self.headerView];
    [self.pageScrollView sendSubviewToBack:self.headerView];
    [self makePublishBt];

    [self getBannerData];
    [self getCategoryList];
    [self getGetInfoTag];
}

- (void)changeBackViewImage {
    [self.headerView getBackImageStr];
    if(!self.childVCs.count) {
        [self getCategoryList];
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

#pragma mark - GKPageScrollViewDelegate
- (UIView *)headerViewInPageScrollView:(GKPageScrollView *)pageScrollView {
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 0)];
    return header;
}

- (UIView *)pageViewInPageScrollView:(GKPageScrollView *)pageScrollView {
    return self.pageView;
}

- (NSArray<id<GKPageListViewDelegate>> *)listViewsInPageScrollView:(GKPageScrollView *)pageScrollView {
    return self.childVCs;
}

- (void)mainTableViewDidScroll:(UIScrollView *)scrollView isMainCanScroll:(BOOL)isMainCanScroll {
    CGFloat offsetY = scrollView.contentOffset.y;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"kScrollContentOffsetNotification" object:nil userInfo:@{@"offsetY": @(offsetY)}];
}


#pragma mark - GKWBPageViewControllDelegate
- (void)pageScrollViewWillBeginScroll {
    [self.pageScrollView horizonScrollViewWillBeginScroll];
}

- (void)pageScrollViewDidEndedScroll {
    [self.pageScrollView horizonScrollViewDidEndedScroll];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    self.pageScrollView.ceilPointHeight = 0;
}

#pragma mark - 懒加载
- (QJCommunityScrollView *)pageScrollView {
    if (!_pageScrollView) {
        _pageScrollView = [[QJCommunityScrollView alloc] initWithDelegate:self];
        _pageScrollView.mainTableView.backgroundColor = [UIColor clearColor];
        _pageScrollView.isControlVerticalIndicator = YES;
        _pageScrollView.isAllowListRefresh = YES;
        self.pageScrollView.isAllowCustomHit = YES;
        self.pageScrollView.mainTableView.isAllowCustomHit = YES;
        NSInteger insetTopInt = 180*kWScale;
        self.pageScrollView.contentInsetTop = insetTopInt;
    }
    return _pageScrollView;
}

- (QJNewsHeaderView *)headerView {
    if (!_headerView) {
        _headerView = [[QJNewsHeaderView alloc] initWithFrame:CGRectMake(0, 10 * kWScale, kScreenWidth, 170*kWScale)];
    }
    return _headerView;
}

- (QJNewsPageViewController *)pageVC {
    if (!_pageVC) {
        self.pageVC = [[QJNewsPageViewController alloc] init];
        self.pageVC.scrollDelegate = self;
    }
    return _pageVC;
}

- (NSMutableArray *)bannerArr {
    if (!_bannerArr) {
        _bannerArr = [NSMutableArray arrayWithCapacity:0];
    }
    return _bannerArr;
}

- (UIView *)pageView {
    if (!_pageView) {
        [self addChildViewController:self.pageVC];
        [self.pageVC didMoveToParentViewController:self];
        _pageView = self.pageVC.view;
    }
    return _pageView;
}

#pragma mark - 发布
- (UIButton *)publishBt {
    if (!_publishBt) {
        _publishBt = [UIButton new];
        [_publishBt setBackgroundImage:[UIImage imageNamed:@"newsPublishIcon"] forState:UIControlStateNormal];
        [_publishBt addTarget:self action:@selector(publishTitle:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _publishBt;
}

- (void)makePublishBt {
    [self.view addSubview:self.publishBt];
    [self.publishBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.view.mas_right).offset(-15);
        make.bottom.equalTo(self.view.mas_bottom).offset(-24);
        make.width.height.mas_equalTo(70*kWScale);
    }];
}

- (void)publishTitle:(UIButton *)sender {
    DLog(@"发布");
    
    QJCommunityChoiceVideoOrImageVC *vc = [QJCommunityChoiceVideoOrImageVC new];
    QJNavigationController *nav = [[QJNavigationController alloc]initWithRootViewController:vc];
    nav.modalPresentationStyle = UIModalPresentationCustom;
    WS(weakSelf)
    [self checkLogin:^(BOOL isLogin) {
        if (!isLogin) {
            return;
        }
        if (![QJUserManager shareManager].isVerifyState) {
            DLog(@"没实名认证跳转到实名");
            QJMineAuthentViewController *vc = [QJMineAuthentViewController new];
            vc.realNameBlock = ^{
                [weakSelf presentViewController:nav animated:NO completion:nil];
            };
            [weakSelf.navigationController pushViewController:vc animated:YES];
            
        }else{
            [weakSelf presentViewController:nav animated:NO completion:nil];
        }
        
    }];
   
    
  
    
}

#pragma mark - NetWork
- (void)getBannerData {
    QJCampsiteRequest *startRequest = [[QJCampsiteRequest alloc] init];
    [startRequest netWorkGetBannerData:@"INFO"];
    [startRequest setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            NSArray *dataArr = request.responseJSONObject[@"data"];
            [self.bannerArr removeAllObjects];
            for (NSDictionary *dic in dataArr) {
                QJBannerDataModel *model = [QJBannerDataModel modelWithDictionary:dic];
                [self.bannerArr addObject:model];
                self.headerView.dataModelArr = [self.bannerArr copy];
            }

        }
    }];
}

- (void)getCategoryList {
    QJCampsiteRequest *startRequest = [[QJCampsiteRequest alloc] init];
    [startRequest netWorkGetInfoCategory];
    [startRequest setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            NSMutableArray *categoryList = [NSMutableArray arrayWithCapacity:0];
            QJNewsCategoryModel *model = [[QJNewsCategoryModel alloc] init];
            model.name = @"推荐";
            model.code = @"";
            [categoryList addObject:model];
            NSArray *dataArr = request.responseJSONObject[@"data"];
            for (NSDictionary *dic in dataArr) {
                QJNewsCategoryModel *model = [QJNewsCategoryModel modelWithDictionary:dic];
                [categoryList addObject:model];
            }
            QJNewsCategoryModel *modelAtt = [[QJNewsCategoryModel alloc] init];
            modelAtt.name = @"关注";
            modelAtt.code = @"";
            [categoryList addObject:modelAtt];
            [self configCategoryList:[categoryList copy]];
        }
    }];
}

// 获取标签数组
- (void)getGetInfoTag {
    QJCampsiteRequest *startRequest = [[QJCampsiteRequest alloc] init];
    [startRequest netWorkGetInfoTagKeyword:@""];
    [startRequest setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            
            NSArray *dataArr = request.responseJSONObject[@"data"];
            NSMutableArray *tagArr = [NSMutableArray array];
            for (NSDictionary *dic in dataArr) {
                QJNewsTagModel *model = [QJNewsTagModel modelWithDictionary:dic];
                [tagArr addObject:model];
            }
            self.pageVC.tags = tagArr;
        }
    }];
}

- (void)configCategoryList:(NSArray *)arr {
    NSMutableArray *titles = [NSMutableArray arrayWithCapacity:0];
    NSMutableArray *childVCs = [NSMutableArray arrayWithCapacity:0];

    for (QJNewsCategoryModel *model in arr) {
        NSString *name = kCheckStringNil(model.name);
        [titles addObject:name];
        QJNewsListViewController *vc = [QJNewsListViewController new];
        vc.categoryModel = model;
        [childVCs addObject:vc];
    }
    self.childVCs = [childVCs copy];
    [self.pageVC configTitles:[titles copy] childVCs:[childVCs copy]];
    [self.pageScrollView reloadData];
}
@end
