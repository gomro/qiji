//
//  QJCampsiteViewController.m
//  QJBox
//
//  Created by Sun on 2022/7/5.
//

#import "QJCampsiteViewController.h"
#import "QJCampsiteHomeListViewController.h"
#import "QJEditCampsiteHeaderView.h"
#import "QJCampsiteTapView.h"
#import "QJEditCampsiteViewController.h"
#import "QJCampsiteListModel.h"
#import "QJCampsiteRequest.h"
 
#import "QJArticleEditerVC.h"
/* 倒计时 */
#import "QJCountDown.h"
#import "QJCheckInPostRequest.h"
#import "QJCampsiteListViewController.h"
@interface QJCampsiteViewController ()

@property (nonatomic, strong) NSArray *imageNames;
@property (nonatomic, strong) QJCampsiteTapView *campsiteTapView;
/**发布*/
@property (nonatomic, strong) UIButton *publishBt;
@property (nonatomic, strong) UIButton *countDownButton;
@property (nonatomic, strong) QJCountDown *countDown;

@property (nonatomic, strong) NSMutableArray *chirldVC;
@end

@implementation QJCampsiteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor clearColor];
    
    NSMutableArray *imageArr = [NSMutableArray arrayWithCapacity:0];
    [self.chirldVC removeAllObjects];
    for (QJCampsiteListModel *model in self.myCampsiteData) {
        
        QJCampsiteHomeListViewController *collectionView = [[QJCampsiteHomeListViewController alloc] init];
        [self.chirldVC addObject:collectionView];
        NSString *imageString = [kCheckStringNil(model.iconAddress) checkImageUrlString];
        NSURL *imageURL = [NSURL URLWithString:imageString];
        [imageArr addObject:imageURL];
    }
    [imageArr addObject:[NSURL URLWithString:@"communityAddedIcon"]];
    self.titles = [imageArr copy];
    self.imageNames = [imageArr copy];
    self.myCategoryView.imageURLs = self.imageNames;
    self.myCategoryView.loadImageCallback = ^(UIImageView *imageView, NSURL *imageURL) {
        if ([imageURL isEqual:[NSURL URLWithString:@"communityAddedIcon"]]) {
            imageView.image = [UIImage imageNamed:@"communityAddedIcon"];
        } else {
            [imageView setImageWithURL:imageURL placeholder:nil];
        }
    };
    self.myCategoryView.selectedImageURLs = self.imageNames;
    self.myCategoryView.imageZoomEnabled = YES;
    self.myCategoryView.imageZoomScale = 1.3;
    self.myCategoryView.imageCornerRadius = 8*kWScale;
    self.myCategoryView.imageSize = CGSizeMake(30 *kWScale, 30 *kWScale);
    self.myCategoryView.averageCellSpacingEnabled = NO;
    self.myCategoryView.contentEdgeInsetLeft = 20;
    self.myCategoryView.cellSpacing = 10 * kWScale;
    
    self.myCategoryView.cellWidthZoomEnabled = YES;
    self.myCategoryView.cellWidthZoomScale = 1.3;
    
    [self.view addSubview:self.campsiteTapView];
    self.campsiteTapView.frame = CGRectMake(20, 10*kWScale, kScreenWidth - 20, 50*kWScale);
    self.campsiteTapView.alpha = 0;
    if (self.myCampsiteData.count) {
        QJCampsiteListModel *model = self.myCampsiteData[0];
        [self.campsiteTapView configureTapWithData:model];
    }    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeData:) name:@"kScrollContentOffsetNotification" object:nil];

    [self makePublishBt];
    
    if ([QJAppTool shareManager].campsiteID.length != 0) {
        NSDictionary *dic = @{@"campsiteId":[QJAppTool shareManager].campsiteID,@"isInit":@(YES)};
        [self changeSelectCampsite:dic];
    }
}

- (void)changeBackViewImage {
    NSInteger index = self.myCategoryView.selectedIndex;
    if(self.myCampsiteData.count) {
        QJCampsiteListModel *model = self.myCampsiteData[index];
        [self.campsiteTapView configureTapWithData:model];
    }
    
    if (self.chirldVC.count > index) {
        QJCampsiteHomeListViewController *collectionView = self.chirldVC[index];
        [collectionView changeBackViewImage];
    }
}

// 改变当前选择的营地
- (void)changeSelectCampsite:(NSDictionary *)dic {
    NSString *campsiteId = dic[@"campsiteId"];
    BOOL isInit = dic[@"isInit"];
    NSInteger campsiteIndex = -1;
    for (int i = 0; i < self.myCampsiteData.count; i++) {
        QJCampsiteListModel *model = self.myCampsiteData[i];
        if ([model.campsiteID isEqualToString:campsiteId]) {
            campsiteIndex = i;
        }
    }
    // 如果是第一次进入，1秒后再次调用方法
    if (campsiteIndex == -1) {
        [self performSelector:@selector(changeSelectCampsite:) withObject:dic afterDelay:1];
    } else {
        if (isInit) {
            [self.categoryView setDefaultSelectedIndex:campsiteIndex];
        } else {
            [self.categoryView selectItemAtIndex:campsiteIndex];
        }
    }
    [self changeBackViewImage];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[IQKeyboardManager sharedManager] setEnable:NO];
    [IQKeyboardManager sharedManager].enableAutoToolbar = YES; // 控制是否显示键盘上的工具条
    if ([QJAppTool shareManager].campsiteID.length != 0) {
        NSDictionary *dic = @{@"campsiteId":[QJAppTool shareManager].campsiteID,@"isInit":@(NO)};
        [self changeSelectCampsite:dic];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[IQKeyboardManager sharedManager] setEnable:YES];
    [IQKeyboardManager sharedManager].enableAutoToolbar = NO; // 控制是否显示键盘上的工具条
    [QJAppTool shareManager].campsiteID = @"";
}

- (void)makePublishBt {
    [self.view addSubview:self.publishBt];
    [self.publishBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.view.mas_right).offset(-15);
        make.bottom.equalTo(self.view.mas_bottom).offset(-24);
        make.width.height.mas_equalTo(70);
    }];
    
    [self.view addSubview:self.countDownButton];
    [self.countDownButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.view.mas_right).offset(-15);
        make.top.equalTo(self.view).offset(10);
        make.width.mas_equalTo(50);
        make.height.mas_equalTo(26);
    }];
}

- (void)resetCountButtonStatus{
    self.countDown = [QJCountDown new];
    [self.countDown destoryTimer];
    if (![[QJAppTool shareManager].taskID isEqualToString:@""]) {
        self.countDownButton.hidden = NO;
        WS(weakSelf)
        [self.countDown countDownWithfinishDate:60 completeBlock:^(NSInteger day, NSInteger hour, NSInteger minute, NSInteger second) {
            [weakSelf.countDownButton setTitle:[NSString stringWithFormat:@"%ld", (long)second] forState:UIControlStateNormal];
        }];
        self.countDown.TimerStopComplete = ^{
            dispatch_async(dispatch_get_main_queue(), ^{
                weakSelf.countDownButton.userInteractionEnabled = YES;
                [weakSelf qjTaskComplate];
            });
        };
    }else{
        self.countDownButton.hidden = YES;
    }
}

- (void)changeData:(NSNotification *)notification {
    NSDictionary *dic = notification.userInfo;
    CGFloat offsetY = [dic[@"offsetY"] floatValue];
    CGFloat alpha = offsetY / (134*kWScale);
    if (alpha > 0.97) {
        alpha = 1;
    }
    self.myCategoryView.alpha = 1 - alpha;
    self.campsiteTapView.alpha = alpha;
    if (alpha > 0.2) {
        self.listContainerView.scrollView.scrollEnabled = NO;
    } else {
        self.listContainerView.scrollView.scrollEnabled = YES;
    }
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    self.myCategoryView.frame = CGRectMake(0, 10*kWScale, kScreenWidth, 50*kWScale);
    self.listContainerView.frame = CGRectMake(0, 60*kWScale, kScreenWidth, kScreenHeight - Bottom_iPhoneX_SPACE - NavigationBar_Bottom_Y - 60*kWScale - 49);
}

- (JXCategoryImageView *)myCategoryView {
    return (JXCategoryImageView *)self.categoryView;
}

- (JXCategoryBaseView *)preferredCategoryView {
    return [[JXCategoryImageView alloc] init];
}

- (NSMutableArray *)chirldVC {
    if (!_chirldVC) {
        _chirldVC = [NSMutableArray arrayWithCapacity:0];
    }
    return _chirldVC;
}

- (CGFloat)preferredCategoryViewHeight {
    return 0;
}

- (QJCampsiteTapView *)campsiteTapView {
    if (!_campsiteTapView) {
        _campsiteTapView = [[QJCampsiteTapView alloc] init];
    }
    return _campsiteTapView;
}

- (UIButton *)publishBt {
    if (!_publishBt) {
        _publishBt = [UIButton new];
        [_publishBt setBackgroundImage:[UIImage imageNamed:@"newsPublishIcon"] forState:UIControlStateNormal];
        [_publishBt addTarget:self action:@selector(publishTitle:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _publishBt;
}

- (UIButton *)countDownButton {
    if (!_countDownButton) {
        _countDownButton = [UIButton new];
        _countDownButton.backgroundColor = [UIColor colorWithWhite:0 alpha:0.49];
        [_countDownButton setTitle:@"" forState:UIControlStateNormal];
        _countDownButton.titleLabel.font = kFont(12);
        _countDownButton.userInteractionEnabled = NO;
        [_countDownButton addTarget:self action:@selector(qjTaskComplate) forControlEvents:UIControlEventTouchUpInside];
        _countDownButton.layer.cornerRadius = 13;
        _countDownButton.hidden = YES;
    }
    return _countDownButton;
}

- (NSMutableArray *)myCampsiteData {
    if (!_myCampsiteData) {
        _myCampsiteData = [NSMutableArray arrayWithCapacity:0];
    }
    return _myCampsiteData;
}
#pragma mark - JXCategoryListContainerViewDelegate
- (NSInteger)numberOfListsInlistContainerView:(JXCategoryListContainerView *)listContainerView {
    return self.titles.count - 1;
}
- (id<JXCategoryListContentViewDelegate>)listContainerView:(JXCategoryListContainerView *)listContainerView initListForIndex:(NSInteger)index {
    QJCampsiteListModel *model = self.myCampsiteData[index];
    QJCampsiteHomeListViewController *collectionView = self.chirldVC[index];
    collectionView.campsiteID = kCheckStringNil(model.campsiteID);
    return collectionView;
}

#pragma mark - JXCategoryViewDelegate
- (void)categoryView:(JXCategoryBaseView *)categoryView didSelectedItemAtIndex:(NSInteger)index {
    QJCampsiteListModel *model = self.myCampsiteData[index];
    [self.campsiteTapView configureTapWithData:model];
    [self changeBackViewImage];
    //作为嵌套的子容器，不需要处理侧滑手势处理。示例demo因为是继承，所以直接覆盖掉该代理方法，达到父类不调用下面一行处理侧滑手势的代码。
//    self.navigationController.interactivePopGestureRecognizer.enabled = (index == 0);
}

- (BOOL)categoryView:(JXCategoryBaseView *)categoryView canClickItemAtIndex:(NSInteger)index {
    if (index == self.titles.count - 1) {
        DLog(@"点击添加");
        QJEditCampsiteViewController *editCampsite = [[QJEditCampsiteViewController alloc] init];
        editCampsite.campsiteStyle = QJCampsiteViewStyleEdit;
        [self.navigationController pushViewController:editCampsite animated:YES];
        return NO;
    }
    return YES;
}

- (void)publishTitle:(UIButton *)sender {    
    if (self.myCampsiteData.count) {
        NSInteger selectedIndex = self.myCategoryView.selectedIndex;
        QJCampsiteListModel *model = self.myCampsiteData[selectedIndex];
        QJCampsiteHomeListViewController *homelistVC = self.chirldVC[selectedIndex];
        QJCampsiteListViewController *listvc = homelistVC.childVCs.firstObject;
        if(listvc.banType == 2){//封禁
            [QJAppTool showWarningToast:@"很抱歉,营地违反平台规则已封停"];
             
            return;
        }
        QJArticleEditerVC *vc = [QJArticleEditerVC new];
        vc.campsiteID =  kCheckStringNil(model.campsiteID);
        vc.type = QJEditerTypeDongTai;
        [self.navigationController pushViewController:vc animated:YES];
    }
   
}

/**
 * @author: zjr
 * @date: 2022-8-30
 * @desc: 奇迹有礼任务完成
 */
- (void)qjTaskComplate{
    QJCheckInPostRequest *checkinReq = [[QJCheckInPostRequest alloc] init];
    [checkinReq getTaskCompleteRequest:[QJAppTool shareManager].taskID];
    WS(weakSelf)
    checkinReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        weakSelf.countDownButton.hidden = YES;
        [QJAppTool shareManager].taskID = @"";
        if (ResponseSuccess) {
            NSString *simpleDesc = @"";
            if ([EncodeDicFromDic(request.responseObject, @"data") isKindOfClass:[NSDictionary class]]) {
                NSDictionary *dataDic = EncodeDicFromDic(request.responseObject, @"data");
                simpleDesc = EncodeStringFromDicDefEmtryValue(dataDic, @"simpleDesc");
            }
            if (![kCheckNil(simpleDesc) isEqualToString:@""]) {
                [[QJAppTool getCurrentViewController].view makeToast:simpleDesc duration:3 position:CSToastPositionCenter];
            }
        }else{
            NSString *msg = EncodeStringFromDic(request.responseObject, @"message");
            [weakSelf.view makeToast:msg];
        }
    };
    checkinReq.failureCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        [weakSelf.view makeToast:@"数据异常 请稍后再试"];
    };

}

- (UIView *)listView {
    return self.view;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];

}

@end
