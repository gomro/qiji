//
//  QJCampsiteViewController.h
//  QJBox
//
//  Created by Sun on 2022/7/5.
//

#import "QJContentBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJCampsiteViewController : QJContentBaseViewController<JXCategoryListContentViewDelegate>
@property (nonatomic, strong) NSMutableArray *myCampsiteData;
- (void)changeBackViewImage;
- (void)resetCountButtonStatus;

@end

NS_ASSUME_NONNULL_END
