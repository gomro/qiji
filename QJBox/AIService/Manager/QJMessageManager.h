//
//  QJMessageManager.h
//  QJBox
//
//  Created by macm on 2022/8/12.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

//消息中控件与内容间隔
static NSInteger const kChat_margin = 8;

//消息中控件与内容间隔，针对设计图做出微调的间距
static NSInteger const kChat_min_margin = 4;

//头像距离页面左右边距
static NSInteger const kChat_icon_margin = 16;

//time
//时间间隙
static NSInteger const kChat_margin_time = 7;

//icon
//头像宽高
static NSInteger const kChat_icon = 34;

//单行气泡高度
static NSInteger const kChat_min_h = kChat_icon;

//内容字体
#define kChatFont_content [UIFont systemFontOfSize:14*kWScale]

//时间字体
#define kChatFont_time [UIFont systemFontOfSize:10*kWScale]

//内容最大宽度（截取到气泡）
#define kChat_content_maxW (kScreenWidth - 4*kChat_margin - 2*kChat_icon - 2*kChat_icon_margin)

#define TIME_Formatter2 @"yyyy-MM-dd-HH-mm-ss-SSS"

#define TIME_Formatter @"MM-dd HH:mm"

#define D_MINUTE    60

/**
 *  发送方
 */
typedef enum{
    QJBubbleMessageType_Send = 0, // 发送
    QJBubbleMessageType_Receiving, // 接收
}QJBubbleMessageType;

/**
 *  消息发送状态
 */
typedef enum{
    QJSendMessageType_Successed = 1,  //发送成功
    QJSendMessageType_Failed,         //发送失败
    QJSendMessageType_Sending         //发送中
}QJSendMessageStatus;

/**
 *  消息类型
 */
typedef enum {
    QJMessageBodyType_text = 1,       //文本类型
    QJMessageBodyType_custom,         //自定义类型
}QJMessageBodyType;

@class QJMessageFrame;
@class QJMessage;

@interface QJMessageManager : NSObject

#pragma mark - 获取零时区时间
+ (NSString *)getTimeWithZone;

#pragma mark 获取零时区转当前时区的date
+ (NSDate *)getCurrentDateWithZone:(NSString *)zone;

#pragma mark - 获取时间差
+ (NSInteger)getLeadTime:(NSString *)time;

#pragma mark - 是否显示时间
+ (BOOL)isShowTimeWithTime:(NSString *)time setTime:(NSString *)setTime;

#pragma mark - 获取聊天时间
+ (NSString *)getChatTimeWithTime:(NSString *)time;

#pragma mark 计算富文本的size
+ (CGSize)getSizeWithAtt:(NSAttributedString *)att
                 maxSize:(CGSize)maxSize;

#pragma mark 计算字符串的size
+ (CGSize)getSizeWithStr:(NSString *)str
                    font:(UIFont *)font
                 maxSize:(CGSize)maxSize;

#pragma mark 处理数据属性
+ (QJMessageFrame *)dealDataWithMessage:(QJMessage *)message dateSoure:(NSMutableArray *)dataSoure time:(NSString *)time;

@end

NS_ASSUME_NONNULL_END
