//
//  QJMessageDataManager.m
//  QJBox
//
//  Created by macm on 2022/8/16.
//

#import "QJMessageDataManager.h"
#import "FMDatabase.h"
#import "QJMessageFrame.h"
 
@interface QJMessageDataManager()
@property (nonatomic, strong) FMDatabase *dataBase;
 
@end

@implementation QJMessageDataManager

//单例
+ (instancetype)shareManager {
    static QJMessageDataManager* manager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager =[[self alloc] init];
    });
    return manager;
}

// 新建数据库并打开
- (BOOL)createDataBase {
    if (![_dataBase open]) {
        NSArray *documents=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsPath = [documents firstObject];
        NSString *dbPath = [documentsPath stringByAppendingPathComponent:@"QJMessageDataBase.db"];
        NSLog(@"%@", dbPath);
        _dataBase = [FMDatabase databaseWithPath:dbPath];
        if ([_dataBase open]) {
            NSLog(@"打开成功");
            return [self createTable];
        } else {
            NSLog(@"打开失败");
            [self.dataBase close];
            return NO;
        }
    } else {
        return YES;
    }
    
}

// 创建表
- (BOOL)createTable {
    NSString *createTableSql = @"create table if not exists Message(id integer primary key autoincrement,messageId text,sendTime text,msgText text,messageState integer,bubbleMessageType integer,messageType integer,fromText text,contentIdText text,pushType text)";
    BOOL success = [_dataBase executeUpdate:createTableSql];
    if (success) {
        NSLog(@"创建表成功");
    } else {
        NSLog(@"创建表失败");
        [self.dataBase close];
    }
    return success;
}
 
//写入数据
- (BOOL)insertMessage:(QJMessage *)msg {
    if ([_dataBase open]) {
        NSString *insertSQL = @"insert into Message (messageId,sendTime,msgText,messageState,bubbleMessageType,messageType,fromText,contentIdText,pushType) values (?,?,?,?,?,?,?,?,?)";
        BOOL success = [_dataBase executeUpdate:insertSQL,msg.messageId,msg.sendTime,msg.text,@(msg.messageState),@(msg.bubbleMessageType),@(msg.messageType),msg.fromText,msg.contentIdText,msg.pushType];
        if (success) {
            NSLog(@"插入成功");
        } else {
            NSLog(@"插入失败");
            [QJAppTool showWarningToast:@"消息发送失败"];
        }
        [_dataBase close];
        return success;
    } else {
        return NO;
    }
}

//修改数据
- (BOOL)updateMessage:(QJMessage *)msg {
    if ([_dataBase open]) {
        NSString *selectSQL = [NSString stringWithFormat:@"SELECT * FROM Message WHERE messageId = '%@'",msg.messageId];
        //查询返回的为一个结果集 
        FMResultSet *set = [_dataBase executeQuery:selectSQL, @0];
        NSInteger idStr = -1;
        while ([set next]) {
            //获取下一条记录，如果没有下一条，返回NO，跳出循环
            //取数据
            idStr = [set intForColumn:@"id"];
        }
        if (idStr != -1) {
            NSString *updateSQL = @"update Message set messageState=? where id=?";
            BOOL success = [_dataBase executeUpdate:updateSQL, @(msg.messageState), @(idStr)];
            if (success) {
                NSLog(@"更新成功");
            } else {
                NSLog(@"更新失败");
                [QJAppTool showWarningToast:@"消息发送失败"];
            }
            [_dataBase close];
            return success;

        } else {
            return NO;
        }
        
    } else {
        return NO;
    }
}

// 查询数据
- (NSArray *)selectMessage:(NSInteger)page idStr:(NSInteger)idStr {
    NSMutableArray *temp = [NSMutableArray array];

    // 如果id=-2，说明是第一次进入，此时数据库没有历史数据，不需要返回数据
    if (idStr == -2) {
        return temp;
    }
    if ([_dataBase open]) {
        NSString *selectSQL = [NSString stringWithFormat:@"SELECT*FROM Message WHERE id <= %ld ORDER BY id DESC LIMIT %ld,10",idStr,page*10];
        // 如果id=-1，说明是退出后重新进入加载数据，不需要按条件查询，直接查询所有
        if (idStr == -1) {
            selectSQL = [NSString stringWithFormat:@"SELECT*FROM Message ORDER BY id DESC LIMIT %ld,10",page*10];
        }

        //查询返回的为一个结果集
        FMResultSet *set = [_dataBase executeQuery:selectSQL, @0];
        //需要对结果集进行遍历操作
        while ([set next]) {
            //获取下一条记录，如果没有下一条，返回NO，跳出循环
            //取数据
            QJMessage *model = [[QJMessage alloc] init];
            model.idStr = [set intForColumn:@"id"];
            model.messageId = [set stringForColumn:@"messageId"];
            model.sendTime = [set stringForColumn:@"sendTime"];
            model.text = [set stringForColumn:@"msgText"];
            model.messageState = [set intForColumn:@"messageState"];
            model.bubbleMessageType = [set intForColumn:@"bubbleMessageType"];
            model.messageType = [set intForColumn:@"messageType"];
            model.fromText = [set stringForColumn:@"fromText"];
            model.contentIdText = [set stringForColumn:@"contentIdText"];
            model.pushType = [set stringForColumn:@"pushType"];

            [temp addObject:model];
        }
        
        [_dataBase close];
    }
    return [temp sortedArrayUsingSelector:@selector(class)];

}

// 默认数据添加
- (QJMessage *)addPublicParameters:(NSString *)text bubbleMessageType:(QJBubbleMessageType)bubbleMessageType bodyMessageType:(QJMessageBodyType)bodyMessageType {
    
    QJMessage *model = [[QJMessage alloc] init];
    model.messageId = [QJMessageManager getTimeWithZone];
    model.messageState = bubbleMessageType == QJBubbleMessageType_Send ? QJSendMessageType_Sending : QJSendMessageType_Successed;
    model.sendTime = [QJMessageManager getTimeWithZone];
    model.text = text;
    model.bubbleMessageType = bubbleMessageType;
    model.messageType = bodyMessageType;
    
    return model;

}

// 删除一条数据
- (BOOL)deleteMessage:(QJMessage *)msg {
    if ([_dataBase open]) {
        NSString *selectSQL = [NSString stringWithFormat:@"DELETE FROM Message WHERE messageId = '%@'",msg.messageId];
        BOOL success = [_dataBase executeUpdate:selectSQL];
        if (success) {
            NSLog(@"删除成功");
        } else {
            NSLog(@"删除失败");
        }
        [_dataBase close];
        return success;
        
    } else {
        return NO;
    }
}

// 删除全部数据
- (void)deleteAll {
    if ([_dataBase open]) {
        NSString *deleteAllSQL = @"delete from Message where 1";
        BOOL success = [_dataBase executeUpdate:deleteAllSQL];
        if (success) {
            NSLog(@"删除成功");
        } else {
            NSLog(@"删除失败");
        }
        [_dataBase close];
    }
}
 
@end

