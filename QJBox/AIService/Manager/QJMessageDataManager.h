//
//  QJMessageDataManager.h
//  QJBox
//
//  Created by macm on 2022/8/16.
//

#import <Foundation/Foundation.h>
#import "QJMessageManager.h"

NS_ASSUME_NONNULL_BEGIN
@class QJMessage;

@interface QJMessageDataManager : NSObject

//单例
+ (instancetype)shareManager;

// 新建数据库并打开
- (BOOL)createDataBase;

//写入数据
- (BOOL)insertMessage:(QJMessage *)msg;

//重新发送数据
- (BOOL)updateMessage:(QJMessage *)msg;

// 查询数据
- (NSArray *)selectMessage:(NSInteger)page idStr:(NSInteger)idStr;

// 默认数据添加
- (QJMessage *)addPublicParameters:(NSString *)text bubbleMessageType:(QJBubbleMessageType)bubbleMessageType bodyMessageType:(QJMessageBodyType)bodyMessageType;

// 删除一条数据
- (BOOL)deleteMessage:(QJMessage *)msg;

// 删除全部数据
- (void)deleteAll;

// 时间差
@property (nonatomic, assign) NSInteger leadTime;

@end

NS_ASSUME_NONNULL_END
