//
//  QJMessageManager.m
//  QJBox
//
//  Created by macm on 2022/8/12.
//

#import "QJMessageManager.h"
#import "QJMessageFrame.h"

@implementation QJMessageManager

#pragma mark - 获取零时区时间
+ (NSString *)getTimeWithZone {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:TIME_Formatter2];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    
    return [dateFormatter stringFromDate:[NSDate date]];
}
 
#pragma mark 获取零时区转当前时区的date
+ (NSDate *)getCurrentDateWithZone:(NSString *)zone{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:TIME_Formatter2];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    
    return [dateFormatter dateFromString:zone];
}

#pragma mark - 获取时间差
+ (NSInteger)getLeadTime:(NSString *)time {
    return [[self getCurrentDateWithZone:[self getTimeWithZone]] timeIntervalSinceDate:[self getCurrentDateWithZone:time]];
}

#pragma mark - 是否显示时间
+ (BOOL)isShowTimeWithTime:(NSString *)time setTime:(NSString *)setTime {
    
    if (!setTime.length) {
        return YES;
    }
    //大于5分钟进行显示
    BOOL greater = [[self getCurrentDateWithZone:time] timeIntervalSinceDate:[self getCurrentDateWithZone:setTime]] > 1*D_MINUTE;
    BOOL less = [[self getCurrentDateWithZone:setTime] timeIntervalSinceDate:[self getCurrentDateWithZone:time]] > 1*D_MINUTE;

    if (greater || less) {
        return YES;
    }else{
        return NO;
    }
}

#pragma mark - 获取聊天时间
+ (NSString *)getChatTimeWithTime:(NSString *)time {
    
    NSDate *myDate = [self getCurrentDateWithZone:time];

    NSCalendar *calendar = [ NSCalendar currentCalendar ];
    int unit = NSCalendarUnitDay | NSCalendarUnitMonth |  NSCalendarUnitYear ;
    NSDateComponents *nowCmps = [calendar components:unit fromDate:[ NSDate date ]];
    NSDateComponents *myCmps = [calendar components:unit fromDate:myDate];
    
    NSDateFormatter *dateFmt = [[NSDateFormatter alloc ] init ];
    
    //2. 指定日历对象,要去取日期对象的那些部分.
    NSDateComponents *comp =  [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitWeekday fromDate:myDate];
    NSString *timeString = @"";
    
    if (nowCmps.year != myCmps.year) {
        dateFmt.dateFormat = @"yyyy-MM-dd HH:mm";
        timeString = [dateFmt stringFromDate:myDate];
    } else {
        if (nowCmps.day == myCmps.day && nowCmps.month == myCmps.month && nowCmps.year == myCmps.year) {
            dateFmt.dateFormat = @"HH:mm";
            timeString = [dateFmt stringFromDate:myDate];
            NSString *sub = [timeString substringToIndex:2];
            if (sub.integerValue > 12) {
                timeString = [NSString stringWithFormat:@"下午 %.2ld%@", sub.integerValue-12,[timeString substringFromIndex:2]];
            } else {
                timeString = [NSString stringWithFormat:@"上午 %@", timeString];
            }
        } else {
            if ((nowCmps.day-myCmps.day) <=7 && nowCmps.month == myCmps.month && nowCmps.year == myCmps.year) {
                dateFmt.dateFormat = @"HH:mm";
                NSString *weekString = @"";
                switch (comp.weekday) {
                    case 1:
                        weekString = @"周日";
                        break;
                    case 2:
                        weekString = @"周一";
                        break;
                    case 3:
                        weekString = @"周二";
                        break;
                    case 4:
                        weekString = @"周三";
                        break;
                    case 5:
                        weekString = @"周四";
                        break;
                    case 6:
                        weekString = @"周五";
                        break;
                    case 7:
                        weekString = @"周六";
                        break;
                    default:
                        break;
                }
                timeString = [dateFmt stringFromDate:myDate];
                NSString *sub = [timeString substringToIndex:2];
                if (sub.integerValue > 12) {
                    timeString = [NSString stringWithFormat:@"%@ 下午 %.2ld%@", weekString, sub.integerValue-12,[timeString substringFromIndex:2]];
                } else {
                    timeString = [NSString stringWithFormat:@"%@ 上午 %@", weekString, timeString];
                }
            }else {
                dateFmt.dateFormat = @"MM月dd日";
                NSString *monthString = [dateFmt stringFromDate:myDate];
                
                dateFmt.dateFormat = @"HH:mm";
                timeString = [dateFmt stringFromDate:myDate];
                NSString *sub = [timeString substringToIndex:2];
                if (sub.integerValue > 12) {
                    timeString = [NSString stringWithFormat:@"%@ 下午 %.2ld%@", monthString,sub.integerValue-12,[timeString substringFromIndex:2]];
                } else {
                    timeString = [NSString stringWithFormat:@"%@ 上午 %@", monthString,timeString];
                }
            }
        }
    }
    return timeString;
}

#pragma mark 计算富文本的size
+ (CGSize)getSizeWithAtt:(NSAttributedString *)att
                 maxSize:(CGSize)maxSize{
    
    if (att.length == 0) {
        return CGSizeZero;
    }
    
    CGSize size = [att boundingRectWithSize:maxSize options: NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil].size;
    if (att.length && !size.width && !size.height) {
        size = maxSize;
    }
    return CGSizeMake(ceilf(size.width), ceilf(size.height));
}

#pragma mark 计算字符串的size
+ (CGSize)getSizeWithStr:(NSString *)str
                    font:(UIFont *)font
                 maxSize:(CGSize)maxSize{
    if (str.length == 0) {
        return CGSizeZero;
    }
    
    CGSize size = [str boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:font} context:nil].size;
    if (str.length && !size.width && !size.height) {
        size = maxSize;
    }
    return CGSizeMake(ceilf(size.width), ceilf(size.height));
}

#pragma mark 处理数据属性
+ (QJMessageFrame *)dealDataWithMessage:(QJMessage *)message dateSoure:(NSMutableArray *)dataSoure time:(NSString *)time {
    __block QJMessageFrame *messageFrame = [[QJMessageFrame alloc] init];

    //是否需要添加
    __block BOOL isAdd = YES;

    //为了判断是否有重复的数据
    [dataSoure enumerateObjectsUsingBlock:^(QJMessageFrame *obj, NSUInteger idx, BOOL *_Nonnull stop) {
      if ([message.messageId isEqualToString:obj.message.messageId]) { //同一条消息
          *stop = YES;

          //发送时间判断
          if ([message.sendTime isEqualToString:obj.message.sendTime]) {
              //相同 - 更新消息(如：发送状态)
              isAdd = NO;
              obj.message = message;
              messageFrame = obj;

              [dataSoure replaceObjectAtIndex:idx withObject:messageFrame];

          } else {
              //消息重复了 进行删除(保护)
              [dataSoure removeObjectAtIndex:idx];
          }
      }
    }];

    //需要添加
    if (isAdd) {
        //是否显示时间
        messageFrame.showTime = [self isShowTimeWithTime:message.sendTime setTime:time];

        //设置数据
        [messageFrame setMessage:message];

        return messageFrame;
    }
    return nil;
}

@end
