//
//  QJMineServiceViewController.m
//  QJBox
//
//  Created by macm on 2022/8/12.
//

#import "QJMineServiceViewController.h"
#import "QJMessageTextTableViewCell.h"
#import "QJMessageCustomTableViewCell.h"
#import "QJMessageInputView.h"
#import "FMDatabase.h"
#import "QJMessageDataManager.h"
#import "QJMineRequest.h"
#import "QJMineServiceModel.h"
#import "QJProductDetailViewController.h"
#import "AppDelegate.h"
#import "QJConsultDetailViewController.h"
#import "QJHelpCenterDetailViewController.h"
#import "QJProblemFeedbackVC.h"
#import "QJHelpCenterSearchVC.h"
#import "QJCommunityViewController.h"
#import "QJCampsiteListModel.h"
#import "QJCampsiteRequest.h"
#import "QJMediator.h"

@interface QJMineServiceViewController ()<UITableViewDelegate, UITableViewDataSource, QJMessageInputViewDelegate,QJMessageTableViewCellDelegate>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *dataArray;

//显示的时间集合
@property (nonatomic, strong) NSMutableArray *timeArr;

@property (nonatomic, strong) QJMessageInputView *inputView;

//加载控件
@property (nonatomic, strong) UIRefreshControl *refresh;
@property (nonatomic, strong) NSMutableArray *historyArray; // 历史数据
@property (nonatomic, assign) NSInteger historyId; // 历史数据最新id

@property (nonatomic, strong) FMDatabase *dataBase;

@property (nonatomic, assign) NSInteger page;
@property (nonatomic, strong) dispatch_source_t timer;
@end

@implementation QJMineServiceViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [IQKeyboardManager sharedManager].enableAutoToolbar = NO;
    [[IQKeyboardManager sharedManager] setEnable:NO];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [IQKeyboardManager sharedManager].enableAutoToolbar = YES;
    [[IQKeyboardManager sharedManager] setEnable:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"客服中心";
    self.automaticallyAdjustsScrollViewInsets = NO;

    self.page = 0;
    self.historyId = -1;
    [self createView];
    
    // 创建数据库并打开
    if ([[QJMessageDataManager shareManager] createDataBase]) {
        // 第一次进入页面,初始化数据
        [self initMessageData];
        [self tableViewScrollToBottom];
    } else {
        [QJAppTool showWarningToast:@"数据初始化失败，请稍后重试！"];
    }
    
}

- (void)sendRequestQueryType:(NSInteger)queryType content:(NSString *)content block:(void (^)(BOOL sendSuccess))block {
    @weakify(self);
    QJMineRequest *request = [[QJMineRequest alloc] init];
    [request requestAiCustomerServiceQueryContent:content queryType:queryType completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        @strongify(self);
        if (isSuccess) {
            if (block) {
                block(YES);
            }
            NSDictionary *data = EncodeDicFromDic(request.responseJSONObject, @"data");
            QJMineServiceModel *model = [QJMineServiceModel modelWithJSON:data];
            if (queryType == 5) {
                LSUserDefaultsSET(model.headPortraitUrl, @"HeadPortraitUrl");
                LSUserDefaultsSET(@(model.endTime*60), @"ConversationTime");

                [self sendMessageTypeInit:model.replyHead];
            } else if (queryType == 6) {
                NSString *inputString = model.replyHead;
                NSString *idString = @"-1";
                for (QJMineServiceReplyContentModel *item in model.replyContents) {
                    inputString = [NSString stringWithFormat:@"%@(,)%@",inputString,item.name];
                    idString = [NSString stringWithFormat:@"%@(,)%@",idString,item.idStr];
                }
                [self sendMessageTypeInput:inputString contentId:idString fromText:content pushType:[NSString stringWithFormat:@"%d",-1]];
            } else {
                NSString *inputString = model.replyHead;
                NSString *idString = @"-1";
                NSString *pushString = @"";
                for (QJMineServiceReplyContentModel *item in model.replyContents) {
                    inputString = [NSString stringWithFormat:@"%@(,)%@",inputString,item.name];
                    
                    idString = [NSString stringWithFormat:@"%@(,)%@",idString,[QJAppTool dictionaryToJson:item.appPage]];
                    pushString = [self pushTypeDic][item.appPage[@"code"]];
                }
                
                [self sendMessageTypeInput:inputString contentId:idString fromText:content pushType:pushString];
            }
            
        } else {
            if (block) {
                block(NO);
            }
            
        }

    }];
    
}

- (NSDictionary *)pushTypeDic {
    return @{@"/game/detail":@"0",
             @"/game/home":@"0",
             @"/camp/detail":@"1",
             @"/camp/home":@"1",
             @"/information/detail":@"2",
             @"/information/home":@"2",
             @"/help-center/question/detail":@"3",
             @"/help-center/question/search":@"3",
             @"/knowledge-base/detail":@"4",
             @"/feedback":@"5",
    };
}

// type = 5,初始化页面,
- (void)sendMessageTypeInit:(NSString *)msg {
    QJMessage *autoMessage = [[QJMessageDataManager shareManager] addPublicParameters:msg bubbleMessageType:QJBubbleMessageType_Receiving bodyMessageType:QJMessageBodyType_text];
    [[QJMessageDataManager shareManager] insertMessage:autoMessage];
    [self addChatMessageWithMessage:autoMessage isBottom:YES];
    NSNumber *time = LSUserDefaultsGET(@"ConversationTime");
    [QJMessageDataManager shareManager].leadTime = time.integerValue;
    self.historyId = -2;
    
    [self startTimer];
}

// type = 6,用户输入未点击选项，msg:客服返回的消息，fromText：原消息, contentId：消息内容id，pushType：跳转类型
- (void)sendMessageTypeInput:(NSString *)msg contentId:(NSString *)contentId fromText:(NSString *)fromText pushType:(NSString *)pushType {
    //更新倒计时时间
    NSNumber *time = LSUserDefaultsGET(@"ConversationTime");
    [QJMessageDataManager shareManager].leadTime = time.integerValue;
    
    //发送成功后，再次添加客服消息
    QJMessage *resultMessage = [[QJMessageDataManager shareManager] addPublicParameters:msg bubbleMessageType:QJBubbleMessageType_Receiving bodyMessageType:QJMessageBodyType_custom];
    resultMessage.fromText = fromText;
    resultMessage.contentIdText = contentId;
    resultMessage.pushType = pushType;
    [[QJMessageDataManager shareManager] insertMessage:resultMessage];
    [self addChatMessageWithMessage:resultMessage isBottom:YES];
}

// 初始化数据
- (void)initMessageData {
    NSArray *messageArray = [[QJMessageDataManager shareManager] selectMessage:0 idStr:self.historyId];
    if (messageArray.count > 0) {
        // 第一次进入判断距离上一条消息是否超过三分钟，如果超过清空本地数据，否则开启倒计时
        QJMessage *message = messageArray.lastObject;
        self.historyId = message.idStr;
        NSNumber *time = LSUserDefaultsGET(@"ConversationTime");
        [QJMessageDataManager shareManager].leadTime = time.integerValue - [QJMessageManager getLeadTime:message.sendTime];
        if ([QJMessageDataManager shareManager].leadTime < 0) {
            [[QJMessageDataManager shareManager] deleteAll];
            messageArray = nil;
            //如果是第一次进入，发送消息
            [self sendRequestQueryType:5 content:@"" block:nil];

        } else {
            [self loadData];
            [self startTimer];
        }
        
    } else {
        //如果是第一次进入，发送消息
        [self sendRequestQueryType:5 content:@"" block:nil];
    }
    
}

- (void)dealloc {
    if (_timer) {
        dispatch_source_cancel(_timer);
    }
}

- (void)startTimer {
    __weak typeof (self) weakSelf=self;
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
    dispatch_source_set_timer(_timer, DISPATCH_TIME_NOW, 1.0 * NSEC_PER_SEC, 0 * NSEC_PER_SEC);
    dispatch_source_set_event_handler(_timer, ^{
        if ([QJMessageDataManager shareManager].leadTime < 1) {
            dispatch_source_cancel(weakSelf.timer);
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if ([QJMessageDataManager shareManager].leadTime < 1) {
                    [[QJMessageDataManager shareManager] deleteAll];
                    [weakSelf.navigationController popViewControllerAnimated:YES];
                }
                
            });
        }else{
            [QJMessageDataManager shareManager].leadTime --;
//            NSLog(@"%ld",[QJMessageDataManager shareManager].leadTime);
        }
    });
    dispatch_resume(_timer);
}

#pragma mark - 加载历史数据
- (void)loadData {
    NSMutableArray *temp = [NSMutableArray array];

    NSArray *messageArray = [[QJMessageDataManager shareManager] selectMessage:_page idStr:self.historyId];
    for (QJMessage *model in messageArray) {
        //处理数据
        QJMessageFrame *messageF = [QJMessageManager dealDataWithMessage:model dateSoure:temp time:self.timeArr.count > 0 ? self.timeArr.firstObject : @""];

        if (messageF) { //做添加

            if (messageF.showTime) {
                [self.timeArr insertObject:model.sendTime atIndex:0];
            }

            [temp addObject:messageF];
        }
    }

    if (temp.count) {
        //插入数据
        NSMutableIndexSet *indexes = [NSMutableIndexSet indexSetWithIndexesInRange:NSMakeRange(0, temp.count)];
        [self.dataArray insertObjects:temp atIndexes:indexes];

        [self.tableView reloadData];
        
        //滚动到刷新位置，加view动画防止页面跳动
        [UIView animateWithDuration:0 delay:1.0 options:UIViewAnimationOptionAllowUserInteraction animations:^{
            
        }completion:^(BOOL finished) {
            [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:temp.count-1 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
        }];
        
    }

    [self.refresh endRefreshing];
    self.page++;
}

- (void)createView {
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.inputView];
}

#pragma mark - QJMessageInputViewDelegate
#pragma mark 发送文本
- (void)QJMessageInputViewSend:(NSString *)sendText {
    [self sendText:sendText queryContent:sendText queryType:6];
}

- (void)sendText:(NSString *)sendText queryContent:(NSString *)queryContent queryType:(NSInteger)queryType {
    QJMessage *message = [[QJMessageDataManager shareManager] addPublicParameters:sendText bubbleMessageType:QJBubbleMessageType_Send bodyMessageType:QJMessageBodyType_text];
    if ([[QJMessageDataManager shareManager] insertMessage:message]) {
        // 普通文字发送
        [self sendRequestQueryType:queryType content:queryContent block:^(BOOL sendSuccess) {
            if (sendSuccess) {
                message.messageState = QJSendMessageType_Successed;
            } else {
                message.messageState = QJSendMessageType_Failed;
            }
            
            //更新本地数据库
            if ([[QJMessageDataManager shareManager] updateMessage:message]) {
                //添加消息到聊天界面
                [self addChatMessageWithMessage:message isBottom:NO];
            }
        }];
    }
    
}

#pragma mark - 数据处理
#pragma mark 添加到下方聊天界面
- (void)addChatMessageWithMessage:(QJMessage *)message isBottom:(BOOL)isBottom {
    //处理数据
    QJMessageFrame *messageF = [QJMessageManager dealDataWithMessage:message dateSoure:self.dataArray time:self.timeArr.lastObject];

    if (messageF) {
        if (messageF.showTime) {
            [self.timeArr addObject:message.sendTime];
        }
        [self.dataArray addObject:messageF];
    }
    //刷新界面
    [self.tableView reloadData];

    if (isBottom) {
        //滚动到底部
        [self tableViewScrollToBottom];
    }

}

#pragma mark - QJMessageTableViewCellDelegate
// 重发
- (void)QJMessageTableViewCellReSendMessage:(QJMessage *)message {
    [self.dataArray enumerateObjectsUsingBlock:^(QJMessageFrame *obj, NSUInteger idx, BOOL *_Nonnull stop) {
      if ([obj.message.messageId isEqualToString:message.messageId]) {
          *stop = YES;

          //删除之前数据
          [self.dataArray removeObject:obj];

          //处理时间操作
          [self dealTimeMassageDataWithCurrent:obj idx:idx];

          //重新发送，删除原有消息，重新发送一条新的消息
          if ([[QJMessageDataManager shareManager] deleteMessage:obj.message]) {
              [self sendText:obj.message.text queryContent:obj.message.text queryType:6];
          }
          
      }
        
    }];
}

// 点击系统回复消息内容
- (void)QJMessageTableViewCellReplyMessage:(QJMessage *)message text:(NSString *)text {
    
    //对点击消息做处理
    NSArray *idArray = [message.contentIdText componentsSeparatedByString:@"(,)"];
    NSArray *textArray = [message.text componentsSeparatedByString:@"(,)"];
    NSInteger row = [textArray indexOfObject:text];
    
    if ([message.pushType isEqualToString:@"-1"]) {
        NSString *queryId = idArray[row];
        QJMessage *autoMessage = [[QJMessageDataManager shareManager] addPublicParameters:@"小客服正在查询中，请稍等一会哦~" bubbleMessageType:QJBubbleMessageType_Receiving bodyMessageType:QJMessageBodyType_text];
        [[QJMessageDataManager shareManager] insertMessage:autoMessage];
        [self addChatMessageWithMessage:autoMessage isBottom:YES];

        [self sendRequestQueryType:[queryId integerValue] content:message.fromText block:^(BOOL sendSuccess) {
            if (!sendSuccess) {
                QJMessage *failMessage = [[QJMessageDataManager shareManager] addPublicParameters:@"暂无法查询，请稍后再试" bubbleMessageType:QJBubbleMessageType_Receiving bodyMessageType:QJMessageBodyType_text];
                [[QJMessageDataManager shareManager] insertMessage:failMessage];
                [self addChatMessageWithMessage:failMessage isBottom:YES];
            }

        }];
    } else {
        NSDictionary *appPage = [QJAppTool jsonToDictionary:idArray[row]];
        [[QJMediator sharedInstance] pushQJMediatorViewController:appPage];
        NSString *name = appPage[@"code"];
        if ([[self QJModuleHome] containsObject:name]) {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
    
}

- (NSArray *)QJModuleHome {
    return @[
             @"/information/home",//资讯模块
             @"/camp/detail",//指定营地
             @"/camp/home",//营地模块
             @"/game/home",//游戏模块
    ];
}

#pragma mark 处理时间操作
- (void)dealTimeMassageDataWithCurrent:(QJMessageFrame *)current idx:(NSInteger)idx {
    //操作的此条是显示时间的
    if (current.showTime) {
        if (self.dataArray.count > idx) { //操作的是中间的

            QJMessageFrame *frame = self.dataArray[idx];

            [self.timeArr enumerateObjectsUsingBlock:^(NSString *_Nonnull obj, NSUInteger idx, BOOL *_Nonnull stop) {
              if ([obj isEqualToString:current.message.sendTime]) {
                  *stop = YES;
                  [self.timeArr replaceObjectAtIndex:idx withObject:frame.message.sendTime];
              }
            }];

            frame.showTime = YES;
            //重新计算高度
            [frame setMessage:frame.message];
            [self.dataArray replaceObjectAtIndex:idx withObject:frame];

        } else { //操作的是最后一条
            [self.timeArr removeObject:current.message.sendTime];
        }
    }
}

#pragma mark 滚动最下方
- (void)tableViewScrollToBottom {
    //界面滚动到指定位置
    [self tableViewScrollToIndex:self.dataArray.count - 1];
}

#pragma mark 滚动到指定位置
- (void)tableViewScrollToIndex:(NSInteger)index {
    @synchronized(self.dataArray) {
        if (self.dataArray.count > index) {
            [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:NO];
        }
    }
}

//键盘变化
- (void)QJMessageInputViewKeyboardStatus:(BOOL)isShow keyboardHeight:(CGFloat)keyboardHeight animationTime:(CGFloat)animationTime {
    if (isShow) {
        self.tableView.height = kScreenHeight - NavigationBar_Bottom_Y - keyboardHeight - 55;
    } else {
        self.tableView.height = kScreenHeight - NavigationBar_Bottom_Y - 55 - Bottom_iPhoneX_SPACE;
    }
    [self tableViewScrollToBottom];
}

#pragma mark - UITableViewdataArray
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    QJMessageFrame *model = self.dataArray[indexPath.row];

    if (model.message.messageType == QJMessageBodyType_custom) {
        QJMessageCustomTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QJMessageCustomTableViewCell"];
        cell.delegate = self;
        cell.messageFrame = model;
        return cell;
    } else {
        QJMessageTextTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QJMessageTextTableViewCell"];
        cell.delegate = self;
        cell.messageFrame = model;
        return cell;
    }
    
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    QJMessageFrame *messageFrame = self.dataArray[indexPath.row];
    return messageFrame.cell_h;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

- (UITableView *)tableView {
    if (!_tableView) {
        self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, NavigationBar_Bottom_Y, kScreenWidth, kScreenHeight-NavigationBar_Bottom_Y-48-Bottom_iPhoneX_SPACE) style:UITableViewStylePlain];
        self.tableView.backgroundColor = UIColorHex(f8f8f8);
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.tableView.showsVerticalScrollIndicator = NO;
        self.tableView.showsHorizontalScrollIndicator = NO;
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        self.tableView.refreshControl = self.refresh;

        if (@available(iOS 11.0, *)) {
            self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }

        [self.tableView registerClass:[QJMessageTextTableViewCell class] forCellReuseIdentifier:@"QJMessageTextTableViewCell"];
        [self.tableView registerClass:[QJMessageCustomTableViewCell class] forCellReuseIdentifier:@"QJMessageCustomTableViewCell"];
    }
    return _tableView;
}

- (QJMessageInputView *)inputView {
    if (!_inputView) {
        _inputView = [[QJMessageInputView alloc] init];
        _inputView.delegate = self;
    }
    return _inputView;
}

- (NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}

#pragma mark 时间集合
- (NSMutableArray *)timeArr {
    if (!_timeArr) {
        _timeArr = [[NSMutableArray alloc] init];
    }
    return _timeArr;
}

- (NSMutableArray *)historyArray {
    if (!_historyArray) {
        _historyArray = [NSMutableArray array];
    }
    return _historyArray;
}

#pragma mark 加载
- (UIRefreshControl *)refresh {
    if (!_refresh) {
        _refresh = [[UIRefreshControl alloc] init];
        [_refresh addTarget:self action:@selector(loadData) forControlEvents:UIControlEventValueChanged];
    }
    return _refresh;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
