//
//  QJMessageFrame.m
//  QJBox
//
//  Created by macm on 2022/8/12.
//

#import "QJMessageFrame.h"
#import "QJMessageManager.h"

@interface QJMessageFrame ()
@property (nonatomic, strong) YYLabel *textView;

@end

@implementation QJMessageFrame

#pragma mark - 修改一些数据源与界面frame的计算
- (void)setMessage:(QJMessage *)message{
    _message = message;
    //数据源的一些修改（计算时间、头像、id、聊天气泡大小）
    
    // 判断收发
    BOOL isSend = (message.bubbleMessageType == QJBubbleMessageType_Send);
    // 消息居中
    BOOL isCenter = NO;
    
    BOOL isCustom = (message.messageType == QJMessageBodyType_custom);
    
    // 计算整体聊天气泡的Size
    CGSize contentSize = CGSizeZero;
    
    if (isCustom) {
        NSArray *textArray = [message.text componentsSeparatedByString:@"(,)"];
        int i = 0;
        for (NSString *str in textArray) {
            CGSize strSize = CGSizeZero;
            //TODO: 有表情计算高度偏高
            NSMutableAttributedString *att = [[NSMutableAttributedString alloc]initWithString:str attributes:@{NSFontAttributeName:kChatFont_content}];
            NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
            paragraphStyle.lineSpacing = 5.0; // 设置行间距
            [att addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, att.length)];
            
            if (i == 0) {
                strSize = [QJMessageManager getSizeWithAtt:att maxSize:CGSizeMake(kChat_content_maxW, CGFLOAT_MAX)];
            } else {
                strSize = [QJMessageManager getSizeWithStr:@"默认消息" font:kChatFont_content maxSize:CGSizeMake(kChat_content_maxW, CGFLOAT_MAX)];
            }

            strSize.width += 2*kChat_margin;
            strSize.height += kChat_margin+kChat_min_margin;

            [self.sizeArray addObject:@(strSize.height)];
            [self.textAttArray addObject:att];
            contentSize.height += strSize.height;
            i++;
        }
        if (contentSize.height < kChat_min_h) {//为了使聊天内容与最小高度对齐
            contentSize.height = kChat_min_h;
        }
        contentSize.height += kChat_margin+kChat_min_margin;
        contentSize.width = kChat_content_maxW+2*kChat_margin;

    } else {
        //TODO: 有表情计算高度偏高
        NSMutableAttributedString *att = [[NSMutableAttributedString alloc]initWithString:message.text attributes:@{NSFontAttributeName:kChatFont_content}];
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        paragraphStyle.lineSpacing = 5.0; // 设置行间距
        [att addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, att.length)];
        
        _att = att;
        contentSize = [QJMessageManager getSizeWithAtt:att maxSize:CGSizeMake(kChat_content_maxW, CGFLOAT_MAX)];
        
        contentSize.height += 2*kChat_margin+kChat_min_margin;
        
        if (contentSize.height < kChat_min_h) {//为了使聊天内容与最小高度对齐
            contentSize.height = kChat_min_h;
        }
        contentSize.width += 2*kChat_margin;
    }
    
    // 计算Y轴
    CGFloat viewY = kChat_margin;
    
    // 计算时间
    _timeF = CGRectZero;
    if (_showTime){
        
        CGSize timeSize = [QJMessageManager getSizeWithStr:[QJMessageManager getChatTimeWithTime:message.sendTime] font:kChatFont_time maxSize:CGSizeMake(CGFLOAT_MAX, kChat_content_maxW)];
        
        CGFloat timeX = (kScreenWidth - timeSize.width - 2*kChat_margin_time) / 2;
        _timeF = CGRectMake(timeX, viewY, timeSize.width + 2*kChat_margin_time, timeSize.height + 2*kChat_margin_time);
        
        viewY = CGRectGetMaxY(_timeF) + kChat_margin;
    }
    
    // 计算头像
    CGFloat iconX = kChat_icon_margin;
    CGFloat iconY = viewY;
    if (isSend) {//发送方
        iconX = kScreenWidth - iconX - kChat_icon;
    }

    _iconF = CGRectMake(iconX, iconY, kChat_icon, kChat_icon);
    
    // 计算X轴
    CGFloat viewX = 0;
    
    // 气泡居中
    if (isCenter) {
        viewX = (kScreenWidth - contentSize.width)/2;
    }else{//其他消息 根据发送方X轴不一样
        
        viewX = CGRectGetMaxX(_iconF) + kChat_margin;
        if (isSend) {
            viewX = kScreenWidth - kChat_margin - contentSize.width;
            viewX -= kChat_icon + kChat_icon_margin;
        }
        
        // 起始位置
        _startX = 0;
        
    }
    
    //聊天气泡frame
    _contentF = CGRectMake(viewX, viewY, contentSize.width, contentSize.height);
    //cell高度
    _cell_h = CGRectGetMaxY(_contentF)  + kChat_margin;
}

- (NSMutableArray *)sizeArray {
    if (!_sizeArray) {
        _sizeArray = [NSMutableArray array];
    }
    return _sizeArray;
}

- (NSMutableArray *)textAttArray {
    if (!_textAttArray) {
        _textAttArray = [NSMutableArray array];
    }
    return _textAttArray;
}

#pragma mark - 获取Size
- (CGSize)getSizeWithMaxSize:(CGSize)maxSize size:(CGSize)size min:(CGFloat)min{
    
    //规定的宽高都小于最大的 则使用规定的
    if (MIN(size.width, size.height)) {
        
        if (size.width > size.height) {
            //宽大 按照宽给高
            CGFloat width = MIN(maxSize.width, size.width);
            size = CGSizeMake(width, width*size.height/size.width);
            if (size.height < min) {
                size.height = min;
            }
        }else{
            //高大 按照高给宽
            CGFloat height = MIN(maxSize.height, size.height);
            size =  CGSizeMake(height*size.width/size.height, height);
            if (size.width < min) {
                size.width = min;
            }
        }
    }else{
        size = maxSize;
    }

    return size;
}

@end
