//
//  QJMessageFrame.h
//  QJBox
//
//  Created by macm on 2022/8/12.
//

#import <Foundation/Foundation.h>
#import "QJMessage.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJMessageFrame : NSObject

//聊天模型
@property (nonatomic, strong) QJMessage *message;
//是否显示时间
@property (nonatomic, assign) BOOL showTime;

//内部计算
//时间CGRect
@property (nonatomic, assign, readonly) CGRect timeF;
//头像CGRect
@property (nonatomic, assign, readonly) CGRect iconF;
//内容CGRect
@property (nonatomic, assign, readonly) CGRect contentF;
//整体cell高度
@property (nonatomic, assign, readonly) CGFloat cell_h;

//X初始位置
@property (nonatomic, assign, readonly) CGFloat startX;
//富文本
@property (nonatomic, copy, readonly) NSAttributedString *att;
//消息内容
@property (nonatomic, copy, readonly) NSString *contentText;

// 回复列表问题高度
@property (nonatomic, strong) NSMutableArray *sizeArray;
// 回复文字富文本数组
@property (nonatomic, strong) NSMutableArray *textAttArray;

@end

NS_ASSUME_NONNULL_END
