//
//  QJMineServiceModel.h
//  QJBox
//
//  Created by macm on 2022/8/25.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJMineServiceModel : NSObject
@property (nonatomic, assign) NSInteger endTime; //    会话关闭时间间隔    integer(int32)
@property (nonatomic, copy) NSString *headPortraitUrl; //    机器人头像    string
@property (nonatomic, copy) NSString *name; //    机器人名称    string
@property (nonatomic, copy) NSString *replyBottom; //    回复内容底部    string
@property (nonatomic, strong)  NSArray *replyContents; //    查询结果    array    ai客服中心返回内容列表项
@property (nonatomic, copy) NSString *replyHead; //    回复内容头部    string
@property (nonatomic, assign) NSInteger type; //    模块类型 value = 0 游戏模块 ，1 营地模块，2资讯资讯模块 3 帮助中心 4 知识库 5 问题反馈    integer(int32)
@end

@interface QJMineServiceAppPageModel : NSObject
@property (nonatomic, copy) NSString *code;
@property (nonatomic, strong) NSDictionary *params;

@end

@interface QJMineServiceReplyContentModel : NSObject
@property (nonatomic, strong) NSDictionary *appPage;
@property (nonatomic, copy) NSString *name; //    标题名称    string
@property (nonatomic, copy) NSString *idStr; //    标题名称    string

@end


NS_ASSUME_NONNULL_END
