//
//  QJMessage.h
//  QJBox
//
//  Created by macm on 2022/8/12.
//

#import <Foundation/Foundation.h>
#import "QJMessageManager.h"

NS_ASSUME_NONNULL_BEGIN
/**
 聊天界面模型
 */
@interface QJMessage : NSObject

//消息公共内容
//消息数据库ID
@property (nonatomic, assign) NSInteger idStr;
//消息ID
@property (nonatomic, copy) NSString *messageId;

//消息的发送时间
@property (nonatomic, copy) NSString *sendTime;

//消息状态
@property (nonatomic, assign) QJSendMessageStatus messageState;
//消息发送与接收
@property (nonatomic, assign) QJBubbleMessageType bubbleMessageType;
//消息类型
@property (nonatomic, assign) QJMessageBodyType messageType;

//文本
@property (nonatomic, copy) NSString *text;

//拓展(json)
//@property (nonatomic, copy) NSString *ext;

// 来源文字
@property (nonatomic, copy) NSString *fromText;

// 内容条目id
@property (nonatomic, copy) NSString *contentIdText;

// 有跳转需求的时候的跳转类型
@property (nonatomic, copy) NSString *pushType;

@end

NS_ASSUME_NONNULL_END
