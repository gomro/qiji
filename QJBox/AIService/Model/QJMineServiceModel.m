//
//  QJMineServiceModel.m
//  QJBox
//
//  Created by macm on 2022/8/25.
//

#import "QJMineServiceModel.h"

@implementation QJMineServiceModel
+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"replyContents" : [QJMineServiceReplyContentModel class]};
}
@end

@implementation QJMineServiceReplyContentModel
+ (NSDictionary *)modelCustomPropertyMapper{
    return @{@"idStr":@"id"};
}
@end

@implementation QJMineServiceAppPageModel

@end

