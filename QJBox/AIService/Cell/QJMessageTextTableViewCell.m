//
//  QJMessageTextTableViewCell.m
//  QJBox
//
//  Created by macm on 2022/8/12.
//

#import "QJMessageTextTableViewCell.h"
//#import "QJMessageTextView.h"

@interface QJMessageTextTableViewCell ()
@property (nonatomic, strong) UILabel *textView;

@end

@implementation QJMessageTextTableViewCell

- (void)setMessageFrame:(QJMessageFrame *)messageFrame{
    [super setMessageFrame:messageFrame];
    
//    SHMessage *message = messageFrame.message;

    self.textView.attributedText = messageFrame.att;
    
    //设置frame
    CGFloat view_y = kChat_margin;
    if (self.bgView.height == kChat_min_h) {//为了使聊天内容与最小高度对齐
        view_y = (kChat_min_h - kChatFont_content.lineHeight)/2;
    }
    
    CGFloat margin = messageFrame.startX;
    self.textView.frame = CGRectMake(margin + kChat_margin, view_y, self.bgView.width - 2*kChat_margin, self.bgView.height - 2*view_y);
}

#pragma mark 文本消息视图
- (UILabel *)textView{
    //文本
    if (!_textView) {
        _textView = [[UILabel alloc]init];
//        _textView.editable = NO;
//        _textView.scrollEnabled = NO;
//        _textView.showsVerticalScrollIndicator = NO;
        _textView.numberOfLines = 0;
        [_textView setFont:kChatFont_content];
        [self.bgView addSubview:_textView];
    }
    return _textView;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
