//
//  QJMessageCustomTextTableViewCell.h
//  QJBox
//
//  Created by macm on 2022/8/29.
//

#import <UIKit/UIKit.h>
//#import "YYLabel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJMessageCustomTextTableViewCell : UITableViewCell
@property (nonatomic, strong) UILabel *textView;
@property (nonatomic, strong) UIButton *rightTextView;
@property (nonatomic, strong) UIButton *leftTextView;

@end

NS_ASSUME_NONNULL_END
