//
//  QJMessageTableViewCell.m
//  QJBox
//
//  Created by macm on 2022/8/12.
//

#import "QJMessageTableViewCell.h"
#import <Foundation/Foundation.h>
#import "QJActivityIndicatorView.h"

@implementation QJMessageTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

#pragma mark 创建时间
- (UILabel *)timeLabel{
    if (!_timeLabel) {
        _timeLabel = [[UILabel alloc] init];
        _timeLabel.textAlignment = NSTextAlignmentCenter;
        _timeLabel.textColor = kColorGray;
        _timeLabel.backgroundColor = [UIColor clearColor];
        _timeLabel.layer.masksToBounds = YES;
        _timeLabel.layer.cornerRadius = 5;
        _timeLabel.font = kChatFont_time;
        [self.contentView addSubview:_timeLabel];
    }
    return _timeLabel;
}

#pragma mark 创建头像
- (UIImageView *)headImageView{
    if (!_headImageView) {
        _headImageView = [[UIImageView alloc] init];
        _headImageView.layer.cornerRadius = 17;
        _headImageView.layer.masksToBounds = YES;
        [self.contentView addSubview:_headImageView];
    }
    return _headImageView;
}

#pragma mark 创建内容
- (UIView *)bgView{
    if (!_bgView) {
        _bgView = [[UIView alloc] init];
        _bgView.backgroundColor = [UIColor whiteColor];
        _bgView.layer.cornerRadius = 4;
        [self.contentView addSubview:_bgView];
    }
    return _bgView;
}

#pragma mark 消息发送状态视图
- (QJActivityIndicatorView *)activityView{
    if (!_activityView) {
        _activityView = [[QJActivityIndicatorView alloc]init];
        [_activityView addTarget:self action:@selector(repeatClick) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_activityView];
    }
    return _activityView;
}

#pragma mark - 内容及Frame设置
- (void)setMessageFrame:(QJMessageFrame *)messageFrame {
    
    _messageFrame = messageFrame;
    QJMessage *message = messageFrame.message;
    
    // 初始化 (如果不显示时间、头像，在frame中就没有计算)
    //发送状态
    self.activityView.hidden = YES;
    
    BOOL isSend = (message.bubbleMessageType == QJBubbleMessageType_Send);
    
    // 设置时间
    self.timeLabel.frame = messageFrame.timeF;
    self.timeLabel.text = [QJMessageManager getChatTimeWithTime:message.sendTime];

    // 设置头像
    self.headImageView.frame = messageFrame.iconF;
    if (isSend) {
        [self.headImageView setImageWithURL:[NSURL URLWithString:[LSUserDefaultsGET(kQJUserHeadImage) checkImageUrlString]] placeholder:[UIImage imageNamed:@"mine_head"]];
    } else {
        [self.headImageView setImageWithURL:[NSURL URLWithString:[LSUserDefaultsGET(@"HeadPortraitUrl") checkImageUrlString]] placeholder:[UIImage imageNamed:@"mine_head"]];
    }
    
    // 设置内容
    self.bgView.frame = messageFrame.contentF;
    
    // 设置发送状态样式
    self.activityView.messageState = message.messageState;
    
    // 发送状态
    if (isSend && message.messageState != QJSendMessageType_Successed) {
        self.activityView.frame = CGRectMake(self.bgView.hx_x - (5 + 20), self.bgView.hx_y + (self.bgView.height - 20)/2, 20, 20);
    }
    
}

#pragma mark - 点击事件

#pragma mark 点击重发
- (void)repeatClick{
    if (self.messageFrame.message.messageState == QJSendMessageType_Failed) {
        if ([self.delegate respondsToSelector:@selector(QJMessageTableViewCellReSendMessage:)])  {
            [self.delegate QJMessageTableViewCellReSendMessage:self.messageFrame.message];
        }
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
