//
//  QJMessageTableViewCell.h
//  QJBox
//
//  Created by macm on 2022/8/12.
//

#import <UIKit/UIKit.h>
#import "QJMessageFrame.h"

NS_ASSUME_NONNULL_BEGIN

@class QJActivityIndicatorView;

@protocol QJMessageTableViewCellDelegate <NSObject>

// 点击重新发送消息
@optional
- (void)QJMessageTableViewCellReSendMessage:(QJMessage *)message;

// 点击回复内容选项
@optional
- (void)QJMessageTableViewCellReplyMessage:(QJMessage *)message text:(NSString *)text;

@end

@interface QJMessageTableViewCell : UITableViewCell

@property (nonatomic, strong) QJMessageFrame *messageFrame;
//时间
@property (nonatomic, strong) UILabel *timeLabel;
//头像
@property (nonatomic, strong) UIImageView *headImageView;
//消息状态
@property (nonatomic, strong) QJActivityIndicatorView *activityView;
//背景
@property (nonatomic, strong) UIView *bgView;

@property (nonatomic, weak) id<QJMessageTableViewCellDelegate>delegate;

@end

NS_ASSUME_NONNULL_END
