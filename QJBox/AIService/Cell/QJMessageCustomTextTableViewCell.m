//
//  QJMessageCustomTextTableViewCell.m
//  QJBox
//
//  Created by macm on 2022/8/29.
//

#import "QJMessageCustomTextTableViewCell.h"
#import "QJMessageManager.h"

@interface QJMessageCustomTextTableViewCell ()

@end

@implementation QJMessageCustomTextTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;

        [self setupUI];
    }
    return self;
}

- (void)setupUI {
    [self.contentView addSubview:self.textView];
    [self.textView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kChat_margin);
        make.right.mas_equalTo(-kChat_margin);
        make.top.bottom.mas_equalTo(0);
    }];
    
    [self.contentView addSubview:self.rightTextView];
    [self.rightTextView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-kChat_margin);
        make.top.bottom.mas_equalTo(0);
    }];
    
    [self.contentView addSubview:self.leftTextView];
    [self.leftTextView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kChat_margin);
        make.top.bottom.mas_equalTo(0);
    }];
    
}

#pragma mark 文本消息视图
- (UILabel *)textView{
    //文本
    if (!_textView) {
        _textView = [[UILabel alloc]init];
        _textView.numberOfLines = 0;
//        _textView.editable = NO;
//        _textView.scrollEnabled = NO;
//        _textView.showsVerticalScrollIndicator = NO;
        [_textView setFont:kChatFont_content];
    }
    return _textView;
}

- (UIButton *)leftTextView{
    //文本
    if (!_leftTextView) {
        _leftTextView = [[UIButton alloc]init];
        [_leftTextView setTitleColor:UIColorFromRGB(0x007aff) forState:UIControlStateNormal];
        _leftTextView.backgroundColor = [UIColor whiteColor];
        [_leftTextView.titleLabel setFont:kChatFont_content];
        [_leftTextView setSemanticContentAttribute:UISemanticContentAttributeForceRightToLeft];
        [_leftTextView setImage:[UIImage imageNamed:@"mine_AI_more_left"] forState:UIControlStateNormal];
        [_leftTextView setEnabled:NO];
    }
    return _leftTextView;
}

- (UIButton *)rightTextView{
    //文本
    if (!_rightTextView) {
        _rightTextView = [[UIButton alloc]init];
        [_rightTextView setTitleColor:kColorGray forState:UIControlStateNormal];
        _rightTextView.backgroundColor = [UIColor whiteColor];
        [_rightTextView.titleLabel setFont:kChatFont_content];
        [_rightTextView setSemanticContentAttribute:UISemanticContentAttributeForceRightToLeft];
        [_rightTextView setImage:[UIImage imageNamed:@"mine_AI_more_right"] forState:UIControlStateNormal];
        [_rightTextView setEnabled:NO];
    }
    return _rightTextView;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
