//
//  QJMessageCustomTableViewCell.m
//  QJBox
//
//  Created by macm on 2022/8/18.
//

#import "QJMessageCustomTableViewCell.h"
#import "QJMessageCustomTextTableViewCell.h"

@interface QJMessageCustomTableViewCell ()<UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;

@end

@implementation QJMessageCustomTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setupUI];
    }
    return self;
}

- (void)setupUI {
    [self.bgView addSubview:self.tableView];
}

- (void)setMessageFrame:(QJMessageFrame *)messageFrame{
    [super setMessageFrame:messageFrame];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(kChat_margin);
        make.left.bottom.right.mas_equalTo(0);
    }];
    [self.tableView reloadData];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.messageFrame.textAttArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [self.messageFrame.sizeArray[indexPath.row] floatValue];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        return;
    }
    
    if ([self.delegate respondsToSelector:@selector(QJMessageTableViewCellReplyMessage:text:)])  {
        [self.delegate QJMessageTableViewCellReplyMessage:self.messageFrame.message text:[self.messageFrame.textAttArray[indexPath.row] string]];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    QJMessageCustomTextTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    cell.textView.attributedText = self.messageFrame.textAttArray[indexPath.row];

    if (indexPath.row == 0) {
        cell.textView.textColor = UIColorFromRGB(0x000000);
        cell.textView.numberOfLines = 0;
    } else {
        cell.textView.textColor = UIColorFromRGB(0x007aff);
        cell.textView.numberOfLines = 1;
    }
    
    [cell.leftTextView setHidden:YES];
    [cell.rightTextView setHidden:YES];
    [cell.textView setHidden:NO];
    
    if (indexPath.row == self.messageFrame.textAttArray.count - 1 && self.messageFrame.message.pushType.integerValue < 4 && self.messageFrame.message.pushType.integerValue != -1) {
        
        [cell.textView setHidden:YES];
        
        if (self.messageFrame.message.pushType.integerValue < 3) {
            [cell.leftTextView setHidden:NO];
            [cell.leftTextView setTitle:[self.messageFrame.textAttArray[indexPath.row] string] forState:UIControlStateNormal];
        } else {
            if (indexPath.row > 3) {
                [cell.rightTextView setHidden:NO];
                [cell.rightTextView setTitle:[self.messageFrame.textAttArray[indexPath.row] string] forState:UIControlStateNormal];
            } else {
                [cell.textView setHidden:NO];
            }
            
        }
        
    }
    
    
    
    return cell;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.dataSource = self;
        _tableView.delegate = self;
        [_tableView registerClass:[QJMessageCustomTextTableViewCell class] forCellReuseIdentifier:@"cell"];
        
    }
    return _tableView;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
