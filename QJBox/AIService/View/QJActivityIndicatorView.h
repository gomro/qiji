//
//  QJActivityIndicatorView.h
//  QJBox
//
//  Created by macm on 2022/8/12.
//

#import <UIKit/UIKit.h>
#import "QJMessageManager.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJActivityIndicatorView : UIButton
@property (nonatomic, assign) QJSendMessageStatus messageState;

@end

NS_ASSUME_NONNULL_END
