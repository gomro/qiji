//
//  QJMessageInputView.m
//  QJBox
//
//  Created by macm on 2022/8/15.
//

#import "QJMessageInputView.h"

// 文字输入默认高度
#define TEXTHEIGHT 40

@interface QJMessageInputView ()<UITextViewDelegate>
@property (nonatomic, assign) CGFloat keyBoardHeight;
@property (nonatomic, assign) CGFloat textHieght;
@property (nonatomic, assign) CGFloat textViewHeight;
@end

@implementation QJMessageInputView

- (instancetype)init {
    self = [super init];
    if (self) {
         
        self.frame = CGRectMake(0, kScreenHeight-55-Bottom_iPhoneX_SPACE, kScreenWidth, 1000);
        self.backgroundColor = [UIColor whiteColor];
        
        self.textHieght = TEXTHEIGHT;
        
        [self addNotification];
        
        [self createView];
        
    }
    return self;
}

// 监听通知
- (void)addNotification {
    // 添加对键盘的监控
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

//按下发送
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ([text isEqualToString:@"\n"]) {
        
        if ([[self.textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0) {
            [QJAppTool showWarningToast:@"不能发送空白消息"];
        } else {
            if (self.delegate && [self.delegate respondsToSelector:@selector(QJMessageInputViewSend:)]) {
                [self.delegate QJMessageInputViewSend:self.textView.text];
            }
            UITextRange *textRange = [self.textView textRangeFromPosition:self.textView.beginningOfDocument toPosition:self.textView.endOfDocument];
            [self.textView replaceRange:textRange withText:@""];
        }
        
        return NO;//这里返回NO，就代表return键值失效，即页面上按下return，不会出现换行，如果为yes，则输入页面会换行
    }
    return YES;
}

- (void)keyBoardWillShow:(NSNotification *) note {
    
    // 获取用户信息
    NSDictionary *userInfo = [NSDictionary dictionaryWithDictionary:note.userInfo];
    // 获取键盘高度
    CGRect keyBoardBounds  = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    self.keyBoardHeight = keyBoardBounds.size.height;
    // 获取键盘动画时间
    CGFloat animationTime  = [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];

    // 定义好动作
    void (^animation)(void) = ^void(void) {
        self.transform = CGAffineTransformMakeTranslation(0, - self.keyBoardHeight - self.textHieght + Bottom_iPhoneX_SPACE-5);
    };

    if (animationTime > 0) {
        [UIView animateWithDuration:animationTime animations:animation];
    } else {
        animation();
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(QJMessageInputViewKeyboardStatus:keyboardHeight:animationTime:)]) {
        [self.delegate QJMessageInputViewKeyboardStatus:YES keyboardHeight:self.keyBoardHeight animationTime:animationTime];
    }
}

- (void)keyBoardWillHide:(NSNotification *) note {
    
    // 获取用户信息
    NSDictionary *userInfo = [NSDictionary dictionaryWithDictionary:note.userInfo];
    
    // 获取键盘动画时间
    CGFloat animationTime  = [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];

    // 定义好动作
    void (^animation)(void) = ^void(void) {
        self.transform = CGAffineTransformMakeTranslation(0, - self.textHieght-5);
    };

    if (animationTime > 0) {
        [UIView animateWithDuration:animationTime animations:animation];
    } else {
        animation();
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(QJMessageInputViewKeyboardStatus:keyboardHeight:animationTime:)]) {
        [self.delegate QJMessageInputViewKeyboardStatus:NO keyboardHeight:0 animationTime:animationTime];
    }
}

- (void)createView {
    
    [self addSubview:self.textView];
    
    @weakify(self);
    self.textView.textHeightChangedBlock = ^(NSString *text, CGFloat textHeight) {
        @strongify(self);
        if (self.textViewHeight == 0) {
            self.textViewHeight = textHeight;
        }
        self.textHieght = textHeight - self.textViewHeight;
        [self changeTextViewHeight];
    
    };
}

- (void)changeTextViewHeight {
    self.textView.height = self.textHieght + self.textViewHeight;
    if (self.textView.text.length > 0) {
        self.transform = CGAffineTransformMakeTranslation(0, - self.keyBoardHeight - self.textHieght + Bottom_iPhoneX_SPACE-5);
    }
}

- (void)textViewDidChange:(UITextView *)textView{
    UITextRange *selectedRange = [textView markedTextRange];
    NSString * newText = [textView textInRange:selectedRange]; //获取高亮部分
    if(newText.length>0)
        return;
   
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 3;// 字体的行间距
    NSDictionary *attributes = @{NSFontAttributeName:kFont(14),NSParagraphStyleAttributeName:paragraphStyle};
    textView.attributedText = [[NSAttributedString alloc] initWithString:textView.text attributes:attributes];
}

- (QJMineMsgTextView *)textView {
    if (!_textView) {
        _textView = [[QJMineMsgTextView alloc] initWithFrame:CGRectMake(16, 8, kScreenWidth - 32, 40)];
        _textView.backgroundColor = UIColorFromRGB(0xf5f5f5);
        _textView.delegate = self;
        _textView.font = kFont(14);
        _textView.layer.cornerRadius = 20;
        _textView.placeholder = @"发送消息";
        _textView.maxNumberOfLines = 5;
        _textView.returnKeyType = UIReturnKeySend;
        _textView.textContainerInset = UIEdgeInsetsMake(13, 17, 13, 17);
        _textView.placeholderView.textContainerInset = UIEdgeInsetsMake(13, 17, 13, 17);
        
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        paragraphStyle.lineSpacing = 15;// 字体的行间距
        NSDictionary *attributes = @{NSFontAttributeName:kFont(17),
                                         NSParagraphStyleAttributeName:paragraphStyle
                                         };
        _textView.attributedText = [[NSAttributedString alloc] initWithString:_textView.text attributes:attributes];


    }
    return _textView;
}

@end
