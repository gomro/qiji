//
//  QJMessageTextView.m
//  QJBox
//
//  Created by macm on 2022/8/12.
//

#import "QJMessageTextView.h"

@implementation QJMessageTextView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        //配置
        [self setup];
    }
    return self;
}

#pragma mark - 配置
- (void)setup{
    self.backgroundColor = [UIColor clearColor];
    self.textContainer.lineFragmentPadding = 0;
    self.textContainerInset = UIEdgeInsetsZero;
    self.dataDetectorTypes = UIDataDetectorTypePhoneNumber | UIDataDetectorTypeLink;
    self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
}
//
//#pragma mark 设置光标
//- (CGRect)caretRectForPosition:(UITextPosition *)position {
//    CGRect rect = [super caretRectForPosition:position];
//    CGFloat y = CGRectGetMidY(rect);
//    rect.size.height = self.font.lineHeight - 3;
//    rect.origin.y = y - 8;
//    return rect;
//}
//
- (void)setTextContainerInset:(UIEdgeInsets)textContainerInset{
    CGFloat padding = self.textContainer.lineFragmentPadding;
    [super setTextContainerInset:UIEdgeInsetsMake(textContainerInset.top, textContainerInset.left - padding, textContainerInset.bottom, textContainerInset.right - padding)];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
