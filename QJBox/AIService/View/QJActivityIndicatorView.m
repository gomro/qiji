//
//  QJActivityIndicatorView.m
//  QJBox
//
//  Created by macm on 2022/8/12.
//

#import "QJActivityIndicatorView.h"

@interface QJActivityIndicatorView ()

//失败按钮
@property (nonatomic, strong) UIButton *failBtn;
//菊花图标
@property (nonatomic, strong) UIActivityIndicatorView *activity;

@end

@implementation QJActivityIndicatorView

- (UIButton *)failBtn{
    if (!_failBtn) {
        //失败图标
        _failBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _failBtn.userInteractionEnabled = NO;
        [_failBtn setBackgroundImage:[UIImage imageNamed:@"mine_msg_redo"] forState:UIControlStateNormal];
        [self addSubview:_failBtn];
    }
    return _failBtn;
    
}

- (UIActivityIndicatorView *)activity{
    if (!_activity) {
        //菊花图标
        _activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [self addSubview:_activity];
    }
    return _activity;
}


- (void)setFrame:(CGRect)frame{
    [super setFrame:frame];

    self.failBtn.frame = CGRectMake(0, 0, frame.size.width, frame.size.height);
    self.activity.frame = CGRectMake(0, 0, frame.size.width, frame.size.height);
}

#pragma mark - 显示操作
- (void)setMessageState:(QJSendMessageStatus)messageState{
    
    _messageState = messageState;
    
    self.failBtn.hidden = YES;
    self.activity.hidden = YES;
    [self.activity stopAnimating];
    
    switch (messageState) {
        case QJSendMessageType_Sending://发送中
        {
            self.hidden = NO;
            self.activity.hidden = NO;
            [self.activity startAnimating];
        }
            break;
        case QJSendMessageType_Failed://失败
        {
            self.hidden = NO;
            self.failBtn.hidden = NO;
        }
            break;
        case QJSendMessageType_Successed://成功
        {
            self.hidden = YES;
        }
            break;
        default:
            break;
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
