//
//  QJMessageInputView.h
//  QJBox
//
//  Created by macm on 2022/8/15.
//

#import <UIKit/UIKit.h>
#import "QJMineMsgTextView.h"

NS_ASSUME_NONNULL_BEGIN
@protocol QJMessageInputViewDelegate <NSObject>
//发送消息
- (void)QJMessageInputViewSend:(NSString *)sendText;
//监听键盘状态
- (void)QJMessageInputViewKeyboardStatus:(BOOL)isShow keyboardHeight:(CGFloat)keyboardHeight animationTime:(CGFloat)animationTime;

@end

@interface QJMessageInputView : UIView
@property (nonatomic, strong) QJMineMsgTextView *textView;
@property (nonatomic, weak) id<QJMessageInputViewDelegate> delegate;
@end

NS_ASSUME_NONNULL_END
