//
//  QJGetVerifyCodeRequest.h
//  QJBox
//
//  Created by wxy on 2022/6/15.
//

#import "QJBaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

/// 获取验证码
@interface QJGetVerifyCodeRequest : QJBaseRequest

@end

NS_ASSUME_NONNULL_END
