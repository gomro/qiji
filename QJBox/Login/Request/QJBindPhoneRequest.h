//
//  QJBindPhoneRequest.h
//  QJBox
//
//  Created by wxy on 2022/7/13.
//

#import "QJBaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

/// 绑定手机号接口
@interface QJBindPhoneRequest : QJBaseRequest



@end

NS_ASSUME_NONNULL_END
