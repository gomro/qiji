//
//  QJMakeSureVerifyCodeRequest.h
//  QJBox
//
//  Created by wxy on 2022/6/29.
//

#import "QJBaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

/// 确认验证码
@interface QJMakeSureVerifyCodeRequest : QJBaseRequest

@end

NS_ASSUME_NONNULL_END
