//
//  QJGetVerifyCodeRequest.m
//  QJBox
//
//  Created by wxy on 2022/6/15.
//

#import "QJGetVerifyCodeRequest.h"

@implementation QJGetVerifyCodeRequest



- (NSString *)requestUrl {
    return @"/user/verify/code";
}




- (YTKRequestMethod)requestMethod {
    
    return YTKRequestMethodGET;
}


- (YTKRequestSerializerType)requestSerializerType {
    
    return YTKRequestSerializerTypeHTTP;
}


 





@end
