//
//  QJBindPhoneRequest.m
//  QJBox
//
//  Created by wxy on 2022/7/13.
//

#import "QJBindPhoneRequest.h"

@implementation QJBindPhoneRequest

- (NSString *)requestUrl {
    return @"/user/bindMobile";
}


- (YTKRequestMethod)requestMethod {
    
    return YTKRequestMethodPOST;
}


- (YTKRequestSerializerType)requestSerializerType {
    
    return YTKRequestSerializerTypeJSON;
}




@end
