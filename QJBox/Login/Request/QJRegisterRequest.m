//
//  QJRegisterRequest.m
//  QJBox
//
//  Created by wxy on 2022/6/15.
//

#import "QJRegisterRequest.h"

@implementation QJRegisterRequest




- (NSString *)requestUrl {
    return @"/user/verify/register";
}




- (YTKRequestMethod)requestMethod {
    
    return YTKRequestMethodPOST;
}


- (YTKRequestSerializerType)requestSerializerType {
    
    return YTKRequestSerializerTypeJSON;
}


 



@end
