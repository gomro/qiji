//
//  QJMakeSureVerifyCodeRequest.m
//  QJBox
//
//  Created by wxy on 2022/6/29.
//

#import "QJMakeSureVerifyCodeRequest.h"

@implementation QJMakeSureVerifyCodeRequest



- (NSString *)requestUrl {
    return @"/user/verify/codeVerify";
}


 

- (YTKRequestMethod)requestMethod {
    
    return YTKRequestMethodGET;
}


- (YTKRequestSerializerType)requestSerializerType {
    
    return YTKRequestSerializerTypeHTTP;
}


@end
