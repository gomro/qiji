//
//  QJLoginSetPwdRequest.h
//  QJBox
//
//  Created by wxy on 2022/6/29.
//

#import "QJBaseRequest.h"

NS_ASSUME_NONNULL_BEGIN


/// 设置密码接口
@interface QJLoginSetPwdRequest : QJBaseRequest



@end

NS_ASSUME_NONNULL_END
