//
//  QJLoginAppRequest.m
//  QJBox
//
//  Created by wxy on 2022/6/10.
//

#import "QJLoginAppRequest.h"

@implementation QJLoginAppRequest


- (NSString *)requestUrl {
    return @"/user/login";
}


 

- (YTKRequestMethod)requestMethod {
    
    return YTKRequestMethodPOST;
}


- (YTKRequestSerializerType)requestSerializerType {
    
    return YTKRequestSerializerTypeJSON;
}


 

 

@end
