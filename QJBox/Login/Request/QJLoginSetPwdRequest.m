//
//  QJLoginSetPwdRequest.m
//  QJBox
//
//  Created by wxy on 2022/6/29.
//

#import "QJLoginSetPwdRequest.h"

@implementation QJLoginSetPwdRequest


- (NSString *)requestUrl {
    return @"/user/setpwd";
}


 

- (YTKRequestMethod)requestMethod {
    
    return YTKRequestMethodPUT;
}


- (YTKRequestSerializerType)requestSerializerType {
    
    return YTKRequestSerializerTypeJSON;
}




@end
