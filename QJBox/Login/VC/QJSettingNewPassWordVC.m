//
//  QJSettingNewPassWordVC.m
//  QJBox
//
//  Created by wxy on 2022/6/28.
//

#import "QJSettingNewPassWordVC.h"
#import "QJLoginBgView.h"
#import "QJLoginSetPwdRequest.h"
#import "QJAccountLoginViewController.h"

@interface QJSettingNewPassWordVC ()

@property (nonatomic, strong) QJLoginBgView *bgView;

@property (nonatomic, strong) UILabel *topLabel;

@property (nonatomic, strong) UIImageView *firstIconImageView;

@property (nonatomic, strong) UITextField *firstTF;

@property (nonatomic, strong) UIView *firstLineView;

@property (nonatomic, strong) UIImageView *secondIconImageView;

@property (nonatomic, strong) UITextField *secondTF;

@property (nonatomic, strong) UIView *secondLineView;

@property (nonatomic, strong) QJLoginBottomBtn *bottomBtn;

@property (nonatomic, strong) QJLoginSetPwdRequest *loginSetPWDReq;//设置密码接口

@property (nonatomic, strong) UIButton *firstEyeBtn;

@property (nonatomic, strong) UIButton *secondEyeBtn;//

@property (nonatomic, assign) NSInteger animationCurve;

@property (nonatomic, assign) NSInteger animationDuration;

@property (nonatomic, assign) CGFloat backBtnY;

@end


@implementation QJSettingNewPassWordVC

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
   
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.backBtnY = self.navBar.origin.y;
    self.navBar.backgroundColor = [UIColor clearColor];
    [self setUI];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHidden:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWillShow:(NSNotification *)aNotification {
    NSInteger curve = [[aNotification userInfo][UIKeyboardAnimationCurveUserInfoKey] integerValue];
    _animationCurve = curve<<16;

    //  Getting keyboard animation duration
    CGFloat duration = [[aNotification userInfo][UIKeyboardAnimationDurationUserInfoKey] floatValue];
    
    //Saving animation duration
    if (duration != 0.0)    _animationDuration = duration;
    [IQKeyboardManager sharedManager].movedDistanceChanged = ^(CGFloat movedDistance) {
        [UIView animateWithDuration:self.animationDuration delay:0 options:(self.animationCurve|UIViewAnimationOptionBeginFromCurrentState) animations:^{
            [self.bgView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.height.left.right.equalTo(self.view);
                make.top.equalTo(self.view).offset(movedDistance);
                
            }];
            CGFloat y = self.backBtnY;
            y = y +movedDistance;
            CGPoint origin = CGPointMake(self.navBar.origin.x, y);
            self.navBar.origin = origin;
            self.bgView.logoImageView.hidden = YES;
            [self.view layoutIfNeeded];
        } completion:^(BOOL finished) {
             
        }];
    };
}


- (void)keyboardWillHidden:(NSNotification *)notification {
    self.bgView.logoImageView.hidden = NO;
    CGPoint origin = CGPointMake(self.navBar.origin.x, self.backBtnY);
    self.navBar.origin = origin;
}

- (void)setUI {
    
    [self.view addSubview:self.bgView];
    [self.view addSubview:self.topLabel];
    [self.view addSubview:self.firstIconImageView];
    [self.view addSubview:self.firstTF];
    [self.view addSubview:self.firstLineView];
    [self.view addSubview:self.secondIconImageView];
    [self.view addSubview:self.secondTF];
    [self.view addSubview:self.secondLineView];
    [self.view addSubview:self.bottomBtn];
    [self.view addSubview:self.firstEyeBtn];
    [self.view addSubview:self.secondEyeBtn];
    
    
    
    [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
        
    }];
    
    [self.topLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.firstIconImageView.mas_top).offset(-24);
        make.left.equalTo(self.view).offset(37);
    }];
    
    [self.firstIconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.bottomBtn.mas_top).offset(-96);
        make.left.equalTo(self.view).offset(35);
        make.width.equalTo(@24);
        make.height.equalTo(@24);
    }];
    
    [self.firstTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.firstIconImageView);
        make.left.equalTo(self.firstIconImageView.mas_right).offset(6);
        make.height.equalTo(@32);
        make.right.equalTo(self.firstEyeBtn.mas_left);
    }];
    
    [self.firstLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.firstTF);
        make.right.equalTo(self.firstEyeBtn);
        make.height.equalTo(@1);
        make.top.equalTo(self.firstTF.mas_bottom);
        
    }];
    
    [self.secondIconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.firstIconImageView);
        make.width.equalTo(@24);
        make.height.equalTo(@24);
        make.bottom.equalTo(self.bottomBtn.mas_top).offset(-44);
        
    }];
    
    [self.secondTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.secondIconImageView);
        make.left.equalTo(self.firstTF);
        make.height.equalTo(@32);
        make.right.equalTo(self.firstTF);
    }];
    
    [self.secondLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.secondTF);
        make.right.equalTo(self.secondEyeBtn);
        make.height.equalTo(@1);
        make.top.equalTo(self.secondTF.mas_bottom);
        
        
    }];
    
    [self.bottomBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).offset(39);
        make.right.equalTo(self.view).offset(-36);
        make.height.equalTo(@48);
        make.bottom.equalTo(self.view).offset(-80-Bottom_iPhoneX_SPACE);
    }];
    
    [self.firstEyeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.firstTF);
        make.width.equalTo(@16);
        make.height.equalTo(@16);
        make.right.equalTo(self.view).offset(-45);
    }];
    
    [self.secondEyeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.secondTF);
        make.width.equalTo(@16);
        make.height.equalTo(@16);
        make.right.equalTo(self.view).offset(-45);
    }];
    
}


#pragma mark  ------ getter


- (QJLoginBgView *)bgView {
    if (!_bgView) {
        _bgView = [QJLoginBgView new];
        
    }
    return _bgView;
}

- (UILabel *)topLabel {
    if (!_topLabel) {
        _topLabel = [UILabel new];
        _topLabel.font = kboldFont(16);
        _topLabel.text = @"请设置新密码";
        _topLabel.textAlignment = NSTextAlignmentLeft;
    }
    return _topLabel;
}

- (UIImageView *)firstIconImageView {
    if (!_firstIconImageView) {
        _firstIconImageView = [UIImageView new];
        _firstIconImageView.image = [UIImage imageNamed:@"loginLock"];
    }
    return _firstIconImageView;
}

-(UITextField *)firstTF {
    if (!_firstTF) {
        _firstTF = [UITextField new];
        NSAttributedString *attStr = [[NSAttributedString alloc]initWithString:@"请输入新的登录密码" attributes:@{NSFontAttributeName:FontRegular(14),NSForegroundColorAttributeName:[UIColor colorWithHexString:@"#919599"]}];
        _firstTF.attributedPlaceholder = attStr;
        _firstTF.font = kFont(14);
        _firstTF.secureTextEntry = YES;
    }
    return _firstTF;
}

- (UIView *)firstLineView {
    if (!_firstLineView) {
        _firstLineView = [UIView new];
        _firstLineView.backgroundColor = [UIColor grayColor];
    }
    return _firstLineView;
}


- (UIImageView *)secondIconImageView {
    if (!_secondIconImageView) {
        _secondIconImageView = [UIImageView new];
        _secondIconImageView.image = [UIImage imageNamed:@"loginLock"];
    }
    return _secondIconImageView;
}

-(UITextField *)secondTF {
    if (!_secondTF) {
        _secondTF = [UITextField new];
        NSAttributedString *attStr = [[NSAttributedString alloc]initWithString:@"请确认新的登录密码" attributes:@{NSFontAttributeName:FontRegular(14),NSForegroundColorAttributeName:[UIColor colorWithHexString:@"#919599"]}];
        _secondTF.attributedPlaceholder = attStr;
        _secondTF.font = kFont(14);
        _secondTF.secureTextEntry = YES;
    }
    return _secondTF;
}

- (UIView *)secondLineView {
    if (!_secondLineView) {
        _secondLineView = [UIView new];
        _secondLineView.backgroundColor = [UIColor grayColor];
    }
    return _secondLineView;
}

- (UIButton *)firstEyeBtn {
    if (!_firstEyeBtn) {
        _firstEyeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_firstEyeBtn addTarget:self action:@selector(firstBtyAction:) forControlEvents:UIControlEventTouchUpInside];
        
        [_firstEyeBtn setImage:[UIImage imageNamed:@"preview-closelogin"] forState:UIControlStateNormal];
        [_firstEyeBtn setImage:[UIImage imageNamed:@"previewlogin"] forState:UIControlStateSelected];
        
    }
    return _firstEyeBtn;
}

- (UIButton *)secondEyeBtn {
    if (!_secondEyeBtn) {
        _secondEyeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_secondEyeBtn addTarget:self action:@selector(secondBtyAction:) forControlEvents:UIControlEventTouchUpInside];
        
        [_secondEyeBtn setImage:[UIImage imageNamed:@"preview-closelogin"] forState:UIControlStateNormal];
        [_secondEyeBtn setImage:[UIImage imageNamed:@"previewlogin"] forState:UIControlStateSelected];
        
        
    }
    return _secondEyeBtn;
}

- (QJLoginBottomBtn *)bottomBtn {
    if (!_bottomBtn) {
        WS(weakSelf)
        _bottomBtn = [QJLoginBottomBtn new];
        _bottomBtn.title = @"确认";
        _bottomBtn.btnBlock = ^{
            if (IsStrEmpty(weakSelf.firstTF.text)) {
                [QJAppTool showWarningToast:@"请输入新的登录密码"];
                return;
            }
            if (IsStrEmpty(weakSelf.secondTF.text)) {
                
                [QJAppTool showWarningToast:@"请确认新的登录密码"];
                
                return;
            }
            if (![weakSelf.secondTF.text isEqualToString:weakSelf.firstTF.text]) {
                
                [QJAppTool showWarningToast:@"密码输入不一致，请重新输入"];
                return;
            }
            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
            [dic setValue:[weakSelf.secondTF.text stringByReplacingOccurrencesOfString:@" " withString:@""]  forKey:@"checkedPassword"];
            [dic setValue:[weakSelf.firstTF.text stringByReplacingOccurrencesOfString:@" " withString:@""]  forKey:@"newPassword"];
            [dic setValue:weakSelf.mobile?:@"" forKey:@"mobile"];
            [dic setValue:weakSelf.code?:@"" forKey:@"code"];
            weakSelf.loginSetPWDReq.dic = dic.copy;
            weakSelf.loginSetPWDReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
                if ([request.responseObject[@"code"] isEqualToString:@"0"]) {
                    //成功
                    for (UIViewController *vc in weakSelf.navigationController.viewControllers) {
                        if ([vc isKindOfClass:[QJAccountLoginViewController class]]) {
                            [weakSelf.navigationController popToViewController:vc animated:YES];
                            return;
                        }
                    }
                    
                    
                }else {//异常处理
                    
                    [QJAppTool showToast:EncodeStringFromDic(request.responseObject, @"message")];
                }
            };
            weakSelf.loginSetPWDReq.failureCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
                
            };
            [weakSelf.loginSetPWDReq start];
        };
        
    }
    return _bottomBtn;
}

- (QJLoginSetPwdRequest *)loginSetPWDReq {
    if (!_loginSetPWDReq) {
        _loginSetPWDReq = [QJLoginSetPwdRequest new];
    }
    return _loginSetPWDReq;
}

#pragma mark  ------ action

- (void)firstBtyAction:(UIButton *)sender {
    self.firstTF.secureTextEntry = sender.selected;
    sender.selected = !sender.selected;
    
}


- (void)secondBtyAction:(UIButton *)sender {
    self.secondTF.secureTextEntry = sender.selected;
    sender.selected = !sender.selected;
    
}

@end
