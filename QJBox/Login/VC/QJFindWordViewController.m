//
//  QJFindWordViewController.m
//  QJBox
//
//  Created by wxy on 2022/6/13.
//

#import "QJFindWordViewController.h"
#import "QJLoginBgView.h"
#import "QJGetVerifyCodeRequest.h"
#import "QJVerifyCodeViewController.h"
#import "QJImageVerifyCode.h"
@interface QJFindWordViewController ()<UITextFieldDelegate,QJImageVerifyCodeDelegate>

{
    NSString    *previousTextFieldContent;
    UITextRange *previousSelection;

}
@property (nonatomic, strong) QJLoginBgView *bgView;

@property (nonatomic, strong) UILabel *phoneLabel;//输入手机号

@property (nonatomic, strong) UIImageView *userIconImageView;

@property (nonatomic, strong) UITextField *userTFView;//手机号输入

@property (nonatomic, strong) UIView *phoneLine;

@property (nonatomic, strong) QJLoginBottomBtn *loginBtn;//登录按钮

@property (nonatomic, strong) QJGetVerifyCodeRequest *getVerifyCodeReq;//获取验证码

@property (nonatomic, strong) QJImageVerifyCode *imageVerifyCode;

@property (nonatomic, assign) NSInteger animationCurve;

@property (nonatomic, assign) NSInteger animationDuration;

@property (nonatomic, assign) CGFloat backBtnY;

@end

@implementation QJFindWordViewController
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

//返回按钮响应
- (void)onBack:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
 
    if (self.isHiddenNavgationBar) {
        //隐藏系统导航栏
        [self.navigationController setNavigationBarHidden:YES animated:YES];
    }
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.backBtnY = self.navBar.origin.y;
    self.navBar.backgroundColor = [UIColor clearColor];
    [self setUI];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHidden:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWillShow:(NSNotification *)aNotification {
    NSInteger curve = [[aNotification userInfo][UIKeyboardAnimationCurveUserInfoKey] integerValue];
    _animationCurve = curve<<16;

    //  Getting keyboard animation duration
    CGFloat duration = [[aNotification userInfo][UIKeyboardAnimationDurationUserInfoKey] floatValue];
    
    //Saving animation duration
    if (duration != 0.0)    _animationDuration = duration;
    [IQKeyboardManager sharedManager].keyboardDistanceFromTextField = 110;
    [IQKeyboardManager sharedManager].movedDistanceChanged = ^(CGFloat movedDistance) {
        [UIView animateWithDuration:self.animationDuration delay:0 options:(self.animationCurve|UIViewAnimationOptionBeginFromCurrentState) animations:^{
            [self.bgView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.height.left.right.equalTo(self.view);
                make.top.equalTo(self.view).offset(movedDistance);
                
            }];
            CGFloat y = self.backBtnY;
            y = y +movedDistance;
            CGPoint origin = CGPointMake(self.navBar.origin.x, y);
            self.navBar.origin = origin;
            self.bgView.logoImageView.hidden = YES;
            [self.view layoutIfNeeded];
        } completion:^(BOOL finished) {
             
        }];
    };
    
}

- (void)keyboardWillHidden:(NSNotification *)notification {
    self.bgView.logoImageView.hidden = NO;
    CGPoint origin = CGPointMake(self.navBar.origin.x, self.backBtnY);
    self.navBar.origin = origin;
}

- (void)setUI {
    
    [self.view addSubview:self.bgView];
    [self.view addSubview:self.phoneLabel];
    [self.view addSubview:self.userIconImageView];
    [self.view addSubview:self.userTFView];
    [self.view addSubview:self.loginBtn];
    [self.view addSubview:self.phoneLine];
    
    [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.view);
    }];
    
    [self.loginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.view).offset(-80 - Bottom_iPhoneX_SPACE);
        make.left.equalTo(self.view).offset(39);
        make.right.equalTo(self.view).offset(-36);
        make.height.equalTo(@48);
    }];
    
    [self.userIconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).offset(36);
        make.width.height.equalTo(@24);
        make.bottom.equalTo(self.loginBtn.mas_top).offset(-40);
    }];
    
    [self.userTFView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.userIconImageView.mas_right).offset(8);
            make.height.equalTo(@32);
            make.right.equalTo(self.view).offset(-42);
            make.centerY.equalTo(self.userIconImageView);
    }];
    
    [self.phoneLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.loginBtn.mas_top).offset(-39);
            make.left.equalTo(self.userTFView);
            make.right.equalTo(self.userTFView);
            make.height.equalTo(@1);
    }];
    
//    [self.phoneLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.bottom.equalTo(self.userTFView.mas_top).offset(-30);
//            make.left.equalTo(self.userIconImageView);
//            make.right.equalTo(self.loginBtn);
//            
//    }];
  
}

#pragma mark  --- 获取验证码
- (void)getCode {
    WS(weakSelf)
    NSString *mobileStr = [self.userTFView.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:mobileStr forKey:@"mobile"];
    NSString *type = @"";
    switch (self.type) {
        case QJGetVerifyCodeVCType_forgetVC:
            type = @"UPDATE_PASSWORD";
            break;
        case QJGetVerifyCodeVCType_bindingVC:
            type = @"BIND_PHONE";
            break;
        case QJGetVerifyCodeVCType_phoneLogin:
            type = @"LOGIN";
            break;
        default: {
            type = @"UPDATE_PASSWORD";
        }
            break;
    }
    [dic setValue:type forKey:@"type"];
    self.getVerifyCodeReq.dic = dic.copy;
    self.getVerifyCodeReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        if ([EncodeStringFromDic(request.responseObject, @"code") isEqualToString:@"0"]) {
            QJVerifyCodeViewController *vc = [QJVerifyCodeViewController new];
            vc.mobile = mobileStr;
            vc.type = weakSelf.type;
            vc.bindToken = weakSelf.bindToken;
            vc.bindType = weakSelf.bindType;
            vc.loginOrRegisterSuccessBlock = weakSelf.loginOrRegisterSuccessBlock;
            [weakSelf.userTFView resignFirstResponder];
            [weakSelf.navigationController pushViewController:vc animated:YES];
        }else if ([EncodeStringFromDic(request.responseObject, @"code") isEqualToString:@"501"]) {//出滑块
            //滑块
            weakSelf.imageVerifyCode = [QJImageVerifyCode new];
            weakSelf.imageVerifyCode.delegate = weakSelf;
            [weakSelf.imageVerifyCode showView];
            
        }
        else{
            NSString *msg = EncodeStringFromDic(request.responseObject, @"message");
            [QJAppTool showWarningToast:msg];
        }
       
    };
    
    weakSelf.getVerifyCodeReq.failureCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        
        [QJAppTool showFailToast:@"验证码获取失败"];

    };
    [weakSelf.getVerifyCodeReq start];
}

#pragma mark  --------- getter

- (QJLoginBgView *)bgView {
    if (!_bgView) {
        _bgView = [QJLoginBgView new];
    }
    return _bgView;
}

- (UITextField *)userTFView {
    if (!_userTFView) {
        _userTFView = [UITextField new];
        _userTFView.delegate = self;
        NSAttributedString *attStr = [[NSAttributedString alloc]initWithString:@"请输入手机号" attributes:@{NSFontAttributeName:FontRegular(14),NSForegroundColorAttributeName:[UIColor colorWithHexString:@"#919599"]}];
        _userTFView.attributedPlaceholder = attStr;
        _userTFView.font = kFont(14);
        _userTFView.clearButtonMode = UITextFieldViewModeWhileEditing;
        _userTFView.keyboardType = UIKeyboardTypeNumberPad;
        [_userTFView addTarget:self action:@selector(formatPhoneNumber:) forControlEvents:UIControlEventEditingChanged];
    }
    return _userTFView;
}


-(UIImageView *)userIconImageView {
    if (!_userIconImageView) {
        _userIconImageView = [UIImageView new];
        _userIconImageView.image = [UIImage imageNamed:@"phoneiconPhone"];
    }
    return _userIconImageView;
}

- (QJLoginBottomBtn *)loginBtn {
    if (!_loginBtn) {
        _loginBtn = [QJLoginBottomBtn new];
        _loginBtn.title = @"获取验证码";
        WS(weakSelf)
        _loginBtn.btnBlock = ^{
            DLog(@"获取验证码");
            if (IsStrEmpty(weakSelf.userTFView.text)) {
                
                [QJAppTool showWarningToast:@"请先输入登录手机号"];
                return;
            }
            
            [weakSelf getCode];
     
        };
    }
    return _loginBtn;
}
 


- (QJGetVerifyCodeRequest *)getVerifyCodeReq {
    
    if (!_getVerifyCodeReq) {
        _getVerifyCodeReq = [QJGetVerifyCodeRequest new];
        
    }
    return _getVerifyCodeReq;
    
}


- (UILabel *)phoneLabel {
    if (!_phoneLabel) {
        _phoneLabel = [UILabel new];
        _phoneLabel.font = kboldFont(14);
        _phoneLabel.text = @"输入手机号";
        _phoneLabel.textAlignment = NSTextAlignmentLeft;
        _phoneLabel.hidden = YES;
        
    }
    return _phoneLabel;
}

- (void)setType:(QJGetVerifyCodeVCType)type {
    _type = type;
    switch (type) {
        case QJGetVerifyCodeVCType_forgetVC:
            self.phoneLabel.text = @"输入手机号";
            self.title = @"忘记密码";
            break;
        case QJGetVerifyCodeVCType_bindingVC:
            self.phoneLabel.text = @"请绑定手机";
            break;
        default: {
            self.phoneLabel.text = @"输入手机号";
        }
            break;
    }
}

- (UIView *)phoneLine {
    if (!_phoneLine) {
        _phoneLine = [UIView new];
        _phoneLine.backgroundColor = [UIColor colorWithHexString:@"#919599"];
    }
    return _phoneLine;
}


- (QJImageVerifyCode *)imageVerifyCode {
    if (!_imageVerifyCode) {
        _imageVerifyCode = [QJImageVerifyCode new];
    }
    return _imageVerifyCode;
}

#pragma mark ----- action




#pragma mark  ----- 手机号控制
- (void)formatPhoneNumber:(UITextField*)textField{
    NSUInteger targetCursorPosition = [textField offsetFromPosition:textField.beginningOfDocument toPosition:textField.selectedTextRange.start];
    NSLog(@"targetCursorPosition:%li", (long)targetCursorPosition);
    NSString* nStr = [textField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString* preTxt = [previousTextFieldContent stringByReplacingOccurrencesOfString:@" " withString:@""];
    char editFlag = 0;// 正在执行删除操作时为0，否则为1
    if (nStr.length <= preTxt.length) {
        editFlag = 0;
    }else{
        
        editFlag = 1;
    }
    if (nStr.length > 11) {
        textField.text = previousTextFieldContent;
        textField.selectedTextRange = previousSelection;
        return;
    }
    NSString* spaceStr = @" ";
    NSMutableString *mStrTemp = [NSMutableString string];
    int spaceCount = 0;
    if (nStr.length < 3 && nStr.length > -1) {
        spaceCount = 0;
    }else if (nStr.length < 7&& nStr.length > 2){
        spaceCount = 1;
    }else if (nStr.length < 12&& nStr.length > 6){
        spaceCount = 2;
    }
    for (int i = 0; i < spaceCount; i++)
    {
        if (i == 0) {
            [mStrTemp appendFormat:@"%@%@", [nStr substringWithRange:NSMakeRange(0, 3)],spaceStr];
        }else if (i == 1)
        {
            [mStrTemp appendFormat:@"%@%@", [nStr substringWithRange:NSMakeRange(3, 4)], spaceStr];
        }else if (i == 2)
        {
            [mStrTemp appendFormat:@"%@%@", [nStr substringWithRange:NSMakeRange(7, 4)], spaceStr];
        }
    }
    
    if (nStr.length == 11)
    {
        [mStrTemp appendFormat:@"%@%@", [nStr substringWithRange:NSMakeRange(7, 4)], spaceStr];
    }
    
    if (nStr.length < 4 && nStr.length > 0)
    {
        [mStrTemp appendString:[nStr substringWithRange:NSMakeRange(nStr.length-nStr.length % 3,nStr.length % 3)]];
    }else if(nStr.length > 3)
    {
        NSString *str = [nStr substringFromIndex:3];
        [mStrTemp appendString:[str substringWithRange:NSMakeRange(str.length-str.length % 4,str.length % 4)]];
        if (nStr.length == 11)
        {
            [mStrTemp deleteCharactersInRange:NSMakeRange(13, 1)];
        }
    }
    NSLog(@"=======mstrTemp=%@",mStrTemp);
    textField.text = mStrTemp;
    // textField设置selectedTextRange
    NSUInteger curTargetCursorPosition = targetCursorPosition;// 当前光标的偏移位置
    if (editFlag == 0){
        //删除
        if (targetCursorPosition == 9 || targetCursorPosition == 4)
        {
            curTargetCursorPosition = targetCursorPosition - 1;
        }
     }
    else {
        //添加
        if (nStr.length == 8 || nStr.length == 4)
        {
            curTargetCursorPosition = targetCursorPosition + 1;
        }
    }
    
    UITextPosition *targetPosition = [textField positionFromPosition:[textField beginningOfDocument] offset:curTargetCursorPosition];
    [textField setSelectedTextRange:[textField textRangeFromPosition:targetPosition toPosition :targetPosition]];
}

#pragma mark UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    previousTextFieldContent = textField.text;
    previousSelection = textField.selectedTextRange;
    if (range.location == 0)
    {
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:@"1"] invertedSet];
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        BOOL basicTest = [string isEqualToString:filtered];
        if (!basicTest)
        {
             
            return NO;
        }
    }
    
    if (range.location > 12)
    {
        return NO;
    }
    return YES;
}

#pragma mark ---- QJImageVerifyCodeDelegate

/// 完成验证之后的回调
/// @param result 验证结果 BOOL:YES/NO
/// @param validate 二次校验数据，如果验证结果为false，validate返回空
/// @param message 结果描述信息
- (void)QJImageVerifyCodeValidateFinish:(BOOL)result validate:(NSString *)validate message:(NSString *)message {
    DLog(@"收到验证结果的回调:(%d,%@,%@)", result, validate, message);
    if (result) {
        [QJUserManager shareManager].validate = validate;
        [self getCode];
      
        
    }
  
}
 
@end
