//
//  QJThirdLoginViewController.m
//  QJBox
//
//  Created by wxy on 2022/6/27.
//

#import "QJThirdLoginViewController.h"
#import <AuthenticationServices/AuthenticationServices.h>
#import "QJSignInWithApple.h"
#import "QJLoginWithGreen.h"
#import "QJLoginWithPenguin.h"
#import "QJAccountLoginViewController.h"
#import "QJFindWordViewController.h"
#import "QJLoginAppRequest.h"
@interface QJThirdLoginViewController ()<WXAuthDelegate>

/** 暗色背景 */
@property (nonatomic, strong) UIView *bgView;
/** 控件View */
@property (nonatomic, strong) UIView *contentView;

@property (nonatomic, strong) UIButton *accountBtn;

@property (nonatomic, strong) UIButton *greenBtn;//微信

@property (nonatomic, strong) UIButton *penguinBtn;//qq

@property (nonatomic, strong) QJSignInWithApple *apple;

@property (nonatomic, strong) QJLoginAppRequest *loginGreenReq;//微信登录

@property (nonatomic, strong) QJLoginWithPenguin *penguin;

@property (nonatomic, strong) QJLoginWithGreen *green;

@property (nonatomic, strong) UIButton *closeBtn;


@property (nonatomic, copy) NSString *appleName;




@end

@implementation QJThirdLoginViewController

- (instancetype)init {
    if (self = [super init]) {
        self.modalPresentationStyle = UIModalPresentationCustom;
    }
    return self;
}



- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navBar.hidden = YES;
    [self setUI];
    
    [self startAnimation];
    
}


- (void)startAnimation {
    self.bgView.frame = [UIScreen mainScreen].bounds;
    self.contentView.frame = CGRectMake(0,[UIScreen mainScreen].bounds.size.height, CGRectGetWidth([UIScreen mainScreen].bounds), 210*kWScale);
    
    
    [UIView animateWithDuration:0.4 delay:0 usingSpringWithDamping:0.95 initialSpringVelocity:0.05 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        
        self.bgView.alpha = 1.0f ;
        
        self.contentView.frame = CGRectMake(0, [UIScreen mainScreen].bounds.size.height - 210*kWScale, CGRectGetWidth([UIScreen mainScreen].bounds), 210*kWScale);
        
    } completion:^(BOOL finished) {
    }];
}


- (void)setUI {
    
    self.view.backgroundColor = [UIColor clearColor];
    UIView *bgView = [[UIView alloc] init];
    bgView.backgroundColor = [UIColor colorWithRed:0.25 green:0.25 blue:0.25 alpha:0.5f];
    [self.view addSubview:bgView];
    self.bgView = bgView;
    self.bgView.alpha = 0.0f;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickBgView)];
    [bgView addGestureRecognizer:tap];
    
    UIView *cView = [[UIView alloc] init];
    cView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:cView];
    self.contentView = cView;
    self.contentView.layer.cornerRadius = 16;
    self.contentView.layer.masksToBounds = YES;
    
    [self.contentView addSubview:self.closeBtn];
    [self.closeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.contentView).offset(-kWScale*14);
            make.top.equalTo(self.contentView).offset(kWScale*14);
            make.width.height.equalTo(@(kWScale*24));
    }];
    
    NSMutableArray *arr = [NSMutableArray array];
    NSMutableArray *labelArr = [NSMutableArray array];
    [self.contentView addSubview:self.accountBtn];
    [arr safeAddObject:self.accountBtn];
    UILabel *accountLabe = [UILabel new];
    accountLabe.font = FontRegular(12);
    accountLabe.textColor = [UIColor colorWithHexString:@"#000000"];
    accountLabe.text = @"密码登录";
    accountLabe.textAlignment = NSTextAlignmentCenter;
    [labelArr safeAddObject:accountLabe];
    [self.contentView addSubview:accountLabe];
    
    
    if ([WXApi isWXAppInstalled]) {
        [self.contentView addSubview:self.greenBtn];
        [arr safeAddObject:self.greenBtn];
        UILabel *greLabe = [UILabel new];
        greLabe.font = FontRegular(12);
        greLabe.textColor = [UIColor colorWithHexString:@"#000000"];
        greLabe.text = @"微信";
        greLabe.textAlignment = NSTextAlignmentCenter;
        [labelArr safeAddObject:greLabe];
        [self.contentView addSubview:greLabe];
    }
    [self.contentView addSubview:self.penguinBtn];
    [arr safeAddObject:self.penguinBtn];
    UILabel *penguinLabe = [UILabel new];
    penguinLabe.font = FontRegular(12);
    penguinLabe.textColor = [UIColor colorWithHexString:@"#000000"];
    penguinLabe.text = @"QQ";
    penguinLabe.textAlignment = NSTextAlignmentCenter;
    [labelArr safeAddObject:penguinLabe];
    [self.contentView addSubview:penguinLabe];
    //苹果登录
    if (@available(iOS 13.0, *)) {
        ASAuthorizationAppleIDButton *appleLoginBtn = [[ASAuthorizationAppleIDButton alloc] initWithAuthorizationButtonType:ASAuthorizationAppleIDButtonTypeSignIn authorizationButtonStyle:ASAuthorizationAppleIDButtonStyleBlack];
        
        appleLoginBtn.layer.cornerRadius = 24*kWScale;
        appleLoginBtn.layer.masksToBounds = YES;
        [appleLoginBtn addTarget:self action:@selector(appleLogin) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:appleLoginBtn];
        
        [arr safeAddObject:appleLoginBtn];
        UILabel *appleLabe = [UILabel new];
        appleLabe.font = FontRegular(12);
        appleLabe.textColor = [UIColor colorWithHexString:@"#000000"];
        appleLabe.text = @"Apple";
        appleLabe.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:appleLabe];
        [labelArr safeAddObject:appleLabe];
        
    }
    
    NSArray *viewArr = arr.copy;
    
    [viewArr mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedItemLength:60 leadSpacing:43 tailSpacing:43];
    [viewArr mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(kWScale*58);
        make.width.height.equalTo(@(48*kWScale));
    }];
    
    NSArray *labelA = labelArr.copy;
    [labelA mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedItemLength:60 leadSpacing:43 tailSpacing:43];
    [labelA mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(kWScale*118);
        make.width.equalTo(@(49*kWScale));
        make.height.equalTo(@(13*kWScale));
    }];
    
}


//去绑定手机号
- (void)goToBindingPhoneWithToken:(NSString *)token type:(NSString *)type{
    QJFindWordViewController  *vc = [QJFindWordViewController new];
    vc.type = QJGetVerifyCodeVCType_bindingVC;
    vc.bindToken = token;
    vc.bindType = type;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark -------- getter



- (UIButton *)closeBtn {
    if (!_closeBtn) {
        _closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_closeBtn addTarget:self action:@selector(closeBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [_closeBtn setBackgroundImage:[UIImage imageNamed:@"qj_close_24"] forState:UIControlStateNormal];
        [_closeBtn setBackgroundImage:[UIImage imageNamed:@"qj_close_24"] forState:UIControlStateHighlighted];
        
        
    }
    return _closeBtn;
}

- (UIButton *)accountBtn {
    if (!_accountBtn) {
        _accountBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_accountBtn addTarget:self action:@selector(accountBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [_accountBtn setImage:[UIImage imageNamed:@"login_account"] forState:UIControlStateNormal];
        [_accountBtn setImage:[UIImage imageNamed:@"login_account"] forState:UIControlStateHighlighted];
        
        
    }
    return _accountBtn;
}



- (UIButton *)greenBtn {
    if (!_greenBtn) {
        _greenBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_greenBtn addTarget:self action:@selector(greenBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [_greenBtn setImage:[UIImage imageNamed:@"login_wx"] forState:UIControlStateNormal];
        [_greenBtn setImage:[UIImage imageNamed:@"login_wx"] forState:UIControlStateHighlighted];
        
    }
    return _greenBtn;
}


- (UIButton *)penguinBtn {
    if (!_penguinBtn) {
        _penguinBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_penguinBtn addTarget:self action:@selector(penguinBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [_penguinBtn setImage:[UIImage imageNamed:@"login_qq"] forState:UIControlStateNormal];
        [_penguinBtn setImage:[UIImage imageNamed:@"login_qq"] forState:UIControlStateHighlighted];
    }
    return _penguinBtn;
}


- (QJLoginAppRequest *)loginGreenReq {
    if (!_loginGreenReq) {
        _loginGreenReq = [QJLoginAppRequest new];
        
    }
    return _loginGreenReq;
}

#pragma mark ------- action


- (void)closeBtnAction:(UIButton *)sender {
    [self clickBgView];
}

- (void)accountBtnAction:(UIButton *)sender {
    
    QJAccountLoginViewController *vc = [QJAccountLoginViewController new];
    vc.loginOrRegisterSuccessBlock =  self.loginOrRegisterSuccessBlock;
 
    [self.navigationController pushViewController:vc animated:YES];
    
}



- (void)greenBtnAction:(UIButton *)sender {
    
    self.green = [QJLoginWithGreen new];
    self.green.delegate = self;
    [self.green clickGreenBtn];
}

 

- (void)penguinBtnAction:(UIButton *)sender {
    self.penguin = [QJLoginWithPenguin new];
    WS(weakSelf)
    self.penguin.didCompleteBlock = ^(NSDictionary * _Nonnull paramDic) {
        DLog(@"QQ信息：=%@",paramDic);
        
        NSString *code = EncodeStringFromDic(paramDic, @"code");
        if (!IsStrEmpty(code)) {
            [weakSelf loginWithType:@"Q" code:code];
        }
    };
    [self.penguin clickPenguinBtn];
    
}



- (void)appleLogin {
    WS(weakSelf)
    self.apple = [QJSignInWithApple new];
    self.apple.didCompleteBlock = ^(NSDictionary * _Nonnull paramDic) {
        
        NSString *name = EncodeStringFromDic(paramDic, @"name");
        NSString *authorizationCodeStr = EncodeStringFromDic(paramDic, @"union_token");
        weakSelf.appleName = name;
        [weakSelf loginWithType:@"A" code:authorizationCodeStr];
    };
     
    self.apple.didfailedBlock = ^(NSDictionary * _Nonnull paramDic) {
        
    };
    
    [self.apple appleLoginClick];
    
}


#pragma mark -
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
  
}


#pragma mark - 消失
- (void)clickBgView
{
    [UIView animateWithDuration:0.4 delay:0 usingSpringWithDamping:0.95 initialSpringVelocity:0.05 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        
        self.bgView.alpha = 0.0f ;
        self.contentView.frame = CGRectMake(0,[UIScreen mainScreen].bounds.size.height, CGRectGetWidth([UIScreen mainScreen].bounds), 210*kWScale);
        
    } completion:^(BOOL finished) {
        // 动画Animated必须是NO，不然消失之后，会有0.35s时间，再点击无效
        [self dismissViewControllerAnimated:NO completion:nil];
    }];
}



// 这里主动释放一些空间，加速内存的释放，防止有时候消失之后，再点不出来。
- (void)dealloc
{
    NSLog(@"%@ --> dealloc",[self class]);
    [self.bgView removeFromSuperview];
    self.bgView = nil;
    [self.contentView removeFromSuperview];
    self.contentView = nil;
}



/// 登录接口
/// @param type QQ WX
/// @param code  三登录的授权code
- (void)loginWithType:(NSString *)type code:(NSString *)code {
    WS(weakSelf)
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    if ([type isEqualToString:@"Q"]) {
        [dic setValue:code forKey:@"qqLoginCode"];
    }else if ([type isEqualToString:@"W"]) {//微信登录
        [dic setValue:code forKey:@"wxCode"];
    }else if ([type isEqualToString:@"A"]) {
        [dic setValue:self.appleName?:@"" forKey:@"appleName"];
        [dic setValue:code?:@"" forKey:@"identityToken"];
            }
    [dic setValue:type forKey:@"type"];
    
    self.loginGreenReq.dic = dic;
    self.loginGreenReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            NSDictionary *data = EncodeDicFromDic(request.responseObject, @"data");
            NSNumber *temp = EncodeNumberFromDic(data, @"cacheUser");
            NSString *userId = EncodeStringFromDic(data, @"uid");
            if (temp.boolValue) {//说明没有绑定手机号
                
                [weakSelf goToBindingPhoneWithToken:EncodeStringFromDic(data, @"token") type:type];
            }else{
                //登录成功
                
                [QJAppTool showSuccessToast:@"登录成功"];
                LSUserDefaultsSET(EncodeStringFromDic(data, @"token"), kQJToken);
                LSUserDefaultsSET(EncodeNumberFromDic(data, @"verifyState"), kQJIsRealName);
                LSUserDefaultsSET(userId, kQJUserId);
                [QJUserManager shareManager].userID = userId;
                [QJUserManager shareManager].isLogin = YES;
                [QJUserManager shareManager].isNetLogout = NO;//登录成功状态改变
                [QJUserManager shareManager].isVerifyState = [EncodeNumberFromDic(data, @"verifyState") boolValue];
                [weakSelf dismissLoginVC:^{
                    if (weakSelf.loginOrRegisterSuccessBlock) {
                        weakSelf.loginOrRegisterSuccessBlock(YES);
                    }
                }];
            }
        }else{
            
            [QJAppTool showToast:ResponseMsg];
        }
    };
    
    self.loginGreenReq.failureCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
         
        
        [QJAppTool showToast:ResponseFailToastMsg];
    };
    [self.loginGreenReq start];
}

#pragma mark ----- weixin 登录回调


- (void)wxAuthSucceed:(NSString*)code {
    
    [self loginWithType:@"W" code:code];
    
}


- (void)wxAuthDenied {
    
    
    [QJAppTool showToast:@"登录失败"];
}


- (void)wxAuthCancel {
    
}
@end
