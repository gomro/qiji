//
//  QJRegisterViewController.m
//  QJBox
//
//  Created by wxy on 2022/6/13.
//

#import "QJRegisterViewController.h"
#import "QJRegisterInputView.h"
#import "QJRegisterRequest.h"
@interface QJRegisterViewController ()<UITextFieldDelegate>

//账号
@property (nonatomic, strong) QJRegisterInputView *accountInputView;

//密码
@property (nonatomic, strong) QJRegisterInputView *passInputView;

//确认密码
@property (nonatomic, strong) QJRegisterInputView *rePassInputView;

//
@property (nonatomic, strong) UIButton *btn;


@property (nonatomic, strong) UIButton *agreeBtn;


@property (nonatomic, strong) YYLabel *userAgreementLabel;//用户协议


@property (nonatomic, strong) QJRegisterRequest *registerReq;//注册账号请求

@end

@implementation QJRegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"注册账号";
    
    [self.view addSubview:self.accountInputView];
    [self.view addSubview:self.passInputView];
    [self.view addSubview:self.rePassInputView];
    [self.view addSubview:self.btn];
    [self.view addSubview:self.userAgreementLabel];
    [self.view addSubview:self.agreeBtn];
    
    
    [self.accountInputView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).offset(24);
        make.right.equalTo(self.view).offset(-24);
        make.top.equalTo(self.navBar.mas_bottom).offset(50);
        make.height.equalTo(@25);
    }];
    
    [self.passInputView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).offset(24);
        make.right.equalTo(self.view).offset(-24);
        make.top.equalTo(self.accountInputView.mas_bottom).offset(25);
        make.height.equalTo(@25);
    }];
    
    [self.rePassInputView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).offset(24);
        make.right.equalTo(self.view).offset(-24);
        make.top.equalTo(self.passInputView.mas_bottom).offset(25);
        make.height.equalTo(@25);
    }];
    
    [self.btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).offset(24);
        make.right.equalTo(self.view).offset(-24);
        make.top.equalTo(self.rePassInputView.mas_bottom).offset(30);
        make.height.equalTo(@30);
    }];
    
    [self.userAgreementLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.view).offset(-Bottom_SN_iPhoneX_OR_LATER_SPACE);
        make.centerX.equalTo(self.view);
        
    }];
    
    [self.agreeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.userAgreementLabel.mas_left).offset(-5);
        make.centerY.equalTo(self.userAgreementLabel);
        make.width.height.equalTo(@20);
    }];
}




#pragma mark ----- getter


- (QJRegisterInputView *)accountInputView {
    if (!_accountInputView) {
        _accountInputView = [[QJRegisterInputView alloc]initWithFrame:CGRectZero];
        _accountInputView.titleLabel.text = @"账号";
        _accountInputView.textField.placeholder = @"账号规则";
        [_accountInputView hiddenRightBtn];
    }
    return _accountInputView;
}


- (QJRegisterInputView *)passInputView {
    if (!_passInputView) {
        _passInputView = [[QJRegisterInputView alloc]initWithFrame:CGRectZero];
        _passInputView.titleLabel.text = @"密码";
        _passInputView.textField.placeholder = @"密码规则";
        _passInputView.btnClick = ^(BOOL isSelected) {
            
        };
    }
    return _passInputView;
}

- (QJRegisterInputView *)rePassInputView {
    if (!_rePassInputView) {
        _rePassInputView = [[QJRegisterInputView alloc]initWithFrame:CGRectZero];
        _rePassInputView.titleLabel.text = @"确认密码";
        _rePassInputView.textField.placeholder = @"两次输入的密码请保持一致";
        _rePassInputView.btnClick = ^(BOOL isSelected) {
            
        };
    }
    return _rePassInputView;
}


- (UIButton *)btn {
    if (!_btn) {
        _btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_btn setTitle:@"下一步" forState:UIControlStateNormal];
        [_btn addTarget:self action:@selector(nextBtnAction) forControlEvents:UIControlEventTouchUpInside];
        [_btn setEnlargeEdgeWithTop:5 right:5 bottom:5 left:5];
        [_btn setTitleColor:[UIColor colorWithHexString:@"000000"] forState:UIControlStateNormal];
        [_btn setBackgroundColor:[UIColor grayColor]];
        
    }
    return _btn;
}

- (UIButton *)agreeBtn {
    if (!_agreeBtn) {
        _agreeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_agreeBtn addTarget:self action:@selector(btnAction:) forControlEvents:UIControlEventTouchUpInside];
        [_agreeBtn setBackgroundImage:[UIImage imageWithColor:[UIColor grayColor]] forState:UIControlStateNormal];
        [_agreeBtn setBackgroundImage:[UIImage imageWithColor:[UIColor greenColor]] forState:UIControlStateSelected];
    }
    return _agreeBtn;
}


- (YYLabel *)userAgreementLabel {
    if (!_userAgreementLabel) {
        _userAgreementLabel = [[YYLabel alloc]init];
        _userAgreementLabel.textAlignment = NSTextAlignmentCenter;
        NSString *str = @"同意《用户授权协议》和《隐私条款》";
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc]initWithString:str];
        attributedString.color = [UIColor blackColor];
        attributedString.font = kFont(12);
        NSRange firstRange = [str rangeOfString:@"《用户授权协议》"];
        NSRange secondRange = [str rangeOfString:@"《隐私条款》"];
        [attributedString setTextHighlightRange:firstRange color:[UIColor redColor] backgroundColor:[UIColor whiteColor] tapAction:^(UIView * _Nonnull containerView, NSAttributedString * _Nonnull text, NSRange range, CGRect rect) {
            DLog(@"点击用户授权协议");
        }];
        
        [attributedString setTextHighlightRange:secondRange color:[UIColor redColor] backgroundColor:[UIColor whiteColor] tapAction:^(UIView * _Nonnull containerView, NSAttributedString * _Nonnull text, NSRange range, CGRect rect) {
            DLog(@"点击隐私条款");
        }];
        _userAgreementLabel.attributedText = attributedString;
        
    }
    return _userAgreementLabel;
}


-(QJRegisterRequest *)registerReq {
    if (!_registerReq) {
        _registerReq = [QJRegisterRequest new];
    }
    return _registerReq;
}



#pragma mark ------- action

- (void)nextBtnAction {
    if (!self.agreeBtn.selected) {
        
        [QJAppTool showWarningToast:@"请勾选同意《用户授权协议》和《隐私条款》"];
        return;
    }
    if (IsStrEmpty(self.accountInputView.textField.text)) {
        
        [QJAppTool showWarningToast:@"请输入账号"];
        return;
    }
    
    if (IsStrEmpty(self.passInputView.textField.text)) {
        [QJAppTool showWarningToast:@"请输入密码"];
        
        return;
    }
    
    if (IsStrEmpty(self.rePassInputView.textField.text)) {
        [QJAppTool showWarningToast:@"请确认密码"];
        
        return;
    }
    
    if (![self.rePassInputView.textField.text isEqualToString:self.passInputView.textField.text]) {
        
        [QJAppTool showWarningToast:@"密码和和确认密码不一致"];
        return;
    }
    
    DLog(@"下一步");
    NSMutableDictionary *muDic = [NSMutableDictionary new];
    [muDic setValue:self.accountInputView.textField.text forKey:@"mobile"];
    [muDic setValue:self.passInputView.textField.text forKey:@"password"];
    [muDic setValue:@"P" forKey:@"type"];

    self.registerReq.dic = muDic.copy;
    WS(weakSelf)
    self.registerReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        
        [weakSelf.navigationController popViewControllerAnimated:YES];
    };
    [self.registerReq start];
}
 
- (void)btnAction:(UIButton *)sender {
    
    sender.selected = !sender.selected;
    
    if (sender.selected) {
        [self.btn setBackgroundColor:[UIColor greenColor]];
    }else{
        [self.btn setBackgroundColor:[UIColor grayColor]];
    }
    
}
@end
