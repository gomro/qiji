//
//  QJSettingNewPassWordVC.h
//  QJBox
//
//  Created by wxy on 2022/6/28.
//

#import "QJBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN


///设置新密码界面
@interface QJSettingNewPassWordVC : QJBaseViewController


@property (nonatomic, copy) NSString *code;//验证码

@property (nonatomic, copy) NSString *mobile;//电话


@end

NS_ASSUME_NONNULL_END
