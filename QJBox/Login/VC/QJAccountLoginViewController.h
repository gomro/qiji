//
//  QJAccountLoginViewController.h
//  QJBox
//
//  Created by wxy on 2022/6/27.
//

#import "QJBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

/// 账号密码登录页面
@interface QJAccountLoginViewController : QJBaseViewController

@property (nonatomic, copy) LoginOrRegisterSuccessBlock loginOrRegisterSuccessBlock;


@end

NS_ASSUME_NONNULL_END
