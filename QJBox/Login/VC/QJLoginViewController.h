//
//  QJLoginViewController.h
//  QJBox
//
//  Created by wxy on 2022/6/7.
//

#import "QJBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

typedef void (^LoginOrRegisterSuccessBlock)(BOOL isRegister);
/// 登录vc
@interface QJLoginViewController : QJBaseViewController


@property (nonatomic, copy) LoginOrRegisterSuccessBlock loginOrRegisterSuccessBlock;


@end

NS_ASSUME_NONNULL_END
