//
//  QJVerifyCodeViewController.m
//  QJBox
//
//  Created by wxy on 2022/6/15.
//

#import "QJVerifyCodeViewController.h"
#import "QJSMSCodeInputView.h"
#import "QJLoginAppRequest.h"
#import "QJGetVerifyCodeRequest.h"
#import "QJLoginBgView.h"
#import "QJMakeSureVerifyCodeRequest.h"
#import "QJSettingNewPassWordVC.h"
#import "QJImageVerifyCode.h"
#import "QJBindPhoneRequest.h"
@interface QJVerifyCodeViewController ()<UITextFieldDelegate,QJImageVerifyCodeDelegate>

@property (nonatomic, strong) QJSMSCodeInputView *smsInputView;

@property (nonatomic, strong) UILabel *aLabel;//请输入验证码

@property (nonatomic, strong) QJLoginAppRequest *loginReq;

@property (nonatomic, strong) QJLoginBottomBtn *bottomBtn;//重新发送按钮

@property (nonatomic, strong) QJLoginBgView *bgView;

@property (nonatomic, strong) QJMakeSureVerifyCodeRequest *makeSureCodeReq;

@property (nonatomic, strong) QJGetVerifyCodeRequest *getVerifyCodeReq;//获取验证码

@property (nonatomic, strong) QJBindPhoneRequest *bindPhoneReq;//绑定手机号

@property (nonatomic, strong) QJImageVerifyCode *imageVerifyCode;

@property (nonatomic, assign) NSInteger animationCurve;

@property (nonatomic, assign) NSInteger animationDuration;

@property (nonatomic, assign) CGFloat backBtnY;

@property (nonatomic, strong) dispatch_source_t timer;

@end

@implementation QJVerifyCodeViewController
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
   
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.backBtnY = self.navBar.origin.y;
    self.navBar.backgroundColor = [UIColor clearColor];
    if (self.type == QJGetVerifyCodeVCType_forgetVC) {
        self.title = @"忘记密码";
    }
    [self.view addSubview:self.bgView];
    [self.view addSubview:self.smsInputView];
    [self.view addSubview:self.bottomBtn];
    [self.view addSubview:self.aLabel];
    
    [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    
    [self.bottomBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.view).offset(-80 - Bottom_iPhoneX_SPACE);
        make.left.equalTo(self.view).offset(37.5);
        make.right.equalTo(self.view).offset(-37.5);
        make.height.equalTo(@48);
    }];
    
    [self.smsInputView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).offset(43.5);
        make.bottom.equalTo(self.bottomBtn.mas_top).offset(-32);
        make.height.equalTo(@48);
        make.right.equalTo(self.view).offset(-43.5);
    }];
    
    [self.aLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.smsInputView.mas_top).offset(-16);
        make.left.equalTo(self.view).offset(39);
    }];
    [self startTimer];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHidden:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWillShow:(NSNotification *)aNotification {
    NSInteger curve = [[aNotification userInfo][UIKeyboardAnimationCurveUserInfoKey] integerValue];
    _animationCurve = curve<<16;

    //  Getting keyboard animation duration
    CGFloat duration = [[aNotification userInfo][UIKeyboardAnimationDurationUserInfoKey] floatValue];
    
    //Saving animation duration
    if (duration != 0.0)    _animationDuration = duration;
    [IQKeyboardManager sharedManager].movedDistanceChanged = ^(CGFloat movedDistance) {
        [UIView animateWithDuration:self.animationDuration delay:0 options:(self.animationCurve|UIViewAnimationOptionBeginFromCurrentState) animations:^{
            [self.bgView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.height.left.right.equalTo(self.view);
                make.top.equalTo(self.view).offset(movedDistance);
                
            }];
            CGFloat y = self.backBtnY;
            y = y +movedDistance;
            CGPoint origin = CGPointMake(self.navBar.origin.x, y);
            self.navBar.origin = origin;
            self.bgView.logoImageView.hidden = YES;
            [self.view layoutIfNeeded];
        } completion:^(BOOL finished) {
             
        }];
    };
}
- (void)keyboardWillHidden:(NSNotification *)notification {
    self.bgView.logoImageView.hidden = NO;
    CGPoint origin = CGPointMake(self.navBar.origin.x, self.backBtnY);
    self.navBar.origin = origin;
}

#pragma mark  ----------- getter


- (QJLoginBgView *)bgView {
    if (!_bgView) {
        _bgView = [QJLoginBgView new];
    }
    return _bgView;
}


- (UILabel *)aLabel {
    if (!_aLabel) {
        _aLabel = [UILabel new];
        _aLabel.font = FontRegular(16);
        _aLabel.textColor = [UIColor colorWithHexString:@"#919599"];
        _aLabel.text = @"请输入验证码";
        _aLabel.textAlignment = NSTextAlignmentLeft;
    }
    return _aLabel;
}

- (QJSMSCodeInputView *)smsInputView {
    if (!_smsInputView) {
        _smsInputView = [QJSMSCodeInputView new];
        WS(weakSelf)
        _smsInputView.codeInputFinish = ^(NSString * _Nonnull code) {
            DLog(@"验证码=%@",code);
            switch (weakSelf.type) {
                case  QJGetVerifyCodeVCType_forgetVC:
                    [weakSelf makeSureCode:code];
                    break;
                case  QJGetVerifyCodeVCType_bindingVC:
                    
                    [weakSelf bindPhone:code];
                    break;
                case  QJGetVerifyCodeVCType_phoneLogin:
                    [weakSelf loginStart:code];
                    break;
                 
                default:
                    break;
            }
 
        };
        
    }
    return _smsInputView;
}


- (QJLoginAppRequest *)loginReq {
    if (!_loginReq) {
        _loginReq = [QJLoginAppRequest new];
        
    }
    return _loginReq;
}


- (QJLoginBottomBtn *)bottomBtn {
    if (!_bottomBtn) {
        WS(weakSelf)
        _bottomBtn = [QJLoginBottomBtn new];
        _bottomBtn.btnBlock = ^{
            [weakSelf getVerifyCode];
            [weakSelf startTimer];
        };
        
        _bottomBtn.title = @"重新发送";
    }
    return _bottomBtn;
}

- (QJGetVerifyCodeRequest *)getVerifyCodeReq {
    if (!_getVerifyCodeReq) {
        _getVerifyCodeReq = [QJGetVerifyCodeRequest new];
    }
    return _getVerifyCodeReq;
}

 

-(QJBindPhoneRequest *)bindPhoneReq {
    if (!_bindPhoneReq) {
        _bindPhoneReq = [QJBindPhoneRequest new];
        
    }
    return _bindPhoneReq;
}

- (QJImageVerifyCode *)imageVerifyCode {
    if (!_imageVerifyCode) {
        _imageVerifyCode = [QJImageVerifyCode new];
    }
    return _imageVerifyCode;
}

#pragma mark  ------- 定时器

- (void)startTimer {
    __weak typeof (self) weakSelf=self;
    __block NSInteger time = 60;
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    if(!self.timer){
        self.timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
    }
    dispatch_source_set_timer(self.timer, DISPATCH_TIME_NOW, 1.0 * NSEC_PER_SEC, 0 * NSEC_PER_SEC);
    dispatch_source_set_event_handler(self.timer, ^{
        if (time < 1) {
            dispatch_source_cancel(self.timer);
            self.timer = nil;
            dispatch_async(dispatch_get_main_queue(), ^{
                weakSelf.bottomBtn.userInteractionEnabled = YES;
                weakSelf.bottomBtn.color = [UIColor colorWithHexString:@"#1F2A4D"];
                weakSelf.bottomBtn.title = @"重新发送";
            });
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                weakSelf.bottomBtn.userInteractionEnabled = NO;
                weakSelf.bottomBtn.color = [UIColor colorWithHexString:@"#C8CACC"];
                weakSelf.bottomBtn.title = [NSString stringWithFormat:@"重新发送（%lds）",time];
                
            });
            time --;
        }
    });
    dispatch_resume(self.timer);
    
}

//获取验证码
- (void)getVerifyCode {

    WS(weakSelf)
    NSString *type = @"LOGIN";
    switch (self.type) {
        case QJGetVerifyCodeVCType_phoneLogin:
            type = @"LOGIN";
            break;
        case QJGetVerifyCodeVCType_bindingVC:
            type = @"BIND_PHONE";
            break;
        case QJGetVerifyCodeVCType_forgetVC:
            type = @"UPDATE_PASSWORD";
            break;
            
        default:
            break;
    }
    self.getVerifyCodeReq.dic = @{@"mobile" : self.mobile?:@"", @"type":type};
    
    self.getVerifyCodeReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        if ([EncodeStringFromDic(request.responseObject, @"code") isEqualToString:@"0"]) {
             
        }else if ([EncodeStringFromDic(request.responseObject, @"code") isEqualToString:@"501"]) {//出滑块
            //滑块
            weakSelf.imageVerifyCode = [QJImageVerifyCode new];
            weakSelf.imageVerifyCode.delegate = weakSelf;
            [weakSelf.imageVerifyCode showView];
            
        }
        else{
            NSString *msg = EncodeStringFromDic(request.responseObject, @"message");
            
            [QJAppTool showWarningToast:msg];
        }
    };
    
    [self.getVerifyCodeReq start];
    
  
}

#pragma mark ----- 登录

- (void)loginStart:(NSString *)code {
    NSMutableDictionary *muDic = [NSMutableDictionary dictionary];
    [muDic setValue:self.mobile?:@"" forKey:@"mobile"];
    [muDic setValue:@"V" forKey:@"type"];
    [muDic setValue:code forKey:@"verificationCode"];
    
  
    
    self.loginReq.dic = muDic.copy;
    WS(weakSelf)
    self.loginReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
      
        //记录登录状态
        if ([request.responseObject[@"code"] isEqualToString:@"0"]) {
             
            
            [QJAppTool showSuccessToast:@"登录成功"];
            NSDictionary *data = EncodeDicFromDic(request.responseObject, @"data");
            NSString *userId = EncodeStringFromDic(data, @"uid");
            LSUserDefaultsSET(EncodeStringFromDic(data, @"token"), kQJToken);
            LSUserDefaultsSET(userId, kQJUserId);
            LSUserDefaultsSET(EncodeNumberFromDic(data, @"verifyState"), kQJIsRealName);
            
            [QJUserManager shareManager].isLogin = YES;
            [QJUserManager shareManager].isNetLogout = NO;//登录成功状态改变
            [QJUserManager shareManager].isVerifyState = [EncodeNumberFromDic(data, @"verifyState") boolValue];
            [QJUserManager shareManager].userID = userId;
           
            
            [weakSelf dismissLoginVC:^{
                if (weakSelf.loginOrRegisterSuccessBlock) {
                    weakSelf.loginOrRegisterSuccessBlock(YES);
                }
            }];
        }else{
            NSString *msg = EncodeStringFromDic(request.responseObject, @"message");
            if (!IsStrEmpty(msg)) {
                
                [QJAppTool showToast:msg];
            }
            if (weakSelf.loginOrRegisterSuccessBlock) {
                weakSelf.loginOrRegisterSuccessBlock(NO);
            }
        }
        
        

        
    };
    
    self.loginReq.failureCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        
        [QJAppTool showFailToast:@"登录失败"];
        if (weakSelf.loginOrRegisterSuccessBlock) {
            weakSelf.loginOrRegisterSuccessBlock(NO);
        }
    };
    
    [self.loginReq start];
}


#pragma mark ----- 绑定手机
- (void)bindPhone:(NSString *)code {
    
    NSMutableDictionary *muDic = [NSMutableDictionary dictionary];
    [muDic setValue:self.mobile?:@"" forKey:@"mobile"];
    [muDic setValue:code forKey:@"verificationCode"];
    [muDic setValue:self.bindToken?:@"" forKey:@"token"];
    [muDic setValue:self.bindType?:@"" forKey:@"type"];
        
     
    
    self.bindPhoneReq.dic = muDic.copy;
    WS(weakSelf)
    self.bindPhoneReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
      
        //记录登录状态
        if ([request.responseObject[@"code"] isEqualToString:@"0"]) {
            [QJAppTool showSuccessToast:@"登录成功"];
            NSDictionary *data = EncodeDicFromDic(request.responseObject, @"data");
            NSString *userId = EncodeStringFromDic(data, @"uid");
            LSUserDefaultsSET(EncodeStringFromDic(data, @"token"), kQJToken);
            LSUserDefaultsSET(EncodeNumberFromDic(data, @"verifyState"), kQJIsRealName);
            LSUserDefaultsSET(userId, kQJUserId);
            [QJUserManager shareManager].isLogin = YES;
            [QJUserManager shareManager].isNetLogout = NO;//登录成功状态改变
            [QJUserManager shareManager].isVerifyState = [EncodeNumberFromDic(data, @"verifyState") boolValue];
            [QJUserManager shareManager].userID = userId;
            [weakSelf dismissLoginVC:^{
                if (weakSelf.loginOrRegisterSuccessBlock) {
                    weakSelf.loginOrRegisterSuccessBlock(YES);
                }
            }];
        }else{
            NSString *msg = EncodeStringFromDic(request.responseObject, @"message");
            if (!IsStrEmpty(msg)) {
                
                [QJAppTool showToast:msg];
            }
            if (weakSelf.loginOrRegisterSuccessBlock) {
                weakSelf.loginOrRegisterSuccessBlock(NO);
            }
        }
        
        

        
    };
    
    self.bindPhoneReq.failureCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        
        [QJAppTool showFailToast:ResponseFailToastMsg];
        if (weakSelf.loginOrRegisterSuccessBlock) {
            weakSelf.loginOrRegisterSuccessBlock(NO);
        }
    };
    
    [self.bindPhoneReq start];

    
}



#pragma mark --------- 校验验证码


- (void)makeSureCode:(NSString *)code {
    
    if (!self.makeSureCodeReq) {
        self.makeSureCodeReq = [QJMakeSureVerifyCodeRequest new];
    }
    WS(weakSelf)
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:code?:@"" forKey:@"code"];
    [dic setValue:self.mobile forKey:@"mobile"];
    NSString *type = @"UPDATE_PASSWORD";
    switch (self.type) {
        case QJGetVerifyCodeVCType_phoneLogin:
            type = @"LOGIN";
            break;
        case QJGetVerifyCodeVCType_bindingVC:
            type = @"BIND_PHONE";
            break;
        case QJGetVerifyCodeVCType_forgetVC:
            type = @"UPDATE_PASSWORD";
            break;
            
        default:
            break;
    }
    [dic setValue:type forKey:@"type"];
    
    self.makeSureCodeReq.dic = dic.copy;
    self.makeSureCodeReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        if ([request.responseObject[@"code"] isEqualToString:@"0"]) {
            QJSettingNewPassWordVC *vc = [QJSettingNewPassWordVC new];
            vc.mobile = weakSelf.mobile;
            vc.code = code;
            [weakSelf.navigationController pushViewController:vc animated:YES];
            
        }else{
            NSString *msg = EncodeStringFromDic(request.responseObject, @"message");
            [QJAppTool showToast:msg];
        }
    };
    
    
    self.makeSureCodeReq.failureCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        
        [QJAppTool showWarningToast:@"网络异常，稍后再试"];
    };
    
    
    [self.makeSureCodeReq start];
    
    
}


#pragma mark ---- QJImageVerifyCodeDelegate

/// 完成验证之后的回调
/// @param result 验证结果 BOOL:YES/NO
/// @param validate 二次校验数据，如果验证结果为false，validate返回空
/// @param message 结果描述信息
- (void)QJImageVerifyCodeValidateFinish:(BOOL)result validate:(NSString *)validate message:(NSString *)message {
    DLog(@"收到验证结果的回调:(%d,%@,%@)", result, validate, message);
    if (result) {
        
        [QJUserManager shareManager].validate = validate;
        [self getVerifyCode];
        
        
    }
  
}

@end
