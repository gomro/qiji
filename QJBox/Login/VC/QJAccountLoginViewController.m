//
//  QJAccountLoginViewController.m
//  QJBox
//
//  Created by wxy on 2022/6/27.
//

#import "QJAccountLoginViewController.h"
#import "QJLoginBgView.h"
#import "QJFindWordViewController.h"
#import "QJLoginAppRequest.h"
#import "QJImageVerifyCode.h"
@interface QJAccountLoginViewController ()<UITextFieldDelegate,QJImageVerifyCodeDelegate>
{
    NSString    *previousTextFieldContent;
    UITextRange *previousSelection;
    
}

@property (nonatomic, strong) QJLoginBgView *bgView;

@property (nonatomic, strong) UIImageView *userIconImageView;

@property (nonatomic, strong) UITextField *userTFView;//手机号输入

@property (nonatomic, strong) UIImageView *passWIconImageView;

@property (nonatomic, strong) UITextField *passWTFView;//登录密码

@property (nonatomic, strong) UIButton *eyeBtn;

@property (nonatomic, strong) UILabel *forgetLabel;//忘记密码

@property (nonatomic, strong) UIButton *forgetBtn;//忘记密码按钮

@property (nonatomic, strong) UIView *passLine;

@property (nonatomic, strong) UIView *phoneLine;

@property (nonatomic, strong) QJLoginBottomBtn *loginBtn;//登录按钮

@property (nonatomic, strong) QJLoginAppRequest *loginReq;//登录接口

@property (nonatomic, assign) BOOL isNeedSliderValidation;//是否需要滑块验证

@property (nonatomic, strong) QJImageVerifyCode *imageVerifyCode;

@property (nonatomic, assign) NSInteger animationCurve;

@property (nonatomic, assign) NSInteger animationDuration;

@property (nonatomic, assign) CGFloat backBtnY;

@end

@implementation QJAccountLoginViewController
 
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
   
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.backBtnY = self.navBar.origin.y;
    self.navBar.backgroundColor = [UIColor clearColor];
    [self setUI];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHidden:) name:UIKeyboardWillHideNotification object:nil];
}


#pragma mark  -------- method
- (void)keyboardWillShow:(NSNotification *)aNotification {
    NSInteger curve = [[aNotification userInfo][UIKeyboardAnimationCurveUserInfoKey] integerValue];
    _animationCurve = curve<<16;

    //  Getting keyboard animation duration
    CGFloat duration = [[aNotification userInfo][UIKeyboardAnimationDurationUserInfoKey] floatValue];
    
    //Saving animation duration
    if (duration != 0.0)    _animationDuration = duration;
    [IQKeyboardManager sharedManager].keyboardDistanceFromTextField = 110;
    [IQKeyboardManager sharedManager].movedDistanceChanged = ^(CGFloat movedDistance) {
        [UIView animateWithDuration:self.animationDuration delay:0 options:(self.animationCurve|UIViewAnimationOptionBeginFromCurrentState) animations:^{
            [self.bgView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.height.left.right.equalTo(self.view);
                make.top.equalTo(self.view).offset(movedDistance);
                
            }];
            CGFloat y = self.backBtnY;
            y = y +movedDistance;
            CGPoint origin = CGPointMake(self.navBar.origin.x, y);
            self.navBar.origin = origin;
            self.bgView.logoImageView.hidden = YES;
            [self.view layoutIfNeeded];
        } completion:^(BOOL finished) {
             
        }];
    };
}

- (void)keyboardWillHidden:(NSNotification *)notification {
    self.bgView.logoImageView.hidden = NO;
    CGPoint origin = CGPointMake(self.navBar.origin.x, self.backBtnY);
    self.navBar.origin = origin;
}


//返回按钮响应
- (void)onBack:(id)sender {
    UIViewController *vc = self.navigationController.viewControllers.firstObject;
    [vc dismissViewControllerAnimated:YES completion:nil];
    
 
}


- (void)setUI {
    
    [self.view addSubview:self.bgView];
    [self.view addSubview:self.userIconImageView];
    [self.view addSubview:self.userTFView];
    [self.view addSubview:self.passWIconImageView];
    [self.view addSubview:self.passWTFView];
    [self.view addSubview:self.forgetLabel];
    [self.view addSubview:self.forgetBtn];
    [self.view addSubview:self.loginBtn];
    [self.view addSubview:self.eyeBtn];
    [self.view addSubview:self.passLine];
    [self.view addSubview:self.phoneLine];
    
    [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.left.right.equalTo(self.view);
    }];
    
    [self.loginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.view).offset(-80 - Bottom_iPhoneX_SPACE);
        make.left.equalTo(self.view).offset(39);
        make.right.equalTo(self.view).offset(-36);
        make.height.equalTo(@48);
    }];
    
    [self.forgetLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.view).offset(-43);
            make.top.equalTo(self.loginBtn.mas_bottom).offset(24);
            
    }];
    
    [self.forgetBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(self.forgetLabel);
            make.width.equalTo(@80);
            make.height.equalTo(@30);
    }];
    
    [self.passWIconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.loginBtn.mas_top).offset(-44);
            make.left.equalTo(self.view).offset(34);
            make.width.height.equalTo(@24);
    }];
    
    [self.passWTFView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.passWIconImageView.mas_right).offset(6);
            make.height.equalTo(@32);
            make.centerY.equalTo(self.passWIconImageView);
            make.right.equalTo(self.eyeBtn.mas_left);
    }];
    
    [self.userIconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.passWIconImageView);
        make.width.height.equalTo(@24);
        make.bottom.equalTo(self.loginBtn.mas_top).offset(-92);
    }];
    
    [self.userTFView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.userIconImageView);
            make.height.equalTo(@32);
            make.left.equalTo(self.userIconImageView.mas_right).offset(6);
            make.right.equalTo(self.passWTFView);
    }];
    
    [self.eyeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.height.equalTo(@16);
            make.right.equalTo(self.view).offset(-44);
            make.bottom.equalTo(self.loginBtn.mas_top).offset(-48);
    }];
    
    [self.passLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.passWTFView);
            make.height.equalTo(@1);
            make.right.equalTo(self.eyeBtn);
            make.top.equalTo(self.passWTFView.mas_bottom);
    }];
    
    [self.phoneLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.passWTFView);
            make.height.equalTo(@1);
            make.right.equalTo(self.eyeBtn);
            make.top.equalTo(self.userTFView.mas_bottom);
    }];
    
}



#pragma mark  --------- getter

- (QJLoginBgView *)bgView {
    if (!_bgView) {
        _bgView = [QJLoginBgView new];
    }
    return _bgView;
}

- (UITextField *)userTFView {
    if (!_userTFView) {
        _userTFView = [UITextField new];
        _userTFView.delegate = self;
        NSAttributedString *attStr = [[NSAttributedString alloc]initWithString:@"请输入手机号" attributes:@{NSFontAttributeName:FontRegular(14),NSForegroundColorAttributeName:[UIColor colorWithHexString:@"#919599"]}];
        _userTFView.attributedPlaceholder = attStr;
        _userTFView.font = kFont(14);
        _userTFView.clearButtonMode = UITextFieldViewModeWhileEditing;
        _userTFView.keyboardType = UIKeyboardTypeNumberPad;
        [_userTFView addTarget:self action:@selector(formatPhoneNumber:) forControlEvents:UIControlEventEditingChanged];
    }
    return _userTFView;
}

-(UIImageView *)userIconImageView {
    if (!_userIconImageView) {
        _userIconImageView = [UIImageView new];
        _userIconImageView.image = [UIImage imageNamed:@"phoneiconPhone"];
    }
    return _userIconImageView;
}

- (UITextField *)passWTFView {
    if (!_passWTFView) {
        _passWTFView = [UITextField new];
        NSAttributedString *attStr = [[NSAttributedString alloc]initWithString:@"请输入密码" attributes:@{NSFontAttributeName:FontRegular(14),NSForegroundColorAttributeName:[UIColor colorWithHexString:@"#919599"]}];
        _passWTFView.attributedPlaceholder = attStr;
        _passWTFView.font = kFont(14);
        _passWTFView.secureTextEntry = YES;
    }
    return _passWTFView;
}

- (UIImageView *)passWIconImageView {
    if (!_passWIconImageView) {
        _passWIconImageView = [UIImageView new];
        _passWIconImageView.image = [UIImage imageNamed:@"loginLock"];
    }
    return _passWIconImageView;
}

- (UIButton *)forgetBtn {
    if (!_forgetBtn) {
        _forgetBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _forgetBtn.backgroundColor = [UIColor clearColor];
        [_forgetBtn addTarget:self action:@selector(forgetBtnAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _forgetBtn;
}

- (UILabel *)forgetLabel {
    if (!_forgetLabel) {
        _forgetLabel = [UILabel new];
        _forgetLabel.font = FontRegular(12);
        _forgetLabel.textColor = [UIColor colorWithHexString:@"#474849"];
        NSMutableAttributedString *title = [[NSMutableAttributedString alloc] initWithString:@"忘记密码"];
        NSRange titleRange = {0,[title length]};
        [title addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:titleRange];
        _forgetLabel.attributedText = title;
        _forgetLabel.textAlignment = NSTextAlignmentRight;
    }
    return _forgetLabel;
}


- (QJLoginBottomBtn *)loginBtn {
    if (!_loginBtn) {
        _loginBtn = [QJLoginBottomBtn new];
        _loginBtn.title = @"登录";
        WS(weakSelf)
        _loginBtn.btnBlock = ^{
            DLog(@"登录");
            if (IsStrEmpty(weakSelf.userTFView.text)) {
                
                [QJAppTool showWarningToast:@"请先输入登录手机号"];

                return;
            }
            
            if (IsStrEmpty(weakSelf.passWTFView.text)) {
                
                [QJAppTool showWarningToast:@"请输入登录密码"];

                return;
            }
            [weakSelf loginNetWork];
         
        };
    }
    return _loginBtn;
}

- (UIButton *)eyeBtn {
    if (!_eyeBtn) {
        _eyeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_eyeBtn addTarget:self action:@selector(eyeAction:) forControlEvents:UIControlEventTouchUpInside];
        [_eyeBtn setImage:[UIImage imageNamed:@"preview-closelogin"] forState:UIControlStateNormal];
        [_eyeBtn setImage:[UIImage imageNamed:@"previewlogin"] forState:UIControlStateSelected];
        
    }
    return _eyeBtn;
}


- (QJLoginAppRequest *)loginReq {
    if (!_loginReq) {
        _loginReq = [QJLoginAppRequest new];
        
    }
    return _loginReq;
}

- (UIView *)passLine {
    if (!_passLine) {
        _passLine = [UIView new];
        _passLine.backgroundColor = [UIColor colorWithHexString:@"#919599"];
    }
    return _passLine;
}

- (UIView *)phoneLine {
    if (!_phoneLine) {
        _phoneLine = [UIView new];
        _phoneLine.backgroundColor = [UIColor colorWithHexString:@"#919599"];
    }
    return _phoneLine;
}
#pragma mark ------登录

- (void)loginNetWork {
    WS(weakSelf)
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:[self.userTFView.text stringByReplacingOccurrencesOfString:@" " withString:@""]  forKey:@"mobile"];
    [dic setValue:[self.passWTFView.text stringByReplacingOccurrencesOfString:@" " withString:@""] forKey:@"password"];
    
    [dic setValue:@"P" forKey:@"type"];
    self.loginReq.dic = dic.copy;
    self.loginReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
      
        //记录登录状态
        if ([request.responseObject[@"code"] isEqualToString:@"0"]) {
            
            
            [QJAppTool showSuccessToast:@"登录成功"];
            NSDictionary *data = EncodeDicFromDic(request.responseObject, @"data");
            NSString *userId = EncodeStringFromDic(data, @"uid");
            LSUserDefaultsSET(EncodeStringFromDic(data, @"token"), kQJToken);
            LSUserDefaultsSET(EncodeNumberFromDic(data, @"verifyState"), kQJIsRealName);
            LSUserDefaultsSET(userId, kQJUserId);
            [QJUserManager shareManager].isLogin = YES;
            [QJUserManager shareManager].isNetLogout = NO;//登录成功状态改变
            [QJUserManager shareManager].isVerifyState = [EncodeNumberFromDic(data, @"verifyState") boolValue];
            [QJUserManager shareManager].userID = userId;
            [weakSelf dismissLoginVC:^{
                if (weakSelf.loginOrRegisterSuccessBlock) {
                    weakSelf.loginOrRegisterSuccessBlock(YES);
                }
            }];
        }else if ([EncodeStringFromDic(request.responseObject, @"code") isEqualToString:@"501"]) {//出滑块
            //滑块
            [weakSelf showImageCode];
            
        }
        else{
            NSString *msg = EncodeStringFromDic(request.responseObject, @"message");
            if (!IsStrEmpty(msg)) {
                
                [QJAppTool showToast:msg];
            }
            
            if (weakSelf.loginOrRegisterSuccessBlock) {
                weakSelf.loginOrRegisterSuccessBlock(NO);
            }
        }
        
        
    };
    self.loginReq.failureCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        
        [QJAppTool showWarningToast:@"网络异常，稍后再试"];
        
    };
    [self.loginReq start];
}

#pragma mark -------- 展示滑块

- (void)showImageCode {
    self.imageVerifyCode = [QJImageVerifyCode new];
    self.imageVerifyCode.delegate = self;
    [self.imageVerifyCode showView];
}

#pragma mark ---- QJImageVerifyCodeDelegate

/// 完成验证之后的回调
/// @param result 验证结果 BOOL:YES/NO
/// @param validate 二次校验数据，如果验证结果为false，validate返回空
/// @param message 结果描述信息
- (void)QJImageVerifyCodeValidateFinish:(BOOL)result validate:(NSString *)validate message:(NSString *)message {
    DLog(@"收到验证结果的回调:(%d,%@,%@)", result, validate, message);
    WS(weakSelf)
    if (result) {
        [QJUserManager shareManager].validate = validate;
        [weakSelf loginNetWork];
    }
  
}


#pragma mark -------- action

//是否显示明文密码
- (void)eyeAction:(UIButton *)sender {
    self.passWTFView.secureTextEntry = sender.selected;
    sender.selected = !sender.selected;
    
}

- (void)forgetBtnAction {
    DLog(@"忘记密码");
    QJFindWordViewController *vc = [QJFindWordViewController new];
    vc.type = QJGetVerifyCodeVCType_forgetVC;
    [self.navigationController pushViewController:vc animated:YES];
}


#pragma mark ------ 手机号输入控制

- (void)formatPhoneNumber:(UITextField*)textField{
    NSUInteger targetCursorPosition = [textField offsetFromPosition:textField.beginningOfDocument toPosition:textField.selectedTextRange.start];
    NSLog(@"targetCursorPosition:%li", (long)targetCursorPosition);
    NSString* nStr = [textField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString* preTxt = [previousTextFieldContent stringByReplacingOccurrencesOfString:@" " withString:@""];
    char editFlag = 0;// 正在执行删除操作时为0，否则为1
    if (nStr.length <= preTxt.length) {
        editFlag = 0;
    }else{
        
        editFlag = 1;
    }
    if (nStr.length > 11) {
        textField.text = previousTextFieldContent;
        textField.selectedTextRange = previousSelection;
        return;
    }
    NSString* spaceStr = @" ";
    NSMutableString *mStrTemp = [NSMutableString string];
    int spaceCount = 0;
    if (nStr.length < 3 && nStr.length > -1) {
        spaceCount = 0;
    }else if (nStr.length < 7&& nStr.length > 2){
        spaceCount = 1;
    }else if (nStr.length < 12&& nStr.length > 6){
        spaceCount = 2;
    }
    for (int i = 0; i < spaceCount; i++)
    {
        if (i == 0) {
            [mStrTemp appendFormat:@"%@%@", [nStr substringWithRange:NSMakeRange(0, 3)],spaceStr];
        }else if (i == 1)
        {
            [mStrTemp appendFormat:@"%@%@", [nStr substringWithRange:NSMakeRange(3, 4)], spaceStr];
        }else if (i == 2)
        {
            [mStrTemp appendFormat:@"%@%@", [nStr substringWithRange:NSMakeRange(7, 4)], spaceStr];
        }
    }
    
    if (nStr.length == 11)
    {
        [mStrTemp appendFormat:@"%@%@", [nStr substringWithRange:NSMakeRange(7, 4)], spaceStr];
    }
    
    if (nStr.length < 4 && nStr.length > 0)
    {
        [mStrTemp appendString:[nStr substringWithRange:NSMakeRange(nStr.length-nStr.length % 3,nStr.length % 3)]];
    }else if(nStr.length > 3)
    {
        NSString *str = [nStr substringFromIndex:3];
        [mStrTemp appendString:[str substringWithRange:NSMakeRange(str.length-str.length % 4,str.length % 4)]];
        if (nStr.length == 11)
        {
            [mStrTemp deleteCharactersInRange:NSMakeRange(13, 1)];
        }
    }
    NSLog(@"=======mstrTemp=%@",mStrTemp);
    textField.text = mStrTemp;
    // textField设置selectedTextRange
    NSUInteger curTargetCursorPosition = targetCursorPosition;// 当前光标的偏移位置
    if (editFlag == 0){
        //删除
        if (targetCursorPosition == 9 || targetCursorPosition == 4)
        {
            curTargetCursorPosition = targetCursorPosition - 1;
        }
    }
    else {
        //添加
        if (nStr.length == 8 || nStr.length == 4)
        {
            curTargetCursorPosition = targetCursorPosition + 1;
        }
    }
    
    UITextPosition *targetPosition = [textField positionFromPosition:[textField beginningOfDocument] offset:curTargetCursorPosition];
    [textField setSelectedTextRange:[textField textRangeFromPosition:targetPosition toPosition :targetPosition]];
}

#pragma mark UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    previousTextFieldContent = textField.text;
    previousSelection = textField.selectedTextRange;
    if (range.location == 0)
    {
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:@"1"] invertedSet];
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        BOOL basicTest = [string isEqualToString:filtered];
        if (!basicTest)
        {
            
            return NO;
        }
    }
    
    if (range.location > 12)
    {
        return NO;
    }
    return YES;
}


@end
