//
//  QJFindWordViewController.h
//  QJBox
//
//  Created by wxy on 2022/6/13.
//

#import "QJBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN


typedef NS_ENUM(NSUInteger, QJGetVerifyCodeVCType) {
    QJGetVerifyCodeVCType_None,
    QJGetVerifyCodeVCType_forgetVC,
    QJGetVerifyCodeVCType_bindingVC,
    QJGetVerifyCodeVCType_phoneLogin,//手机号登录
    
};



/// 找回密码VC
@interface QJFindWordViewController : QJBaseViewController


@property (nonatomic, assign) QJGetVerifyCodeVCType type;

@property (nonatomic, copy) LoginOrRegisterSuccessBlock loginOrRegisterSuccessBlock;

/// 是否隐藏系统的导航栏
@property (nonatomic, assign) BOOL isHiddenNavgationBar;

@property (nonatomic, copy) NSString *bindToken;//绑定手机号使用的token

@property (nonatomic, copy) NSString *bindType;

@end

NS_ASSUME_NONNULL_END
