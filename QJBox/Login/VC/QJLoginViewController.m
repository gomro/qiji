//
//  QJLoginViewController.m
//  QJBox
//
//  Created by wxy on 2022/6/7.
//

#import "QJLoginViewController.h"
#import <AuthenticationServices/AuthenticationServices.h>
#import "QJSignInWithApple.h"
#import "QJLoginWithPenguin.h"
#import "QJLoginAppRequest.h"
#import "QJRegisterViewController.h"
#import "QJLoginBgView.h"
#import "QJThirdLoginViewController.h"
#import "QJVerifyCodeViewController.h"
#import "QJGetVerifyCodeRequest.h"
#import "QJImageVerifyCode.h"



@interface QJLoginViewController ()<UITextFieldDelegate,QJImageVerifyCodeDelegate>
{
    NSString    *previousTextFieldContent;
    UITextRange *previousSelection;
    
}

@property (nonatomic, strong) QJLoginBgView *bgView;

@property (nonatomic, strong) UIImageView *userIconImageView;

@property (nonatomic, strong) UITextField *userTFView;//手机号输入

@property (nonatomic, strong) UIView *userLineView;

@property (nonatomic, strong) UIButton *checkBtn;

@property (nonatomic, strong) YYLabel *userAgreementLabel;//用户协议

@property (nonatomic, strong) QJLoginBottomBtn *submitBtn;//提交订单按钮

@property (nonatomic, strong) UILabel *thridLoginLabel;

@property (nonatomic, strong) UIButton *thridLoginBtn;

@property (nonatomic, strong) UILabel *centerPhoneLabel;

@property (nonatomic, strong) QJLoginAppRequest *loginReq;//登录接口

@property (nonatomic, strong) QJSignInWithApple *apple;

@property (nonatomic, strong) QJGetVerifyCodeRequest *getVerifyCodeReq;//

@property (nonatomic, strong) UILabel *vLine;

@property (nonatomic, strong) UIImageView *tipImageView;

@property (nonatomic, strong) QJImageVerifyCode *imageVerifyCode;

@property (nonatomic, assign) NSInteger animationCurve;

@property (nonatomic, assign) NSInteger animationDuration;

@property (nonatomic, assign) CGFloat backBtnY;
@end

@implementation QJLoginViewController

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [QJUserManager shareManager].isNetLogout = NO;
    
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
   
}

- (void)viewDidLoad {
    [super viewDidLoad];
     self.backBtnY = self.navBar.origin.y;
    [IQKeyboardManager sharedManager].keyboardDistanceFromTextField = 110;
    self.navBar.backgroundColor = [UIColor clearColor];
    [self setUpSubViews];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHidden:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWillShow:(NSNotification *)aNotification {
    NSInteger curve = [[aNotification userInfo][UIKeyboardAnimationCurveUserInfoKey] integerValue];
    _animationCurve = curve<<16;

    //  Getting keyboard animation duration
    CGFloat duration = [[aNotification userInfo][UIKeyboardAnimationDurationUserInfoKey] floatValue];
    
    //Saving animation duration
    if (duration != 0.0)    _animationDuration = duration;
    [IQKeyboardManager sharedManager].keyboardDistanceFromTextField = 110;
    [IQKeyboardManager sharedManager].movedDistanceChanged = ^(CGFloat movedDistance) {
        [UIView animateWithDuration:self.animationDuration delay:0 options:(self.animationCurve|UIViewAnimationOptionBeginFromCurrentState) animations:^{
            [self.bgView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.height.left.right.equalTo(self.view);
                make.top.equalTo(self.view).offset(movedDistance);

            }];
            CGFloat y = self.backBtnY;
            y = y +movedDistance;
            CGPoint origin = CGPointMake(self.navBar.origin.x, y);
            self.navBar.origin = origin;
            self.bgView.logoImageView.hidden = YES;
            [self.view layoutIfNeeded];
        } completion:^(BOOL finished) {

        }];
    };
}

- (void)keyboardWillHidden:(NSNotification *)notification {
    self.bgView.logoImageView.hidden = NO;
    CGPoint origin = CGPointMake(self.navBar.origin.x, self.backBtnY);
    self.navBar.origin = origin;
}
#pragma mark ------ setupUI

- (void)setUpSubViews {
    
    [self.view addSubview:self.bgView];
    [self.bgView addSubview:self.userAgreementLabel];
    [self.bgView addSubview:self.checkBtn];
    [self.bgView addSubview:self.tipImageView];
    
    [self.tipImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.checkBtn.mas_top).offset(-8.5);
        make.width.equalTo(@(100*kWScale));
        make.height.equalTo(@(27.5*kWScale));
        make.left.equalTo(self.checkBtn).offset(-11.5);
    }];
    
    
   
        self.submitBtn.title = @"获取验证码";
        [self.view addSubview:self.userTFView];
        [self.view addSubview:self.userLineView];
        [self.view addSubview:self.userIconImageView];
     
    
    [self.bgView addSubview:self.thridLoginBtn];
    [self.bgView addSubview:self.thridLoginLabel];
    [self.view addSubview:self.submitBtn];
    
    [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.left.right.equalTo(self.view);
    }];
    
    [self.checkBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.submitBtn);
        make.width.height.equalTo(@20);
        make.top.equalTo(self.userAgreementLabel).offset(-3);
    }];
    
    [self.userAgreementLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.checkBtn.mas_right).offset(2);
        make.width.equalTo(@282);
        if(iPhoneX){
            make.bottom.equalTo(self.bgView.mas_bottom).offset(-Bottom_iPhoneX_SPACE);
        }else{
            make.bottom.equalTo(self.bgView.mas_bottom).offset(-16);
        }
    }];
  
    [self.thridLoginLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.bgView.mas_centerX).offset(40.5);
        make.width.equalTo(@110);
        make.bottom.equalTo(self.checkBtn.mas_top).offset(-50);
    }];
    
    [self.thridLoginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.thridLoginLabel);
        make.height.equalTo(@20);
        make.centerY.equalTo(self.thridLoginLabel);
    }];
    
    [self.submitBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        
        if(iPhoneX){
            make.bottom.equalTo(self.view).offset(-(105+Bottom_iPhoneX_SPACE));
        }else{
            make.bottom.equalTo(self.view).offset(-(105+Bottom_iPhoneX_SPACE+16));
        }
        make.centerX.equalTo(self.view);
        make.width.equalTo(@300);
        make.height.equalTo(@48);
    }];
    

        [self.userIconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.userLineView.mas_top).offset(-2);
            make.left.equalTo(self.view).offset(38.5);
            make.width.equalTo(@24);
            make.height.equalTo(@24);
        }];
        
        [self.userTFView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.userIconImageView.mas_right).offset(8);
            make.right.equalTo(self.view).offset(-43);
            make.height.equalTo(@32);
            make.bottom.equalTo(self.userLineView.mas_top).offset(0);
        }];
        
        [self.userLineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.userIconImageView.mas_right).offset(8);
            make.right.equalTo(self.view).offset(-43);
            make.bottom.equalTo(self.self.submitBtn.mas_top).offset(-23);
            make.height.equalTo(@1);
        }];
        
     
    

}





#pragma mark ----- getter

- (UIImageView *)tipImageView {
    if (!_tipImageView) {
        _tipImageView = [[UIImageView alloc]init];
        _tipImageView.image = [UIImage imageNamed:@"agreementTips"];
        
    }
    return _tipImageView;
}

- (UILabel *)vLine {
    if (!_vLine) {
        _vLine = [UILabel new];
        _vLine.backgroundColor = [UIColor colorWithHexString:@"#919599"];
    }
    return _vLine;
}

- (QJLoginBgView *)bgView {
    if (!_bgView) {
        _bgView = [QJLoginBgView new];
    }
    return _bgView;
}

- (UIImageView *)userIconImageView {
    if (!_userIconImageView) {
        _userIconImageView = [UIImageView new];
        _userIconImageView.image = [UIImage imageNamed:@"phoneiconPhone"];
        
    }
    return _userIconImageView;
}

 

- (UILabel *)thridLoginLabel {
    if (!_thridLoginLabel) {
        _thridLoginLabel = [UILabel new];
        _thridLoginLabel.font = kFont(14);
        _thridLoginLabel.textColor = [UIColor grayColor];
        _thridLoginLabel.text = @"其他方式登录";
        _thridLoginLabel.textAlignment = NSTextAlignmentLeft;
    }
    return _thridLoginLabel;
}

- (UIButton *)thridLoginBtn {
    if (!_thridLoginBtn) {
        _thridLoginBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_thridLoginBtn addTarget:self action:@selector(thridLoginBtnAction) forControlEvents:UIControlEventTouchUpInside];
        _thridLoginBtn.backgroundColor = [UIColor clearColor];
    }
    return _thridLoginBtn;
}

 

- (QJLoginBottomBtn *)submitBtn {
    if (!_submitBtn) {
        _submitBtn = [QJLoginBottomBtn new];
        WS(weakSelf)
        _submitBtn.btnBlock = ^{
        
                //获取验证码
                [weakSelf getVerifyCode];
           
            
        };
        
    }
    return _submitBtn;
}


- (UITextField *)userTFView {
    if (!_userTFView) {
        _userTFView = [UITextField new];
        _userTFView.font = kFont(14);
        NSAttributedString *attStr = [[NSAttributedString alloc]initWithString:@"请输入手机号" attributes:@{NSFontAttributeName:FontRegular(14),NSForegroundColorAttributeName:[UIColor colorWithHexString:@"#919599"]}];
        _userTFView.attributedPlaceholder = attStr;
        _userTFView.clearButtonMode = UITextFieldViewModeWhileEditing;
        _userTFView.keyboardType = UIKeyboardTypeNumberPad;
        [_userTFView addTarget:self action:@selector(formatPhoneNumber:) forControlEvents:UIControlEventEditingChanged];
        _userTFView.delegate = self;
    }
    return _userTFView;
}



- (UIButton *)checkBtn {
    if (!_checkBtn) {
        _checkBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_checkBtn setImage:[UIImage imageNamed:@"unSelectedAgreementIcon"] forState:UIControlStateNormal];
        [_checkBtn setEnlargeEdgeWithTop:10 right:10 bottom:10 left:10];
        [_checkBtn addTarget:self action:@selector(btnAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _checkBtn;
}


- (YYLabel *)userAgreementLabel {
    if (!_userAgreementLabel) {
        WS(weakSelf)
        _userAgreementLabel = [[YYLabel alloc]init];
        _userAgreementLabel.textAlignment = NSTextAlignmentLeft;
        NSString *str = @"同意《用户授权协议》和《隐私条款》";
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc]initWithString:str];
        attributedString.color = [UIColor colorWithHexString:@"#919599"];
        attributedString.font = kFont(12);
        NSRange firstRange = [str rangeOfString:@"《用户授权协议》"];
        NSRange secondRange = [str rangeOfString:@"《隐私条款》"];
        [attributedString setTextHighlightRange:firstRange color:[UIColor colorWithHexString:@"#000000"] backgroundColor:[UIColor clearColor] tapAction:^(UIView * _Nonnull containerView, NSAttributedString * _Nonnull text, NSRange range, CGRect rect) {
            DLog(@"点击用户授权协议");
            QJWKWebViewController *vc = [QJWKWebViewController new];
            vc.url = [QJUserManager shareManager].userProtocol;
            [weakSelf.navigationController pushViewController:vc animated:YES];
        }];
        
        [attributedString setTextHighlightRange:secondRange color:[UIColor colorWithHexString:@"#000000"] backgroundColor:[UIColor clearColor] tapAction:^(UIView * _Nonnull containerView, NSAttributedString * _Nonnull text, NSRange range, CGRect rect) {
            DLog(@"点击隐私条款");
            QJWKWebViewController *vc = [QJWKWebViewController new];
            vc.url = [QJUserManager shareManager].userPolicy;
            [weakSelf.navigationController pushViewController:vc animated:YES];
            
        }];
        _userAgreementLabel.attributedText = attributedString;
        _userAgreementLabel.numberOfLines = 0;
    }
    return _userAgreementLabel;
}


- (UIView *)userLineView {
    if (!_userLineView) {
        _userLineView = [UIView new];
        _userLineView.backgroundColor = [UIColor colorWithHexString:@"#919599"];
    }
    return _userLineView;
}

- (void)formatPhoneNumber:(UITextField*)textField{
    NSUInteger targetCursorPosition = [textField offsetFromPosition:textField.beginningOfDocument toPosition:textField.selectedTextRange.start];
    NSLog(@"targetCursorPosition:%li", (long)targetCursorPosition);
    NSString* nStr = [textField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString* preTxt = [previousTextFieldContent stringByReplacingOccurrencesOfString:@" " withString:@""];
    char editFlag = 0;// 正在执行删除操作时为0，否则为1
    if (nStr.length <= preTxt.length) {
        editFlag = 0;
    }else{
        
        editFlag = 1;
    }
    if (nStr.length > 11) {
        textField.text = previousTextFieldContent;
        textField.selectedTextRange = previousSelection;
        return;
    }
    NSString* spaceStr = @" ";
    NSMutableString *mStrTemp = [NSMutableString string];
    int spaceCount = 0;
    if (nStr.length < 3 && nStr.length > -1) {
        spaceCount = 0;
    }else if (nStr.length < 7&& nStr.length > 2){
        spaceCount = 1;
    }else if (nStr.length < 12&& nStr.length > 6){
        spaceCount = 2;
    }
    for (int i = 0; i < spaceCount; i++)
    {
        if (i == 0) {
            [mStrTemp appendFormat:@"%@%@", [nStr substringWithRange:NSMakeRange(0, 3)],spaceStr];
        }else if (i == 1)
        {
            [mStrTemp appendFormat:@"%@%@", [nStr substringWithRange:NSMakeRange(3, 4)], spaceStr];
        }else if (i == 2)
        {
            [mStrTemp appendFormat:@"%@%@", [nStr substringWithRange:NSMakeRange(7, 4)], spaceStr];
        }
    }
    
    if (nStr.length == 11)
    {
        [mStrTemp appendFormat:@"%@%@", [nStr substringWithRange:NSMakeRange(7, 4)], spaceStr];
    }
    
    if (nStr.length < 4 && nStr.length > 0)
    {
        [mStrTemp appendString:[nStr substringWithRange:NSMakeRange(nStr.length-nStr.length % 3,nStr.length % 3)]];
    }else if(nStr.length > 3)
    {
        NSString *str = [nStr substringFromIndex:3];
        [mStrTemp appendString:[str substringWithRange:NSMakeRange(str.length-str.length % 4,str.length % 4)]];
        if (nStr.length == 11)
        {
            [mStrTemp deleteCharactersInRange:NSMakeRange(13, 1)];
        }
    }
    NSLog(@"=======mstrTemp=%@",mStrTemp);
    textField.text = mStrTemp;
    // textField设置selectedTextRange
    NSUInteger curTargetCursorPosition = targetCursorPosition;// 当前光标的偏移位置
    if (editFlag == 0){
        //删除
        if (targetCursorPosition == 9 || targetCursorPosition == 4)
        {
            curTargetCursorPosition = targetCursorPosition - 1;
        }
    }
    else {
        //添加
        if (nStr.length == 8 || nStr.length == 4)
        {
            curTargetCursorPosition = targetCursorPosition + 1;
        }
    }
    
    UITextPosition *targetPosition = [textField positionFromPosition:[textField beginningOfDocument] offset:curTargetCursorPosition];
    [textField setSelectedTextRange:[textField textRangeFromPosition:targetPosition toPosition :targetPosition]];
}


 

#pragma mark UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([[textField.textInputMode primaryLanguage] isEqualToString:@"emoji"] || ![textField.textInputMode primaryLanguage]) {
        return NO;
    }
    //判断键盘是不是九宫格键盘
    if ([self isNineKeyBoard:string] ){
        return YES;
    }else{
        if ([self hasEmoji:string] || [self isContainsTwoEmoji:string]){
            return NO;
        }
    }
    
    previousTextFieldContent = textField.text;
    previousSelection = textField.selectedTextRange;
    if (range.location == 0)
    {
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:@"1"] invertedSet];
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        BOOL basicTest = [string isEqualToString:filtered];
        if (!basicTest)
        {
            
            return NO;
        }
    }
    
    if (range.location > 12)
    {
        return NO;
    }
    return YES;
}


- (BOOL)isContainsTwoEmoji:(NSString *)string {
    __block BOOL isEomji = NO;
    [string enumerateSubstringsInRange:NSMakeRange(0, [string length]) options:NSStringEnumerationByComposedCharacterSequences usingBlock:
     ^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop) {
        const unichar hs = [substring characterAtIndex:0];
        //     NSLog(@"hs++++++++%04x",hs);
        if (0xd800 <= hs && hs <= 0xdbff) {
            if (substring.length > 1) {
                const unichar ls = [substring characterAtIndex:1];
                const int uc = ((hs - 0xd800) * 0x400) + (ls - 0xdc00) + 0x10000;
                if (0x1d000 <= uc && uc <= 0x1f77f)
                {
                    isEomji = YES;
                }
                //         NSLog(@"uc++++++++%04x",uc);
            }
        } else if (substring.length > 1) {
            const unichar ls = [substring characterAtIndex:1];
            if (ls == 0x20e3|| ls ==0xfe0f) {
                isEomji = YES;
            }
            //       NSLog(@"ls++++++++%04x",ls);
        } else {
            if (0x2100 <= hs && hs <= 0x27ff && hs != 0x263b) {
                isEomji = YES;
            } else if (0x2B05 <= hs && hs <= 0x2b07) {
                isEomji = YES;
            } else if (0x2934 <= hs && hs <= 0x2935) {
                isEomji = YES;
            } else if (0x3297 <= hs && hs <= 0x3299) {
                isEomji = YES;
            } else if (hs == 0xa9 || hs == 0xae || hs == 0x303d || hs == 0x3030 || hs == 0x2b55 || hs == 0x2b1c || hs == 0x2b1b || hs == 0x2b50|| hs == 0x231a ) {
                isEomji = YES;
            }
        }
    }];
    return isEomji;
}

//是否是系统自带九宫格输入 yes-是 no-不是
- (BOOL)isNineKeyBoard:(NSString *)string {
    NSString *other = @"➋➌➍➎➏➐➑➒";
    int len = (int)string.length;
    for(int i=0;i<len;i++){
        if(!([other rangeOfString:string].location != NSNotFound))
            return NO;
    }
    return YES;
}
//判断第三方键盘中的表情
- (BOOL)hasEmoji:(NSString*)string {
    NSString *pattern = @"[^\\u0020-\\u007E\\u00A0-\\u00BE\\u2E80-\\uA4CF\\uF900-\\uFAFF\\uFE30-\\uFE4F\\uFF00-\\uFFEF\\u0080-\\u009F\\u2000-\\u201f\r\n]";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", pattern];
    BOOL isMatch = [pred evaluateWithObject:string];
    return isMatch;
}

- (QJLoginAppRequest *)loginReq {
    if (!_loginReq) {
        _loginReq = [QJLoginAppRequest new];
    }
    return _loginReq;
}

- (QJGetVerifyCodeRequest *)getVerifyCodeReq {
    if (!_getVerifyCodeReq) {
        _getVerifyCodeReq = [QJGetVerifyCodeRequest new];
    }
    return _getVerifyCodeReq;
}

- (QJImageVerifyCode *)imageVerifyCode {
    if (!_imageVerifyCode) {
        _imageVerifyCode = [QJImageVerifyCode new];
    }
    return _imageVerifyCode;
}

#pragma mark ----- btnAction

- (void)thridLoginBtnAction {
    DLog(@"三方登录");
    if (!self.checkBtn.selected) {
         
        [QJAppTool showWarningToast:@"请勾选同意《用户授权协议》和《隐私条款》"];
        return;
    }
    QJThirdLoginViewController *vc = [QJThirdLoginViewController new];
    vc.loginOrRegisterSuccessBlock = self.loginOrRegisterSuccessBlock;
    QJNavigationController *nav = [[QJNavigationController alloc]initWithRootViewController:vc];
    nav.modalPresentationStyle = UIModalPresentationCustom;
    [self presentViewController:nav animated:NO completion:nil];
    
}

- (void)btnAction:(UIButton *)sender {
    
    sender.selected = !sender.selected;
    
    if (sender.selected) {
        self.tipImageView.hidden = YES;
      
        [_checkBtn setImage:[UIImage imageNamed:@"selectedAgreementIcon"] forState:UIControlStateNormal];
    }else{
        [_checkBtn setImage:[UIImage imageNamed:@"unSelectedAgreementIcon"] forState:UIControlStateNormal];
         
    }
    
}




//获取验证码
- (void)getVerifyCode {
    
    if (!self.checkBtn.selected) {
       
        [QJAppTool showWarningToast:@"请勾选同意《用户授权协议》和《隐私条款》"];
        return;
    }
    
    
        if (IsStrEmpty(self.userTFView.text)) {
           
            [QJAppTool showWarningToast:@"请输入手机号"];
            return;
        }
    
    
    WS(weakSelf)
    NSString *str = [self.userTFView.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    self.getVerifyCodeReq.dic = @{@"mobile" : str,@"type":@"LOGIN"};
    self.getVerifyCodeReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        if ([EncodeStringFromDic(request.responseObject, @"code") isEqualToString:@"0"]) {
            QJVerifyCodeViewController *vc = [QJVerifyCodeViewController new];
            vc.type = QJGetVerifyCodeVCType_phoneLogin;
            vc.loginOrRegisterSuccessBlock = weakSelf.loginOrRegisterSuccessBlock;
            vc.mobile = str;
            [weakSelf.userTFView resignFirstResponder];
            [weakSelf.navigationController pushViewController:vc animated:YES];
        }else if ([EncodeStringFromDic(request.responseObject, @"code") isEqualToString:@"501"]) {//出滑块
            //滑块
            weakSelf.imageVerifyCode = [QJImageVerifyCode new];
            weakSelf.imageVerifyCode.delegate = weakSelf;
            [weakSelf.imageVerifyCode showView];
            
        }
        else{
            NSString *msg = EncodeStringFromDic(request.responseObject, @"message");
            
            [QJAppTool showWarningToast:msg];
        }
    };
    
    self.getVerifyCodeReq.failureCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        
        [QJAppTool showWarningToast:@"数据异常 请稍后再试"];
    };
    
    [self.getVerifyCodeReq start];
    
    
}


 



#pragma mark ---- QJImageVerifyCodeDelegate

/// 完成验证之后的回调
/// @param result 验证结果 BOOL:YES/NO
/// @param validate 二次校验数据，如果验证结果为false，validate返回空
/// @param message 结果描述信息
- (void)QJImageVerifyCodeValidateFinish:(BOOL)result validate:(NSString *)validate message:(NSString *)message {
    DLog(@"收到验证结果的回调:(%d,%@,%@)", result, validate, message);
    if (result) {
        [QJUserManager shareManager].validate = validate;
        [self getVerifyCode];
      
        
    }
  
}

 


 
@end
