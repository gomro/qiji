//
//  QJRegisterViewController.h
//  QJBox
//
//  Created by wxy on 2022/6/13.
//



#import "QJBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN



/// 注册控制器
@interface QJRegisterViewController : QJBaseViewController


@property (nonatomic, copy) LoginOrRegisterSuccessBlock loginOrRegisterSuccessBlock;


@end

NS_ASSUME_NONNULL_END
