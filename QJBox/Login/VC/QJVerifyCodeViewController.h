//
//  QJVerifyCodeViewController.h
//  QJBox
//
//  Created by wxy on 2022/6/15.
//

#import "QJBaseViewController.h"
#import "QJFindWordViewController.h"
NS_ASSUME_NONNULL_BEGIN

@interface QJVerifyCodeViewController : QJBaseViewController


@property (nonatomic, copy) NSString *mobile;

@property (nonatomic, assign) QJGetVerifyCodeVCType type;

@property (nonatomic, copy) LoginOrRegisterSuccessBlock loginOrRegisterSuccessBlock;

@property (nonatomic, copy) NSString *bindToken;

@property (nonatomic, copy) NSString *bindType;//绑定类型:微信:W,QQ:Q,苹果:A


@end

NS_ASSUME_NONNULL_END
