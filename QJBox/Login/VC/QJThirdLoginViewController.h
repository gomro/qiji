//
//  QJThirdLoginViewController.h
//  QJBox
//
//  Created by wxy on 2022/6/27.
//

#import "QJBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

/// 三方登录控制器   包含 账号密码、微信、QQ、苹果账号登录
@interface QJThirdLoginViewController : QJBaseViewController


@property (nonatomic, copy) LoginOrRegisterSuccessBlock loginOrRegisterSuccessBlock;



@end

NS_ASSUME_NONNULL_END
