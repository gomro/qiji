//
//  QJSMSCodeInputView.m
//  QJBox
//
//  Created by wxy on 2022/6/10.
//

#import "QJSMSCodeInputView.h"
#import "QJSMSCodeView.h"

@interface QJSMSCodeInputView ()<UITextFieldDelegate>

/// 输入框
@property (nonatomic, strong) UITextField *textField;


/// 格子数组
@property (nonatomic, strong) NSMutableArray <QJSMSCodeView *> *arrayTextField;

///记录上一次字符串
@property (nonatomic, copy) NSString *lastString;

//放置小格子
@property (nonatomic, strong) UIView *contentView;
@end


@implementation QJSMSCodeInputView

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}



- (instancetype)initWithFrame:(CGRect)frame {
    self  = [super initWithFrame:frame];
    if (self) {
        [self config];
    }
    return self;
}


- (void)config {
    _codeCount = 4;
    _codeSpace = 32;
    
    //初始化数组
    _arrayTextField = [NSMutableArray array];
    
    _lastString = @"";
    
    
    
   
    [self addSubview:self.textField];
 
    
    //放置view
    _contentView = [[UIView alloc]init];
    _contentView.backgroundColor = [UIColor clearColor];
    _contentView.userInteractionEnabled = NO;
    [self addSubview:_contentView];
    

}



- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self updateSubViews];
}


- (void)updateSubViews {
    
    self.textField.frame = self.bounds;
    self.contentView.frame = self.bounds;
    
    if (_arrayTextField.count < _codeCount) {//已经存在的子控件比新来的数要小，那么就创建
        NSUInteger c = _codeCount - _arrayTextField.count;
        for (NSInteger i = 0; i < c; i++) {
            QJSMSCodeView *v = [[QJSMSCodeView alloc]init];
            [_arrayTextField addObject:v];
        }
         
    }else if (_arrayTextField.count == _codeCount) {//个数相等
        
        return;
    }else if (_arrayTextField.count > _codeCount) {//个数有多余，那么不用创建新的，
        NSUInteger c = _arrayTextField.count - _codeCount;
        for (NSInteger i = 0; i < c; i++) {
            [_arrayTextField.lastObject removeFromSuperview];
            [_arrayTextField removeLastObject];
        }
    }
    
    
    //可用宽度/格子总数
    CGFloat w = (self.bounds.size.width - _codeSpace * (_codeCount - 1)) / (_codeCount * 1.0);
    
    //重新布局小格子
    for (NSInteger i = 0;  i < _arrayTextField.count; i++) {
        QJSMSCodeView *t = [_arrayTextField safeObjectAtIndex:i];
        [self.contentView addSubview:t];
        t.frame = CGRectMake(i *(w + _codeSpace), 0, w
                             , self.bounds.size.height);
    }
    
}


//已经编辑
- (void)textFieldDidChangeValue:(NSNotification *)notification {
 
    UITextField *sender = (UITextField *)[notification object];
    
    BOOL a = sender.text.length >= self.lastString.length;
    BOOL b = sender.text.length - self.lastString.length >= _codeCount;
    
    if (a && b) {//判断为一连串验证码输入，那么，最后N个，就是来自键盘上的短信验证码，取最后N个
        DLog(@"一连串输入");
        sender.text = [sender.text substringFromIndex:sender.text.length - _codeCount];
         
    }
    
    if (sender.text.length >= _codeCount + 1) {//对于持续输入，只要前面N个就行
        DLog(@"持续输入");
        sender.text = [sender.text substringToIndex:_codeCount - 1];
    }
    
    //字符串转数组
    NSMutableArray <NSString *> *stringArray = [NSMutableArray array];
    NSString *temp = nil;
    for (int i = 0; i < [sender.text length]; i++) {
        temp = [sender.text substringWithRange:NSMakeRange(i, 1)];
        [stringArray addObject:temp];
    }
    
    //设置文字
    for (int i = 0; i < self.arrayTextField.count; i++) {
        QJSMSCodeView *smsCodeView = [self.arrayTextField safeObjectAtIndex:i];
        if (i < stringArray.count) {
            smsCodeView.text = [stringArray safeObjectAtIndex:i];
        }else{
            smsCodeView.text = @"";
        }
    }
    
    //设置光标
    if (stringArray.count == 0) {
        for (int i = 0; i < self.arrayTextField.count; i++) {
            BOOL hide  = (i == 0 ? YES : NO);
            QJSMSCodeView *codeView = [self.arrayTextField safeObjectAtIndex:i];
            codeView.showCursor = hide;
        }
    }else if (stringArray.count == self.arrayTextField.count) {
        for (int i = 0; i < self.arrayTextField.count; i++) {
            QJSMSCodeView *codeView = [self.arrayTextField safeObjectAtIndex:i];
            codeView.showCursor = NO;
        }
    }else {
        for (int i = 0; i < self.arrayTextField.count; i++) {
            QJSMSCodeView *codeView = [self.arrayTextField safeObjectAtIndex:i];
            if (i == stringArray.count - 0) {
                codeView.showCursor = YES;
            }else{
                codeView.showCursor = NO;
            }
        }
    }
    
    if (stringArray.count == self.arrayTextField.count) {
        [self.textField resignFirstResponder];
        if (self.codeInputFinish) {
            self.codeInputFinish(self.textField.text);
        }
    }
    
    self.lastString = sender.text;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    //检查上一次的字符串
    if (self.lastString.length == 0 || self.lastString.length == 0) {
        self.arrayTextField.firstObject.showCursor = YES;
    } else if (self.lastString.length == self.arrayTextField.count) {
        self.arrayTextField.lastObject.showCursor = YES;
    } else {
        self.arrayTextField[self.lastString.length - 0].showCursor = YES;
    }
}

- (NSString *)codeText {
    return self.textField.text;
}

- (BOOL)resignFirstResponder {
    for(int i = 0; i < self.arrayTextField.count; i++) {
        QJSMSCodeView *SMSCodeView = [self.arrayTextField safeObjectAtIndex:i];
        SMSCodeView.showCursor = NO;
    }
    [self.textField resignFirstResponder];
    return YES;
}

- (BOOL)becomeFirstResponder {
    [self.textField becomeFirstResponder];
    return YES;
}

- (void)showKeyBoard {
    [self.textField becomeFirstResponder];
}


///如果要求可以随时更改输入位数, 那么,
- (void)setCodeCount:(NSInteger)codeCount {
    _codeCount = codeCount;
    
    //因为个数改变,清空之前输入的内容
    self.lastString = @"";
    self.textField.text = @"";
    
    for (NSInteger i = 0; i < _arrayTextField.count; i ++) {
        QJSMSCodeView *t = [_arrayTextField safeObjectAtIndex:i];
        t.text = @"";
        if (i == 0) {
            t.showCursor = YES;
        } else {
            t.showCursor = NO;
        }
    }
    
    [self setNeedsLayout];
    [self layoutIfNeeded];
}



- (UITextField *)textField {
    if (!_textField) {
        //输入框
        _textField = [[UITextField alloc] init];
        _textField.backgroundColor = [UIColor clearColor];
        _textField.keyboardType = UIKeyboardTypeNumberPad;
        _textField.delegate = self;
        _textField.textColor = [UIColor clearColor];
        _textField.tintColor = [UIColor clearColor];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldDidChangeValue:) name:UITextFieldTextDidChangeNotification object:_textField];
    }
    return _textField;
}

@end
