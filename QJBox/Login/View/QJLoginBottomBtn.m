//
//  QJLoginBottomBtn.m
//  QJBox
//
//  Created by wxy on 2022/6/27.
//

#import "QJLoginBottomBtn.h"


@interface QJLoginBottomBtn ()


@end



@implementation QJLoginBottomBtn

 
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
       
        [self addSubview:self.bgBtn];
        [self.bgBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.edges.equalTo(self);
        }];
        self.layer.cornerRadius = 8;
        self.layer.masksToBounds = YES;
    }
    return self;
}


- (UIButton *)bgBtn {
    if (!_bgBtn) {
        _bgBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_bgBtn setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithHexString:@"#1F2A4D"]] forState:UIControlStateNormal];
        [_bgBtn setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithHexString:@"#919599"]] forState:UIControlStateDisabled];
        _bgBtn.titleLabel.font = FontSemibold(16);
        [_bgBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
         
        [_bgBtn addTarget:self action:@selector(btnAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _bgBtn;
}

- (void)setTitle:(NSString *)title {
    [self.bgBtn setTitle:title forState:UIControlStateNormal];
}

- (void)setColor:(UIColor *)color {
    
    [_bgBtn setBackgroundImage:[UIImage imageWithColor:color] forState:UIControlStateNormal];
    [_bgBtn setBackgroundImage:[UIImage imageWithColor:color] forState:UIControlStateHighlighted];
}

- (void)setTitleColor:(UIColor *)titleColor {
    _titleColor = titleColor;
    [_bgBtn setTitleColor:titleColor forState:UIControlStateNormal];
}

- (void)btnAction:(UIButton *)btn {
    if (self.btnBlock) {
        self.btnBlock();
    }
    
}

@end
