//
//  QJSMSCodeView.h
//  QJBox
//
//  Created by wxy on 2022/6/10.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN


/// 登录验证码输入框中的显示数字的view
@interface QJSMSCodeView : UIView

///文字
@property (nonatomic, copy) NSString *text;

///显示光标 默认关闭
@property (nonatomic) BOOL showCursor;

@end

NS_ASSUME_NONNULL_END
