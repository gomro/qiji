//
//  QJLoginBgView.m
//  QJBox
//
//  Created by wxy on 2022/6/27.
//

#import "QJLoginBgView.h"

@implementation QJLoginBgView


- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self addSubview:self.bgView];
        [self addSubview:self.logoImageView];
        
        [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
        }];
        
        [self.logoImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self);
            make.top.equalTo(self).offset(72 + NavigationBar_Bottom_Y);
            make.width.equalTo(@221);
            make.height.equalTo(@131);
        }];
        
        
    }
    return self;
}





#pragma mark ---------- 登录

- (UIImageView *)bgView {
    if (!_bgView) {
        _bgView = [UIImageView new];
        _bgView.userInteractionEnabled = YES;
        _bgView.image = [UIImage imageNamed:@"bglogbgImage"];
    }
    return _bgView;
}


- (UIImageView *)logoImageView {
    if (!_logoImageView) {
        _logoImageView = [UIImageView new];
        _logoImageView.image = [UIImage imageNamed:@"LogologinLog"];
        _logoImageView.userInteractionEnabled = YES;
    }
    return _logoImageView;
}


@end
