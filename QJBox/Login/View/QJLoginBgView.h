//
//  QJLoginBgView.h
//  QJBox
//
//  Created by wxy on 2022/6/27.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN


/// 登录模块背景视图
@interface QJLoginBgView : UIView

@property (nonatomic, strong) UIImageView *bgView;

@property (nonatomic, strong) UIImageView *logoImageView;


@end

NS_ASSUME_NONNULL_END
