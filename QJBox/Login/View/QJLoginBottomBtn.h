//
//  QJLoginBottomBtn.h
//  QJBox
//
//  Created by wxy on 2022/6/27.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/// 登录模块底部绿色按钮
@interface QJLoginBottomBtn : UIView


@property (nonatomic, copy) void (^btnBlock)(void);

@property (nonatomic, strong) UIButton *bgBtn;

@property (nonatomic, copy) NSString *title;

@property (nonatomic, strong) UIColor *color;

@property (nonatomic, strong) UIColor *titleColor;
@end

NS_ASSUME_NONNULL_END
