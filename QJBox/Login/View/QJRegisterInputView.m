//
//  QJRegisterInputView.m
//  QJBox
//
//  Created by wxy on 2022/6/13.
//

#import "QJRegisterInputView.h"

@implementation QJRegisterInputView


- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self addSubview:self.titleLabel];
        [self addSubview:self.textField];
        [self addSubview:self.line];
        [self addSubview:self.rightBtn];
        
        [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self);
            make.width.equalTo(@60);
            make.top.equalTo(self);
            make.height.equalTo(@13);
        }];
        
        [self.textField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.titleLabel.mas_right).offset(5);
            make.centerY.equalTo(self.titleLabel);
            make.height.equalTo(@13);
            make.right.equalTo(self.rightBtn.mas_left);
        }];
        
        [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.equalTo(self);
            make.height.equalTo(@1);
        }];
        
        [self.rightBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self);
            make.centerY.equalTo(self.titleLabel);
            make.width.height.equalTo(@20);
        }];
        
    }
    return self;
}




#pragma mark ------- getter

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.font = kFont(12);
        _titleLabel.textAlignment = NSTextAlignmentLeft;
        _titleLabel.textColor = [UIColor colorWithHexString:@"000000"];
        
    }
    return _titleLabel;
}


- (UITextField *)textField {
    if (!_textField) {
        _textField = [UITextField new];
        _textField.delegate = self;
        _textField.font = kFont(12);
        _textField.textColor = [UIColor colorWithHexString:@"000000"];
        _textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    }
    return _textField;
}


- (UILabel *)line {
    if (!_line) {
        _line = [UILabel new];
        _line.backgroundColor = [UIColor colorWithHexString:@"111111"];
    }
    return _line;
}


- (UIButton *)rightBtn {
    if (!_rightBtn) {
        _rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_rightBtn addTarget:self action:@selector(btnAction:) forControlEvents:UIControlEventTouchUpInside];
        [_rightBtn setBackgroundImage:[UIImage imageWithColor:[UIColor grayColor]] forState:UIControlStateNormal];
        
        [_rightBtn setBackgroundImage:[UIImage imageWithColor:[UIColor greenColor]] forState:UIControlStateSelected];
    }
    return _rightBtn;
}





#pragma mark -------- btnAction

- (void)btnAction:(UIButton *)sender {
    sender.selected = !sender.selected;
    
    if (self.btnClick) {
        self.btnClick(sender.selected);
    }
}


//隐藏右边按钮
- (void)hiddenRightBtn {
    
    self.rightBtn.hidden = YES;
    [self.textField mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.titleLabel.mas_right).offset(5);
        make.centerY.equalTo(self.titleLabel);
        make.height.equalTo(@13);
        make.right.equalTo(self);
    }];
    
}

#pragma mark  ---- UITextFieldDelegate


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if ([[textField.textInputMode primaryLanguage] isEqualToString:@"emoji"] || ![textField.textInputMode primaryLanguage]) {
        return NO;
    }
    //判断键盘是不是九宫格键盘
    if ([NSString isNineKeyBoard:string] ){
        return YES;
    }else{
        if ([NSString hasEmoji:string] || [NSString isContainsTwoEmoji:string]){
            return NO;
        }
    }
    
    return YES;
}




@end
