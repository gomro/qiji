//
//  QJRegisterInputView.h
//  QJBox
//
//  Created by wxy on 2022/6/13.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/// 注册账号输入视图
@interface QJRegisterInputView : UIView <UITextFieldDelegate>


@property (nonatomic, strong) UILabel *titleLabel;


@property (nonatomic, strong) UITextField *textField;


@property (nonatomic, strong) UILabel *line;


@property (nonatomic, strong) UIButton *rightBtn;


@property (nonatomic, copy) void(^btnClick)(BOOL isSelected);


- (void)hiddenRightBtn;
@end

NS_ASSUME_NONNULL_END
