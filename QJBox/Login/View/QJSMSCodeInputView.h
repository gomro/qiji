//
//  QJSMSCodeInputView.h
//  QJBox
//
//  Created by wxy on 2022/6/10.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN




/// 登录验证码输入框
@interface QJSMSCodeInputView : UIView

///验证码文字
@property (nonatomic, copy) NSString *codeText;

///设置验证码位数 默认 4 位
@property (nonatomic) NSInteger codeCount;

///验证码数字之间的间距 默认 35
@property (nonatomic) CGFloat codeSpace;


@property (nonatomic ,copy) void(^codeInputFinish)(NSString *code);

- (void)showKeyBoard;
@end

NS_ASSUME_NONNULL_END
