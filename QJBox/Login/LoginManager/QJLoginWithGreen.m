//
//  QJLoginWithGreen.m
//  QJBox
//
//  Created by wxy on 2022/6/8.
//

#import "QJLoginWithGreen.h"

@implementation QJLoginWithGreen


- (void)clickGreenBtn {
    
    
    [QJWXMANAGER WXAuth];
    QJWXMANAGER.delegate = self;
    
}



- (void)managerDidRecvAuthResponse:(SendAuthResp *)response {
    
    SendAuthResp *resp = (SendAuthResp *)response;
 
    switch (resp.errCode) {
        case WXSuccess:
            NSLog(@"RESP:code:%@,state:%@\n", resp.code, resp.state);
                if (self.delegate && [self.delegate respondsToSelector:@selector(wxAuthSucceed:)])
                    [self.delegate wxAuthSucceed:resp.code];
            break;
        case WXErrCodeAuthDeny:
                if (self.delegate && [self.delegate respondsToSelector:@selector(wxAuthDenied)])
                    [self.delegate wxAuthDenied];
            break;
        case WXErrCodeUserCancel:
                if (self.delegate && [self.delegate respondsToSelector:@selector(wxAuthCancel)])
                    [self.delegate wxAuthCancel];
        default:
            break;
    }
    
}


@end
