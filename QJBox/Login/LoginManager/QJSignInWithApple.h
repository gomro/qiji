//
//  QJSignInWithApple.h
//  QJBox
//
//  Created by wxy on 2022/6/7.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJSignInWithApple : NSObject


/// 点击苹果登录
- (void)appleLoginClick;


/// 授权成功回调
@property (nonatomic, copy) void(^didCompleteBlock)(NSDictionary *paramDic);


/// 授权失败回调
@property (nonatomic, copy) void(^didfailedBlock)(NSDictionary *paramDic);

@end

NS_ASSUME_NONNULL_END
