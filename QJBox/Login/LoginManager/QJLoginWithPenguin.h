//
//  QJLoginWithPenguin.h
//  QJBox
//
//  Created by wxy on 2022/6/9.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN


/// 使用QQ登录
@interface QJLoginWithPenguin : NSObject



- (void)clickPenguinBtn;


/// 授权成功回调
@property (nonatomic, copy) void(^didCompleteBlock)(NSDictionary *paramDic);



@end

NS_ASSUME_NONNULL_END
