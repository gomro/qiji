//
//  QJUserManager.h
//  QJBox
//
//  Created by wxy on 2022/6/30.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJUserManager : NSObject

+ (QJUserManager *)shareManager;

+ (instancetype)new NS_UNAVAILABLE;

/**
 *  Unavailable initializer
 */
- (instancetype)init NS_UNAVAILABLE;


@property (nonatomic, assign) BOOL isLogin;//是否已经登录 YES登录，NO没有登录

//用户协议
@property (nonatomic, copy) NSString *userPolicy;

//隐私政策
@property (nonatomic, copy) NSString *userProtocol;

@property (nonatomic, assign) BOOL isVerifyState;//NO未实名，YES实名

@property (nonatomic, copy) NSString *userID;

@property (nonatomic, copy) NSString *validate;//滑块校验参数

@property (nonatomic, assign) BOOL isInterdiction;//是否封禁

@property (nonatomic, assign) BOOL isNetLogout;//是否接口触发重新登录
@end

NS_ASSUME_NONNULL_END
