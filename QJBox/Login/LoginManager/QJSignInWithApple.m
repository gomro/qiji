//
//  QJSignInWithApple.m
//  QJBox
//
//  Created by wxy on 2022/6/7.
//

#import "QJSignInWithApple.h"
#import <AuthenticationServices/AuthenticationServices.h>



@interface QJSignInWithApple ()<ASAuthorizationControllerDelegate, ASAuthorizationControllerPresentationContextProviding>



@end


@implementation QJSignInWithApple


- (void)appleLoginClick {
    
    if (@available(iOS 13.0, *)) {
        // 基于用户的Apple ID授权用户，生成用户授权请求的一种机制
        ASAuthorizationAppleIDProvider *appleIDProvider = [[ASAuthorizationAppleIDProvider alloc] init];
        // 创建新的AppleID 授权请求
        ASAuthorizationAppleIDRequest *appleIDRequest = [appleIDProvider createRequest];
        // 在用户授权期间请求的联系信息
        appleIDRequest.requestedScopes = @[ASAuthorizationScopeFullName, ASAuthorizationScopeEmail];
        // 由ASAuthorizationAppleIDProvider创建的授权请求 管理授权请求的控制器
        ASAuthorizationController *authorizationController = [[ASAuthorizationController alloc] initWithAuthorizationRequests:@[appleIDRequest]];
        // 设置授权控制器通知授权请求的成功与失败的代理
        authorizationController.delegate = self;
        // 设置提供 展示上下文的代理，在这个上下文中 系统可以展示授权界面给用户
        authorizationController.presentationContextProvider = self;
        // 在控制器初始化期间启动授权流
        [authorizationController performRequests];
    }else{
        // 处理不支持系统版本
        DLog(@"该系统版本不可用Apple登录");
    }
    
    
}



//@optional 授权成功地回调
- (void)authorizationController:(ASAuthorizationController *)controller didCompleteWithAuthorization:(ASAuthorization *)authorization API_AVAILABLE(ios(13.0)) {
    if ([authorization.credential isKindOfClass:[ASAuthorizationAppleIDCredential class]]) {
        // 用户登录使用ASAuthorizationAppleIDCredential
        ASAuthorizationAppleIDCredential *appleIDCredential = authorization.credential;
        NSString *user = appleIDCredential.user;
        // 使用过授权的，可能获取不到以下三个参数
        NSPersonNameComponents * fullName = appleIDCredential.fullName;
        NSString *email = appleIDCredential.email;
        
        NSData *identityToken = appleIDCredential.identityToken;
        NSData *authorizationCode = appleIDCredential.authorizationCode;
        
        // 服务器验证需要使用的参数
        NSString *identityTokenStr = [[NSString alloc] initWithData:identityToken encoding:NSUTF8StringEncoding];
        NSString *authorizationCodeStr = [[NSString alloc] initWithData:authorizationCode encoding:NSUTF8StringEncoding];
        
        // 用于判断当前登录的苹果账号是否是一个真实用户，取值有：unsupported、unknown、likelyReal
        ASUserDetectionStatus realUserStatus = appleIDCredential.realUserStatus;
        // Create an account in your system.
        // For the purpose of this demo app, store the userIdentifier in the keychain.
        // 需要使用钥匙串的方式保存用户的唯一信息
        [[NSUserDefaults standardUserDefaults] setObject:user forKey:@"appleID"];
        
        DLog(@"userID: %@", user);
        DLog(@"fullName: %@", fullName);
        DLog(@"email: %@", email);
        DLog(@"authorizationCode: %@", authorizationCodeStr);
        DLog(@"identityToken: %@", identityTokenStr);
        DLog(@"realUserStatus: %@", @(realUserStatus));
        
        //请求苹果登录（自己服务器）
        NSMutableDictionary *parmDic = [NSMutableDictionary dictionary];
        [parmDic setValue:identityTokenStr forKey:@"union_token"];
        [parmDic setValue:user forKey:@"union_id"];
        [parmDic setValue:authorizationCodeStr forKey:@"authorizationToken"];
        //拼接名字
        NSString *name=[NSString stringWithFormat:@"%@%@%@ ",fullName.familyName?:@"",fullName.middleName?:@"",fullName.givenName?:@""];
        [parmDic setValue:name forKey:@"name"];
        [parmDic setValue:@"apple" forKey:@"union_type"];
        [parmDic setValue:@"ios" forKey:@"platform"];
        [parmDic setValue:@"" forKey:@"email"];
        if (self.didCompleteBlock) {
            self.didCompleteBlock(parmDic);
        }
        
    } else if ([authorization.credential isKindOfClass:[ASPasswordCredential class]]) {
        // 用户登录使用现有的密码凭证（钥匙串凭证，登录过app保存下来的密码可以直接使用，不做处理，未使用）
        ASPasswordCredential *passwordCredential = authorization.credential;
        // 密码凭证对象的用户标识 用户的唯一标识
        NSString *user = passwordCredential.user;
        // 密码凭证对象的密码
        NSString *password = passwordCredential.password;
        DLog(@"userID: %@", user);
        DLog(@"password: %@", password);
    } else {
        DLog(@"授权信息均不符");
    }
}
// 授权失败的回调
- (void)authorizationController:(ASAuthorizationController *)controller didCompleteWithError:(NSError *)error API_AVAILABLE(ios(13.0)) {
    // Handle error.
    DLog(@"Handle error：%@", error);
    NSString *errorMsg = nil;
    switch (error.code) {
        case ASAuthorizationErrorCanceled:
            errorMsg = @"用户取消了授权请求";
            break;
        case ASAuthorizationErrorFailed:
            errorMsg = @"授权请求失败";
            break;
        case ASAuthorizationErrorInvalidResponse:
            errorMsg = @"授权请求响应无效";
            break;
        case ASAuthorizationErrorNotHandled:
            errorMsg = @"未能处理授权请求";
            break;
        case ASAuthorizationErrorUnknown:
            errorMsg = @"授权请求失败未知原因";
            break;
        default:
            break;
    }
//    if (self.didfailedBlock) {
//        self.didfailedBlock(@{
//            @"union_type":@"apple",
//            @"fail_reason":errorMsg
//        });
//    }
}

// 告诉代理应该在哪个window 展示内容给用户
- (ASPresentationAnchor)presentationAnchorForAuthorizationController:(ASAuthorizationController *)controller API_AVAILABLE(ios(13.0)){
    // 返回window
    return [UIApplication sharedApplication].keyWindow;
}


@end
