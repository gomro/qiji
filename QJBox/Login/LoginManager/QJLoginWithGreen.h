//
//  QJLoginWithGreen.h
//  QJBox
//
//  Created by wxy on 2022/6/8.
//


#import <Foundation/Foundation.h>
#import "WXApi.h"
NS_ASSUME_NONNULL_BEGIN


@protocol WXAuthDelegate <NSObject>

@optional
- (void)wxAuthSucceed:(NSString*)code;
- (void)wxAuthDenied;
- (void)wxAuthCancel;

@end

/// 微信登录
@interface QJLoginWithGreen : NSObject<WXApiManagerDelegate>



@property (nonatomic, weak) id<WXAuthDelegate, NSObject> delegate;


/// 点击微信图标
- (void)clickGreenBtn;

 
@end

NS_ASSUME_NONNULL_END
