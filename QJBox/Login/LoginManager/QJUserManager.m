//
//  QJUserManager.m
//  QJBox
//
//  Created by wxy on 2022/6/30.
//

#import "QJUserManager.h"
#import "QJMineSetRequest.h"
@implementation QJUserManager

+ (QJUserManager *)shareManager {
    static QJUserManager *user = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (!user) {
            user = [QJUserManager alloc];
        }
    });
    return user;
}



- (NSString *)userPolicy {
    if (!_userPolicy) {
        _userPolicy = [NSString stringWithFormat:@"%@/privacy/policy",[QJEnvironmentConfigure shareInstance].baseURL];
    }
    
    return _userPolicy;
}

- (NSString *)userProtocol {
    if (!_userProtocol) {
        _userProtocol = [NSString stringWithFormat:@"%@/privacy/protocol",[QJEnvironmentConfigure shareInstance].baseURL];
    }
    return _userProtocol;
}


// 用户协议
- (void)sendRequestProtocol {

    @weakify(self);
    QJMineSetRequest *request = [[QJMineSetRequest alloc] init];
    [request requestPrivacyProtocol];
    [request setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {

        @strongify(self);
        DLog(@"-=-=-==-%@",request.responseJSONObject[@"code"]);
        if (ResponseSuccess) {
            NSString *htmlUrl= request.responseJSONObject[@"data"][@"url"];
            self.userPolicy = htmlUrl;
        }
    }];

    [request setFailureCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {

        
    }];
}

// 隐私政策
- (void)sendRequestPolicy {
    @weakify(self);
    QJMineSetRequest *request = [[QJMineSetRequest alloc] init];
    [request requestPrivacyPolicy];
    [request setSuccessCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {
        @strongify(self);
        DLog(@"-=-=-==-%@",request.responseJSONObject[@"code"]);
        if (ResponseSuccess) {
            NSString *htmlUrl= request.responseJSONObject[@"data"][@"url"];
            self.userProtocol = htmlUrl;
        }
    }];

    [request setFailureCompletionBlock:^(__kindof YTKBaseRequest * _Nonnull request) {

        
    }];
}

@end
