//
//  QJLoginWithPenguin.m
//  QJBox
//
//  Created by wxy on 2022/6/9.
//

#import "QJLoginWithPenguin.h"

 

@interface QJLoginWithPenguin ()<QQApiManagerDelegate>


@property (nonatomic, strong) TencentOAuth *tencentOAuth;


@end

@implementation QJLoginWithPenguin


- (void)clickPenguinBtn {

    [QJQQAPIMANAGER setupSDK];
    QJQQAPIMANAGER.delegate = self;
    [QJQQAPIMANAGER QQAuth];
}

#pragma mark  ----------- QQApiManagerDelegate

- (void)getResultDic:(NSDictionary *)resultDic {
    if (self.didCompleteBlock) {
        self.didCompleteBlock(resultDic);
    }
    
}


 

@end
