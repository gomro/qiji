//
//  QJHomeRecommendTabModel.h
//  QJBox
//
//  Created by wxy on 2022/9/19.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

/// 推荐tab 模型
@interface QJHomeRecommendTabModel : NSObject

@property (nonatomic, copy) NSString *name;

@property (nonatomic, copy) NSString *idStr;

@property (nonatomic, copy) NSString *windowType;

@end

NS_ASSUME_NONNULL_END
