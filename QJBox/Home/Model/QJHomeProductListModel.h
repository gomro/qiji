//
//  QJHomeProductListModel.h
//  QJBox
//
//  Created by wxy on 2022/7/6.
//

#import <Foundation/Foundation.h>
#import "QJSearchGameListModel.h"

NS_ASSUME_NONNULL_BEGIN

/// 游戏推荐模型
@interface QJHomeProductListModel : NSObject

@property (nonatomic, copy) NSString *createTime;

@property (nonatomic, copy) NSString *updatedTime;

@property (nonatomic, copy) NSString *icon;//图标

@property (nonatomic, copy) NSString *thumbnail;//缩略图

@property (nonatomic, copy) NSString *name;//游戏名称

@property (nonatomic, copy) NSString *iCusInsUrl;//iOS 自定义安装链接

@property (nonatomic, copy) NSString *iUrl;//iOS AppStore 链接

@property (nonatomic, copy) NSString *iSch;//iOS Schema

@property (nonatomic, copy) NSString *iPkgMode;//iOS安装模式: appstore: 0, 自定义: 1,可用值:APPSTORE,CUSTOM

@property (nonatomic, copy) NSString *iPkgSize;//iOS 包大小

@property (nonatomic, copy) NSString *onlineCount;//在线人数

@property (nonatomic, strong) NSArray *tags;//标签

@property (nonatomic, copy) NSString *idStr;

@end

NS_ASSUME_NONNULL_END
