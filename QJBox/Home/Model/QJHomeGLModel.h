//
//  QJHomeGLModel.h
//  QJBox
//
//  Created by wxy on 2022/7/6.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

/// 首页推荐攻略
@interface QJHomeGLModel : NSObject

@property (nonatomic, copy) NSString *createTime;

@property (nonatomic, copy) NSString *updatedTime;

@property (nonatomic, copy) NSString *coverImage;//封面图

@property (nonatomic, copy) NSString *title;//标题

@property (nonatomic, copy) NSString *reviewCount;//观看量

@property (nonatomic, copy) NSString *likeCount;//点赞数

@property (nonatomic, copy) NSString *userNickname;//作者

@property (nonatomic, copy) NSString *userCoverImage;//作者头像

@property (nonatomic, copy) NSString *bloggerLevel;//用户等级

@property (nonatomic, copy) NSString *idStr;

@property (nonatomic, assign) BOOL liked;//是否点过赞

@property (nonatomic, copy) NSString *videoAddress;//视频地址

@property (nonatomic, copy) NSString *infoTag;//标签



@end

NS_ASSUME_NONNULL_END
