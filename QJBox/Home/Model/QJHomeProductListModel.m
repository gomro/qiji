//
//  QJHomeProductListModel.m
//  QJBox
//
//  Created by wxy on 2022/7/6.
//

#import "QJHomeProductListModel.h"

@implementation QJHomeProductListModel


+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"tags" : [QJSearchGameDetailModel class]};
}

+ (NSDictionary *)modelCustomPropertyMapper{
    return @{@"idStr":@"id"};
}
@end
