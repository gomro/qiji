//
//  QJHomeBannerModel.h
//  QJBox
//
//  Created by wxy on 2022/7/6.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

/// 首页轮播图模型
@interface QJHomeBannerModel : NSObject

@property (nonatomic, copy) NSString *createTime;

@property (nonatomic, copy) NSString *updatedTime;

@property (nonatomic, copy) NSString *imgUrl;

@property (nonatomic, copy) NSString *backImgUrl;

@property (nonatomic, copy) NSString *title;

@property (nonatomic, copy) NSString *innerType;

@property (nonatomic, copy) NSString *sourceId;

@property (nonatomic, copy) NSString *jumpType;

@end

NS_ASSUME_NONNULL_END
