//
//  QJHomeMarketModel.h
//  QJBox
//
//  Created by wxy on 2022/7/6.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

/// 首页金刚区模型
@interface QJHomeMarketModel : NSObject

@property (nonatomic, copy) NSString *createTime;

@property (nonatomic, copy) NSString *updatedTime;

@property (nonatomic, copy) NSString *imgUrl;//图片地址

@property (nonatomic, copy) NSString *h5Url;

@property (nonatomic, copy) NSString *title;

@property (nonatomic, copy) NSString *innerType;//类型--banner（0：游戏，1：攻略，2：营地/动态，3：短视频）、金刚区（0找游戏、1找攻略、2找圈子、3福利礼包、4新用户奖励、5签到）

@property (nonatomic, copy) NSString *sourceId;

@property (nonatomic, copy) NSString *jumpType;//跳转类型（0不跳转、1跳转内部、2跳转外部）





@end

NS_ASSUME_NONNULL_END
