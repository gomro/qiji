//
//  QJHomeViewController.m
//  QJBox
//
//  Created by wxy on 2022/6/1.
//

#import "QJHomeViewController.h"
#import "QJLoginViewController.h"
#import "QJHomeCarouselCell.h"
#import "QJHomeMarketingCell.h"
#import "QJHomeMainCell.h"
#import "QJHomeSectionHeadView.h"
#import "QJHomeNestTableViewCell.h"
#import "QJNestTableView.h"


#import "QJCheckInViewController.h"//签到页面
#import "QJProductDetailViewController.h"//游戏详情页面
#import "QJMyBoxListViewController.h"//我的游戏页面

#import "QJHomeGLCell.h"
#import "QJHomeMarkettingRequest.h"
#import "QJHomeBannerModel.h"
#import "QJHomeMarketModel.h"
#import "QJHomeGLRequest.h"
#import "QJHomeProductListRequest.h"
#import "QJHomeGLModel.h"
#import "QJHomeProductListModel.h"
#import "QJHomeBannerView.h"

#import "QJSearchGameViewController.h"/* 找游戏 */

#import "QJHomeMarketSectionFooterView.h"
#import "QJHomeBannerSectionFooterView.h"
#import "QJHomeMarketingView.h"
#import "QJNestScrollView.h"
#import "QJHomeListTableView.h"
#import "QJHomeLikeRequest.h"
#import "QJConsultDetailViewController.h"
#import "QJUserNoticeView.h"
#import "QJHomeRecommendTabTypeRequest.h"
#import "QJHomeRecommendTabModel.h"

#import "QJMineSetRequest.h"
#import "QJMineSetVersionModel.h"
#import "QJMineSetNewVersionView.h"
#import "QJCommunityScrollView.h"
#import "GKPageScrollView.h"
#import "QJHomeRecommendTabListView.h"
#import "QJDraftEmptyView.h"
#define kTabViewTag   1000
@interface QJHomeViewController ()<GKPageScrollViewDelegate,GKPageTableViewGestureDelegate,UIScrollViewDelegate>



@property (nonatomic, strong) QJCommunityScrollView  *pageScrollView;
@property (nonatomic, strong) NSMutableArray    *childVCs;
@property (nonatomic, strong) UIView            *pageView;
@property (nonatomic, strong) UIScrollView      *scrollView;



@property (nonatomic, strong) NSMutableArray *dataArr;

@property (nonatomic, strong) QJHomeNestTableViewCellModel *nestCellModel;


@property (nonatomic, strong) QJHomeSectionHeadView *headView;

@property (nonatomic, strong) QJHomeNestTableViewCell *nestCell;

@property (nonatomic, strong) QJHomeMarkettingRequest *marketReq;//金刚区请求

@property (nonatomic, strong) QJHomeMarkettingRequest *bannerReq;//banner请求

@property (nonatomic, strong) NSArray *bannerDataArr;//banner数据

@property (nonatomic, strong) NSArray *marketDataArr;//金刚区数据

@property (nonatomic, strong) QJHomeGLRequest *glRequest;//攻略推荐

@property (nonatomic, strong) QJHomeProductListRequest *productListReq;//推荐游戏

@property (nonatomic, strong) NSArray *productListArr;//推荐游戏数据

@property (nonatomic, strong) NSArray *glDataArr;//攻略推荐数据

@property (nonatomic, assign) NSInteger scrollIndex;//0热门攻略，1热门游戏

@property (nonatomic, assign) NSInteger bottomCellOffset;


@property (nonatomic, strong) QJHomeBannerView *bannerView;//轮播图

@property (nonatomic, strong) QJHomeMarketingView *marketView;//金刚区


@property (nonatomic, assign) CGFloat marketViewHeight;//金刚区高度


@property (nonatomic, strong) QJHomeLikeRequest *likeRequest;//点赞接口

@property (nonatomic, strong) QJHomeRecommendTabTypeRequest *recomendTabReq;//推荐tab数量接口

@property (nonatomic, strong) NSArray *tabArrList;

/* GKScrollView回调 */
@property (nonatomic,   copy) void(^listScrollViewDidScroll)(UIScrollView *scrollView);

@end

@implementation QJHomeViewController

- (void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    [self.navBar graduateLeftColor:[UIColor hx_colorWithHexStr:@"#000000" alpha:0.7] ToColor:[UIColor hx_colorWithHexStr:@"000000" alpha:0.0] startPoint:CGPointMake(0, 0) endPoint:CGPointMake(0, 1)];
    
    self.pageScrollView.ceilPointHeight = 0;
    
    self.pageScrollView.contentInsetTop = self.bottomCellOffset;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navBar.backgroundColor = [UIColor clearColor];
    
    [self.view addSubview:self.bannerView];
    [self.bannerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@(Get375Width(255)));
        make.top.equalTo(self.view);
        make.left.equalTo(self.view);
        make.width.equalTo(@(kScreenWidth));
        
    }];
    
    [self.view addSubview:self.marketView];
    
    [self.marketView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.bannerView);
        make.height.equalTo(@(Get375Width(98)));
        make.top.equalTo(self.view).offset(Get375Width(233));
    }];
    
    
    self.headView = [[QJHomeSectionHeadView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, Get375Width(42))];
    [self.view addSubview:self.pageScrollView];
    
    [self.pageScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.top.equalTo(self.view).offset(NavigationBar_Bottom_Y);
    }];
    self.scrollIndex = 0;
     
    [self.childVCs safeAddObject:[QJHomeRecommendTabListView new]];
  
    [self.childVCs enumerateObjectsUsingBlock:^(UIView *vc, NSUInteger idx, BOOL * _Nonnull stop) {
        [self.scrollView addSubview:vc];
      
        vc.frame = CGRectMake(idx * QJScreenWidth,  0, QJScreenWidth, self.scrollView.height);
    }];
    _scrollView.contentSize = CGSizeMake(self.childVCs.count * QJScreenWidth, 0);
    [self.pageScrollView reloadData];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshAllData) name:QJHomeRefreshAllDataNotification object:nil];//下拉刷新
    
    /* 签到button */
//    [self setNavBarRightItemHidden:NO];
    //    [self.navBar.rightButton setTarget:self action:@selector(goToMyGame) forControlEvents:UIControlEventTouchUpInside];
    //    [self.navBar.rightButton setImage:[UIImage imageNamed:@"qj_home_shoubing"] forState:UIControlStateNormal];
    self.bottomCellOffset = Get375Width(233 + 8 + 98) - NavigationBar_Bottom_Y;
    
    [self refreshData];
    [self showUserNoticeView];
    
    [self sendRequestVersion];
}

- (void)sendRequestVersion {
    
    QJMineSetRequest *request = [[QJMineSetRequest alloc] init];
    [request requestConfigVersion_completion:^(BOOL isSuccess, YTKBaseRequest * _Nonnull request) {
        
        if (isSuccess) {
            QJMineSetVersionModel *vModel = [QJMineSetVersionModel modelWithJSON:request.responseJSONObject[@"data"]];
            
            if (!vModel.isNew) {
                QJMineSetNewVersionView *vc = [[QJMineSetNewVersionView alloc] initVersion:vModel.version content:vModel.updateDesc upType:vModel.updateType];
                [vc show];
            }
            
        }
        
    }];
}


- (void)refreshAllData {
    
    [self getHomeDate];
}

//获取tableview初始偏移量
- (void)makeOffsetY {
    CGFloat offsetY = 233 + 8;
    self.bottomCellOffset = 0;
    if (self.marketDataArr.count > 0) {
        CGFloat height = 98;
        if (self.marketDataArr.count > 5) {
            height = height + 66;
            
        }
        offsetY = height + offsetY;
    }
    self.bottomCellOffset = Get375Width(offsetY) - NavigationBar_Bottom_Y ;
    DLog(@"llllllllll = %f",offsetY);
    self.pageScrollView.contentInsetTop = self.bottomCellOffset;
    
}

- (void)configerData {
    
    
    WS(weakSelf)
    
    
    //banner
    if (self.bannerDataArr.count > 0) {
        self.bannerView.bannerClick = ^(QJHomeBannerModel * _Nonnull model) {
            [weakSelf checkLogin:^(BOOL isLogin) {
                if (isLogin) {
                    DLog(@"登录后返回此处刷新");
                    
                }
            }];
        };
        [self.bannerView setImages:self.bannerDataArr];
    }
    
    //金刚区
    if (self.marketDataArr.count > 0) {
        self.marketView.hidden = NO;
        CGFloat height = 98;
        if (self.marketDataArr.count > 5) {
            height = height + 66;
            
        }
        self.marketViewHeight = height;
        
        [self.marketView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self.bannerView);
            make.height.equalTo(@(Get375Width(height)));
            make.top.equalTo(self.view).offset(Get375Width(233));
        }];
        self.marketView.clickIcon = ^(QJHomeMarketModel * _Nonnull model) {
            DLog(@"点击icon");
            [weakSelf checkLogin:^(BOOL isLogin) {
                if (isLogin) {
                    DLog(@"登录后返回此处刷新");
                    /* 跳转找游戏页面 */
                    if ([model.innerType isEqualToString:@"0"]) {
                        QJSearchGameViewController *checkInVC = [[QJSearchGameViewController alloc] init];
                        [weakSelf.navigationController pushViewController:checkInVC animated:YES];
                    }else if ([model.innerType isEqualToString:@"5"]) {
                        [weakSelf goToCheckIn];
                    }else{
                        [[QJAppTool getCurrentViewController].view makeToast:@"敬请期待！"];
                    }
                }
            }];
        };
        self.marketView.array = self.marketDataArr;
    }else{
        self.marketView.hidden = YES;
    }
    
    
   
    self.headView.headViewBlock = ^(NSInteger index) {
        weakSelf.scrollIndex = index;
        [weakSelf.scrollView setContentOffset:CGPointMake(index*kScreenWidth, 0) animated:YES];
    };
    
    
    
    
    
    
}




#pragma mark  getter

 


- (QJHomeMarkettingRequest *)bannerReq {
    if (!_bannerReq) {
        
        _bannerReq = [QJHomeMarkettingRequest new];
        
    }
    return _bannerReq;
}

- (QJHomeMarkettingRequest *)marketReq {
    if (!_marketReq) {
        _marketReq = [QJHomeMarkettingRequest new];
        
    }
    return _marketReq;
}

-(QJHomeGLRequest *)glRequest {
    if (!_glRequest) {
        _glRequest = [QJHomeGLRequest new];
        
        
    }
    return _glRequest;
}


-(QJHomeProductListRequest *)productListReq {
    if (!_productListReq) {
        
        _productListReq = [QJHomeProductListRequest new];
        
    }
    return _productListReq;
}




- (QJHomeBannerView *)bannerView {
    if (!_bannerView) {
        _bannerView = [QJHomeBannerView new];
    }
    return _bannerView;
}

- (QJHomeMarketingView *)marketView {
    if (!_marketView) {
        _marketView = [QJHomeMarketingView new];
        _marketView.hidden =YES;
    }
    return _marketView;
}

- (QJHomeLikeRequest *)likeRequest {
    if (!_likeRequest) {
        _likeRequest = [QJHomeLikeRequest new];
    }
    return _likeRequest;
}

- (QJHomeRecommendTabTypeRequest *)recomendTabReq {
    if (!_recomendTabReq) {
        _recomendTabReq = [QJHomeRecommendTabTypeRequest new];
    }
    return _recomendTabReq;
}

- (QJCommunityScrollView *)pageScrollView {
    if (!_pageScrollView) {
        _pageScrollView = [[QJCommunityScrollView alloc] initWithDelegate:self];
        _pageScrollView.mainTableView.backgroundColor = [UIColor clearColor];
        _pageScrollView.isControlVerticalIndicator = YES;
        _pageScrollView.isAllowListRefresh = YES;
        _pageScrollView.mainTableView.isAllowCustomHit = YES;
        
        _pageScrollView.isAllowCustomHit = YES;
        
    }
    return _pageScrollView;
}
- (UIScrollView *)scrollView {
    if (!_scrollView) {
        CGFloat scrollW = self.view.width;
        CGFloat scrollH = QJScreenHeight - 42*kWScale - NavigationBar_Bottom_Y - (49+ Bottom_iPhoneX_SPACE);
        
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 42*kWScale, scrollW, scrollH)];
        _scrollView.backgroundColor = [UIColor whiteColor];
        _scrollView.pagingEnabled = YES;
        _scrollView.directionalLockEnabled = YES;
//        _scrollView.scrollEnabled = NO;
        _scrollView.bounces = NO;
        _scrollView.delegate = self;
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.showsHorizontalScrollIndicator = NO;
    }
    return _scrollView;
}
- (UIView *)pageView {
    if (!_pageView) {
        _pageView = [UIView new];
        _pageView.backgroundColor = [UIColor clearColor];
        [_pageView addSubview:self.headView];
        [_pageView addSubview:self.scrollView];
    }
    return _pageView;
}
- (NSMutableArray *)childVCs {
    if (!_childVCs) {
        _childVCs = [NSMutableArray array];
    }
    return _childVCs;
}

 
#pragma mark  ------- method

- (void)refreshData {
    
    [self getHomeDate];
}

- (void)getHomeDate {
    //    [self.view showHUDIndicator];
  
    
   
    
    WS(weakSelf)
    self.bannerReq.dic = @{@"homeLayoutTypeCode" : @"1"};
    _bannerReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            weakSelf.bannerDataArr = [NSArray modelArrayWithClass:[QJHomeBannerModel class] json:EncodeArrayFromDic(request.responseJSONObject, @"data")];
            
            [weakSelf configerData];
            
        }
    };
    
    
    [self.bannerReq start];
    
    self.marketReq.dic = @{@"homeLayoutTypeCode" : @"3"};
    
    self.marketReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        if (ResponseSuccess) {
            weakSelf.marketDataArr = [NSArray modelArrayWithClass:[QJHomeMarketModel class] json:EncodeArrayFromDic(request.responseJSONObject, @"data")];
        }
        [weakSelf configerData];
        [weakSelf makeOffsetY];
    };
    
    self.marketReq.failureCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        [weakSelf configerData];
        [weakSelf makeOffsetY];
    };
    
    [self.marketReq start];
    
    self.recomendTabReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        

        if (ResponseSuccess) {
            weakSelf.tabArrList = [NSArray modelArrayWithClass:[QJHomeRecommendTabModel class] json:EncodeArrayFromDic(request.responseJSONObject, @"data")];
            //列表
         
            if (weakSelf.tabArrList.count > 0)
            {
                weakSelf.headView.arrays = weakSelf.tabArrList;
                [weakSelf.headView updateBtn:weakSelf.scrollIndex];
                [weakSelf.childVCs removeAllObjects];
//                [weakSelf.scrollView removeAllSubviews];
                for (int i = 0; i < self.tabArrList.count; i++) {
                    QJHomeRecommendTabListView *view = [weakSelf.scrollView viewWithTag:(kTabViewTag + i)];
                    if (!view) {
                        view = [QJHomeRecommendTabListView new];
                        view.tag = kTabViewTag + i;
                    }
                    view.endRefresh = ^{
                       
                    };
                    
//                    view.delegate = weakSelf;
                    view.tabModel = [weakSelf.tabArrList safeObjectAtIndex:i];
                    
                    [weakSelf.childVCs safeAddObject:view];
                    
                }
               
                
                [weakSelf.childVCs enumerateObjectsUsingBlock:^(UIView *vc, NSUInteger idx, BOOL * _Nonnull stop) {
                    [weakSelf.scrollView addSubview:vc];
                  
                    vc.frame = CGRectMake(idx * QJScreenWidth,  0, QJScreenWidth, weakSelf.scrollView.height);
                }];
                weakSelf.scrollView.contentSize = CGSizeMake(self.childVCs.count * QJScreenWidth, 0);
                [weakSelf.pageScrollView reloadData];
            }
           
           
        }
        
        
        
    };
    
    self.recomendTabReq.failureCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        [QJAppTool showToast:ResponseFailToastMsg];
        QJDraftEmptyView *emptyView = [QJDraftEmptyView new];
        emptyView.string = @"无网络数据，请下拉刷新重试";
        emptyView.offsetY = 72*kWScale;
        emptyView.imageStr = @"empty_no_internet";
        QJHomeRecommendTabListView *view = weakSelf.childVCs.firstObject;
        if([view isKindOfClass:[QJHomeRecommendTabListView class]]){
            [view.leftTableView tableViewDisplayWhenHaveNoDataWithView:emptyView ifNecessaryForRowCount:0];
        }
       
    };
    
    [self.recomendTabReq start];
    
    
    
}

#pragma mark - GKPageScrollView
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

- (BOOL)mainTableViewGestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

- (UIView *)headerViewInPageScrollView:(GKPageScrollView *)pageScrollView {
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 0)];
    return header;
}

- (NSArray<id<GKPageListViewDelegate>> *)listViewsInPageScrollView:(GKPageScrollView *)pageScrollView {
    return self.childVCs;
}

- (UIView *)pageViewInPageScrollView:(GKPageScrollView *)pageScrollView {
    return self.pageView;
}

- (void)listViewDidScrollCallback:(void (^)(UIScrollView * _Nonnull))callback {
        self.listScrollViewDidScroll = callback;
    
}

- (void)mainTableViewDidScroll:(UIScrollView *)scrollView isMainCanScroll:(BOOL)isMainCanScroll {
    
     CGFloat offset = scrollView.contentOffset.y + self.bottomCellOffset;
    if (self.marketViewHeight > 10) {
        CGFloat alp = offset*1/self.marketViewHeight;
        if (alp <= 0 ) {
            alp = 0;
        }else if (alp >= 1) {
            alp = 1;
        }
        
        self.marketView.collectionView.alpha = 1 - alp;
    }
}
 

 
- (void)listScrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat x = scrollView.contentOffset.x;
    NSInteger index = x/kScreenWidth;
     
    if (x == index*kScreenWidth) {
        DLog(@"偏移量===== %d",index);
        
    }
}
 



 


 


#pragma  mark  ----- UIScrollViewDelegate

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [self.pageScrollView horizonScrollViewWillBeginScroll];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    [self.pageScrollView horizonScrollViewDidEndedScroll];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    [self.pageScrollView horizonScrollViewDidEndedScroll];
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    !self.listScrollViewDidScroll ? : self.listScrollViewDidScroll(scrollView);
    CGFloat x = scrollView.contentOffset.x;
    NSInteger index = x/kScreenWidth;
    if (x == index*kScreenWidth) {
        DLog(@"偏移量===== %ld",index);
        self.scrollIndex = index;
        
        [self.headView updateBtn:index];
    }
    
   
    
}


 


 



/**
 * @author: zjr
 * @date: 2022-6-27
 * @desc: button点击事件-签到页面
 */
- (void)goToCheckIn{
    WS(weakSelf)
    [self checkLogin:^(BOOL isLogin) {
        if (isLogin) {
            QJCheckInViewController *checkInVC = [[QJCheckInViewController alloc] init];
            [weakSelf.navigationController pushViewController:checkInVC animated:YES];
        }
    }];
}

- (void)goToGame:(id)dto{
    QJProductDetailViewController *checkInVC = [[QJProductDetailViewController alloc] init];
    NSDictionary *dic = [dto modelToJSONObject];
    QJSearchGameDetailModel *model = [QJSearchGameDetailModel modelWithJSON:dic];
    if (model) {
        checkInVC.model = model;
        [self.navigationController pushViewController:checkInVC animated:YES];
    }
   
}

- (void)goToMyGame{
    QJMyBoxListViewController *myBoxListVC = [[QJMyBoxListViewController alloc] init];
    [self.navigationController pushViewController:myBoxListVC animated:YES];
}

#pragma  mark  ----- 隐私弹窗
- (void)showUserNoticeView {
    NSString *isDefalult = LSUserDefaultsGET(kQJUserNoticeView);
    if(![kCheckStringNil(isDefalult) isEqualToString:@"1"]) {
        QJUserNoticeView *userView = [[QJUserNoticeView alloc] initWithFrame:CGRectZero style:QJUserNoticeViewStyleDefault];
        [userView showAnimation:self.navigationController];
    }
}

#pragma mark ----- 状态栏颜色
/// 状态栏颜色
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    if (@available(iOS 13.0, *)) {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDarkContent];
        
    } else {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    }
}


@end
