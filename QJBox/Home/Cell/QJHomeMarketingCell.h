//
//  QJHomeMarketingCell.h
//  QJBox
//
//  Created by wxy on 2022/6/14.
//

#import "QJBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN
@class  QJHomeMarketModel;
/// 营销专栏cell 用来展示功能入口的cell
@interface QJHomeMarketingCell : QJBaseTableViewCell






@end

#define QJ_IMAGEWIDTH   48*kWScale
#define QJ_IMAGEHEIGHT  48*kWScale
#define QJ_TOP_SPACE    0
#define QJ_MIDDLE_SPACE 4*kWScale

@interface QJHomeMarkingCollectionCell : UICollectionViewCell


@property (nonatomic, strong) UIImageView *iconImageView;


@property (nonatomic, strong) UILabel *subLabel;


@property (nonatomic, strong) UIImageView *cornerMarkImageView;


@property (nonatomic, strong) QJHomeMarketModel *model;

@end


@interface QJHomeMarketingCellModel : QJBaseCellModel


@property (nonatomic, copy) void(^clickIcon)(QJHomeMarketModel *model);


@end

NS_ASSUME_NONNULL_END
