//
//  QJHomeMainCell.h
//  QJBox
//
//  Created by wxy on 2022/6/14.
//

#import "QJBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@class  QJSearchGameDetailModel;
/// 首页主列表cell  游戏
@interface QJHomeMainCell : QJBaseTableViewCell


@property (nonatomic, strong) QJSearchGameDetailModel *model;


@end


@interface QJHomeMainCellModel : QJBaseCellModel




@end

NS_ASSUME_NONNULL_END
