//
//  QJHomeNestTableViewCell.m
//  QJBox
//
//  Created by wxy on 2022/6/23.
//

#import "QJHomeNestTableViewCell.h"
#import "QJNestScrollView.h"
#import "QJNestTableView.h"
#import "QJHomeMainCell.h"
#import "QJHomeGLCell.h"
 
#import "QJHomeProductListModel.h"
#import "QJHomeGLModel.h"
#import "UITableView+Common.h"
#import "QJHomeRecommendTabListView.h"
#import "QJHomeRecommendTabModel.h"


#define kTabViewTag   1000

@interface QJHomeNestTableViewCell ()<UIScrollViewDelegate,QJHomeRecommendTabListViewDelegate>

@property (nonatomic, strong) UIScrollView *bgScrollView;

@property (nonatomic, strong) NSArray *tabArr;

@end





@implementation QJHomeNestTableViewCell



- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self.contentView addSubview:self.bgScrollView];
      
 
        [self.bgScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.contentView);
        }];
        
 
      
        
        
    }
    return self;
}

 
 
- (void)setCurrentPage:(NSInteger)currentPage {
    _currentPage = currentPage;
    [self.bgScrollView setContentOffset:CGPointMake(currentPage *kScreenWidth, 0) animated:YES];
    
}


- (void)layoutSubviews {
    [super layoutSubviews];
    self.bgScrollView.contentSize = CGSizeMake(self.tabArr.count*kScreenWidth, self.height);
}

#pragma mark ------- getter

- (UIScrollView *)bgScrollView {
    if (!_bgScrollView) {
        _bgScrollView = [[UIScrollView alloc]init];
        _bgScrollView.pagingEnabled = YES;
        _bgScrollView.bounces = NO;
        _bgScrollView.delegate = self;
        _bgScrollView.showsHorizontalScrollIndicator = NO;
    }
    return _bgScrollView;
}

 

 


 


- (void)setCellModel:(QJBaseCellModel *)cellModel {
    [super setCellModel:cellModel];
    QJHomeNestTableViewCellModel *model = (QJHomeNestTableViewCellModel *)cellModel;
    self.currentPage = model.currentPage;
    self.tabArr = model.tabArr;
    if (self.tabArr.count != model.tabArr.count) {//数量不一样了重新布局
        [self.bgScrollView removeAllSubviews];
    }
    
    for (int i = 0; i < self.tabArr.count; i++) {
        QJHomeRecommendTabListView *view = [self.bgScrollView viewWithTag:(kTabViewTag + i)];
        if (!view) {
            view = [QJHomeRecommendTabListView new];
            view.tag = kTabViewTag + i;
        }
        view.endRefresh = ^{
            if (self.endRefresh) {
                self.endRefresh();
            }
        };
        [self.bgScrollView addSubview:view];
        view.delegate = self;
        view.tabModel = [self.tabArr safeObjectAtIndex:i];
        
        if (i == 0) {
            [view mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.bgScrollView);
                make.height.equalTo(self.bgScrollView);
                make.width.equalTo(@(kScreenWidth));
                make.top.equalTo(self.bgScrollView);
            }];
        }else {
            UIView *leftView = [self.bgScrollView viewWithTag:(kTabViewTag + i-1)];
            if (leftView) {
                [view mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.equalTo(leftView.mas_right);
                    make.height.equalTo(self.bgScrollView);
                    make.width.equalTo(@(kScreenWidth));
                    make.top.equalTo(self.bgScrollView);
                }];
            }
            
        }
    }
    
    
    
    [self.bgScrollView setContentOffset:CGPointMake(self.currentPage *kScreenWidth, 0) animated:YES];
    
    
 
}

#pragma mark  -------- method




 



 

#pragma mark  -----UIScrollViewDelegate
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    
    
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    CGFloat x = scrollView.contentOffset.x;
    
    
    NSInteger index = x/kScreenWidth;
     
    if (x == index*kScreenWidth) {
        DLog(@"偏移量===== %d",index);
        QJHomeRecommendTabListView *view = [self.bgScrollView viewWithTag:kTabViewTag + index];
        
            DLog(@"YYYYYYYYYYYYY  %.2F",view.leftTableView.contentOffset.y);
            if (view.leftTableView.contentOffset.y == 0) {
                view.leftTableView.contentOffset = CGPointMake(0, 1);
            }
            QJHomeNestTableViewCellModel *model = (QJHomeNestTableViewCellModel *)self.cellModel;
            self.currentPage = index;
            model.currentPage = index;
            if (model.currentPageBlock) {
                model.currentPageBlock(index);
            }
       
    }
   
    
    
}


- (void)homeRecommendTabListScrollViewDidScroll:(UIScrollView *)scrollView {
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(homeRecommendTablviewScrollViewDidScroll:)]) {
        [self.delegate homeRecommendTablviewScrollViewDidScroll:scrollView];
    }
}


@end



@implementation QJHomeNestTableViewCellModel

- (NSString *)identifier {
    return [QJHomeNestTableViewCell cellIdentifier];
}

@end
