//
//  QJHomeGLCell.h
//  QJBox
//
//  Created by wxy on 2022/7/6.
//

#import "QJBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN
@class QJHomeGLModel;
/// 首页攻略cell
@interface QJHomeGLCell : QJBaseTableViewCell



@end


@interface QJHomeGLCellModel : QJBaseCellModel

@property (nonatomic, copy) void (^likeBtnClick)(QJHomeGLModel *cellData);

@property (nonatomic, copy) void (^videoBtnClick)(QJHomeGLModel *cellData);


@end
NS_ASSUME_NONNULL_END
