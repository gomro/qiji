//
//  QJHomeGLCell.m
//  QJBox
//
//  Created by wxy on 2022/7/6.
//

#import "QJHomeGLCell.h"
#import "QJHomeGLModel.h"
@interface QJHomeGLCell ()

@property (nonatomic, strong) UIView *whiteBgView;//白色底图

@property (nonatomic, strong) UIImageView *productImageView;

@property (nonatomic, strong) UILabel *titleLabel;//标题

@property (nonatomic, strong) UIImageView *authorImageView;//作者头像

@property (nonatomic, strong) UILabel *authorLabel;//作者标签

@property (nonatomic, strong) UIImageView *logoImageView;//徽章

@property (nonatomic, strong) UIImageView *browseImageView;

@property (nonatomic, strong) UILabel *browseLabel;//浏览人数

@property (nonatomic, strong) UIButton *starBtn;//

@property (nonatomic, strong) UILabel *starLabel;//点赞数

@property (nonatomic, strong) UIImageView *videoImageView;//视频封面图片


@property (nonatomic, strong) UIView  *lineView;

@property (nonatomic, strong) UIView *labelBgView;

@property (nonatomic, strong) UILabel *bottomLabel;

@property (nonatomic, strong) UIButton *videoImageBtn;


@end


@implementation QJHomeGLCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setUpSubView];
    }
    return self;
}


#pragma mark  ---- UI
- (void)setUpSubView {
    self.contentView.backgroundColor = [UIColor grayColor];
    [self.contentView addSubview:self.whiteBgView];
    [self.whiteBgView addSubview:self.productImageView];
    [self.whiteBgView addSubview:self.titleLabel];
    [self.whiteBgView addSubview:self.authorLabel];
    [self.whiteBgView addSubview:self.authorImageView];
    [self.whiteBgView addSubview:self.logoImageView];
    [self.whiteBgView addSubview:self.browseImageView];
    [self.whiteBgView addSubview:self.browseLabel];
    [self.whiteBgView addSubview:self.starBtn];
    [self.whiteBgView addSubview:self.starLabel];
    [self.whiteBgView addSubview:self.videoImageView];
    [self.whiteBgView addSubview:self.lineView];
    [self.productImageView addSubview:self.labelBgView];
    [self.labelBgView addSubview:self.bottomLabel];
    
    [self.whiteBgView addSubview:self.videoImageBtn];
    
    
    [self.whiteBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.contentView);
    }];
    
    [self.productImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.whiteBgView).offset(-16);
        make.top.equalTo(self.whiteBgView).offset(16);
        make.width.equalTo(@(114*kWScale));
        make.height.equalTo(@(76*kWScale));
    }];
    
    [self.videoImageBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.whiteBgView).offset(-16);
            make.top.equalTo(self.whiteBgView).offset(16);
            make.width.equalTo(@(114*kWScale));
            make.height.equalTo(@(76*kWScale));
    }];
    
    [self.labelBgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.left.right.equalTo(self.productImageView);
            make.height.equalTo(@(16*kWScale));
    }];
    
    [self.bottomLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(self.labelBgView);
            make.left.right.equalTo(self.labelBgView);
    }];
    
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.whiteBgView).offset(17);
        make.top.equalTo(self.productImageView).offset(0);
        make.right.equalTo(self.productImageView.mas_left).offset(-16);
        
    }];
    
    [self.authorImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.titleLabel);
        make.centerY.equalTo(self.starBtn);
        make.width.height.equalTo(@(15*kWScale));
    }];
    
    [self.authorLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.authorImageView.mas_right).offset(4);
        make.centerY.equalTo(self.starBtn);
        
    }];
    
    
    [self.authorLabel setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
    
    [self.logoImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.authorImageView.mas_right);
        make.width.equalTo(@(12*kWScale));
        make.height.equalTo(@(7*kWScale));
        make.bottom.equalTo(self.authorImageView.mas_bottom).offset(-1);
    }];
    
    [self.browseImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.browseLabel.mas_left).offset(-5);
        make.centerY.equalTo(self.browseLabel);
        make.width.equalTo(@(16*kWScale));
        make.height.equalTo(@(16*kWScale));
    }];
    
    [self.browseLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.starLabel);
        make.right.equalTo(self.starBtn.mas_left).offset(-18);
        
        
    }];
    
    [self.browseLabel setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
    
    [self.starBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.contentView).offset(-17);
        make.width.equalTo(@(16*kWScale));
        make.height.equalTo(@(16*kWScale));
        make.right.equalTo(self.productImageView.mas_left).offset(-39);
    }];
    
    [self.starLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.starBtn.mas_right).offset(2*kWScale);
        make.centerY.equalTo(self.starBtn);
        
    }];
    
    [self.starLabel setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
    
    
    
    [self.videoImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self.productImageView);
        make.width.height.equalTo(@(24*kWScale));
    }];
    
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.whiteBgView);
        make.height.equalTo(@1);
        make.left.equalTo(self.whiteBgView).offset(16.5);
        make.right.equalTo(self.whiteBgView).offset(-18.5);
    }];
    
    
    
    
}


#pragma mark  ------- getter

- (UIView *)whiteBgView {
    if (!_whiteBgView) {
        _whiteBgView = [UIView new];
        _whiteBgView.backgroundColor = [UIColor whiteColor];
        
    }
    return _whiteBgView;
}

- (UIImageView *)productImageView {
    if (!_productImageView) {
        _productImageView = [UIImageView new];
        _productImageView.layer.cornerRadius = 4;
        _productImageView.layer.masksToBounds = YES;
        _productImageView.contentMode = UIViewContentModeScaleAspectFill;
    }
    return _productImageView;
}


- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.font = FontRegular(14);
        _titleLabel.textColor = kColorWithHexString(@"#000000");
        _titleLabel.numberOfLines = 2;
    }
    return _titleLabel;
}

- (UILabel *)authorLabel {
    if (!_authorLabel) {
        _authorLabel = [UILabel new];
        _authorLabel.font = FontMedium(11);
        _authorLabel.textColor = [UIColor colorWithHexString:@"#919599"];
        
    }
    return _authorLabel;
}

- (UIImageView *)logoImageView {
    if (!_logoImageView) {
        _logoImageView = [UIImageView new];
        _logoImageView.image = [UIImage imageNamed:@"qj_home_vlogo"];
    }
    return _logoImageView;
}


- (UILabel *)browseLabel {
    if (!_browseLabel) {
        _browseLabel = [UILabel new];
        _browseLabel.font = FontMedium(11);
        _browseLabel.textAlignment = NSTextAlignmentLeft;
        _browseLabel.textColor = [UIColor colorWithHexString:@"#919599"];
        _browseLabel.hidden = YES;
    }
    return _browseLabel;
}

- (UIImageView *)browseImageView {
    if (!_browseImageView) {
        _browseImageView = [UIImageView new];
        _browseImageView.image = [UIImage imageNamed:@"home_preview"];
        _browseImageView.hidden = YES;
        
    }
    return _browseImageView;
}

- (UILabel *)starLabel {
    if (!_starLabel) {
        _starLabel = [UILabel new];
        _starLabel.font = FontMedium(11);
        _starLabel.text = @"K";
        _starLabel.textColor = [UIColor colorWithHexString:@"#919599"];
        _starLabel.textAlignment = NSTextAlignmentLeft;
    }
    return _starLabel;
}

- (UIButton *)starBtn {
    if (!_starBtn) {
        _starBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_starBtn addTarget:self action:@selector(starBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [_starBtn setBackgroundImage:[UIImage imageNamed:@"qj_star"] forState:UIControlStateNormal];
        [_starBtn setBackgroundImage:[UIImage imageNamed:@"qj_star_selected"] forState:UIControlStateSelected];
        
        [_starBtn setEnlargeEdgeWithTop:5 right:5 bottom:5 left:5];
    }
    return _starBtn;
}


-(UIImageView *)videoImageView {
    if (!_videoImageView) {
        _videoImageView = [UIImageView new];
        _videoImageView.image = [UIImage imageNamed:@"qj_home_play"];
        _videoImageView.hidden = YES;
    }
    return _videoImageView;
}

- (UIView *)lineView {
    if (!_lineView) {
        _lineView = [UIView new];
        _lineView.backgroundColor = [UIColor colorWithHexString:@"#EBEDF0"];
    }
    return _lineView;
}

- (UIImageView *)authorImageView {
    if (!_authorImageView) {
        _authorImageView = [UIImageView new];
        _authorImageView.layer.cornerRadius = 7.5;
        _authorImageView.layer.masksToBounds = YES;
    }
    return _authorImageView;
}

- (UIView *)labelBgView {
    if (!_labelBgView) {
        _labelBgView = [UIView new];
        _labelBgView.backgroundColor = [UIColor hx_colorWithHexStr:@"000000" alpha:0.6];
        _labelBgView.layer.cornerRadius = 2;
    }
    return _labelBgView;
}

- (UILabel *)bottomLabel {
    if (!_bottomLabel) {
        _bottomLabel = [UILabel new];
        _bottomLabel.font = FontMedium(10);
        _bottomLabel.textColor = [UIColor hx_colorWithHexStr:@"ffffff" alpha:0.6];
        _bottomLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _bottomLabel;
}

- (UIButton *)videoImageBtn {
    if(!_videoImageBtn){
        _videoImageBtn = [UIButton new];
        [_videoImageBtn addTarget:self action:@selector(videoImageBtnAction) forControlEvents:UIControlEventTouchUpInside];
        _videoImageBtn.backgroundColor = [UIColor clearColor];
        _videoImageBtn.hidden = YES;
    }
    return _videoImageBtn;
}

- (void)setCellModel:(QJBaseCellModel *)cellModel {
    [super setCellModel:cellModel];
    
    if ([cellModel.relativeData isKindOfClass:[QJHomeGLModel class]]) {
        QJHomeGLModel *model = cellModel.relativeData;
        self.productImageView.imageURL = [NSURL URLWithString:[model.coverImage checkImageUrlString]];
        [self.productImageView setImageWithURL:[NSURL URLWithString:[model.coverImage checkImageUrlString]] placeholder:[UIImage imageNamed:@"home_list"]];
        self.titleLabel.text = model.title?:@"";
        self.authorLabel.text = model.userNickname?:@"";
       
//        self.browseLabel.text = [NSString changeCountFromString:model.reviewCount?:@""];
        self.starLabel.text = [NSString changeCountFromString:model.likeCount?:@""];
        self.starBtn.selected = model.liked;
        [self.authorImageView setImageWithURL:[NSURL URLWithString:[model.userCoverImage checkImageUrlString]] placeholder:[UIImage imageNamed:@""]];
        if (model.videoAddress.length > 0) {
            self.videoImageView.hidden = NO;
            self.videoImageBtn.hidden = NO;
        }else{
            self.videoImageView.hidden = YES;
            self.videoImageBtn.hidden = YES;
        }
        
        if (model.infoTag.length > 0) {
            self.labelBgView.hidden = NO;
            self.bottomLabel.hidden = NO;
            self.bottomLabel.text = model.infoTag;
            
        }else{
            self.bottomLabel.hidden = YES;
            self.labelBgView.hidden = YES;
        }
        
    }
    
}


#pragma mark  ------- action

- (void)starBtnAction:(UIButton *)sender {
    QJHomeGLCellModel *model = (QJHomeGLCellModel *)self.cellModel;
    if (model.likeBtnClick) {
        model.likeBtnClick(model.relativeData);
    }
}

- (void)videoImageBtnAction {
    QJHomeGLCellModel *model = (QJHomeGLCellModel *)self.cellModel;
    if (model.videoBtnClick) {
        model.videoBtnClick(model.relativeData);
    }
}
@end




@implementation QJHomeGLCellModel


- (NSString *)identifier {
    return [QJHomeGLCell cellIdentifier];
}

@end
