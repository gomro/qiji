//
//  QJHomeNestTableViewCell.h
//  QJBox
//
//  Created by wxy on 2022/6/23.
//


#import "QJBaseTableViewCell.h"
#import "QJHomeRecommendTabModel.h"
NS_ASSUME_NONNULL_BEGIN

@protocol QJHomeNestTableViewCellDelegate <NSObject>

- (void)homeRecommendTablviewScrollViewDidScroll:(UIScrollView *)scrollView;


@end

@interface QJHomeNestTableViewCell : QJBaseTableViewCell


@property (nonatomic, assign) NSInteger currentPage;


@property (nonatomic, copy) void(^endRefresh)(void);


@property (nonatomic, weak) id<QJHomeNestTableViewCellDelegate>delegate;

@end



@interface QJHomeNestTableViewCellModel : QJBaseCellModel

@property (nonatomic, assign) NSInteger currentPage;

@property (nonatomic, assign) BOOL isLoadMore;


@property (nonatomic, strong) NSArray *tabArr;


@property (nonatomic, copy) void (^currentPageBlock)(NSInteger currentPage);

@property (nonatomic, copy) void (^loadMoreDateBlock)(NSInteger currentPage, UITableView *tableView,NSDictionary *parm);


@end

NS_ASSUME_NONNULL_END
