//
//  QJHomeMarketingCell.m
//  QJBox
//
//  Created by wxy on 2022/6/14.
//

#import "QJHomeMarketingCell.h"
#import "QJHomeMarketModel.h"
@interface QJHomeMarketingCell ()<UICollectionViewDelegate,UICollectionViewDataSource>


@property (nonatomic, strong) UICollectionView *collectionView;

@property (nonatomic, strong) NSArray <QJHomeMarketModel*>*array;

@end



@implementation QJHomeMarketingCell
  

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self.contentView addSubview:self.collectionView];
        self.contentView.backgroundColor = [UIColor whiteColor];
        [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self.contentView);
            make.top.equalTo(self.contentView).offset(0);
            make.bottom.equalTo(self.contentView).offset(0);
        }];
        
    }
    return self;
    
}


#pragma mark ------ getter

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
        
        layout.minimumLineSpacing = 0;
        layout.minimumInteritemSpacing = 0;
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        [_collectionView registerClass:[QJHomeMarkingCollectionCell class] forCellWithReuseIdentifier:@"QJHomeMarkingCollectionCell"];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.showsHorizontalScrollIndicator = NO;
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.scrollEnabled = NO;
        
    }
    return _collectionView;
}


- (void)setCellModel:(QJBaseCellModel *)cellModel {
    [super setCellModel:cellModel];
    if ([cellModel.relativeData isKindOfClass:[NSArray class]]) {
        NSArray *arr = cellModel.relativeData;
        self.array = arr;
        
        [self.collectionView reloadData];
    }
}



#pragma mark  -------- UICollectionViewDelegate,UICollectionViewDataSource


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.array.count;
}


- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    QJHomeMarkingCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"QJHomeMarkingCollectionCell" forIndexPath:indexPath];
    cell.model = [self.array safeObjectAtIndex:indexPath.item];
    return cell;
    
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    QJHomeMarketingCellModel *model = (QJHomeMarketingCellModel *)self.cellModel;
    if (model.clickIcon) {
        NSInteger index = indexPath.item;
        model.clickIcon([self.array safeObjectAtIndex:index]);
    }
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    //小于等于四个以四个为一行布局
    if (self.array.count <= 4) {
        return CGSizeMake(kScreenWidth/4.0, 66*kWScale);
    }else if (self.array.count > 4) {
        return CGSizeMake(kScreenWidth/5.0, 66*kWScale);
    }
    
    return CGSizeMake(kScreenWidth/4.0, 66*kWScale);;
    
}


@end



@implementation QJHomeMarkingCollectionCell


- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self.contentView addSubview:self.iconImageView];
        [self.contentView addSubview:self.subLabel];
        [self.contentView addSubview:self.cornerMarkImageView];
        
        [self.iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.contentView);
            make.width.height.equalTo(@(QJ_IMAGEWIDTH));
            make.top.equalTo(self.contentView).offset(QJ_TOP_SPACE);
        }];
        
        [self.subLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.contentView);
            make.top.equalTo(self.iconImageView.mas_bottom).offset(QJ_MIDDLE_SPACE);
            make.height.equalTo(@(14*kWScale));
        }];
        
        [self.cornerMarkImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.iconImageView.mas_centerX);
            make.width.equalTo(@(45*kWScale));
            make.height.equalTo(@(14*kWScale));
            make.top.equalTo(self.contentView).offset(QJ_TOP_SPACE-2);
        }];
        
    }
    return self;
}



#pragma mark  ----- getter

- (UIImageView *)iconImageView {
    if (!_iconImageView) {
        _iconImageView = [UIImageView new];
        
    }
    return _iconImageView;
}

- (UILabel *)subLabel {
    if (!_subLabel) {
        _subLabel = [UILabel new];
        _subLabel.textColor = [UIColor colorWithHexString:@"000000"];
        _subLabel.font = FontRegular(12);
        _subLabel.textAlignment = NSTextAlignmentCenter;
        _subLabel.text = @"";
    }
    return _subLabel;
}


- (UIImageView *)cornerMarkImageView {
    if (!_cornerMarkImageView) {
        _cornerMarkImageView = [UIImageView new];
        
    }
    return _cornerMarkImageView;
}


- (void)setModel:(QJHomeMarketModel *)model {
    _model = model;
    self.subLabel.text = model.title ?:@"";
    self.iconImageView.imageURL = [NSURL URLWithString:[model.imgUrl checkImageUrlString]];
    
}




@end


@implementation QJHomeMarketingCellModel

- (NSString *)identifier {
    return [QJHomeMarketingCell cellIdentifier];
}
 

@end
