//
//  QJHomeCarouselCell.m
//  QJBox
//
//  Created by wxy on 2022/6/14.
//

#import "QJHomeCarouselCell.h"
#import "SPCycleScrollView.h"
#import "QJHomeBannerModel.h"
@interface QJHomeCarouselCell ()


@property (nonatomic, strong) SPCycleScrollView *cycleImageView;


@end


@implementation QJHomeCarouselCell


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self.contentView addSubview:self.cycleImageView];
        [self.cycleImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView);
            make.right.equalTo(self.contentView);
            make.top.bottom.equalTo(self.contentView);
        }];
        
    }
    return self;
}


#pragma mark -------- setter && getter

- (SPCycleScrollView *)cycleImageView {
    if (!_cycleImageView) {
        _cycleImageView = [[SPCycleScrollView alloc]init];
        _cycleImageView.duration = 3.0;
        _cycleImageView.pageControl.hidden = YES;
    }
    return _cycleImageView;
}



- (void)setCellModel:(QJBaseCellModel *)cellModel {
    [super setCellModel:cellModel];
    if ([cellModel.relativeData isKindOfClass:[NSArray class]]) {
        NSArray *images = cellModel.relativeData;
        NSMutableArray *muArr = [NSMutableArray array];
        for (QJHomeBannerModel *model in images) {
            if (!IsStrEmpty(model.imgUrl)) {
                NSString *str = [model.imgUrl checkImageUrlString];
                [muArr safeAddObject:str];
            }
        }
        self.cycleImageView.urlImages = muArr.copy;
        
        
        
        self.cycleImageView.clickedImageBlock = ^(NSUInteger index) {
            QJHomeCarouselCellModel *model = (QJHomeCarouselCellModel *)cellModel;
            NSArray *modes = model.relativeData;
            if (model.bannerClick) {
                model.bannerClick([modes safeObjectAtIndex:index]);
            }
        };
    }
    
}





@end


@implementation QJHomeCarouselCellModel


- (NSString *)identifier {
    return [QJHomeCarouselCell cellIdentifier];
}
 

@end
