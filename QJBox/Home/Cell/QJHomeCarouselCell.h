//
//  QJHomeCarouselCell.h
//  QJBox
//
//  Created by wxy on 2022/6/14.
//

#import "QJBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@class QJHomeCarouselCellModel;
/// 轮播图cell
@interface QJHomeCarouselCell : QJBaseTableViewCell



@end

@class QJHomeBannerModel;

@interface QJHomeCarouselCellModel : QJBaseCellModel
/// 获取点击的下标
@property (nonatomic, copy) void(^bannerClick)(QJHomeBannerModel *model);


@end



NS_ASSUME_NONNULL_END
