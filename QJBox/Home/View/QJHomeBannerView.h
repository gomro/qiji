//
//  QJHomeBannerView.h
//  QJBox
//
//  Created by wxy on 2022/7/14.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class QJHomeBannerModel;
/// 头部banner图
@interface QJHomeBannerView : UIView

@property (nonatomic, copy) void(^bannerClick)(QJHomeBannerModel *model);

- (void)setImages:(NSArray *)urls;

@property (nonatomic, assign) BOOL autoScroll;
@end

NS_ASSUME_NONNULL_END
