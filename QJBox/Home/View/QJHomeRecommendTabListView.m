//
//  QJHomeRecommendTabListView.m
//  QJBox
//
//  Created by wxy on 2022/9/19.
//

#import "QJHomeRecommendTabListView.h"
#import "QJHomeGLCell.h"
#import "QJHomeMainCell.h"
#import "QJHomeProductListModel.h"
#import "QJHomeGLModel.h"
#import "UITableView+Common.h"
#import "QJHomeGLRequest.h"
#import "QJHomeProductListRequest.h"
#import "QJConsultDetailViewController.h"
#import "QJHomeLikeRequest.h"
#import "QJProductDetailViewController.h"
#import "QJMyBoxListViewController.h"
#import "GKPageScrollView.h"
#import "QJDraftEmptyView.h"
@interface QJHomeRecommendTabListView ()<UITableViewDataSource, UITableViewDelegate,GKPageListViewDelegate>

@property (nonatomic, assign) NSInteger dataPage;

@property (nonatomic, strong) QJHomeGLRequest *glRequest;//攻略推荐

@property (nonatomic, strong) QJHomeProductListRequest *productListReq;//推荐游戏

@property (nonatomic, strong) QJHomeLikeRequest *likeRequest;//点赞接口

/* GKScroll回调 */
@property (nonatomic, copy) void(^listScrollViewScrollBlock)(UIScrollView *scrollView);

@end



@implementation QJHomeRecommendTabListView

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.dataPage = 1;
        [self addSubview:self.leftTableView];
        [self.leftTableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
        }];
     
        
        
        
    }
    return self;
}

 

 
 



#pragma mark ------- method
- (void)goToGame:(id)dto{
    QJProductDetailViewController *checkInVC = [[QJProductDetailViewController alloc] init];
    NSDictionary *dic = [dto modelToJSONObject];
    QJSearchGameDetailModel *model = [QJSearchGameDetailModel modelWithJSON:dic];
    if (model) {
        checkInVC.model = model;
        UIViewController *vc = [QJAppTool getCurrentViewController];
        [vc.navigationController pushViewController:checkInVC animated:YES];
    }
    
}

- (void)goToMyGame{
    QJMyBoxListViewController *myBoxListVC = [[QJMyBoxListViewController alloc] init];
    UIViewController *vc = [QJAppTool getCurrentViewController];
    [vc.navigationController pushViewController:myBoxListVC animated:YES];
}

#pragma mark ------ getter && setter

- (NSArray *)dataArr {
    if(!_dataArr){
        _dataArr = [NSArray array];
    }
    return _dataArr;
}

-(QJHomeGLRequest *)glRequest {
    if (!_glRequest) {
        _glRequest = [QJHomeGLRequest new];
        
    }
    return _glRequest;
}

-(QJHomeProductListRequest *)productListReq {
    if (!_productListReq) {
        
        _productListReq = [QJHomeProductListRequest new];
        
    }
    return _productListReq;
}

- (QJHomeLikeRequest *)likeRequest {
    if (!_likeRequest) {
        _likeRequest = [QJHomeLikeRequest new];
    }
    return _likeRequest;
}

- (void)setTabModel:(QJHomeRecommendTabModel *)tabModel {
    _tabModel = tabModel;
    if (self.dataPage == 1) {
        if ([tabModel.windowType isEqualToString:@"0"]) {//游戏
            [self getProductListDataWith:@{@"page":@1,@"size":@10,@"id":tabModel.idStr?:@"" } tableView:self.leftTableView];
        }else if ([tabModel.windowType isEqualToString:@"1"]) {//资讯
            [self getGLDataWith:@{@"page":@1,@"size":@10,@"id":tabModel.idStr?:@""} tableView:self.leftTableView];
        }else {
            [self getGLDataWith:@{@"page":@1,@"size":@10,@"id":tabModel.idStr?:@""} tableView:self.leftTableView];
        }
    }
    
    
}


- (UITableView *)leftTableView {
    if (!_leftTableView) {
        _leftTableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        _leftTableView.dataSource = self;
        _leftTableView.delegate = self;
        [_leftTableView registerClass:[QJHomeGLCell class] forCellReuseIdentifier:@"QJHomeGLCell"];
        [_leftTableView registerClass:[QJHomeMainCell class] forCellReuseIdentifier:@"QJHomeMainCell"];
        _leftTableView.showsVerticalScrollIndicator = NO;
        _leftTableView.mj_header = [QJRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(refreshData)];
        _leftTableView.mj_footer = [QJRefreshFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreHomeData)];
        if (@available(iOS 11.0, *)) {
            _leftTableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
            _leftTableView.estimatedRowHeight = 0;
            _leftTableView.estimatedSectionFooterHeight = 0;
            _leftTableView.estimatedSectionHeaderHeight = 0;
        }
        
        _leftTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
         
    }
    return _leftTableView;
}

- (void)refreshData {
    [self.leftTableView.mj_footer endRefreshing];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [self.leftTableView.mj_header endRefreshing];
    });
    self.dataPage = 1;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:QJHomeRefreshAllDataNotification object:nil];
 
}


- (void)loadMoreHomeData {
    self.dataPage++;
    
    if ([self.tabModel.windowType isEqualToString:@"0"]) {//游戏
        [self getProductListDataWith:@{@"page":@(self.dataPage),@"size":@10,@"id":self.tabModel.idStr?:@"" } tableView:self.leftTableView];
    }else if ([self.tabModel.windowType isEqualToString:@"1"]) {//资讯
        [self getGLDataWith:@{@"page":@(self.dataPage),@"size":@10,@"id":self.tabModel.idStr?:@""} tableView:self.leftTableView];
    }else {
        [self getGLDataWith:@{@"page":@(self.dataPage),@"size":@10,@"id":self.tabModel.idStr?:@""} tableView:self.leftTableView];
    }
    
    
}

 

#pragma mark ------ 网络请求
//热门攻略请求
- (void)getGLDataWith:(NSDictionary *)param tableView:(UITableView *)tableView {
    WS(weakSelf)
    NSNumber *page = EncodeNumberFromDic(param, @"page");
    
    self.glRequest.dic = param;
    self.glRequest.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        
        [tableView.mj_footer endRefreshing];
        [tableView.mj_header endRefreshing];
        if (weakSelf.dataPage == 1) {
            if (weakSelf.endRefresh) {
                weakSelf.endRefresh();
            }
        }
        if (ResponseSuccess) {
         
            NSDictionary *data = EncodeDicFromDic(request.responseJSONObject, @"data");
            NSArray *glDataArr = [NSArray array];
            glDataArr = [NSArray modelArrayWithClass:[QJHomeGLModel class] json:EncodeArrayFromDic(data, @"records")];
            if (page.integerValue > 1) {//load more
                if (glDataArr.count > 0) {
                    [tableView.mj_footer endRefreshing];
                    [weakSelf makeZXDataArr:glDataArr];
                    //                    weakSelf.isLoadMore = YES;
                    [weakSelf reloadView];
                }else{
                    [tableView.mj_footer endRefreshingWithNoMoreData];
                }
            }else{//下拉刷新
                if (glDataArr.count > 0) {
//                    tableView.mj_footer = [QJRefreshFooter footerWithRefreshingTarget:weakSelf refreshingAction:@selector(loadMoreHomeData)];
                }
                weakSelf.dataArr = @[];
                [weakSelf makeZXDataArr:glDataArr];
                [weakSelf reloadView];
            }
            
            
        }
        QJDraftEmptyView *emptyView = [QJDraftEmptyView new];
        emptyView.string = @"暂无数据";
        emptyView.offsetY = 72*kWScale;
        emptyView.imageStr = @"empty_no_data";
        [weakSelf.leftTableView tableViewDisplayWhenHaveNoDataWithView:emptyView ifNecessaryForRowCount:weakSelf.dataArr.count];
        [weakSelf.leftTableView reloadData];
    };
    
    self.glRequest.failureCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        [QJAppTool showToast:ResponseFailToastMsg];
        QJDraftEmptyView *emptyView = [QJDraftEmptyView new];
        emptyView.string = @"无网络数据，请下拉刷新重试";
        emptyView.offsetY = 72*kWScale;
        emptyView.imageStr = @"empty_no_internet";
        [weakSelf.leftTableView tableViewDisplayWhenHaveNoDataWithView:emptyView ifNecessaryForRowCount:weakSelf.dataArr.count];
        [weakSelf.leftTableView reloadData];
        [tableView.mj_footer endRefreshing];
        [tableView.mj_header endRefreshing];
        if (page.integerValue > 1) {
            [tableView.mj_footer endRefreshing];
        }else{
            if (weakSelf.endRefresh) {
                weakSelf.endRefresh();
            }
            
        }
    };
    [self.glRequest start];
    
}


//热门游戏请求
- (void)getProductListDataWith:(NSDictionary *)param tableView:(UITableView *)tableView {
    WS(weakSelf)
    NSNumber *page = EncodeNumberFromDic(param, @"page");
    [tableView.mj_header endRefreshing];
    [tableView.mj_footer endRefreshing];
    self.productListReq.dic = param;
    self.productListReq.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
       
        if (weakSelf.dataPage == 1) {
            if (weakSelf.endRefresh) {
                weakSelf.endRefresh();
            }
        }
        if (ResponseSuccess) {
            
            NSDictionary *data = EncodeDicFromDic(request.responseJSONObject, @"data");
            if (page.integerValue > 1) {//加载更多
                NSArray *glDataArr = [NSArray array];
                glDataArr = [NSArray modelArrayWithClass:[QJHomeProductListModel class] json:EncodeArrayFromDic(data, @"records")];
                if (glDataArr.count > 0) {
                    [tableView.mj_footer endRefreshing];
                   
                    [weakSelf makeYXDataArr:glDataArr];
                    [weakSelf reloadView];
                }else{
                    [tableView.mj_footer endRefreshingWithNoMoreData];
                }
                
            }else{//下拉刷新
                if ([NSArray modelArrayWithClass:[QJHomeProductListModel class] json:EncodeArrayFromDic(data, @"records")].count > 0) {
//                    tableView.mj_footer = [QJRefreshFooter footerWithRefreshingTarget:weakSelf refreshingAction:@selector(loadMoreHomeData)];
                }
                weakSelf.dataArr = @[];
                [weakSelf makeYXDataArr:[NSArray modelArrayWithClass:[QJHomeProductListModel class] json:EncodeArrayFromDic(data, @"records")]];
                [weakSelf reloadView];
            }
            
            
        }
        QJDraftEmptyView *emptyView = [QJDraftEmptyView new];
        emptyView.string = @"暂无数据";
        emptyView.offsetY = 72*kWScale;
        emptyView.imageStr = @"empty_no_data";
        [weakSelf.leftTableView tableViewDisplayWhenHaveNoDataWithView:emptyView ifNecessaryForRowCount:weakSelf.dataArr.count];
        [weakSelf reloadView];
    };
    self.productListReq.failureCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
        [QJAppTool showToast:ResponseFailToastMsg];
        QJDraftEmptyView *emptyView = [QJDraftEmptyView new];
        emptyView.string = @"无网络数据，请下拉刷新重试";
        emptyView.offsetY = 72*kWScale;
        emptyView.imageStr = @"empty_no_internet";
        [weakSelf.leftTableView tableViewDisplayWhenHaveNoDataWithView:emptyView ifNecessaryForRowCount:weakSelf.dataArr.count];
        [weakSelf.leftTableView reloadData];
        [tableView.mj_header endRefreshing];
        [tableView.mj_footer endRefreshing];
        if (page.integerValue > 1) {
            [tableView.mj_footer endRefreshing];
        }else{
            if (weakSelf.dataPage == 1) {
                if (weakSelf.endRefresh) {
                    weakSelf.endRefresh();
                }
            }
        }
    };
    [self.productListReq start];
    
    
}

//处理资讯接口数据
- (void)makeZXDataArr:(NSArray *)arr {
    NSMutableArray *muarr = [NSMutableArray arrayWithArray:self.dataArr];
    WS(weakSelf)
    for (QJHomeGLModel *glModel  in arr) {
        QJHomeGLCellModel *model = [QJHomeGLCellModel new];
        model.relativeData = glModel;
        model.custHeight = Get375Width(109);
        model.didSelectedRow = ^(QJBaseCellModel * _Nonnull cellModel) {
            DLog(@"点击左边cell");
            UIViewController *vc = [QJAppTool getCurrentViewController];
            [vc checkLogin:^(BOOL isLogin) {
                if (isLogin) {
                    DLog(@"登录后返回此处刷新攻略");
                    QJConsultDetailViewController *detailView = [[QJConsultDetailViewController alloc] init];
                    
                    QJHomeGLModel *model = (QJHomeGLModel *)cellModel.relativeData;
                    if (model.idStr.length > 0) {
                        detailView.idStr = model.idStr;
                        detailView.isNews = YES;
                        
                        [vc.navigationController pushViewController:detailView animated:YES];
                    }
                    
                }
            }];
            
        };
        model.likeBtnClick = ^(QJHomeGLModel * _Nonnull cellData) {
            UIViewController *vc = [QJAppTool getCurrentViewController];
            [vc checkLogin:^(BOOL isLogin) {
                if (isLogin) {
                    DLog(@"登录后返回此处刷新");
                    weakSelf.likeRequest.idStr = cellData.idStr;
                    weakSelf.likeRequest.action = @(!cellData.liked);
                    weakSelf.likeRequest.successCompletionBlock = ^(__kindof YTKBaseRequest * _Nonnull request) {
                        if (ResponseSuccess) {
                            cellData.liked = !cellData.liked;
                            if (cellData.liked) {
                                cellData.likeCount = [NSString stringWithFormat:@"%ld",cellData.likeCount.integerValue + 1];
                            }else{
                                cellData.likeCount = [NSString stringWithFormat:@"%ld",cellData.likeCount.integerValue - 1];
                            }
                            
                            [weakSelf reloadView];
                        }else{
                            [vc.view makeToast:ResponseMsg];
                        }
                    };
                    
                    [weakSelf.likeRequest start];
                    
                }
            }];
        };
        
        model.videoBtnClick = ^(QJHomeGLModel * _Nonnull cellData) {
            UIViewController *vc = [QJAppTool getCurrentViewController];
            [vc checkLogin:^(BOOL isLogin) {
                if (isLogin) {
                    DLog(@"登录后返回此处刷新攻略");
                    QJConsultDetailViewController *detailView = [[QJConsultDetailViewController alloc] init];
                    QJHomeGLModel *model = cellData;
                    if (model.idStr.length > 0) {
                        detailView.idStr = model.idStr;
                        detailView.isNews = YES;
                        detailView.isVolume = YES;
                        [vc.navigationController pushViewController:detailView animated:YES];
                    }
                    
                }
            }];
        };
        
        [muarr addObject:model];
        
    }
    weakSelf.dataArr = muarr.copy;
}


//处理游戏接口数据
- (void)makeYXDataArr:(NSArray *)arr {
 
    WS(weakSelf)
    NSMutableArray *muarr = [NSMutableArray arrayWithArray:weakSelf.dataArr];
     
    for (QJHomeProductListModel *productModel  in arr) {
        QJHomeMainCellModel *model = [QJHomeMainCellModel new];
        model.relativeData = productModel;
        model.custHeight = Get375Width(80);
        __weak typeof(model) weakModel = model;
        model.didSelectedRow = ^(QJBaseCellModel * _Nonnull cellModel) {
            DLog(@"点击右边cell");
            UIViewController *vc = [QJAppTool getCurrentViewController];
            [vc checkLogin:^(BOOL isLogin) {
                if (isLogin) {
                    DLog(@"登录后返回此处刷新");
                    [weakSelf goToGame:weakModel.relativeData];
                }
            }];
        };
 
        [muarr addObject:model];
        
    }
    
    weakSelf.dataArr = muarr.copy;
    
    
}

#pragma mark ------- UITableViewDelegate, UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    [tableView tableViewDisplayWhenHaveNoDataWithView:[UIView new] ifNecessaryForRowCount:self.dataArr.count];
    return self.dataArr.count;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    QJBaseCellModel *model = [self.dataArr safeObjectAtIndex:indexPath.row];
    return model.custHeight;
    
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    QJHomeMainCellModel *model = nil;
    model = [self.dataArr safeObjectAtIndex:indexPath.row];
    NSString *cellIdentifier = IsStrEmpty(model.identifier) ? NSStringFromClass([model cellClass]) : model.identifier;
    if (!cellIdentifier) {
        return [UITableViewCell new];
    }
    QJBaseTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    [cell setCellModel:model];
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    QJHomeMainCellModel *model = nil;
    model = [self.dataArr safeObjectAtIndex:indexPath.row];
    if (model.didSelectedRow) {
        model.didSelectedRow(model);
    }
    
}


#pragma mark - GKPageListViewDelegate
- (UIScrollView *)listScrollView {
    
    return self.leftTableView;
}

- (void)listViewDidScrollCallback:(void (^)(UIScrollView * _Nonnull))callback {
    self.listScrollViewScrollBlock = callback;
}


#pragma mark  ------- UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    !self.listScrollViewScrollBlock ? : self.listScrollViewScrollBlock(scrollView);
    

   
}


//刷新
- (void)reloadView {
    
    [self.leftTableView reloadData];
}



@end
