//
//  QJProductCommonLabel.m
//  QJBox
//
//  Created by wxy on 2022/6/14.
//

#import "QJProductCommonLabel.h"



@interface QJProductCommonLabel ()

@property (nonatomic, strong) UILabel *contentLabel;


@end


@implementation QJProductCommonLabel



- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor colorWithHexString:@"#FDF5F5"];
        self.layer.cornerRadius = Get375Width(2);
        [self addSubview:self.contentLabel];
        [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).offset(Get375Width(6));
            make.top.equalTo(self).offset(Get375Width(4));
            make.center.equalTo(self);
            make.height.equalTo(@(Get375Width(9)));
        }];
    }
    return self;
}




#pragma mark  ------- setter && getter

- (UILabel *)contentLabel {
    if (!_contentLabel) {
        _contentLabel = [UILabel new];
        _contentLabel.font = FontMedium(8);
        _contentLabel.textColor = [UIColor colorWithHexString:@"#E5A1A1"];
        
    }
    return _contentLabel;
}

- (void)setText:(NSString *)text {
    _text = text;
    
    self.contentLabel.text = text;
    
}

- (void)setTextColor:(UIColor *)textColor{
    _textColor = textColor;
    _contentLabel.textColor = _textColor;
}

@end
