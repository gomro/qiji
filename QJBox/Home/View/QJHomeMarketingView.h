//
//  QJHomeMarketingView.h
//  QJBox
//
//  Created by wxy on 2022/7/14.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN


@class QJHomeMarketModel;
@interface QJHomeMarketingView : UIView

@property (nonatomic, strong) NSArray <QJHomeMarketModel*>*array;

@property (nonatomic, copy) void(^clickIcon)(QJHomeMarketModel *model);

@property (nonatomic, strong) UICollectionView *collectionView;
@end

NS_ASSUME_NONNULL_END
