//
//  QJHomeSectionHeadView.m
//  QJBox
//
//  Created by wxy on 2022/6/14.
//

#import "QJHomeSectionHeadView.h"
#import "QJHomeRecommendTabModel.h"
#import "QJMineSpaceSegementView.h"
#import "QJSegmentImageView.h"
@interface QJHomeSectionHeadView ()

@property (nonatomic, strong) UIView *whiteView;


@property (nonatomic, strong) UIButton *leftBtn;


@property (nonatomic, strong) UIButton *rightBtn;

@property (nonatomic, strong) UIImageView *hotImageView;

@property (nonatomic, strong) UILabel *leftLabel;//热门攻略

@property (nonatomic, strong) UILabel *rightLabel;

@property (nonatomic, strong) QJSegmentImageView *spaceSegmentView;


@end

@implementation QJHomeSectionHeadView


- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor clearColor];
        [self addSubview:self.whiteView];
        [self addSubview:self.hotImageView];
       
        
        [self.whiteView mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.edges.equalTo(self);
                     
        }];
        
        [self.hotImageView mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.equalTo(self).offset(Get375Width(17));
                    make.width.equalTo(@(Get375Width(9)));
                    make.height.equalTo(@(Get375Width(11)));
                    make.bottom.equalTo(self).offset(-(Get375Width(12)));
        }];
 
        if (!self.spaceSegmentView) {
            self.spaceSegmentView = [[QJSegmentImageView alloc]initWithFrame:CGRectZero];
            self.spaceSegmentView.font = FontRegular(16);
            self.spaceSegmentView.selectedFont =  FontSemibold(18);
            self.spaceSegmentView.textColor = kColorWithHexString(@"16191C");
            self.spaceSegmentView.selectedTextColor = kColorWithHexString(@"#16191C");
            self.spaceSegmentView.itemSpace = Get375Width(16);
            self.spaceSegmentView.showBottomLine = NO;
            self.spaceSegmentView.selectedImage = @"";//隐藏图片
        }
        [self addSubview:self.spaceSegmentView];
        [self.spaceSegmentView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.hotImageView.mas_right).offset(Get375Width(3.5));
                make.bottom.equalTo(self);
            make.top.equalTo(self).offset(6);
            make.right.equalTo(self).offset(-(Get375Width(20)));
        }];
        
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [_whiteView showCorner:Get375Width(16) rectCorner:UIRectCornerTopLeft | UIRectCornerTopRight];
}


#pragma mark  ------ setter

- (UIView *)whiteView {
    if (!_whiteView) {
        _whiteView = [UIView new];
        _whiteView.backgroundColor = [UIColor whiteColor];
        [_whiteView showCorner:Get375Width(16) rectCorner:UIRectCornerTopLeft | UIRectCornerTopRight];
        
    }
    return _whiteView;
}

 


- (UIImageView *)hotImageView {
    if (!_hotImageView) {
        _hotImageView = [UIImageView new];
        _hotImageView.image = [UIImage imageNamed:@"qj_home_hot"];
    }
    return _hotImageView;
}


//配置数据
- (void)setArrays:(NSArray *)arrays {
    _arrays = arrays;
    WS(weakSelf)
    NSMutableArray *muArr = @[].mutableCopy;
    for (QJHomeRecommendTabModel *model in arrays) {
        [muArr safeAddObject:model.name];
    }
     
    self.spaceSegmentView.segmentedSelectedItemBlock = ^(NSInteger selectedIndex) {
        if (weakSelf.headViewBlock) {
            weakSelf.headViewBlock(selectedIndex);
        }
    };
    self.spaceSegmentView.titleArray = muArr.copy;
    
}
 

#pragma mark --------- action


 
#pragma mark  ------

- (void)updateBtn:(NSInteger)page {
    [self.spaceSegmentView setSegmentCurIndex:page];
   
    
}


 

@end
