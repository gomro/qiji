//
//  QJHomeMarketingView.m
//  QJBox
//
//  Created by wxy on 2022/7/14.
//

#import "QJHomeMarketingView.h"
#import "QJHomeMarketModel.h"
#import "QJHomeMarketingCell.h"

@interface QJHomeMarketingView ()<UICollectionViewDelegate,UICollectionViewDataSource>






@end

@implementation QJHomeMarketingView

 
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self addSubview:self.collectionView];
        self.layer.cornerRadius = Get375Width(16);
        self.layer.masksToBounds = YES;
        self.backgroundColor = [UIColor whiteColor];
        [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self);
            make.top.equalTo(self).offset(Get375Width(17));
            make.bottom.equalTo(self).offset(-(Get375Width(15)));
        }];
    }
    return self;
}



#pragma mark  ----- getter


- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
        
        layout.minimumLineSpacing = 0;
        layout.minimumInteritemSpacing = 0;
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        [_collectionView registerClass:[QJHomeMarkingCollectionCell class] forCellWithReuseIdentifier:@"QJHomeMarkingCollectionCell"];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.showsHorizontalScrollIndicator = NO;
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.scrollEnabled = NO;
        
    }
    return _collectionView;
}


- (void)setArray:(NSArray<QJHomeMarketModel *> *)array {
    _array = array;
    [self.collectionView reloadData];
}



#pragma mark  -------- UICollectionViewDelegate,UICollectionViewDataSource


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.array.count;
}


- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    QJHomeMarkingCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"QJHomeMarkingCollectionCell" forIndexPath:indexPath];
    cell.model = [self.array safeObjectAtIndex:indexPath.item];
    return cell;
    
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
     
    if (self.clickIcon) {
        NSInteger index = indexPath.item;
        self.clickIcon([self.array safeObjectAtIndex:index]);
    }
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    //小于等于四个以四个为一行布局
    if (self.array.count <= 4) {
        return CGSizeMake(kScreenWidth/4.0, Get375Width(66));
    }else if (self.array.count > 4) {
        return CGSizeMake(kScreenWidth/5.0, Get375Width(66));
    }
    
    return CGSizeMake(kScreenWidth/4.0, Get375Width(66));;
    
}


@end
