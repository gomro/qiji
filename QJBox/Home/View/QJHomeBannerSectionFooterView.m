//
//  QJHomeBannerSectionFooterView.m
//  QJBox
//
//  Created by wxy on 2022/7/11.
//

#import "QJHomeBannerSectionFooterView.h"


@interface QJHomeBannerSectionFooterView ()

@property (nonatomic, strong) UIView *whiteView;

@property (nonatomic, strong) UIView *bgView;

@end


@implementation QJHomeBannerSectionFooterView

- (instancetype)initWithFrame:(CGRect)frame {
    
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor clearColor];
        [self addSubview:self.bgView];
        [self.bgView addSubview:self.whiteView];
        
        
        [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.right.equalTo(self);
                    make.height.equalTo(@(Get375Width(50)));
            make.top.equalTo(self).offset(-(Get375Width(12)));
        }];
        
        [self.whiteView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self.bgView);
            make.height.equalTo(@(Get375Width(12)));
            make.top.equalTo(self.bgView);
            
        }];
        
        
        
        
        
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [_bgView showCorner:Get375Width(16) rectCorner:UIRectCornerTopLeft | UIRectCornerTopRight];
    
}


#pragma mark  ------ setter
- (UIView *)bgView {
    if (!_bgView) {
        _bgView = [UIView new];
        _bgView.backgroundColor = [UIColor clearColor];
        _bgView.clipsToBounds = YES;
        
    }
    return _bgView;
}
- (UIView *)whiteView {
    if (!_whiteView) {
        _whiteView = [UIView new];
        _whiteView.backgroundColor = [UIColor whiteColor];
        
        
    }
    return _whiteView;
}


@end
