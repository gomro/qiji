//
//  QJHomeRecommendTabListView.h
//  QJBox
//
//  Created by wxy on 2022/9/19.
//

#import <UIKit/UIKit.h>
#import "QJHomeRecommendTabModel.h"
#import "QJHomeListTableView.h"
NS_ASSUME_NONNULL_BEGIN

@protocol QJHomeRecommendTabListViewDelegate <NSObject>

 
- (void)homeRecommendTabListScrollViewDidScroll:(UIScrollView *)scrollView;

@end

@interface QJHomeRecommendTabListView : UIView

@property (nonatomic, strong) NSArray *dataArr;

@property (nonatomic, assign) NSInteger index;//第几个

@property (nonatomic, strong) QJHomeRecommendTabModel *tabModel;//tab切数据

@property (nonatomic, weak) id<QJHomeRecommendTabListViewDelegate> delegate;

@property (nonatomic, copy) void(^loadMoreDataBlock)(NSInteger page);

@property (nonatomic, strong) UITableView *leftTableView;

@property (nonatomic, copy) void(^endRefresh)(void);

- (void)reloadView;



@end

NS_ASSUME_NONNULL_END
