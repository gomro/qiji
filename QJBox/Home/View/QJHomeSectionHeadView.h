//
//  QJHomeSectionHeadView.h
//  QJBox
//
//  Created by wxy on 2022/6/14.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

///
@interface QJHomeSectionHeadView : UIView

@property (nonatomic, copy) void (^headViewBlock)(NSInteger index);

@property (nonatomic, strong) NSArray *arrays;

- (void)updateBtn:(NSInteger)page;

@end

NS_ASSUME_NONNULL_END
