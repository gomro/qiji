//
//  QJProductCommonLabel.h
//  QJBox
//
//  Created by wxy on 2022/6/14.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QJProductCommonLabel : UIView

@property (nonatomic, copy) NSString *text;
@property (nonatomic, copy) UIColor *textColor;
@property (nonatomic, copy) UIColor *bgColor;

@end

NS_ASSUME_NONNULL_END
