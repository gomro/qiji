//
//  QJHomeMarketSectionFooterView.m
//  QJBox
//
//  Created by wxy on 2022/7/8.
//

#import "QJHomeMarketSectionFooterView.h"
@interface QJHomeMarketSectionFooterView ()


@property (nonatomic, strong) UIView *whiteView;

@property (nonatomic, strong) UIView *bgView;
@end
@implementation QJHomeMarketSectionFooterView

- (instancetype)initWithFrame:(CGRect)frame {
    
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor colorWithHexString:@"#F7F7F7"];
        [self addSubview:self.bgView];
        [self.bgView addSubview:self.whiteView];
        
        self.layer.masksToBounds = YES;
        
        [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self);
            make.height.equalTo(@(Get375Width(50)));
            make.bottom.equalTo(self).offset(-(Get375Width(10)));
        }];
        
        [self.whiteView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self.bgView);
            make.height.equalTo(@(Get375Width(20)));
            make.bottom.equalTo(self.bgView);
            
        }];
        
        
        
        
        
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [_bgView showCorner:Get375Width(16) rectCorner:UIRectCornerBottomLeft | UIRectCornerBottomRight];
    
}


#pragma mark  ------ setter

- (UIView *)whiteView {
    if (!_whiteView) {
        _whiteView = [UIView new];
        _whiteView.backgroundColor = [UIColor whiteColor];
        
        
    }
    return _whiteView;
}

- (UIView *)bgView {
    if (!_bgView) {
        _bgView = [UIView new];
        _bgView.backgroundColor = [UIColor clearColor];
        _bgView.clipsToBounds = YES;
        
    }
    return _bgView;
}



@end
