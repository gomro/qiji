//
//  QJNestScrollView.m
//  QJBox
//
//  Created by wxy on 2022/6/23.
//

#import "QJNestScrollView.h"


@interface QJNestScrollView ()<UIGestureRecognizerDelegate>



@end

@implementation QJNestScrollView

//- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
//    UIView *view = [super hitTest:point withEvent:event];
//    if (view == self) {
//        return nil;
//    }
//    return view;
//}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
    DLog(@"gesture1 = %@",gestureRecognizer.view);
    DLog(@"gesture2 = %@",otherGestureRecognizer.view);
    CGFloat height = otherGestureRecognizer.view.frame.size.height;
    if (height == 279*kWScale) {
        return NO;
    }
    return YES;
}


@end
