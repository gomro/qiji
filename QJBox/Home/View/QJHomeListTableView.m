//
//  QJHomeListTableView.m
//  QJBox
//
//  Created by wxy on 2022/7/14.
//

#import "QJHomeListTableView.h"

@implementation QJHomeListTableView


- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    
    
    UIView *view = [super hitTest:point withEvent:event];
    if (view == self) {
        return nil;
    }
    return view;
}


@end
