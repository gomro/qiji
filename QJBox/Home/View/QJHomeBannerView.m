//
//  QJHomeBannerView.m
//  QJBox
//
//  Created by wxy on 2022/7/14.
//

#import "QJHomeBannerView.h"
#import "SPCycleScrollView.h"
#import "QJHomeBannerModel.h"
@interface QJHomeBannerView ()

@property (nonatomic, strong) SPCycleScrollView *cycleImageView;

@property (nonatomic, strong) NSArray *array;

@end



@implementation QJHomeBannerView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {

        [self addSubview:self.cycleImageView];
        
   
        
        [self.cycleImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self);
            make.right.equalTo(self);
            make.top.bottom.equalTo(self);
        }];
    }
    return self;
}



- (void)setImages:(NSArray *)urls {
    NSMutableArray *muArr = [NSMutableArray array];
    for (QJHomeBannerModel *model in urls) {
        if (!IsStrEmpty(model.imgUrl)) {
            NSString *str = [model.imgUrl checkImageUrlString];
            [muArr safeAddObject:str];
        }
    }
    self.cycleImageView.urlImages = muArr.copy;
    self.array = muArr.copy;
    WS(weakSelf)
    self.cycleImageView.clickedImageBlock = ^(NSUInteger index) {
        if (weakSelf.bannerClick) {
            weakSelf.bannerClick([weakSelf.array safeObjectAtIndex:index]);
        }
    };
    
}

// 是否自动轮播
- (void)setAutoScroll:(BOOL)autoScroll {
    _autoScroll = autoScroll;
    self.cycleImageView.autoScroll = autoScroll;
}

#pragma mark ------- getter


- (SPCycleScrollView *)cycleImageView {
    if (!_cycleImageView) {
        _cycleImageView = [[SPCycleScrollView alloc]init];
        _cycleImageView.duration = 3.0;
        _cycleImageView.pageControl.hidden = YES;
        
    }
    return _cycleImageView;
}


 

@end
