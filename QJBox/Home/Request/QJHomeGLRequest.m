//
//  QJHomeGLRequest.m
//  QJBox
//
//  Created by wxy on 2022/7/6.
//

#import "QJHomeGLRequest.h"

@implementation QJHomeGLRequest
- (NSString *)requestUrl {
    return @"/home/strategyRecommendContent";
}


 

- (YTKRequestMethod)requestMethod {
    
    return YTKRequestMethodGET;
}


- (YTKRequestSerializerType)requestSerializerType {
    
    return YTKRequestSerializerTypeHTTP;
}


@end
