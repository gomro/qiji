//
//  QJHomeRecommendTabTypeRequest.m
//  QJBox
//
//  Created by wxy on 2022/9/19.
//

#import "QJHomeRecommendTabTypeRequest.h"

@implementation QJHomeRecommendTabTypeRequest


- (NSString *)requestUrl {
    
    return @"/home/recommend";
    
}


 

- (YTKRequestMethod)requestMethod {
    
    return YTKRequestMethodGET;
}


- (YTKRequestSerializerType)requestSerializerType {
    
    return YTKRequestSerializerTypeHTTP;
}





@end
