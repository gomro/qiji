//
//  QJHomeSaveToMyListRequest.m
//  QJBox
//
//  Created by wxy on 2022/9/6.
//

#import "QJHomeSaveToMyListRequest.h"

@implementation QJHomeSaveToMyListRequest


- (NSString *)requestUrl {
    
    return @"/game/saveMyGame";
    
}


 

- (YTKRequestMethod)requestMethod {
    
    return YTKRequestMethodPOST;
}


- (YTKRequestSerializerType)requestSerializerType {
    
    return YTKRequestSerializerTypeJSON;
}


@end
