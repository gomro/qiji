//
//  QJHomeSaveToMyListRequest.h
//  QJBox
//
//  Created by wxy on 2022/9/6.
//

#import "QJBaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJHomeSaveToMyListRequest : QJBaseRequest

@end

NS_ASSUME_NONNULL_END
