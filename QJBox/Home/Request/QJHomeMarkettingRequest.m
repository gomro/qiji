//
//  QJHomeMarkettingRequest.m
//  QJBox
//
//  Created by wxy on 2022/7/6.
//

#import "QJHomeMarkettingRequest.h"

@implementation QJHomeMarkettingRequest


- (NSString *)requestUrl {
    return @"/home/layoutContent";
}


 

- (YTKRequestMethod)requestMethod {
    
    return YTKRequestMethodGET;
}


- (YTKRequestSerializerType)requestSerializerType {
    
    return YTKRequestSerializerTypeHTTP;
}




@end
