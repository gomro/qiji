//
//  QJHomeProductListRequest.m
//  QJBox
//
//  Created by wxy on 2022/7/6.
//

#import "QJHomeProductListRequest.h"

@implementation QJHomeProductListRequest


- (NSString *)requestUrl {
    return @"/home/gameRecommendContent";
}


 

- (YTKRequestMethod)requestMethod {
    
    return YTKRequestMethodGET;
}


- (YTKRequestSerializerType)requestSerializerType {
    
    return YTKRequestSerializerTypeHTTP;
}






@end
