//
//  QJHomeLikeRequest.h
//  QJBox
//
//  Created by wxy on 2022/7/27.
//

#import "QJBaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface QJHomeLikeRequest : QJBaseRequest


@property (nonatomic, copy) NSString *idStr;

@property (nonatomic, strong) NSNumber *action;


@end

NS_ASSUME_NONNULL_END
