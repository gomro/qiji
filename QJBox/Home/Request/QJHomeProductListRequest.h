//
//  QJHomeProductListRequest.h
//  QJBox
//
//  Created by wxy on 2022/7/6.
//

#import "QJBaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

/// 推荐游戏列表查询
@interface QJHomeProductListRequest : QJBaseRequest


@end

NS_ASSUME_NONNULL_END
