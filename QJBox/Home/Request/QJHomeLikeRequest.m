//
//  QJHomeLikeRequest.m
//  QJBox
//
//  Created by wxy on 2022/7/27.
//

#import "QJHomeLikeRequest.h"

@implementation QJHomeLikeRequest

- (NSString *)requestUrl {
    
    return [NSString stringWithFormat:@"/like/1/%@/%@",self.idStr,self.action];
    
}


 

- (YTKRequestMethod)requestMethod {
    
    return YTKRequestMethodPUT;
}


- (YTKRequestSerializerType)requestSerializerType {
    
    return YTKRequestSerializerTypeHTTP;
}






@end
