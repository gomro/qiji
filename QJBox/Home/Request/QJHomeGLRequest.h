//
//  QJHomeGLRequest.h
//  QJBox
//
//  Created by wxy on 2022/7/6.
//

#import "QJBaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

/// 推荐攻略列表
@interface QJHomeGLRequest : QJBaseRequest

@end

NS_ASSUME_NONNULL_END
