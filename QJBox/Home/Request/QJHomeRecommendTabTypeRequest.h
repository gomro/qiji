//
//  QJHomeRecommendTabTypeRequest.h
//  QJBox
//
//  Created by wxy on 2022/9/19.
//

#import "QJBaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

/// 推荐模块tab切 动态接口返回  tab数量和内容   “热门游戏”  “热门推荐”
@interface QJHomeRecommendTabTypeRequest : QJBaseRequest




@end

NS_ASSUME_NONNULL_END
